﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class HealthController : MonoBehaviour {

	
   // variable lebenstaerke
	public float currentHealth = 100;
	public float damageEffectPause =0.2F;
	


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        }
	

	//übergabe Parameter
	void ApplyDamage (float damage)
	{

		if (currentHealth > 0) {

			currentHealth -= damage;
	

			if (currentHealth < 0)
				currentHealth = 0;

			if (currentHealth == 0) {
				
				//Gameover //oder Neustart kann man noch ändern
				RestartScene ();
			}

			else 
			{
				// blink funktion
				StartCoroutine(DamageEffect());
			}

			
		}
	}

			// Funktion neustart der szene
	void RestartScene()
		{
		// index des level was gerade gespielt wird
		//Application.LoadLevel(Application.loadedLevel);
      //  SceneManager.LoadScene("Wald", LoadSceneMode.Additive);
    }

	IEnumerator DamageEffect()

	{
		GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds (damageEffectPause);
		GetComponent<Renderer>().enabled = true;
		GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds (damageEffectPause);
		GetComponent<Renderer>().enabled = true;
		GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds (damageEffectPause);
		GetComponent<Renderer>().enabled = true;
	}




	}
	



 