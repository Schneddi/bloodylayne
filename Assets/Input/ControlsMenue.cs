﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class ControlsMenue : MonoBehaviour
    {
        public InputActionAsset inputActionAsset;
        GameObject listeningWindow;
        Button lastActiveOptionsButton;
        private bool listenForActionMap;
        private CurrentBindText[] currentBindTexts;

        public void Start()
        {
            listeningWindow = transform.Find("ListeningWindow").gameObject;
            currentBindTexts = GetComponentsInChildren<CurrentBindText>();
        }

        public void RebindPlayerInput(InputActionReference actionToRebindReference, CurrentBindText textToUpdate, string bindingNameString, int bindingIndex)
        {
            InputAction actionToRebind = actionToRebindReference.action;
            if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
            {
                lastActiveOptionsButton = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
            }
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);
            listeningWindow.SetActive(true);
            actionToRebind.Disable();
            if (string.IsNullOrEmpty(bindingNameString))
            {
                InputActionRebindingExtensions.RebindingOperation rebindOperation = actionToRebind.PerformInteractiveRebinding()
                            .WithControlsExcluding("<Gamepad>/buttonSouth")
                            .WithCancelingThrough("<Keyboard>/escape")
                            .WithCancelingThrough("<Gamepad>/start")
                            .WithTargetBinding(bindingIndex)
                            .OnMatchWaitForAnother(0.1f)
                            .OnComplete(
                                operation =>
                                {
                                    operation.Dispose();
                                    lastActiveOptionsButton.Select();
                                    listeningWindow.SetActive(false);
                                    actionToRebind.Enable();
                                    GameObject.Find("GameController").transform.GetComponent<GameController>().StoreControlOverrides(actionToRebindReference.asset, "playerBindings");

                                    textToUpdate.setBindingsText();
                                    GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().InvokeBindingsDisplayChangedEvent();
                                }
                            )
                            .Start();
            }
            else
            {
                int index=0;
                for(int i=0; i < actionToRebind.bindings.Count; i++)
                {
                    if (actionToRebind.bindings[i].name == bindingNameString)
                    {
                        index = i;
                        break;
                    }
                }
                InputActionRebindingExtensions.RebindingOperation rebindOperation = actionToRebind.PerformInteractiveRebinding(index)
                            .WithControlsExcluding("<Gamepad>/buttonSouth")
                            .WithCancelingThrough("<Keyboard>/escape")
                            .WithCancelingThrough("<Gamepad>/start")
                            .WithTargetBinding(bindingIndex)
                            .OnMatchWaitForAnother(0.1f)
                            .OnComplete(
                                operation =>
                                {
                                    operation.Dispose();
                                    lastActiveOptionsButton.Select();
                                    listeningWindow.SetActive(false);
                                    actionToRebind.Enable();
                                    GameObject.Find("GameController").transform.GetComponent<GameController>().StoreControlOverrides(actionToRebindReference.asset, "playerBindings");

                                    textToUpdate.setBindingsText();
                                    GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().InvokeBindingsDisplayChangedEvent();
                                }
                            )
                            .Start();
            }
        }

        public void resetKeybindings()
        {
            GameObject.Find("GameController").transform.GetComponent<GameController>().ResetKeybindings(inputActionAsset);
        }

        public void RefreshBindTexts()
        {
            if(currentBindTexts == null ) currentBindTexts = GetComponentsInChildren<CurrentBindText>();
            foreach(CurrentBindText currentBindText in currentBindTexts)
            {
                currentBindText.setBindingsText();
            }
        }
    }
}
