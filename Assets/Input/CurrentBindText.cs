﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;


namespace UnityStandardAssets._2D
{
    public class CurrentBindText : MonoBehaviour
    {
        ControlsMenue controlsMenue;
        Text currentBindingText1, currentBindingText2;
        public InputActionReference actionReference;
        public string bindingNameString;
        string currentBindingsText1, currentBindingsText2;
        private GlobalVariables gv;
        private int _currentBindIndex1, _currentBindIndex2;

        // Start is called before the first frame update
        void Start()
        {
            controlsMenue = transform.parent.gameObject.GetComponent<ControlsMenue>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            setBindingsText();
        }

        public void RebindKeyOperationKeyboard(int button)
        {
            int bindIndex;
            switch (button)
            {
                case 0:
                    bindIndex = _currentBindIndex1;
                    break;
                case 1:
                    bindIndex = _currentBindIndex2;
                    break;
                default:
                    bindIndex = _currentBindIndex1;
                    break;
            }

            if (bindIndex == -1)
            {
                //create new Binding
                actionReference.asset.actionMaps[0].AddBinding(actionReference.action.bindings[0].path, actionReference.action);
                bindIndex = actionReference.action.bindings.Count-1;
            }
            controlsMenue.RebindPlayerInput(actionReference, this, bindingNameString, bindIndex);
        }

        public void setBindingsText()
        {
            if(gv == null) gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            currentBindingsText1 = "";
            currentBindingsText2 = "";
            
            int bindIndex = 0;
            if (string.IsNullOrEmpty(bindingNameString))
            {
                foreach (InputBinding binding in actionReference.action.bindings)
                {
                    if (CheckForSelectedControlScheme(binding.path))
                    {
                        bindIndex++;
                        continue;
                    }
                    
                    if (!String.IsNullOrEmpty(currentBindingsText1))
                    {
                        currentBindingsText2 = DeleteControlDeviceFromString(InputControlPath.ToHumanReadableString(binding.effectivePath));
                        _currentBindIndex2 = bindIndex;
                        break;
                    }
                    currentBindingsText1 = DeleteControlDeviceFromString(InputControlPath.ToHumanReadableString(binding.effectivePath));
                    _currentBindIndex1 = bindIndex;
                    _currentBindIndex2 = -1;
                    bindIndex++;
                }
            }
            else
            {
                foreach (InputBinding binding in actionReference.action.bindings)
                {
                    if (binding.name == bindingNameString)
                    {
                        if (CheckForSelectedControlScheme(binding.path))
                        {
                            bindIndex++;
                            continue;
                        }
                        
                        if (!String.IsNullOrEmpty(currentBindingsText1))
                        {
                            currentBindingsText2 = DeleteControlDeviceFromString(InputControlPath.ToHumanReadableString(binding.effectivePath));
                            _currentBindIndex2 = bindIndex;
                            break;
                        }
                        currentBindingsText1 = DeleteControlDeviceFromString(InputControlPath.ToHumanReadableString(binding.effectivePath));
                        _currentBindIndex1 = bindIndex;
                        _currentBindIndex2 = -1;
                    }
                    bindIndex++;
                }
            }

            if (String.IsNullOrEmpty(currentBindingsText2))
            {
                currentBindingsText2 = "-";
            }
            
            //apply new text
            currentBindingText1 = transform.Find("Current Binding Text 1").GetComponentInChildren<Text>();
            currentBindingText1.text = currentBindingsText1;
            currentBindingText2 = transform.Find("Current Binding Text 2").GetComponentInChildren<Text>();
            currentBindingText2.text = currentBindingsText2;

            bool CheckForSelectedControlScheme(string bindingPath)
            {
                string compareString = gv.currentControlDevice.ToString();
                
                switch (gv.currentControlDevice)
                {
                    case CurrentControlDevice.MouseKeyboard:
                        if (!bindingPath.Contains("Mouse") && !bindingPath.Contains("Keyboard")) return true;
                        break;
                    case CurrentControlDevice.Gamepad:
                        if (!bindingPath.Contains(compareString)) return true;
                        break;
                    case CurrentControlDevice.XInputController:
                        if (!bindingPath.Contains("Controller") && !bindingPath.Contains(compareString)) return true;
                        break;
                }
                
                return false;
            }
            
            string DeleteControlDeviceFromString(string inputString)
            {
                int endPos = inputString.IndexOf("[");
                return inputString.Substring(0,endPos);
            }
        }
    }
}
