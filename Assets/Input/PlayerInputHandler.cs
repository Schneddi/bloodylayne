using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XInput;
using UnityEngine.UI;
using UnityStandardAssets._2D;

public class PlayerInputHandler : MonoBehaviour, PlayerInputActions.IPlayerControlsActions
{
    public bool listenForInput;
    
    private GlobalVariables gv;
    private PlayerInputActions _playerInputActions;
    private ControlsMenue _controlsMenu;
    private Dropdown _controlsDropdown;
    private int _scene;
    
    // Start is called before the first frame update
    private void Start()
    {
        gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        _playerInputActions = new PlayerInputActions();
        _playerInputActions.PlayerControls.SetCallbacks(this);
        _playerInputActions.PlayerControls.Enable();
        listenForInput = true;
        _controlsMenu = GameObject.Find("Canvas PauseUI").transform.Find("Controls").GetComponent<ControlsMenue>();
        _controlsDropdown = _controlsMenu.transform.Find("Control Scheme").GetComponent<Dropdown>();
    }
    
    public void SetCurrentControlDevice(CurrentControlDevice currentControlDevice)
    {
        if (currentControlDevice == gv.currentControlDevice) return;
        gv.currentControlDevice = currentControlDevice;
        switch (currentControlDevice)
        {
            case CurrentControlDevice.MouseKeyboard:
                _controlsDropdown.SetValueWithoutNotify(0);
                break;
            case CurrentControlDevice.Gamepad:
                _controlsDropdown.SetValueWithoutNotify(1);
                break;
            case CurrentControlDevice.XInputController:
                _controlsDropdown.SetValueWithoutNotify(2);
                break;
        }
        _controlsMenu.RefreshBindTexts();
        gv.InvokeBindingsDisplayChangedEvent();
    }

    private void RegisterInputAction(InputAction.CallbackContext context)
    {
        if (_scene is 1 or 2 or 3) return;
        if (!listenForInput) return;
        
        CurrentControlDevice currentControlDevice = gv.currentControlDevice;
        if (context.control.device is Keyboard || context.control.device is Mouse)
        {
            currentControlDevice = CurrentControlDevice.MouseKeyboard;
        }
        if (context.control.device is Gamepad)
        {
            currentControlDevice = CurrentControlDevice.Gamepad;
        }
        if (context.control.device is XInputController)
        {
            currentControlDevice = CurrentControlDevice.XInputController;
        }
        
        SetCurrentControlDevice(currentControlDevice);
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        RegisterInputAction(context);
    }

    public void OnBloodmagic(InputAction.CallbackContext context)
    {
        RegisterInputAction(context);
    }

    public void OnBite(InputAction.CallbackContext context)
    {
        RegisterInputAction(context);
    }

    public void OnCrouch(InputAction.CallbackContext context)
    {
        RegisterInputAction(context);
    }

    public void OnEpicAbility(InputAction.CallbackContext context)
    {
        RegisterInputAction(context);
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        RegisterInputAction(context);
    }

    public void OnAimPositionInput(InputAction.CallbackContext context)
    {
        RegisterInputAction(context);
    }

    public void OnAimDirectionInput(InputAction.CallbackContext context)
    {
        RegisterInputAction(context);
    }
    
    private void OnDisable()
    {
        _playerInputActions.PlayerControls.RemoveCallbacks(this);
        _playerInputActions.PlayerControls.Disable();
    }
}
