﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class HealthKit : MonoBehaviour
    {

        public int healthPoints = 10;
        public GameObject partikel;
        PowerUp powerUpScript;
        PlatformerCharacter2D playerChar;
        AudioSource sounds;


        // Use this for initialization

        void Start()
        {
            playerChar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            sounds = GetComponent<AudioSource>();
            powerUpScript = GetComponent<PowerUp>();
        }


        // Update is called once per frame
        IEnumerator OnTriggerEnter2D(Collider2D other)
        {
            if (powerUpScript.active)
            {
                if (other.gameObject.tag == "Player")
                {
                    playerChar.AddMaxHealth(healthPoints);
                    playerChar.ChangeBloodthirst(15, Guid.Empty);
                    if (playerChar.skill9choice1 && !playerChar.skill9u1Spawned)
                    {
                        playerChar.SpawnSoulSkill9u1();
                    }
                    if (playerChar.skill9choice2)
                    {
                        playerChar.SpawnSoulSkill9u2();
                    }
                    GetComponent<BoxCollider2D>().enabled = false;
                    GetComponent<SpriteRenderer>().enabled = false;
                    sounds.PlayDelayed(0);
                    Instantiate(partikel, transform.position, Quaternion.identity);
                    yield return new WaitForSeconds(0.8f);
                    Destroy(gameObject);
                }
            }
        }
    }
}


 

