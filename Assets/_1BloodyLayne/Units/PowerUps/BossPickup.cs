using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    //A pick up dropped in BossFights, which enables abilities, which normally require Souls or Chalices
    public class BossPickup : MonoBehaviour
    {
        public int healthPoints = 50;
        public GameObject pickUpParticle;
        
        private PowerUp powerUpScript;
        private Rigidbody2D rb;
        private GameObject m_lightObject;

        // Use this for initialization
        void Awake()
        {
            powerUpScript = GetComponent<PowerUp>();
            m_lightObject = transform.Find("Point Light 2D").gameObject;
        }

        public void spawn(Vector2 pushDirection, float rotation)
        {
            rb = GetComponent<Rigidbody2D>();
            rb.AddForce(pushDirection);
            rb.rotation = rotation;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (powerUpScript.active)
            {
                if (other.gameObject.CompareTag("Player"))
                {
                    GetComponent<CircleCollider2D>().enabled = false;
                    GetComponent<SpriteRenderer>().enabled = false;
                    GetComponent<ParticleSystem>().Stop();
                    transform.Find("GlowParticle").GetComponent<ParticleSystem>().Stop();
                    Destroy(m_lightObject);
                    GameObject pickUpParticleInstance =
                        Instantiate(pickUpParticle, transform.position, Quaternion.identity);
                    pickUpParticleInstance.GetComponent<BossPickUpParticle>().healthPoints = healthPoints;
                    Destroy(gameObject);
                }
            }
        }
    }
}