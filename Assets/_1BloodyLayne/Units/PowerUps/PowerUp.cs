﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public bool active, startFadedOut;
    bool fadeIn;
    SpriteRenderer sRenderer;
    Color fadeInColor;
    // Start is called before the first frame update
    void Start()
    {
        fadeInColor = new Color(1f, 1, 1, 1f);
        sRenderer = GetComponent<SpriteRenderer>();
        if (startFadedOut)
        {
            sRenderer.color = new Color(0, 0, 0, 0);
        }
        else
        {
            active = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (fadeIn)
        {
            sRenderer.color = new Color(sRenderer.color.r, sRenderer.color.g, sRenderer.color.b,
                sRenderer.color.a + Time.deltaTime * 2f);
        }

        if (sRenderer.color.a > 0.99f && !active)
        {
            sRenderer.color = fadeInColor;
            active = true;
            fadeIn = false;
        }
    }

    public void spawn()
    {
        fadeIn = true;
    }
}
