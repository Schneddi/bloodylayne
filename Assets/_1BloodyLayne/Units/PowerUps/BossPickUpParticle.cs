using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEditor;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class BossPickUpParticle : MonoBehaviour
    {
        public float stage1Duration = 1;
        public float stage2Duration = 0.6f;
        public float stage3Duration = 0.2f;
        public int healthPoints = 50;
        
        private Transform playerTransform;
        private PlatformerCharacter2D player;
        private bool move;
        private float stage2StartTime;
        private Vector3 startPositon;
        private AudioSource[] splatterSound;
        
        // Start is called before the first frame update
        void Start()
        { 
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            startPositon = transform.position;
            playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
            StartCoroutine(waitToMove());
        }

        // Update is called once per frame
        void Update()
        {
            if (move)
            {
                float progressionPercentage = (Time.time - stage2StartTime) / stage2Duration;
                transform.position = Vector3.Lerp(startPositon, playerTransform.position, progressionPercentage);
            }
        }

        IEnumerator waitToMove()
        {
            yield return new WaitForSeconds(stage1Duration);
            stage2StartTime = Time.time;
            move = true;
            yield return new WaitForSeconds(stage2Duration);
            yield return new WaitForSeconds(stage3Duration);
            
            player.AddHealth(healthPoints);
            player.ChangeBloodthirst(player.GetComponent<PlayerStats>().maxBloodlust, Guid.Empty);
            if (player.skill9choice1 && !player.skill9u1Spawned)
            {
                player.SpawnSoulSkill9u1();
            }
            if (player.skill9choice2 && !player.skill9u2Spawned)
            {
                player.SpawnSoulSkill9u2();
            }
                    
            if (player.skill12choice1)
            {
                player.RdySkill12u1();
            }
            if (player.skill12choice2)
            {
                player.RdySkill12u2();
            }
                    
            if (player.skill11choice1 && player.skill11u1Stacks < 5)
            {
                player.AddSkill11u1Stack(5, System.Guid.NewGuid());
                GameObject hateChild = Instantiate(player.killingSpreeEffectSkill11u1, player.transform.position, Quaternion.identity);
                hateChild.GetComponent<AudioSource>().pitch = hateChild.GetComponent<AudioSource>().pitch + player.skill11u1Stacks * 0.2f;
                hateChild.transform.parent = player.gameObject.transform;
            }
            yield return new WaitForSeconds(0.85f);
            Destroy(gameObject);
        }
    }
}