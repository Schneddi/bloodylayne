﻿using System.Collections;
using System.Collections.Generic;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D 
{
    public class LebensKit : MonoBehaviour {
	
	    public int leben = 1;
	    PlatformerCharacter2D player;
        AudioSource[] sounds;
        AudioSource fangen, loop;
        PowerUp powerUpScript;
        bool catched;
        Animator animator;
        float startingScale, secondsTillDestroy = 1;
        private GameObject m_lightObject;

        void Start()
        {
            sounds = GetComponents<AudioSource>();
            fangen = sounds[0];
            loop = sounds[1];
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            powerUpScript = GetComponent<PowerUp>();
            animator = GetComponent<Animator>();
            startingScale = transform.localScale.x;
            m_lightObject = transform.Find("Point Light 2D").gameObject;
        }

        void Update()
        {
            if (catched)
            {
                transform.position = player.transform.position;
                float modifier = transform.localScale.x - Time.deltaTime * startingScale * secondsTillDestroy;
                transform.localScale = new Vector3(modifier, modifier, transform.localScale.z);
                animator.speed = animator.speed + Time.deltaTime * 4;
            }
        }


        // Update is called once per frame
        IEnumerator OnTriggerEnter2D (Collider2D other)
	    {
            if (powerUpScript.active)
            {
                if (other.gameObject.name == "Player")
                {
                    Destroy(m_lightObject);
                    animator.speed += 0.5f;
                    GetComponent<BoxCollider2D>().enabled = false;
                    fangen.PlayDelayed(0);
                    loop.Stop();
                    catched = true;
                    yield return new WaitForSeconds(secondsTillDestroy);
                    GetComponent<SpriteRenderer>().enabled = false;
                    if (player.skill12choice1)
                    {
                        player.RdySkill12u1();
                    }
                    if (player.skill12choice2)
                    {
                        player.RdySkill12u2();
                    }
                    player.GetComponent<PlayerStats>().lifes = player.GetComponent<PlayerStats>().lifes + leben;
                    Destroy(gameObject);
                }
            }
	    }

	}
}


 