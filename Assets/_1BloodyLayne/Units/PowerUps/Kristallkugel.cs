﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kristallkugel : MonoBehaviour {
    Transform kugel;
	// Use this for initialization
	void Start () {
        kugel = gameObject.transform;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(new Vector3(0,0,5) * Time.unscaledDeltaTime * 10, Space.World);
	}
}
