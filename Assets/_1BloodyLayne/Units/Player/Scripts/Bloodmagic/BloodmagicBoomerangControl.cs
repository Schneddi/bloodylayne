using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityStandardAssets._2D;

public class BloodmagicBoomerangControl : MonoBehaviour
{
    public GameObject deathEffect;
    public float lifetime = 4;
    
    private Rigidbody2D rb;
    private bool flyBack, timeOut;
    private BlutmagieFunctions bFunctions;
    private List<GameObject> hitEnemies;
    private float startTime;
    private Transform boomerangSprite;
    private bool deathAnimation;
    private Light2D light2D;
    private float deathAnimationDuration = 0.1f, startLightIntensity, deathAnimStartTime;

    // Use this for initialization
    void Start()
    {
        bFunctions = GetComponent<BlutmagieFunctions>();
        hitEnemies = new List<GameObject>();
        rb = GetComponent<Rigidbody2D>();
        boomerangSprite = transform.Find("BoomerangSprite");
        light2D = transform.Find("Point Light 2D").GetComponent<Light2D>();
        startLightIntensity = light2D.intensity;
        launch();
        StartCoroutine(bFunctions.delayAndDie(lifetime));
    }

    // Update is called once per frame
    void Update()
    {
        damageEnemys();
        boomerangSprite.Rotate (new Vector3 (0, 0, 1500) * Time.deltaTime);
        if (Time.time - startTime > lifetime/2 && !flyBack)
        {
            bounce();
        }

        if (Time.time - startTime > lifetime && !timeOut)
        {
            timeOut = true;
            StartCoroutine(Death());
        }
        if(deathAnimation && light2D.intensity > 0)
        {
            light2D.intensity = startLightIntensity * (1 - ( (Time.time-deathAnimStartTime) / deathAnimationDuration));
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ground") && !flyBack)
        {
            bounce();
        }
        else if (other.gameObject.CompareTag("Enemy"))
        {
            if (!hitEnemies.Contains(other.gameObject))
            {
                other.gameObject.GetComponent<EnemyStats>().Hit(bFunctions.attackDamage, 0, bFunctions.guid, true);
                bFunctions.DeathFunctions(transform.position);
                hitEnemies.Add(other.gameObject);
                bFunctions.DeathFunctions(transform.position);
            }
        }
    }
    
    private void launch()
    {
        startTime = Time.time;
        rb.velocity = bFunctions.speed * bFunctions.direction;
        setRotation();
    }
    
    private void bounce()
    {
        flyBack = true;
        hitEnemies.Clear();
        bFunctions.speed *= -1;
        rb.velocity = bFunctions.speed * bFunctions.direction;
        setRotation();
    }

    private void setRotation()
    {
        transform.rotation = Quaternion.LookRotation(Vector3.back, new Vector3(bFunctions.direction.x, bFunctions.direction.y, 0));
        transform.Rotate(new Vector3(0,0,180));
    }
    
    void damageEnemys()
    {
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.3f, 1 << 9);
        {
            foreach (Collider2D col in cols)
            {
                if ((col.gameObject.GetComponent("EnemyStats") as EnemyStats) != null && !hitEnemies.Contains(col.gameObject))
                {
                    col.gameObject.GetComponent<EnemyStats>().Hit(bFunctions.attackDamage, 0, bFunctions.guid, true);
                    bFunctions.DeathFunctions(transform.position);
                    hitEnemies.Add(col.gameObject);
                    bFunctions.DeathFunctions(transform.position);
                }
            }
        }
    }
    

    IEnumerator Death()
    {
        rb.velocity = new Vector2(0, 0);
        rb.isKinematic = true;
        GetComponent<CircleCollider2D>().enabled = false;
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        deathAnimStartTime = Time.time;
        deathAnimation = true;
        yield return new WaitForSeconds(deathAnimationDuration);
        Destroy(gameObject);
    }
}
