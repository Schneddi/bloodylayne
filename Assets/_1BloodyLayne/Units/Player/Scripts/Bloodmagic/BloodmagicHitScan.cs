using System;
using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class BloodmagicHitScan : MonoBehaviour
{
    public GameObject rayParticle, missEffect, invulnerableMissEffect;
    public float range, rayParticlesDistance, deathAnimationDuration;
    public Quaternion directionRotation;

    private BlutmagieFunctions bFunctions;
    private Light2D light2D;
    private float startLightIntensity, deathAnimStartTime;
    private PlatformerCharacter2D playerChar;
    private bool invulnerableMiss;

    // Use this for initialization
    void Awake()
    {
        bFunctions = GetComponent<BlutmagieFunctions>();
        light2D = transform.Find("Point Light 2D").GetComponent<Light2D>();
        startLightIntensity = light2D.intensity;
        deathAnimStartTime = Time.time;
        playerChar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
        if (deathAnimationDuration == 0)
        {
            Debug.Log("deathAnimationDuration is set to 0 in BloodmagicHitScan in GameObject " + gameObject.name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(light2D.intensity > 0)
        {
            float newLightIntensity = startLightIntensity * (1 - ( (Time.time-deathAnimStartTime) / deathAnimationDuration));
            if (newLightIntensity < 0)
            {
                newLightIntensity = 0;
                Destroy(gameObject);
            }
            light2D.intensity = newLightIntensity;
        }
    }
    
    public void Launch(GameObject muzzleFlashEffect)
    {
        SetRotation(muzzleFlashEffect);
        
        RaycastHit2D hit = Physics2D.Raycast(transform.position, bFunctions.direction, range, 1<<9 | 1<<11 | 1<<14 | 1<<15);
        Vector3 endPoint; 
        if (hit.collider != null)
        {
            endPoint = hit.point;
            DoDamage(hit.collider.gameObject);
        }
        else
        {
            bFunctions.miss = true;
            endPoint = transform.position + (Vector3)bFunctions.direction.normalized * range;
        }

        
        int totalParticleEffects = (int)Math.Round(Vector2.Distance(transform.position, endPoint) / rayParticlesDistance, 0, MidpointRounding.ToEven);
        
        for(int i = 0; i < totalParticleEffects; i++)
        {
            Vector2 nextParticlePosition = (Vector2)transform.position + bFunctions.direction * rayParticlesDistance * i;
            Instantiate(rayParticle,nextParticlePosition, directionRotation);
        }

        bFunctions.DeathFunctions(endPoint);
        
        if (bFunctions.miss)
        {
            if (invulnerableMiss)
            {
                Instantiate(invulnerableMissEffect, endPoint, transform.rotation);
            }
            else
            {
                Instantiate(missEffect, endPoint, transform.rotation);
            }
        }
    }

    private void SetRotation(GameObject muzzleFlashEffect)
    {
        transform.rotation = Quaternion.LookRotation(Vector3.back, new Vector3(bFunctions.direction.x, bFunctions.direction.y, 0));
        transform.Rotate(new Vector3(0,0,90));
        muzzleFlashEffect.transform.rotation = Quaternion.LookRotation(Vector3.back, new Vector3(bFunctions.direction.x, bFunctions.direction.y, 0));
        float zRotation = 90;
        
        if (playerChar.transform.localScale.x < 0)
        {
            zRotation = -90;
        }
        muzzleFlashEffect.transform.Rotate(new Vector3(0,0,zRotation));
    }
    
    private void DoDamage(GameObject target)
    {
        if (target.CompareTag("Enemy"))
        {
            if (target.GetComponent<EnemyStats>().unverwundbar)
            {
                invulnerableMiss = true;
                bFunctions.miss = true;
            }
            target.GetComponent<EnemyStats>().Hit(bFunctions.attackDamage, 0, bFunctions.guid, true);
        }

        if (target.CompareTag("Object") && !bFunctions.hit)
        {
            if (target.GetComponent<ObjektStats>().destructable)
            {
                target.GetComponent<ObjektStats>().hitPoints -= bFunctions.attackDamage;
                if (target.GetComponent<ObjektStats>().hitPoints <= 0)
                {
                    target.GetComponent<ObjektStats>().startDeath();
                    bFunctions.miss = true;
                    bFunctions.hit = true;
                }
            }
        }

        if (target.CompareTag("Ground") || target.CompareTag("Invulnerable"))
        {
            bFunctions.miss = true;
            bFunctions.hit = true;
        }
    }
}
