using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class BlutmagieFunctions : MonoBehaviour
{
    public float speed;
    public Vector2 direction;
    public int attackDamage = 100;
    public int additionaldamage = 0;
    public bool empoweredByBloodthirst, empowered7u1, hit, miss, dying, noMissSound, enhanceSkill8;
    public bool skill11Empowerment;
    public System.Guid guid;
    [FormerlySerializedAs("empoweredHitEffect5u2")] public GameObject empoweredHitEffect;
    public GameObject empoweredHitEffect7u1, normalHitEffect, empoweredHitEffectSkill11;
    private PlatformerCharacter2D playerChar;
    AudioSource missSound;

    public void InitBlutmagieFunctions()
    {
        playerChar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
        attackDamage += additionaldamage;
        if (!noMissSound)
        {
            missSound = GetComponent<AudioSource>();
        }
    }

    public void DeathFunctions(Vector2 hitEffectSpawnPosition, bool skipEmpoweredSkills7And11, bool noNormalHitEffect)
    {
        spawnHitEffects(hitEffectSpawnPosition, skipEmpoweredSkills7And11, noNormalHitEffect);
    }

    public void DeathFunctions(Vector2 hitEffectSpawnPosition)
    {
        spawnHitEffects(hitEffectSpawnPosition, false, false);
    }

    private void spawnHitEffects(Vector2 hitEffectSpawnPosition, bool skipEmpoweredSkills7And11, bool noNormalHitEffect)
    {
        dying = true;
        hit = true;
        if (skill11Empowerment && !skipEmpoweredSkills7And11)
        {
            Instantiate(empoweredHitEffectSkill11, hitEffectSpawnPosition, Quaternion.identity);
        }
        if (empowered7u1 && !skipEmpoweredSkills7And11)
        {
            Instantiate(empoweredHitEffect7u1, hitEffectSpawnPosition, Quaternion.identity);
        }
        else if (!miss && !noNormalHitEffect)
        {
            if (empoweredByBloodthirst)
            {
                Instantiate(empoweredHitEffect, hitEffectSpawnPosition, Quaternion.identity);
            }
            else
            {
                Instantiate(normalHitEffect, hitEffectSpawnPosition, Quaternion.identity);
            }
            
            if (enhanceSkill8 && playerChar.skill8choice1)
            {
                playerChar.skill8choice1Rdy = true;
            }
            else if(enhanceSkill8 && playerChar.skill8choice2)
            {
                playerChar.skill8choice2Rdy = true;
            }
        }
        else if(!noMissSound)
        {
            missSound.PlayDelayed(0);
        }
    }
    
    public IEnumerator delayAndDie(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (!hit && !miss)
        {
            miss = true;
        }
    }
}
