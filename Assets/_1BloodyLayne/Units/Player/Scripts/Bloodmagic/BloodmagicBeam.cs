using System;
using Units.Enemys;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace Units.Player.Scripts.Bloodmagic
{
    public class BloodmagicBeam : MonoBehaviour
    {
        public GameObject beamEffect, groundHitEffect, invulnerableMissEffect, spawnedSound;
        public float range, deathAnimationDuration;
        public Quaternion directionRotation;
        
        [SerializeField] private AudioClip normalSound, empoweredSound;

        private BlutmagieFunctions bFunctions;
        private Light2D light2D, beamLight;
        private float startLightIntensity, deathAnimStartTime;
        private PlatformerCharacter2D playerChar;
        private bool invulnerableMiss, groundHit;
        private LineRenderer beamLineRenderer;
        private GameObject muzzleFlash, beamInstance;
        private GameObject launchSound;

        // Use this for initialization
        void Awake()
        {
            bFunctions = GetComponent<BlutmagieFunctions>();
            light2D = transform.Find("Point Light 2D").GetComponent<Light2D>();
            startLightIntensity = light2D.intensity;
            deathAnimStartTime = Time.time;
            playerChar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            if (deathAnimationDuration == 0)
            {
                Debug.Log("deathAnimationDuration is set to 0 in BloodmagicHitScan in GameObject " + gameObject.name);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if(light2D.intensity > 0)
            {
                float newLightIntensity = startLightIntensity * (1 - ( (Time.time-deathAnimStartTime) / deathAnimationDuration));
                if (newLightIntensity < 0)
                {
                    newLightIntensity = 0;
                    Destroy(beamInstance);
                    Destroy(muzzleFlash);
                    Destroy(gameObject);
                }
                light2D.intensity = newLightIntensity;
                if (beamLight != null)
                {
                    beamLight.intensity = newLightIntensity;
                }
            }

            if (beamLineRenderer != null)
            {
                float newAlpha = 1 - ((Time.time - deathAnimStartTime) / deathAnimationDuration);
                Gradient newColorGradiant = beamLineRenderer.colorGradient;
                newColorGradiant.SetKeys(
                    beamLineRenderer.colorGradient.colorKeys,
                    new GradientAlphaKey[]
                    {
                        new GradientAlphaKey(newColorGradiant.alphaKeys[0].alpha, 0.0f),
                        new GradientAlphaKey(newAlpha, 0.05f),
                        new GradientAlphaKey(newAlpha, 0.95f),
                        new GradientAlphaKey(newColorGradiant.alphaKeys[3].alpha, 1.0f)
                    }
                    );
                beamLineRenderer.colorGradient = newColorGradiant;
            }
        }
    
        public void Launch(GameObject muzzleFlashEffect)
        {
            GameObject spawnedSoundInstance = Instantiate(spawnedSound, transform.position, Quaternion.identity);
            if (bFunctions.empoweredByBloodthirst)
            {
                spawnedSoundInstance.GetComponent<SpawnedSound>().PlaySound(normalSound);
            }
            spawnedSoundInstance.GetComponent<SpawnedSound>().PlaySound(empoweredSound);
            
            muzzleFlash = muzzleFlashEffect;
            SetMuzzleRotation(muzzleFlashEffect);
        
            RaycastHit2D hit = Physics2D.Raycast(transform.position, bFunctions.direction, range, 1<<9 | 1<<11 | 1<<14 | 1<<15);
            Vector3 endPoint; 
            if (hit.collider != null)
            {
                endPoint = hit.point;
                DoDamage(hit.collider.gameObject);
            }
            else
            {
                bFunctions.miss = true;
                endPoint = transform.position + (Vector3)bFunctions.direction.normalized * range;
            }

            beamInstance = Instantiate(beamEffect, transform.position, directionRotation);
            SetBeamEffect(beamInstance, endPoint);

            bFunctions.DeathFunctions(endPoint);
        
            if (bFunctions.miss)
            {
                if (invulnerableMiss)
                {
                    Instantiate(invulnerableMissEffect, endPoint, transform.rotation);
                }
                else if(groundHit)
                {
                    Instantiate(groundHitEffect, endPoint, transform.rotation);
                }
            }
        }

        private void SetBeamEffect(GameObject beam, Vector3 endpoint)
        {
            float distance = Vector2.Distance(transform.position, endpoint);
            beam.transform.localScale = new Vector3(1, distance/10, 1);
            beam.transform.Find("Particle System").localScale = new Vector3(distance/10, 1, 1);
            beamLineRenderer = beam.GetComponentInChildren<LineRenderer>();
            beamLight = beam.GetComponentInChildren<Light2D>();
        }

        private void SetMuzzleRotation(GameObject muzzleFlashEffect)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.back, new Vector3(bFunctions.direction.x, bFunctions.direction.y, 0));
            transform.Rotate(new Vector3(0,0,90));
            muzzleFlashEffect.transform.rotation = Quaternion.LookRotation(Vector3.back, new Vector3(bFunctions.direction.x, bFunctions.direction.y, 0));
            float zRotation = 90;
        
            if (playerChar.transform.localScale.x < 0)
            {
                zRotation = -90;
            }
            muzzleFlashEffect.transform.Rotate(new Vector3(0,0,zRotation));
        }
    
        private void DoDamage(GameObject target)
        {
            if (target.CompareTag("Enemy"))
            {
                if (target.GetComponent<EnemyStats>().unverwundbar)
                {
                    invulnerableMiss = true;
                    bFunctions.miss = true;
                }
                target.GetComponent<EnemyStats>().Hit(bFunctions.attackDamage, DamageTypeToEnemy.Explosion, bFunctions.guid, true);
            }

            if (target.CompareTag("Object") && !bFunctions.hit)
            {
                if (target.GetComponent<ObjektStats>().destructable)
                {
                    target.GetComponent<ObjektStats>().hitPoints -= bFunctions.attackDamage;
                    if (target.GetComponent<ObjektStats>().hitPoints <= 0)
                    {
                        target.GetComponent<ObjektStats>().startDeath();
                        bFunctions.miss = true;
                        bFunctions.hit = true;
                    }
                }
            }

            if (target.CompareTag("Ground") || target.CompareTag("Invulnerable"))
            {
                groundHit = true;
                bFunctions.miss = true;
                bFunctions.hit = true;
            }
        }
    }
}
