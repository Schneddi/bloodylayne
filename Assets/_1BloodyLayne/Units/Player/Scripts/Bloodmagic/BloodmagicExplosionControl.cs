using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityStandardAssets._2D;

public class BloodmagicExplosionControl : MonoBehaviour
{
    public AudioClip normalExplosionSound, empoweredExplosionSound;
    public float deathTime;

    private BlutmagieFunctions bFunctions;
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        bFunctions = GetComponent<BlutmagieFunctions>();
        audioSource = GetComponent<AudioSource>();
        if (!bFunctions.empoweredByBloodthirst)
        {
            transform.Find("NormalExplosionEffect").gameObject.SetActive(true);
            audioSource.clip = normalExplosionSound;
        }
        else if (bFunctions.empoweredByBloodthirst)
        {
            transform.Find("EmpoweredExplosionEffect").gameObject.SetActive(true);
            audioSource.clip = empoweredExplosionSound;
        }
        audioSource.PlayDelayed(0);
        StartCoroutine(DealDamageAndDestroy());
    }
    
    private IEnumerator DealDamageAndDestroy()
    {
        bFunctions.DeathFunctions(transform.position, false, true);
        yield return new WaitForSeconds(0.05f);
        damageEnemys();
        yield return new WaitForSeconds(deathTime);
        Destroy(gameObject);
    }
    
    
    private void damageEnemys()
    {
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 2.2f, 1 << 9 | 1<< 14);
        {
            foreach (Collider2D col in cols)
            {
                if (col.gameObject.GetComponent<EnemyStats>() != null)
                {
                    col.gameObject.GetComponent<EnemyStats>().Hit(bFunctions.attackDamage, DamageTypeToEnemy.Explosion, bFunctions.guid, true);
                    bFunctions.DeathFunctions(col.transform.position, true, false);
                }
                else if (col.gameObject.CompareTag("Object"))
                {
                    if (col.gameObject.GetComponent<ObjektStats>() != null)
                    {
                        if (col.gameObject.GetComponent<ObjektStats>().destructable)
                        {
                            col.gameObject.GetComponent<ObjektStats>().hitPoints -= bFunctions.attackDamage;
                            if (col.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                            {
                                col.gameObject.GetComponent<ObjektStats>().startDeath();
                                bFunctions.miss = true;
                            }
                        }
                    }
                }
            }
        }
    }
}
