using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(BlutmagieFunctions))]
    public class BloodmagicBiteControl : MonoBehaviour
    {
        public GameObject biteHitEffect, missSound;
        
        private BlutmagieFunctions bFunctions;
        private SpriteRenderer sRenderer;
        private Rigidbody2D rb;
        private bool deathAnimation;
        private Light2D light2D;
        private float deathAnimationDuration = 0.2f, startLightIntensity, deathAnimStartTime;

        // Use this for initialization
        void Start()
        {
            bFunctions = GetComponent<BlutmagieFunctions>();
            rb = GetComponent<Rigidbody2D>();
            sRenderer = GetComponent<SpriteRenderer>();
            light2D = transform.Find("Point Light 2D").GetComponent<Light2D>();
            startLightIntensity = light2D.intensity;
            rb.velocity = bFunctions.speed * bFunctions.direction;
            if (rb.velocity.x < 0)
            {
                sRenderer.flipX = false;
            }
            StartCoroutine(bFunctions.delayAndDie(0.65f));
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (!bFunctions.hit)
            {
                rb.velocity = bFunctions.speed * bFunctions.direction;
            }
        }
        private void Update()
        {
            if (!bFunctions.hit)
            {
                checkInvulnerable();
                damageEnemys();
                damageObjects();
                if (Physics2D.OverlapCircle(transform.position, 0.3f, 1 << 11) && !bFunctions.hit)
                {
                    bFunctions.miss = true;
                }
            }
            if (bFunctions.miss && !bFunctions.dying)
            {
                StartCoroutine(Death());
            }
            if(deathAnimation && light2D.intensity > 0)
            {
                sRenderer.color -= new Color(0,0,0, Time.deltaTime / deathAnimationDuration);
                transform.localScale += new Vector3(Time.deltaTime, Time.deltaTime, Time.deltaTime);
                light2D.intensity = startLightIntensity * (1 - ( (Time.time-deathAnimStartTime) / deathAnimationDuration));
            }
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Enemy") && !bFunctions.hit)
            {
                hitEnemy(other.gameObject);
            }

            if (other.gameObject.CompareTag("Object") && !bFunctions.hit)
            {
                StartCoroutine(Death());
            }

            if (other.gameObject.CompareTag("Ground") || other.gameObject.CompareTag("Invulnerable") && !bFunctions.hit)
            {
                bFunctions.miss = true;
                bFunctions.hit = true;
                StartCoroutine(Death());
            }
        }

        IEnumerator Death()
        {
            Instantiate(missSound, transform.position, Quaternion.identity);
            bFunctions.DeathFunctions(transform.position);
            rb.velocity = new Vector2(0, 0);
            rb.isKinematic = true;
            GetComponent<CircleCollider2D>().enabled = false;
            deathAnimStartTime = Time.time;
            deathAnimation = true;
            yield return new WaitForSeconds(deathAnimationDuration);
            Destroy(gameObject);
        }

        void checkInvulnerable()
        {
            if (Physics2D.OverlapCircle(transform.position, 0.3f, 1 << 15))
            {
                bFunctions.miss = true;
                StartCoroutine(Death());
            }
        }

        void damageEnemys()
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.3f, 1 << 9);
            {
                foreach (Collider2D col in cols)
                {
                    if ((col.gameObject.GetComponent("EnemyStats") as EnemyStats) != null)
                    {
                        hitEnemy(col.gameObject);
                    }
                }
            }
        }


        void damageObjects()
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.3f, 1 << 14);
            {
                foreach (Collider2D col in cols)
                {
                    if (col.gameObject.GetComponent<ObjektStats>() != null)
                    {
                        StartCoroutine(Death());
                    }
                }
            }
        }
        
        
        private void hitEnemy(GameObject enemy)
        {
            if (enemy.GetComponent<EnemyStats>().unverwundbar || enemy.GetComponent<EnemyStats>().unbiteable)
            {
                bFunctions.miss = true;
            }
            else
            {
                GameObject biteInstance = biteHitEffect;
                biteInstance.GetComponent<BloodmagicBiteEffect>().hitEnemy = enemy.GetComponent<EnemyStats>();
                biteInstance.GetComponent<BloodmagicBiteEffect>().damage = bFunctions.attackDamage;
                biteInstance.GetComponent<BloodmagicBiteEffect>().empowered7u1 = bFunctions.empowered7u1;
                biteInstance.GetComponent<BloodmagicBiteEffect>().skill11Empowerment = bFunctions.skill11Empowerment;
                biteInstance.GetComponent<BloodmagicBiteEffect>().guid = bFunctions.guid;
                Instantiate(biteInstance, enemy.transform.position, Quaternion.identity);
            }
            StartCoroutine(Death());
        }
    }
}
