using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(BlutmagieFunctions))]
    public class BloodmagicSkill13 : MonoBehaviour
    {
        private BlutmagieFunctions bFunctions;
        private Animator pAnimator;
        private bool deathAnimation;
        private Light2D light2D;
        private float deathAnimationDuration = 0.5f, startLightIntensity, deathAnimStartTime;
        private List<GameObject> hitEnemies;

        // Use this for initialization
        void Start()
        {
            hitEnemies = new List<GameObject>();
            bFunctions = GetComponent<BlutmagieFunctions>();
            pAnimator = GetComponent<Animator>();
            light2D = transform.Find("Point Light 2D").GetComponent<Light2D>();
            startLightIntensity = light2D.intensity;
        }
        
        private void Update()
        {
            if (!bFunctions.hit)
            {
                checkInvulnerable();
                damageEnemys();
                damageObjects();
                if (Physics2D.OverlapCircle(transform.position, 0.3f, 1 << 11) && !bFunctions.hit)
                {
                    bFunctions.miss = true;
                }
            }
            if (bFunctions.miss && !bFunctions.dying)
            {
                StartCoroutine(Death());
            }
            if(deathAnimation && light2D.intensity > 0)
            {
                light2D.intensity = startLightIntensity * (1 - ( (Time.time-deathAnimStartTime) / deathAnimationDuration));
            }
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Enemy") && !bFunctions.hit)
            {
                hitEnemy(other.gameObject);
            }

            if (other.gameObject.CompareTag("Object") && !bFunctions.hit)
            {
                hitObject(other.gameObject);
            }

            if (other.gameObject.CompareTag("Ground") || other.gameObject.CompareTag("Invulnerable") && !bFunctions.hit)
            {
                bFunctions.miss = true;
                bFunctions.hit = true;
                StartCoroutine(Death());
            }
        }

        IEnumerator Death()
        {
            pAnimator.SetTrigger("Hit");
            bFunctions.DeathFunctions(transform.position);
            GetComponent<CircleCollider2D>().enabled = false;
            deathAnimation = true;
            transform.Find("MainParticleSystem").GetComponent<ParticleSystem>().Stop();
            transform.Find("TrailParticleSystem").GetComponent<ParticleSystem>().Stop();
            deathAnimStartTime = Time.time;
            deathAnimation = true;
            yield return new WaitForSeconds(deathAnimationDuration);
            Destroy(gameObject);
        }

        void checkInvulnerable()
        {
            if (Physics2D.OverlapCircle(transform.position, 0.3f, 1 << 15))
            {
                bFunctions.miss = true;
                StartCoroutine(Death());
            }
        }

        void damageEnemys()
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.3f, 1 << 9);
            {
                foreach (Collider2D col in cols)
                {
                    if ((col.gameObject.GetComponent("EnemyStats") as EnemyStats) != null)
                    {
                        hitEnemy(col.gameObject);
                    }
                }
            }
        }


        void damageObjects()
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.3f, 1 << 14);
            {
                foreach (Collider2D col in cols)
                {
                    if (col.gameObject.GetComponent<ObjektStats>() != null)
                    {
                        hitObject(col.gameObject);
                    }
                }
            }
        }
        
        private void hitObject(GameObject hitObject)
        {
            
            if (hitObject.CompareTag("Object") && !bFunctions.hit)
            {
                if (hitObject.GetComponent<ObjektStats>().destructable)
                {
                    hitObject.GetComponent<ObjektStats>().hitPoints -= bFunctions.attackDamage;
                    if (hitObject.GetComponent<ObjektStats>().hitPoints <= 0)
                    {
                        hitObject.GetComponent<ObjektStats>().startDeath();
                        bFunctions.miss = true;
                    }
                }
                StartCoroutine(Death());
            }
        }
        
        private void hitEnemy(GameObject enemy)
        {
            if (!hitEnemies.Contains(enemy))
            {
                if (enemy.GetComponent<EnemyStats>().unverwundbar)
                {
                    bFunctions.miss = true;
                }
                enemy.GetComponent<EnemyStats>().Hit(bFunctions.attackDamage, DamageTypeToEnemy.Skill13Dash, bFunctions.guid, true);
                if (enemy.GetComponent<EnemyStats>().health >= 0)
                {
                    StartCoroutine(Death());
                }
                else
                {
                    hitEnemies.Add(enemy);
                }
            }
        }

        public void die()
        {
            bFunctions.miss = true;
            StartCoroutine(Death());
        }
    }
}
