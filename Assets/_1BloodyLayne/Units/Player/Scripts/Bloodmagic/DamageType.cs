
 public enum DamageTypeToPlayer
 {
  Normal = 0,
  WerewolfBite = 1,
  Smashed = 2,
  ImpaledFeet = 3, //impaled by spikes on ground
  Burned = 4,
  DragonBurn = 5,
  AssassinStab = 6,
  KilledByUser = 7,
  Bullet = 8,
  BluntProjectile = 9,
  Arrow = 10,
  MeleeSwing = 11,
  MagicProjectile = 12,
  KnifeThrow = 13,
  Explosion = 14,
  MeleeStab = 15,
  SonicWave = 16,
  GhostTouch = 17,
  ClappingHands = 18,
  GodNailImpale = 19,
  Electrocuted = 20,
  MagicShield = 21,
  RavenStab = 22,
  SkullBite = 23,
  MeleeFeetStab = 24,
 }

public enum DamageTypeToEnemy
{
 Bloodmagic = 0,
 Bite = 1,
 Fall = 2,
 Fire = 3,
 Explosion = 4,
 Fart = 5,
 Impaled = 6, //impaled by spikes on ground
 MagicShield = 7,
 Skill13Dash = 8,
 Skill13Push = 9,
 Smashed = 10
}
