using System;
using System.Collections;
using Units.Enemys;
using UnityEditor;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class BloodmagicBiteEffect : MonoBehaviour
{
    public int damage;
    public EnemyStats hitEnemy;
    public bool empowered7u1, skill11Empowerment;
    public GameObject skill7u1BiteEffect, skill11EmpowermentBiteEffect;
    public System.Guid guid;
    
    [SerializeField] private GameObject _particleHeal;
    
    private PlatformerCharacter2D layne;
    private Animator animator;
    private AudioSource biteSound, biteSoundEmpowered;
    // Start is called before the first frame update
    void Start()
    {
        layne = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
        biteSound = GetComponents<AudioSource>()[0];
        biteSoundEmpowered = GetComponents<AudioSource>()[1];
        animator = GetComponent<Animator>();
        StartCoroutine(bitingEffects());
    }

    private void Update()
    {
        if (hitEnemy == null) return;
        transform.position = hitEnemy.transform.position;
    }

    private IEnumerator bitingEffects()
    {
        layne.BiteEffects();
        for (int i=0; i<5; i++)
        {
            if (hitEnemy.dead)
            {
                continue;
            }

            if (layne.skill6choice2)
            {
                biteSound.PlayOneShot(biteSound.clip);
            }
            else
            {
                biteSound.PlayOneShot(biteSoundEmpowered.clip);
            }
            animator.SetTrigger("Close");
            yield return new WaitForSeconds(0.1f);
            if (empowered7u1)
            {
                Instantiate(skill7u1BiteEffect, hitEnemy.transform.position, Quaternion.identity);
            }
            if (skill11Empowerment)
            {
                Instantiate(skill11EmpowermentBiteEffect, hitEnemy.transform.position, Quaternion.identity);
            }
            layne.BiteHit(hitEnemy, damage, guid, false);
            animator.SetTrigger("Open");
            yield return new WaitForSeconds(0.1f);
        }

        if (!hitEnemy.dead && hitEnemy.isBoss)
        {
            Instantiate(_particleHeal, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }
}
