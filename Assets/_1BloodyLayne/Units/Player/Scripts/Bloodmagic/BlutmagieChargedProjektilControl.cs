﻿using System;
using System.Collections;
using System.Collections.Generic;
using UI.PlayerUI;
using Units.Enemys;
using UnityEngine;


namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(BlutmagieFunctions))]
    public class BlutmagieChargedProjektilControl : MonoBehaviour
    {
        Rigidbody2D rb;
        CircleCollider2D circleCollider;
        Animator pAnimator;
        public bool charging = true;
        private UnityEngine.Rendering.Universal.Light2D lightningSource;
        private BlutmagieFunctions bFunctions;
        GameObject hitExplosionEffectChild;
        public GameObject hitExplosionEffect;
        private Camera2DFollow camerascript;
        private float startTime;

        // Use this for initialization
        void Start()
        {
            bFunctions = GetComponent<BlutmagieFunctions>();
            rb = GetComponent<Rigidbody2D>();
            circleCollider = GetComponent<CircleCollider2D>();
            pAnimator = GetComponent<Animator>();
            lightningSource = GetComponentInChildren<UnityEngine.Rendering.Universal.Light2D>();
            camerascript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
            startTime = Time.time;
        }

        private void Update()
        {
            if (charging)
            {
                transform.position = transform.parent.position;
                float timeAdjust = Time.deltaTime * 35;
                transform.localScale += new Vector3(timeAdjust, timeAdjust, 0);
                circleCollider.radius *= 1 - 0.5f * Time.deltaTime;
                lightningSource.pointLightOuterRadius += Time.deltaTime * 4;
                lightningSource.pointLightInnerRadius += Time.deltaTime * 4;
            }
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (!bFunctions.hit && !charging)
            {
                rb.velocity = bFunctions.speed * bFunctions.direction;
            }

            if (bFunctions.miss && !bFunctions.dying)
            {
                StartCoroutine(Death());
            }
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Enemy") && !bFunctions.hit)
            {
                charging = false;
                bFunctions.hit = true;
                other.gameObject.GetComponent<EnemyStats>().Hit(bFunctions.attackDamage/2, DamageTypeToEnemy.Explosion, bFunctions.guid, true);
                if (other.gameObject.GetComponent<EnemyStats>().unverwundbar)
                {
                    bFunctions.miss = true;
                }
                StartCoroutine(Death());
            }

            if (other.gameObject.CompareTag("Object") && !bFunctions.hit)
            {
                charging = false;
                bFunctions.hit = true;
                if (other.gameObject.GetComponent<ObjektStats>().destructable)
                {
                    other.gameObject.GetComponent<ObjektStats>().hitPoints -= bFunctions.attackDamage;
                    if (other.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                    {
                        other.gameObject.GetComponent<ObjektStats>().startDeath();
                        charging = false;
                        bFunctions.miss = true;
                        bFunctions.hit = true;
                    }
                }
                StartCoroutine(Death());
            }

            if (other.gameObject.CompareTag("Ground") || other.gameObject.CompareTag("Invulnerable") && !bFunctions.hit)
            {
                charging = false;
                bFunctions.miss = true;
                bFunctions.hit = true;
                StartCoroutine(Death());
            }
        }

        public void release()
        {
            if (!bFunctions.hit)
            {
                transform.SetParent(null);
                charging = false;
                rb.velocity = bFunctions.speed * bFunctions.direction;
                StartCoroutine(bFunctions.delayAndDie(2.5f));
            }
        }

        IEnumerator Death()
        {
            transform.SetParent(null);
            bFunctions.attackDamage += (int)((Time.time - startTime) * 200);
            camerascript.ShakeCamera(0.5f,1f, 60);
            bFunctions.hit = true;
            hitExplosionEffectChild = Instantiate(hitExplosionEffect, transform.position, Quaternion.identity);
            hitExplosionEffectChild.transform.localScale = new Vector3(transform.localScale.x/10, transform.localScale.y/10, transform.localScale.z/10);
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 15f, 1 << 9 |  1<<14);
            foreach (Collider2D col in cols)
            {
                if ((col.gameObject.GetComponent("EnemyStats") as EnemyStats) != null)
                {
                    if (Vector2.Distance(transform.position, col.transform.position) < 6)
                    {
                        col.gameObject.GetComponent<EnemyStats>().Hit((int)(bFunctions.attackDamage / 1.5f), DamageTypeToEnemy.Explosion, bFunctions.guid, true);
                    }
                    else
                    {
                        Vector3 direction = col.transform.position - transform.position;
                        RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, direction);
                        foreach (RaycastHit2D hitter in hit)
                        {
                            if (hitter.transform.tag == "Ground")
                            {
                                break;
                            }
                            if (hitter.transform.tag == "Enemy")
                            {
                                col.gameObject.GetComponent<EnemyStats>().Hit(bFunctions.attackDamage / 2, DamageTypeToEnemy.Explosion, bFunctions.guid, true);
                            }
                        }
                    }
                }
                else if ((col.gameObject.GetComponent("ObjektStats") as ObjektStats) != null)
                {   
                    Vector3 direction = col.transform.position - transform.position;
                    RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, direction);
                    foreach (RaycastHit2D hitter in hit)
                    {
                        if (hitter.transform.tag == "Ground")
                        {
                            break;
                        }
                        if (hitter.transform.tag == "Object")
                        {
                            if (col.gameObject.GetComponent<ObjektStats>().destructable)
                            {
                                col.gameObject.GetComponent<ObjektStats>().hitPoints -= bFunctions.attackDamage / 2;
                                if (col.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                                {
                                    col.gameObject.GetComponent<ObjektStats>().startDeath();
                                    bFunctions.miss = true;
                                }
                            }
                        }
                    }
                }
            }
            rb.velocity = new Vector2(0, 0);
            pAnimator.SetTrigger("Hit");
            rb.isKinematic = true;
            GetComponent<CircleCollider2D>().enabled = false;
            bFunctions.DeathFunctions(transform.position);
            yield return new WaitForSeconds(0.7f);
            Destroy(gameObject);
        }
    }
}
