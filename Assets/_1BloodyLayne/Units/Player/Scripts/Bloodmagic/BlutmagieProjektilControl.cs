﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(BlutmagieFunctions))]
    public class BlutmagieProjektilControl : MonoBehaviour
    {
        Rigidbody2D rb;
        int minDamage = 30;
        Animator pAnimator;
        private BlutmagieFunctions bFunctions;
        public float totalLiveTime;
        private float initTime, initScale;
        private Light2D light2D;
        private float deathAnimationDuration = 0.7f, startLightIntensity, deathAnimStartTime;

        // Use this for initialization
        void Start()
        {
            bFunctions = GetComponent<BlutmagieFunctions>();
            rb = GetComponent<Rigidbody2D>();
            pAnimator = GetComponent<Animator>();
            rb.velocity = bFunctions.speed * bFunctions.direction;
            initTime = Time.time;
            initScale = transform.localScale.x;
            light2D = transform.Find("Point Light 2D").GetComponent<Light2D>();
            startLightIntensity = light2D.intensity;
            StartCoroutine(bFunctions.delayAndDie(totalLiveTime));
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (!bFunctions.hit)
            {
                rb.velocity = bFunctions.speed * bFunctions.direction;
            }
        }
        private void Update()
        {
            float newScale = initScale * (totalLiveTime - (Time.time - initTime)*0.6f)/totalLiveTime;
            if (newScale < transform.localScale.x)
            {
                transform.localScale = new Vector3(newScale,newScale,0);
            }
            if (!bFunctions.hit)
            {
                checkInvulnerable();
                damageEnemys();
                damageObjects();
                if (Physics2D.OverlapCircle(transform.position, 0.3f, 1 << 11))
                {
                    bFunctions.miss = true;
                }
            }
            if (bFunctions.miss && !bFunctions.dying)
            {
                StartCoroutine(Death());
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Enemy") && !bFunctions.hit)
            {
                other.gameObject.GetComponent<EnemyStats>().Hit(calculateDamage(), 0, bFunctions.guid, true);
                if (other.gameObject.GetComponent<EnemyStats>().unverwundbar)
                {
                    bFunctions.miss = true;
                    bFunctions.hit = true;
                }
                StartCoroutine(Death());
            }
            if (other.gameObject.CompareTag("Object") && !bFunctions.hit)
            {
                if (other.gameObject.GetComponent<ObjektStats>().destructable)
                {
                    other.gameObject.GetComponent<ObjektStats>().hitPoints -= calculateDamage();
                    if(other.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                    {
                        other.gameObject.GetComponent<ObjektStats>().startDeath();
                    }
                }
                StartCoroutine(Death());
            }
            if (other.gameObject.CompareTag("Ground") || other.gameObject.CompareTag("Invulnerable") && !bFunctions.hit)
            {
                bFunctions.miss = true;
                bFunctions.hit = true;
                StartCoroutine(Death());
            }
        }

        IEnumerator Death()
        {
            bFunctions.DeathFunctions(transform.position);
            rb.velocity = new Vector2(0, 0);
            pAnimator.SetTrigger("Hit");
            rb.isKinematic = true;
            GetComponent<CircleCollider2D>().enabled = false;
            deathAnimStartTime = Time.time;
            yield return new WaitForSeconds(deathAnimationDuration);
            Destroy(gameObject);
        }

        void checkInvulnerable()
        {
            if (Physics2D.OverlapCircle(transform.position, 0.3f, 1 << 15))
            {
                bFunctions.miss = true;
                StartCoroutine(Death());
            }
        }

        void damageEnemys()
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.2f, 1 << 9);
            {
                foreach (Collider2D col in cols)
                {
                    if ((col.gameObject.GetComponent("EnemyStats") as EnemyStats) != null)
                    {
                        if (col.gameObject.GetComponent<EnemyStats>().unverwundbar)
                        {
                            bFunctions.miss = true;
                        }
                        col.gameObject.GetComponent<EnemyStats>().Hit(calculateDamage(), 0, bFunctions.guid, true);
                        StartCoroutine(Death());
                    }
                }
            }
        }


        void damageObjects()
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.2f, 1 << 14);
            {
                foreach (Collider2D col in cols)
                {
                    if ((col.gameObject.GetComponent("ObjektStats") as ObjektStats) != null)
                    {
                        if (col.gameObject.GetComponent<ObjektStats>().destructable)
                        {
                            col.gameObject.GetComponent<ObjektStats>().hitPoints -= calculateDamage();
                            if (col.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                            {
                                col.gameObject.GetComponent<ObjektStats>().startDeath();
                                bFunctions.miss = true;
                                bFunctions.hit = true;
                            }
                            StartCoroutine(Death());
                        }
                    }
                }
            }
        }

        private int calculateDamage()
        {
            int calculatedDamage = (int)((float)bFunctions.attackDamage * (totalLiveTime - (Time.time - initTime)*1.1f)/totalLiveTime);
            if (calculatedDamage < minDamage)
            {
                calculatedDamage = minDamage;
            }

            return calculatedDamage;
        }
    }
}