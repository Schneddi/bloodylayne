using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering.Universal;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class PlayerAimVisualisation : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private Platformer2DUserControl userControl;
    private PlatformerCharacter2D player;
    private GlobalVariables gv;
    private Transform lineSpawn, bone1;
    
    public Transform currentTarget;
    public bool aimingLocked, scanForTarget, usingMouseAim;
    
    private SpriteRenderer cursorRenderer, cursorBloomRenderer;
    private Light2D cursorLight;

    void Awake()
    {
        lineSpawn = transform.Find("Side Animation/bone_1/BulletSpawn");
        bone1 = transform.Find("Side Animation/bone_1");
        lineRenderer = lineSpawn.GetComponent<LineRenderer>();
        userControl = GetComponent<Platformer2DUserControl>();
        player = GetComponent<PlatformerCharacter2D>();
        gv = GameObject.Find("GlobalVariables").GetComponent<GlobalVariables>();
        //Vector2 hotSpot = new Vector2(cursorTexture.width/2, cursorTexture.height/2);
        //Cursor.SetCursor(cursorTexture, hotSpot, CursorMode.ForceSoftware);
        aimingLocked = true;
        cursorRenderer = GameObject.Find("Crosshair").GetComponent<SpriteRenderer>();
        cursorRenderer.enabled = false;
        cursorBloomRenderer = cursorRenderer.transform.Find("CrosshairBloom").GetComponent<SpriteRenderer>();
        cursorBloomRenderer.enabled = false;
        cursorLight = cursorRenderer.GetComponentInChildren<Light2D>();
    }

    void Update()
    {
        if (scanForTarget)
        {
            Vector2 direction;
            if (!aimingLocked)
            {
                direction = bone1.rotation * Vector2.down;
            }
            else
            {
                direction = lineSpawn.transform.position - currentTarget.transform.position;
            }
            RaycastHit2D hit = Physics2D.Raycast(lineSpawn.transform.position, direction, 20, 1<<9 | 1<<11 | 1<<14);
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.layer == 9 && !aimingLocked)
                {
                    registerTarget(hit.collider.transform);
                }
                if (hit.collider.gameObject.layer == 11 || hit.collider.gameObject.layer == 14 && aimingLocked)
                {
                    unregisterTarget();
                }
            }
        }
    }

    public void startScanning()
    {
        unregisterTarget();
        if (gv.aimAssist)
        {
            scanForTarget = true;
        }
    }

    public void stopScanning()
    {
        unregisterTarget();
        scanForTarget = false;
    }
    

    private void registerTarget(Transform target)
    {
        if (player.aimingStopped)
        {
            player.noAimingLock = true;
        }
        userControl.useLockedTarget = true;
        aimingLocked = true;
        currentTarget = target;
    }
    
    private void unregisterTarget()
    {
        userControl.useLockedTarget = false;
        aimingLocked = false;
        currentTarget = null;
    }

    public void showLineRenderer()
    {
        if (gv.aimVisualization)
        {
            lineRenderer.enabled = true;
        }
    }
    
    public void hideLineRenderer()
    {
        lineRenderer.enabled = false;
    }

    public void showCursorRenderer()
    {
        if (usingMouseAim)
        {
            cursorRenderer.enabled = true;
            cursorBloomRenderer.enabled = true;
            cursorLight.enabled = true;
        }
    }
    
    public void hideCursorRenderer()
    {
        cursorRenderer.enabled = false;
        cursorBloomRenderer.enabled = false;
        cursorLight.enabled = false;
    }
}
