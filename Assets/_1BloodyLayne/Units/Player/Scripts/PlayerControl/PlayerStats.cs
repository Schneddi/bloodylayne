using UnityEngine;

namespace Units.Player.Scripts.PlayerControl
{
    public class PlayerStats : MonoBehaviour
    {
        public int health = 100, maxHealth = 100, lifes = 3, biteDamage = 10;
        public float maxBloodlust = 35, bloodLossByRecievingDamage = -5, bloodLossByDealingDamage = -5, biteBloodThirstHeal = 25;
        public bool human;
        [SerializeField] public float m_MaxSpeed = 14f, maxJumpSpeed = 31f;                    // The fastest the player can travel in the x axis.
        [SerializeField] public float m_JumpForce = 21.75f;                  // Amount of force added when the player jumps.
        [Range(0, 1)] [SerializeField] public float m_CrouchSpeed = 0.8f, climbSpeed = 0.9f, flySpeed = 0.8f;  // Amount of maxSpeed applied to crouching movement. 1 = 100%
    }
}
