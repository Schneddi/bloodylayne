using System;
using System.Collections;
using System.Linq;
using Units.Enemys;
using Units.Player.Scripts.Bloodmagic;
using UnityEngine;
using UnityStandardAssets._2D;
using Object = UnityEngine.Object;

namespace Units.Player.Scripts.PlayerControl.PlatformCharacter2D
{
    public partial class PlatformerCharacter2D : MonoBehaviour
    {
        [SerializeField] private float dashSpeed, dashCooldown, bloodmagicPushback, rangedBiteCooldown;
        private Coroutine _airDashCooldownCoroutine, _groundDashCooldownCoroutine, _rangedBiteCooldownCoroutine;
        private int _dashCounter;
        
        //type 0 = standard attack, type 1 = charged
        IEnumerator Fire(int type)
        {
            System.Guid guid = System.Guid.NewGuid();
            aimTimer = 0;
            ApplyBloodmagicSelfDamage();
            StartNewHealthCooldown(0, 0);
            string animationName = null;
            firingBloodmagic = true;
            userControl.bloodmagicBlockInput = true;

            bool[] damageInfos = GetExtraDamageInfo();
            ClearBloodmagicEmpowerments();
            
            GameObject chargeEffect = bloodmagicCharging;
            Transform parent = firePos.transform;
            BloodmagicType bloodmagicType = BloodmagicType.NormalFireball;
            if (type == 0)
            {
                playerAimVisualisation.startScanning();
                if (skill2choice1Rdy)
                {
                    bloodmagicType = BloodmagicType.Explosion;
                    animationName = "BloodmagicExplosion";
                    m_Anim.SetBool(animationName, true);
                    bloodmagicExplosionChargeSound.PlayDelayed(0);
                    parent = sideHandAttachmentRight;
                }
                else if (skill2choice2Rdy)
                {
                    bloodmagicType = BloodmagicType.Beam;
                    animationName = "BloodmagicBeam";
                    m_Anim.SetBool(animationName, true);
                    bloodmagicBeamChargeSound.PlayDelayed(0);
                }
                else if (skill8choice1Rdy)
                {
                    bloodmagicType = BloodmagicType.Boomerang;
                    animationName = "BlutmagieBoomerang";
                    m_Anim.SetBool(animationName, true);
                    bloodmagicBoomerangSound.PlayDelayed(0);
                    chargeEffect = boomerangChargeEffect;
                    parent = sideHandAttachmentRight;
                }
                else if (skill8choice2Rdy)
                {
                    bloodmagicType = BloodmagicType.RangedVolley;
                    animationName = "BlutmagieFar";
                    m_Anim.SetBool(animationName, true);
                    bloodmagicFarSound.PlayDelayed(0);
                }
                else
                {
                    if (skill0choice1)
                    {
                        bloodmagicType = BloodmagicType.NormalFireball;
                        animationName = "Blutmagie";
                        m_Anim.SetBool(animationName, true);
                        if (!(bloodlust <= 0))
                        {
                            blutmagieCharge.PlayDelayed(0);
                        }
                        else
                        {
                            blutmagieChargeEmpowered.PlayDelayed(0);
                        }
                    }
                    else if (skill0choice2)
                    {
                        bloodmagicType = BloodmagicType.HitScan;
                        animationName = "BlutmagieRaycast";
                        m_Anim.SetBool(animationName, true);
                        chargeEffect = null;
                    }
                }
            }

            if (type == 2)
            {
                playerAimVisualisation.startScanning();
                bloodmagicType = BloodmagicType.RangedBite;
                animationName = "BlutmagieBite";
                m_Anim.SetBool(animationName, true);
                bloodmagicBiteSound.PlayOneShot(bloodmagicBiteSound.clip);
            }

            GameObject bloodmagicChargeEffect = null;
            if (chargeEffect != null)
            {
                bloodmagicChargeEffect = Instantiate(chargeEffect, firePos.position, Quaternion.identity);
                bloodmagicChargeEffect.transform.parent = parent;
            }

            if (type != 1) yield return new WaitForSeconds(0.05f);
            
            if (type == 1)
            {
                animationName = "BlutmagieCharging";
                m_Anim.SetBool(animationName, true);
                yield return new WaitForSeconds(0.25f);
                blutmagieChargedSound.PlayDelayed(0);
                StartCoroutine((IEnumerator)MaxChargingCounter());
                GameObject bloodmagicProjectile = Instantiate(bulletCharging, chargedBloodmagicFirePos.position, Quaternion.identity);
                bloodmagicProjectile.transform.SetParent(chargedBloodmagicFirePos);
                bloodmagicProjectile.transform.localScale = Vector3.one;
                InitializeBloodmagicSkills(bloodmagicProjectile, damageInfos[0], damageInfos[1], guid);
                yield return new WaitForSeconds(0.35f);
                while (firingChargedBloodmagic && bloodmagicProjectile != null)
                {
                    if (bloodmagicProjectile.GetComponent<BlutmagieChargedProjektilControl>().charging)
                    {
                        yield return null;
                    }
                    else
                    {
                        break;
                    }
                }
                blutmagieChargedSound.Stop();
                if (bloodmagicProjectile != null)
                {
                    Quaternion bloodmagicDirection = currentAimDirection * Quaternion.Euler(0, 0, -90f);
                    Vector2 directionOutput = Quaternion.AngleAxis(bloodmagicDirection.eulerAngles.z, Vector3.forward) * Vector3.right;
                    if (aimingStopped)
                    {
                        if (m_FacingRight)
                        {
                            directionOutput = Vector2.right;
                        }
                        else
                        {
                            directionOutput = Vector2.left;
                        }
                    }
                    bloodmagicProjectile.GetComponent<BlutmagieFunctions>().direction = directionOutput;
                    bloodmagicProjectile.GetComponent<BlutmagieChargedProjektilControl>().release();
                }
            }
            else if (!dead)
            {
                GameObject bloodmagicProjectile;
                switch (bloodmagicType)
                {
                    case BloodmagicType.Explosion:
                        bloodmagicProjectile = Instantiate(bloodmagicExplosion, bloodmagicExplosionPos.position, Quaternion.identity);
                        InitializeBloodmagicSkills(bloodmagicProjectile, damageInfos[0], damageInfos[1], guid);
                        skill2choice1Rdy = false;
                        AddBloodmagicPushback(bloodmagicProjectile.GetComponent<BlutmagieFunctions>().direction);
                        InitiatePushedTimer(0.5f);
                        break;
                    case BloodmagicType.RangedVolley:
                        for (int i = 0; i < 6; i++)
                        {
                            if (dead) break;
                            yield return new WaitForSeconds(0.05f);
                            bloodmagicProjectile = Instantiate(bulletFar, firePos.position, Quaternion.identity);
                            InitializeBloodmagicSkills(bloodmagicProjectile, damageInfos[0], damageInfos[1], guid);
                        }
                        skill8choice2Rdy = false;
                        break;
                    case BloodmagicType.Boomerang:
                        bloodmagicProjectile = Instantiate(boomerangBloodmagic, firePos.position, Quaternion.identity);
                        InitializeBloodmagicSkills(bloodmagicProjectile, damageInfos[0], damageInfos[1], guid);
                        skill8choice1Rdy = false;
                        break;
                    case BloodmagicType.RangedBite:
                        float angle = -90;
                        if (!m_FacingRight)
                        {
                            angle *= -1;
                        }
                        Quaternion biteRotation = currentAimDirection * Quaternion.Euler(0, 0, angle);
                        bloodmagicProjectile = Instantiate(biteBloodmagic, firePos.position, biteRotation);
                        InitializeBloodmagicSkills(bloodmagicProjectile, damageInfos[0], damageInfos[1], guid);
                        _rangedBiteCooldownCoroutine = StartCoroutine(RangedBiteCooldownRoutine());
                        break;
                    case BloodmagicType.NormalFireball:
                        bloodmagicProjectile = Instantiate(standardBullet, firePos.position, Quaternion.identity);
                        InitializeBloodmagicSkills(bloodmagicProjectile, damageInfos[0], damageInfos[1], guid);
                        break;
                    case BloodmagicType.HitScan:
                        GameObject muzzleFlash = RaycastBloodmagic(damageInfos[0], damageInfos[1], guid);
                        yield return new WaitForSeconds(0.15f);
                        Destroy(muzzleFlash);
                        aimTimer = 0;
                        muzzleFlash = RaycastBloodmagic(damageInfos[0], damageInfos[1], guid);
                        yield return new WaitForSeconds(0.2f);
                        Destroy(muzzleFlash);
                        break;
                    case BloodmagicType.Beam:
                        GameObject bloodmagicBeam = BeamBloodmagic(damageInfos[0], damageInfos[1], guid);
                        AddBloodmagicPushback(bloodmagicBeam.GetComponent<BlutmagieFunctions>().direction);
                        skill2choice2Rdy = false;
                        break;
                }
            }
            playerAimVisualisation.stopScanning();
            
            if (bloodmagicChargeEffect != null)
            {
                Destroy(bloodmagicChargeEffect);
            }
            
            if (type != 1)
            {
                if (bloodmagicType == BloodmagicType.HitScan)
                {
                    yield return new WaitForSeconds(0.05f);
                }
                else if (bloodmagicType == BloodmagicType.RangedVolley)
                {
                    yield return new WaitForSeconds(0.15f);
                }
                else
                {
                    yield return new WaitForSeconds(0.45f);
                }
            }
            
            firingBloodmagic = false;
            userControl.bloodmagicBlockInput = false;
            m_Anim.SetBool(animationName, false);
            

            void AddBloodmagicPushback(Vector2 direction)
            {
                float pushback = bloodmagicPushback;
                if (!grounded)
                {
                    pushback *= 2;
                }
                m_Rigidbody2D.AddForce(direction * -1 * pushback);
                InitiatePushedTimer(0.25f);
            }
        }

        private GameObject RaycastBloodmagic(bool extraDamageSkill7, bool extraDamageSkill11, System.Guid guid)
        {
            GameObject muzzleFlashInstance = Instantiate(hitScanBloodmagicMuzzleFlash, rayFirePos.position, Quaternion.identity, rayFirePos);
            GameObject bloodmagicProjectile = Instantiate(hitScanBloodmagic, rayFirePos.position, Quaternion.identity);
            InitializeBloodmagicSkills(bloodmagicProjectile, extraDamageSkill7, extraDamageSkill11, guid);
            bloodmagicProjectile.GetComponent<BloodmagicHitScan>().directionRotation = currentAimDirection;
            bloodmagicProjectile.GetComponent<BloodmagicHitScan>().Launch(muzzleFlashInstance);
            return muzzleFlashInstance;
        }
        
        private GameObject BeamBloodmagic(bool extraDamageSkill7, bool extraDamageSkill11, System.Guid guid)
        {
            GameObject muzzleFlashInstance = Instantiate(beamBloodmagicMuzzleFlash, rayFirePos.position, Quaternion.identity, rayFirePos);
            GameObject bloodmagicBeam = Instantiate(beamBloodmagic, rayFirePos.position, Quaternion.identity);
            InitializeBloodmagicSkills(bloodmagicBeam, extraDamageSkill7, extraDamageSkill11, guid);
            bloodmagicBeam.GetComponent<BloodmagicBeam>().directionRotation = currentAimDirection;
            bloodmagicBeam.GetComponent<BloodmagicBeam>().Launch(muzzleFlashInstance);
            return bloodmagicBeam;
        }
        
        IEnumerator Biting()
        {
            userControl.blockActions = true;
            System.Guid guid = System.Guid.NewGuid();
            
            if(!stats.human)
            {
                m_Anim.SetBool("Biting", true);
                m_Anim.SetTrigger("Bite"); 
                biteSound.PlayDelayed(0);
            }

            AppySkill1Effects();
            
            yield return new WaitForSeconds(0.1f);
            if (!stats.human)
            {
                Collider2D[] cols1;
                Collider2D[] cols2;
                for (int i = 0; i < 5; i++) {
                    if (!m_Anim.GetBool("Death")) {
                        cols1 = Physics2D.OverlapCircleAll(biteCheckPos1.position, 1.0f);
                        cols2 = Physics2D.OverlapCircleAll(biteCheckPos2.position, 1.0f);
                        foreach (Collider2D col in cols1)
                        {
                            if (col.gameObject.GetComponent<EnemyStats>() != null)
                            {
                                BiteHit(col.gameObject.GetComponent<EnemyStats>(), stats.biteDamage, guid, biteDashed);
                                BiteEffects();
                            }
                        }
                        foreach (Collider2D col in cols2)
                        {
                            if (cols1.Any(x => x.Equals(col))) continue;
                            
                            if (col.gameObject.GetComponent<EnemyStats>() != null)
                            {
                                BiteHit(col.gameObject.GetComponent<EnemyStats>(), stats.biteDamage, guid, biteDashed);
                                BiteEffects();
                            }
                        }
                    }
                    yield return new WaitForSeconds(0.05f);
                }
                m_Anim.SetBool("Biting", false);
            }
            userControl.blockActions = false;
            if (skill1choice1)
            {
                biteDashed = false;
            }
        }

        private void AppySkill1Effects()
        {
            if ((skill1choice1 && grounded && _groundDashCooldownCoroutine == null) || (!grounded && (_dashCounter < 1  || (skill1choice2 && _dashCounter < 2))) && !biteDashed)
            {
                GameObject dashChild;
                if (skill1choice1 && grounded && _groundDashCooldownCoroutine == null)
                {
                    dashChild = Instantiate(dashEffect1u1, transform.position, Quaternion.identity);
                    NewInvulnerable(0.45f, true, true);
                    _groundDashCooldownCoroutine = StartCoroutine(GroundDashCooldownRoutine());
                }
                else
                {
                    _dashCounter++;
                    dashChild = Instantiate(dashEffect1u2, transform.position, Quaternion.identity);
                }
                dashChild.transform.parent = gameObject.transform;

                Vector2 direction = bone1.rotation * Vector2.down;
                direction = -aimDirection;
                if (aimingStopped)
                {
                    if (m_FacingRight)
                    {
                        direction = Vector2.right;
                    }
                    else
                    {
                        direction = Vector2.left;
                    }
                }
                
                float dashVelocityY = m_Rigidbody2D.velocity.y;
                if (dashVelocityY < 0 && direction.y > 0)
                {
                    m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, 0);
                }
                m_Rigidbody2D.velocity = Vector2.zero;
                
                //apply extra force
                direction = direction.normalized * dashSpeed;

                //height is capped to limit out of bounds jumps
                if (direction.y > dashSpeed/2)
                {
                    direction = new Vector2(direction.x, dashSpeed/2f);
                }
                
                m_Rigidbody2D.AddForce(direction);
                biteDashed = true;
                if (!flyingSkill12u1Active)
                {
                    m_Rigidbody2D.gravityScale = 0;
                }

                float dashTime = 0.15f;
                InitiatePushedTimer(dashTime);
                StartCoroutine(BiteDashedFlyCounter(dashTime));
            }
        }

        private IEnumerator GroundDashCooldownRoutine()
        {
            yield return new WaitForSeconds(dashCooldown);
            _groundDashCooldownCoroutine = null;
            groundDashOffCooldownSound.PlayOneShot(groundDashOffCooldownSound.clip);
        }
        
        private IEnumerator RangedBiteCooldownRoutine()
        {
            yield return new WaitForSeconds(rangedBiteCooldown);
            _rangedBiteCooldownCoroutine = null;
        }

        public void BiteHit(EnemyStats hitEnemy, int damage, System.Guid guid, bool biteDashEnhance)
        {
            if (!hitEnemy.unverwundbar && hitEnemy.health > 0 && !hitEnemy.unbiteable )
            {
                if (hitEnemy.health < damage)
                {
                    Instantiate((Object)biteHitEffectBig, hitEnemy.GetComponent<Rigidbody2D>().position, Quaternion.identity);
                }
                else
                {
                    Instantiate((Object)biteHitEffectSmall, hitEnemy.GetComponent<Rigidbody2D>().position, Quaternion.identity);
                }

                int biteDamage = damage;
                if (biteDashEnhance && skill1choice2)
                {
                    biteDamage += damage * 2;
                }
                if (bloodlust <= 0 && skill6choice2)
                {
                    biteDamage += damage * 2;
                }
                hitEnemy.Hit(biteDamage, DamageTypeToEnemy.Bite, guid, true);
            }
        }

        public void BiteEffects()
        {
            if (skill2choice1)
            {
                skill2choice1Rdy = true;
            }
            if (skill2choice2)
            {
                skill2choice2Rdy = true;
            }
        }

        public void BiteHeal()
        {
            int healthHeal = 30;
            float biteBloodThirstHeal = stats.biteBloodThirstHeal;
            if (bloodlust <= 0 && skill6choice2)
            {
                healthHeal *= 2;
                biteBloodThirstHeal *= 2;
            }
            if (skill4choice2)
            {
                healthHeal += 25;
            }
            ChangeBloodthirst(biteBloodThirstHeal, Guid.Empty);
            if (skill3choice2)
            {
                AddMaxHealth(1);
            }
            if (skill7choice1 && !extraDamage7u1)
            {
                extraDamage7u1 = true;
            }
            if (skill7choice2)
            {
                AddHealthOverheal(healthHeal);
            }
            else
            {
                AddHealth(healthHeal);
            }
        }

        private IEnumerator BiteDashedFlyCounter(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            biteDashed = false;
            if (!flyingSkill12u1Active)
            {
                m_Rigidbody2D.gravityScale = startGravity;
            }
        }

        private IEnumerator LowHPBloodmagicCooldown()
        {
            lowHPBloodMagicOnCooldown = true;
            yield return new WaitForSeconds(3f);
            lowHPBloodMagicOnCooldown = false;
        }
        
        private void ClearBloodmagicEmpowerments()
        {
            if (extraDamage7u1)
            {
                extraDamage7u1 = false;
            }
            if (skill11u1Stacks >= 5 || skill11u2Stacks >= 5)
            {
                skill11u1Stacks = 0;
                skill11u2Stacks = 0;
            }
        }
    }
    
    enum BloodmagicType
    {
        NormalFireball,
        HitScan,
        Explosion,
        RangedVolley,
        Boomerang,
        RangedBite,
        Beam,
    }
}
