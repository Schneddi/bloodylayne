using System;
using System.Collections;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using UnityEngine;
using UnityStandardAssets._2D;
using Object = UnityEngine.Object;

namespace Units.Player.Scripts.PlayerControl.PlatformCharacter2D
{
    public partial class PlatformerCharacter2D : MonoBehaviour
    {
        public bool Hit(int damage, DamageTypeToPlayer damageType, Vector2 hitPosition, GameObject source, Guid guid)//incoming attackDamage without a push
        {
            return HandleHits(damage, damageType, Vector2.zero, true, false, hitPosition, source, guid);
        }
        
        public bool Hit(int damage, DamageTypeToPlayer damageType, Vector2 pushDirection, Vector2 hitPosition, GameObject source, Guid guid)//incoming attackDamage with a push added to the rigidbody
        {
            return HandleHits(damage, damageType, pushDirection, true, false, hitPosition, source, guid);
        }
        
        public void ForcedHit(int damage, DamageTypeToPlayer damageType, Vector2 hitPosition, GameObject source, Guid guid)//incoming attackDamage with a push added to the rigidbody, does not check invulnerability
        {
            HandleHits(damage, damageType, Vector2.zero, true, true, hitPosition, source, guid);
        }
        
        public void ForcedHit(int damage, DamageTypeToPlayer damageType, Vector2 pushDirection, Vector2 hitPosition, GameObject source, Guid guid)//incoming attackDamage with a push added to the rigidbody, does not check invulnerability
        {
            HandleHits(damage, damageType, pushDirection, true, true, hitPosition, source, guid);
        }
        
        public bool HitBeam(int damage, DamageTypeToPlayer damageType, Vector2 pushDirection, Vector2 hitPosition, GameObject source, Guid guid)//for beam attacks
        {
            return HandleHits(damage, damageType, pushDirection, false, false, hitPosition, source, guid);
        }

        private bool HandleHits(int damage, DamageTypeToPlayer damageType, Vector2 pushDirection, bool makeInvul, bool skipInvulnerable, Vector2 hitPosition, GameObject source, Guid guid)
        {
            if (invulnerable && invulnerableEffectCoroutine != null && !dead && makeInvul && !skipInvulnerable)
            {
                if (lastInvulEffectTimeTable.ContainsKey(source))
                {
                    float lastInvulTime;
                    lastInvulEffectTimeTable.TryGetValue(source, out lastInvulTime);
                    if (Time.time - lastInvulTime < 0.25f)
                    {
                        return false;
                    }
                    else
                    {
                        lastInvulEffectTimeTable.Remove(source);
                    }
                }
                lastInvulEffectTimeTable.Add(source, Time.time);
                Instantiate((Object)invulnerableHitEffect, hitPosition, Quaternion.identity);
            }
            if (invulnerable && !skipInvulnerable)
            {
                return false;
            }

            if (skill9u1Spawned && (damageType == DamageTypeToPlayer.MeleeStab || damageType == DamageTypeToPlayer.MeleeSwing|| damageType == DamageTypeToPlayer.MeleeFeetStab || damageType == DamageTypeToPlayer.RavenStab || damageType == DamageTypeToPlayer.SkullBite))
            {
                Vector2 pushBackDirection = source.transform.position - transform.position;
                soulSkill9u1Child.GetComponent<SoulSkill9u1>().ApplySoul9u1Effects(source, pushBackDirection);
                Instantiate((Object)soulSkill9u1HitEffect, hitPosition, Quaternion.identity);
                return false;
            }
            //look into death for is player invul
            AddDamage(damage, damageType, guid);

            float damagePercentage = (float)damage / (float)stats.maxHealth;
            float remainingHealth = (float)stats.health / (float)stats.maxHealth;
            damageUIScript.DamagePlayer(damagePercentage, remainingHealth);
            if (pushDirection != Vector2.zero)
            {
                m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.y + pushDirection.x, m_Rigidbody2D.velocity.y + pushDirection.y);
                InitiatePushedTimer(0.2f);
            }

            if (makeInvul)
            {
                NewInvulnerable(0.5f, false, true);
            }
            return true;
        }

        public void KillPlayer(DamageTypeToPlayer damageType)
        {
            if (!m_Anim.GetBool("Death"))
            {
                stats.health = 0;
                StartCoroutine(Death(damageType));
            }
        }
        
        public void KillPlayer(DamageTypeToPlayer damageType, Vector2 pushDirection)
        {
            if (!m_Anim.GetBool("Death"))
            {
                stats.health = 0;
                StartCoroutine(Death(damageType));
                m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.y + pushDirection.x, m_Rigidbody2D.velocity.y + pushDirection.y);
                InitiatePushedTimer(0.2f);
            }
        }

        private void AddDamage(int damage, DamageTypeToPlayer damageType, Guid guid)
        {
            if(!dead)
            {  
                if (skill10choice1 && !gasCloudOnCooldown)
                {
                    gasCloudOnCooldown = true;
                    StartCoroutine(GasCloudCooldown());
                    Instantiate((Object)gasCloudSkill10u1, m_GroundCheck2.transform.position, Quaternion.identity);
                }
                if (skill11choice2 && !skill11u2DamageOnCooldown)
                {
                    if (skill11u2Stacks < 5)
                    {
                        skill11u2Stacks++;
                        GameObject hateChild = Instantiate(hateEffectSkill11u2, transform.position, Quaternion.identity);
                        hateChild.GetComponent<AudioSource>().pitch = 1 - 0.4f + skill11u2Stacks * 0.2f;
                        hateChild.transform.parent = gameObject.transform;
                        StartCoroutine(Skill11u2DamageCooldown());
                    }
                }
                StartNewHealthCooldown(damage, stats.health);
                if (stats.health - damage <= 0)
                {
                    if (!dead)
                    {
                        ShowDamageNumber(damage);
                        StartCoroutine(Death(damageType));
                    }
                }
                else
                {
                    ShowDamageNumber(damage);
                    stats.health = stats.health - damage;
                    ChangeBloodthirst(stats.bloodLossByRecievingDamage, guid);
                }
            }
        }
        public void NewInvulnerable(float seconds, bool showVisualEffect, bool higherLength)
        {
            if (invulnerableCoroutine != null)
            {
                float passedTime = Time.time - invulnerableInitTime;
                float remainingInvulDuration = invulnerableInitLength - passedTime;
                if (higherLength && seconds > remainingInvulDuration)
                {
                    StopInvulnerableVisualEffect();
                    invulnerableCoroutine = StartCoroutine(InvulnerableCoroutine(seconds, showVisualEffect));
                }
                else if (!higherLength)
                {
                    StopCoroutine(invulnerableCoroutine);
                    invulnerableCoroutine = StartCoroutine(InvulnerableCoroutine(seconds, showVisualEffect));
                }
            }
            else
            {
                invulnerableCoroutine = StartCoroutine(InvulnerableCoroutine(seconds, showVisualEffect));
            }
        }

        private IEnumerator InvulnerableCoroutine(float seconds, bool showVisualEffect)
        {
            invulnerableInitLength = seconds;
            invulnerableInitTime = Time.time;
            invulnerable = true;
            if (showVisualEffect)
            {
                StopInvulnerableVisualEffect();
                invulnerableEffectCoroutine = StartCoroutine(InvulnerableVisualEffect());
            }
            yield return new WaitForSeconds(seconds);
            StopInvulnerable();
        }

        public void StopInvulnerable()
        {
            if (!gv.forcedInvulnerable)
            {
                if (invulnerableCoroutine != null)
                {
                    StopCoroutine(invulnerableCoroutine);
                    invulnerableCoroutine = null;
                }
                StopInvulnerableVisualEffect();
                invulnerable = false;
                invulnerableInitLength = 0;
            }
        }

        private IEnumerator InvulnerableVisualEffect()
        {
            foreach (SpriteRenderer spriteRenderer in GetComponentsInChildren<SpriteRenderer>(true))
            {
                spriteRenderer.color = Color.cyan;
            }
            yield return new WaitForSeconds(0.2f);
            foreach (SpriteRenderer spriteRenderer in GetComponentsInChildren<SpriteRenderer>(true))
            {
                spriteRenderer.color = Color.white;
            }
            yield return new WaitForSeconds(0.2f);
            if (invulnerable)
            {
                invulnerableEffectCoroutine = StartCoroutine(InvulnerableVisualEffect());
            }
        }

        private void StopInvulnerableVisualEffect()
        {
            if (invulnerableEffectCoroutine != null)
            {
                foreach (SpriteRenderer spriteRenderer in GetComponentsInChildren<SpriteRenderer>(true))
                {
                    spriteRenderer.color = Color.white;
                }
                StopCoroutine(invulnerableEffectCoroutine);
                invulnerableEffectCoroutine = null;
            }
        }
        
        public void AddHealth(int healthAdd)
        {
            if (!dead)
            {
                if (stats.health > stats.maxHealth)
                {
                    healthAdd = 0;
                }
                else if (stats.health + healthAdd > stats.maxHealth)
                {
                    healthAdd = stats.maxHealth-stats.health;
                }

                DisplayHealingNumber(healthAdd);
                
                stats.health += healthAdd;
            }
        }

        public void AddMaxHealth(int maxHealthAdd)
        {
            stats.maxHealth = stats.maxHealth + maxHealthAdd;
            if (stats.health > 0)
            {
                DisplayHealingNumber(maxHealthAdd);
                
                stats.health = stats.health + maxHealthAdd;
            }
        }

        private IEnumerator StartBatTransform()
        {
            if (!batform)
            {
                deathCloudEffect.GetComponent<ParticleSystem>().Play();
                respawnBatFlyLoop.PlayDelayed(0);
                respawnSmokeSound.PlayDelayed(0);
                m_Anim.SetTrigger("BatStartTransform");
                gameObject.name = "PlayerBat";
                gameObject.layer = 16;
                bCollider.enabled = false;
                startGravity = m_Rigidbody2D.gravityScale;
                m_Rigidbody2D.gravityScale = 0;
                m_Rigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;
                m_Rigidbody2D.velocity = new Vector2(0, 0);
                yield return new WaitForSeconds(0.25f);
                m_Rigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;
                m_Rigidbody2D.velocity = new Vector2(0, 0);
                respawnStartPoint = transform.position;
                respawnEndPoint = RespawnPosition;
                batFlightDuration = Vector2.Distance(respawnEndPoint, respawnStartPoint) * 0.1f;
                if (batFlightDuration > 1.25f)
                {
                    batFlightDuration = 1.25f;
                }
                passedBatFlightTime = 0;
                batform = true;
            }
        }
        
        private IEnumerator EndBatTransform()
        {
            deathCloudEffect.GetComponent<ParticleSystem>().Play();
            batform = false;
            m_Anim.SetTrigger("BatEndTransform");
            respawnBatFlyLoop.Stop();
            respawnSmokeSound.PlayDelayed(0);
            if (LastRespawnPoint != null)
            {
                LastRespawnPoint.GetComponent<AutosaveZone>().PlayRespawnEffect();
            }
            yield return new WaitForSeconds(0.25f);
            m_Rigidbody2D.velocity = new Vector2(0, 0);
            m_Rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
            gameObject.name = "Player";
            gameObject.layer = 8;
            bCollider.enabled = true;
            m_Rigidbody2D.gravityScale = startGravity;
            Respawn(false);
        }

        private void CheckDeath()
        {
            if (stats.health <= 0 && !m_Anim.GetBool("Death"))
            {
                StartCoroutine(Death(0));
            }
        }
        
        private IEnumerator Death(DamageTypeToPlayer damageType)
        {
            try
            {
                GameController.PlayerHealthUI.showDeadHeart();
            }
            catch (Exception e)
            {
                Debug.Log("Error referencing the playerUI: " + e);
            }
            stats.health = 0;
            dead = true;
            StopLadderLoop();
            NewInvulnerable(6.5f, false, true);
            if(bloodLossCounterCoroutine != null)
            {
                StopCoroutine(bloodLossCounterCoroutine);
            }
            userControl.BlockInput = true;
            m_Anim.SetBool("Death", true);
            m_Rigidbody2D.gravityScale = startGravity;
            if (extraDamage7u1)
            {
                extraDamage7u1 = false;
            }
            if (skill10choice2)
            {
                Instantiate((Object)deathExplosionSkill10u2, transform.position, Quaternion.identity);
                ExplosionSkill10u1Damage();
            }
            switch (damageType)
            {
                case DamageTypeToPlayer.Arrow:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.Bullet:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.Burned:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.Electrocuted:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.Explosion:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.Normal:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.Smashed:
                    m_Anim.SetTrigger("Smashed");
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.AssassinStab:
                    m_Anim.SetTrigger("AssassinStabDeath");
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.BluntProjectile:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.ClappingHands:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.DragonBurn:
                    m_Anim.SetTrigger("DragonBurn");
                    yield return new WaitForSeconds(4.5f);
                    break;
                case DamageTypeToPlayer.GhostTouch:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.ImpaledFeet:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.KnifeThrow:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.MagicProjectile:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.MagicShield:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.MeleeStab:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.MeleeSwing:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.RavenStab:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.SkullBite:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.SonicWave:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.WerewolfBite:
                    m_Anim.SetTrigger("WerewolfBite");
                    yield return new WaitForSeconds(2.5f);
                    break;
                case DamageTypeToPlayer.GodNailImpale:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.KilledByUser:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
                case DamageTypeToPlayer.MeleeFeetStab:
                    m_Anim.SetTrigger("DeathTrigger");
                    normalDeathSound.PlayDelayed(0);
                    yield return new WaitForSeconds(1.5f);
                    break;
            }
			if(stats.lifes>0){
                stats.lifes--;
                StartCoroutine(StartBatTransform());
                yield return new WaitForSeconds(2f);
                StopInvulnerable();
            }
            else{
                GameController gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
                gc.GameOver();
            }
        }
        
        private void Respawn(bool resetCamera)
        {
            GameController.PlayerHealthUI.ResetHeartRate();
            dead = false;
            transform.position = RespawnPosition;
            m_Rigidbody2D.velocity = new Vector2(0, 0);
            userControl.BlockInput = false;
            userControl.bloodmagicBlockInput = false;
            stats.health = stats.maxHealth;
            bloodlust = stats.maxBloodlust;
            damageUIScript.StopAllDamageUIEffects();
            RestartBloodthirstDamage();
            if (skill8choice2)
            {
                AddMaxHealth(5);
            }
            StartNewHealthCooldown(0,0);
            m_Anim.SetBool("Death", false);
            if (resetCamera)
            {
                StartCoroutine(RespawnCamera());
            }
        }
        public void Respawn2()//Respawn nicht bei Tod, sondern z.B. wenn durch Waende gefallen
        {
            if (LastRespawnPoint != null)
            {
                LastRespawnPoint.GetComponent<AutosaveZone>().PlayRespawnEffect();
            }
            transform.position = RespawnPosition;
            m_Rigidbody2D.velocity = new Vector2(0, 0);
            StartCoroutine(RespawnCamera());
        }
        
        private IEnumerator RespawnCamera()
        {
            Camera2DFollow camera2DFollowScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
            for(int i=30; i > 0; i--)
            {
                camera2DFollowScript.SetDamping((float)i/100);
                yield return new WaitForSeconds(1f/30f);
            }
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().ResetDamping();
        }
        
        //goals:
        //feedbackEffect when player activated invul
        //no feedbackEffect when player is dead or hiddenInvul
        private void ShowDamageNumber(int damage)
        {
            if (gv.displayChangeNumbers)
            {
                GameObject statChangeNumber = Instantiate(gv.healthChangeNumber, transform.position, Quaternion.identity);
                statChangeNumber.GetComponent<StatChangeNumberControl>().setupInfoNumber(damage, DamageNumberType.DamageReceived);
            }
        }
        
        private bool[] GetExtraDamageInfo()
        {
            bool[] damageInfos = new bool[2];
                
            if (extraDamage7u1)
            {
                damageInfos[0] = true;
            }
            if (skill11u1Stacks >= 5 || skill11u2Stacks >= 5)
            {
                damageInfos[1] = true;
            }

            return damageInfos;
        }

        private void ApplyBloodmagicSelfDamage()
        {
            if (!(skill5choice2 && bloodlust <= 0) && stats.health > 5)
            {
                stats.health = stats.health - 5;
            }
        }

        public void AddHealthOverheal(int healthAdd)
        {
            if (stats.health > 0)
            {
                DisplayHealingNumber(healthAdd);
                
                stats.health = stats.health + healthAdd;
                if (stats.health > stats.maxHealth && !overhealed)
                {
                    overhealed = true;
                    StartCoroutine(OverhealTicker());
                }
            }
        }

        private void DisplayHealingNumber(int healing)
        {
            if (healing != 0 && gv.displayChangeNumbers)
            {
                GameObject statChangeNumber = Instantiate(gv.healthChangeNumber, transform.position, Quaternion.identity);
                statChangeNumber.GetComponent<StatChangeNumberControl>().setupInfoNumber(healing, DamageNumberType.HealingDone);
            }
        }

    }
}
