using System;
using System.Collections;
using UI.PlayerUI;
using Units.Enemys;
using UnityEngine;
using UnityStandardAssets._2D;
using Object = UnityEngine.Object;

namespace Units.Player.Scripts.PlayerControl.PlatformCharacter2D
{
    public partial class PlatformerCharacter2D : MonoBehaviour
    {
        public void InitializeSkills(bool forceInit)
        {
            if (!gv.skill0Known && !gv.skill2Known && !gv.skill8Known && !gv.skill13Known)
            {
                userControl.disableBloodmagic = true;
            }
            else
            {
                userControl.disableBloodmagic = false;
            }
            if (skill3choice1)
            {
                stats.maxBloodlust *= 2;
                bloodlust *= 2;
            }
            if (!noStartSkillInit || forceInit)
            {
                if (skill9u1Spawned)
                {
                    SpawnSoulSkill9u1();
                }
                else if (skill9u2Spawned)
                {
                    SpawnSoulSkill9u2();
                }

                if (skill12u1Loaded)
                {
                    RdySkill12u1();
                }
                else if (skill12u2Loaded && !stats.human)
                {
                    RdySkill12u2();
                }
            }
        }

        private void InitializeBloodmagicSkills(GameObject bloodmagicChild, bool extraDamageSkill7, bool extraDamageSkill11, System.Guid guid)
        {
            if (bloodlust <= 0)
            {
                bloodmagicChild.GetComponent<BlutmagieFunctions>().empoweredByBloodthirst = true;
                bloodmagicChild.GetComponent<BlutmagieFunctions>().additionaldamage += (int)(bloodmagicChild.GetComponent<BlutmagieFunctions>().attackDamage * 0.50f);
            }
            if (extraDamageSkill7)
            {
                bloodmagicChild.GetComponent<BlutmagieFunctions>().empowered7u1 = true;
                bloodmagicChild.GetComponent<BlutmagieFunctions>().additionaldamage += (int)(bloodmagicChild.GetComponent<BlutmagieFunctions>().attackDamage * 1.0f);
            }
            if (extraDamageSkill11)
            {
                bloodmagicChild.GetComponent<BlutmagieFunctions>().skill11Empowerment = true;
                bloodmagicChild.GetComponent<BlutmagieFunctions>().additionaldamage += (int)(bloodmagicChild.GetComponent<BlutmagieFunctions>().attackDamage * 1.0f);
            }

            Quaternion bloodmagicDirection = currentAimDirection * Quaternion.Euler(0, 0, -90f);
            Vector2 directionOutput = Quaternion.AngleAxis(bloodmagicDirection.eulerAngles.z, Vector3.forward) * Vector3.right;
            if (aimingStopped)
            {
                if (m_FacingRight)
                {
                    directionOutput = Vector2.right;
                }
                else
                {
                    directionOutput = Vector2.left;
                }
            }
            bloodmagicChild.GetComponent<BlutmagieFunctions>().direction = directionOutput;
            bloodmagicChild.GetComponent<BlutmagieFunctions>().guid = guid;
            bloodmagicChild.GetComponent<BlutmagieFunctions>().InitBlutmagieFunctions();
        }
        
        private void StartNewHealthCooldown(int healthAmount, int maxHealHealth)
        {
            if (skill4choice1)
            {
                if (skill4u1HealingEffectChild != null)
                {
                    skill4u1HealingEffectChild.GetComponent<ParticleSystem>().Stop();
                }
                if (healthAmount > 100)
                {
                    healthAmount = 100;
                }
                if (healCooldownCoroutineRef != null)
                {
                    StopCoroutine(healCooldownCoroutineRef);
                }
                if (healingCoroutineRef != null)
                {
                    StopCoroutine(healingCoroutineRef);
                }
                if (healthAmount > 0)
                {
                    healCooldownCoroutineRef = StartCoroutine(InitiateHealCooldown(healthAmount, maxHealHealth));
                }
            }
        }
        
        IEnumerator InitiateHealCooldown(int healthAmount, int maxHealHealth)
        {
            yield return new WaitForSeconds(5f);
            if (skill4u1HealingEffectChild != null)
            {
                skill4u1HealingEffectChild.GetComponent<ParticleSystem>().Stop();
            }
            skill4u1HealingEffectChild = Instantiate(skill4u1HealEffect, m_CeilingCheck3.position, Quaternion.identity, transform);
            healingCoroutineRef = StartCoroutine(HealingCoroutine(healthAmount, maxHealHealth));
        }

        IEnumerator HealingCoroutine(int healthAmount, int maxHealHealth)
        {
            for (int i = 0; i < healthAmount; i++)
            {
                if (stats.health > 0 && stats.health < stats.maxHealth && stats.health < maxHealHealth)
                {
                    AddHealth(1);
                    yield return new WaitForSeconds(0.1f);
                }
                else
                {
                    if (skill4u1HealingEffectChild != null)
                    {
                        skill4u1HealingEffectChild.GetComponent<ParticleSystem>().Stop();
                    }
                    break;
                }
            }
            if (skill4u1HealingEffectChild != null)
            {
                skill4u1HealingEffectChild.GetComponent<ParticleSystem>().Stop();
            }
        }
        IEnumerator Skill11u2DamageCooldown()
        {
            skill11u2DamageOnCooldown = true;
            yield return new WaitForSeconds(1f);
            skill11u2DamageOnCooldown = false;
        }

        private IEnumerator GasCloudCooldown()
        {
            yield return new WaitForSeconds(10f);
            gasCloudOnCooldown = false;
        }
        
        private void ExplosionSkill10u1Damage()
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 5.5f, 1 << 9);//layer Ebene 9 = Enemys
            System.Guid guid = System.Guid.NewGuid();
            foreach (Collider2D col in cols)
            {
                if (col.GetComponent<EnemyStats>() != null)
                {
                    Vector3 richtung = col.gameObject.transform.position - transform.position;
                    RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
                    foreach (RaycastHit2D hitter in hit)
                    {
                        if (hitter.transform.tag == "Ground")
                        {
                            break;
                        }
                        if (hitter.transform.tag == "Enemy")
                        {
                            if (hitter == col)
                            {
                                col.GetComponent<EnemyStats>().Hit(250, DamageTypeToEnemy.Explosion, guid, true);
                            }
                            break;
                        }
                    }
                }
            }

            cols = Physics2D.OverlapCircleAll(transform.position, 5.5f, 1 << 14);//layer Ebene 14 = Objekte
            {
                foreach (Collider2D col in cols)
                {
                    if (col.GetComponent<ObjektStats>() != null)
                    {
                        Vector3 richtung = col.gameObject.transform.position - transform.position;
                        RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
                        foreach (RaycastHit2D hitter in hit)
                        {
                            if (hitter.transform.CompareTag("Ground"))
                            {
                                break;
                            }

                            if (hitter.transform.CompareTag("Object"))
                            {
                                if (col.gameObject.GetComponent<ObjektStats>().destructable)
                                {
                                    if (hitter == col)
                                    {
                                        col.gameObject.GetComponent<ObjektStats>().addDamage(250);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private IEnumerator OverhealTicker()
        {
            stats.health--;
            yield return new WaitForSeconds(0.5f);
            if (stats.health > stats.maxHealth)
            {
                StartCoroutine(OverhealTicker());
            }
            else
            {
                overhealed = false;
            }
        }

        private IEnumerator MaxChargingCounter()
        {
            yield return new WaitForSeconds(3f);
            firingChargedBloodmagic = false;
        }

        public void SpawnSoulSkill9u1()
        {
            skill9u1Spawned = true;
            soulSkill9u1Child = Instantiate(soulSkill9u1, transform.position, Quaternion.identity);
            soulSkill9u1Child.transform.SetParent(gameObject.transform, false);
            soulSkill9u1Child.transform.localPosition = Vector3.zero;
        }

        public void SpawnSoulSkill9u2()
        {
            float radiant = 0;
            if (soulSkill9u2Child1 != null)
            {
                radiant = soulSkill9u2Child1.GetComponent<SoulSkill9u2>().radiant + 1.0f / 3.0f * Mathf.PI * 2;
                if (radiant > 2 * Mathf.PI)
                {
                    radiant = radiant - 2 * Mathf.PI;
                }
            }
            if (soulSkill9u2Child1 == null)
            {
                soulSkill9u2Child1 = Instantiate(soulSkill9u2, transform.position, Quaternion.identity);
                soulSkill9u2Child1.GetComponent<SoulSkill9u2>().playerChar = transform;
                soulSkill9u2Child1.GetComponent<SoulSkill9u2>().radiant = radiant;
                radiant = radiant + 1.0f / 3.0f * Mathf.PI * 2;
            }
            else
            {
                radiant = soulSkill9u2Child1.GetComponent<SoulSkill9u2>().radiant + 1.0f / 3.0f * Mathf.PI * 2;
                if (radiant > 2 * Mathf.PI)
                {
                    radiant = radiant - 2 * Mathf.PI;
                }
            }

            if (soulSkill9u2Child2 == null)
            {
                soulSkill9u2Child2 = Instantiate(soulSkill9u2, transform.position, Quaternion.identity);
                soulSkill9u2Child2.GetComponent<SoulSkill9u2>().playerChar = transform;
                soulSkill9u2Child2.GetComponent<SoulSkill9u2>().radiant = radiant;
                radiant = radiant + 1.0f / 3.0f * Mathf.PI * 2;
            }
            else
            {
                radiant = soulSkill9u2Child2.GetComponent<SoulSkill9u2>().radiant + 1.0f / 3.0f * Mathf.PI * 2;
                if (radiant > 2 * Mathf.PI)
                {
                    radiant = radiant - 2 * Mathf.PI;
                }
            }

            if (soulSkill9u2Child3 == null)
            {
                soulSkill9u2Child3 = Instantiate(soulSkill9u2, transform.position, Quaternion.identity);
                soulSkill9u2Child3.GetComponent<SoulSkill9u2>().playerChar = transform;
                soulSkill9u2Child3.GetComponent<SoulSkill9u2>().radiant = radiant;
            }
        }

        public void AddSkill11u1Stack(int stackAmount, System.Guid guid)
        {
            if (!registeredGUIDs.Contains(guid))
            {
                registeredGUIDs.Add(guid);
                skill11u1Stacks += stackAmount;
            }
        }

        public void RdySkill12u1()
        {
            if (flyingEffectChild == null)
            {
                flyingEffectChild = Instantiate(flyingRdyEffectSkill12u2, new Vector3(transform.position.x + 0.02f, transform.position.y + 0.05f, transform.position.z), Quaternion.identity);
                flyingEffectChild.transform.parent = gameObject.transform;
                flyingEffectChild.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                skill12u1Loaded = true;
            }
        }

        public void RdySkill12u2()
        {
            if (chargeBloodMagicEffectChild == null)
            {
                chargeBloodMagicEffectChild = Instantiate(chargeBloodSkill12u2, transform.position, Quaternion.identity);
                chargeBloodMagicEffectChild.transform.SetParent(gameObject.transform);
                chargeBloodMagicEffectChild.transform.rotation = Quaternion.identity;
                skill12u2Loaded = true;
            }
        }

        public IEnumerator Skill13u1()
        {
            castingSkill13 = true;
            System.Guid guid = System.Guid.NewGuid();
            if (m_Rigidbody2D.velocity.y < 0)
            {
                m_Rigidbody2D.velocity = new Vector2(0, 30);
            }
            else
            {
                m_Rigidbody2D.velocity = new Vector2(0, m_Rigidbody2D.velocity.y + 30);
            }
            m_Anim.SetBool("JumpSkill", true);
            m_Anim.SetTrigger("JumpDownstrike");
            skill13choice1Rdy = false;
            userControl.skill13DashBlock = true;
            yield return new WaitForSeconds(0.2f);
            m_Rigidbody2D.velocity = new Vector2(0, -5);
            float yPosSave = transform.position.y;
            GameObject bloodmagicChild = null;
            bool[] damageInfos = GetExtraDamageInfo();
            ClearBloodmagicEmpowerments();
            while (m_Rigidbody2D.velocity.y < -1.5)
            {
                if (bloodmagicChild == null)
                {
                    if (skill13BloodmagicPressed)
                    {

                        StartNewHealthCooldown(0, 0);
                        ApplyBloodmagicSelfDamage();
                        if (stats.health <= 5 && !lowHPBloodMagicOnCooldown && !(skill5choice2 && bloodlust <= 0))
                        {
                            StartCoroutine(LowHPBloodmagicCooldown());
                        }
                        bloodmagicChild = Instantiate(skill13BloodmagicEffect, frontHandAttachmentLeft.position, Quaternion.identity);
                        bloodmagicChild.transform.SetParent(frontHandAttachmentLeft);
                        bloodmagicChild.transform.localScale = new Vector3(2, 2, 1);
                        
                        if (!(90 < frontHandAttachmentLeft.parent.eulerAngles.z && frontHandAttachmentLeft.parent.eulerAngles.z < 100) && !(260 < frontHandAttachmentLeft.parent.eulerAngles.z && frontHandAttachmentLeft.parent.eulerAngles.z < 280))
                        {
                            //bloodmagicChild.transform.Find("TrailParticleSystem").localPosition = new Vector3(-0.4f, 0.35f, 0);
                        }
                        InitializeBloodmagicSkills(bloodmagicChild, damageInfos[0], damageInfos[1], guid);
                        skill13TotalAttackDamage = bloodmagicChild.GetComponent<BlutmagieFunctions>().attackDamage;
                    }
                }

                if (bloodmagicChild != null && skill13TotalAttackDamage < 300)
                {
                    skill13TotalAttackDamage += Time.deltaTime * 200;
                    if (skill13TotalAttackDamage > 300)
                    {
                        skill13TotalAttackDamage = 300;
                    }
                    bloodmagicChild.GetComponent<BlutmagieFunctions>().attackDamage = (int)skill13TotalAttackDamage;
                }
                yield return null;
            }

            float stompStrength = yPosSave - transform.position.y * 1.5f;
            
            Collider2D[] colliders1 = Physics2D.OverlapCircleAll(m_GroundCheck1.position, 0.75f, 1<<11 | 1<<10 | 1<<14);
            Collider2D[] colliders2 = Physics2D.OverlapCircleAll(m_GroundCheck2.position, 0.75f, 1<<11 | 1<<10 | 1<<14);
            Collider2D[] colliders3 = Physics2D.OverlapCircleAll(m_GroundCheck3.position, 0.75f, 1<<11 | 1<<10 | 1<<14);

            bool groundHit = false;
            
            for (int i = 0; i < colliders1.Length; i++)
            {
                if (colliders1[i].gameObject != gameObject)
                {
                    groundHit = true;
                }
            }
            for (int i = 0; i < colliders2.Length; i++)
            {
                if (colliders2[i].gameObject != gameObject)
                {
                    groundHit = true;
                }
            }
            for (int i = 0; i < colliders3.Length; i++)
            {
                if (colliders3[i].gameObject != gameObject)
                {
                    groundHit = true;
                }
            }
            
            if (groundHit)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().ShakeCamera(0.1f,1f, 60);
                if (bloodmagicChild != null)
                {
                    skill13u1PushEffect.GetComponent<Skill13u1Control>().bloodmagic = true;
                    skill13u1BloodWaveSound.PlayDelayed(0);
                }
                else
                {
                    skill13u1PushEffect.GetComponent<Skill13u1Control>().bloodmagic = false;
                }
                GameObject leftPush,rightPush;
                skill13u1PushSound.PlayDelayed(0);
                if (m_FacingRight)
                {
                    rightPush = Instantiate(skill13u1PushEffect,
                        new Vector3(firePos.position.x+ .5f, m_GroundCheck3.position.y + 0.75f, firePos.position.z),
                        Quaternion.identity);
                    leftPush = Instantiate(skill13u1PushEffect,
                        new Vector3(firePos.position.x - 1f, m_GroundCheck3.position.y + 0.75f, firePos.position.z),
                        Quaternion.identity);
                }
                else
                {
                    leftPush = Instantiate(skill13u1PushEffect,
                        new Vector3(firePos.position.x- .5f, m_GroundCheck3.position.y + 0.75f, firePos.position.z),
                        Quaternion.identity);
                    rightPush = Instantiate(skill13u1PushEffect,
                        new Vector3(firePos.position.x + 1f, m_GroundCheck3.position.y + 0.75f, firePos.position.z),
                        Quaternion.identity);
                }

                rightPush.GetComponent<Skill13u1Control>().strength = stompStrength;
                leftPush.GetComponent<Skill13u1Control>().strength = stompStrength;
                leftPush.GetComponent<Skill13u1Control>().left = true;
                ParticleSystem.MainModule psMain = leftPush.GetComponent<ParticleSystem>().main;
                psMain.startRotation = 180.0F * Mathf.Deg2Rad;
                InitializeBloodmagicSkills(rightPush, damageInfos[0], damageInfos[1], guid);
                InitializeBloodmagicSkills(leftPush, damageInfos[0], damageInfos[1], guid);
            }
            if (bloodmagicChild != null)
            {
                bloodmagicChild.GetComponent<BloodmagicSkill13>().die();
                bloodmagicChild.transform.SetParent(null);
            }
            userControl.BlockInput = true;
            yield return new WaitForSeconds(0.2f);
            m_Anim.SetBool("JumpSkill", false);
            castingSkill13 = false;
            skill13BloodmagicPressed = false;
            userControl.skill13DashBlock = false;
            userControl.BlockInput = false;
        }

        private IEnumerator Skill13u2()
        {
            System.Guid guid = System.Guid.NewGuid();
            GameObject bloodmagicChild = null;
            GameObject dashChild = Instantiate(skill13u2DashEffect, transform.position, Quaternion.identity);
            dashAiming = true;
            dashChild.transform.parent = gameObject.transform;
            float dashspeed = 60;
            m_Anim.SetBool("JumpSkill", true);
            m_Anim.SetTrigger("JumpSidestrike");
            skill13choice2Rdy = false;
            userControl.skill13DashBlock = true;
            yield return new WaitForSeconds(0.1f);
            if (Math.Abs(m_Rigidbody2D.velocity.magnitude) > dashspeed)
            {
                dashspeed = Math.Abs(m_Rigidbody2D.velocity.magnitude);
            }
            if (!m_FacingRight)
            {
                dashspeed *= -1;
            }
            m_Rigidbody2D.velocity = new Vector2(dashspeed,0);
            while (skill13u2Pressed && Math.Abs(m_Rigidbody2D.velocity.magnitude) > 3 && !onLadder && !dead)
            {
                int bloodmagicBonus = 0;
                if (bloodmagicChild != null)
                {
                    bloodmagicBonus = 20;
                }
                dashspeed = Math.Abs(dashspeed);
                if (grounded)
                {
                    dashspeed -= Time.deltaTime * (80 - bloodmagicBonus);
                }
                else
                {
                    dashspeed -= Time.deltaTime * (100 - bloodmagicBonus);
                }
                if (!m_FacingRight)
                {
                    dashspeed = -dashspeed;
                }

                Vector2 lookDirection = transform.rotation * Vector2.right;
                if (aimingStopped)
                {
                    //facing is already accounted in dashspeed
                    lookDirection = Vector2.right;
                }
                Vector2 dashSpeedIntoDirection = lookDirection.normalized * dashspeed;
                dashSpeedIntoDirection += Vector2.down*18;

                m_Rigidbody2D.velocity = dashSpeedIntoDirection;
                
                if (bloodmagicChild == null && skill13BloodmagicPressed)
                {
                    StartNewHealthCooldown(0, 0);
                    ApplyBloodmagicSelfDamage();
                    if (stats.health <= 5 && !lowHPBloodMagicOnCooldown && !(skill5choice2 && bloodlust <= 0))
                    {
                        StartCoroutine(LowHPBloodmagicCooldown());
                    }
                    bloodmagicChild = Instantiate(skill13BloodmagicEffect, frontHandAttachmentLeft.position, Quaternion.identity);
                    bloodmagicChild.transform.SetParent(frontHandAttachmentLeft);
                    bloodmagicChild.transform.localScale = new Vector3(4, 4, 1);
                    bool[] damageInfos = GetExtraDamageInfo();
                    ClearBloodmagicEmpowerments();
                    InitializeBloodmagicSkills(bloodmagicChild, damageInfos[0], damageInfos[1], guid);
                    skill13TotalAttackDamage = bloodmagicChild.GetComponent<BlutmagieFunctions>().attackDamage;
                }

                if (bloodmagicChild != null && skill13TotalAttackDamage < 300)
                {
                    skill13TotalAttackDamage += Time.deltaTime * 200;
                    if (skill13TotalAttackDamage > 300)
                    {
                        skill13TotalAttackDamage = 300;
                    }
                    bloodmagicChild.GetComponent<BlutmagieFunctions>().attackDamage = (int)skill13TotalAttackDamage;
                }
                yield return new WaitForEndOfFrame();
            }

            if (bloodmagicChild != null)
            {
                bloodmagicChild.GetComponent<BloodmagicSkill13>().die();
                bloodmagicChild.transform.SetParent(null);
            }
            skill13u2Pressed = false;
            m_Anim.SetBool("JumpSkill", false);
            dashChild.GetComponent<ParticleSystem>().Stop();
            castingSkill13 = false;
            skill13BloodmagicPressed = false;
            userControl.skill13DashBlock = false;
            dashAiming = false;
            transform.localRotation = Quaternion.identity;
        }
        
        private IEnumerator DestroySkill13BloodmagicChargeEffect()
        {
            yield return new WaitForSeconds(0.25f);
            if (skill13ChargeEffect != null)
            {
                Destroy(skill13ChargeEffect);
            }
        }

        private IEnumerator Flying()
        {
            flyingSkill12u1Active = true;
            yield return new WaitForSeconds(10f);
            m_Rigidbody2D.gravityScale = startGravity;
            flyingSkill12u1Active = false;
            flyingWingsEffectChild.GetComponent<LarryWingAnimations>().endFlight();
        }

    }
}
