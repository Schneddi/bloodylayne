using System;
using UnityEngine;

namespace Units.Player.Scripts.PlayerControl.PlatformCharacter2D
{
    public partial class PlatformerCharacter2D : MonoBehaviour
    {
        private void ShowPreparedAttachments()
        {
            GameObject currentAttackEffect = null;
            GameObject currentEmpowerment7u1 = null;
            GameObject currentEmpowerment11 = null;
            
            if (extraDamage7u1)
            {
                currentEmpowerment7u1 = empoweredBloodmagic7u1Effect;
            }
            if (skill11u1Stacks >= 5 || skill11u2Stacks >= 5)
            {
                currentEmpowerment11 = empoweredBloodmagicSkill11Effect;
            }
            if (skill8choice1Rdy)
            {
                currentAttackEffect = skill8u1RdyEffect;
            }
            if (skill8choice2Rdy)
            {
                currentAttackEffect = skill8u2RdyEffect;
            }
            if (skill2choice1Rdy)
            {
                currentAttackEffect = skill2u1RdyEffect;
            }
            if (skill2choice2Rdy)
            {
                currentAttackEffect = skill2u2RdyEffect;
            }

            Transform currentAttachmentPoint = GetCurrentRightHandAttachmentTransform();
            
            //skip next logic, if no attack is and was attached
            if (currentEmpowerment7u1 != null || lastEmpowerment7u1 != null ||  currentEmpowerment11 != null || lastEmpowerment11 != null || currentAttackEffect != null || lastAttackEffect != null)
            {
                spawnedEmpowerment7u1 = CheckEffectAttachment(currentEmpowerment7u1, lastEmpowerment7u1, spawnedEmpowerment7u1, currentAttachmentPoint);
                spawnedEmpowerment11 = CheckEffectAttachment(currentEmpowerment11, lastEmpowerment11, spawnedEmpowerment11, currentAttachmentPoint);
                spawnedAttackEffect = CheckEffectAttachment(currentAttackEffect, lastAttackEffect, spawnedAttackEffect, currentAttachmentPoint);
            }

            lastAttachmentPoint = currentAttachmentPoint;
            lastEmpowerment7u1 = currentEmpowerment7u1;
            lastEmpowerment11 = currentEmpowerment11;
            lastAttackEffect = currentAttackEffect;
        }

        private GameObject CheckEffectAttachment(GameObject currentEffect, GameObject lastEffect, GameObject effectInstance, Transform attachmentPoint)
        {
            if (lastEffect != currentEffect)
            {
                if (currentEffect == null)
                {
                    Destroy(effectInstance);
                }
                else
                {
                    if (lastEffect != null)
                    {
                        Destroy(effectInstance);
                    }
                    effectInstance = Instantiate(currentEffect, attachmentPoint, false);
                    effectInstance.transform.position = attachmentPoint.position;
                    if (effectInstance.GetComponent<AudioSource>() != null)
                    {
                        effectInstance.GetComponent<AudioSource>().PlayDelayed(0);
                    }
                    ChangeAttachmentRenderingLayer(effectInstance.GetComponent<ParticleSystem> ().GetComponent<Renderer>(), attachmentPoint, 1);
                }
            }
            if (effectInstance != null && attachmentPoint != lastAttachmentPoint)
            {
                effectInstance.transform.position = attachmentPoint.position;
                effectInstance.transform.SetParent(attachmentPoint);
                ChangeAttachmentRenderingLayer(effectInstance.GetComponent<ParticleSystem> ().GetComponent<Renderer>(), attachmentPoint, 1);
                effectInstance.transform.localScale = new Vector3(Math.Abs(effectInstance.transform.localScale.x), Math.Abs(effectInstance.transform.localScale.y), Math.Abs(effectInstance.transform.localScale.z));
            }

            if(effectInstance != null && effectInstance.transform.position != attachmentPoint.position)
            {
                effectInstance.transform.position = attachmentPoint.position;
            }
            return effectInstance;
        }

        private Transform GetCurrentRightHandAttachmentTransform()
        {
            if (frontAnimation.gameObject.activeSelf)
            {
                return frontHandAttachmentRight;
            }
            if (sideAnimation.gameObject.activeSelf)
            {
                return sideHandAttachmentRight;
            }
            if (backAnimation.gameObject.activeSelf)
            {
                return backHandAttachmentRight;
            }
            return backHandAttachmentRight;
        }
        
        private void ChangeAttachmentRenderingLayer(Renderer renderer, Transform currentAttachment, int type)
        {
            int currentRenderLayer = renderer.sortingOrder;
            int frontLayer = 80;
            int sideLayer = 22;
            int backLayer = 0;
            if (type == 1)
            {
                frontLayer += 1;
                sideLayer += 1;
                backLayer += 1;
            }
            if (currentAttachment == frontHandAttachmentRight && currentRenderLayer != frontLayer)
            {
                renderer.sortingOrder = frontLayer;
            }
            if (currentAttachment == sideHandAttachmentRight && currentRenderLayer != sideLayer)
            {
                renderer.sortingOrder = sideLayer;
            }
            if (currentAttachment == backHandAttachmentRight && currentRenderLayer != backLayer)
            {
                renderer.sortingOrder = backLayer;
            }
        }
        
    }
}
