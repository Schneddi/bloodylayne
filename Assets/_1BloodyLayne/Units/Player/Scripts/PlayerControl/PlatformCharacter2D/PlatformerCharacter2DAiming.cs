using UI.PlayerUI;
using UnityEngine;

namespace Units.Player.Scripts.PlayerControl.PlatformCharacter2D
{
    public partial class PlatformerCharacter2D : MonoBehaviour
    {
        [SerializeField] private float timeTillAimStop;
        
        private void CheckAiming()
        {
            if (userControl.BlockInput) return;
            
            aimTimer += Time.deltaTime;

            if (dashAiming && aiming)
            {
                DashAim();
                return;
            }
            
            if (aiming)
            {
                aimTimer = 0;
                Aim();
            }
            
            if (!aiming)
            {
                if (aimTimer > timeTillAimStop && !aimingStopped && !firingChargedBloodmagic)
                {
                    StopAiming();
                }
                else if (!aimingStopped)
                {
                    Aim();
                }
            }
        }
        
        private void Aim()
        {
            aimingStopped = false;
            bool right;
            if (Mathf.Atan2(aimDirection.x, aimDirection.y) < 0)
            {
                //character is looking right
                right = true;
            }
            else
            {
                //character is looking left
                right = false;
            }

            if (right != m_FacingRight)
            {
                Flip();
            }

            currentAimDirection = Quaternion.LookRotation(Vector3.forward, aimDirection);
            Quaternion boneRotation = currentAimDirection;
                
            if (!m_FacingRight)
            {
                boneRotation = Quaternion.Inverse(boneRotation);
            }
                
            //Cursor.visible = true;
            if (!noAimingLock)
            {
                playerAimVisualisation.showLineRenderer();
                playerAimVisualisation.showCursorRenderer();
            }
            m_Anim.SetBool("Aiming", true);
            
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().LookIntoAimDirection(aimDirection);
                
            bone1.localRotation = boneRotation;
        }

        private void StopAiming()
        {
            noAimingLock = false;
            playerAimVisualisation.hideLineRenderer();
            aimingStopped = true;
            //Cursor.visible = false;
            playerAimVisualisation.hideCursorRenderer();
            m_Anim.SetBool("Aiming", false);
            transform.localRotation = Quaternion.identity;
            //Animator will reset Bone1 to default position
            
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().LookAheadAiming = false;
        }
        
        private void DashAim()
        {
            aimingStopped = false;
            bool right;
            if (Mathf.Atan2(aimDirection.x, aimDirection.y) < 0)
            {
                //character is looking right
                right = true;
            }
            else
            {
                //character is looking left
                right = false;
            }

            if (right != m_FacingRight)
            {
                Flip();
            }

            currentAimDirection = Quaternion.LookRotation(Vector3.forward, aimDirection);
            Quaternion boneRotation = currentAimDirection;
                
            float multiplier = 1;
            if(!m_FacingRight)
            {
                boneRotation = Quaternion.Inverse(boneRotation);
                multiplier = -1;
            }
            Vector3 rot = boneRotation.eulerAngles;
            float clampedZRot = Mathf.Clamp(rot.z - 90, -50, 50);
            rot = new Vector3(rot.x,rot.y,clampedZRot * multiplier);
            boneRotation = Quaternion.Euler(rot);
                
            //Cursor.visible = true;
            playerAimVisualisation.showCursorRenderer();
                
            transform.localRotation = boneRotation;
        }

    }
}
