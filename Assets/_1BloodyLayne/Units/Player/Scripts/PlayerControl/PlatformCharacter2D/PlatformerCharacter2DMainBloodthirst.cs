using System;
using System.Collections;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.Rendering;
using UnityStandardAssets._2D;
using Object = UnityEngine.Object;

namespace Units.Player.Scripts.PlayerControl.PlatformCharacter2D
{
    public partial class PlatformerCharacter2D : MonoBehaviour
    {
        public void ChangeBloodthirst(float change, Guid guid)
        {
            if (noBloodThirstUI || (!(guid == Guid.Empty) && registeredGUIDs.Contains(guid)) || stats.health <= 0 || dead)
            {
                return;
            }
            if ( (bloodlust > 0 && change < 0) || change > 0 && bloodlust < stats.maxBloodlust)
            {
                registeredGUIDs.Add(guid);
                bloodlust += change;
                if (bloodlust > stats.maxBloodlust)
                {
                    bloodlust = stats.maxBloodlust;
                }
                if (bloodlust <= 0)
                {
                    StartBloodLustDamage();
                }
                else
                {
                    StopBloodthirstDamage();
                }
            }
        }

        private void StartBloodLustDamage()
        {
            bloodlust = 0;
            if(bloodLossCounterCoroutine != null)
            {
                StopCoroutine(bloodLossCounterCoroutine);
            }
            bloodLossCounterCoroutine = StartCoroutine(BloodthirstDamageOverTime());
            ShowBloodLustEffects(true);
        }

        public void ShowBloodLustEffects(bool showUIEffect)
        {
            GameController.PlayerHealthUI.SetHeartRate(4);
            Instantiate((Object)bloodLustEffect, transform);
            if (showUIEffect)
            {
                bloodUI.bloodthirstStartEffect();
            }

            lastBloodlustActivationTime = Time.time;
        }

        private void UpdateBloodlustCamEffects()
        {
            if (lastBloodlustActivationTime > Time.time - lensDistortionAnimationTime && lastBloodlustActivationTime != 0)
            {
                float animationProgress = (Time.time - lastBloodlustActivationTime)/lensDistortionAnimationTime;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponentsInChildren<Volume>()[2].weight = animationProgress;
            }
            else if(lastBloodlustActivationTime > Time.time - lensDistortionAnimationTime * 2 && lastBloodlustActivationTime != 0)
            {
                float animationProgress = 2-((Time.time - lastBloodlustActivationTime)/lensDistortionAnimationTime);
                GameObject.FindGameObjectWithTag("MainCamera").GetComponentsInChildren<Volume>()[2].weight = animationProgress;
            }
            else
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponentsInChildren<Volume>()[2].weight = 0;
            }
            
            if (bloodlust <= 0 && GameObject.FindGameObjectWithTag("MainCamera").GetComponentsInChildren<Volume>()[0].weight >= 0)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponentsInChildren<Volume>()[0].weight -= Time.deltaTime / bloodLustColorChangeTime;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponentsInChildren<Volume>()[1].weight += Time.deltaTime / bloodLustColorChangeTime;
            }
            else if(bloodlust >= 0 && GameObject.FindGameObjectWithTag("MainCamera").GetComponentsInChildren<Volume>()[0].weight <= 1)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponentsInChildren<Volume>()[0].weight += Time.deltaTime / bloodLustColorChangeTime;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponentsInChildren<Volume>()[1].weight -= Time.deltaTime / bloodLustColorChangeTime;
            }
        }
        
        public void StopBloodthirstDamage()
        {
            try
            {
                GameController.PlayerHealthUI.ResetHeartRate();
            }
            catch (Exception e)
            {
                Debug.Log("Error referencing the playerUI: " + e);
            }
            if(bloodLossCounterCoroutine != null)
            {
                StopCoroutine(bloodLossCounterCoroutine);
                bloodLossCounterCoroutine = null;
            }
        }
        
        public void RestartBloodthirstDamage()
        {
            if (activeBloodthirst && bloodlust == 0)
            {
                StartBloodLustDamage();
            }
        }
        
        
        private IEnumerator BloodthirstDamageOverTime()
        {
            if (stats.health > 1)
            {
                stats.health -= 1;
            }
            yield return new WaitForSeconds(0.25f);
            if (stats.health == 1)
            {
                bloodlust = stats.maxBloodlust / 2;
                StopBloodthirstDamage();
            }
            else if (bloodLossCounterCoroutine != null)
            {
                bloodLossCounterCoroutine = StartCoroutine(BloodthirstDamageOverTime());
            }
        }
    }
}
