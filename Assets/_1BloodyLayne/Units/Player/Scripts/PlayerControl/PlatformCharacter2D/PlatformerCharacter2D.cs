﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityStandardAssets._2D;

namespace Units.Player.Scripts.PlayerControl.PlatformCharacter2D
{
    public partial class PlatformerCharacter2D : MonoBehaviour
    {
        public float k_GroundedRadius = .2145f; // Radius of the overlap circle to determine if grounded
        public float k_CeilingRadius = .21f; // Radius of the overlap circle to determine if the player can stand up
        public float bloodlust {private set; get; }
        public bool aimingStopped {private set; get; }
        public bool m_FacingRight = true;  // For determining which way the player is currently facing.
        public bool noStartSkillInit;
        public float crouchHigh = 0.55f; //how far does the character go into crouch
        
        [Foldout("Spawned Effects")]
        public GameObject bloodmagicExplosion, standardBullet, hitScanBloodmagic, bulletFar, bulletCharging, bloodmagicCharging, boomerangBloodmagic, biteBloodmagic, boomerangChargeEffect; //projectiles
        [Foldout("Spawned Effects")]
        public GameObject hitScanBloodmagicMuzzleFlash, beamBloodmagicMuzzleFlash, beamBloodmagic, empoweredBloodmagic7u1Effect, empoweredBloodmagicSkill11Effect, skill2u1RdyEffect, skill2u2RdyEffect, skill8u1RdyEffect, skill8u2RdyEffect;
        [Foldout("Spawned Effects")]
        public GameObject biteHitEffectBig, biteHitEffectSmall;
        [Foldout("Spawned Effects")]
        public GameObject jumpeffect, dashEffect1u1, dashEffect1u2, skill5u1HealEffect, bloodLustEffect, invulnerableHitEffect;
        [Foldout("Spawned Effects")]
        public GameObject deathExplosionSkill10u2;
        [Foldout("Spawned Effects")]
        public GameObject soulSkill9u1, soulSkill9u1HitEffect, soulSkill9u2, gasCloudSkill10u1, killingSpreeEffectSkill11u1, hateEffectSkill11u2, flyingRdyEffectSkill12u2, flyingWingsEffectSkill12u2, chargeBloodSkill12u2, skill13u1PushEffect, skill13u2DashEffect, skill13BloodmagicEffect, skill4u1HealEffect;
        
        public bool invulnerable { get; private set; }
        public bool activeBloodthirst, noBloodThirstUI;
        
        
        [Foldout("Skill Choices")]
        public bool skill0choice1, skill0choice2, skill1choice1, skill1choice2, skill2choice1, skill2choice2, skill3choice1, skill3choice2, skill4choice1, skill4choice2, skill5choice1, skill5choice2, skill6choice1, skill6choice2, skill7choice1, skill7choice2, skill8choice1, skill8choice2, skill9choice1, skill9choice2, skill10choice1, skill10choice2, skill11choice1, skill11choice2, skill12choice1, skill12choice2, skill13choice1, skill13choice2;
        
        [NonSerialized]public bool crouchIn, noAimingLock;            // Player is entering crouch.
        [NonSerialized]public bool onLadder, dead;
        [NonSerialized]public bool extraDamage7u1, skill8choice1Rdy, skill8choice2Rdy, skill9u1Spawned, skill9u2Spawned, skill12u1Loaded, skill12u2Loaded, firingChargedBloodmagic;
        public int skill11u1Stacks{ get; private set; }
        [NonSerialized]public int skill11u2Stacks;
        [NonSerialized]public Vector2 RespawnPosition;
        [NonSerialized]public bool overideGrounded, incomingBloodthirstHeal, lowHPBloodMagicOnCooldown;
        [NonSerialized]public bool overhealed, pushed;
        [NonSerialized]public GameObject LastRespawnPoint;
        [ReadOnly]public bool grounded, onIce;
        
        
        [SerializeField] private LayerMask m_WhatIsGround;   // A mask determining what is ground to the character

        [SerializeField] float slopeDetectionLength = Single.PositiveInfinity;
        [Range(0, 90)] [SerializeField]private float slopeMinAngle = 40;
        [Range(0, 90)] [SerializeField]private float slopeMaxAngle = 50;
        [SerializeField]private float maxNegativeDownSlopeYVelocity = -5;
        [SerializeField]private float downwardsSlopeFactor = -0.1f;
        [SerializeField]private float lensDistortionAnimationTime = 0.4f, bloodLustColorChangeTime = 0.8f;
        
        [ReadOnly] public Vector2 additionalPlatformVelocity;

        private Transform m_GroundCheck1, m_GroundCheck2, m_GroundCheck3, deathCloudEffect;    // A position marking where to check if the player is grounded.
        private Transform frontHandAttachmentRight, frontHandAttachmentLeft, sideHandAttachmentRight, backHandAttachmentRight, frontAnimation, sideAnimation, backAnimation;
        private bool m_wasGrounded, ungroundedJump;            // Whether or not the player is grounded.
        private bool justJumped; //bool which tells, if the player just jumped. Used to avoid unintentional doubleJumps
        private bool jumpCooldown ; //avaid doule Jumps (inconsistent)
        private bool ceilingIsFree;
        private bool aiming, dashAiming;
        private bool skill11u2DamageOnCooldown, castingSkill13,skill13BloodmagicPressed;
        private Transform m_CeilingCheck1, m_CeilingCheck2, m_CeilingCheck3, m_CeilingCheck4;   // A position marking where to check for ceilings
        private float oldXVelocity; //um den totalDuration schneller zu beschleunigen/abzubremsen
        private Animator m_Anim;            // Reference to the player's animator component.
        private Rigidbody2D m_Rigidbody2D;
        private BoxCollider2D bCollider;
        private GameObject flyingEffectChild, flyingWingsEffectChild, chargeBloodMagicEffectChild, skill4u1HealingEffectChild, soulSkill9u1Child, soulSkill9u2Child1, soulSkill9u2Child2, soulSkill9u2Child3, lastAttackEffect, lastEmpowerment7u1, lastEmpowerment11, spawnedAttackEffect, spawnedEmpowerment7u1, spawnedEmpowerment11, skill13ChargeEffect;
        private Transform firePos, rayFirePos, biteCheckPos1, biteCheckPos2, chargedBloodmagicFirePos, lastAttachmentPoint, bloodmagicExplosionPos;
        private DamageUIScript damageUIScript;
        private PlayerStats stats;
        private Platformer2DUserControl userControl;
        private BloodUI bloodUI;

        private AudioSource[] sounds;
        private AudioSource jumpSound;
        private AudioSource blutmagieCharge, blutmagieChargeEmpowered, bloodmagicFarSound, bloodmagicBeamChargeSound, bloodmagicExplosionChargeSound, bloodmagicBoomerangSound, bloodmagicBiteSound;
        private AudioSource biteSound;
        private AudioSource runSound;
        private AudioSource crawlSound;
        private AudioSource blutmagieChargedSound;
        private AudioSource ladderClimpLoopSound;
        private AudioSource skill13u1PushSound, skill13u1BloodWaveSound;
        private AudioSource respawnSmokeSound;
        private AudioSource respawnBatFlyLoop;
        private AudioSource normalDeathSound;
        private AudioSource airDashOffCooldownSound;
        private AudioSource rangedBiteOffCooldownSound;
        private AudioSource groundDashOffCooldownSound;
        private AudioSource bloodmagicCancelSound;

        private float climbVelocity, aimTimer, lastBloodlustActivationTime;
        private float startGravity, pushedTime, invulnerableInitTime, invulnerableInitLength, skill13TotalAttackDamage;
        private float passedBatFlightTime, batFlightDuration;
        private Vector2 respawnStartPoint, respawnEndPoint, aimDirection;

        private bool biteDashed, gasCloudOnCooldown, flyingSkill12u1Active, firingBloodmagic, skill2choice1Rdy, skill2choice2Rdy,skill13choice1Rdy, skill13choice2Rdy, skill13u2Pressed, batform;
        private bool[] pausedObjects;
        private Coroutine healCooldownCoroutineRef, healingCoroutineRef, pushedTimer, invulnerableEffectCoroutine, invulnerableCoroutine, bloodLossCounterCoroutine, jumpTimerCoroutine;
        private GlobalVariables gv;
        private Transform bone1;
        private Quaternion currentAimDirection;
        private PlayerAimVisualisation playerAimVisualisation;
        private Dictionary<GameObject, float> lastInvulEffectTimeTable;
        private List<System.Guid> registeredGUIDs;
        private SlopeState slopeState;

        private void Start()
        {
            // Setting up references.
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            m_GroundCheck1 = transform.Find("GroundCheck1");
            m_GroundCheck2 = transform.Find("GroundCheck2");
            m_GroundCheck3 = transform.Find("GroundCheck3");
            m_CeilingCheck1 = transform.Find("CeilingCheck1");
            m_CeilingCheck2 = transform.Find("CeilingCheck2");
            m_CeilingCheck3 = transform.Find("CeilingCheck3");
            m_CeilingCheck4 = transform.Find("CeilingCheck4");
            deathCloudEffect = transform.Find("DeathCloudEffect");
            //fastest search for attachments, but prone to changes in path
            frontHandAttachmentRight = transform.Find("Front Animation").Find("bone_1").Find("bone_2").Find("bone_3").Find("bone_8").Find("bone_18").Find("bone_9").Find("FrontHandAttachmentRight");
            sideHandAttachmentRight = transform.Find("Side Animation").Find("bone_1").Find("bone_2").Find("bone_3").Find("bone_4").Find("bone_12").Find("bone_13").Find("SideHandAttachmentRight");
            backHandAttachmentRight = transform.Find("Back Animation").Find("bone_1").Find("bone_2").Find("bone_3").Find("bone_11").Find("bone_12").Find("bone_14").Find("BackHandAttachmentRight");
            frontHandAttachmentLeft = transform.Find("Front Animation").Find("bone_1").Find("bone_2").Find("bone_3").Find("bone_10").Find("bone_17").Find("bone_11").Find("FrontHandAttachmentLeft");
            frontAnimation = transform.Find("Front Animation");
            sideAnimation = transform.Find("Side Animation");
            backAnimation = transform.Find("Back Animation");
            bone1 = transform.Find("Side Animation/bone_1");
            m_Anim = GetComponent<Animator>();
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
            bCollider = GetComponent<BoxCollider2D>();
            firePos = transform.Find("Side Animation/bone_1/BulletSpawn");
            rayFirePos = transform.Find("Side Animation/bone_1/RaySpawn");
            biteCheckPos1 = transform.Find("BiteCheck1");
            biteCheckPos2 = transform.Find("BiteCheck2");
            chargedBloodmagicFirePos = transform.Find("Side Animation/bone_1/ChargedSpawn");
            bloodmagicExplosionPos = transform.Find("Side Animation/bone_1/BloodmagicExplosionSpawn");
            damageUIScript = GetComponent<DamageUIScript>();
            userControl = GetComponent<Platformer2DUserControl>();
            sounds = GetComponents<AudioSource>();     
            stats = GetComponent<PlayerStats>();
            playerAimVisualisation = GetComponent<PlayerAimVisualisation>();
            aimingStopped = true;
            jumpSound = sounds[0];
            blutmagieCharge = sounds[1];
            runSound = sounds[3];
            crawlSound = sounds[4];
            blutmagieChargedSound = sounds[5];
            ladderClimpLoopSound = sounds[6];
            skill13u1PushSound = sounds[7];
            if (gv.gender == 0)
            {
                biteSound = transform.Find("Sounds").Find("MaleSounds").GetComponents<AudioSource>()[0];
                normalDeathSound = transform.Find("Sounds").Find("MaleSounds").GetComponents<AudioSource>()[1];
            }

            if (gv.gender == 1)
            {
                biteSound = transform.Find("Sounds").Find("FemaleSounds").GetComponents<AudioSource>()[0];
                normalDeathSound = transform.Find("Sounds").Find("FemaleSounds").GetComponents<AudioSource>()[1];
            }
            pausedObjects = new bool[11];
            respawnSmokeSound = transform.Find("Sounds").GetComponents<AudioSource>()[0];
            respawnBatFlyLoop = transform.Find("Sounds").GetComponents<AudioSource>()[1];
            bloodmagicFarSound = transform.Find("Sounds").GetComponents<AudioSource>()[2];
            bloodmagicExplosionChargeSound = transform.Find("Sounds").GetComponents<AudioSource>()[3];
            bloodmagicBoomerangSound = transform.Find("Sounds").GetComponents<AudioSource>()[4];
            bloodmagicBiteSound = transform.Find("Sounds").GetComponents<AudioSource>()[5];
            skill13u1BloodWaveSound = transform.Find("Sounds").GetComponents<AudioSource>()[6];
            blutmagieChargeEmpowered = transform.Find("Sounds").GetComponents<AudioSource>()[7];
            airDashOffCooldownSound = transform.Find("Sounds").GetComponents<AudioSource>()[8];
            rangedBiteOffCooldownSound = transform.Find("Sounds").GetComponents<AudioSource>()[9];
            groundDashOffCooldownSound = transform.Find("Sounds").GetComponents<AudioSource>()[10];
            bloodmagicBeamChargeSound = transform.Find("Sounds").GetComponents<AudioSource>()[11];
            bloodmagicCancelSound = transform.Find("Sounds").GetComponents<AudioSource>()[12];
            if (!noBloodThirstUI)
            {
                bloodUI = GameObject.Find("BloodUI").GetComponent<BloodUI>();
            }

            RespawnPosition = transform.position;

            startGravity = m_Rigidbody2D.gravityScale;
            lastInvulEffectTimeTable = new Dictionary<GameObject, float>();
            if (!m_FacingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
            }
            registeredGUIDs = new List<System.Guid>();
            bloodlust = stats.maxBloodlust;
            
            if (GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().scene == 0)
            {
                InitializeSkills(false); // this is only needed for testing. In gameplay it is called by the gameController after setting skills
            }
        }

        private void FixedUpdate()
        {
            CheckDeath();
            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            // This can be done using layers instead but Sample Assets will not overwrite your project settings.
            
            if (m_Rigidbody2D.velocity.y > stats.maxJumpSpeed)
            {
                m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, stats.maxJumpSpeed);
            }
            
            if (Physics2D.OverlapCircle(m_CeilingCheck1.position, k_CeilingRadius, m_WhatIsGround) ||
                Physics2D.OverlapCircle(m_CeilingCheck2.position, k_CeilingRadius, m_WhatIsGround) ||
                Physics2D.OverlapCircle(m_CeilingCheck3.position, k_CeilingRadius, m_WhatIsGround) ||
                Physics2D.OverlapCircle(m_CeilingCheck4.position, k_CeilingRadius, m_WhatIsGround))
            {
                ceilingIsFree = false;
            }
            else
            {
                ceilingIsFree = true;
            }

            // Set the vertical animation
            m_Anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);
            if (stats.health > stats.maxHealth && !skill7choice2)
            {
                stats.health = stats.maxHealth;
            }
            
            CheckSoundLoopStates();
        }

        private void Update()
        {
            if (!crouchIn || dashAiming)
            {
                CheckAiming();
            }

            ShowPreparedAttachments();
            if (batform)
            {
                passedBatFlightTime += Time.deltaTime;
                float step = passedBatFlightTime / batFlightDuration;
                transform.position = Vector2.Lerp(respawnStartPoint,respawnEndPoint, step) ; 
                if (Vector2.Distance(transform.position, respawnEndPoint) < 0.05f)
                {
                    StartCoroutine((IEnumerator)EndBatTransform());
                }
            }

            UpdateBloodlustCamEffects();
        }

        public void EvaluateInput(Vector2 move, bool crouch, bool jump, bool bloodmagic, bool bite, bool epic, Vector2 aimDirection)
        {
            if (aimDirection != Vector2.zero)
            {
                this.aimDirection = aimDirection;
                aiming = true;
            }
            else
            {
                aiming = false;
            }
            
            GroundCheck();
            DetectAndHandleSlopeState();
            
            if (grounded && !justJumped)
            {
                _dashCounter = 0;
                skill13choice1Rdy = false;
                skill13choice2Rdy = false;
            }
            
            if (bloodmagic && !firingBloodmagic && (stats.health > 5 || !lowHPBloodMagicOnCooldown || (skill5choice2 && bloodlust <= 0) ))
            {
                if (!m_Anim.GetBool("Crouch") && !castingSkill13 && (gv.skill0Known || gv.skill2Known && (skill2choice1Rdy || skill2choice2Rdy) || gv.skill8Known && (skill8choice1Rdy || skill8choice2Rdy)))
                {
                    StartCoroutine(Fire(0));
                    if (stats.health <= 5 && !lowHPBloodMagicOnCooldown)
                    {
                        StartCoroutine(LowHPBloodmagicCooldown());
                    }
                }
                else if (castingSkill13 && !skill13BloodmagicPressed)
                {
                    skill13BloodmagicPressed = true;
                    if (bloodlust > 0)
                    {
                        blutmagieCharge.PlayDelayed(0);
                    }
                    else
                    {
                        blutmagieChargeEmpowered.PlayDelayed(0);
                    }
                    if (skill13ChargeEffect != null)
                    {
                        Destroy(skill13ChargeEffect);
                    }
                    skill13ChargeEffect = Instantiate(bloodmagicCharging, frontHandAttachmentLeft, false);
                    skill13ChargeEffect.transform.position = frontHandAttachmentLeft.position;
                    skill13ChargeEffect.transform.localScale = new Vector3(3, 3, 3);
                    skill13ChargeEffect.GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder = 80;
                    StartCoroutine(DestroySkill13BloodmagicChargeEffect());
                }
            }
            else if (bloodmagic)
            {
                bloodmagicCancelSound.PlayOneShot(bloodmagicCancelSound.clip);
            }

            if (!epic && firingChargedBloodmagic)
            {
                firingChargedBloodmagic = false;
            }
            else if (epic && (skill12u1Loaded || skill12u2Loaded) && !castingSkill13)
            {
                if (skill12u1Loaded && !flyingSkill12u1Active)
                {
                    skill12u1Loaded = false;
                    Destroy(flyingEffectChild);
                    m_Rigidbody2D.gravityScale = 1f;
                    StartCoroutine(Flying());
                    flyingWingsEffectChild = Instantiate(flyingWingsEffectSkill12u2, new Vector3(transform.position.x, transform.position.y+0.1f, transform.position.z), Quaternion.identity);
                    flyingWingsEffectChild.transform.parent = gameObject.transform;
                }
                else if (skill12u2Loaded)
                {
                    skill12u2Loaded = false;
                    Destroy(chargeBloodMagicEffectChild);
                    firingChargedBloodmagic = true;
                    StartCoroutine(Fire(1));
                }
            }

            if (bite && !m_Anim.GetBool("Crouch") && !firingBloodmagic)
            {
                if (bloodlust == 0)
                {
                    if (_rangedBiteCooldownCoroutine == null)
                    {
                        StartCoroutine(Fire(2));
                        if (skill6choice1)
                        {
                            StartCoroutine(Biting());
                        }
                    }
                    else
                    {
                        rangedBiteOffCooldownSound.PlayOneShot(rangedBiteOffCooldownSound.clip);
                    }
                }
                else if (bloodlust > 0)
                {
                    StartCoroutine(Biting());
                }
            }

            //crouch aber nicht grounded
            if (!crouch && !bloodmagic && !skill13BloodmagicPressed)
            {
                skill13u2Pressed = false;
            }
            else if (crouch && !grounded)
            {
                if (skill13choice1 && skill13choice1Rdy)
                {
                    castingSkill13 = true;
                    StartCoroutine(Skill13u1());
                }
                else if(skill13choice2 && skill13choice2Rdy && !skill13u2Pressed)
                {
                    castingSkill13 = true;
                    skill13u2Pressed = true;
                    StartCoroutine(Skill13u2());
                }
                else
                {
                    crouch = true;
                    bloodmagic = false;
                    bite = false;
                    if (!ungroundedJump)
                    {
                        jump = false;
                    }
                }
            }

            if (skill13u2Pressed)
            {
                crouch = false;
            }
            
            //crouch aber nicht grounded
            if (crouch && jump && !grounded)
            {
                // If the character has a ceiling preventing them from standing up, keep them crouching + no jumping
                if (!ceilingIsFree)
                {
                    crouch = true;
                    jump = false;
                }
            }

            // If crouching, check to see if the character can stand up
            if (!crouch && m_Anim.GetBool("Crouch"))
            {
                // If the character has a ceiling preventing them from standing up, keep them crouching + no jumping
                if (!ceilingIsFree)
                {
                    crouch = true;
                    jump = false;
                }
            }

            if (crouch && onLadder)
            {
                crouch = false;
            }

            // Set whether or not the character is crouching in the animator
            m_Anim.SetBool("Crouch", crouch);
            if (crouchIn == false && (crouch || skill13u2Pressed))//in den crouch gehen
            {
                GoIntoCrouch();
            }
            else if (crouchIn && !crouch && !skill13u2Pressed)//aus dem crouch gehen
            {
                StandUp();
            }

            // Reduce the totalDuration if crouching by the crouchSpeed multiplier
            move = (crouch ? move * stats.m_CrouchSpeed : move);

            // The Speed animator parameter is set to the absolute value of the horizontal input.
            if (Math.Abs(move.x) > 0)
            {
                m_Anim.SetFloat("Speed", Math.Abs(m_Rigidbody2D.velocity.x));
            }
            else
            {
                m_Anim.SetFloat("Speed", 0);
            }

            // If the input is moving the player right and the player is facing left...
            if (!pushed && !firingBloodmagic && !firingChargedBloodmagic && aimingStopped)
            {
                if (move.x > 0 && !m_FacingRight)
                {
                    Flip();
                }
                else if (move.x < 0 && m_FacingRight)
                {
                    Flip();
                }
            }

            // Move the character
            float xVelocity = 5 * move.x + m_Rigidbody2D.velocity.x + additionalPlatformVelocity.x;
            m_Rigidbody2D.drag = 0;
            if (!pushed && Mathf.Abs(move.x) > 0 && !skill13u2Pressed)
            {
                if (Mathf.Abs(m_Rigidbody2D.velocity.x * 0.9f) > stats.m_MaxSpeed + additionalPlatformVelocity.x)
                {
                    m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x * 0.9f * Time.deltaTime * 50, m_Rigidbody2D.velocity.y);
                }
                else if (Mathf.Abs(xVelocity) > stats.m_MaxSpeed + additionalPlatformVelocity.x && !firingBloodmagic && aimingStopped)
                {
                    if (m_FacingRight)
                    {
                        m_Rigidbody2D.velocity = new Vector2(stats.m_MaxSpeed + additionalPlatformVelocity.x, m_Rigidbody2D.velocity.y);
                    }
                    else
                    {
                        m_Rigidbody2D.velocity = new Vector2(-stats.m_MaxSpeed - additionalPlatformVelocity.x, m_Rigidbody2D.velocity.y);
                    }
                }
                else
                {
                    m_Rigidbody2D.velocity = new Vector2(xVelocity, m_Rigidbody2D.velocity.y);
                }
            }
            else if (!pushed && Mathf.Abs(move.x) == 0 && !biteDashed && !skill13u2Pressed && !onIce)
            {
                if (Math.Abs(oldXVelocity) >= Math.Abs(xVelocity) && Math.Abs(xVelocity) > 0.1f)
                {
                    m_Rigidbody2D.velocity = new Vector2(0.015f * (xVelocity - additionalPlatformVelocity.x) + additionalPlatformVelocity.x, m_Rigidbody2D.velocity.y);
                    if (slopeState != SlopeState.NotOnSlope && grounded)
                    {
                        m_Rigidbody2D.drag = 1000;
                    }
                }
                else
                {
                    m_Rigidbody2D.velocity = new Vector2(0 + additionalPlatformVelocity.x, m_Rigidbody2D.velocity.y);
                    if (m_Rigidbody2D.velocity.y > 0 && !jump && !jumpCooldown && !onLadder && additionalPlatformVelocity != Vector2.zero)
                    {
                        m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, 0.015f * m_Rigidbody2D.velocity.y);
                    }

                    if (slopeState != SlopeState.NotOnSlope && grounded)
                    {
                        m_Rigidbody2D.drag = 10000;
                    }
                }
            }
            m_Anim.SetBool("BackwardsWalk", false);
            if (Math.Abs(move.x) > 0)
            {
                m_Anim.SetFloat("RunSpeed", Math.Clamp(Math.Abs(m_Rigidbody2D.velocity.x) * 0.5f ,0,1));
                if ((firingBloodmagic || !aimingStopped) && Mathf.Abs(m_Rigidbody2D.velocity.x) >= 0.01f)
                {
                    if (m_FacingRight && m_Rigidbody2D.velocity.x < 0)
                    {
                        m_Anim.SetBool("BackwardsWalk", true);
                    }
                    else if(!m_FacingRight && m_Rigidbody2D.velocity.x > 0)
                    {
                        m_Anim.SetBool("BackwardsWalk", true);
                    }
                }
            }
            oldXVelocity = xVelocity;

            // If the player should jump...
            if ((ungroundedJump || grounded) && jump && !justJumped && !jumpCooldown && !onLadder)
            {
                // Add a vertical force to the player.
                grounded = false;
                m_Rigidbody2D.drag = 0;
                if (jumpTimerCoroutine != null)
                {
                    StopCoroutine(jumpTimerCoroutine);
                }
                jumpTimerCoroutine = StartCoroutine(UngroundedJumpTimer());
                m_Anim.SetBool("Ground", false);
                m_Anim.SetBool("Jumping", true);
                skill13choice1Rdy = true;
                skill13choice2Rdy = true;

                if (m_Rigidbody2D.velocity.y < 5f)
                {
                    m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, stats.m_JumpForce);
                }
                else if (stats.maxJumpSpeed > m_Rigidbody2D.velocity.y + stats.m_JumpForce)
                {
                    m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, m_Rigidbody2D.velocity.y + stats.m_JumpForce);
                }
                else
                {
                    m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, stats.maxJumpSpeed);
                }

                jumpSound.PlayDelayed(0);
                Instantiate(jumpeffect, new Vector3(m_GroundCheck3.position.x, m_GroundCheck3.position.y + 0.15f, m_GroundCheck3.position.z), Quaternion.identity);
                justJumped = true;
                StartCoroutine(DoubleJumpCooldown());
            }
            else if (grounded && justJumped && !jumpCooldown && !castingSkill13)
            {
                justJumped = false;
                m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x * 0.01f, m_Rigidbody2D.velocity.y * 0.01f);
            }
            else if (!grounded && !jump && !m_Anim.GetBool("Ground") && justJumped && !jumpCooldown && !pushed && !onLadder && !castingSkill13)
            {
                if (m_Rigidbody2D.velocity.y > 0)
                {
                    justJumped = false;
                    m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, m_Rigidbody2D.velocity.y / 8f);
                }
            }
            else if (Math.Abs(additionalPlatformVelocity.y) > 0)
            {
                if (Math.Abs(m_Rigidbody2D.velocity.y) < Math.Abs(additionalPlatformVelocity.y))
                {
                    m_Rigidbody2D.velocity = new (m_Rigidbody2D.velocity.x, additionalPlatformVelocity.y);
                }
            }

            if (flyingSkill12u1Active)
            {
                float flyVelocity = stats.flySpeed * move.y;
                float endYVelocity = flyVelocity + m_Rigidbody2D.velocity.y;
                if (Mathf.Abs(endYVelocity) >= stats.m_MaxSpeed)
                {
                    endYVelocity =  Mathf.Sign(endYVelocity) * stats.m_MaxSpeed;
                }
                m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, endYVelocity);
            }
            
            //Ladder Part
            if (onLadder && !m_Anim.GetBool("Death") && !flyingSkill12u1Active)
            {
                m_Rigidbody2D.gravityScale = 0f;
                m_Anim.SetBool("OnLadder", true);

                climbVelocity = m_Rigidbody2D.velocity.y + stats.climbSpeed * move.y;
                if (climbVelocity != 0)
                {
                    if (!m_Anim.GetBool("Climbing"))
                    {
                        PlayLadderLoop();
                        m_Anim.speed = 1;
                        m_Anim.SetTrigger("Climb");
                        m_Anim.SetBool("Climbing", true);
                    }
                    else
                    {
                        PlayLadderLoop();
                        m_Anim.speed = 1;
                        m_Anim.SetBool("Climbing", true);
                    }
                    if (move.y == 0)
                    {
                        climbVelocity = 0f;
                    }
                    else if ((move.y > 0 && climbVelocity < 0) || (move.y < 0 && climbVelocity > 0))
                    {
                        climbVelocity = 0f;
                    }
                    if (Mathf.Abs(climbVelocity) >= stats.m_MaxSpeed)
                    {
                        climbVelocity = Mathf.Sign(climbVelocity) * stats.m_MaxSpeed;
                    }

                }
                else
                {
                    StopLadderLoop();
                    if (onLadder && m_Anim.GetCurrentAnimatorStateInfo(0).IsTag("ClimbLadder"))
                    {
                        m_Anim.speed = 0;
                    }
                    else
                    {
                        m_Anim.speed = 1;
                        m_Anim.SetBool("Climbing", false);
                    }
                }

                m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, climbVelocity);
            }
            else if (!onLadder)
            {
                if (!biteDashed && !flyingSkill12u1Active)
                {
                    m_Rigidbody2D.gravityScale = startGravity;
                }
                m_Anim.SetBool("Climbing", false);
                m_Anim.SetBool("OnLadder", false);
                m_Anim.speed = 1;
                StopLadderLoop();

            }
            
            if (!overideGrounded)
            {
                if (!jump)
                {
                    m_Anim.SetBool("Ground", grounded);
                }
                else
                {
                    m_Anim.SetBool("Ground", false);
                }
                if (grounded && !jumpCooldown)
                {
                    m_Anim.SetBool("Jumping", false);
                }
            }
            else
            {
                m_Anim.SetBool("Jumping", false);
                m_Anim.SetBool("Ground", true);
            }
        }

        public void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
            if (!pushed && ((!m_FacingRight && m_Rigidbody2D.velocity.x > 0) || (m_FacingRight && m_Rigidbody2D.velocity.x < 0)) && aimingStopped)
            {
                m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x * -1, m_Rigidbody2D.velocity.y);
            }
        }
        
        public void CheckSoundLoopStates()
        {
            if (m_Anim.GetCurrentAnimatorStateInfo(0).IsTag("Run") && Time.timeScale != 0 && !runSound.isPlaying)
            {
                runSound.PlayDelayed(0);
            }
            else if ((!m_Anim.GetCurrentAnimatorStateInfo(0).IsTag("Run") || Time.timeScale == 0) && runSound.isPlaying)
            {
                runSound.Stop();
            }

            if (m_Anim.GetCurrentAnimatorStateInfo(0).IsTag("Crawl") && Time.timeScale != 0 && !crawlSound.isPlaying)
            {
                crawlSound.PlayDelayed(0.15f);
            }
            else if ((!m_Anim.GetCurrentAnimatorStateInfo(0).IsTag("Crawl") || Time.timeScale == 0) && crawlSound.isPlaying)
            {
                crawlSound.Stop();
            }
        }
        
        private void GroundCheck()
        {
            Collider2D[] colliders1 = Physics2D.OverlapCircleAll(m_GroundCheck1.position, 0.075f, m_WhatIsGround);
            Collider2D[] colliders2 = Physics2D.OverlapCircleAll(m_GroundCheck2.position, 0.075f, m_WhatIsGround);
            Collider2D[] colliders3 = Physics2D.OverlapCircleAll(m_GroundCheck3.position, k_GroundedRadius, m_WhatIsGround);

            bool col1Grounded = false;
            bool col2Grounded = false;
            bool col3Grounded = false;
            for (int i = 0; i < colliders1.Length; i++)
            {
                if (colliders1[i].gameObject != gameObject)
                {
                    col1Grounded = true;
                }
            }
            for (int i = 0; i < colliders2.Length; i++)
            {
                if (colliders2[i].gameObject != gameObject)
                {
                    col2Grounded = true;
                }
            }
            for (int i = 0; i < colliders3.Length; i++)
            {
                if (colliders3[i].gameObject != gameObject)
                {
                    col3Grounded = true;
                }
            }
            
            bool isGroundedCheck = col1Grounded || col2Grounded || col3Grounded;

            if (grounded && !isGroundedCheck)
            {
                if (jumpTimerCoroutine != null)
                {
                    StopCoroutine(jumpTimerCoroutine);
                }
                jumpTimerCoroutine = StartCoroutine(UngroundedJumpTimer());
            }

            grounded = isGroundedCheck;
        }
        
        IEnumerator UngroundedJumpTimer()
        {
            ungroundedJump = true;
            yield return new WaitForSeconds(0.075f);
            ungroundedJump = false;
        }
        
        IEnumerator DoubleJumpCooldown()
        {
            jumpCooldown = true;
            yield return new WaitForSeconds(0.1f);
            m_Anim.SetBool("Jumping", false);
            jumpCooldown = false;
        }

        public void InitiatePushedTimer(float seconds)
        {
            if (pushedTimer != null)
            {
                if(seconds > pushedTime) //start new makeInvul, if old Timer would have lower value
                {
                    StopCoroutine(pushedTimer);
                    pushedTime = seconds;
                    pushedTimer = StartCoroutine(PushedCounter(seconds));
                }
            }
            else
            {
                pushedTime = seconds;
                pushedTimer = StartCoroutine(PushedCounter(seconds));
            }
        }
        
        private IEnumerator PushedCounter(float seconds)
        {
            m_Rigidbody2D.drag = 0;
            pushed = true;
            while (pushedTime > 0)
            {
                pushedTime -= Time.deltaTime; //we where never changing the firetime
                yield return null;
            }
            pushed = false;
        }
        
        public void Restart()
        {
			SceneManager.LoadScene(SceneManager.GetActiveScene().handle);
        }
        
        private void GoIntoCrouch()
        {
            bCollider.offset = new Vector2(bCollider.offset.x, bCollider.offset.y - crouchHigh/2);
            bCollider.size = new Vector2(bCollider.size.x, bCollider.size.y - crouchHigh);
            crouchIn = true;
            if (flyingEffectChild != null)
            {
                flyingEffectChild.transform.position = new Vector2(flyingEffectChild.transform.position.x, transform.position.y - crouchHigh/5);
            }
            if (chargeBloodMagicEffectChild != null)
            {
                chargeBloodMagicEffectChild.transform.position = new Vector2(chargeBloodMagicEffectChild.transform.position.x, transform.position.y - crouchHigh/5);
            }
        }

        public void StandUp()
        {
            bCollider.offset = new Vector2(bCollider.offset.x, bCollider.offset.y + (float)crouchHigh/2);
            bCollider.size = new Vector2(bCollider.size.x, bCollider.size.y + crouchHigh);
            crouchIn = false;
            if (flyingEffectChild != null)
            {
                flyingEffectChild.transform.position = new Vector2(flyingEffectChild.transform.position.x, flyingEffectChild.transform.position.y + crouchHigh/5); 
            }
            if (chargeBloodMagicEffectChild != null)
            {
                chargeBloodMagicEffectChild.transform.position = new Vector2(chargeBloodMagicEffectChild.transform.position.x, chargeBloodMagicEffectChild.transform.position.y + crouchHigh/5);
            }
        }

        public void PlayLadderLoop()
        {
            if (!ladderClimpLoopSound.isPlaying)
            {
                ladderClimpLoopSound.PlayDelayed(0);
            }
        }

        public void StopLadderLoop()
        {
            ladderClimpLoopSound.Stop();
        }

        public void Pause()
        {
            if (runSound.isPlaying)
            {
                pausedObjects[0] = true;
                runSound.Stop();
            }
            if (crawlSound.isPlaying)
            {
                pausedObjects[1] = true;
                crawlSound.Stop();
            }
            if (ladderClimpLoopSound.isPlaying)
            {
                pausedObjects[2] = true;
                ladderClimpLoopSound.Stop();
            }
            if(flyingEffectChild != null)
            {
                pausedObjects[3] = true;
                flyingEffectChild.GetComponent<AudioSource>().Stop();
            }
            if (chargeBloodMagicEffectChild != null)
            {
                pausedObjects[4] = true;
                chargeBloodMagicEffectChild.GetComponent<AudioSource>().Stop();
            }
            if (flyingWingsEffectChild != null)
            {
                pausedObjects[6] = true;
                flyingWingsEffectChild.GetComponent<AudioSource>().Stop();
            }
            if (soulSkill9u1Child != null)
            {
                pausedObjects[7] = true;
                soulSkill9u1Child.GetComponent<AudioSource>().Stop();
            }
            if (soulSkill9u2Child1 != null)
            {
                pausedObjects[8] = true;
                soulSkill9u2Child1.GetComponent<AudioSource>().Stop();
            }
            if (soulSkill9u2Child2 != null)
            {
                pausedObjects[9] = true;
                soulSkill9u2Child2.GetComponent<AudioSource>().Stop();
            }
            if (soulSkill9u2Child3 != null)
            {
                pausedObjects[10] = true;
                soulSkill9u2Child3.GetComponent<AudioSource>().Stop();
            }
        }

        public void Resume()
        {
            if (pausedObjects[0])
            {
                pausedObjects[0] = false;
                runSound.PlayDelayed(0);
            }
            if (pausedObjects[1])
            {
                pausedObjects[1] = false;
                crawlSound.PlayDelayed(0);
            }
            if (pausedObjects[2])
            {
                pausedObjects[2] = false;
                ladderClimpLoopSound.PlayDelayed(0);
            }
            if (pausedObjects[3])
            {
                pausedObjects[3] = false;
                flyingEffectChild.GetComponent<AudioSource>().PlayDelayed(0);
            }
            if (pausedObjects[4])
            {
                pausedObjects[4] = false;
                chargeBloodMagicEffectChild.GetComponent<AudioSource>().PlayDelayed(0);
            }
            if (pausedObjects[5])
            {
                pausedObjects[5] = false;
            }
            if (pausedObjects[6])
            {
                pausedObjects[6] = false;
                flyingWingsEffectChild.GetComponent<AudioSource>().PlayDelayed(0);
            }
            if (pausedObjects[7])
            {
                pausedObjects[7] = false;
                soulSkill9u1Child.GetComponent<AudioSource>().PlayDelayed(0);
            }
            if (pausedObjects[8])
            {
                pausedObjects[8] = false;
                soulSkill9u2Child1.GetComponent<AudioSource>().PlayDelayed(0);
            }
            if (pausedObjects[9])
            {
                pausedObjects[9] = false;
                soulSkill9u2Child2.GetComponent<AudioSource>().PlayDelayed(0);
            }
            if (pausedObjects[10])
            {
                pausedObjects[10] = false;
                soulSkill9u2Child3.GetComponent<AudioSource>().PlayDelayed(0);
            }
        }

        public void InitializeHuman(int level)
        {
            userControl.humanMode = true;
            stats.human = true;
            stats.health = 100;
            stats.maxHealth = 100;
            stats.lifes = 0;
            skill12choice1 = false;
            skill12choice2 = false;
            if(flyingWingsEffectChild != null)
            {
                Destroy(flyingWingsEffectChild);
            }
            if(chargeBloodSkill12u2 != null)
            {
                Destroy(chargeBloodMagicEffectChild);
            }
            skill12u1Loaded = false;
            skill12u2Loaded = false;
            if (level == 2)
            {
                skill10choice1 = false;
                skill10choice2 = false;
                skill11choice1 = false;
                skill11choice2 = false;
            }
        }
    
        private void DetectAndHandleSlopeState()
        {
            if (justJumped || pushed)
            {
                m_wasGrounded = false;
                slopeState = SlopeState.NotOnSlope;
                return;
            }

            if (onLadder)
            {
                slopeState = SlopeState.NotOnSlope;
                return;
            }
            
            //cherck if some ground is underneath the character
            List<RaycastHit2D> results = new List<RaycastHit2D>();
            ContactFilter2D contactFilter2D = new ContactFilter2D();
            contactFilter2D.layerMask = 1 << 8;
            m_Rigidbody2D.Cast(Vector2.down, contactFilter2D, results, slopeDetectionLength);
            if (results.Count == 0)
            {
                //nothing underneath the character = do nothing
                slopeState = SlopeState.NotOnSlope;
                m_wasGrounded = false;
                return;
            }
            
            //check the closest target it
            float shortestDistance = slopeDetectionLength + 1;
            RaycastHit2D shortestResult = results[0];
            foreach (RaycastHit2D result in results)
            {
                float newShortestDistance = transform.position.y - result.point.y;
                if (newShortestDistance < shortestDistance)
                {
                    shortestDistance = newShortestDistance;
                    shortestResult = result;
                }
            }
                
            float slopeAngle = Vector2.SignedAngle(Vector2.up, shortestResult.normal);
            
            //categorize the ground hit angle to a slopeState
            if (Mathf.Abs(slopeAngle) <= 1)
            {
                //prob flat ground
                slopeState = SlopeState.NotOnSlope;
            }
            else if (slopeMinAngle < Mathf.Abs(slopeAngle) && Mathf.Abs(slopeAngle) < slopeMaxAngle)
            {
                if ((Math.Sign(slopeAngle) == -1 && Math.Sign(m_Rigidbody2D.velocity.x) == 1) || (Math.Sign(slopeAngle) == 1 && Math.Sign(m_Rigidbody2D.velocity.x) == -1))
                {
                    slopeState = SlopeState.DownwardsSlope;
                }
                else if ((Math.Sign(slopeAngle) == -1 && Math.Sign(m_Rigidbody2D.velocity.x) == -1) || (Math.Sign(slopeAngle) == 1 && Math.Sign(m_Rigidbody2D.velocity.x) == 1))
                {
                    slopeState = SlopeState.UpwardsSlope;
                }
            }
            else
            {
                //not flat ground and ground angle is high, should be some kind of cliff
                m_wasGrounded = false;
                slopeState = SlopeState.NotOnSlope;
                return;
            }
            
            //we should only reach this code, if the character is supposed top stick the ground
            if (!grounded && m_wasGrounded)
            {
                grounded = true;
                m_wasGrounded = grounded;

                switch (slopeState)
                {
                    case SlopeState.DownwardsSlope:
                        if (m_Rigidbody2D.velocity.y > maxNegativeDownSlopeYVelocity)
                        {
                            float newYVelocity = m_Rigidbody2D.velocity.y + downwardsSlopeFactor * Time.deltaTime;
                            if (newYVelocity < maxNegativeDownSlopeYVelocity)
                            {
                                newYVelocity = maxNegativeDownSlopeYVelocity;
                            }

                            m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, newYVelocity);
                        }
                        break;
                    case SlopeState.NotOnSlope:
                        //put the y velocity to 0, so we stick to the ground, when coming from a slope
                        m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, 0);
                        break;
                }

                transform.position = new Vector3(transform.position.x, shortestResult.centroid.y, transform.position.z);
                return;
            }
            m_wasGrounded = grounded;
        }
    }
    

    enum SlopeState
    {
        NotOnSlope,
        DownwardsSlope,
        UpwardsSlope,
    }
}