﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        [SerializeField]private bool blockInput;
        
        public bool BlockInput
        {
            get => blockInput;
            set
            {
                blockInput = value ;
                if (blockInput)
                {
                    aimVisualisation.hideCursorRenderer();
                }
                else
                {
                    aimVisualisation.showCursorRenderer();
                }
            }    
        }
        
        public bool onlyXY, blockActions, continuePress, bloodmagicBlockInput, disableBite, skill13DashBlock, useLockedTarget;
        public bool disableBloodmagic;
        public bool humanMode;
        public PlayerInputActions playerInputActions;
        public float aimDeviceResetTime = 0.4f;
        
        private bool bloodmagicInput, biteInput, crouchInput, jumpInput, epicInput, startDelay = true;
        private Vector2 moveInput, moveOutput, aimOutput, oldAimInputPos, oldAimInputMove;
        private Camera cameraRef;
        private PlatformerCharacter2D m_Character;
        private Transform bone1, bulletSpawn, offsetTarget;
        private Vector2 relativePointP;
        private PlayerAimVisualisation aimVisualisation;
        private CurrentAimDevice currentAimDevice;
        private Coroutine aimDeviceTimeoutCoroutine;
        private float aimDeviceTimeoutStartTime;
        
        private void Awake()
        {
            currentAimDevice = CurrentAimDevice.None;
            cameraRef = Camera.main;
            bone1 = transform.Find("Side Animation/bone_1");
            bulletSpawn = transform.Find("Side Animation/bone_1/BulletSpawn");
            m_Character = GetComponent<PlatformerCharacter2D>();
            playerInputActions = new PlayerInputActions();
            Vector3 oldBone1Rotation = bone1.transform.eulerAngles;
            bone1.transform.eulerAngles = new Vector3(bone1.transform.eulerAngles.x, bone1.transform.eulerAngles.y, 90);
            relativePointP = new Vector2(0, bulletSpawn.position.y - bone1.transform.position.y);
            bone1.transform.eulerAngles = oldBone1Rotation;
            aimVisualisation = GetComponent<PlayerAimVisualisation>();
            StartCoroutine(startDelayTimer());
        }

        private void OnEnable()
        {
            playerInputActions.PlayerControls.Enable();
        }

        private void OnDisable()
        {
            playerInputActions.PlayerControls.Disable();
        }

        public void handleMoveInput(InputAction.CallbackContext context)
        {
            moveInput = context.ReadValue<Vector2>();
        }
        
        public void handleBloodMagicInput(InputAction.CallbackContext context)
        {
            if (context.started)
            {
                bloodmagicInput = true;
            }
        }

        public void handleBiteInput(InputAction.CallbackContext context)
        {
            if (context.started)
            {
                biteInput = true;
            }
        }

        public void handleEpicInput(InputAction.CallbackContext context)
        {
            epicInput = context.ReadValue<float>() >= 0.1f;
        }

        public void handleCrouchInput(InputAction.CallbackContext context)
        {
            crouchInput = context.ReadValue<float>() >= 0.1f;
        }

        public void handleJumpInput(InputAction.CallbackContext context)
        {
            jumpInput = context.ReadValue<float>() >= 0.1f;
        }

        private void FixedUpdate()
        {
            aimOutput = Vector2.zero;
            Vector2 aimPositionInput = playerInputActions.PlayerControls.AimPositionInput.ReadValue<Vector2>();
            Vector2 aimDirectionInput = playerInputActions.PlayerControls.AimDirectionInput.ReadValue<Vector2>();
            if (useLockedTarget)
            {
                if (aimVisualisation.currentTarget == null)
                {
                    useLockedTarget = false;
                }
                else
                {
                    aimOutput = calculateAimingDirection(aimVisualisation.currentTarget.position);
                }
            }
            else if (oldAimInputPos != aimPositionInput)
            {
                RegisterCurrentAimDevice(CurrentAimDevice.Position);
                m_Character.noAimingLock = false;
                aimOutput = calculateAimingDirection(cameraRef.ScreenToWorldPoint(aimPositionInput));
                aimVisualisation.usingMouseAim = true;
            }
            else if (aimDirectionInput != Vector2.zero && currentAimDevice != CurrentAimDevice.Position)
            {
                RegisterCurrentAimDevice(CurrentAimDevice.DirectionInput);
                aimVisualisation.usingMouseAim = false;
                m_Character.noAimingLock = false;
                aimOutput = Quaternion.AngleAxis(180, Vector3.forward) * (Vector3)aimDirectionInput;
            }
            else if(currentAimDevice == CurrentAimDevice.None || currentAimDevice == CurrentAimDevice.MoveInput)
            {
                RegisterCurrentAimDevice(CurrentAimDevice.MoveInput);
                aimVisualisation.usingMouseAim = false;
                m_Character.noAimingLock = false;
                if (oldAimInputMove.y != 0 || moveInput.y != 0)
                {
                    aimOutput = Quaternion.AngleAxis(180, Vector3.forward) * (Vector3)moveInput;
                }
            }
            else if (currentAimDevice == CurrentAimDevice.Position)
            {
                aimOutput = calculateAimingDirection(cameraRef.ScreenToWorldPoint(aimPositionInput));
            }
            oldAimInputPos = aimPositionInput;
            oldAimInputMove = moveInput;
            if (startDelay)
            {
                aimOutput = Vector2.zero;
            }
            
            moveOutput = moveInput;
            if (crouchInput && (bloodmagicInput || biteInput) && !skill13DashBlock)
            {
                biteInput = false;
                bloodmagicInput = false;
                epicInput = false;
            }
            bool bloodmagicOutput = bloodmagicInput, biteOutput = biteInput, crouchOutnput = crouchInput, jumpOutput = jumpInput, epicOutput = epicInput;
            if (continuePress) //jump hat eine kleine Abklingzeit, um ungewollte Doppelsprünge zu verhindern
            {
                if (!jumpOutput)
                {
                    continuePress = false;
                }
                else
                {
                    jumpOutput = false;
                }
            }

            // Pass all parameters to the character control script.
            if (!BlockInput)
            {
                if (disableBloodmagic)
                {
                    bloodmagicOutput = false;
                }
                if (disableBite)
                {
                    biteOutput = false;
                }
                if (humanMode)
                {
                    bloodmagicOutput = false;
                }
                if (bloodmagicBlockInput)
                {
                    //moveOutput = Vector2.zero;
                    crouchOutnput = false;
                    bloodmagicOutput = false;
                    biteOutput = false;
                    epicOutput = false;
                    if (m_Character.firingChargedBloodmagic)
                    {
                        epicOutput = epicInput;
                    }
                }
                if (onlyXY)
                {
                    jumpOutput = false;
                    crouchOutnput = false;
                    bloodmagicOutput = false;
                    epicOutput = false;
                    biteOutput = false;
                }
                else if (blockActions)
                {
                    crouchOutnput = false;
                    bloodmagicOutput = false;
                    epicOutput = false;
                    biteOutput = false;
                }
                if (skill13DashBlock)
                {
                    moveOutput = Vector2.zero;
                    jumpOutput = false;
                    biteOutput = false;
                    epicOutput = false;
                }
                m_Character.EvaluateInput(moveOutput, crouchOutnput, jumpOutput, bloodmagicOutput, biteOutput, epicOutput, aimOutput);
            }
            else
            {
                m_Character.EvaluateInput(Vector2.zero, false, false, false, false, false, Vector2.zero);
            }
            bloodmagicInput = false;
            biteInput = false;

            void RegisterCurrentAimDevice(CurrentAimDevice usedCurrentAimDevice)
            {
                currentAimDevice = usedCurrentAimDevice;
                aimDeviceTimeoutStartTime = Time.time;

                if (aimDeviceTimeoutCoroutine == null)
                {
                    aimDeviceTimeoutCoroutine = StartCoroutine(AimDeviceTimeout());
                }
            }
        }

        private Vector2 calculateAimingDirection(Vector2 targetPosition)
        {
            //The aim line must be rotated to match the cursor. The aim line is on child (bulletSpawn) of bone1. Bone1 will rotate so the bulletSpawn Line matches the cursor
            Vector2 pointA = targetPosition;
            Vector2 pointB = bone1.transform.position;
            Vector2 pointP = new Vector2(bone1.transform.position.x + relativePointP.x, bone1.transform.position.y + relativePointP.y);
            //We search for Point C in a Triangle. Point P is above point B, where the aim line in default position hits its x Axis
                
            float b = Vector2.Distance(pointB,pointP);
            float c = Vector2.Distance(pointB,pointA);
            float a = (float)Math.Sqrt(-Math.Pow(b, 2) + Math.Pow(c, 2));

            //Calculate hypotenuse
            float p = (float)Math.Pow(a, 2) / c;
            float q = (float)Math.Pow(b, 2) / c;
            float h = (float)Math.Sqrt(p * q);

            //use hypotenuse to find Point C
            Vector2 directionBtoA =  pointA - pointB;
            Vector2 pointH = pointB + directionBtoA.normalized * p;
            float angle = 90;
            if (pointB.x < pointA.x)
            {
                angle *= -1;
            }
            Vector2 directionBtoARotated = Quaternion.AngleAxis(angle, Vector3.forward) * directionBtoA.normalized;
            Vector2 pointC = (Vector2)pointH + directionBtoARotated.normalized * h;
                
            return pointB - pointC;
        }

        //at start there is a small delay, before the character can start aiming, since it looks confusing
        private IEnumerator startDelayTimer()
        {
            yield return new WaitForSeconds(0.25f);
            startDelay = false;
        }
        
        private IEnumerator AimDeviceTimeout()
        {
            while (Time.time - aimDeviceTimeoutStartTime < aimDeviceResetTime)
            {
                yield return null;
            }
            currentAimDevice = CurrentAimDevice.None;
            StopCoroutine(aimDeviceTimeoutCoroutine);
            aimDeviceTimeoutCoroutine = null;
        }
        
        enum CurrentAimDevice
        {
            Position,
            DirectionInput,
            MoveInput,
            None
        }
    }
    
}
