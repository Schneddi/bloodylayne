using _1BloodyLayne.Control;
using UnityEngine;
using UnityStandardAssets._2D;

namespace Units.Player.Scripts.PlayerControl
{
    public class SkinSelector : MonoBehaviour
    {
        public bool selfActivation;
        public int playerSkin;
        public int gender;
        public UnityEngine.U2D.Animation.SpriteLibraryAsset[] maleSkins, femaleSkins;

        private void Start()
        {
            if (selfActivation)
            {
                playerSkin = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().playerSkin;
                InitSkin();
            }
        }

        public void InitSkin()
        {
            playerSkin = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().playerSkin;
            GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().playerSkin = playerSkin;
            gender = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().gender;
        
            if (gender == 0)
            {
                GetComponent<UnityEngine.U2D.Animation.SpriteLibrary>().spriteLibraryAsset = maleSkins[playerSkin];
            }
            else if (gender == 1)
            {
                GetComponent<UnityEngine.U2D.Animation.SpriteLibrary>().spriteLibraryAsset = femaleSkins[playerSkin];
            }
            gameObject.SetActive(true);
            UnityEngine.U2D.Animation.SpriteResolver[] spriteResolvers = GetComponentsInChildren<UnityEngine.U2D.Animation.SpriteResolver>(true);
            foreach (UnityEngine.U2D.Animation.SpriteResolver spriteResolver in spriteResolvers)
            {
                spriteResolver.SetCategoryAndLabel(spriteResolver.GetCategory(), spriteResolver.GetLabel());
                //individual Sprite layer changes for certain skins. Arm order changes
                if (spriteResolver.transform.parent.name.Equals("Front Animation"))
                {
                    if (gender == 1)
                    {
                        if (spriteResolver.gameObject.name.Equals("Arm rechts"))
                        {
                            spriteResolver.GetComponent<SpriteRenderer>().sortingOrder = 55;
                        }
                    }
                    if (gender == 0 && (playerSkin == 2 || playerSkin == 3))
                    {
                        if (spriteResolver.gameObject.name.Equals("Arm rechts"))
                        {
                            spriteResolver.GetComponent<SpriteRenderer>().sortingOrder = 55;
                        }
                    }
                }
            }
            gameObject.SetActive(false);
        }
    }
}
