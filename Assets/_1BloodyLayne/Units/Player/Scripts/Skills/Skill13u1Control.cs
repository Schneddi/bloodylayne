using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityStandardAssets._2D;

public class Skill13u1Control : MonoBehaviour
{
    public bool left, bloodmagic;
    public float speed = 4, strength, startTime;
    private Rigidbody2D rb;
    private List<Collider2D> hitList;
    private GameObject bloodparticleSys;
    private bool dying;
    private int damage;
    private BlutmagieFunctions bFunctions;
    
    // Start is called before the first frame update
    void Start()
    {
        if (left)
        {
            speed *= -1;
        }

        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(speed, 0);
        hitList = new List<Collider2D>();
        bloodparticleSys = transform.Find("BloodmagicEffect").gameObject;
        if (bloodmagic)
        {
            bloodparticleSys.SetActive(true);
            ParticleSystem.MainModule main = GetComponent<ParticleSystem>().main;
            main.startColor = Color.red;
        }
        bFunctions = GetComponent<BlutmagieFunctions>();
        GetComponent<ParticleSystem>().Play();
        startTime = Time.time;
        StartCoroutine(Death());
    }

    void FixedUpdate()
    {
        if (!dying)
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.5f, 1 << 9);
            {
                foreach (Collider2D col in cols)
                {
                    if (!hitList.Contains(col))
                    {
                        hitList.Add(col);
                        if (bloodmagic)
                        {
                            //100 => must be the same as in bFunctions attack Damage
                            damage = (int)((strength - 5 * (Time.time - startTime)) * 8) * (1 + bFunctions.additionaldamage/100);
                        }
                        if (col.gameObject.GetComponent<EnemyStats>() != null)
                        {
                            col.gameObject.GetComponent<EnemyStats>().PushHit(damage, DamageTypeToEnemy.Skill13Push,
                                new Vector2(speed * 75 * strength, 100 * strength), bFunctions.guid, true);
                            if (bloodmagic)
                            {
                                bFunctions.DeathFunctions(transform.position);
                            }
                        }
                    }
                }
            }
        }
    }

    IEnumerator Death()
    {
        yield return new WaitForSeconds(1.5f);
        //particle Systems are playing for 1.5 seconds and dont need to be stopped
        dying = true;
        yield return new WaitForSeconds(1.5f);
        Destroy(gameObject);
        
    }
}
