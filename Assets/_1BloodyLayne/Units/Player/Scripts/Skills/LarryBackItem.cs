using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.XR;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class LarryBackItem : MonoBehaviour
{
    public bool sprite;
    [FormerlySerializedAs("particleSystem")] public bool useParticleSystem;

    private SpriteRenderer sRenderer;
    private ParticleSystem pSystem;
    private GameObject backAnimations, sideAnimations;
    private int startLOrder;
    private float transitionTime;
    private Animator animator, spriteAnimator;
    private Vector3 startPosition, cowerPosition, cawlPosition, startScale, sideAnimationScale, oldPosition, goToPosition, adjustedBackBoneRotation, startRotation;
    private Transform backBone;
    private PlatformerCharacter2D playerCharacter;
    
    // Start is called before the first frame update
    void Start()
    {
        playerCharacter = transform.parent.GetComponent<PlatformerCharacter2D>();
        backAnimations = transform.parent.transform.Find("Back Animation").gameObject;
        sideAnimations = transform.parent.transform.Find("Side Animation").gameObject;
        animator = transform.parent.GetComponent<Animator>();
        if (sprite)
        {
            sRenderer = GetComponentInChildren<SpriteRenderer>();
            startLOrder = sRenderer.sortingOrder;
            spriteAnimator = GetComponent<Animator>();
        }
        if (useParticleSystem)
        {
            pSystem = GetComponent<ParticleSystem>();
            startLOrder = pSystem.GetComponent<Renderer>().sortingOrder;
        }
        startPosition = transform.localPosition;
        cowerPosition = new Vector3(startPosition.x - 1.5f, startPosition.y - playerCharacter.crouchHigh / 2f,
            startPosition.z);
        cawlPosition = new Vector3(startPosition.x, startPosition.y - playerCharacter.crouchHigh / 1.5f,
            startPosition.z);
        backBone = transform.parent.transform.Find("Side Animation/bone_1/bone_2/bone_3").gameObject.transform;
        startScale = transform.localScale;
        sideAnimationScale = new Vector3(startScale.x / 1.33f, startScale.y, startScale.z);
        startRotation = transform.rotation.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        if (backAnimations.activeSelf)
        {
            if (sprite && sRenderer.sortingOrder != 50)
            {
                sRenderer.sortingOrder = 50;
            }
            else if (useParticleSystem && pSystem.GetComponent<Renderer>().sortingOrder != 50)
            {
                pSystem.GetComponent<Renderer>().sortingOrder = 50;
            }
        }
        else
        {
            if (sprite && sRenderer.sortingOrder != startLOrder)
            {
                sRenderer.sortingOrder = startLOrder;
            }
            else if (useParticleSystem && pSystem.GetComponent<Renderer>().sortingOrder != startLOrder)
            {
                pSystem.GetComponent<Renderer>().sortingOrder = startLOrder;
            }
        }

        if (spriteAnimator != null)
        {
            if (!spriteAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Folding") && !spriteAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Spreading"))
            {
                manageRotationPositionAndScale();
            }
        }
        else
        {
            manageRotationPositionAndScale();
        }
    }

    private void manageRotationPositionAndScale()
    {
        if (sideAnimations.activeSelf && transform.localScale != sideAnimationScale)
        {
            transform.localScale = sideAnimationScale;
        }
        else if(!sideAnimations.activeSelf && transform.localScale == sideAnimationScale)
        {
            transform.localScale = startScale;
        }
        
        if (sideAnimations.activeSelf)
        {
            goToPosition = Vector3.zero;
            if (animator.GetCurrentAnimatorStateInfo(0).IsTag("Crawl"))
            {
                transform.rotation = Quaternion.Euler(startRotation);
            }
            else
            {
                float angle = 65;
                if (playerCharacter.m_FacingRight)
                {
                    angle *= -1;
                }
                transform.rotation = backBone.rotation * Quaternion.Euler(0, 0, angle);
            }
            transform.position = backBone.position;
        }
        else if (animator.GetCurrentAnimatorStateInfo(0).IsTag("Cower") && goToPosition != cowerPosition)
        {
            transform.rotation = Quaternion.Euler(startRotation);
            transitionTime = 0;
            oldPosition = transform.localPosition;
            goToPosition = cowerPosition;
        }
        else if(!animator.GetCurrentAnimatorStateInfo(0).IsTag("Cower"))
        {
            transform.rotation = Quaternion.Euler(startRotation);
            transitionTime = 0;
            oldPosition = transform.localPosition;
            goToPosition = startPosition;
        }

        if (transitionTime < 1)
        {
            transitionTime += Time.deltaTime * 9;
            if (transitionTime > 1)
            {
                transform.localPosition = goToPosition;
            }
            transform.localPosition = Vector3.Lerp(oldPosition, goToPosition, transitionTime);
        }
    }
}
