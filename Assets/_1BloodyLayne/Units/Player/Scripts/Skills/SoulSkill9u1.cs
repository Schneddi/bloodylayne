﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using Units.Player;
using UnityEditor;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class SoulSkill9u1 : MonoBehaviour
    {
        public int attackDamage = 50;
        public float pushForce;

        //pushes back the enemy, applies attackDamage and destroys itself
        public void ApplySoul9u1Effects(GameObject target, Vector2 pushDirection)
        {
            System.Guid guid = System.Guid.NewGuid();
            target.gameObject.GetComponent<EnemyStats>().Hit(attackDamage, DamageTypeToEnemy.MagicShield, guid, true);
            target.gameObject.GetComponent<Rigidbody2D>().AddForce(pushDirection.normalized * pushForce);
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill9u1Spawned = false;
            Destroy(gameObject);
        }
    }
}
