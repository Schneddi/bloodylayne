﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEditor;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class Skill8u1ExplosionDamage : MonoBehaviour
    {
        public int attackDamage;
        
        // Start is called before the first frame update
        void Start()
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 12f, 1 << 9);
            System.Guid guid = System.Guid.NewGuid();
            foreach (Collider2D col in cols)
            {
                if ((col.gameObject.GetComponent("EnemyStats") as EnemyStats) != null)
                {
                    col.gameObject.GetComponent<EnemyStats>().Hit(attackDamage, DamageTypeToEnemy.Explosion, guid, true);
                }
                else if (col.gameObject.GetComponent<ObjektStats>() != null)
                {
                    if (col.gameObject.GetComponent<ObjektStats>().destructable)
                    {
                        col.gameObject.GetComponent<ObjektStats>().hitPoints -= attackDamage;
                        if (col.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                        {
                            col.gameObject.GetComponent<ObjektStats>().startDeath();
                        }
                    }
                }
            }
        }
    }
}