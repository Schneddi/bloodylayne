﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using Units.Player;
using UnityEditor;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    public class Skill10u1GasDamage : MonoBehaviour
    {
        public int attackDamage;
        private PlatformerCharacter2D larry;
        public GameObject gasHit;
        private bool damaging = true;
        private bool startDimming;
        private UnityEngine.Rendering.Universal.Light2D[] m_Lightsources;
        private System.Guid guid;
        
        // Start is called before the first frame update
        void Start()
        {
            larry = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            StartCoroutine(damageTickerDelay());
            StartCoroutine(dealayAndDie());
            m_Lightsources = GetComponentsInChildren<UnityEngine.Rendering.Universal.Light2D>();
            guid = System.Guid.NewGuid();
        }
        
        void Update()
        {
            if (startDimming)
            {
                if (m_Lightsources[0].intensity > 0)
                {
                    foreach (UnityEngine.Rendering.Universal.Light2D lightsource in m_Lightsources)
                    {
                        lightsource.intensity -= 0.5f * Time.deltaTime;
                    }    
                }
            }
        }

        IEnumerator damageTickerDelay()
        {
            yield return new WaitForSeconds(0.7f);
            StartCoroutine(damageTicker());
        }

        IEnumerator damageTicker()
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 3f, 1 << 9);
            foreach (Collider2D col in cols)
            {
                if ((col.gameObject.GetComponent("EnemyStats") as EnemyStats) != null && Mathf.Abs(col.transform.position.y - transform.position.y) < 1.75f)
                {
                    if (!col.gameObject.GetComponent<EnemyStats>().dead && !col.gameObject.GetComponent<EnemyStats>().unverwundbar)
                    {
                        col.gameObject.GetComponent<EnemyStats>().Hit(attackDamage, DamageTypeToEnemy.Fart, guid, true);
                        Instantiate(gasHit, col.transform.position, Quaternion.identity);
                    }
                }
            }
            yield return new WaitForSeconds(0.40f);
            if (damaging)
            {
                StartCoroutine(damageTicker());
            }
        }

        IEnumerator dealayAndDie()
        {
            yield return new WaitForSeconds(4f);
            GetComponent<ParticleSystem>().Stop();
            GetComponents<AudioSource>()[0].Stop();
            yield return new WaitForSeconds(2f);
            damaging = false;
            startDimming = true;
            yield return new WaitForSeconds(2f);
            Destroy(gameObject);
        }
    }
}