﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LarryWingAnimations : MonoBehaviour
{
    public bool foldWings = false;
    AudioSource[] sounds;
    private Animator m_animator;

    private void Start()
    {
        sounds = GetComponents<AudioSource>();
        StartCoroutine(playWingAnimation());
        m_animator = GetComponent<Animator>();
        foldWings = false;
    }

    private void Update()
    {
        if (foldWings)
        {
            if (transform.localScale.x <= 0.05f)
            {
                foldWings = false;
                Destroy(gameObject);
            }
        }
    }

    IEnumerator playWingAnimation()//gibt einen Schild, damit der nosferatu Zeit hat seine letzte Attacke abzuschließen und in die nächste runde zu gehen
    {
        int rnd = Random.Range(0, 4);
        sounds[rnd].PlayDelayed(0);
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(playWingAnimation());
    }

    public void endFlight()
    {
        m_animator.SetTrigger("Folding");
        foldWings = true;
    }
}