using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEditor;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Skill5u1HealControl : MonoBehaviour
    {
        public int healAmount;
        
        private Transform larry;
        private float lerpStep;
        
        void Start()
        {
            larry = GameObject.FindGameObjectWithTag("Player").transform;
        }

        // Update is called once per frame
        void Update()
        {
            lerpStep += Time.deltaTime * 2f;
            transform.position = Vector3.Lerp(transform.position, larry.position, lerpStep);
            if (lerpStep >= 1)
            {
                larry.GetComponent<PlatformerCharacter2D>().AddHealth(healAmount);
                Destroy(gameObject);
            }
        }
    }
}