﻿using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class SoulSkill9u2 : MonoBehaviour
    {
        public GameObject abwehrSound;
        public float radiantMultipliy = 0.001f;
        public float radiant;
        float rotationX, roationY;
        public Transform playerChar;
        // Start is called before the first frame update
        void Start()
        {
            playerChar = GameObject.FindGameObjectWithTag("Player").transform;
        }

        void Update()
        {
            radiant += radiantMultipliy * 1 * Time.deltaTime;
            if (radiant > 2 * Mathf.PI)
            {
                radiant = radiant - 2 * Mathf.PI;
            }
            rotationX = Mathf.Sin(radiant);
            roationY = Mathf.Cos(radiant);
            transform.position = new Vector2(playerChar.transform.position.x + rotationX * 1.5f, playerChar.transform.position.y + roationY * 3f);

            if (Physics2D.OverlapCircle(transform.position, 2f, 1 << 10))
            {
                Collider2D projectile = Physics2D.OverlapCircle(transform.position, 2f, 1 << 10);
                if (projectile.gameObject.GetComponent<BlutmagieFarProjektilControl>() == null && projectile.gameObject.GetComponent<BlutmagieProjektilControl>() == null && projectile.gameObject.GetComponent<BlutmagieStandardControl>() == null && projectile.gameObject.GetComponent<BlutmagieChargedProjektilControl>() == null && projectile.gameObject.GetComponent<BloodmagicBiteControl>() == null && projectile.gameObject.GetComponent<BloodmagicBoomerangControl>() == null)
                {
                    Destroy(projectile.gameObject);
                    GameObject deathKind = Instantiate(abwehrSound, transform.position, Quaternion.identity);
                    deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
                    playerChar.GetComponent<PlatformerCharacter2D>().skill9u2Spawned = false;
                    Destroy(gameObject);
                }
            }

            if (Physics2D.OverlapCircle(transform.position, 2f, 1 << 18))
            {
                Collider2D projectile = Physics2D.OverlapCircle(transform.position, 2f, 1 << 18);
                if (projectile.gameObject.GetComponent<BlutmagieFarProjektilControl>() == null && projectile.gameObject.GetComponent<BlutmagieProjektilControl>() == null && projectile.gameObject.GetComponent<BlutmagieStandardControl>() == null && projectile.gameObject.GetComponent<BlutmagieChargedProjektilControl>() == null && projectile.gameObject.GetComponent<BloodmagicBiteControl>() == null && projectile.gameObject.GetComponent<BloodmagicBoomerangControl>() == null)
                {
                    Destroy(projectile.gameObject);
                    GameObject deathKind = Instantiate(abwehrSound, transform.position, Quaternion.identity);
                    deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
                    playerChar.GetComponent<PlatformerCharacter2D>().skill9u2Spawned = false;
                    Destroy(gameObject);
                }
            }

            if (Physics2D.OverlapCircle(transform.position, 2f, 1 << 19))
            {
                Collider2D projectile = Physics2D.OverlapCircle(transform.position, 2f, 1 << 19);
                if (projectile.gameObject.GetComponent<BlutmagieFarProjektilControl>() == null && projectile.gameObject.GetComponent<BlutmagieProjektilControl>() == null && projectile.gameObject.GetComponent<BlutmagieStandardControl>() == null && projectile.gameObject.GetComponent<BlutmagieChargedProjektilControl>() == null && projectile.gameObject.GetComponent<BloodmagicBiteControl>() == null && projectile.gameObject.GetComponent<BloodmagicBoomerangControl>() == null)
                {
                    Destroy(projectile.gameObject);
                    GameObject deathChild = Instantiate(abwehrSound, transform.position, Quaternion.identity);
                    deathChild.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
                    playerChar.GetComponent<PlatformerCharacter2D>().skill9u2Spawned = false;
                    Destroy(gameObject);
                }
            }
        }
    }
}
