using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class AnimationTester : MonoBehaviour
{
    public string animationName;
    public string boolName;
    public float speed;

    private Animator m_animator;
    // Start is called before the first frame update
    void Start()
    {
        m_animator = GetComponent<Animator>();
        if (boolName != "")
        {
            m_animator.SetBool(boolName, true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!m_animator.GetCurrentAnimatorStateInfo(0).IsTag(animationName))
        {
            m_animator.SetTrigger(animationName);
        }
        if (speed > 0)
        {
            m_animator.SetFloat("Speed",speed);
        }
    }
}
