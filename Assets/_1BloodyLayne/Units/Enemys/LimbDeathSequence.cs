using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class LimbDeathSequence : MonoBehaviour
{
    Color deathColor;
    SpriteRenderer sRenderer;
    public GameObject bloodParticle;
    private BoxCollider2D m_boxCollider;
    // Use this for initialization
    void Start()
    {
        deathColor = new Color(255f, 0, 0, 255f);
        sRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine(DeathTimer());
        m_boxCollider = GetComponent<BoxCollider2D>();
    }

    public void Update()
    {
        sRenderer.color = Color.Lerp(sRenderer.color, deathColor,  Time.deltaTime * 2f);
    }

    IEnumerator DeathTimer()
    {
        float deathTime = Random.Range(0.5f, 1f);
        yield return new WaitForSeconds(deathTime);
        //Instantiate(bloodParticle, transform.position + new Vector3(m_boxCollider.offset.x * transform.localScale.x, m_boxCollider.offset.y * transform.localScale.y, 0), Quaternion.identity);
        Instantiate(bloodParticle, m_boxCollider.bounds.center, Quaternion.identity);
        Destroy(gameObject);
    }
}
