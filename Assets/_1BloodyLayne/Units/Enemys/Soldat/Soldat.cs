﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Enemys;
using Unity.Mathematics;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Soldat : MonoBehaviour
    {
        EnemyStats stats;
        public float speed, jumpforce;
        public bool startAggressive;
        bool angriffsmodus, shieldMode, hitZone1, hitZone2, hitZone3, hitZone4, rennt, idle=true, aggro;
        private Transform player, schild, hit1, hit2, hit3, hit4, heightSensor;
        Animator animator;
        AudioSource[] sounds;
        AudioSource schildenSound, entschildenSound, abwehrSound, attackSound, hitSound, runSound, jumpSound;
        Rigidbody2D rb;
        private float gravityStore, dragStore, idleDrag = 30;
        private BoxCollider2D shieldCollider;
        public GameObject deniedEffect;
        private GlobalVariables gv;

        // Use this for initialization
        void Start()
        {
            player = GameObject.FindWithTag("Player").transform;
            schild = transform.Find("Projektilabfang");
            hit1 = transform.Find("Hit1");
            hit2 = transform.Find("Hit2");
            hit3 = transform.Find("Hit3");
            hit4 = transform.Find("Hit4");
            heightSensor = transform.Find("HeightSensor");
            shieldCollider = transform.Find("ShieldCollider").GetComponent<BoxCollider2D>();
            stats = GetComponent<EnemyStats>();
            rb = GetComponent<Rigidbody2D>();
            sounds = GetComponents<AudioSource>();
            schildenSound = sounds[0];
            entschildenSound = sounds[1];
            abwehrSound = sounds[2];
            attackSound = sounds[3];
            hitSound = sounds[4];
            runSound = sounds[5];
            jumpSound = sounds[6];
            animator = GetComponent<Animator>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            dragStore = rb.drag;
            rb.drag = idleDrag;
            switch (gv.difficulty)
            {
                case 0:
                    speed *= 0.8f;
                    break;
                case 1:
                    speed *= 0.9f;
                    break;
                case 2:
                    speed *= 1f;
                    break;
                case 3:
                    speed *= 1.25f;
                    break;
                case 4:
                    speed *= 1.5f;
                    break;
            }
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                speed *= -1;
                transform.localScale = theScale;
            }
            gravityStore = rb.gravityScale;

            if (startAggressive)
            {
                FlipCheck();
                idle = false;
                rb.drag = dragStore;
                aggro = true;
                StartCoroutine(Schilden());
            }
        }

        private void Update()
        {
            //block incoming projectiles with shield, dont block charged Bloodmagic though
            if (shieldMode)
            {
                RaycastHit2D hit = Physics2D.Raycast(schild.position, Vector2.up, 2.6f, 1 << 20);
                if (hit.collider != null && (hit.collider.gameObject.GetComponent("BlutmagieChargedProjektilControl") as BlutmagieChargedProjektilControl) == null)
                {
                    DestroyProjectile(hit.collider.gameObject);
                }
                hit = Physics2D.Raycast(new Vector2(schild.position.x + 0.1f, schild.position.y), Vector2.up, 2.6f, 1 << 20);
                if (hit.collider != null && (hit.collider.gameObject.GetComponent("BlutmagieChargedProjektilControl") as BlutmagieChargedProjektilControl) == null)
                {
                    DestroyProjectile(hit.collider.gameObject);
                }
                hit = Physics2D.Raycast(new Vector2(schild.position.x - 0.1f, schild.position.y), Vector2.up, 2.6f, 1 << 20);
                if (hit.collider != null && (hit.collider.gameObject.GetComponent("BlutmagieChargedProjektilControl") as BlutmagieChargedProjektilControl) == null)
                {
                    DestroyProjectile(hit.collider.gameObject);
                }
            }
        }

        private void FixedUpdate()
        {
            if (!stats.dead)
            {
                checkDeath();
                if (hitZone1)//fragt nacheinander die HitZonen ab, an denen das Schwert Schaden erzeugt, dabei wird am gesamten Schwertschwung abgefragt
                {
                    Guid attackGuid = new Guid();
                    if (Physics2D.OverlapCircle(hit1.position, 0.5f, 1 << 8))
                    {
                        if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeAttack"), DamageTypeToPlayer.MeleeSwing, new Vector2(speed / 10, 0), hit1.position, gameObject, attackGuid))
                        {
                            hitSound.PlayDelayed(0);
                        }
                    }
                    damageObjects(hit1);
                    if (hitZone2)
                    {
                        if (Physics2D.OverlapCircle(hit2.position, 0.5f, 1 << 8))
                        {
                            if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeAttack"), DamageTypeToPlayer.MeleeSwing, new Vector2(speed / 10, 0), hit2.position, gameObject, attackGuid))
                            {
                                hitSound.PlayDelayed(0);
                            }
                        }
                        damageObjects(hit2);
                        if (hitZone3)
                        {
                            if (Physics2D.OverlapCircle(hit3.position, 0.5f, 1 << 8))
                            {
                                if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeAttack"), DamageTypeToPlayer.MeleeSwing, new Vector2(speed / 10, 0), hit3.position, gameObject, attackGuid))
                                {
                                    hitSound.PlayDelayed(0);
                                }
                            }
                            damageObjects(hit3);
                            if (hitZone4)
                            {
                                if (Physics2D.OverlapCircle(hit4.position, 0.5f, 1 << 8))
                                {
                                    if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeAttack"), DamageTypeToPlayer.MeleeSwing, new Vector2(speed / 10, 0), hit4.position, gameObject, attackGuid))
                                    {
                                        hitSound.PlayDelayed(0);
                                    }
                                }
                            }
                            damageObjects(hit4);
                        }
                    }
                }

                //block incoming projectiles with shield, dont block charged Bloodmagic though
                if (shieldMode)
                {
                    RaycastHit2D hit = Physics2D.Raycast(schild.position, Vector2.up, 2.6f, 1 << 20);
                    if (hit.collider != null && (hit.collider.gameObject.GetComponent("BlutmagieChargedProjektilControl") as BlutmagieChargedProjektilControl) == null)
                    {
                        DestroyProjectile(hit.collider.gameObject);
                    }
                    hit = Physics2D.Raycast(new Vector2(schild.position.x + 0.1f, schild.position.y), Vector2.up, 2.6f, 1 << 20);
                    if (hit.collider != null && (hit.collider.gameObject.GetComponent("BlutmagieChargedProjektilControl") as BlutmagieChargedProjektilControl) == null)
                    {
                        DestroyProjectile(hit.collider.gameObject);
                    }
                    hit = Physics2D.Raycast(new Vector2(schild.position.x - 0.1f, schild.position.y), Vector2.up, 2.6f, 1 << 20);
                    if (hit.collider != null && (hit.collider.gameObject.GetComponent("BlutmagieChargedProjektilControl") as BlutmagieChargedProjektilControl) == null)
                    {
                        DestroyProjectile(hit.collider.gameObject);
                    }
                }
                
                if (idle)
                {
                    if(Physics2D.OverlapCircle(transform.position, 16f, 1 << 8) && (Mathf.Abs(player.position.y -transform.position.y)<2) || aggro && Physics2D.OverlapCircle(transform.position, 20f, 1 << 8))
                    {
                        if (Physics2D.OverlapCircle(transform.position, 2.5f, 1 << 9))
                        {
                            Vector3 richtung;
                            if (stats.facingRight) {
                                richtung = new Vector3(5,0,0);
                            }
                            else
                            {
                                richtung = new Vector3(-5, 0, 0);
                            }
                            RaycastHit2D hit = Physics2D.Raycast(transform.position, richtung);
                            if (hit)
                            {
                                if (hit.collider.gameObject.GetComponent("Armbrustschuetze") == null ||
                                    !(Physics2D.OverlapCircle(transform.position, 2.5f, 1 << 9)))
                                {
                                    richtung = player.transform.position - transform.position;
                                    RaycastHit2D playerHit = Physics2D.Raycast(transform.position, richtung, 20,1<<8 | 1<< 11);
                                    if (playerHit)
                                    {
                                        if (playerHit.transform.CompareTag("Player"))
                                        {
                                            FlipCheck();
                                            idle = false;
                                            rb.drag = dragStore;
                                            aggro = true;
                                            StartCoroutine(Schilden());
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Vector3 richtung = player.transform.position - transform.position;
                            RaycastHit2D hit = Physics2D.Raycast(transform.position, richtung, 20,1<<8 | 1<< 11);
                            if (hit)
                            {
                                if (hit.transform.CompareTag("Player"))
                                {
                                    FlipCheck();
                                    idle = false;
                                    rb.drag = dragStore;
                                    aggro = true;
                                    StartCoroutine(Schilden());
                                }
                            }
                        }
                    }
                }
                else if (!Physics2D.OverlapCircle(transform.position, 30f, 1 << 8) && aggro)
                {
                    aggro = false;
                }
                if (rennt)
                {
                    Vector3 richtung;
                    if (stats.facingRight)
                    {
                        richtung = Vector3.right;
                    }
                    else
                    {
                        richtung = Vector3.left;
                    }
                    
                    RaycastHit2D forwardRay = Physics2D.Raycast(new Vector2(transform.position.x + richtung.x * 0.5f, transform.position.y), richtung);
                    if (forwardRay.collider.gameObject.GetComponent<EnemyStats>() != null && Mathf.Abs(transform.position.x - forwardRay.transform.position.x) < 3f)
                    {
                        if (forwardRay.collider.gameObject.GetComponent<EnemyStats>().ranged)
                        {
                            rennt = false;
                            runSound.Stop();
                            StartCoroutine(Jump(10));
                        }
                        else
                        {
                            StartCoroutine(Entschilden());
                            rennt = false;
                        }
                    }
                    else if (Physics2D.OverlapCircle(heightSensor.transform.position, 0.2f, 1 << 11 | 1 << 14) || Physics2D.OverlapCircle(heightSensor.transform.position, 0.4f, 1 << 14)&& Mathf.Abs(rb.velocity.x) < 1  && player.transform.position.y > transform.position.y)
                    {
                        rennt = false;
                        runSound.Stop();
                        float jumpheight = 15;
                        RaycastHit2D upwardsRay = Physics2D.Raycast(heightSensor.position, Vector2.up, 10, 1 << 11 | 1 << 14);
                        if (upwardsRay.collider != null)
                        {
                            if ((upwardsRay.transform.position.y - transform.position.y) * 0.5f > 15)
                            {
                                jumpheight = (upwardsRay.transform.position.y - transform.position.y) * 0.5f;
                            }
                        }
                        StartCoroutine(Jump(jumpheight));
                    }
                    else
                    {
                        if (Physics2D.OverlapCircle(transform.position, 2.5f, 1 << 8))
                        {
                            StartCoroutine(Entschilden());
                            rennt = false;
                            runSound.Stop();
                        }
                        else if (stats.facingRight && transform.position.x > player.position.x)
                        {
                            StartCoroutine(Entschilden());
                            rennt = false;
                        }
                        else if (!stats.facingRight && transform.position.x < player.position.x)
                        {
                            StartCoroutine(Entschilden());
                            rennt = false;
                        }
                        else if (Physics2D.OverlapCircle(transform.position, 40f, 1 << 8) && (Mathf.Abs(player.position.y - transform.position.y) < 6))
                        {
                            rb.AddForce(new Vector2(speed, rb.velocity.y));
                        }
                        else
                        {
                            StartCoroutine(Entschilden());
                            rennt = false;
                        }
                    }
                }
            }

            if (stats.onLadder && !animator.GetBool("Death"))
            {
                rb.gravityScale = 0f;

                //climbVelocity = 0;

                rb.velocity = new Vector2(rb.velocity.x, 0);
            }

            if (!stats.onLadder)
            {
                rb.gravityScale = gravityStore;

            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Projectile" && other.GetComponent<BlutmagieChargedProjektilControl>() == null && other.GetComponent<Witchbomb>() == null)
            {
                DestroyProjectile(other.gameObject);
            }
        }

        private void DestroyProjectile(GameObject projectile)
        {
            Instantiate(deniedEffect, projectile.transform.position, quaternion.identity);
            abwehrSound.PlayOneShot(abwehrSound.clip);
            Destroy(projectile);
        }
        
        void FlipCheck()//z.B. in andere Richtung gucken
        {
            if (stats.facingRight && transform.position.x > player.position.x)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
            else if (!stats.facingRight && transform.position.x < player.position.x)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
        }

        IEnumerator Schilden()//Gibt Schussfrequenz und Animationen an
        {
            schildenSound.PlayDelayed(0);
            animator.SetTrigger("Schilden");
            if (gv.difficulty >= 3)
            {
                animator.SetFloat("ShieldingSpeed", 2);
                yield return new WaitForSeconds(0.15f);
            }
            else
            {
                yield return new WaitForSeconds(0.3f);
            }
            if (!animator.GetBool("Death"))
            {
                shieldMode = true;
                shieldCollider.enabled = true;
                runSound.PlayDelayed(0);
                rennt = true;
                animator.SetTrigger("SchildRun");
            }
        }


        IEnumerator Entschilden()//Gibt Schussfrequenz und Animationen an
        {
            runSound.Stop();
            entschildenSound.PlayDelayed(0);
            animator.SetTrigger("Entschilden");
            if (gv.difficulty >= 3)
            {
                animator.SetFloat("ShieldingSpeed", 2);
                yield return new WaitForSeconds(0.15f);
            }
            else
            {
                yield return new WaitForSeconds(0.3f);
            }
            shieldMode = false;
            shieldCollider.enabled = false;
            if (gv.difficulty >= 3)
            {
                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                yield return new WaitForSeconds(0.2f);
            }
            
            //if harder difficulty, shield yourself again, if player is not in front
            if ((Physics2D.OverlapCircle(transform.position, 4f, 1 << 8) && gv.difficulty >= 3) || (gv.difficulty <= 2 && !(Physics2D.OverlapCircle(heightSensor.transform.position, 0.2f, 1 << 11 | 1 << 14) || Physics2D.OverlapCircle(heightSensor.transform.position, 0.4f, 1 << 14)&& Mathf.Abs(rb.velocity.x) < 1  && player.transform.position.y > transform.position.y))) {
                if (!animator.GetBool("Death"))
                {
                    FlipCheck();
                    StartCoroutine(Attack());
                }
            }
            else
            {
                if (!animator.GetBool("Death"))
                {
                    idle = true;
                    rb.drag = idleDrag;
                }
            }
        }
        
        IEnumerator Jump(float height)//Gibt Schussfrequenz und Animationen an
        {
            jumpSound.PlayDelayed(0);
            animator.SetTrigger("Jump");
            yield return new WaitForSeconds(0.1f);
            rb.AddForce(new Vector2(0, height * jumpforce));
            yield return new WaitForSeconds(0.2f);
            rb.AddForce(new Vector2(speed * 10, 0));
            yield return new WaitForSeconds(0.15f);
            if (!animator.GetBool("Death"))
            {
                shieldCollider.enabled = true;
                shieldMode = true;
                runSound.PlayDelayed(0);
                rennt = true;
                animator.SetTrigger("SchildRun");
                FlipCheck();
            }
        }
        
        IEnumerator Attack()//Gibt Schussfrequenz und Animationen an
        {
            for(int i=0; i<3; i++)
            {
                if (!animator.GetBool("Death"))
                {
                    attackSound.PlayDelayed(0);
                    animator.SetTrigger("Attack");
                    rb.AddForce(new Vector2(speed * 20f, 0));
                    yield return new WaitForSeconds(0.2f);
                    hitZone1 = true;
                    yield return new WaitForSeconds(0.1f);
                    hitZone2 = true;
                    yield return new WaitForSeconds(0.1f);
                    hitZone3 = true;
                    yield return new WaitForSeconds(0.1f);
                    hitZone4 = true;
                    yield return new WaitForSeconds(0.1f);
                    hitZone1 = false;
                    hitZone2 = false;
                    hitZone3 = false;
                    hitZone4 = false;
                }
            }
            yield return new WaitForSeconds(0.2f);
            if (!animator.GetBool("Death"))
            {
                StartCoroutine(Schilden());
            }
        }

        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }

        private void Death()
        {
            stats.dead = true;
            runSound.Stop();
            rennt = false;
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            GetComponent<HumanDeathSequence>().Death();
        }

        void damageObjects(Transform hit)
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(hit.position, 0.5f, 1 << 14);
            {
                foreach (Collider2D col in cols)
                {
                    if (col.gameObject.GetComponent<ObjektStats>() != null)
                    {
                        if (col.gameObject.GetComponent<ObjektStats>().destructable)
                        {
                            col.gameObject.GetComponent<ObjektStats>().hitPoints -= stats.attacks.GetValueOrDefault("MeleeAttack");
                            if (col.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                            {
                                col.gameObject.GetComponent<ObjektStats>().startDeath();
                            }
                        }
                    }
                }
            }
        }
    }
}