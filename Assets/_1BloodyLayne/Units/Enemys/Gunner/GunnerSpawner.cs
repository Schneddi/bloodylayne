using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunnerSpawner : MonoBehaviour
{
    public bool multiSpawn;
    
    private GunnerSpawn gunnerspawn;
    private bool activated;
    // Start is called before the first frame update
    void Start()
    {
        gunnerspawn = transform.parent.parent.GetComponent<GunnerSpawn>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player" && (!activated || multiSpawn))
        {
            activated = true;
            GunnerSpawnPoint[] gunnerSpawnPoints = gunnerspawn.Spawnpoints;
            foreach (GunnerSpawnPoint gunnerSpawnPoint in gunnerSpawnPoints)
            {
                gunnerSpawnPoint.active = true;
            }
        }
    }

    public void customActivation()
    {
        activated = true;
        GunnerSpawnPoint[] gunnerSpawnPoints = gunnerspawn.Spawnpoints;
        foreach (GunnerSpawnPoint gunnerSpawnPoint in gunnerSpawnPoints)
        {
            gunnerSpawnPoint.active = true;
        }
    }
}
