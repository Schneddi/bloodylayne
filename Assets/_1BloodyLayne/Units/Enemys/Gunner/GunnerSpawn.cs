using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunnerSpawn : MonoBehaviour
{
    public bool startAlarmed, alwaysAggro;
    public GunnerSpawnPoint[] Spawnpoints;
    // Start is called before the first frame update
    void Start()
    {
        Spawnpoints = transform.GetComponentsInChildren<GunnerSpawnPoint>();
    }
}
