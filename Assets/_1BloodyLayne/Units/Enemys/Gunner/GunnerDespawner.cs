using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunnerDespawner : MonoBehaviour
{
    private GunnerSpawn gunnerspawn;
    // Start is called before the first frame update
    void Start()
    {
        gunnerspawn = transform.parent.parent.GetComponent<GunnerSpawn>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            GunnerSpawnPoint[] gunnerSpawnPoints = gunnerspawn.Spawnpoints;
            foreach (GunnerSpawnPoint gunnerSpawnPoint in gunnerSpawnPoints)
            {
                if (gunnerSpawnPoint.spawnedGunner != null)
                {
                    gunnerSpawnPoint.despawnActivated();
                }
            }
        }
    }
}
