using System;
using System.Collections;
using System.Collections.Generic;
using Maps.Castle;
using UnityEngine;
using UnityStandardAssets._2D;

public class GunnerSpawnPoint : MonoBehaviour
{
    public bool active;
    public GameObject gunner;
    public bool stopSpawning;
    public int spawnCount = 1, remainingSpawnCount;
    public AutoForeground foreground;
    public GameObject spawnedGunner;
    private GunnerSpawn gunnerSpawn;
    private Transform patrolWaypoint;
    
    void Start()
    {
        gunnerSpawn = transform.parent.GetComponent<GunnerSpawn>();
        patrolWaypoint = transform.Find("Waypoint");
        remainingSpawnCount = spawnCount;
    }

    private void Update()
    {
        if (remainingSpawnCount > 0 && active && !stopSpawning && spawnedGunner == null)
        {
            if (foreground != null)
            {
                if (!foreground.playerInBounds)
                {
                    spawn();
                }
            }
            else
            {
                spawn();
            }
        }
    }

    private void spawn()
    {
        GameObject gunnerCopy = gunner;
        if (remainingSpawnCount < spawnCount)
        {
            gunnerSpawn.startAlarmed = true;
        }
        gunnerCopy.transform.Find("Sensors").Find("PatrolWaypoint").localPosition = new Vector3(
            patrolWaypoint.localPosition.x / gunner.transform.localScale.x, patrolWaypoint.localPosition.y / gunner.transform.localScale.y, patrolWaypoint.localPosition.z / gunner.transform.localScale.z);
        spawnedGunner = Instantiate(gunnerCopy, transform.position, Quaternion.identity);
        spawnedGunner.GetComponent<Gunner>().originGunnerSpawnPoint = this;
        if (gunnerSpawn.alwaysAggro)
        {
            spawnedGunner.GetComponent<Gunner>().alwaysAggro = true;
        }
        if (gunnerSpawn.startAlarmed)
        {
            spawnedGunner.GetComponent<Gunner>().startAggro();
        }

        remainingSpawnCount--;
    }

    public void despawnActivated()
    {
        gunnerSpawn.startAlarmed = false;
        if (spawnedGunner != null)
        {
            spawnedGunner.GetComponent<Gunner>().despawn = true;
            spawnedGunner.GetComponent<Gunner>().alwaysAggro = false;
        }
        stopSpawning = true;
        active = false;
    }
}
