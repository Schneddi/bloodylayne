 using System;
 using System.Collections;
using System.Collections.Generic;
 using System.Diagnostics;
 using _1BloodyLayne.Control;
 using Units.Enemys;
 using Units.Player;
 using Units.Player.Scripts.PlayerControl;
 using UnityEngine;
 using Debug = UnityEngine.Debug;
 using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;
 using Random = UnityEngine.Random;

 namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Gunner : MonoBehaviour
    {
        EnemyStats stats;
        public float speed, jumpforce, jumpLimit, initialWaitTime;
        bool hitActive1, hitActive2, hitActive3, hitActive4;
        bool walking, running, idle, aggro, doingAction, deactivated, flipOnCooldown, playerNear; //CurrentStatus
        public bool gunned;
        private struct hitZone
        {
            public hitZone(bool hitActive,Transform hitTransform)
            {
                this.hitActive = hitActive;
                this.hitTransform = hitTransform;
            }
            public bool hitActive;
            public Transform hitTransform;
        };
        private hitZone[] hitZones;
        private Transform playerChar, hit1, hit2, hit3, hit4, heightSensor, groundSensor, bulletSpawn;
        private Vector3 originPoint, targetPoint, patrolWaypoint;
        Animator animator;
        AudioSource[] sounds;
        AudioSource walkSound, runSound, yell1Eng, yell2Eng, yell3Eng, yell4Eng, yell1Ger, yell2Ger, yell3Ger, yell4Ger, aimSound, fireSound, weaponSwitchSound, swordSwingSound, hitSound, jumpSound, climbSound, jumpSwingSound;
        Rigidbody2D rb;
        public GameObject bullet, smoke;
        public GunnerSpawnPoint originGunnerSpawnPoint;
        private float gravityStore;
        public bool janichar, despawn, alwaysAggro;
        private Animator playerAnimations;
        private GameObject patrolWaypointObject;
        private GlobalVariables gv;
        private float dragStore, idleDrag = 30;
        private Coroutine idleWaitRoutine;

        // Use this for initialization
        void Awake()
        {
            playerChar = GameObject.FindWithTag("Player").transform;
            playerAnimations = playerChar.GetComponent<Animator>();
            hit1 = transform.Find("Sensors").Find("Hit1");
            hitZone hitZone1 = new hitZone(hitActive1, hit1);
            hit2 = transform.Find("Sensors").Find("Hit2");
            hitZone hitZone2 = new hitZone(hitActive2, hit2);
            hit3 = transform.Find("Sensors").Find("Hit3");
            hitZone hitZone3 = new hitZone(hitActive3, hit3);
            hit4 = transform.Find("Sensors").Find("Hit4");
            hitZone hitZone4 = new hitZone(hitActive4, hit4);
            hitZones = new []{hitZone1, hitZone2, hitZone3, hitZone4};
            heightSensor = transform.Find("Sensors").Find("HeightSensor");
            groundSensor = transform.Find("Sensors").Find("GroundSensor");
            bulletSpawn = transform.Find("Sensors").Find("BulletSpawn");
            patrolWaypointObject = transform.Find("Sensors").Find("PatrolWaypoint").gameObject;
            patrolWaypoint = patrolWaypointObject.transform.position;
            patrolWaypointObject.transform.SetParent(transform.parent);
            originPoint = transform.position;
            targetPoint = patrolWaypoint;
            stats = GetComponent<EnemyStats>();
            stats.isHuman = true;
            rb = GetComponent<Rigidbody2D>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            sounds = transform.Find("Sounds").GetComponents<AudioSource>();
            walkSound = sounds[0];
            runSound = sounds[1];
            yell1Eng = sounds[2];
            yell2Eng = sounds[3];
            yell3Eng = sounds[4];
            yell4Eng = sounds[5];
            yell1Ger = sounds[6];
            yell2Ger = sounds[7];
            yell3Ger = sounds[8];
            yell4Ger = sounds[9];
            aimSound = sounds[10];
            fireSound = sounds[11];
            weaponSwitchSound = sounds[12];
            swordSwingSound = sounds[13];
            hitSound = sounds[14];
            jumpSound = sounds[15];
            climbSound = sounds[16];
            jumpSwingSound = sounds[17];
            animator = GetComponent<Animator>();
            dragStore = rb.drag;
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                speed *= -1;
                transform.localScale = theScale;
            }
            gravityStore = rb.gravityScale;
            
            deactivated = true;
            rb.drag = idleDrag;
        }

        private void FixedUpdate()
        {
            if (!animator.GetBool("Death") && !deactivated)
            {
                checkDeath();
                
                foreach (hitZone zone in hitZones)
                {
                    if (zone.hitActive)
                    {
                        if (Physics2D.OverlapCircle(zone.hitTransform.position, 0.5f, 1 << 8))
                        {
                            if (playerChar.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeSwing"), DamageTypeToPlayer.MeleeSwing, new Vector2(speed / 10, 0), zone.hitTransform.position, gameObject, Guid.NewGuid()))
                            {
                                hitSound.PlayDelayed(0);
                            }
                        }
                        damageObjects(zone.hitTransform);
                    }
                }
            }
            if (!animator.GetBool("Death") && !doingAction && !deactivated)
            {
                if (!aggro && idle || walking)
                {
                    float detectionRadius = 15f;
                    if (janichar)
                    {
                        detectionRadius = 10f;
                        if (playerAnimations.GetBool("Crouch"))
                        {
                            detectionRadius = 6f;
                        }
                    }

                    if (Physics2D.OverlapCircle(transform.position, detectionRadius, 1 << 8))
                    {
                        if (checkIfPlayerInSight())
                        {
                            StartCoroutine(Pointing());
                        }
                    }
                    if (walking)
                    {
                        if ((transform.position.x - targetPoint.x > 0 && stats.facingRight) || (transform.position.x - targetPoint.x < 0 && !stats.facingRight))
                        {
                            Flip();
                        }
                        rb.AddForce(new Vector2(speed, rb.velocity.y));
                        if (Vector3.Distance(transform.position, targetPoint) < 1)
                        {
                            if (targetPoint == originPoint)
                            {
                                targetPoint = patrolWaypoint;
                            }
                            else
                            {
                                targetPoint = originPoint;
                            }

                            idleWaitRoutine ??= StartCoroutine(IdleWait(10));
                        }
                        else if ((Physics2D.OverlapCircle(heightSensor.transform.position, 0.2f, 1 << 11) ||
                                 Physics2D.OverlapCircle(heightSensor.transform.position, 0.4f, 1 << 14)) && 
                                 Physics2D.OverlapCircle(groundSensor.transform.position, 0.25f, 1 << 11 | 1 << 14) &&
                                 Mathf.Abs(rb.velocity.x) < 1 && playerChar.transform.position.y > transform.position.y &&
                                 !stats.onLadder) 
                        {
                            walking = false;
                            walkSound.Stop();
                            float jumpheight = 8;
                            RaycastHit2D upwardsRay = Physics2D.Raycast(heightSensor.position, Vector2.up, 10,
                                1 << 11 | 1 << 14);
                            if (upwardsRay.collider != null)
                            {
                                if ((upwardsRay.transform.position.y - transform.position.y) * 0.5f > 8)
                                {
                                    jumpheight = (upwardsRay.transform.position.y - transform.position.y) * 0.5f;
                                }
                            }
                            StartCoroutine(Jump(jumpheight, true));
                        }
                    }
                }

                if (aggro && (!Physics2D.OverlapCircle(transform.position, 35f, 1 << 8) || playerChar.GetComponent<PlayerStats>().health == 0) && !alwaysAggro)
                {
                    stopAggro();
                }

                if (aggro && idle)
                {
                    Vector3 richtung;
                    if (stats.facingRight)
                    {
                        richtung = Vector3.right;
                    }
                    else
                    {
                        richtung = Vector3.left;
                    }
                    RaycastHit2D forwardRay = Physics2D.Raycast(new Vector2(transform.position.x + richtung.x * 0.8f, transform.position.y), richtung);
                    if (forwardRay.collider != null)
                    {
                        if (forwardRay.collider.gameObject.GetComponent<EnemyStats>() == null || Mathf.Abs(transform.position.x - forwardRay.transform.position.x) > 3f)
                        {
                            startAggro();
                        }
                        else
                        {
                            FlipCheck();
                        }
                    }
                    else
                    {
                        FlipCheck();
                    }
                }


                if (aggro && running)
                {
                    
                    rb.AddForce(new Vector2(speed * 2f, rb.velocity.y));
                    float attackRadius = 7;
                    if (gunned)
                    {
                        attackRadius = 11;
                    }
                    Vector3 richtung;
                    if (stats.facingRight)
                    {
                        richtung = Vector3.right;
                    }
                    else
                    {
                        richtung = Vector3.left;
                    }

                    RaycastHit2D forwardRay = Physics2D.Raycast(new Vector2(transform.position.x + richtung.x * 0.8f, transform.position.y), richtung);
                    if (forwardRay.collider != null && Physics2D.OverlapCircle(groundSensor.position, 0.25f, 1 << 11))
                    {
                        if (forwardRay.collider.gameObject.GetComponent<EnemyStats>() != null && Mathf.Abs(transform.position.x - forwardRay.transform.position.x) < 3f)
                        {
                            if (forwardRay.collider.gameObject.GetComponent<EnemyStats>().ranged)
                            {
                                running = false;
                                runSound.Stop();
                                StartCoroutine(Jump(10, true));
                            }
                            else
                            {
                                running = false;
                                runSound.Stop();
                                idle = true;
                                if (gunned)
                                {
                                    animator.SetTrigger("GunIdle");
                                }
                                else
                                {
                                    animator.SetTrigger("SwordIdle");
                                }
                            }
                        }
                    }
                    if (running)
                    {
                        if ((Physics2D.OverlapCircle(heightSensor.transform.position, 0.2f, 1 << 11) ||
                            Physics2D.OverlapCircle(heightSensor.transform.position, 0.4f, 1 << 14)) &&
                            Physics2D.OverlapCircle(groundSensor.transform.position, 0.25f, 1 << 11 | 1 << 14) &&
                            Mathf.Abs(rb.velocity.x) < 1 && playerChar.transform.position.y > transform.position.y &&
                            !stats.onLadder) 
                        {
                            running = false;
                            runSound.Stop();
                            float jumpheight = 15;
                            RaycastHit2D upwardsRay = Physics2D.Raycast(heightSensor.position, Vector2.up, 10,
                                1 << 11 | 1 << 14);
                            if (upwardsRay.collider != null)
                            {
                                if ((upwardsRay.transform.position.y - transform.position.y) * 0.5f > 15)
                                {
                                    jumpheight = (upwardsRay.transform.position.y - transform.position.y) * 0.5f;
                                }
                            }
                            StartCoroutine(Jump(jumpheight, true));
                        }
                        else if (checkIfPlayerInSight() && Physics2D.OverlapCircle(transform.position, attackRadius, 1 << 8) && Mathf.Abs(playerChar.position.y - transform.position.y) < 2f)
                        {
                            FlipCheck();
                            runSound.Stop();
                            running = false;
                            if (gunned)
                            {
                                StartCoroutine(Shooting());
                            }
                            else
                            {
                                swordAttack();
                            }
                        }
                        else
                        {
                            FlipCheck();
                        }
                    }
                }

                if (stats.onLadder && rb.gravityScale != 0f)
                {
                    rb.gravityScale = 0f;

                    //climbVelocity = 0;

                    rb.velocity = new Vector2(rb.velocity.x, 0);
                }

                if (!stats.onLadder)
                {
                    rb.gravityScale = gravityStore;

                }
            }
        }

        private void Update()
        {
            if (deactivated)
            {
                if (despawn)
                {
                    initiateDespawn();
                }
                else if (Physics2D.OverlapCircle(transform.position, 45f, 1 << 8))
                {
                    deactivated = false;
                    rb.drag = dragStore;
                    if (initialWaitTime == 0)
                    {
                        initialWaitTime = Random.Range(5, 10);
                    }
                    idleWaitRoutine ??= StartCoroutine(IdleWait(initialWaitTime));
                }
            }
        }

        private void FlipCheck()
        {
            if (!flipOnCooldown)
            {
                if (stats.facingRight && transform.position.x > playerChar.position.x)
                {
                    StartCoroutine(flipCheckCooldown());
                    Flip();
                }
                else if (!stats.facingRight && transform.position.x < playerChar.position.x)
                {
                    StartCoroutine(flipCheckCooldown());
                    Flip();
                }
            }
        }

        private void Flip()
        {
            if (stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
            else if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
        }


        IEnumerator flipCheckCooldown()
        {
            flipOnCooldown = true;
            yield return new WaitForSeconds(0.75f);
            flipOnCooldown = false;
        }

        IEnumerator Pointing()
        {
            FlipCheck();
            walkSound.Stop();
            doingAction = true;
            idle = false;
            walking = false;
            int yellRng = Random.Range(0, 5);
            switch (yellRng)
            {
                case 1:
                    if (gv.Language == 0)
                    {
                        yell1Eng.PlayDelayed(0);
                    }
                    else if (gv.Language == 1)
                    {
                        yell1Ger.PlayDelayed(0);
                    }
                    break;
                case 2:
                    if (gv.Language == 0)
                    {
                        yell2Eng.PlayDelayed(0);
                    }
                    else if (gv.Language == 1)
                    {
                        yell2Ger.PlayDelayed(0);
                    }
                    break;
                case 3:
                    if (gv.Language == 0)
                    {
                        yell3Eng.PlayDelayed(0);
                    }
                    else if (gv.Language == 1)
                    {
                        yell3Ger.PlayDelayed(0);
                    }
                    break;
                case 4:
                    if (gv.Language == 0)
                    {
                        yell4Eng.PlayDelayed(0);
                    }
                    else if (gv.Language == 1)
                    {
                        yell4Ger.PlayDelayed(0);
                    }
                    break;
                default:
                    if (gv.Language == 0)
                    {
                        yell1Eng.PlayDelayed(0);
                    }
                    else if (gv.Language == 1)
                    {
                        yell1Ger.PlayDelayed(0);
                    }
                    break;
            }
            
            if (gunned)
            {
                animator.SetTrigger("GunnerGunPointing");
            }
            else
            {
                animator.SetTrigger("GunnerSwordPointing");
            }
            yield return new WaitForSeconds(1f);
            float alarmRange = 20;
            if (janichar)
            {
                alarmRange = 11;
            }
            RaycastHit2D[] castStar = Physics2D.CircleCastAll(transform.position, alarmRange, Vector2.zero, Mathf.Infinity, 1<<9);
            foreach (RaycastHit2D raycastHit in castStar)
            {
                if (raycastHit.collider.GetComponent<Gunner>() != null)
                {
                    if (!raycastHit.collider.GetComponent<Gunner>().aggro)
                    {
                        raycastHit.collider.GetComponent<Gunner>().startAggro();
                    }
                }
            }
            doingAction = false;
        }

        public void startAggro()
        {
            idleWaitRoutine = null;
            walkSound.Stop();
            idle = false;
            walking = false;
            if (gunned)
            {
                animator.SetTrigger("GunRun");
            }
            else
            {
                animator.SetTrigger("SwordRun");
            }
            runSound.PlayDelayed(0);
            running = true;
            aggro = true;
        }

        IEnumerator Shooting()
        {
            FlipCheck();
            doingAction = true;
            animator.SetTrigger("Aim");
            aimSound.PlayDelayed(0);
            yield return new WaitForSeconds(0.40f);
            if (!animator.GetBool("Death"))
            {
                fireSound.PlayDelayed(0);
                GameObject bulletProjectile = Instantiate(bullet, bulletSpawn.position, Quaternion.identity);
                bulletProjectile.GetComponent<BulletControl>().attackDamage = stats.attacks.GetValueOrDefault("ShotAttack");
                float richtung = -1;
                if (stats.facingRight)
                {
                    richtung = 1;
                }
                bulletProjectile.GetComponent<BulletControl>().speed = new Vector2(richtung * 60, 0);
                Instantiate(smoke, bulletSpawn.position, Quaternion.identity);
                animator.SetTrigger("Fire");
                fireSound.PlayDelayed(0);
            }

            yield return new WaitForSeconds(0.20f);
            if (!animator.GetBool("Death"))
            {
                doingAction = false;
                StartCoroutine(switchToSword());
            }
        }

        private void stopAggro()
        {
            runSound.Stop();
            aggro = false;
            idleWaitRoutine ??= StartCoroutine(IdleWait(10));
        }

        IEnumerator switchToSword()
        {
            doingAction = true;
            gunned = false;
            animator.SetTrigger("TakeSword");
            weaponSwitchSound.PlayDelayed(0);
            yield return new WaitForSeconds(0.5f);
            if (!animator.GetBool("Death"))
            {
                doingAction = false;
                stats.ranged = false;
                startAggro();
            }
        }

        IEnumerator Jump(float height, bool setAnimation)
        {
            if (height > jumpLimit)
            {
                height = jumpLimit;
            }
            doingAction = true;
            jumpSound.PlayDelayed(0);
            if (setAnimation)
            {
                animator.SetTrigger("Jump");
            }
            yield return new WaitForSeconds(0.15f);
            if (!animator.GetBool("Death"))
            {
                rb.AddForce(new Vector2(0, height * jumpforce));
                yield return new WaitForSeconds(0.1f);
            }

            if (!animator.GetBool("Death"))
            {
                rb.AddForce(new Vector2(speed * 20, jumpforce));
                yield return new WaitForSeconds(0.1f);
            }

            if (!animator.GetBool("Death"))
            {
                rb.AddForce(new Vector2(speed * 10, 0));
                yield return new WaitForSeconds(0.05f);
            }
            if (!animator.GetBool("Death") && setAnimation)
            {
                doingAction = false;
                if (aggro)
                {
                    startAggro();
                }
                else
                {
                    idleWaitRoutine ??= StartCoroutine(IdleWait(0));
                }
            }
        }

        private void swordAttack()
        {
            FlipCheck();
            doingAction = true;
            int randomInt = Random.Range(0, 2);
            if (randomInt == 0)
            {
                StartCoroutine(DashAttack());
            }
            else
            {
                StartCoroutine(JumpAttack());
            }
        }
        
        IEnumerator JumpAttack()//Gibt Schussfrequenz und Animationen an
        {
            yield return new WaitForSeconds(0.25f);
            float jumpheigth = 6.5f + Mathf.Abs(transform.position.y - playerChar.transform.position.y);
            StartCoroutine(Jump(jumpheigth, false));
            yield return new WaitForSeconds(0.1f);
            rb.AddForce(new Vector2(speed * 15f, 0));
            yield return new WaitForSeconds(0.2f);
            if (!animator.GetBool("Death"))
            {
                swordSwingSound.PlayDelayed(0);
                animator.SetTrigger("JumpSwing");
                jumpSwingSound.PlayDelayed(0);
                rb.AddForce(new Vector2(speed * 25f, -jumpforce));
                hitZones[0].hitActive = true;
                yield return new WaitForSeconds(0.05f);
                hitZones[1].hitActive = true;
                yield return new WaitForSeconds(0.02f);
                hitZones[2].hitActive = true;
                yield return new WaitForSeconds(0.02f);
                hitZones[3].hitActive = true;
                yield return new WaitForSeconds(0.23f);
                hitZones[0].hitActive = false;
                hitZones[1].hitActive = false;
                hitZones[3].hitActive = false;
            }
            yield return new WaitForSeconds(0.45f);
            hitZones[2].hitActive = false;
            if (!animator.GetBool("Death"))
            {
                doingAction = false;
                startAggro();
            }
        }

        IEnumerator IdleWait(float seconds)
        {
            walkSound.Stop();
            idle = true;
            walking = false;
            if (gunned)
            {
                animator.SetTrigger("GunIdle");
            }
            else
            {
                animator.SetTrigger("SwordIdle");
            }
            yield return new WaitForSeconds(seconds);
            idleWaitRoutine = null;
            if (!Physics2D.OverlapCircle(transform.position, 35f, 1 << 8) && !Physics2D.OverlapCircle(originPoint, 35f, 1 << 8))
            {
                deactivated = true;
                rb.drag = idleDrag;
                transform.position = originPoint;
            }
            else if (!animator.GetBool("Death") && idle && !aggro)
            {
                idle = false;
                walking = true;
                walkSound.PlayDelayed(0);
                if (gunned)
                {
                    animator.SetTrigger("GunWalk");
                }
                else
                {
                    animator.SetTrigger("SwordWalk");
                }
            }
        }
        
        
        IEnumerator DashAttack()
        {
            yield return new WaitForSeconds(0.25f);
            swordSwingSound.PlayDelayed(0);
            animator.SetTrigger("DashSwing");
            yield return new WaitForSeconds(0.15f);
            rb.AddForce(new Vector2(speed * 20f, 0));
            yield return new WaitForSeconds(0.15f);
            if (!animator.GetBool("Death"))
            {
                rb.AddForce(new Vector2(speed * 30f, 0));
                hitZones[1].hitActive = true;
                hitZones[2].hitActive = true;
            }
            yield return new WaitForSeconds(0.45f);
            hitZones[1].hitActive = false;
            hitZones[2].hitActive = false;
            if (!animator.GetBool("Death"))
            {
                doingAction = false;
                startAggro();
            }
        }

        private void initiateDespawn()
        {
            if (originGunnerSpawnPoint != null)
            {
                originGunnerSpawnPoint.remainingSpawnCount++;
            }
            Destroy(patrolWaypointObject);
            Destroy(gameObject);
        }

        private void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }
        
        private bool checkIfPlayerInSight()
        {
            if (!playerChar.GetComponent<PlatformerCharacter2D>().dead)
            {
                Vector3 direction = playerChar.position - transform.position;
                float detectionRange = 8;
                if (janichar)
                    detectionRange = 5;
                if (!aggro && direction.x < 0 && stats.facingRight && Vector3.Distance(playerChar.position, transform.position) > detectionRange)
                {
                    return false;
                }
                else if (!aggro && direction.x > 0 && !stats.facingRight && Vector3.Distance(playerChar.position, transform.position) > detectionRange)
                {
                    return false;
                }
                else if(janichar && playerAnimations.GetBool("Crouch") && !aggro && direction.x < 0 && stats.facingRight && Vector3.Distance(playerChar.position, transform.position) > 3)
                {
                    return false;
                }
                else if(janichar && playerAnimations.GetBool("Crouch") && !aggro && direction.x > 0 && !stats.facingRight && Vector3.Distance(playerChar.position, transform.position) > 3)
                {
                    return false;
                }
            
                RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, direction);
                foreach (RaycastHit2D hitter in hit)
                {
                    if (hitter.transform.CompareTag("Ground"))
                    {
                        break;
                    }
                    if (hitter.transform.CompareTag("Object"))
                    {
                        break;
                    }
                    if (hitter.transform.CompareTag("PowerUp"))
                    {
                        break;
                    }
                    if (hitter.transform.CompareTag("Player"))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                return false;
            }
        }

        private void Death()
        {
            stats.dead = true;
            walkSound.Stop();
            runSound.Stop();
            walking = false;
            running = false;
            animator.SetTrigger("DeathTrigger");
            animator.SetBool("Death", true);
            GetComponent<HumanDeathSequence>().Death();
        }

        void damageObjects(Transform hit)
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(hit.position, 0.5f, 1 << 14);
            {
                foreach (Collider2D col in cols)
                {
                    if (col.gameObject.GetComponent<ObjektStats>() != null)
                    {
                        if (col.gameObject.GetComponent<ObjektStats>().destructable)
                        {
                            col.gameObject.GetComponent<ObjektStats>().hitPoints -= stats.attacks.GetValueOrDefault("MeleeSwing");
                            if (col.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                            {
                                col.gameObject.GetComponent<ObjektStats>().startDeath();
                            }
                        }
                    }
                }
            }
        }
    }
}