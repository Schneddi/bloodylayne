﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Enemys;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Skelett : MonoBehaviour
    {
        public bool facingRight = true;
        public bool active = true, haufen = true, laufen=false, startStanding, inactive; //active: skelett ist kurz deaktiviert, wenn es zerstört wurde.
        
        private EnemyStats stats;
        private float speed = 1.5f, maxSpeed = 16;
        private Rigidbody2D rb;
        private BoxCollider2D bCollider;
        private Transform detector, player, hitDetector;    // A position marking where to check if the player is grounded.
        private AudioSource[] sounds;
        private AudioSource awake, walk, hit, death;
        private Animator animator;
        private GameObject Player;
        private PlatformerCharacter2D playerChar;
        private float detectionRange = 10f;
        private float dragStore, idleDrag = 30;
        private GlobalVariables gv;

        // Use this for initialization
        void Start()
        {
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            maxSpeed += gv.difficulty;
            animator = GetComponent<Animator>();
            detector = transform.Find("Detector");
            hitDetector = transform.Find("HitDetector");
            player = GameObject.FindWithTag("Player").transform;
            rb = GetComponent<Rigidbody2D>();
            bCollider = GetComponent<BoxCollider2D>();
            stats = GetComponent<EnemyStats>();
            stats.health = 0;
            Player = GameObject.FindWithTag("Player");
            playerChar = Player.GetComponent<PlatformerCharacter2D>();
            sounds = GetComponents<AudioSource>();
            awake = sounds[0];
            walk = sounds[1];
            hit = sounds[2];
            death = sounds[3];
            walk.enabled = false;
            dragStore = rb.drag;
            rb.drag = idleDrag;
            if (!facingRight)
            {
                Flip();
            }

            if (startStanding)
            {
                active = false;
                haufen = true;
                StartCoroutine(Aufwecken());
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (!haufen && active)
            {
                checkDeath();
            }
            if (active)
            {
                checkPlayerNear();
                if (Physics2D.Raycast(detector.position, Vector2.right, detectionRange, 1<<8) || Physics2D.Raycast(detector.position, Vector2.left, detectionRange, 1<<8))//ist der Spieler (=1<<9; Layermaske Layer 9)rechts oder links, in der gleichen Höhe
                {
                    if (haufen == true)
                    {
                        active = false;
                        StartCoroutine(Aufwecken());
                    }
                    else if(!haufen && active==true)
                    {
                        laufen = true;
                        if ((detector.position.x > player.position.x) && facingRight == true)
                        {
                            Flip();
                        }
                        else if ((detector.position.x < player.position.x) && facingRight == false)
                        {
                            Flip();
                        }
                    }

                    
                }
                else if (Physics2D.Raycast(new Vector2(detector.position.x, detector.position.y-0.7f), Vector2.right, detectionRange, 1 << 8) || Physics2D.Raycast(new Vector2(detector.position.x, detector.position.y - 0.7f), Vector2.left, detectionRange, 1 << 8))
                {
                    if (!haufen && active == true)
                    {
                        laufen = true;
                        if ((detector.position.x > player.position.x) && facingRight == true)
                        {
                            Flip();
                        }
                        else if ((detector.position.x < player.position.x) && facingRight == false)
                        {
                            Flip();
                        }
                    }

                }
                else
                {
                    laufen = false;
                }
            }
        }
        private void FixedUpdate()
        {
            if (laufen == true)
            {
                animator.SetBool("Walk", true);
                walk.enabled = true;
                if(rb.velocity.x < maxSpeed)
                {
                    rb.velocity = new Vector2(rb.velocity.x + speed, rb.velocity.y);
                }

                //Spieler Hit
                if (Physics2D.OverlapCircle(hitDetector.position, 0.7f, 1 << 8))
                {
                    if (facingRight)
                    {
                        rb.velocity = new Vector2(-13f, rb.velocity.y);
                        if (playerChar.Hit(stats.attacks.GetValueOrDefault("MeleeSwing"), DamageTypeToPlayer.MeleeSwing, new Vector2(5f, 0), hitDetector.position, gameObject, new Guid()))
                        {
                            hit.PlayDelayed(0);
                        }
                    }
                    else
                    {
                        rb.velocity = new Vector2(+13f, rb.velocity.y);
                        if (playerChar.Hit(stats.attacks.GetValueOrDefault("MeleeSwing"), DamageTypeToPlayer.MeleeSwing, new Vector2(-5f, 0), hitDetector.position, gameObject, new Guid()))
                        {
                            hit.PlayDelayed(0);
                        }
                    }
                }
            }
            else if(!inactive)
            { 
                animator.SetBool("Walk", false);
                walk.enabled = false;
            }
        }
        void Flip()//z.B. in andere Richtung gucken
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            speed *= -1;
            transform.localScale = theScale;
        }
        
        private void checkPlayerNear()
        {
            if (Physics2D.OverlapCircle(transform.position, 30f, 1<<8))
            {
                rb.drag = dragStore;
            }
            else
            {
                rb.drag = idleDrag;
            }
        }

        void checkDeath()
        {
            if (stats.health <= 0 && !haufen)
            {
                StartCoroutine(Death());
            }
        }
        IEnumerator Aufwecken()
        {
            if (haufen == true)
            {
                haufen = false;
                bCollider.size = new Vector2(bCollider.size.x, 4f);
                bCollider.offset = new Vector2(bCollider.offset.x, bCollider.offset.y + 1.2f);
                animator.SetTrigger("Awake");
                awake.PlayDelayed(0);
                float awakeningTime = 1.5f;
                awakeningTime -= gv.difficulty * 0.25f;
                yield return new WaitForSeconds(awakeningTime);
                rb.gravityScale = 1;
                detectionRange = 16f;
                stats.health = stats.maxHealth;
                if (!inactive)
                {
                    active = true;
                }
            }
        }
        IEnumerator Death()
        {
            rb.gravityScale = 15;
            active = false;
            laufen = false;
            haufen = true;
            animator.SetTrigger("Death");
            death.PlayDelayed(0);
            bCollider.size = new Vector2(bCollider.size.x, 2f);
            bCollider.offset = new Vector2(bCollider.offset.x, bCollider.offset.y - 1.2f);
            float dyingTime = 1.5f;
            dyingTime -= gv.difficulty * 0.25f;
            yield return new WaitForSeconds(dyingTime);
            detectionRange = 10f;
            active = true;
        }
    }
}
