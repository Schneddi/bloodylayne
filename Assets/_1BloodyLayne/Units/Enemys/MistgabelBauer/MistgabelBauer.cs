﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _1BloodyLayne.Control;
using Units.Enemys;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;
using Random = UnityEngine.Random;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class MistgabelBauer : MonoBehaviour
    {
        EnemyStats stats;
        public bool startInactive, ignorePlayerDistance;
        public GameObject hitEffect;
        private Animator animator;
        public float range, speed, jumpforce, jumpLimit, patrZeit;
        Rigidbody2D rb;
        private Transform damageCheck1, damageCheck2, flipCheck, ceilingCheck, groundSensor, cliffSensor;    //damageCheck1 -> check in front der mistGabel, flipCheck -> noch vor damger, damit der bauer nicht mit anderen Bauern zusammenstößt
        int layerMask = 1 << 8;
        GameObject Player;
        private PlatformerCharacter2D playerChar;
        bool turn, attacking, chaseMode, shouting, jumping, inactive, playerNear;
        AudioSource hitSound, runSound, jumpSound, topAttackSound;
        AudioSource[] shoutSounds, swingSounds;
        float oldXVelocity;
        private Coroutine patrolCoroutine;
        private float dragStore, idleDrag = 30;
        private GlobalVariables gv;

        // Use this for initialization
        void Start()
        {
            stats = GetComponent<EnemyStats>();
            animator = GetComponent<Animator>();
            damageCheck1 = transform.Find("damageCheck1");
            damageCheck2 = transform.Find("damageCheck2");
            flipCheck = transform.Find("flipCheck");
            ceilingCheck = transform.Find("CeilingCheck");
            groundSensor = transform.Find("GroundSensor");
            cliffSensor = transform.Find("CliffSensor");
            rb = GetComponent<Rigidbody2D>();
            Player = GameObject.FindWithTag("Player");
            playerChar = Player.GetComponent<PlatformerCharacter2D>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            speed += gv.difficulty * 10;
            if (!stats.facingRight)
            {
                Flip();
            }
            else
            {
                stats.facingRight = !stats.facingRight;
            }
            stats.isHuman = true;
            AudioSource[] audioSources = GetComponents<AudioSource>();
            hitSound = audioSources[0];
            shoutSounds = new AudioSource[4];
            shoutSounds[0] = audioSources[1];
            shoutSounds[1] = audioSources[2];
            shoutSounds[2] = audioSources[3];
            shoutSounds[3] = audioSources[4];
            runSound = audioSources[5];
            jumpSound = audioSources[6];
            topAttackSound = audioSources[7];
            swingSounds  = new AudioSource[5];
            swingSounds[0] = audioSources[8];
            swingSounds[1] = audioSources[9];
            swingSounds[2] = audioSources[10];
            swingSounds[3] = audioSources[11];
            swingSounds[4] = audioSources[12];
            dragStore = rb.drag;
            rb.drag = idleDrag;
            if (startInactive)
            {
                inactive = true;
            }
            if (ignorePlayerDistance)
            {
                rb.drag = dragStore;
            }
            oldXVelocity = 10;
        }

        private void FixedUpdate()
        {
            checkDeath();
            if (!animator.GetBool("Death") && !inactive)
            {
                if (!playerNear && !ignorePlayerDistance)
                {
                    checkPlayerNear();
                }
                if (!attacking && !shouting && !jumping && (playerNear || ignorePlayerDistance))
                {
                    if (!chaseMode) {
                        rb.AddForce(new Vector2(speed, 0));
                        layerMask = 1 << 9; //layer Ebene 9 = Enemy
                        if (Physics2D.OverlapCircle(flipCheck.position, 0.35f, layerMask))
                        {
                            Flip();
                        }
                        
                        if (Physics2D.OverlapCircle(transform.position, 12.5f, 1 << 8) || stats.health < stats.maxHealth)
                        {
                            checkIfLarryInSight();
                        }
                        
                        if (!Physics2D.OverlapCircle(cliffSensor.position, 0.1f, 1 << 11))
                        {
                            Flip();
                            if (patrolCoroutine != null)
                            {
                                StopCoroutine(patrolCoroutine);
                                patrolCoroutine = StartCoroutine(Patroulieren());
                            }
                        }
                    }
                    else
                    {
                        rb.AddForce(new Vector2(speed * 1.5f, 0));
                        Vector3 richtung;
                        if (stats.facingRight)
                        {
                            richtung = Vector3.left;
                        }
                        else
                        {
                            richtung = Vector3.right;
                        }
                        RaycastHit2D forwardRay = Physics2D.Raycast(new Vector2(transform.position.x + richtung.x * 1.5f, transform.position.y), richtung);
                        if (forwardRay.collider != null)
                        {
                            if (forwardRay.collider.gameObject.GetComponent<EnemyStats>() != null && Mathf.Abs(transform.position.x - forwardRay.transform.position.x) < 3f)
                            {
                                if (forwardRay.collider.gameObject.GetComponent<EnemyStats>().ranged)
                                {
                                    chaseMode = false;
                                    jumping = true;
                                    runSound.Stop();
                                    StartCoroutine(Jump(10));
                                }
                                if (Physics2D.OverlapCircle(flipCheck.position, 0.35f, layerMask))
                                {
                                    Flip();
                                }
                            }
                        }
                        if (!Physics2D.OverlapCircle(transform.position, 17f, 1 << 8))
                        {
                            chaseMode = false;
                            runSound.Stop();
                            patrolCoroutine = StartCoroutine(Patroulieren());
                        }
                        
                        layerMask = 1 << 8;
                        if (Physics2D.OverlapCircle(flipCheck.position, 30f, layerMask))
                        {
                            if (stats.facingRight && transform.position.x < playerChar.transform.position.x)
                            {
                                Flip();
                            }
                            else if(!stats.facingRight && transform.position.x > playerChar.transform.position.x)
                            {
                                Flip();
                            }
                        }
                        if (Physics2D.OverlapCircle(groundSensor.transform.position, 0.2f, 1 << 11 | 1 << 14) && Physics2D.OverlapCircle(flipCheck.transform.position, 0.3f, 1 << 11 | 1 << 14) || Physics2D.OverlapCircle(damageCheck1.transform.position, 0.3f, 1 << 11 | 1 << 14) && Mathf.Abs(rb.velocity.x) > 1 && Player.transform.position.y > transform.position.y)
                        {
                            chaseMode = false;
                            jumping = true;
                            runSound.Stop();
                            float jumpheight = 10;
                            RaycastHit2D upwardsRay= Physics2D.Raycast(flipCheck.position, Vector2.up, 10, 1 << 11 | 1 << 14);
                            if (upwardsRay.collider != null)
                            {
                                if ((upwardsRay.transform.position.y - transform.position.y) > 10)
                                {
                                    jumpheight = (upwardsRay.transform.position.y - transform.position.y);
                                }
                            }
                            StartCoroutine(Jump(jumpheight));
                        }
                        else if (Mathf.Abs(rb.velocity.x) < 0.5f && Physics2D.OverlapCircle(groundSensor.transform.position, 0.2f, 1 << 11 | 1 << 14) || Physics2D.OverlapCircle(damageCheck1.transform.position, 0.3f, 1 << 11 | 1 << 14) && Mathf.Abs(Player.transform.position.x - transform.position.x) > 0.5f)
                        {
                            if (oldXVelocity < Mathf.Abs(rb.velocity.x))
                            {
                                chaseMode = false;
                                jumping = true;
                                runSound.Stop();
                                float jumpheight = 2;
                                StartCoroutine(Jump(jumpheight));
                            }
                            oldXVelocity = Mathf.Abs(rb.velocity.x);
                        }
                    }


                    if (gv.difficulty >= 3)
                    {
                        layerMask = 1 << 8;
                        if ((Physics2D.OverlapCircle(damageCheck1.position, 0.3f, layerMask) || Physics2D.OverlapCircle(damageCheck2.position, 0.3f, layerMask)) && !attacking)
                        {
                            if (playerChar.Hit(stats.attacks.GetValueOrDefault("MeleeStab"), DamageTypeToPlayer.MeleeStab, new Vector2(25 * Mathf.Sign(speed), 0), damageCheck1.position, gameObject, Guid.NewGuid()))
                            {
                                Instantiate(hitEffect, playerChar.transform.position, Quaternion.identity);
                                hitSound.PlayDelayed(0);
                            }
                        }
                        if (chaseMode)
                        {
                            layerMask = 1 << 14; //layer Ebene 14 = Objects
                            Collider2D[] cols = Physics2D.OverlapCircleAll(flipCheck.position, 0.35f, layerMask);
                            {
                                foreach (Collider2D col in cols)
                                {
                                    if (col.gameObject.GetComponent<ObjektStats>() != null)
                                    {
                                        if (col.gameObject.GetComponent<ObjektStats>().destructable)
                                        {
                                            col.gameObject.GetComponent<ObjektStats>().hitPoints -= stats.attacks.GetValueOrDefault("MeleeStab");
                                            if (col.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                                            {
                                                col.gameObject.GetComponent<ObjektStats>().startDeath();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        layerMask = 1 << 8;//layer Ebene 8 = Player
                        if (Physics2D.OverlapCircle(damageCheck1.position, 0.2f, layerMask) || Physics2D.OverlapCircle(damageCheck2.position, 0.2f, layerMask) || Physics2D.OverlapCircle(flipCheck.position, 0.2f, layerMask))
                        {
                            StartCoroutine(FrontAttack());
                        }
                        else if (Physics2D.OverlapCircle(ceilingCheck.position, 0.2f, layerMask))
                        {
                            StartCoroutine(TopAttack());
                        }
                    }
                    if (Physics2D.OverlapCircle(flipCheck.position, 0.35f, 1 << 9))
                    {
                        if (stats.facingRight)
                        {
                            rb.velocity = new Vector2(Mathf.Abs(rb.velocity.x), rb.velocity.y);
                        }
                        else
                        {
                            rb.velocity = new Vector2(-Mathf.Abs(rb.velocity.x), rb.velocity.y);
                        }
                        Flip();
                    }
                }
            }
        }
        void Flip()
        {
            if (!turn)
            {
                stats.facingRight = !stats.facingRight;
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                speed *= -1;
                StartCoroutine(flipCoolDown());
            }
        }
        
        
        private IEnumerator FrontAttack()
        {
            int rng = Random.Range(0, 5);
            swingSounds[rng].PlayDelayed(0);
            attacking = true;
            animator.SetTrigger("MeleeAttack");
            runSound.Stop();
            yield return new WaitForSeconds(0.22925f);
            
            if (!stats.dead)
            {
                layerMask = 1 << 8;
                if (Physics2D.OverlapCircle(damageCheck1.position, 0.35f, layerMask) || Physics2D.OverlapCircle(damageCheck2.position, 0.35f, layerMask) || Physics2D.OverlapCircle(flipCheck.position, 0.35f, layerMask))
                {
                    if (playerChar.Hit(stats.attacks.GetValueOrDefault("MeleeStab"), DamageTypeToPlayer.MeleeStab, new Vector2(25 * Mathf.Sign(speed), 0), damageCheck1.position, gameObject, Guid.NewGuid()))
                    {
                        Instantiate(hitEffect, playerChar.transform.position, Quaternion.identity);
                        hitSound.PlayDelayed(0);
                    }
                }
                
                
                layerMask = 1 << 14;
                
                Collider2D[] cols = Physics2D.OverlapCircleAll(damageCheck1.position, 0.35f, layerMask);
                Collider2D[] cols2 = Physics2D.OverlapCircleAll(damageCheck2.position, 0.35f, layerMask);
                Collider2D[] collider2Ds = cols.Concat(cols2).ToArray();
                foreach (Collider2D col in collider2Ds)
                {
                    if (col.gameObject.GetComponent<ObjektStats>() != null)
                    {
                        if (col.gameObject.GetComponent<ObjektStats>().destructable)
                        {
                            col.gameObject.GetComponent<ObjektStats>().hitPoints -= stats.attacks.GetValueOrDefault("MeleeStab");
                            hitSound.PlayDelayed(0);
                            if (col.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                            {
                                col.gameObject.GetComponent<ObjektStats>().startDeath();
                            }
                        }
                    }
                }
                yield return new WaitForSeconds(0.22925f);
            }
            if (!stats.dead)
            {
                attacking = false;
                runSound.PlayDelayed(0);
                animator.SetTrigger("RunFast");
            }
        }
        
        private IEnumerator TopAttack()
        {
            attacking = true;
            topAttackSound.PlayDelayed(0);
            rb.velocity = new Vector2(0, rb.velocity.y);
            animator.SetTrigger("TopAttack");
            yield return new WaitForSeconds(0.35f);
            layerMask = 1 << 8;//layer Ebene 8 = Player
            if (Physics2D.OverlapCircle(ceilingCheck.position, 0.2f, layerMask))
            {
                if (playerChar.Hit(stats.attacks.GetValueOrDefault("TopAttack"), DamageTypeToPlayer.ImpaledFeet, new Vector2(0, 15f), ceilingCheck.position, gameObject, Guid.NewGuid()))
                {
                    Instantiate(hitEffect, ceilingCheck.position, Quaternion.identity);
                    hitSound.PlayDelayed(0);
                }
            }
            yield return new WaitForSeconds(0.35f);
            if (!animator.GetBool("Death"))
            {
                attacking = false;
                if (chaseMode)
                {
                    runSound.PlayDelayed(0);
                    animator.SetTrigger("RunFast");
                }
                else
                {
                    StartCoroutine(Shout());
                }
            }
        }

        public IEnumerator Patroulieren()
        {
            animator.SetTrigger("Run");
            inactive = false;
            yield return new WaitForSeconds(patrZeit);
            if (animator != null)
            {
                checkPlayerNear();
                if (!animator.GetBool("Death") && !chaseMode && playerNear)
                {
                    Flip();
                    patrolCoroutine = StartCoroutine(Patroulieren());
                }
            }
        }
        
        private void checkPlayerNear()
        {
            if (Physics2D.OverlapCircle(transform.position, 30f, 1<<8))
            {
                playerNear = true;
                rb.drag = dragStore;
                patrolCoroutine = StartCoroutine(Patroulieren());
            }
            else
            {
                rb.drag = idleDrag;
                playerNear = false;
            }
        }

        private void checkIfLarryInSight()
        {
            Vector3 richtung = playerChar.transform.position - transform.position;
            RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
            foreach (RaycastHit2D hitter in hit)
            {
                if (hitter.transform.tag == "Ground")
                {
                    break;
                }
                if (hitter.transform.tag == "Player")
                {
                    StartCoroutine(Shout());
                    break;
                }
            }
        }

        IEnumerator Shout()
        {
            shouting = true;
            if (playerChar.transform.position.x > rb.position.x && stats.facingRight && !turn)
            {
                Flip();
            }
            else if (playerChar.transform.position.x < rb.position.x && !stats.facingRight && !turn)
            {
                Flip();
            }
            shoutSounds[Random.Range(0,3)].PlayDelayed(0);
            animator.SetTrigger("Shout");
            rb.drag = dragStore;
            yield return new WaitForSeconds(0.8f);
            if (!animator.GetBool("Death"))
            {
                runSound.PlayDelayed(0);
                animator.SetTrigger("RunFast");
                shouting = false;
                chaseMode = true;
            }
        }

        IEnumerator flipCoolDown()
        {
            turn = true;
            yield return new WaitForSeconds(0.5f);
            turn = false;
        }
        
        IEnumerator Jump(float height)//Gibt Schussfrequenz und Animationen an
        {
            if (height > jumpLimit)
            {
                height = jumpLimit;
            }
            jumpSound.PlayDelayed(0);
            animator.SetTrigger("Jump");
            yield return new WaitForSeconds(0.1f);
            rb.AddForce(new Vector2(0, height * jumpforce));
            yield return new WaitForSeconds(0.2f);
            rb.AddForce(new Vector2(speed * 20, 0));
            yield return new WaitForSeconds(0.15f);
            if (!animator.GetBool("Death"))
            {
                runSound.PlayDelayed(0);
                jumping = false;
                chaseMode = true;
                animator.SetTrigger("RunFast");
            }
        }
        
        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }

        private void Death()
        {
            transform.Find("BurningPitchfork").GetComponent<ParticleSystem>().Stop();
            stats.dead = true;
            stats.facingRight = !stats.facingRight;
            runSound.Stop();
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            GetComponent<HumanDeathSequence>().Death();
        }
    }
}
