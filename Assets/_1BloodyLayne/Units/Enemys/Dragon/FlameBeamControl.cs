using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class FlameBeamControl : MonoBehaviour
    {
        public float startDelay;
        GameObject player;
        Vector2 direction;
        AudioSource hitSound;
        private bool damaging;
        // Use this for initialization
        void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player");
            direction = transform.position - transform.Find("DirectionCheck").transform.position;
            hitSound = GetComponent<AudioSource>();
            StartCoroutine(startFlameBreath(startDelay));
        }

        // Update is called once per frame
        private void Update()
        {
            if (damaging)
            {
                damaging = false;
                if (Physics2D.Raycast(transform.position, direction.normalized, 30f, 1 << 8))
                {
                    if (!hitSound.isPlaying)
                    {
                        hitSound.PlayDelayed(0);
                    }
                    player.GetComponent<PlatformerCharacter2D>().HitBeam(20, DamageTypeToPlayer.Burned,new Vector2(direction.normalized.x*300, direction.normalized.y * 300), player.transform.position, gameObject, Guid.NewGuid());
                }
                else
                {
                    hitSound.Stop();
                }
            }
            if (hitSound.isPlaying && player.GetComponent<Animator>().GetBool("Death"))
            {
                hitSound.Stop();
            }
        }

        IEnumerator startFlameBreath(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            damaging=true;
        }
    }
}
