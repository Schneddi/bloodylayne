
using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    public class FireArtillery : MonoBehaviour
    {
        GameObject Player;
        private PlatformerCharacter2D player;
        public GameObject burnEffect;
        Rigidbody2D rbLarry;
        Rigidbody2D rb;
        Vector2 richtung;
        public int attackDamage = 35;
        private bool died;

        // Use this for initialization
        void Start()
        {
            //rb.velocity = Vector2.Scale(richtung.normalized, new Vector2(totalDuration,totalDuration).normalized);
            Destroy(gameObject, 14f);
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (!died)
            {
                if (Physics2D.OverlapCircle(transform.position, 0.3f, 1 << 11))
                {
                    Instantiate(burnEffect, transform.position, Quaternion.identity);
                    StartCoroutine(Death());
                }
            }
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!died)
            {
                if (other.gameObject.CompareTag("Ground"))
                {
                    Instantiate(burnEffect, other.GetContact(0).point, Quaternion.identity);
                    StartCoroutine(Death());
                }
                else if (other.gameObject.CompareTag("Player"))
                {
                    if (player.Hit(attackDamage, DamageTypeToPlayer.Burned, other.GetContact(0).point, gameObject, Guid.NewGuid()))
                    {
                        Instantiate(burnEffect, other.GetContact(0).point, Quaternion.identity);
                    }
                    StartCoroutine(Death());
                }
            }
        }
        public void launch(float x, float y)
        {
            Player = GameObject.FindWithTag("Player");
            player = Player.GetComponent<PlatformerCharacter2D>();
            rb = GetComponent<Rigidbody2D>();
            rb.velocity = new Vector2(x, y);
            setRotation();
        }

        private void setRotation()
        {
            Vector3 direction = transform.position - Player.transform.position;
            Quaternion q = Quaternion.LookRotation(direction);
            q.y = 0;
            q.x = 0;
            q.z += 100;
            transform.rotation = q;
        }

        private IEnumerator Death()
        {
            died = true;
            ParticleSystem[] pSystems = GetComponentsInChildren<ParticleSystem>();
            GetComponent<BoxCollider2D>().enabled = false;
            foreach (ParticleSystem pSystem in pSystems)
            {
                pSystem.Stop();
            }
            GetComponentInChildren<UnityEngine.Rendering.Universal.Light2D>().enabled = false;
            yield return new WaitForSeconds(2f);
            Destroy(gameObject);
        }

    }
}