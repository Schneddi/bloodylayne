using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    public class BurnEffect : MonoBehaviour
    {
        public bool noDespawn;
        public int attackDamage;
        public float damageRadius;
        public GameObject fireHit;
        
        private PlatformerCharacter2D larry;
        private bool damaging = true;
        private bool startDimming, startLight;
        private Light2D[] m_Lightsources;
        private float[] startIntensyties;
        private Guid guid = Guid.NewGuid();
        
        // Start is called before the first frame update
        void Start()
        {
            larry = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            StartCoroutine(damageTickerDelay());
            StartCoroutine(dealayAndDie());
            m_Lightsources = GetComponentsInChildren<Light2D>();
            startIntensyties = new float[m_Lightsources.Length];
            for (int i=0; i< m_Lightsources.Length;i++)
            {
                startIntensyties[i] = m_Lightsources[i].intensity;
                m_Lightsources[i].intensity = 0;
            }
            startLight = true;
        }
        
        void Update()
        {
            if (startLight)
            {
                for (int i=0; i< m_Lightsources.Length;i++)
                {
                    m_Lightsources[i].intensity += 1f * Time.deltaTime;
                    if (m_Lightsources[i].intensity > startIntensyties[i])
                    {
                        m_Lightsources[i].intensity = startIntensyties[i];
                        startLight = false;
                    }
                }
            }
            if (startDimming)
            {
                foreach (Light2D lightsource in m_Lightsources)
                {
                    lightsource.intensity -= 1f * Time.deltaTime;
                    if (m_Lightsources[0].intensity < 0)
                    {
                        m_Lightsources[0].intensity = 0;
                    }
                }
            }
        }

        IEnumerator damageTickerDelay()
        {
            yield return new WaitForSeconds(0.7f);
            StartCoroutine(damageTicker());
        }

        IEnumerator damageTicker()
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, damageRadius, 1 << 8);
            foreach (Collider2D col in cols)
            {
                if (col.name == "Player" && Mathf.Abs(col.transform.position.y - transform.position.y) < 1.75f)
                {
                    larry.ForcedHit(attackDamage, DamageTypeToPlayer.Burned, col.transform.position, gameObject, guid);
                    if (!larry.dead)
                    {
                        Instantiate(fireHit, col.transform.position, Quaternion.identity);
                    }
                }
            }
            yield return new WaitForSeconds(0.10f);
            if (damaging)
            {
                StartCoroutine(damageTicker());
            }
        }

        IEnumerator dealayAndDie()
        {
            if(!noDespawn){
                yield return new WaitForSeconds(4f);
                GetComponent<ParticleSystem>().Stop();
                GetComponents<AudioSource>()[0].Stop();
                yield return new WaitForSeconds(1f);
                damaging = false;
                startDimming = true;
                yield return new WaitForSeconds(1f);
                Destroy(gameObject);
            }
        }
    }
}