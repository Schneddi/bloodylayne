using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class WindBlastControl : MonoBehaviour
{
    public float windStrength;

    private Rigidbody2D playerRb;
    private PlatformerCharacter2D larry;
    // Start is called before the first frame update
    void Start()
    {
        playerRb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
        larry = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
        Destroy(gameObject, 2);
        larry.InitiatePushedTimer(2);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        playerRb.AddForce(new Vector2(windStrength * Time.deltaTime,0));
    }
}
