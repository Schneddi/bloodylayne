using System;
using System.Collections;
using System.Collections.Generic;
using UI.PlayerUI;
using Units.Enemys;
using Unity.Mathematics;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;
using Random = System.Random;

public class Dragon : MonoBehaviour
{
    public float speed, bottomDamagerCheckLength;
    public GameObject windEffect, firebolt, fireArtilleryBolt, landingEffectRight, landingEffectLeft, endDialogControl, splatterEffect, burnEffect, bossPickUp;

    private Transform mouthFrontSpawn,
        mouthTopSpawn,
        mouthBottomSpawn,
        flamingBeamStart,
        flamingBeamEnd,
        flamingBeamStart2,
        flamingBeamEnd2,
        windSpawn,
        secondRoundPosition,
        flyingRightPosition,
        flyingLeftPosition,
        bottomDamagerCheck,
        landingEffectSpawnLeft,
        landingEffectSpawnRight,
        larryMeleeKillPos,
        permaFireSpawns1,
        permaFireSpawns2,
        permaFireSpawns3;
    private EnemyStats stats;
    private Animator animator;
    private Rigidbody2D rb;
    private bool flamingBeamActive, liftOff, flyToPos2, flyLeft, flyRight, flyAttackDamager, flyAttackRdy, round3AttackRdy;
    private AudioSource[] sounds;
    private AudioSource windBlastSound, liftOffSound, deathSound, flyLoop, flyAttackSound, landingHitSound, landingSound, fireBeamWindUpSound, fireBeamLoop, flameArtWindUpSound, flameBreathLoop;
    private BoxCollider2D firstRoundBlocker;
    private GameObject fireBeamChild;
    private int thirdRoundCounter, round;
    private PlatformerCharacter2D playerChar;
    private Camera2DFollow camerascript;
    private BossUI bossUI;
    private ParticleSystem[] mouthParticleEffects;

    void Start()
    {
        stats = GetComponent<EnemyStats>();
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        stats.unverwundbar = true;
        sounds = transform.Find("Sounds").GetComponents<AudioSource>();
        windBlastSound = sounds[0];
        liftOffSound = sounds[1];
        deathSound = sounds[2];
        flyLoop =sounds[3];
        flyAttackSound = sounds[4];
        landingHitSound = sounds[5];
        landingSound = sounds[6];
        fireBeamWindUpSound = sounds[7];
        flameArtWindUpSound = sounds[8];
        fireBeamLoop = sounds[9];
        flameBreathLoop = sounds[10];
        mouthFrontSpawn = transform.Find("Sensors").Find("MouthFrontSpawn");
        mouthTopSpawn = transform.Find("Sensors").Find("MouthTopSpawn");
        mouthBottomSpawn = transform.Find("Sensors").Find("MouthBottomSpawn");
        flamingBeamStart = transform.Find("Sensors").Find("FlamingBeamStart");
        flamingBeamEnd = transform.Find("Sensors").Find("FlamingBeamEnd");
        flamingBeamStart2 = transform.Find("Sensors").Find("FlamingBeamStart2");
        flamingBeamEnd2 = transform.Find("Sensors").Find("FlamingBeamEnd2");
        windSpawn = transform.Find("Sensors").Find("WindSpawn");
        secondRoundPosition = transform.Find("Sensors").Find("SecondRoundPosition");
        flyingRightPosition = transform.Find("Sensors").Find("FlyingRightPosition");
        flyingLeftPosition = transform.Find("Sensors").Find("FlyingLeftPosition");
        bottomDamagerCheck = transform.Find("Sensors").Find("BottomDamagerCheck");
        landingEffectSpawnLeft = transform.Find("Sensors").Find("LandingEffectSpawnLeft");
        landingEffectSpawnRight = transform.Find("Sensors").Find("LandingEffectSpawnRight");
        larryMeleeKillPos = transform.Find("Sensors").Find("LarryMeleeKillPos");
        permaFireSpawns1 = transform.Find("Sensors").Find("PermaFireSpawns1");
        permaFireSpawns2 = transform.Find("Sensors").Find("PermaFireSpawns2");
        permaFireSpawns3 = transform.Find("Sensors").Find("PermaFireSpawns3");
        flamingBeamStart.transform.parent = transform.parent;
        flamingBeamEnd.transform.parent = transform.parent;
        flamingBeamStart2.transform.parent = transform.parent;
        flamingBeamEnd2.transform.parent = transform.parent;
        secondRoundPosition.transform.parent = transform.parent;
        flyingRightPosition.transform.parent = transform.parent;
        flyingLeftPosition.transform.parent = transform.parent;
        permaFireSpawns1.transform.parent = transform.parent;
        permaFireSpawns2.transform.parent = transform.parent;
        permaFireSpawns3.transform.parent = transform.parent;
        firstRoundBlocker = GetComponentsInChildren<BoxCollider2D>()[1];
        bossUI = GameObject.Find("BossUI").GetComponent<BossUI>();
        playerChar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
        flyAttackSound.PlayDelayed(0);
        camerascript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
        mouthParticleEffects = transform.Find("DragonExport/bone_1/bone_30/bone_31").GetComponentsInChildren<ParticleSystem>();
    }

    private void FixedUpdate()
    {
        if (!stats.dead)
        {
            if (liftOff)
            {
                rb.AddForce(new Vector2(0, speed));
                if (transform.position.y > flyingLeftPosition.position.y - 2.5f)
                {
                    liftOff = false;
                    if (flyToPos2)
                    {
                        flyToPos2 = false;
                        StartCoroutine(flyBack2());
                    }
                    if (round == 2)
                    {
                        round = 3;
                        stats.health = stats.maxHealth;
                        bossUI.shiftRight();
                        StartCoroutine(spawnFireGround());
                    }
                }
            }

            if (flyAttackDamager)
            {
                rb.AddForce(new Vector2(0, -speed));
            }

            if (!liftOff && flyToPos2)
            {
                rb.AddForce(new Vector2(speed, 0));
            }

            if (flyLeft  && !flyAttackDamager)
            {
                rb.AddForce(new Vector2(-speed, 0));
            }

            if (flyRight  && !flyAttackDamager)
            {
                rb.AddForce(new Vector2(speed, 0));
            }

            if ((flyLeft || flyRight) && transform.position.y < flyingLeftPosition.position.y && !flyAttackDamager && !flyToPos2)
            {
                rb.AddForce(new Vector2(0, speed/2));
            }
            else if((flyLeft || flyRight) && transform.position.y > flyingLeftPosition.position.y && !flyAttackDamager && !flyToPos2)
            {
                rb.AddForce(new Vector2(0, -speed/2));
            }
        }
    }

    void Update()
    {
        if (!stats.dead)
        {
            checkDeath();
        }
        if (!stats.dead && !liftOff)
        {
            if (flyAttackRdy && Mathf.Abs(transform.position.x - playerChar.transform.position.x) < 0.5f)
            {
                flyAttackRdy = false;
                rb.velocity = new Vector2(0, 0);
                StartCoroutine(flyAttack());
            }
            if (flyAttackDamager)
            {
                Vector2 direction = Vector2.left;
                if (!stats.facingRight)
                {
                    direction = Vector2.right;
                }
                if (Physics2D.Raycast(bottomDamagerCheck.position, direction, bottomDamagerCheckLength, 1 << 8) && playerChar.GetComponent<Animator>().GetBool("Ground"))
                {
                    if(playerChar.Hit(stats.attacks.GetValueOrDefault("SmashAttack"), DamageTypeToPlayer.Smashed, bottomDamagerCheck.position, gameObject, Guid.NewGuid()))
                    {
                        landingHitSound.PlayDelayed(0);
                    }
                    flyAttackDamager = false;
                    showLandingEffect();
                }

                if (Physics2D.OverlapCircle(bottomDamagerCheck.position, 0.3f, 1 << 11))
                {
                    showLandingEffect();
                    flyAttackDamager = false;
                }
            }

            if (flyLeft && transform.position.x < flyingLeftPosition.position.x && round == 3)
            {
                flyLeft = false;
                Flip();
                rb.velocity = new Vector2(0, 0);
                flyRight = true;
                if (round3AttackRdy)
                {
                    rdyNextRound3Attack();
                }
            }
            if (flyRight && transform.position.x > flyingRightPosition.position.x  && round == 3)
            {
                flyRight = false;
                Flip();
                rb.velocity = new Vector2(0, 0);
                flyLeft = true;
                if (round3AttackRdy)
                {
                    rdyNextRound3Attack();
                }
            }
        }
    }

    public void startFight()
    {
        stats.unverwundbar = false;
        round = 1;
        playerChar.InitializeSkills(true);
        StartCoroutine(windBlast());
    }

    //round1 Attacks
    private IEnumerator windBlast()
    {
        animator.SetTrigger("WindBlast");
        yield return new WaitForSeconds(0.75f);
        windBlastSound.PlayDelayed(0);
        yield return new WaitForSeconds(0.75f);
        Instantiate(windEffect, windSpawn.position, quaternion.identity);
        yield return new WaitForSeconds(2.5f);
        if (round == 2)
        {
            if (stats.health <= 0)
            {
                StartCoroutine(startFlying());
            }
            else
            {
                StartCoroutine(fireballs());
            }
        }
        else
        {
            if (stats.health <= 0)
            {
                StartCoroutine(flyBack1());
            }
            else
            {
                StartCoroutine(fireArtillery());
            }
        }
    }
    
    private IEnumerator fireArtillery()
    {
        animator.SetTrigger("FireArtillery");
        yield return new WaitForSeconds(0.5f);
        Vector2 launchDirection;
        int artilleryCount;
        if (round == 2)
        {
            launchDirection = new Vector2(-16,10);
            artilleryCount = 10;
        }
        else
        {
            launchDirection = new Vector2(-9.5f,10);
            artilleryCount = 5;
        }

        for (int i = 0; i < artilleryCount; i++)
        {
            flameArtWindUpSound.PlayDelayed(0);
            FireArtillery fireArtillery = Instantiate(fireArtilleryBolt, mouthTopSpawn.position, quaternion.identity).GetComponent<FireArtillery>();
            fireArtillery.attackDamage = stats.attacks.GetValueOrDefault("FireArtilleryImpact");
            launchDirection = new Vector2(launchDirection.x + 1.5f,launchDirection.y);
            fireArtillery.launch(launchDirection.x, launchDirection.y);
            yield return new WaitForSeconds(0.1666666f);
        }
        yield return new WaitForSeconds(0.5f);
        if (round == 2)
        {
            if (stats.health <= 0)
            {
                StartCoroutine(startFlying());
            }
            else
            {if (Physics2D.OverlapCircle(larryMeleeKillPos.position, 1f, 1<<8))
                {
                    StartCoroutine(meleeAttack());
                }
                else
                {
                    StartCoroutine(windBlast());
                }
            }
        }
        else
        {
            if (stats.health <= 0)
            {
                StartCoroutine(flyBack1());
            }
            else
            {
                if (Physics2D.OverlapCircle(larryMeleeKillPos.position, 1f, 1<<8))
                {
                    StartCoroutine(meleeAttack());
                }
                else
                {
                    StartCoroutine(FireBeam());
                }
            }
        }
    }
    
    private IEnumerator FireBeam()
    {
        fireBeamWindUpSound.PlayDelayed(0);
        animator.SetTrigger("FireBeam");
        yield return new WaitForSeconds(0.5f);
        fireBeamLoop.PlayDelayed(0);
        Vector3 launchDirection;
        for(int i = 0; i < 20; i++)
        {
            float f = i/19f;
            launchDirection = Vector3.Lerp(flamingBeamStart.transform.position,flamingBeamEnd.transform.position, f) - mouthFrontSpawn.position;
            launchDirection = launchDirection.normalized * 20;
            DragonFirebolt fireboltChild = Instantiate(firebolt, mouthFrontSpawn.position, quaternion.identity).GetComponent<DragonFirebolt>();
            fireboltChild.attackDamage = stats.attacks.GetValueOrDefault("FireOdem");
            fireboltChild.abschuss2(launchDirection.x,launchDirection.y);
            yield return new WaitForSeconds(0.2f);
        }
        fireBeamLoop.Stop();
        if (stats.health <= 0)
        {
            StartCoroutine(flyBack1());
        }
        else
        {
            if (Physics2D.OverlapCircle(larryMeleeKillPos.position, 1f, 1<<8))
            {
                StartCoroutine(meleeAttack());
            }
            else
            {
                StartCoroutine(windBlast());
            }
        }
    }

    private IEnumerator flyBack1()
    {
        stats.health = stats.maxHealth;
        animator.SetTrigger("LiftOff");
        flyLoop.PlayDelayed(0);
        liftOffSound.Play(0);
        firstRoundBlocker.enabled = false;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        yield return new WaitForSeconds(1);
        liftOff = true;
        flyToPos2 = true;
        yield return new WaitForSeconds(0.25f);
        rb.gravityScale = 0;
    }

    
    private IEnumerator flyBack2()
    {
        rb.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
        rb.velocity = Vector2.zero;
        StartCoroutine(spawnFlames1());
        animator.SetTrigger("FlyingBurnGround");
        Vector3 launchDirection;
        flameBreathLoop.PlayDelayed(0);
        for(int i = 0; i < 20; i++)
        {
            float f = i/19f;
            launchDirection = Vector3.Lerp(flamingBeamStart.transform.position,flamingBeamEnd.transform.position, f) - mouthFrontSpawn.position;
            launchDirection = launchDirection.normalized * 20;
            DragonFirebolt fireboltChild = Instantiate(firebolt, mouthFrontSpawn.position, quaternion.identity).GetComponent<DragonFirebolt>();
            fireboltChild.attackDamage = stats.attacks.GetValueOrDefault("FireOdem");
            fireboltChild.flameBreathMode = true;
            fireboltChild.abschuss2(launchDirection.x,launchDirection.y);
            yield return new WaitForSeconds(0.2f);
        }
        flameBreathLoop.Stop();
        yield return new WaitForSeconds(1);
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        flyToPos2 = true;
        Flip();
        rb.velocity = new Vector2(0, 0);
        flyLoop.PlayDelayed(0);
        animator.SetTrigger("Fly");
        yield return new WaitForSeconds(3);
        flyToPos2 = false;
        animator.SetTrigger("Landing");
        rb.gravityScale = 1;
        flyLoop.Stop();
        yield return new WaitForSeconds(1);
        transform.position = secondRoundPosition.position;
        showLandingEffect();
        rb.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
        Flip();
        round = 2;
        StartCoroutine(fireballs());
    }

    private IEnumerator spawnFlames1()
    {
        yield return new WaitForSeconds(1);
        Transform[] spawns = permaFireSpawns1.GetComponentsInChildren<Transform>();
        foreach (Transform flameSpawn in spawns)
        {
            if (flameSpawn != permaFireSpawns1)
            {
                BurnEffect burnChild = Instantiate(burnEffect, flameSpawn.position, quaternion.identity).GetComponent<BurnEffect>();
                burnChild.noDespawn = true;
                yield return new WaitForSeconds(0.5f);
            }
        }
    }
    
    //round2 Attacks
    private IEnumerator fireballs()
    {
        animator.SetTrigger("Fireballs");
        yield return new WaitForSeconds(0.666f);
        for (int i = 0; i < 6; i++)
        {
            DragonFirebolt fireboltChild = Instantiate(firebolt, mouthFrontSpawn.position, quaternion.identity).GetComponent<DragonFirebolt>();
            fireboltChild.attackDamage = stats.attacks.GetValueOrDefault("FireBolt");
            fireboltChild.abschuss1();
            yield return new WaitForSeconds(0.333f);
        }
        if (stats.health <= 0)
        {
            StartCoroutine(startFlying());
        }
        else
        {
            if (Physics2D.OverlapCircle(larryMeleeKillPos.position, 1f, 1<<8))
            {
                StartCoroutine(meleeAttack());
            }
            else
            {
                StartCoroutine(fireArtillery());
            }
        }
    }
    
    private IEnumerator startFlying()
    {
        spawnBossPickup();
        liftOff = true;
        liftOffSound.PlayDelayed(0);
        animator.SetTrigger("LiftOff");
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        rb.gravityScale = 0;
        if (round == 3)
        {
            if(!flyLeft && !flyRight)
            {
                flyLeft = true;
            }
            yield return new WaitForSeconds(3);
            round3AttackRdy = true;
        }
    }
    
    private IEnumerator spawnFireGround()
    {
        rb.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
        rb.velocity = Vector2.zero;
        StartCoroutine(spawnFlames2());
        flameBreathLoop.PlayDelayed(0);
        animator.SetTrigger("FlyingBurnGround2");
        Vector3 launchDirection;
        for(int i = 0; i < 40; i++)
        {
            float f = i/39f;
            launchDirection = Vector3.Lerp(flamingBeamStart2.transform.position,flamingBeamEnd2.transform.position, f) - mouthFrontSpawn.position;
            launchDirection = launchDirection.normalized * 20;
            DragonFirebolt fireboltChild = Instantiate(firebolt, mouthFrontSpawn.position, quaternion.identity).GetComponent<DragonFirebolt>();
            fireboltChild.attackDamage = stats.attacks.GetValueOrDefault("FireOdem");
            fireboltChild.flameBreathMode = true;
            fireboltChild.abschuss2(launchDirection.x,launchDirection.y);
            yield return new WaitForSeconds(0.2f);
        }
        flameBreathLoop.Stop();
        yield return new WaitForSeconds(1);
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        flyLeft = true;
        round3AttackRdy = true;
    }

    private IEnumerator spawnFlames2()
    {
        yield return new WaitForSeconds(1);
        Transform[] spawns = permaFireSpawns2.GetComponentsInChildren<Transform>();
        foreach (Transform flameSpawn in spawns)
        {
            if (flameSpawn != permaFireSpawns2)
            {
                BurnEffect burnChild = Instantiate(burnEffect, flameSpawn.position, quaternion.identity).GetComponent<BurnEffect>();
                burnChild.noDespawn = true;
                yield return new WaitForSeconds(0.45f);
            }
        }
        Transform[] spawns2 = permaFireSpawns3.GetComponentsInChildren<Transform>();
        foreach (Transform flameSpawn in spawns2)
        {
            if (flameSpawn != permaFireSpawns3)
            {
                BurnEffect burnChild = Instantiate(burnEffect, flameSpawn.position, quaternion.identity).GetComponent<BurnEffect>();
                burnChild.noDespawn = true;
            }
        }
    }
    
    //round3 Attacks
    private IEnumerator flyAttack()
    {
        flyLoop.Stop();
        flyAttackSound.PlayDelayed(0);
        rb.gravityScale = 1;
        animator.SetTrigger("FlyAttack");
        flyAttackDamager = true;
        yield return new WaitForSeconds(3);
        if (!stats.dead)
        {
            flyAttackDamager = false;
            StartCoroutine(startFlying());
        }
    }

    private void rdyNextRound3Attack()
    {
        if (thirdRoundCounter % 3 == 0)
        {
            flyLoop.Stop();
            StartCoroutine(flyfireArtillery());
        }
        else if (thirdRoundCounter % 3 == 1)
        {
            flyLoop.Stop();
            StartCoroutine(flyfireBalls());
        }
        else
        {
            flyLoop.PlayDelayed(0);
            animator.SetTrigger("Fly");
            flyAttackRdy = true;
        }
        round3AttackRdy = false;
        thirdRoundCounter++;
    }
    
    private IEnumerator flyfireArtillery()
    {
        flameArtWindUpSound.PlayDelayed(0);
        float randomWait = UnityEngine.Random.Range(0f, 0.5f);
        yield return new WaitForSeconds(randomWait);
        animator.SetTrigger("FlyFireArtillery");
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < 5; i++)
        {
            if (!stats.dead)
            {
                fireBeamLoop.PlayDelayed(0);
                float randomPush = UnityEngine.Random.Range(-5f, 5f);
                FireArtillery fireArtillery = Instantiate(fireArtilleryBolt, mouthBottomSpawn.position, quaternion.identity).GetComponent<FireArtillery>();
                fireArtillery.attackDamage = stats.attacks.GetValueOrDefault("FireArtilleryImpact");
                fireArtillery.launch(randomPush, -10);
                yield return new WaitForSeconds(0.1666666f);
            }
        }
        fireBeamLoop.Stop();
        yield return new WaitForSeconds(1.1666666f);
        if (!stats.dead)
        {
            round3AttackRdy = true;
        }
    }
    
    private IEnumerator flyfireBalls()
    {
        animator.SetTrigger("Fireballs");
        yield return new WaitForSeconds(0.666f);
        for (int i = 0; i < 6; i++)
        {
            if (!stats.dead)
            {
                DragonFirebolt fireboltChild = Instantiate(firebolt, mouthBottomSpawn.position, quaternion.identity).GetComponent<DragonFirebolt>();
                fireboltChild.attackDamage = stats.attacks.GetValueOrDefault("FireBolt");
                fireboltChild.abschuss1();
                yield return new WaitForSeconds(0.333f);
            }
        }
        yield return new WaitForSeconds(0.333f);

        if (!stats.dead)
        {
            round3AttackRdy = true;
        }
    }

    private IEnumerator meleeAttack()
    {
        animator.SetTrigger("MeleeAttack");
        yield return new WaitForSeconds(0.5f);
        if (Physics2D.OverlapCircle(larryMeleeKillPos.position, 1f, 1<<8))
        {
            StartCoroutine(meleeInstaKill());
        }
        else
        {
            StartCoroutine(windBlast());
        }
    }


    private IEnumerator meleeInstaKill()
    {
        stats.unverwundbar = true;
        if ((!playerChar.m_FacingRight && !stats.facingRight) || (playerChar.m_FacingRight && stats.facingRight))
        {
            playerChar.Flip();
        }
        if (playerChar.crouchIn)
        {
            playerChar.StandUp();
        }
        rb.velocity = Vector2.zero;
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
        playerChar.KillPlayer(DamageTypeToPlayer.DragonBurn);
        playerChar.GetComponent<BoxCollider2D>().enabled = false;
        playerChar.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        playerChar.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        playerChar.transform.position = larryMeleeKillPos.position;
        animator.SetTrigger("MeleeAttackKill");
        yield return new WaitForSeconds(0.75f);
        fireBeamLoop.PlayDelayed(0);
        for(int i = 0; i < 8; i++)
        {
            Vector3 launchDirection = new Vector3(larryMeleeKillPos.position.x, larryMeleeKillPos.position.y + 0.75f, larryMeleeKillPos.position.z) - mouthFrontSpawn.position;
            launchDirection = launchDirection.normalized * 20;
            DragonFirebolt fireboltChild = Instantiate(firebolt, mouthFrontSpawn.position, quaternion.identity).GetComponent<DragonFirebolt>();
            fireboltChild.flameBreathMode = true;
            fireboltChild.abschuss2(launchDirection.x,launchDirection.y);
            yield return new WaitForSeconds(0.25f);
        }
        fireBeamLoop.Stop();
        yield return new WaitForSeconds(1.0f);
        Instantiate(splatterEffect, larryMeleeKillPos.position, Quaternion.identity);
        yield return new WaitForSeconds(0.75f);
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        playerChar.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        stats.unverwundbar = false;
        yield return new WaitForSeconds(0.25f);
        StartCoroutine(windBlast());
    }

    public void showLandingEffect()
    {
        flyLoop.Stop();
        landingSound.PlayDelayed(0);
        camerascript.ShakeCamera(0.1f,1f, 60);
        Instantiate(landingEffectRight, landingEffectSpawnRight.position, quaternion.identity);
        Instantiate(landingEffectLeft, landingEffectSpawnLeft.position, quaternion.identity);
    }

    private void spawnBossPickup()
    {
        GameObject bossPickUpInstance = Instantiate(bossPickUp, mouthFrontSpawn.position, Quaternion.identity);
        Vector2 bossPickUpPush = new Vector2(500, 50);
        float rotation = 50;
        if (!stats.facingRight)
        {
            bossPickUpPush.x *= -1;
            rotation *= -1;
        }
        bossPickUpInstance.GetComponent<BossPickup>().spawn(bossPickUpPush, rotation);
    }
    
    public void Flip()//z.B. in andere Richtung gucken
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        stats.facingRight = !stats.facingRight;
    }
    
    private void checkDeath()
    {
        if (stats.health <= 0 && !stats.dead && round == 3 && !playerChar.dead)
        {
            round = 4;
            StartCoroutine(Death());
        }
    }
    
    private IEnumerator Death()
    {
        StartCoroutine(endDialogControl.GetComponent<ServantEnd2DialogControl>().playEndingDialog());
        foreach (ParticleSystem mouthParticleEffect in mouthParticleEffects)
        {
            mouthParticleEffect.Stop();
            if (mouthParticleEffect.transform.childCount > 0)
            {
                mouthParticleEffect.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        flyLoop.Stop();
        fireBeamLoop.Stop();
        stats.dead = true;
        rb.gravityScale = 1;
        yield return new WaitForSeconds(1f);
        deathSound.PlayDelayed(0);
        animator.SetTrigger("DeathTrigger");
        animator.SetTrigger("HeadDead");
        animator.SetBool("Death", true);
    }
}
