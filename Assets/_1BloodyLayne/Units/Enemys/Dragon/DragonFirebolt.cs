using System;
using System.Collections;
using System.Collections.Generic;
using UI.PlayerUI;
using Units.Player;
using UnityEngine;
using UnityEngine.Assertions.Must;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    public class DragonFirebolt : MonoBehaviour
    {
        GameObject Player;
        private PlatformerCharacter2D playerChar;
        public GameObject deathSound;
        Rigidbody2D rbLarry;
        Rigidbody2D rb;
        Vector2 richtung;
        public float speed = 15f;
        public bool flameBreathMode;
        public int attackDamage = 35;
        private bool died, effectOnCooldown;
        private Camera2DFollow camerascript;

        // Use this for initialization
        void Start()
        {
            //rb.velocity = Vector2.Scale(richtung.normalized, new Vector2(totalDuration,totalDuration).normalized);
            camerascript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
            //transform.localScale = Vector3.zero;
            Destroy(gameObject, 14f);
        }
        // Update is called once per frame
        void Update()
        {
            if (!effectOnCooldown)
            {
                if (Physics2D.OverlapCircle(transform.position, 0.3f, 1 << 11))
                {
                    StartCoroutine(hitEffectAndCooldown());
                    if (!died)
                    {
                        StartCoroutine(Death());
                    }
                }
            }
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!effectOnCooldown)
            {
                if (other.gameObject.CompareTag("Ground"))
                {
                    StartCoroutine(hitEffectAndCooldown());
                    if (!died)
                    {
                        StartCoroutine(Death());
                    }
                }
                else if (other.gameObject.CompareTag("Player") && !died)
                {
                    if (playerChar.Hit(attackDamage, DamageTypeToPlayer.Burned, transform.position, gameObject, Guid.NewGuid()))
                    {                   
                        StartCoroutine(hitEffectAndCooldown());
                    }
                    if (!died)
                    {
                        StartCoroutine(Death());
                    }
                }
            }
        }
        public void abschuss1()
        {
            Player = GameObject.FindWithTag("Player");
            playerChar = Player.GetComponent<PlatformerCharacter2D>();
            rbLarry = Player.GetComponent<Rigidbody2D>();
            rb = GetComponent<Rigidbody2D>();
            richtung = rbLarry.position - rb.position;
            rb.velocity = richtung.normalized * speed;
            setRotation(richtung.x, richtung.y);
        }
        public void abschuss2(float x, float y)
        {
            Player = GameObject.FindWithTag("Player");
            playerChar = Player.GetComponent<PlatformerCharacter2D>();
            rb = GetComponent<Rigidbody2D>();
            richtung = new Vector2(x, y);
            rb.velocity = new Vector2(richtung.x, richtung.y);
            setRotation(x, y);
        }

        private void setRotation(float x, float y)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.back, new Vector3(x, y, 0));
            transform.Rotate(new Vector3(0,0,180));
        }

        private IEnumerator hitEffectAndCooldown()
        {
            if (Vector2.Distance(transform.position, playerChar.transform.position) < 10)
            {
                if (flameBreathMode)
                {
                    camerascript.ShakeCamera(0.25f, 0.25f, 10);
                }
                else
                {
                    camerascript.ShakeCamera(1f, 1.5f, 30);
                }
            }

            GameObject deathKind = Instantiate(deathSound, transform.position, Quaternion.identity);
            deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
            effectOnCooldown = true;
            if (flameBreathMode)
            {
                StartCoroutine(Death());
            }
            else
            {
                yield return new WaitForSeconds(0.05f);
                effectOnCooldown = false;
            }
        }

        private IEnumerator Death()
        {
            died = true;
            GetComponent<BoxCollider2D>().enabled = false;
            yield return new WaitForSeconds(2f);
            ParticleSystem[] pSystems = GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem pSystem in pSystems)
            {
                pSystem.Stop();
            }
            GetComponentInChildren<UnityEngine.Rendering.Universal.Light2D>().enabled = false;
            yield return new WaitForSeconds(2f);
            Destroy(gameObject);
        }

    }
}
