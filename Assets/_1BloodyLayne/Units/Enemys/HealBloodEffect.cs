﻿using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class HealBloodEffect : MonoBehaviour
    {
        public float stage1Duration = 1;
        public float stage2Duration = 0.6f;
        public float stage3Duration = 0.3f;
        
        private Transform player;
        private bool move;
        private float stage2StartTime;
        private Vector3 startPositon;
        private AudioSource[] splatterSound;
        
        // Start is called before the first frame update
        void Start()
        { 
            startPositon = transform.position;
            player = GameObject.FindGameObjectWithTag("Player").transform;
            StartCoroutine(waitToMove());
            splatterSound = GetComponents<AudioSource>();
            int rnd = Random.Range(0,4);
            splatterSound[rnd].PlayDelayed(0);
            player.GetComponent<PlatformerCharacter2D>().incomingBloodthirstHeal = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (move)
            {
                float progressionPercentage = (Time.time - stage2StartTime) / stage2Duration;
                transform.position = Vector3.Lerp(startPositon, player.position, progressionPercentage);
            }
        }

        IEnumerator waitToMove()
        {
            yield return new WaitForSeconds(stage1Duration);
            stage2StartTime = Time.time;
            move = true;
            yield return new WaitForSeconds(stage2Duration);
            player.GetComponent<PlatformerCharacter2D>().BiteHeal();
            player.GetComponent<PlatformerCharacter2D>().incomingBloodthirstHeal = false;
            yield return new WaitForSeconds(stage3Duration);
            Destroy(gameObject);
        }
    }
}
