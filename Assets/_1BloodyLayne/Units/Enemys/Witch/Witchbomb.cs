using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Witchbomb : MonoBehaviour
    {
        Rigidbody2D rb;
        GameObject Player;
        private PlatformerCharacter2D Larry;
        public int attackDamage = 70;
        public GameObject explosion;
        private bool growing;
        private AudioSource growSound;

        // Use this for initialization
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            rb.AddForce(new Vector2(0,-50));
            Player = GameObject.FindWithTag("Player");
            Larry = Player.GetComponent<PlatformerCharacter2D>();
            growSound = GetComponent<AudioSource>();
        }

        private void Update()
        {
            if (growing)
            {
                transform.localScale = new Vector3(transform.localScale.x * (1 + Time.deltaTime * 2),
                    transform.localScale.y * (1 + Time.deltaTime * 2), transform.localScale.y * (1 + Time.deltaTime * 2));
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            growing = true;
            rb.bodyType = RigidbodyType2D.Static;
            StartCoroutine(detonate());
        }

        IEnumerator detonate()
        {
            yield return new WaitForSeconds(0.5f);
            Instantiate(explosion, transform.position, Quaternion.identity);
            if (Physics2D.OverlapCircle(transform.position, 5.5f, 1 << 8))//Spieler Hit
            {
                Vector3 richtung = Player.transform.position - transform.position;
                RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
                foreach (RaycastHit2D hitter in hit)
                {
                    if (hitter.transform.tag == "Player")
                    {
                        Larry.Hit(attackDamage,DamageTypeToPlayer.Explosion, transform.position, gameObject, new Guid());
                        break;
                    }
                }
            }
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 5.5f, 1<<14);//layer 14 = Objects
            {
                foreach (Collider2D col in cols)
                {
                    if (col.gameObject.GetComponent<ObjektStats>() != null)
                    {
                        Vector3 richtung = col.gameObject.transform.position - transform.position;
                        RaycastHit2D hit = Physics2D.Raycast(transform.position, richtung);
                        if (hit.transform.CompareTag("Ground"))
                        {
                            break;
                        }
                        if (hit.transform.CompareTag("Object"))
                        {
                            if (col.gameObject.GetComponent<ObjektStats>().destructable)
                            {
                                col.gameObject.GetComponent<ObjektStats>().addDamage(attackDamage);
                            }
                        }
                    }
                }
            }
            Destroy(gameObject);
        }
    }
}