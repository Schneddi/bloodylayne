using System;
using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Witch : MonoBehaviour
    {
        public GameObject witchAttack, witchBomb;
        public float speed;
        public string status = "Idle";
        
        private EnemyStats stats;
        private Transform player, attackSpawnPoint, attackDownSpawnPoint, flyingTargetHeigth;
        private Vector3 originPoint;
        private Animator animator;
        private AudioSource witchSpellLaunchSound, witchBombDropSound, laughSound;
        private Rigidbody2D rb;
        private bool goUp;

        // Use this for initialization
        void Start()
        {
            originPoint = transform.position;
            player = GameObject.FindWithTag("Player").transform;
            attackSpawnPoint = transform.Find("AttackSpawnPoint");
            attackDownSpawnPoint = transform.Find("AttackDownSpawnPoint");
            flyingTargetHeigth = transform.Find("FlyingTargetHeigth");
            flyingTargetHeigth.parent = transform.parent;
            stats = GetComponent<EnemyStats>();
            rb = GetComponent<Rigidbody2D>();
            witchSpellLaunchSound = GetComponents<AudioSource>()[0];
            laughSound = GetComponents<AudioSource>()[1];
            witchBombDropSound = GetComponents<AudioSource>()[2];
            animator = GetComponent<Animator>();
            if (!stats.facingRight)
            {
                Flip();
            }
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (!animator.GetBool("Death"))
            {
                checkDeath();

                if (status.Equals("Idle"))
                {
                    rb.AddForce(new Vector2(speed, 0));
                    if (transform.position.y > flyingTargetHeigth.position.y + 1.5f)
                    {
                        goUp = false;
                        rb.AddForce(new Vector2(0, -Mathf.Abs(speed)));
                    }
                    else if (transform.position.y < flyingTargetHeigth.position.y - 0.5f)
                    {
                        goUp = true;
                    }

                    if (Mathf.Abs(transform.position.x -originPoint.x) > 4 && ((transform.position.x < originPoint.x && stats.facingRight)||(transform.position.x > originPoint.x && !stats.facingRight)))
                    {
                        Flip();
                    }
                }

                if (status.Equals("Casting"))
                {
                    rb.AddForce(new Vector2(0, Mathf.Abs(speed)));
                }

                if (status.Equals("FlyAttacking") )
                {
                    rb.AddForce(new Vector2(speed, 0));
                    if (transform.position.y > flyingTargetHeigth.position.y + 1.5f)
                    {
                        goUp = false;
                        rb.AddForce(new Vector2(0, -Mathf.Abs(speed)));
                    }
                    else if (transform.position.y < flyingTargetHeigth.position.y - 0.5f)
                    {
                        goUp = true;
                    }

                    if (Vector3.Distance(transform.position, player.position) < 18  && ((transform.position.x < player.position.x && stats.facingRight)||(transform.position.x > player.position.x && !stats.facingRight)))
                    {
                        StartCoroutine(laugh());
                    }
                    else if(player.position.y > transform.position.y)
                    {
                        StartCoroutine(SpellAttack());
                    }
                    if (Vector3.Distance(transform.position, player.position) > 40)
                    {
                        FlipCheck();
                        status = ("Idle");
                    }
                }

                if (goUp)
                {
                    rb.AddForce(new Vector2(0, Mathf.Abs(speed)*2));
                }
            }
        }

        void Update()
        {
            if (!animator.GetBool("Death"))
            {
                if (status.Equals("Idle"))
                {
                    if (Physics2D.OverlapCircle(transform.position, 13f, 1 << 8))
                    {
                        if (checkIfLarryInSight())
                        {
                            StartCoroutine(laugh());
                        }
                    }
                }
            }
        }

        private void FlipCheck()
        {
            if (stats.facingRight && transform.position.x < player.position.x)
            {
                Flip();
            }
            else if (!stats.facingRight && transform.position.x > player.position.x)
            {
                Flip();
            }
        }

        private void Flip()
        {
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
            stats.facingRight = !stats.facingRight;
            speed *= -1;
        }


        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Ground") && !status.Equals("Casting"))
            {
                Flip();
            }
        }

        IEnumerator laugh()
        {
            goUp = false;
            FlipCheck();
            laughSound.PlayDelayed(0);
            status = ("Laughing");
            animator.SetTrigger("Laugh");
            yield return new WaitForSeconds(1f);
            if (!animator.GetBool("Death"))
            {
                StartCoroutine(SpellAttack());
            }
        }

        IEnumerator SpellAttack()
        {
            for (int i = 0; i< 3; i++)
            {
                if (!animator.GetBool("Death"))
                {
                    FlipCheck();
                    witchSpellLaunchSound.PlayDelayed(0);
                    status = ("Casting");
                    animator.SetTrigger("Attack");
                    yield return new WaitForSeconds(0.25f);
                    GameObject witchAttackChild = Instantiate(witchAttack, attackSpawnPoint.transform.position, Quaternion.identity);
                    witchAttackChild.GetComponent<Firebolt>().attackDamage = stats.attacks.GetValueOrDefault("WitchBolt");
                    witchAttackChild.GetComponent<Firebolt>().abschuss1();
                    yield return new WaitForSeconds(0.25f);
                }
            }
            if (!animator.GetBool("Death"))
            {
                if(player.position.y > transform.position.y)
                {
                    StartCoroutine(SpellAttack());
                }
                else
                {
                    StartCoroutine(FlyAttack());
                }
            }
        }

        IEnumerator FlyAttack()
        {
            status = ("FlyAttacking");
            animator.SetTrigger("Idle");
            while (!animator.GetBool("Death") && status.Equals("FlyAttacking"))
            {
                GameObject witchBombInstance = Instantiate(witchBomb, attackDownSpawnPoint.transform.position, Quaternion.identity);
                witchBombInstance.GetComponent<Witchbomb>().attackDamage = stats.attacks.GetValueOrDefault("WitchBomb");
                witchBombDropSound.PlayDelayed(0);
                yield return new WaitForSeconds(1f);
            }
        }

        private bool checkIfLarryInSight()
        {
            if (player.GetComponent<PlayerStats>().health == 0) return false;
            
            Vector3 richtung = player.position - transform.position;
            RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
            foreach (RaycastHit2D hitter in hit)
            {
                if (hitter.transform.tag == "Ground")
                {
                    break;
                }
                if (hitter.transform.tag == "Player")
                {
                    return true;
                }
            }
            return false;
        }
        
        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
                rb.freezeRotation = false;
                rb.constraints = rb.constraints & ~RigidbodyConstraints2D.FreezePositionX & ~RigidbodyConstraints2D.FreezePositionY;
            }
        }
        private void Death()
        {
            stats.dead = true;
            status = ("Dying");
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            GetComponent<HumanDeathSequence>().Death();
        }
    }
}