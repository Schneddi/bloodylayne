﻿using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace Units.Enemys
{
    public class EnemyStats : MonoBehaviour
    {
        public int maxHealth;
        public int health;
        public Dictionary<string, int> attacks;
        public bool isAnimal;
        public bool isHuman;
        public bool unbiteable;
        public bool unverwundbar;
        public bool onLadder;
        public bool dead;
        public bool ranged;
        public DamageTypeToEnemy lastDamageType;
        public int recievedAttacks;
        public bool facingRight;
        public bool noBloodied;
        public bool isBoss;
        
        [SerializeField] private string[] attackNames;
        [SerializeField] private int[] attackDamages;
        
        private PlatformerCharacter2D playerChar;
        private GlobalVariables gv;
        private GameController gc;
        private Rigidbody2D rb;
        private bool bloodied;
        private bool difficultyIsSetup;
        
        void Awake()
        {
            playerChar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            gc = GameObject.Find("GameController").GetComponent<GameController>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            rb = GetComponent<Rigidbody2D>();
            if (!difficultyIsSetup && gv.difficultyIsSetup)
            {
                SetupDifficulty(gv.difficulty);
            }
        }

        public void SetupDifficulty(int difficulty)
        {
            difficultyIsSetup = true;
            if (attackNames.Length != attackDamages.Length)
            {
                Debug.Log("Warning: Number of attackNames is not Equal to attackDamages in " + gameObject.name);
            }
            else
            {
                attacks = new Dictionary<string, int>();
                int i = 0;
                foreach (string attackName in attackNames)
                {
                    int newAttackDamage = AdjustAttackDamageDifficulty(attackDamages[i], difficulty);
                    attacks.Add(attackName, newAttackDamage);
                    i++;
                }
            }
            
            switch (difficulty)
            {
                case 0:
                    maxHealth = (int)(maxHealth * 0.5f);
                    health = (int)(health * 0.5f);
                    break;
                case 1:
                    maxHealth = (int)(maxHealth * 0.75f);
                    health = (int)(health * 0.75f);
                    break;
                case 2:
                    maxHealth = (int)(maxHealth * 1f);
                    health = (int)(health * 1f);
                    break;
                case 3:
                    maxHealth = (int)(maxHealth * 1.5f);
                    health = (int)(health * 1.5f);
                    break;
                case 4:
                    maxHealth = (int)(maxHealth * 2f);
                    health = (int)(health * 2f);
                    break;
            }

            if (maxHealth <= 0)
            {
                maxHealth = 1;
            }
            if (health <= 0)
            {
                health = 1;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (health < 0)
            {
                health = 0;
            }
        }

        private int AdjustAttackDamageDifficulty(int oldAttackDamage, int difficulty)
        {
            switch (difficulty)
            {
                case 0:
                    return (int)(oldAttackDamage * 0.5f);
                case 1:
                    return (int)(oldAttackDamage * 0.75f);
                case 2:
                    return (int)(oldAttackDamage * 1f);
                case 3:
                    return (int)(oldAttackDamage * 1.5f);
                case 4:
                    return (int)(oldAttackDamage * 2f);
                default:
                    return (int)(oldAttackDamage * 1f);
            }
        }
        
        public void Hit(int damage, DamageTypeToEnemy damageType, System.Guid guid, bool skill11Stack)
        {
            recievedAttacks++;
            if (!unverwundbar)
            {
                lastDamageType = damageType;
                if (!dead && gv.displayChangeNumbers)
                {
                    GameObject healthChangeNumber = Instantiate(gv.healthChangeNumber, transform.position, Quaternion.identity);
                    healthChangeNumber.GetComponent<StatChangeNumberControl>().setupInfoNumber(damage, DamageNumberType.DamageDealt);
                }
                if (!dead && damageType != DamageTypeToEnemy.Bite)
                {
                    playerChar.ChangeBloodthirst(playerChar.GetComponent<PlayerStats>().bloodLossByDealingDamage, guid);
                }
                if (health > 0 && health - damage <= 0 && !dead  && (noBloodied || bloodied))
                {
                    health = health - damage;
                    if (isHuman)
                    {
                        if (damageType == DamageTypeToEnemy.Bloodmagic || damageType == DamageTypeToEnemy.Bite || gv.humansKilled > 0)
                        {
                            gv.humansKilled++;
                            gv.humanKilledLocal++;
                        }
                        if (!gv.fartedToDeath && damageType == DamageTypeToEnemy.Fart)
                        {
                            gv.fartedToDeath = true;
                        }
                    }
                    if (playerChar.skill5choice1 && (damageType == DamageTypeToEnemy.Bloodmagic || damageType == DamageTypeToEnemy.Explosion || damageType == DamageTypeToEnemy.Skill13Dash || damageType == DamageTypeToEnemy.Skill13Push) && playerChar.bloodlust <= 0)
                    {
                        Instantiate(playerChar.skill5u1HealEffect, transform.position, Quaternion.identity);
                    }
                }
                else if (health > 0 && health - damage <= 0 && !dead  && !noBloodied && !bloodied && isHuman && damageType != DamageTypeToEnemy.Explosion && damageType != DamageTypeToEnemy.Smashed)
                {
                    health = 1;
                    bloodied = true;
                    GetComponent<HumanDeathSequence>().showBloodiedState();
                }
                else
                {
                    health = health - damage;
                }
                if (playerChar.skill11choice1 && playerChar.skill11u1Stacks < 5)
                {
                    if (skill11Stack)
                    {
                        playerChar.AddSkill11u1Stack(1, guid);
                    }
                    GameObject hateChild = Instantiate(playerChar.killingSpreeEffectSkill11u1, playerChar.transform.position, Quaternion.identity);
                    hateChild.GetComponent<AudioSource>().pitch = hateChild.GetComponent<AudioSource>().pitch + playerChar.skill11u1Stacks * 0.2f;
                    hateChild.transform.parent = playerChar.gameObject.transform;
                }
                gc.SetStat("DAMAGE_DEALT", damage);
                if(health <= 0)
                {
                    health = 0;
                }
            }
        }

        public void PushHit(int damage, DamageTypeToEnemy damageType, Vector2 direction, System.Guid guid, bool skill11Stack)
        {
            if (!unverwundbar)
            {
                rb.AddForce(direction);
            }
            Hit(damage, damageType, guid, skill11Stack);
        }
    }
}
