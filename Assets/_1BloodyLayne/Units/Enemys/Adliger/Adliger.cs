﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using UnityEngine;
using UnityStandardAssets._2D;

namespace Units.Enemys.Adliger
{
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Adliger : MonoBehaviour
    {
        EnemyStats stats;
        public bool speaker, startPistoled, alarmed, glassIdle, passiv;
        [SerializeField] private float detectionRadius = 15, idleDrag = 30;
        bool dead, flipActive = true, rotateAim, larryInSight;
        AudioSource cheersSound1, cheersSound2, cheersSound3, cheersSound4, tossSound, coverSound, reloadSound1, reloadSound2Spit, reloadSound2, reloadSound3, reloadSound4, reloadSound5, reloadSound6, walkSound, shotSound;
        Animator animator;
        GameObject oberkoerperKind;
        private Transform detector, glasSpawn, player, OberkoerperSpawn, shotSpawn, enemyLayerDetector;    // A position marking where to check if the player is grounded.
        public GameObject glas, oberkoerper, shotEffect, bullet;
        Rigidbody2D rb;
        BoxCollider2D boxCollider;
        Vector3 richtung;
        public float speed;
        Camera cameraReference;
        float cameraOriginalSize;
        private float dragStore;
        private GlobalVariables gv;

        //zustandsbools
        bool reloading, pistolWalking, walking, aiming, shooting, speaking, unarmedIdle;

        // Start is called before the first frame update
        void Start()
        {
            detector = transform.Find("Detector");
            OberkoerperSpawn = transform.Find("OberkoerperSpawn");
            glasSpawn = transform.Find("GlasSpawn");
            enemyLayerDetector = transform.Find("enemyLayerDetector");
            player = GameObject.FindWithTag("Player").transform;
            cheersSound1 = transform.Find("AudioSources").GetComponents<AudioSource>()[0];
            cheersSound2 = transform.Find("AudioSources").GetComponents<AudioSource>()[1];
            cheersSound3 = transform.Find("AudioSources").GetComponents<AudioSource>()[2];
            cheersSound4 = transform.Find("AudioSources").GetComponents<AudioSource>()[3];
            tossSound = transform.Find("AudioSources").GetComponents<AudioSource>()[4];
            coverSound = transform.Find("AudioSources").GetComponents<AudioSource>()[5];
            reloadSound1 = transform.Find("AudioSources").GetComponents<AudioSource>()[6];
            reloadSound2Spit = transform.Find("AudioSources").GetComponents<AudioSource>()[7];
            reloadSound2 = transform.Find("AudioSources").GetComponents<AudioSource>()[8];
            reloadSound3 = transform.Find("AudioSources").GetComponents<AudioSource>()[9];
            reloadSound4 = transform.Find("AudioSources").GetComponents<AudioSource>()[10];
            reloadSound5 = transform.Find("AudioSources").GetComponents<AudioSource>()[11];
            reloadSound6 = transform.Find("AudioSources").GetComponents<AudioSource>()[12];
            walkSound = transform.Find("AudioSources").GetComponents<AudioSource>()[13];
            shotSound = transform.Find("AudioSources").GetComponents<AudioSource>()[14];
            animator = GetComponent<Animator>();
            stats = GetComponent<EnemyStats>();
            rb = GetComponent<Rigidbody2D>();
            boxCollider = GetComponent<BoxCollider2D>();
            richtung = new Vector3(1, 0, 0);
            cameraReference = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            cameraOriginalSize = cameraReference.orthographicSize;
            dragStore = rb.drag;
            rb.drag = idleDrag;
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                speed *= -1;
            }
            if (speaker)
            {
                animator.SetTrigger("IdleWithoutGlas");
            }
            if (startPistoled)
            {
                pistolWalking = true;
                animator.SetTrigger("PistolWalk");
            }
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            checkDeath();
            if (!stats.dead)
            {
                if (glassIdle)
                {
                    //start cheering before player can see Adliger, else do nothing
                    if (Physics2D.OverlapCircle(detector.position, 25f, 1 << 8) || alarmed)
                    {
                        glassIdle = false;
                        rb.drag = dragStore;
                        StartCoroutine(cheers());
                    }
                }
                if (rotateAim)
                {
                    flipCheck();
                    if (transform.position.y < player.transform.position.y + 3f && ((Mathf.Abs(transform.position.x - player.transform.position.x) > 1f) || (transform.position.y + 0.5f > player.transform.position.y)))
                    {
                        richtung = player.transform.position - transform.position;
                        Quaternion q = Quaternion.LookRotation(richtung);
                        q.y = 0;
                        q.x = 0;
                        oberkoerperKind.transform.rotation = q;
                    }

                }
                if (pistolWalking && !rotateAim)
                {
                    flipCheck();
                    larrySightCheck();
                    if (Physics2D.OverlapCircle(detector.position, detectionRadius, 1 << 8) && larryInSight)
                    {
                        rb.velocity = new Vector2(0.25f * rb.velocity.x, rb.velocity.y);
                        pistolWalking = false;
                        walkSound.Stop();
                        StartCoroutine(aim());
                    }
                    else
                    {
                        if (Physics2D.OverlapCircle(enemyLayerDetector.position, 0.1f, ((1 << 9) | (1 << 11))))
                        {
                            animator.SetTrigger("StandWithPistol");
                            walkSound.Stop();
                            rb.velocity = new Vector2(0, rb.velocity.y);
                        }
                        else
                        {
                            if (larryInSight || Physics2D.OverlapCircle(detector.position, 30f, 1 << 8) && !Physics2D.OverlapCircle(detector.position, 8.1f, 1 << 8))
                            {
                                rb.AddForce(new Vector2(speed, 0));
                                if (animator.GetCurrentAnimatorStateInfo(0).IsName("StandWithPistol") && !walkSound.isPlaying)
                                {
                                    walkSound.PlayDelayed(0);
                                    animator.SetTrigger("PistolWalk");
                                }
                            }
                            else if (Physics2D.OverlapCircle(detector.position, 8f, 1 << 8))
                            {
                                animator.SetTrigger("StandWithPistol");
                                walkSound.Stop();
                                rb.velocity = new Vector2(0.25f * rb.velocity.x, rb.velocity.y);
                            }
                        }
                    }
                }
                if (walking)
                {
                    rb.AddForce(new Vector2(speed, 0));
                }
                if (unarmedIdle)
                {
                    flipCheck();
                    larrySightCheck();
                    if (Physics2D.OverlapCircle(detector.position, 25f, 1 << 8) && larryInSight)
                    {
                        rb.velocity = new Vector2(0.25f * rb.velocity.x, rb.velocity.y);
                        unarmedIdle = false;
                        rb.drag = dragStore;
                        StartCoroutine(equipPistol());
                    }
                }
                if (passiv)
                {
                    flipCheck();
                    if (stats.health < stats.maxHealth)
                    {
                        foreach (Adliger adliger in transform.parent.GetComponentsInChildren<Adliger>())
                        {
                            adliger.larrySpeechReaction();
                            adliger.passiv = false;
                        }
                    }
                }
            }
        }

        //check if the Player is visible to the unit or behind a wall
        void larrySightCheck()
        {
            larryInSight = false;
            if (transform.position.y - 3 < player.transform.position.y && ((Mathf.Abs(transform.position.x - player.transform.position.x) > 1f) || (transform.position.y + 0.5f > player.transform.position.y)))
            {
                Vector3 richtung = player.transform.position - transform.position;
                RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
                foreach (RaycastHit2D hitter in hit)
                {
                    if (hitter.transform.tag == "Ground")
                    {
                        break;
                    }
                    if (hitter.transform.tag == "Player")
                    {
                        larryInSight = true;
                        break;
                    }
                }
            }
        }

        void flipCheck()
        {
            if ((stats.facingRight && player.transform.position.x < transform.position.x) || (!stats.facingRight && player.transform.position.x > transform.position.x))
            {
                flip();
            }
        }

        void flip()
        {
            if (flipActive && ((Mathf.Abs(transform.position.x - player.transform.position.x) > 1f) || (transform.position.y + 0.5f > player.transform.position.y)))
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                speed *= -1;
                stats.facingRight = !stats.facingRight;
            }
        }

        public void playCheerSound()
        {
            int rnd = Random.Range(0, 4);
            switch (rnd)
            {
                case 0:
                    cheersSound1.PlayDelayed(0);
                    break;
                case 1:
                    cheersSound2.PlayDelayed(0);
                    break;
                case 2:
                    cheersSound3.PlayDelayed(0);
                    break;
                case 3:
                    cheersSound4.PlayDelayed(0);
                    break;
                default:
                    cheersSound1.PlayDelayed(0);
                    break;
            }
        }

        //Zustandverhalten
        IEnumerator cheers()
        {
            animator.SetTrigger("Cheers");
            playCheerSound();
            yield return new WaitForSeconds(1f);
            if (!stats.dead)
            {
                animator.SetTrigger("CheersBack");
            }
            yield return new WaitForSeconds(1f);
            if (!stats.dead)
            {
                larrySightCheck();
                if (larryInSight && Physics2D.OverlapCircle(detector.position, detectionRadius, 1 << 8) || alarmed)
                {
                    StartCoroutine(Toss());
                    if (!alarmed)
                    {
                        alertOtherAdlige();
                        alarmed = true;
                    }
                }
                else
                {
                    StartCoroutine(cheers());
                }
            }
        }


        IEnumerator Toss()
        {
            flipCheck();
            tossSound.PlayDelayed(0);
            animator.SetTrigger("TossGlass");
            yield return new WaitForSeconds(0.5f);
            if (!stats.dead)
            {
                GameObject glasKind = Instantiate(glas, glasSpawn.position, Quaternion.identity);
                glasKind.GetComponent<GlasControl>().attackDamage = stats.attacks.GetValueOrDefault("GlassThrow");
                if (stats.facingRight)
                {
                    glasKind.GetComponent<GlasControl>().speed = new Vector2(player.position.x - transform.position.x + 3.5f, player.position.y - transform.position.y + 4.2f);
                }
                else
                {
                    glasKind.GetComponent<GlasControl>().speed = new Vector2(player.position.x - transform.position.x - 3.5f, player.position.y - transform.position.y + 4.2f);
                }
            }
            yield return new WaitForSeconds(0.5f);
            if (!stats.dead)
            {
                StartCoroutine(loadPistol());
            }
        }


        void alertOtherAdlige()
        {
            if (transform.parent.GetComponentsInChildren<Adliger>() != null)
            {
                foreach (Adliger adliger in transform.parent.GetComponentsInChildren<Adliger>())
                {
                    adliger.alarmed = true;
                }
            }
        }


        IEnumerator loadPistol()
        {
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("StandWithPistol"))
            {
                animator.SetTrigger("GetDownWithPistol");
            }
            else
            {
                animator.SetTrigger("Cover");
            }
            float offsetYModifier = boxCollider.size.y / 8;
            float sizeYModifier = boxCollider.size.y / 4f;
            boxCollider.offset = new Vector2(boxCollider.offset.x, boxCollider.offset.y - offsetYModifier);
            boxCollider.size = new Vector2(boxCollider.size.x , boxCollider.size.y - sizeYModifier);
            coverSound.PlayDelayed(0);
            yield return new WaitForSeconds(1f);
            if (!stats.dead)
            {
                animator.SetTrigger("Reload1");
                reloadSound1.PlayDelayed(0);
            }
            yield return new WaitForSeconds(1.1f);
            if (!stats.dead)
            {
                reloadSound2.PlayDelayed(0);
                animator.SetTrigger("Reload2");
            }
            yield return new WaitForSeconds(0.5f);
            if (!stats.dead)
            {
                reloadSound2Spit.PlayDelayed(0);
            }
            yield return new WaitForSeconds(0.75f);
            if (!stats.dead)
            {
                reloadSound3.PlayDelayed(0);
                animator.SetTrigger("Reload3");
            }
            float reloadingSpeed = 2.5f;
            reloadingSpeed -= gv.difficulty * 0.5f;
            yield return new WaitForSeconds(reloadingSpeed);
            if (!stats.dead)
            {
                reloadSound3.Stop();
                reloadSound4.PlayDelayed(0);
                animator.SetTrigger("Reload4");
            }
            yield return new WaitForSeconds(1f);
            if (!stats.dead)
            {
                reloadSound5.PlayDelayed(0);
                animator.SetTrigger("Reload5");
            }
            reloadingSpeed = 5;
            reloadingSpeed -= gv.difficulty;
            yield return new WaitForSeconds(reloadingSpeed);
            if (!stats.dead)
            {
                reloadSound5.Stop();
                reloadSound6.PlayDelayed(0);
                animator.SetTrigger("Reload6");
            }
            yield return new WaitForSeconds(0.25f);
            if (!stats.dead)
            {
                animator.SetTrigger("GetUpWithPistol");
                boxCollider.offset = new Vector2(boxCollider.offset.x, boxCollider.offset.y + offsetYModifier);
                boxCollider.size = new Vector2(boxCollider.size.x, boxCollider.size.y + sizeYModifier);
            }

            if (gv.difficulty <= 3)
            {
                yield return new WaitForSeconds(1f);
            }
            else
            {
                yield return new WaitForSeconds(0.25f);
            }
            if (!stats.dead)
            {
                larrySightCheck();
                //start cheering before player can see Adliger, else do nothing
                if (!Physics2D.OverlapCircle(detector.position, detectionRadius, 1 << 8) ||!larryInSight)
                {
                    animator.SetTrigger("PistolWalk");
                    walkSound.PlayDelayed(0);
                    pistolWalking = true;
                }
                else
                {
                    StartCoroutine(aim());
                }
            }
        }


        IEnumerator aim()
        {
            rotateAim = true;
            animator.SetTrigger("ShotLegs");
            oberkoerperKind = Instantiate(oberkoerper, OberkoerperSpawn.position, Quaternion.identity);
            oberkoerperKind.transform.parent = gameObject.transform;
            richtung = new Vector3(1, 0, 0);
            if (!stats.facingRight)
            {
                Vector3 theScale = oberkoerperKind.transform.localScale;
                theScale.x *= -1;
                oberkoerperKind.transform.localScale = theScale;
                richtung.x *= -1;
            }
            shotSpawn = oberkoerperKind.transform.Find("ShotSpawn");
            float minimumAimTime = 2.75f;
            float maximumAimTime = 3.25f;
            minimumAimTime -= gv.difficulty * 0.5f;
            maximumAimTime -= gv.difficulty * 0.5f;
            
            float rnd = Random.Range(minimumAimTime, maximumAimTime);
            yield return new WaitForSeconds(rnd);
            if (!stats.dead)
            {
                StartCoroutine(shotPistol());
            }
        }


        IEnumerator shotPistol()
        {
            shotSound.PlayDelayed(0);
            yield return new WaitForSeconds(0.07f);
            if (!stats.dead)
            {
                Quaternion q = Quaternion.LookRotation(richtung);
                q.y = 0;
                q.x = 0;
                richtung = richtung.normalized * 60f;

                float smokeLifetime = shotEffect.GetComponent<ParticleSystem>().main.startLifetime.Evaluate(1);
                smokeLifetime += gv.difficulty;
                GameObject smokeChild = Instantiate(shotEffect, shotSpawn.position, Quaternion.identity);
                ParticleSystem.MainModule main = smokeChild.GetComponent<ParticleSystem>().main;
                main.startLifetime = new ParticleSystem.MinMaxCurve(smokeLifetime);
                smokeChild.GetComponent<ParticleSystem>().Play();
                
                GameObject bulletChild = Instantiate(bullet, shotSpawn.position, Quaternion.identity);
                if (!stats.facingRight)
                {
                    Vector3 theScale = bulletChild.transform.localScale;
                    theScale.x *= -1;
                    bulletChild.transform.localScale = theScale;
                }

                bulletChild.GetComponent<BulletControl>().speed = new Vector2(richtung.x, richtung.y);
                bulletChild.GetComponent<BulletControl>().attackDamage = stats.attacks.GetValueOrDefault("PistolShot");
                bulletChild.transform.rotation = q;
                rotateAim = false;
            }
            yield return new WaitForSeconds(0.5f);
            if (!stats.dead)
            {
                Destroy(oberkoerperKind);
                animator.SetTrigger("StandWithPistol");
                StartCoroutine(loadPistol());
            }
        }

        IEnumerator equipPistol()
        {
            animator.SetTrigger("GrabPistol");
            yield return new WaitForSeconds(0.5f);
            animator.SetTrigger("StandWithPistol");
            StartCoroutine(loadPistol());
        }

        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                stats.dead = true;
                StartCoroutine(Death());
            }
        }

        IEnumerator Death()
        {
            stats.dead = true;
            reloadSound3.Stop();
            reloadSound5.Stop();
            walkSound.Stop();
            rotateAim = false;
            animator.SetBool("Dead", true);
            animator.SetTrigger("Death");
            if (oberkoerperKind != null)
            {
                Destroy(oberkoerperKind);
            }
            yield return new WaitForSeconds(0.25f);
            if (oberkoerperKind != null)
            {
                Destroy(oberkoerperKind);
            }
            GetComponent<HumanDeathSequence>().Death();
        }

        public void goIntoCrowd()
        {
            StartCoroutine(startGoAnimation());
        }

        public IEnumerator startGoAnimation()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().BlockInput = true;
            animator.SetTrigger("IdleWithoutGlas");
            Adliger thisAdliger = GetComponent<Adliger>();
            foreach (Adliger adliger in transform.parent.GetComponentsInChildren<Adliger>())
            {
                if(adliger != thisAdliger)
                {
                    adliger.GetComponent<Animator>().SetTrigger("Cheers");
                    adliger.playCheerSound();
                }
            }
            yield return new WaitForSeconds(2f);
            animator.SetTrigger("Walk");
            walking = true;
            walkSound.PlayDelayed(0);
            rb.drag = dragStore;
            yield return new WaitForSeconds(1.25f);
            walking = false;
            animator.SetTrigger("IdleWithoutGlas");
            walkSound.Stop();
            StartCoroutine(zoom(cameraOriginalSize));
            yield return new WaitForSeconds(1f);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = GameObject.FindGameObjectWithTag("Player").transform;
            Camera2DFollow camera2DFollowScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
            for (int i = 30; i > 0; i--)
            {
                camera2DFollowScript.SetDamping((float)i / 100);
                yield return new WaitForSeconds(1f / 30f);
            }
           
            foreach (Adliger adliger in transform.parent.GetComponentsInChildren<Adliger>())
            {
                if (adliger != thisAdliger)
                {
                    adliger.GetComponent<Animator>().SetTrigger("CheersBack");
                }
                adliger.passiv = true;
            }
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().BlockInput = false;
        }

        public void larrySpeechReaction()
        {
            StartCoroutine(speechReactionRandomDelay());
        }

        public IEnumerator speechReactionRandomDelay()
        {
            float rnd = Random.Range(0f, 0.5f);
            yield return new WaitForSeconds(rnd);
            if (!speaker)
            {
                StartCoroutine(Toss());
            }
            else
            {
                pistolWalking = true;
                animator.SetTrigger("PistolWalk");
            }
            passiv = false;
        }



        public IEnumerator zoom(float size)
        {
            yield return new WaitForSeconds(0.02f);
            if (cameraReference.orthographicSize - 0.06f < size && size < cameraReference.orthographicSize + 0.06f)
            {
                yield break;
            }
            else if (cameraReference.orthographicSize > size)
            {
                cameraReference.orthographicSize -= 0.05f;
                StartCoroutine(zoom(size));
            }
            else if (cameraReference.orthographicSize < size)
            {
                cameraReference.orthographicSize += 0.05f;
                StartCoroutine(zoom(size));
            }
        }
    }
}