﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    public class BulletControl : MonoBehaviour
    {
        public Vector2 speed;
        Rigidbody2D rb;
        bool collision = false;
        GameObject Player;
        private PlatformerCharacter2D Larry;
        public int attackDamage = 50;
        AudioSource hitSound;

        // Use this for initialization
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            rb.velocity = speed;
            StartCoroutine(Abschuss());
            Player = GameObject.FindWithTag("Player");
            Larry = Player.GetComponent<PlatformerCharacter2D>();
            hitSound = GetComponent<AudioSource>();
            if (Physics2D.OverlapCircle(transform.position, 0.35f, 1 << 9) && collision == true)//layer Ebene 9 = Enemy
            {
                StartCoroutine(PlayerHit());
            }
        }

        private void FixedUpdate()
        {
            rb.velocity = new Vector2(speed.x, speed.y);
            if (Physics2D.OverlapCircle(transform.position, 0.35f, 1 << 9) && collision == true)//layer Ebene 9 = Enemy
            {
                Destroy(gameObject);
            }
            if (Physics2D.OverlapCircle(transform.position, 0.35f, 1 << 11) && collision == true)//layer Ebene 11 = Boden
            {
                Destroy(gameObject);
            }
            if (Physics2D.OverlapCircle(transform.position, 0.35f, 1 << 14) && collision == true)//layer Ebene 14 = Objecte
            {
                Collider2D other = Physics2D.OverlapCircle(transform.position, 0.35f, 1 << 14);
                if (other.gameObject.GetComponent<ObjektStats>().destructable)
                {
                    other.gameObject.GetComponent<ObjektStats>().hitPoints -= attackDamage;
                    if (other.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                    {
                        other.gameObject.GetComponent<ObjektStats>().startDeath();
                    }
                }
                Destroy(gameObject);
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                StartCoroutine(PlayerHit());
            }
            if (other.gameObject.CompareTag("Ground"))
            {
                Destroy(gameObject);
            }
            if (collision == true)
            {
                if (other.gameObject.CompareTag("Enemy"))
                {
                    Destroy(gameObject);
                }
                if (other.gameObject.CompareTag("Object"))
                {
                    if (other.gameObject.GetComponent<ObjektStats>().destructable)
                    {
                        other.gameObject.GetComponent<ObjektStats>().hitPoints -= attackDamage;
                        if (other.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                        {
                            other.gameObject.GetComponent<ObjektStats>().startDeath();
                        }
                    }
                    Destroy(gameObject);
                }
                Destroy(gameObject);
            }
        }
        IEnumerator Abschuss()
        {
            yield return new WaitForSeconds(0.06f);
            collision = true;
        }
        IEnumerator PlayerHit()
        {
            collision = false;
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<ParticleSystem>().Stop();
            if(Larry.Hit(attackDamage, DamageTypeToPlayer.Bullet ,new Vector2(rb.velocity.x, rb.velocity.y), transform.position, gameObject, Guid.NewGuid())){
                hitSound.PlayDelayed(0);
            }
            yield return new WaitForSeconds(0.5f);
            Destroy(gameObject);
        }
    }
}