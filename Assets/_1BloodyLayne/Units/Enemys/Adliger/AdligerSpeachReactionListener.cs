﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys.Adliger;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class AdligerSpeachReactionListener : MonoBehaviour
    {
        public GameObject listenDialog, adligerGroup;
        bool listen;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (listenDialog.activeSelf)
            {
                listen = true;
            }
            if (!listenDialog.activeSelf && listen)
            {
                foreach (Adliger adliger in adligerGroup.GetComponentsInChildren<Adliger>())
                {
                    adliger.GetComponent<Adliger>().larrySpeechReaction();
                }
                listen = false;
                Destroy(gameObject);
            }
        }
    }
}
