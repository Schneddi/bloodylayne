﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class GlasControl : MonoBehaviour
    {
        Vector3 richtung = new Vector3(1,0,0);
        public Vector2 speed;
        Rigidbody2D rb;
        bool collision = false;
        GameObject Player;
        public GameObject shatterObject;
        private PlatformerCharacter2D Larry;
        public int attackDamage = 20;
        AudioSource hitSound;

        // Use this for initialization
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            if (speed.y > 17f)
            {
                speed.y = 17f;
            }
            else if (speed.y < 0)
            {
                speed.y = 5f;
            }
            if (speed.x > 20f)
            {
                speed.x = 20f;
            }
            rb.velocity = speed;
            StartCoroutine(Abschuss());
            Player = GameObject.FindWithTag("Player");
            Larry = Player.GetComponent<PlatformerCharacter2D>();
            hitSound = GetComponent<AudioSource>();
            rb.velocity = new Vector2(speed.x, speed.y);
        }

        // Update is called once per frame
        void Update()
        {
        }

        private void FixedUpdate()
        {
            transform.Rotate(0, 0, 50 * Time.deltaTime);

            if (Physics2D.OverlapCircle(transform.position, 0.35f, 1 << 9) && collision == true)//layer Ebene 9 = Enemy
            {
                GameObject shatterChild = Instantiate(shatterObject, transform.position, Quaternion.identity);
                shatterChild.transform.Rotate(transform.rotation.eulerAngles);
                Destroy(gameObject);
            }
            if (Physics2D.OverlapCircle(transform.position, 0.35f, 1 << 8) && collision == true)//layer Ebene 8 = Player
            {
                LarryHit();
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {

            if (collision == true)
            {
                if (other.gameObject.CompareTag("Enemy"))
                {
                    GameObject shatterChild = Instantiate(shatterObject, transform.position, Quaternion.identity);
                    shatterChild.transform.Rotate(transform.rotation.eulerAngles);
                    Destroy(gameObject);
                }
                if (other.gameObject.CompareTag("Object"))
                {
                    if (other.gameObject.GetComponent<ObjektStats>().destructable)
                    {
                        other.gameObject.GetComponent<ObjektStats>().hitPoints -= attackDamage;
                        if (other.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                        {
                            other.gameObject.GetComponent<ObjektStats>().startDeath();
                        }
                    }
                    GameObject shatterChild = Instantiate(shatterObject, transform.position, Quaternion.identity);
                    shatterChild.transform.Rotate(transform.rotation.eulerAngles);
                    Destroy(gameObject);
                }

                if (other.gameObject.CompareTag("Ground"))
                {
                    GameObject shatterChild = Instantiate(shatterObject, transform.position, Quaternion.identity);
                    shatterChild.transform.Rotate(transform.rotation.eulerAngles);
                    Destroy(gameObject);
                }
            }
        }
        IEnumerator Abschuss()
        {
            yield return new WaitForSeconds(0.2f);
            collision = true;
        }
        void LarryHit()
        {
            collision = false;
            GameObject shatterChild = Instantiate(shatterObject, transform.position, Quaternion.identity);
            shatterChild.transform.Rotate(transform.rotation.eulerAngles);
            Destroy(gameObject);
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            Larry.Hit(attackDamage, DamageTypeToPlayer.BluntProjectile, transform.position, gameObject, Guid.NewGuid());
            Destroy(gameObject);
        }
    }
}