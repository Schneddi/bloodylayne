﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys.Adliger;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class AdligerGoIntoCrowd : MonoBehaviour
    {
        public GameObject listenDialog, adliger;
        bool listen;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (listenDialog.activeSelf)
            {
                listen = true;
            }
            if (!listenDialog.activeSelf && listen)
            {
                adliger.GetComponent<Adliger>().goIntoCrowd();
                listen = false;
                Destroy(gameObject);
            }
        }
    }
}
