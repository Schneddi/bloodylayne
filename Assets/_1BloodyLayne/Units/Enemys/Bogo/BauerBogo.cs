﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(HumanDeathSequence))]
    public class BauerBogo : MonoBehaviour
    {
        public bool noCutScene;
        
        EnemyStats stats;
        private Animator animator;
        bool shaking;
        private Transform damageCheck1, damageCheck2;
        AudioSource hitSound, shakeSound;
        private PlatformerCharacter2D larry;
        public GameObject partikel, deathSound;
        Rigidbody2D rb;

        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            stats = GetComponent<EnemyStats>();
            damageCheck1 = transform.Find("DamageCheck1");
            damageCheck2 = transform.Find("DamageCheck2");
            hitSound = GetComponents<AudioSource>()[0];
            shakeSound = GetComponents<AudioSource>()[1];
            larry = GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>();
            rb = GetComponent<Rigidbody2D>();
            if (noCutScene)
            {
                startShaking();
            }
        }

        // Update is called once per frame
        void Update()
        {
            checkDeath();
            if (!stats.dead)
            {
                if (shaking)
                {
                    if (Physics2D.OverlapCircle(damageCheck1.position, 0.3f, 1 << 8) || Physics2D.OverlapCircle(damageCheck2.position, 0.3f, 1 << 8))
                    {
                        if (larry.Hit(stats.attacks.GetValueOrDefault("MeleeStab"), DamageTypeToPlayer.MeleeSwing, new Vector2(18f, 0), damageCheck1.position, gameObject, Guid.NewGuid()))
                        {
                            hitSound.PlayDelayed(0);
                        }
                    }
                }
            }
        }

        public void startShaking()
        {
            shakeSound.PlayDelayed(0);
            shaking = true;
            animator.SetTrigger("Shaking");
        }

        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }

        private void Death()
        {
            stats.dead = true;
            shakeSound.Stop();
            animator.SetBool("Death", true);
            animator.SetTrigger("Dead");
            GetComponent<HumanDeathSequence>().Death();
        }
    }
}
