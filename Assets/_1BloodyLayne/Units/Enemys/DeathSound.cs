﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSound : MonoBehaviour {
    AudioSource deathSound;
    AudioSource[] deathSounds;
    AudioClip deathClip;
    public Transform sourceUnit;
	// Use this for initialization
	void Start () {
        deathSounds = GetComponents<AudioSource>();
        deathSound = deathSounds[Random.Range(0,deathSounds.Length)];
        deathClip = deathSound.clip;
        deathSound.PlayDelayed(0);
        StartCoroutine(destroyAfterSound());
	}
	
    IEnumerator destroyAfterSound()
    {
        yield return new WaitForSeconds(deathClip.length);
        Destroy(gameObject);
    }

	// Update is called once per frame
	void FixedUpdate () {
		if(sourceUnit != null)
        {
            transform.position = sourceUnit.position;
        }
	}
}
