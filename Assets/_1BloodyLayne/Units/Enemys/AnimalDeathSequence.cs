using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityStandardAssets._2D;

public class AnimalDeathSequence : MonoBehaviour
{
    public bool multipleRenderers;
    public GameObject splatterParticle, healParticle;
    
    private bool fadingOut, playingDeathAnim, bloodmagicFade;
    private SpriteRenderer[] sRenderers;
    private SpriteRenderer sRenderer;
    private List<Material> deathMaterials;
    private Material deathMaterial;
    private EnemyStats stats;
    private AudioSource[] dissolveSounds;
    private GameObject dissolveSoundsObject;
    private float deathStartTime, deathAnimDuration;
    private Rigidbody2D rb;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        stats = GetComponent<EnemyStats>();
        dissolveSoundsObject = GameObject.Find("DissolveSounds");
        if (multipleRenderers)
        {
            sRenderers = transform.GetChild(0).GetComponentsInChildren<SpriteRenderer>();
            deathMaterials = new List<Material>();
            foreach (SpriteRenderer sprite in sRenderers)
            {
                deathMaterials.Add(sprite.material);
            }
        }
        else
        {
            sRenderer = GetComponent<SpriteRenderer>();
            deathMaterial = GetComponent<SpriteRenderer>().material;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (fadingOut)
        {
            if (multipleRenderers)
            {
                foreach (SpriteRenderer sprite in sRenderers)
                {
                    sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b,
                        sprite.color.a - Time.deltaTime);
                    if (sprite.color.a < 0.01f)
                    {
                        Destroy(gameObject);
                    }
                }
            }
            else
            {
                sRenderer.color = new Color(sRenderer.color.r, sRenderer.color.g, sRenderer.color.b,
                    sRenderer.color.a - Time.deltaTime);
                if (sRenderer.color.a < 0.01f)
                {
                    Destroy(gameObject);
                }
            }
        }
        if (bloodmagicFade)
        {
            float deathProgressPercent = (Time.time - deathStartTime) / deathAnimDuration;

            if (multipleRenderers)
            {
                foreach (Material material in deathMaterials)
                {
                    material.SetFloat("_Fade", 1 - deathProgressPercent);
                }
            }
            else
            {
                deathMaterial.SetFloat("_Fade", 1 - deathProgressPercent);
            }
        }
    }

    public IEnumerator Death()
    {
        if (!playingDeathAnim)
        {
            deathStartTime = Time.time;
            playingDeathAnim = true;
            gameObject.layer = 17;
            rb.freezeRotation = false;
            rb.gravityScale = 1;
            rb.mass = 1;
            stats.dead = true;
            if (stats.lastDamageType == DamageTypeToEnemy.Bloodmagic)
            {
                GameObject dissolveInstance = Instantiate(dissolveSoundsObject, transform.position, Quaternion.identity);
                dissolveSounds = dissolveInstance.GetComponents<AudioSource>();
                int r = Random.Range(0, 3);
                dissolveSounds[r].PlayOneShot(dissolveSounds[r].clip);
                bloodmagicFade = true;
                deathStartTime = Time.time;
                deathAnimDuration = 1f;
                yield return new WaitForSeconds(deathAnimDuration);
                Destroy(gameObject);
            }

            if (stats.lastDamageType == DamageTypeToEnemy.Bite)
            {
                if (multipleRenderers)
                {
                    foreach (SpriteRenderer sprite in sRenderers)
                    {
                        sprite.color = new Color(255, 0, 0, 1);
                    }
                }
                else
                {
                    sRenderer.color = new Color(255, 0, 0, 1);
                }
                Instantiate(healParticle, transform.position, Quaternion.identity);
                yield return new WaitForSeconds(1f);
            }
            else
            {
                if (multipleRenderers)
                {
                    foreach (SpriteRenderer sprite in sRenderers)
                    {
                        sprite.color = new Color(255, 0, 0, 1);
                    }
                }
                else
                {
                    sRenderer.color = new Color(255, 0, 0, 1);
                }
                yield return new WaitForSeconds(1f);
                Instantiate(splatterParticle, transform.position, Quaternion.identity);
            }
            fadingOut = true;
        }
    }
}
