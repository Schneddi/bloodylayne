using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class BoomerangMagicControl : MonoBehaviour
    {
        GameObject player;
        private PlatformerCharacter2D larry;
        public GameObject deathSound;
        Rigidbody2D rbLarry;
        Rigidbody2D rb;
        public float speed = 15f, distance = 10, radius;
        public int attackDamage = 35;
        private Vector2 startPos, direction;
        private bool flyBack;

        // Use this for initialization
        void Start()
        {
            player = GameObject.FindWithTag("Player");
            startPos = transform.position;
            Destroy(gameObject, 4f);
        }

        // Update is called once per frame
        void Update()
        {
            if (Vector2.Distance(transform.position, startPos) > distance && !flyBack)
            {
                flyBack = true;
                speed *= -1;
                rb.velocity = new Vector2(direction.x * speed, direction.y * speed);
                setRotation(-direction.x, -direction.y);
            }
            if (Physics2D.OverlapCircle(transform.position, radius, 1 << 8))
            {
                if (!larry.invulnerable)
                {
                    Instantiate(deathSound, transform.position, Quaternion.identity);
                    larry.Hit(attackDamage, DamageTypeToPlayer.MagicProjectile, transform.position, gameObject, new Guid());
                }
            }
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Ground") && !flyBack)
            {
                flyBack = true;
                speed *= -1;
                rb.velocity = new Vector2(direction.x * speed, direction.y * speed);
                setRotation(-direction.x, -direction.y);
            }
            else if (other.gameObject.CompareTag("Player"))
            {
                if (!player.GetComponent<PlatformerCharacter2D>().invulnerable)
                {
                    Instantiate(deathSound, transform.position, Quaternion.identity);
                }
                larry.Hit(attackDamage, DamageTypeToPlayer.MagicProjectile, transform.position, gameObject, new Guid());
            }
        }
        
        public void launch(float x, float y)
        {
            player = GameObject.FindWithTag("Player");
            larry = player.GetComponent<PlatformerCharacter2D>();
            rb = GetComponent<Rigidbody2D>();
            direction = new Vector2(x, y).normalized;
            rb.velocity = new Vector2(direction.x * speed, direction.y * speed);
            setRotation(x, y);
        }

        private void setRotation(float x, float y)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.back, new Vector3(x, y, 0));
            transform.Rotate(new Vector3(0,0,180));
        }

    }
}
