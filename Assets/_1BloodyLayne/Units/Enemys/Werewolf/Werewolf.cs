using System;
using System.Collections;
using System.Collections.Generic;
using UI.PlayerUI;
using Units.Enemys;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Werewolf : MonoBehaviour
    {
        public float speed;
        public NosEnd2Control nosEnd2Control;
        public GameObject splatterEffect, howlEffect1, howlEffect2, howlEffect3, boomerangMagic, chargedSpell, fadeScreen, bossPickUp;
        
        private EnemyStats stats;
        private Animator animator;
        private GameObject player, howlEffectChild;
        private PlatformerCharacter2D playerChar;
        private Platformer2DUserControl userControl;
        private bool flipCheckActive, pounceActive, flipOnCooldown;
        private String state, nextState;
        private Rigidbody2D rb;
        private BoxCollider2D col;
        private int round;
        private float startPosY;
        private Transform howlEffectSpawn,
            instantDeathLarryPos,
            instantDeathBloodSpawn,
            magicBallSpawn,
            topRayCheck;
        private AudioSource[] sounds;
        private AudioSource howlSound, howlSoundTriple, counterAttackSound, meleeHitSound, killSound, walkLoop, fastWalkLoop, pounceSound, pounceFailSound, chargedSpellAttack, deathSound, fastPounceSound, crawlSound;
        private Camera2DFollow camerascript;
        
        // Start is called before the first frame update
        void Start()
        {
            initVars();
        }

        public void initVars()
        {
            stats = GetComponent<EnemyStats>();
            animator = GetComponent<Animator>();
            rb = GetComponent<Rigidbody2D>();
            player = GameObject.FindWithTag("Player");
            playerChar = player.GetComponent<PlatformerCharacter2D>();
            userControl = player.GetComponent<Platformer2DUserControl>();
            howlEffectSpawn = transform.Find("Sensors").Find("HowlEffectSpawn");
            instantDeathLarryPos = transform.Find("Sensors").Find("InstantDeathLarryPos");
            instantDeathBloodSpawn = transform.Find("Sensors").Find("InstantDeathBloodSpawn");
            magicBallSpawn = transform.Find("Sensors").Find("MagicBallSpawn");
            topRayCheck = transform.Find("Sensors").Find("TopRayCheck");
            sounds = transform.Find("Sounds").GetComponents<AudioSource>();
            col = GetComponent<BoxCollider2D>();
            howlSound = sounds[0];
            howlSoundTriple = sounds[1];
            counterAttackSound = sounds[2];
            meleeHitSound = sounds[3];
            killSound = sounds[4];
            walkLoop = sounds[5];
            fastWalkLoop = sounds[6];
            pounceSound = sounds[7];
            pounceFailSound = sounds[8];
            chargedSpellAttack = sounds[9];
            deathSound = sounds[10];
            fastPounceSound = sounds[11];
            crawlSound = sounds[12];
            state = "dialog";
            startPosY = transform.position.y;
            camerascript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
        }

        private void FixedUpdate()
        {
            if (state.Equals("counterAttack"))
            {
                Vector2 direction = (player.transform.position - transform.position).normalized;
                rb.velocity = new Vector2(direction.x * 60, direction.y * 80);
            }
            if (state.Equals("walkPounce"))
            {
                rb.AddForce(new Vector2(speed + 0.5f,0));
            }
            if (state.Equals("fastWalkPounce"))
            {
                rb.AddForce(new Vector2(speed,0));
                if (Vector2.Distance(transform.position, player.transform.position) < 8)
                {
                    fastWalkLoop.Stop();
                    StartCoroutine(fastPounce());
                }
            }
        }

        // Update is called once per frame
        private void Update()
        {
            if (!stats.dead)
            {
                checkDeath();
            }
            if (!stats.dead)
            {
                FlipCheck(false);
                if ((state.Equals("howlInvul") || state.Equals("tripleHowlInvul") || state.Equals("crawl")) && stats.recievedAttacks > 0)
                {
                    crawlSound.Stop();
                    howlSound.Stop();
                    howlSoundTriple.Stop();
                    if (howlEffectChild != null)
                    {
                        Destroy(howlEffectChild);
                    }
                    StartCoroutine(counterAttack());
                }
                else if ((state.Equals("crawl")) && (transform.position.y + 1 < player.transform.position.y))
                {
                    StartCoroutine(counterAttack());
                }

                if (pounceActive)
                {
                    if (col.IsTouching(player.GetComponent<BoxCollider2D>()))
                    {
                        ContactPoint2D[] contacts = new ContactPoint2D[1];
                        col.GetContacts(contacts);
                        Vector2 contactPoint = contacts[0].point;
                        doPounceDamage(contactPoint);
                    }
                }
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (pounceActive && other.collider.name == "Player")
            {
                doPounceDamage(other.GetContact(0).point);
            }
        }

        //------------------------------------Fight Functions--------------------------------------
        private void startNextState()
        {
            if (!stats.dead)
            {
                flipCheckActive = true;
                FlipCheck(false);
                flipCheckActive = false;
                switch (nextState)
                {
                    case "walkPounce":
                        if (stats.health > 0)
                        {
                            StartCoroutine(walkPounce());
                        }
                        else
                        {
                            spawnBossPickup();
                            round = 2;
                            stats.health = stats.maxHealth;
                            StartCoroutine(TripleHowl());
                        }
                        break;
                    case "magicBoomerang":
                        if (stats.health > 0)
                        {
                            StartCoroutine(magicBoomerang());
                        }
                        else
                        {
                            spawnBossPickup();
                            round = 2;
                            stats.health = stats.maxHealth;
                            StartCoroutine(TripleHowl());
                        }
                        break;
                    case "howl":
                        if (stats.health > 0)
                        {
                            StartCoroutine(howl());
                        }
                        else
                        {
                            spawnBossPickup();
                            round = 2;
                            stats.health = stats.maxHealth;
                            StartCoroutine(TripleHowl());
                        }
                        break;
                    case "chargedBall":
                        if (stats.health > 0)
                        {
                            StartCoroutine(chargedBall());
                        }
                        else
                        {
                            spawnBossPickup();
                            round = 3;
                            stats.health = stats.maxHealth;
                            state = "round3Combo";
                            StartCoroutine(howl());
                        }
                        break;
                    case "tripleHowl":
                        if (stats.health > 0)
                        {
                            StartCoroutine(TripleHowl());
                        }
                        else
                        {
                            spawnBossPickup();
                            round = 3;
                            stats.health = stats.maxHealth;
                            state = "round3Combo";
                            StartCoroutine(howl());
                        }
                        break;
                    case "doublePounceAttack":
                        if (stats.health > 0)
                        {
                            StartCoroutine(doublePounceAttack());
                        }
                        else
                        {
                            spawnBossPickup();
                            round = 3;
                            stats.health = stats.maxHealth;
                            state = "round3Combo";
                            StartCoroutine(howl());
                        }
                        break;
                    case "round3Combo":
                        StartCoroutine(doublePounceAttack());
                        break;
                    case "round3ComboHowl":
                        StartCoroutine(howl());
                        break;
                    case "doubleMagicBall":
                        StartCoroutine(doubleMagicBoomerang());
                        break;
                    case "crawl":
                        StartCoroutine(crawl());
                        break;
                    default:
                        if (stats.health > 0)
                        {
                            StartCoroutine(walkPounce());
                        }
                        else
                        {
                            state = "round3Combo";
                            StartCoroutine(howl());
                        }
                        break;
                }
            }
            
        }
        
        //round1
        
        private IEnumerator howl()
        {
            transform.position = new Vector3(transform.position.x, startPosY, transform.position.z);
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            animator.SetTrigger("Howl");
            howlSound.PlayDelayed(0);
            state = "howl";
            if (round == 3)
            {
                nextState = "doubleMagicBall";
            }
            else
            {
                nextState = "magicBoomerang";
            }
            yield return new WaitForSeconds(0.5f);
            if (!stats.dead)
            {
                state = "howlInvul";
                flipCheckActive = false;
                howlEffectChild = Instantiate(howlEffect1, howlEffectSpawn.position, Quaternion.identity);
                stats.unverwundbar = true;
                stats.recievedAttacks = 0;
                yield return new WaitForSeconds(5f);
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                stats.unverwundbar = false;
            }
            if (state.Equals("howlInvul") && !stats.dead)
            {
                Destroy(howlEffectChild);
                animator.SetTrigger("HowlExit");
                state = "howlExit";
                yield return new WaitForSeconds(0.5f);
                startNextState();
            }
        }

        private IEnumerator counterAttack()
        {
            if (howlEffectChild != null){
                
                Destroy(howlEffectChild);
            }

            flipCheckActive = true;
            counterAttackSound.Play(0);
            animator.SetTrigger("CounterAttack");
            state = "counterAttackWindUp";
            yield return new WaitForSeconds(0.25f);
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            if (col.IsTouching(player.GetComponent<BoxCollider2D>()) && !stats.dead)
            {
                ContactPoint2D[] contacts = new ContactPoint2D[1];
                col.GetContacts(contacts);
                Vector2 contactPoint = contacts[0].point;
                doPounceDamage(contactPoint);
            }
            else
            {
                pounceActive = true;
                state = "counterAttack";
                rb.velocity = (player.transform.position - transform.position).normalized * 25;
            }
        }

        private void doPounceDamage(Vector2 touchPosition)
        {
            rb.velocity = new Vector2(0,-30);
            pounceActive = false;
            if (playerChar.GetComponent<PlayerStats>().health <= stats.attacks.GetValueOrDefault("PounceAttack") && !playerChar.dead)
            {
                StartCoroutine(instaKill());
            }
            else
            {
                if (!playerChar.dead)
                {
                    if (playerChar.Hit(stats.attacks.GetValueOrDefault("PounceAttack"), DamageTypeToPlayer.MeleeSwing, new Vector2(rb.velocity.x, rb.velocity.y), touchPosition, gameObject, new Guid()))
                    {
                        meleeHitSound.PlayDelayed(0);
                    }
                }
                startNextState();
            }
        }
        
        private IEnumerator instaKill()
        {
            state = "killing";
            stats.unverwundbar = true;
            if ((playerChar.m_FacingRight && !stats.facingRight) || (!playerChar.m_FacingRight && stats.facingRight))
            {
                playerChar.Flip();
            }
            if (playerChar.crouchIn)
            {
                playerChar.StandUp();
            }
            transform.position = new Vector3(transform.position.x, startPosY, transform.position.z);
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            playerChar.KillPlayer(DamageTypeToPlayer.WerewolfBite);
            player.GetComponent<BoxCollider2D>().enabled = false;
            player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            player.transform.position = instantDeathLarryPos.position;
            animator.SetTrigger("LarryGrab");
            killSound.PlayDelayed(0);
            yield return new WaitForSeconds(1.75f);
            Instantiate(splatterEffect, instantDeathBloodSpawn.position, Quaternion.identity);
            yield return new WaitForSeconds(0.75f);
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            stats.unverwundbar = false;
            yield return new WaitForSeconds(0.25f);
            startNextState();
        }
        
        private IEnumerator walkPounce()
        {
            flipCheckActive = true;
            state = "walkPounce";
            nextState = "howl";
            if (col.IsTouching(player.GetComponent<BoxCollider2D>()))
            {
                StartCoroutine(fastPounce());
            }
            else
            {
                animator.SetTrigger("Walk");
                walkLoop.PlayDelayed(0);
                yield return new WaitForSeconds(1);
                walkLoop.Stop();
                flipCheckActive = false;
                if (Vector2.Distance(transform.position, player.transform.position) < 10)
                {
                    StartCoroutine(slowPounce());
                }
                else
                {
                    fastWalkLoop.PlayDelayed(0);
                    state = "fastWalkPounce";
                    animator.SetTrigger("FastWalk");
                }

                yield return new WaitForSeconds(5);
                if (state.Equals("fastWalkPounce"))
                {
                    flipCheckActive = true;
                    startNextState();
                }
            }
        }
        
        private IEnumerator slowPounce()
        {
            animator.SetTrigger("LaunchAttack");
            pounceSound.PlayDelayed(0);
            state = "pounce";
            FlipCheck(true);
            yield return new WaitForSeconds(1f);
            state = "pounceAttack";
            float pounceDirection = 50;
            if (!stats.facingRight)
            {
                pounceDirection = -50;
            }

            rb.velocity = new Vector2(pounceDirection, 5);
            if (col.IsTouching(player.GetComponent<BoxCollider2D>()) && !stats.dead)
            {
                ContactPoint2D[] contacts = new ContactPoint2D[1];
                col.GetContacts(contacts);
                Vector2 contactPoint = contacts[0].point;
                doPounceDamage(contactPoint);
            }
            else
            {
                pounceActive = true;
            }
            yield return new WaitForSeconds(0.75f);
            if (state.Equals("pounceAttack"))
            {
                pounceActive = false;
                animator.SetTrigger("PounceFail");
                pounceFailSound.PlayDelayed(0);
                rb.velocity = rb.velocity * 0.1f;
                yield return new WaitForSeconds(1f);
                flipCheckActive = true;
                startNextState();
            }
        }
        
        private IEnumerator fastPounce()
        {
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            animator.SetTrigger("FastPounce");
            fastPounceSound.PlayDelayed(0);
            state = "pounce";;
            FlipCheck(true);
            yield return new WaitForSeconds(0.25f);
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            float pounceDirection = 50;
            if (!stats.facingRight)
            {
                pounceDirection = -50;
            }

            rb.velocity = new Vector2(pounceDirection, 5);
            state = "pounceAttack";
            if (col.IsTouching(player.GetComponent<BoxCollider2D>()))
            {
                ContactPoint2D[] contacts = new ContactPoint2D[1];
                col.GetContacts(contacts);
                Vector2 contactPoint = contacts[0].point;
                doPounceDamage(contactPoint);
            }
            else
            {
                pounceActive = true;
            }
            yield return new WaitForSeconds(0.75f);
            if (state.Equals("pounceAttack"))
            {
                rb.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
                pounceActive = false;
                animator.SetTrigger("PounceFail");
                pounceFailSound.PlayDelayed(0);
                yield return new WaitForSeconds(1f);
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                flipCheckActive = true;
                startNextState();
            }
        }
        
        private IEnumerator magicBoomerang()
        {
            yield return new WaitForSeconds(0.25f);
            state = "magicBoomerang";
            nextState = "walkPounce";
            animator.SetTrigger("SpellAttack");
            yield return new WaitForSeconds(1);
            flipCheckActive = false;
            BoomerangMagicControl magicBoomerangChild = Instantiate(boomerangMagic, magicBallSpawn.position, Quaternion.identity).GetComponent<BoomerangMagicControl>();
            float childDirection = 1;
            if (!stats.facingRight)
            {
                childDirection = -1;
            }
            magicBoomerangChild.launch(childDirection,0);
            magicBoomerangChild.attackDamage = stats.attacks.GetValueOrDefault("BoomerangMagic");
            yield return new WaitForSeconds(1);
            startNextState();
        }
        
        //round2
        
        private IEnumerator TripleHowl()
        {
            transform.position = new Vector3(transform.position.x, startPosY, transform.position.z);
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            animator.SetTrigger("HowlTriple");
            state = "tripleHowl";
            howlSoundTriple.PlayDelayed(0);
            nextState = "chargedBall";
            yield return new WaitForSeconds(1f);
            flipCheckActive = false;
            state = "tripleHowlInvul";
            howlEffectChild = Instantiate(howlEffect1, howlEffectSpawn.position, Quaternion.identity);
            stats.unverwundbar = true;
            stats.recievedAttacks = 0;
            yield return new WaitForSeconds(1f);
            if (state.Equals("tripleHowlInvul"))
            {
                Destroy(howlEffectChild);
                howlEffectChild = Instantiate(howlEffect2, howlEffectSpawn.position, Quaternion.identity);
            }
            yield return new WaitForSeconds(1f);
            if (state.Equals("tripleHowlInvul"))
            {
                Destroy(howlEffectChild);
                howlEffectChild = Instantiate(howlEffect3, howlEffectSpawn.position, Quaternion.identity);
                howlEffectChild.GetComponent<WerewolfHowl3Control>().attackDamage = stats.attacks.GetValueOrDefault("HowlAttack");
                camerascript.ShakeCamera(1f,1f, 60);
            }
            yield return new WaitForSeconds(1f);
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            stats.unverwundbar = false;
            if (state.Equals("tripleHowlInvul"))
            {
                Destroy(howlEffectChild);
                yield return new WaitForSeconds(0.5f);
                startNextState();
            }
        }
        
        private IEnumerator chargedBall()
        {
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            state = "chargedBall";
            nextState = "doublePounceAttack";
            animator.SetTrigger("ChargedSpell");
            chargedSpellAttack.PlayDelayed(0);
            yield return new WaitForSeconds(2.25f);
            flipCheckActive = false;
            ChargedSpellControl chargedSpellChild = Instantiate(chargedSpell, magicBallSpawn.position, Quaternion.identity).GetComponent<ChargedSpellControl>();
            float childDirection = 1;
            if (!stats.facingRight)
            {
                childDirection = -1;
            }
            chargedSpellChild.attackDamage = stats.attacks.GetValueOrDefault("ChargedSpell");
            chargedSpellChild.launch(childDirection,0);
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            yield return new WaitForSeconds(0.75f);
            startNextState();
        }
        
        private IEnumerator doublePounceAttack()
        {
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            rb.velocity = new Vector2(0, 0);
            state = "doublePounceAttack";
            FlipCheck(true);
            if (nextState.Equals("round3Combo"))
            {
                nextState = "round3ComboHowl";
            }
            else
            {
                nextState = "tripleHowl";
            }
            animator.SetTrigger("DoublePounce");
            yield return new WaitForSeconds(1.25f);
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            if (col.IsTouching(player.GetComponent<BoxCollider2D>()))
            {
                ContactPoint2D[] contacts = new ContactPoint2D[1];
                col.GetContacts(contacts);
                Vector2 contactPoint = contacts[0].point;
                doPounceDamage(contactPoint);
            }
            else
            {
                pounceActive = true;
                state = "pounceAttack";
            }
            float pounceDirection = 40;
            if (!stats.facingRight)
            {
                pounceDirection = -40;
            }
            rb.velocity = new Vector2(pounceDirection, 3);
            yield return new WaitForSeconds(0.75f);
            if (state.Equals("pounceAttack"))
            {
                rb.velocity = Vector2.zero;
                rb.constraints = RigidbodyConstraints2D.FreezeAll;
                pounceActive = false;
                state = "doublePounceAttack";
                FlipCheck(true);
                rb.velocity = new Vector2(0, 0);
            }
            yield return new WaitForSeconds(0.75f);
            if (state.Equals("doublePounceAttack"))
            {
                FlipCheck(true);
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                if (col.IsTouching(player.GetComponent<BoxCollider2D>()))
                {
                    ContactPoint2D[] contacts = new ContactPoint2D[1];
                    col.GetContacts(contacts);
                    Vector2 contactPoint = contacts[0].point;
                    doPounceDamage(contactPoint);
                }
                else
                {
                    pounceActive = true;
                    state = "pounceAttack";
                }
                pounceDirection = 60;
                if (!stats.facingRight)
                {
                    pounceDirection = -60;
                }
                rb.velocity = new Vector2(pounceDirection, 5);
            }
            yield return new WaitForSeconds(0.75f);
            if (state.Equals("pounceAttack"))
            {
                pounceActive = false;
                animator.SetTrigger("PounceFail");
                pounceFailSound.PlayDelayed(0);
                yield return new WaitForSeconds(1f);
                flipCheckActive = true;
                startNextState();
            }
        }
        
        //round3
        
        private IEnumerator doubleMagicBoomerang()
        {
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            state = "magicBoomerang";
            nextState = "crawl";
            animator.SetTrigger("DoubleSpellAttack");
            yield return new WaitForSeconds(1.5f);
            if (!stats.dead)
            {
                flipCheckActive = false;
                BoomerangMagicControl magicBoomerangChild = Instantiate(boomerangMagic, magicBallSpawn.position, Quaternion.identity).GetComponent<BoomerangMagicControl>();
                magicBoomerangChild.attackDamage = stats.attacks.GetValueOrDefault("BoomerangMagic");
                float childDirection = 1;
                if (!stats.facingRight)
                {
                    childDirection = -1;
                }
                magicBoomerangChild.launch(childDirection,0);
            }
            yield return new WaitForSeconds(0.5f);
            if (!stats.dead)
            {
                BoomerangMagicControl magicBoomerangChild = Instantiate(boomerangMagic, magicBallSpawn.position, Quaternion.identity).GetComponent<BoomerangMagicControl>();
                magicBoomerangChild.attackDamage = stats.attacks.GetValueOrDefault("BoomerangMagic");
                float childDirection = 1;
                if (!stats.facingRight)
                {
                    childDirection = -1;
                }
                magicBoomerangChild.launch(childDirection,0);
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            }
            yield return new WaitForSeconds(0.25f);
            startNextState();
        }
        
        private IEnumerator crawl()
        {
            transform.position = new Vector3(transform.position.x, startPosY, transform.position.z);
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            animator.SetTrigger("Crawl");
            crawlSound.PlayDelayed(0);
            stats.recievedAttacks = 0;
            state = "crawl";
            nextState = "round3Combo";
            yield return new WaitForSeconds(3f);
            if (state.Equals("crawl"))
            {
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                startNextState();
            }
        }
        
        //------------------------------------Basic Functions--------------------------------------
        public void startFight()
        {
            round = 1;
            StartCoroutine(howl());
            flipCheckActive = true;
        }

        private void FlipCheck(bool forced)
        {
            if ( (flipCheckActive && !flipOnCooldown) || forced)
            {
                if (stats.facingRight && transform.position.x > player.transform.position.x)
                {
                    Flip();
                }
                else if (!stats.facingRight && transform.position.x < player.transform.position.x)
                {
                    Flip();
                }
            }
        }

        public void Flip()
        {
            StartCoroutine(flipCooldownCounter());
            if (stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
                rb.velocity = new Vector2(rb.velocity.x * -1, rb.velocity.y);
            }
            else if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
                rb.velocity = new Vector2(rb.velocity.x * -1, rb.velocity.y);
            }
        }

        IEnumerator flipCooldownCounter()
        {
            flipOnCooldown = true;
            yield return new WaitForSeconds(0.25f);
            flipOnCooldown = false;
        }

        private void spawnBossPickup()
        {
            GameObject bossPickUpInstance = Instantiate(bossPickUp, magicBallSpawn.position, Quaternion.identity);
            Vector2 bossPickUpPush = new Vector2(500, 50);
            float rotation = 50;
            if (!stats.facingRight)
            {
                bossPickUpPush.x *= -1;
                rotation *= -1;
            }
            bossPickUpInstance.GetComponent<BossPickup>().spawn(bossPickUpPush, rotation);
        }

        private void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead && round == 3 && !playerChar.dead)
            {
                round = 4;
                StartCoroutine(Death());
            }
        }
        
        IEnumerator Death()
        {
            fadeScreen.GetComponent<FadeScreen>().startFadeScreen();
            stats.dead = true;
            state = "dead";
            nextState = "dead";
            if (howlEffectChild != null){
                
                Destroy(howlEffectChild);
            }
            StartCoroutine(nosEnd2Control.startEndDialogControl());
            yield return new WaitForSeconds(1f);
            deathSound.PlayDelayed(0);
            animator.SetTrigger("DeathTrigger");
            animator.SetBool("Death", true);
        }
    }
}
