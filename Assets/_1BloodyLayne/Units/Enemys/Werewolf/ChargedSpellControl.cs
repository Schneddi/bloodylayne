using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class ChargedSpellControl : MonoBehaviour
    {
        GameObject player;
        private PlatformerCharacter2D larry;
        public GameObject deathSound;
        Rigidbody2D rbLarry;
        Rigidbody2D rb;
        public float speed = 6f, chaseSpeed, radius;
        public int attackDamage = 150, range, lifetime;
        private Vector2 direction;
        private bool chaseMode;
        private BoxCollider2D boxCollider;
        private Vector2 startPos;

        // Use this for initialization
        void Start()
        {
            player = GameObject.FindWithTag("Player");
            StartCoroutine(DieWithDelay(lifetime));
        }

        private void FixedUpdate()
        {
            if (chaseMode)
            {
                direction = (player.transform.position - transform.position).normalized;
                rb.velocity = direction * chaseSpeed;
                setRotation(direction.x, direction.y);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (larry.dead)
            {
                Death();
            }
            if (!chaseMode && Vector2.Distance(transform.position, startPos) > range)
            {
                chaseMode = true;
            }
            if (Physics2D.OverlapCircle(transform.position, radius, 1 << 8))
            {
                if (!larry.invulnerable)
                {
                    Death();
                    larry.Hit(attackDamage, DamageTypeToPlayer.MagicProjectile, transform.position, gameObject, new Guid());
                }
            }
        }
        
        public void launch(float x, float y)
        {
            player = GameObject.FindWithTag("Player");
            larry = player.GetComponent<PlatformerCharacter2D>();
            rb = GetComponent<Rigidbody2D>();
            direction = new Vector2(x, y).normalized;
            rb.velocity = new Vector2(direction.x * speed, direction.y * speed);
            startPos = transform.position;
            setRotation(x, y);
        }

        private void setRotation(float x, float y)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.back, new Vector3(x, y, 0));
            transform.Rotate(new Vector3(0,0,180));
        }


        IEnumerator DieWithDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            Death();
        }

        private void  Death()
        {
            GameObject deathKind = Instantiate(deathSound, transform.position, Quaternion.identity);
            deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
            Destroy(gameObject);
        }

    }
}
