using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class WerewolfHowl3Control : MonoBehaviour
{
    private bool damagerActive, damageDealt;
    private PlatformerCharacter2D playerChar;
    
    public float radius, startDelay;
    public int attackDamage;

    private void Start()
    {
        playerChar = GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>();
        StartCoroutine(initialDelay());
    }

    void Update()
    {
        if (Physics2D.OverlapCircle(transform.position, radius, 1 << 8) && !damageDealt && damagerActive)
        {
            damageDealt = true;
            playerChar.Hit(attackDamage, DamageTypeToPlayer.SonicWave, playerChar.transform.position, gameObject, new Guid());
        }
    }

    IEnumerator initialDelay()
    {
        yield return new WaitForSeconds(startDelay);
        damagerActive = true;
    }
}
