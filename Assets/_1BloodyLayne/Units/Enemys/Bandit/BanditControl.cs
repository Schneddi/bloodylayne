using System;
using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class BanditControl : MonoBehaviour
{
    public Dialogbox startDialog;
    public float pushForce, speed;
    
    private EnemyStats stats;
    private Transform frontCheck, rightGuardPosition;
    private bool attacking;
    private PlatformerCharacter2D playerChar;
    private AudioSource hitSound, swingSound;
    private Animator animator;
    
    
    // Start is called before the first frame update
    void Start()
    {
        stats = GetComponent<EnemyStats>();
        animator = GetComponent<Animator>();
        playerChar = GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>();
        hitSound = transform.Find("Sounds").GetComponents<AudioSource>()[0];
        swingSound = transform.Find("Sounds").GetComponents<AudioSource>()[2];
        frontCheck = transform.Find("Sensors").Find("FrontAttackCheck");
        rightGuardPosition = transform.Find("Sensors").Find("RightGuardingPosition");
        rightGuardPosition.SetParent(null);
        if (stats.facingRight)
        {
            speed *= -1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        checkDeath();
        if (startDialog.finished && !stats.dead && !attacking)
        {
            if (Physics2D.OverlapCircle(frontCheck.position, 0.5f, 1 << 8))
            {
                StartCoroutine(Attack());
            }
        }
    }

    private IEnumerator Attack()
    {
        swingSound.PlayDelayed(0);
        attacking = true;
        animator.SetTrigger("Attack");
        yield return new WaitForSeconds(0.5f);
        if (!stats.dead)
        {
            if (Physics2D.OverlapCircle(frontCheck.position, 0.5f, 1 << 8))
            {
                if (playerChar.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeStab"), DamageTypeToPlayer.MeleeStab, new Vector2(pushForce, 0), frontCheck.position, gameObject, Guid.NewGuid()))
                {
                    hitSound.PlayDelayed(0);
                }
            }
            yield return new WaitForSeconds(0.5f);
        }
        if (!stats.dead)
        {
            attacking = false;
            animator.SetTrigger("Idle");
        }
    }

    /*
    private void FlipCheck()
    {
        if (!flipOnCooldown)
        {
            if (stats.facingRight && transform.position.x > playerChar.transform.position.x)
            {
                StartCoroutine(flipCheckCooldown());
                Flip();
            }
            else if (!stats.facingRight && transform.position.x < playerChar.transform.position.x)
            {
                StartCoroutine(flipCheckCooldown());
                Flip();
            }
        }
    }
    */

    private void Flip()
    {
        if (stats.facingRight)
        {
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
            stats.facingRight = !stats.facingRight;
            speed *= -1;
        }
        else if (!stats.facingRight)
        {
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
            stats.facingRight = !stats.facingRight;
            speed *= -1;
        }
    }

    public void MoveToRightGuardPosition()
    {
        Flip();
        transform.position = rightGuardPosition.position;
    }
    
    private void checkDeath()
    {
        if (stats.health <= 0 && !stats.dead)
        {
            Death();
        }
    }

    private void Death()
    {
        stats.dead = true;
        attacking = false;
        animator.SetTrigger("DeathTrigger");
        animator.SetBool("Death", true);
        GetComponent<HumanDeathSequence>().Death();
    }

}
