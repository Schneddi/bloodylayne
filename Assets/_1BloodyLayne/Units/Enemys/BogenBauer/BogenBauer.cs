﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Enemys;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class BogenBauer : MonoBehaviour
    {
        [SerializeField] private LayerMask isPlayer;                  // A mask determining what is the Player
        public float speed;
        public GameObject arrow; //Geschosse
        
        private EnemyStats stats;
        private Transform detector, pfeilSpawn, player, ceilingCheck, cliffSensor;    // A position marking where to check if the player is grounded.
        private AudioSource arrowSound, topAttackSound, hitSound, runSound;
        private Animator animator;
        private Rigidbody2D rb;
        private bool activeShooting=false; //Bogenbauern schießen nicht, um Berechnungen zu sparen. Sie werden erst aktiv wenn der Spieler sich nähert
        private bool ontop, running, aggro, playerNear;
        private PlatformerCharacter2D larry;
        private float dragStore, idleDrag = 30;
        private GlobalVariables gv;

        // Use this for initialization
        void Start()
        {
            stats = GetComponent<EnemyStats>();
            detector = transform.Find("Eyes");
            pfeilSpawn = transform.Find("ArrowSpawn");
            ceilingCheck = transform.Find("CeilingCheck");
            cliffSensor = transform.Find("CliffSensor");
            player = GameObject.FindWithTag("Player").transform;
            //rb = GetComponent<Rigidbody2D>();
            arrowSound = GetComponents<AudioSource>()[0];
            topAttackSound = GetComponents<AudioSource>()[1];
            hitSound = GetComponents<AudioSource>()[2];
            runSound = GetComponents<AudioSource>()[3];
            animator = GetComponent<Animator>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            speed += gv.difficulty * 10;
            rb = GetComponent<Rigidbody2D>();
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                speed *= -1;
            }
            stats.isHuman = true;
            larry = GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>();
            dragStore = rb.drag;
            rb.drag = idleDrag;
        }

        private void Update()
        {
            checkPlayerNear();
            if (!animator.GetBool("Death") && !ontop && !activeShooting && playerNear)
            {
                if (Physics2D.OverlapCircle(detector.position, 25f,
                    isPlayer))
                {
                    if ((detector.position.x > player.position.x) && stats.facingRight)
                    {
                        Flip();
                    }
                    else if ((detector.position.x < player.position.x) && !stats.facingRight)
                    {
                        Flip();
                    }
                }
                if (Physics2D.OverlapCircle(ceilingCheck.position, 0.6f, 1 << 8))
                {
                    StartCoroutine(TopAttack());
                }
                else if (Physics2D.OverlapCircle(detector.position, 25f, isPlayer) && (transform.position.y - 3f < player.position.y && player.position.y < transform.position.y + 5f))//ist der Spieler in der Nähe? Dann fange an zu schießen!
                {
                    StartCoroutine(Shooting());
                }
            }
        }

        private void FixedUpdate()
        {
            checkDeath();
            if (!animator.GetBool("Death") && !ontop && !activeShooting && playerNear)
            {
                if (running)
                {
                    
                    if(!Physics2D.OverlapCircle(detector.position, 40f, isPlayer) || !Physics2D.OverlapCircle(cliffSensor.position, 0.1f, 1<<11))
                    {
                        animator.SetTrigger("Idle");
                        running = false;
                        runSound.Stop();
                        aggro = false;
                        rb.velocity = new Vector2(0,rb.velocity.y);
                    }
                    else
                    {
                        rb.AddForce(new Vector2(speed,0));
                    }
                }
                else if (!Physics2D.OverlapCircle(detector.position, 25f, isPlayer) &&Physics2D.OverlapCircle(detector.position, 40f, isPlayer) && Physics2D.OverlapCircle(cliffSensor.position, 0.1f, 1<<11) && aggro)
                {
                    animator.SetTrigger("Walk");
                    running = true;
                    runSound.PlayDelayed(0);
                }
            }
        }
        void Flip()//z.B. in andere Richtung gucken
        {
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
            stats.facingRight = !stats.facingRight;
            speed *= -1;
        }
        
        private void checkPlayerNear()
        {
            if (Physics2D.OverlapCircle(transform.position, 30f, 1<<8))
            {
                playerNear = true;
                rb.drag = dragStore;
            }
            else
            {
                rb.drag = idleDrag;
                playerNear = false;
            }
        }
        
        void Fire()//erzeugt einen Pfeil am pfeilSpawn
        {
            GameObject arrowInstance = Instantiate(arrow, pfeilSpawn.position, Quaternion.identity);
            arrowInstance.GetComponent<PfeilControl>().attackDamage = stats.attacks.GetValueOrDefault("RangedAttack");
            arrowInstance.GetComponent<PfeilControl>().speed *= 1 + gv.difficulty * 0.1f;
            if (!stats.facingRight)
            {
                arrowInstance.GetComponent<PfeilControl>().speed.x *= -1;
                arrowInstance.transform.localScale = new Vector3(arrowInstance.transform.localScale.x * -1, arrowInstance.transform.localScale.y, arrowInstance.transform.localScale.z);
            }
        }
        IEnumerator Shooting()//Gibt Schussfrequenz und Animationen an
        {
            runSound.Stop();
            aggro = true;
            activeShooting = true;
            if (gv.difficulty < 3)
            {
                yield return new WaitForSeconds(0.1f);
            }

            if (!animator.GetBool("Death"))
            {
                animator.SetTrigger("Attack");
            }
            yield return new WaitForSeconds(0.45f);
            if (!animator.GetBool("Death"))
            {
                Fire();
                arrowSound.PlayDelayed(0);
                animator.SetTrigger("Reload1");
            }

            float reloadTime = 0.9f;
            reloadTime -= gv.difficulty * 0.1f;
            yield return new WaitForSeconds(reloadTime);
            if (!animator.GetBool("Death"))
            {
                animator.SetTrigger("Reload2");
            }
            yield return new WaitForSeconds(0.5f);
            if (!animator.GetBool("Death"))
            {
                animator.SetTrigger("Idle");
                activeShooting = false;
            }
        }
        
        private IEnumerator TopAttack()
        {
            ontop = true;
            rb.velocity = new Vector2(0, rb.velocity.y);
            animator.SetTrigger("TopAttack");
            topAttackSound.PlayDelayed(0);
            yield return new WaitForSeconds(0.35f);
            if (animator.GetBool("Death"))
            {
                yield break;
            }
            if (Physics2D.OverlapCircle(ceilingCheck.position, 0.6f, 1<<8))
            {
                if (larry.Hit(stats.attacks.GetValueOrDefault("TopAttack"), DamageTypeToPlayer.MeleeFeetStab, new Vector2(0, 15f), ceilingCheck.position, gameObject, Guid.NewGuid()))
                {
                    hitSound.PlayDelayed(0);
                }
            }
            yield return new WaitForSeconds(0.75f);
            if (!animator.GetBool("Death"))
            {
                animator.SetTrigger("Idle");
                ontop = false;
            }
        }
        
        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }
        private void Death()
        {
            stats.dead = true;
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            GetComponent<HumanDeathSequence>().Death();
        }
    }
}
