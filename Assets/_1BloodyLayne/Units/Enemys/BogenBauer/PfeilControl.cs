﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;
using Random = UnityEngine.Random;

namespace UnityStandardAssets._2D
{
    public class PfeilControl : MonoBehaviour
    {
        public Vector2 speed;
        public GameObject hitEffect;
        public int attackDamage=50;
        
        private Rigidbody2D rb;
        private BoxCollider2D boxCollider2D;
        private bool collisionOn, hit;
        private GameObject Player;
        private PlatformerCharacter2D player;
        private AudioSource hitSound;
        private Transform tip, arrowFlames;

        // Use this for initialization
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            speed.y += Random.Range(-2.5f, 1.5f);
            rb.velocity = speed;
            Player = GameObject.FindWithTag("Player");
            player = Player.GetComponent<PlatformerCharacter2D>();
            hitSound = GetComponent<AudioSource>();
            boxCollider2D = GetComponent<BoxCollider2D>();
            tip = transform.Find("ArrowTip");
            arrowFlames = transform.Find("ArrowFlames");
            StartCoroutine(Shoot());
        }

        // Update is called once per frame
        void Update()
        {
            if (hit) return;
            
            transform.rotation = Quaternion.LookRotation(Vector3.back, new Vector3(rb.velocity.x, rb.velocity.y, 0));
            if (rb.velocity.x > 0)
            {
                transform.Rotate(new Vector3(0,0,90));
            }
            else
            {
                transform.Rotate(new Vector3(0,0,-90));
            }
            if (Physics2D.OverlapCircle(tip.position, 0.15f, 1 << 8) && collisionOn && !hit)//layer Ebene 8 = Player
            {
                PlayerHit();
            }
        }

        private void FixedUpdate()
        {
            if (hit) return;
            
            rb.velocity = new Vector2(speed.x, rb.velocity.y);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!collisionOn || hit) return;
            
            if (other.gameObject.CompareTag("Player"))
            {
                PlayerHit();
            }
            if (other.gameObject.CompareTag("Enemy"))
            {
                StartCoroutine(Death(false));
            }
            if (other.gameObject.CompareTag("Ground"))
            {
                StartCoroutine(Death(false));
            }
            if (other.gameObject.CompareTag("Object"))
            {
                if (other.gameObject.GetComponent<ObjektStats>().destructable)
                {
                    other.gameObject.GetComponent<ObjektStats>().hitPoints -= attackDamage;
                    if (other.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                    {
                        other.gameObject.GetComponent<ObjektStats>().startDeath();
                    }
                }
                StartCoroutine(Death(false));
            }
        }
        IEnumerator Shoot()
        {
            yield return new WaitForSeconds(0.1f);
            if (Physics2D.OverlapCircle(transform.position, 0.4f, 1 << 8))//layer Ebene 8 = Player
            {
                PlayerHit();
            }
            else
            {
                collisionOn = true;
                boxCollider2D.enabled = true;
            }
        }
        private void PlayerHit()
        {
            if (player.Hit(attackDamage, DamageTypeToPlayer.Arrow, new Vector2(speed.x, 0), transform.position, gameObject, Guid.NewGuid()))
            {
                hitSound.PlayDelayed(0);
                Instantiate(hitEffect, tip.position, Quaternion.identity);
            }
            StartCoroutine(Death(true));
        }

        IEnumerator Death(bool instantDeath)
        {
            hit = true;
            GetComponent<BoxCollider2D>().enabled = false;
            arrowFlames.GetComponent<ParticleSystem>().Stop();
            if (instantDeath)
            {
                GetComponent<SpriteRenderer>().enabled = false;
            }
            rb.rotation = 0;
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            yield return new WaitForSeconds(1.25f);
            Destroy(gameObject);
        }
    }
}