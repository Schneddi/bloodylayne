﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using Units.Enemys;
using Units.Player;
using UnityEngine;
using UnityEngine.Serialization;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;
using Random = UnityEngine.Random;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Jaeger : MonoBehaviour
    {
        public Transform endeLinks, endeRechts, baer, fass;
        public GameObject dialogIntro, dialogEnd1, dialogEnd2, dialogEnd3, dialogComeBack1, dialogComeBack2, dialogComeBack3, dialogEndTears, bossPickUp, round2Dialog, cross;
        public GameObject deathSound;
        public float speed = 0.9f;
        
        private Animator animator;
        private AudioSource[] sounds, crossThrowLinesEng, crossThrowLinesGer;
        private AudioSource runSound, attackSound, jumpSound, hitSound, introSpeakSound1eng, introSpeakSound1de, introSpeakSound2eng, introSpeakSound2de, idleSpeakSound1eng, idleSpeakSound1de, idleSpeakSound2eng, idleSpeakSound2de, idleSpeakSound3eng, idleSpeakSound3de, deathSpeakSound1eng, deathSpeakSound1de, deathSpeakSound2eng, deathSpeakSound2de, deathSpeakSound3eng, deathSpeakSound3de, deathSpeakSound4eng, deathSpeakSound4de, deathSpeakSound5eng, deathSpeakSound5de, comeBackSound1eng, comeBackSound1de, comeBackSound2eng, comeBackSound2de, comeBackSound3eng, comeBackSound3de, crossThrowSound;
        private Rigidbody2D rb;
        private EnemyStats stats;
        private bool dead, idle, run, jump, attack, point, grounded, backjump, baerDialog1, baerDialog2;
        private Transform player, hit1, hit2, groundCheck, crossSpawn, crossHold;
        private Dialog dialog;
        private PlatformerCharacter2D playerChar;
        private GameController gc;
        private GlobalVariables gv;
        private int round;
        private List<GameObject> crossGrp;

        // Use this for initialization
        void Start()
        {
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
            player = GameObject.FindWithTag("Player").transform;
            playerChar = player.GetComponent<PlatformerCharacter2D>();
            dialog = FindObjectOfType<Dialog>();
            hit1 = transform.Find("Hit1");
            hit2 = transform.Find("Hit2");
            groundCheck = transform.Find("GroundCheckHunter");
            crossSpawn = transform.Find("CrossSpawn");
            crossHold = transform.Find("CrossHold");
            stats = GetComponent<EnemyStats>();
            stats.isHuman = true;
            rb = GetComponent<Rigidbody2D>();
            sounds = GetComponents<AudioSource>();
            crossGrp = new List<GameObject>();
            runSound = sounds[0];
            attackSound = sounds[1];
            jumpSound = sounds[2];
            introSpeakSound1eng = sounds[3];
            introSpeakSound2eng = sounds[4];
            idleSpeakSound1eng = sounds[5];
            idleSpeakSound2eng = sounds[6];
            idleSpeakSound3eng = sounds[7];
            deathSpeakSound1eng = sounds[8];
            deathSpeakSound2eng = sounds[9];
            deathSpeakSound3eng = sounds[10];
            deathSpeakSound4eng = sounds[11];
            deathSpeakSound5eng = sounds[12];
            comeBackSound1eng = sounds[13];
            comeBackSound2eng = sounds[14];
            comeBackSound3eng = sounds[15];
            introSpeakSound1de = sounds[16];
            introSpeakSound2de = sounds[17];
            idleSpeakSound1de = sounds[18];
            idleSpeakSound2de = sounds[19];
            idleSpeakSound3de = sounds[20];
            deathSpeakSound1de = sounds[21];
            deathSpeakSound2de = sounds[22];
            deathSpeakSound3de = sounds[23];
            deathSpeakSound4de = sounds[24];
            deathSpeakSound5de = sounds[25];
            comeBackSound1de = sounds[26];
            comeBackSound2de = sounds[27];
            comeBackSound3de = sounds[28];
            hitSound = sounds[29];
            crossThrowLinesEng = new AudioSource[3];
            crossThrowLinesEng[0] = sounds[30];
            crossThrowLinesEng[1] = sounds[31];
            crossThrowLinesEng[2] = sounds[32];
            crossThrowLinesGer = new AudioSource[3];
            crossThrowLinesGer[0] = sounds[33];
            crossThrowLinesGer[1] = sounds[34];
            crossThrowLinesGer[2] = sounds[35];
            crossThrowSound = sounds[36];
            speed += gv.difficulty * 0.1f;
            animator = GetComponent<Animator>();
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                speed *= -1;
                transform.localScale = theScale;
            }
            StartCoroutine(StartDialog());
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (!dead)
            {
                grounded = false;
                if (Physics2D.OverlapCircle(groundCheck.position, 0.25f, 1 << 11))
                {
                    grounded = true;
                }
                if (attack)
                {
                    if (Physics2D.OverlapCircle(hit1.position, 0.5f, 1 << 8))
                    {
                        if (playerChar.Hit(stats.attacks.GetValueOrDefault("MeleeSwing"), DamageTypeToPlayer.MeleeSwing, new Vector2(speed + 45, 0), hit1.position, gameObject, Guid.NewGuid()))
                        {
                            hitSound.PlayDelayed(0);
                        }
                    }
                    else if (Physics2D.OverlapCircle(hit2.position, 0.5f, 1 << 8))
                    {
                        if (playerChar.Hit(stats.attacks.GetValueOrDefault("MeleeSwing"), DamageTypeToPlayer.MeleeSwing, new Vector2(speed + 45, 0), hit1.position, gameObject, Guid.NewGuid()))
                        {
                            hitSound.PlayDelayed(0);
                        }
                    }
                }
            }

        }
        void Update()
        {
            checkDeath();
            float meleeAttackRange = 2.5f;
            if (round == 2)
            {
                meleeAttackRange = 1;
            }
            if (!dead)
            {
                if (idle && !player.GetComponent<Animator>().GetBool("Death"))
                {
                    if (player.position.x > endeLinks.position.x && player.position.x < endeRechts.position.x && player.position.y < endeRechts.position.y)
                    {
                        runSound.PlayDelayed(0);
                        animator.SetTrigger("Run");
                        idle = false;
                        run = true;
                        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                    }
                }
                if (run)
                {
                    if (player.GetComponent<Animator>().GetBool("Death"))
                    {
                        idle = true;
                        run = false;
                        runSound.Stop();
                        animator.SetTrigger("Idle");
                    }
                    if (player.position.x < endeLinks.position.x || player.position.x > endeRechts.position.x)
                    {
                        run = false;
                        runSound.Stop();
                        animator.SetTrigger("Idle");
                        idle = true;
                    }
                    else if (Physics2D.OverlapCircle(hit1.transform.position, 0.2f, 1 << 11)) 
                    {
                        run = false;
                        backjump = true;
                        jump =true;
                        runSound.Stop();
                        jumpSound.PlayDelayed(0);
                        rb.velocity = new Vector2(rb.velocity.x, 70);
                        animator.SetTrigger("Jump");
                    }
                    else if (stats.facingRight && player.position.x < transform.position.x)
                    {
                        run = false;
                        runSound.Stop();
                        FlipCheck();
                        StartCoroutine(Taunt());
                    }
                    else if (!stats.facingRight && player.position.x > transform.position.x)
                    {
                        run = false;
                        runSound.Stop();
                        FlipCheck();
                        StartCoroutine(Taunt());
                    }
                    else if (!grounded)
                    {
                        runSound.Stop();
                        run = false;
                        jump = true;
                        animator.SetTrigger("Jump");
                    }
                    else if (player.position.y > transform.position.y && (Mathf.Abs(player.position.x - transform.position.x) < 5f || Mathf.Abs(player.position.x - transform.position.x) > 10f))
                    {
                        run = false;
                        backjump = true;
                        jump =true;
                        runSound.Stop();
                        jumpSound.PlayDelayed(0);
                        rb.velocity = new Vector2(rb.velocity.x, 70);
                        animator.SetTrigger("Jump");
                    }
                    else if(Mathf.Abs(player.position.x - transform.position.x) < 2.5f)
                    {
                        attackSound.PlayDelayed(0);
                        run = false;
                        runSound.Stop();
                        StartCoroutine(Attack());
                    }
                    else if(Mathf.Abs(player.position.x - transform.position.x) < 6f && round == 2)
                    {
                        run = false;
                        runSound.Stop();
                        StartCoroutine(Taunt());
                    }
                    else if (Mathf.Abs(player.position.x - transform.position.x) > 10f)
                    {
                        rb.velocity = new Vector2(Vector2.Distance(transform.position, player.position) * 5 * speed, rb.velocity.y);
                    }
                    else
                    {
                        rb.velocity = new Vector2(25f * speed, rb.velocity.y);
                    }
                }
                if (jump)
                {
                    if (player.position.x < endeLinks.position.x || player.position.x > endeRechts.position.x)
                    {
                        jump = false;
                        animator.SetTrigger("Idle");
                        idle = true;
                    }
                    else if (player.position.y + 0.1f < transform.position.y && (Mathf.Abs(player.position.x - transform.position.x) < 5f) && backjump && rb.velocity.y > 0)
                    {
                        backjump = false;
                        rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y/4);
                    }
                    else if (Mathf.Abs(player.position.x - transform.position.x) < meleeAttackRange)
                    {
                        jump = false;
                        attackSound.PlayDelayed(0);
                        StartCoroutine(Attack());
                    }
                    else if (stats.facingRight && player.position.x < transform.position.x)
                    {
                        jump = false;
                        FlipCheck();
                        StartCoroutine(Taunt());
                    }
                    else if (!stats.facingRight && player.position.x > transform.position.x)
                    {
                        jump = false;
                        FlipCheck();
                        StartCoroutine(Taunt());
                    }
                    else if (grounded)
                    {
                        animator.SetTrigger("Run");
                        runSound.PlayDelayed(0);
                        run = true;
                        jump = false;
                    }
                    else if(!Physics2D.OverlapCircle(hit1.position, 0.5f, 1 << 11) && !Physics2D.OverlapCircle(hit2.position, 0.5f, 1 << 11))
                    {
                        rb.velocity = new Vector2(22f * speed, rb.velocity.y);
                    }
                }
                if (attack)
                {
                    if (!Physics2D.OverlapCircle(hit1.position, 0.5f, 1 << 11) && !Physics2D.OverlapCircle(hit2.position, 0.5f, 1 << 11))
                    {
                        if (stats.facingRight && player.position.x > transform.position.x)
                        {
                            rb.velocity = new Vector2(25f * speed, rb.velocity.y);
                        }
                        else if (!stats.facingRight && player.position.x < transform.position.x)
                        {
                            rb.velocity = new Vector2(25f * speed, rb.velocity.y);
                        }
                    }
                    if (player.position.y > transform.position.y && (((Mathf.Abs(player.position.x - transform.position.x) < 5f) && (Mathf.Abs(player.position.x - transform.position.x) > 1f)) || Mathf.Abs(player.position.x - transform.position.x) > 10f) && grounded)
                    {
                        jumpSound.PlayDelayed(0);
                        rb.velocity = new Vector2(rb.velocity.x, 70);
                    }
                }
            }
            if (baerDialog1)
            {
                if (dialog.dialogActive == false)
                {
                    baerDialog1 = false;
                    StartCoroutine(DeathDialog2());
                }
            }
            if (baerDialog2)
            {
                if (dialog.dialogActive == false)
                {
                    baerDialog2 = false;
                    StartCoroutine(Death());
                }
            }
        }


        void checkDeath()
        {
            if (stats.health <= 0 && !dead && !playerChar.dead)
            {
                dead = true;
                playerChar.GetComponent<Rigidbody2D>().velocity = new Vector2(0,playerChar.GetComponent<Rigidbody2D>().velocity.y);
                if (round == 1)
                {
                    StartCoroutine(Round2Dialog());
                }
                else
                {
                    DeathDialog1();
                }
            }
            else if (stats.health <= 0 && !dead && playerChar.dead)
            {
                stats.health = 10;
            }
        }


        void FlipCheck()//z.B. in andere Richtung gucken
        {
            if (stats.facingRight && transform.position.x > player.position.x)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
            else if (!stats.facingRight && transform.position.x < player.position.x)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
        }

        IEnumerator Taunt()
        {
            rb.velocity = Vector2.zero;
            int rnd = Random.Range(0, 3);
            if (round == 1)
            {
                animator.SetTrigger("Point");
                if (gv.Language == 0)
                {
                    if (rnd == 0)
                    {
                        idleSpeakSound1eng.PlayDelayed(0);
                    }

                    if (rnd == 1)
                    {
                        idleSpeakSound2eng.PlayDelayed(0);
                    }

                    if (rnd == 2)
                    {
                        idleSpeakSound3eng.PlayDelayed(0);
                    }
                }
                else
                {
                    if (rnd == 0)
                    {
                        idleSpeakSound1de.PlayDelayed(0);
                    }

                    if (rnd == 1)
                    {
                        idleSpeakSound2de.PlayDelayed(0);
                    }

                    if (rnd == 2)
                    {
                        idleSpeakSound3de.PlayDelayed(0);
                    }
                }
                yield return new WaitForSeconds(1.5f);
            }

            if (round == 2 && !dead)
            {
                FlipCheck();
                rb.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
                animator.SetTrigger("HandsUp");
                yield return new WaitForSeconds(0.5f);
                animator.SetTrigger("HandsUpSpeak");
                if (gv.Language == 0)
                {
                    crossThrowLinesEng[rnd].PlayDelayed(0);
                }
                else
                {
                    crossThrowLinesGer[rnd].PlayDelayed(0);
                }
                GameObject crossInstance = Instantiate(cross, crossSpawn.position, Quaternion.identity);
                crossGrp.Add(crossInstance);
                crossInstance.GetComponent<Rigidbody2D>().AddForce(new Vector2(0,-300));
                while (!dead && crossInstance.transform.position.y > crossHold.position.y)
                {
                    yield return null;
                }

                if (!dead)
                {
                    Vector2 throwForce = new Vector2(player.position.x - transform.position.x,
                        5 + player.position.y - transform.position.y);
                    crossInstance.GetComponent<CrossControl>().throwCross(throwForce * 85);
                    crossInstance.GetComponent<CrossControl>().attackDamage = stats.attacks.GetValueOrDefault("CrossThrow");
                    animator.SetTrigger("ThrowObject");
                    crossThrowSound.PlayDelayed(0);
                    yield return new WaitForSeconds(0.5f);
                }
            }

            if (!dead)
            {
                rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                FlipCheck();
                runSound.PlayDelayed(0);
                animator.SetTrigger("Run");
                run = true;
            }
        }
        IEnumerator Attack()
        {
            animator.SetTrigger("Attack");
            yield return new WaitForSeconds(0.2f);
            attack = true;
            yield return new WaitForSeconds(0.8f);
            if (!dead)
            {
                attack = false;
                FlipCheck();
                StartCoroutine(Taunt());
            }
        }

        IEnumerator StartDialog()
        {
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            yield return new WaitForSeconds(1f);
            dialog.ZeigeBox(dialogIntro);
            while (dialog.dialogActive)
            {
                yield return null;
            }
            player.GetComponent<Platformer2DUserControl>().BlockInput = false;
            playerChar.InitializeSkills(true);
            idle = true;
            round = 1;
            GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().jaegerEncountered = true;
        }
        
        IEnumerator Round2Dialog()
        {
            runSound.Stop();
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            yield return new WaitForSeconds(1f);
            stats.health = stats.maxHealth;
            stats.noBloodied = false;
            dialog.ZeigeBox(round2Dialog);
            while (dialog.dialogActive)
            {
                yield return null;
            }
            spawnBossPickup();
            player.GetComponent<Platformer2DUserControl>().BlockInput = false;
            playerChar.InitializeSkills(true);
            idle = true;
            round = 2;
            dead = false;
            GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().jaegerEncountered = true;
        }

        private void DeathDialog1()
        {
            animator.SetTrigger("Idle");
            runSound.Stop();
            dialog.ZeigeBox(dialogEnd1);
            baerDialog1 = true;
        }

        IEnumerator DeathDialog2()
        {
            foreach (GameObject crossInstance in crossGrp)
            {
                if (crossInstance != null)
                {
                    crossInstance.GetComponent<CrossControl>().death();
                }
            }
            baer.GetComponent<Animator>().SetTrigger("Idle");
            dialog.ZeigeBox(dialogEnd2);
            baerDialog2 = true;
            yield return new WaitForSeconds(0.2f);
        }

        IEnumerator Death()
        {
            GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().jaegerKilled = true;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = fass;
            baer.GetComponent<Baer>().walkAway();
            yield return new WaitForSeconds(5f);
            dialog.ZeigeBox(dialogEnd3);
            while (dialog.dialogActive)
            {
                yield return null;
            }
            animator.SetTrigger("Idle");
            yield return new WaitForSeconds(1f);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().SetDamping(0.2f);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = gameObject.transform;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponents<AudioSource>()[0].Stop();
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            rb.freezeRotation = false;
            rb.constraints = rb.constraints & ~RigidbodyConstraints2D.FreezePositionX;
            rb.velocity = new Vector2(-5,20);
            rb.AddTorque(500);
            GameObject.Find("BossUI").GetComponent<BossUI>().deactivateBossUI();
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = GameObject.FindGameObjectWithTag("Player").transform;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().SetDamping(0.2f);
            player.GetComponent<Platformer2DUserControl>().BlockInput = false;
            StartCoroutine(CameraLock());
            GameObject deathKind = Instantiate(deathSound, transform.position, Quaternion.identity);
            deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
            yield return new WaitForSeconds(2.5f);
            stats.dead = true;
            gc.SetAchievement("HUNTER_1");
            GetComponent<HumanDeathSequence>().Death();
        }
        
        private void spawnBossPickup()
        {
            GameObject bossPickUpInstance = Instantiate(bossPickUp, hit1.position, Quaternion.identity);
            Vector2 bossPickUpPush = new Vector2(50, 100);
            float rotation = 50;
            if (!stats.facingRight)
            {
                bossPickUpPush.x *= -1;
                rotation *= -1;
            }
            bossPickUpInstance.GetComponent<BossPickup>().spawn(bossPickUpPush, rotation);
        }

        IEnumerator CameraLock()
        {
            yield return new WaitForSeconds(0.5f);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().ResetDamping();
            StartCoroutine(dialog.zoom(dialog.cameraOriginalSize, 1));
        }

        public void yellBack1()
        {
            dialog.ZeigeBox(dialogComeBack1);
        }
        public void yellBack2()
        {
            dialog.ZeigeBox(dialogComeBack2);
        }
        public void yellBack3()
        {
            dialog.ZeigeBox(dialogComeBack3);
        }
        public void tearsOfSadness()
        {
            gc.SetAchievement("HUNTER_2");
            dialog.ZeigeBox(dialogEndTears);
        }
    }
}
