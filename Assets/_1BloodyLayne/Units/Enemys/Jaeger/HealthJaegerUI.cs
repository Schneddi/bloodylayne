﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityEngine.UI;


namespace UnityStandardAssets._2D
{
    public class HealthJaegerUI : MonoBehaviour
    {

        public Image heart2;
        public Sprite[] heartSprite;
        DamageUIScript damageScript;
        EnemyStats stats;
        Text jagHealthText, jagMaxHealthText;
        bool aktiv;
        public Image lebenInnenImageNos;
        Color fadeColor;
        bool fadingIn, fadingOut;

        // Use this for initialization
        void Start()
        {
            stats = GameObject.Find("Jaeger").transform.GetComponent<EnemyStats>();
            jagHealthText = GameObject.Find("jagHealth").GetComponent<Text>();
            jagMaxHealthText = GameObject.Find("jagMaxHealth").GetComponent<Text>();
            //health text was for showing enemy HP, but has been disabled for immersion
            jagHealthText.text = null;
            jagMaxHealthText.text = null;
        }

        // Update is called once per frame
        void Update()
        {
            if (aktiv == true)
            {
                ChangingUIHealth();
            }
            if (fadingIn)
            {
                fadeColor = new Color(0f, 0, 0, 1f);
                Image[] images = GetComponentsInChildren<Image>();
                foreach (Image image in images)
                {
                    fadeColor = new Color(0f, 0, 0, 1f);
                    fadeColor += image.color;
                    image.color = Color.Lerp(image.color, fadeColor, 0.1f);
                }
                fadeColor = new Color(0f, 0, 0, 1f);
                Text[] texts = GetComponentsInChildren<Text>();
                foreach (Text text in texts)
                {
                    text.color = Color.Lerp(text.color, fadeColor, 0.1f);
                }
                if (texts[1].color == new Color(0f, 0, 0, 1f))
                {
                    fadingIn = false;
                }
            }
            if (fadingOut)
            {
                Image[] images = GetComponentsInChildren<Image>();
                foreach (Image image in images)
                {
                    image.color = Color.Lerp(image.color, fadeColor, 0.1f);
                }
                Text[] texts = GetComponentsInChildren<Text>();
                foreach (Text text in texts)
                {
                    text.color = Color.Lerp(text.color, fadeColor, 0.1f);
                }
                if (texts[1].color == new Color(0f, 0, 0, 0f))
                {
                    fadingIn = false;
                }
            }
        }

        void ChangingUIHealth()
        {
            if (stats.health >= 0)
            {
                heart2.enabled = true;
                /*
                jagHealthText.text = (stats.health.ToString());
                jagMaxHealthText.text = (stats.maxHealth.ToString());
                */
                lebenInnenImageNos.fillAmount = ((float)stats.health / (float)stats.maxHealth);
            }
            else
            {
                heart2.enabled = false;
            }
        }
        public void aktivieren()
        {
            aktiv = true;
            fadeIn();
        }
        public void deaktivieren()
        {
            aktiv = false;
            jagHealthText.enabled = false;
            jagMaxHealthText.enabled = false;
            lebenInnenImageNos.enabled = false;
            heart2.enabled = false;
        }

        public void fadeIn()
        {
            fadingIn = true;
            fadingOut = false;
            fadeColor = new Color(1f, 1, 1, 1f);
        }

        public void fadeOut()
        {
            fadingOut = true;
            fadingIn = false;
            fadeColor = new Color(0f, 0, 0, 0f);
        }
    }
}