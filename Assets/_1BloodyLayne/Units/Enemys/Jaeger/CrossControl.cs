using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class CrossControl : MonoBehaviour
    {
        public GameObject deathparticles;
        public float lifeTime = 5f;
        public int attackDamage;
        
        private AudioSource bounceSound;
        private bool dead, bounced;
        private Rigidbody2D rb;
        private BoxCollider2D[] colliders;
        private ObjektStats stats;
            
        // Start is called before the first frame update
        void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
            colliders = GetComponents<BoxCollider2D>();
            bounceSound = GetComponent<AudioSource>();
            stats = GetComponent<ObjektStats>();
            foreach (BoxCollider2D collider in colliders)
            {
                collider.enabled = false;
            }
        }

        // Update is called once per frame
        void Update()
        {
            //rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + 0.1f);
            if (rb.velocity.magnitude < 0.2f && !dead && bounced)
            {
                dead = true;
                death();
            }
        }


        private void OnCollisionEnter2D(Collision2D other)
        {
            if (stats.destructable)
            {
                if (other.gameObject.CompareTag("Player") && rb.velocity.magnitude > 0.5f &&
                    other.transform.position.y < transform.position.y + 1f)
                {
                    ContactPoint2D[] contacts = new ContactPoint2D[1];
                    other.GetContacts(contacts);
                    Vector2 contactPoint = contacts[0].point;
                    other.gameObject.GetComponent<PlatformerCharacter2D>().Hit(attackDamage,
                        DamageTypeToPlayer.BluntProjectile, contactPoint, gameObject, Guid.NewGuid());
                    death();
                }

                if (other.gameObject.CompareTag("Ground") && rb.velocity.magnitude > 0.5f)
                {
                    bounced = true;
                    bounceSound.PlayDelayed(0);
                }
            }
        }

        public void throwCross(Vector2 direction)
        {
            foreach (BoxCollider2D collider in colliders)
            {
                collider.enabled = true;
            }
            rb.AddForce(direction);
            rb.rotation = 50 * Math.Sign(direction.x);
            StartCoroutine(deathTimer());
        }

        public void death()
        {
            Instantiate(deathparticles, transform.position, Quaternion.identity);
            stats.startDeath();
        }

        private IEnumerator deathTimer()
        {
            yield return new WaitForSeconds(lifeTime);
            death();
        }
    }
}