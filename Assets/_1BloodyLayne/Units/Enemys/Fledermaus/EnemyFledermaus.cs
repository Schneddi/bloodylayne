﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Enemys;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class EnemyFledermaus : MonoBehaviour
    {
        private Animator animator;
        private float passedFlipTime;
        public float bewegungx=1, bewegungy=1;
        public float speed, secondsUntilFlip;
        public GameObject Schallwelle;
        private Transform schallWelleSpawn;
        [SerializeField] private LayerMask isPlayer;// A mask determining what is the Player
        public bool bewegtSich, kannDurchWaende;
        bool schussAbklingzeit, fadingOut;
        AudioSource schallSound;
        Rigidbody2D rb;
        EnemyStats stats;
        SpriteRenderer sRenderer;
        private GlobalVariables gv;

        // Use this for initialization
        void Start()
        {
            //rb = GetComponent<Rigidbody2D>();
            schallWelleSpawn = transform.Find("schallWelleSpawn");
            schallSound = GetComponent<AudioSource>();
            rb = GetComponent<Rigidbody2D>();
            schallSound.volume = 1;
            stats = GetComponent<EnemyStats>();
            stats.isAnimal=true;
            animator = GetComponent<Animator>();
            sRenderer = GetComponent<SpriteRenderer>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        }

        // Update is called once per frame

        void Update()
        {
            if (!stats.dead)
            {
                if (Physics2D.OverlapCircle(transform.position, 30f, isPlayer)) //ist der Spieler in der Nähe?
                {
                    bewegtSich = true;
                    if (Physics2D.OverlapCircle(schallWelleSpawn.position, 8f, isPlayer) &&
                        schussAbklingzeit == false) //ist der Spieler in der Nähe?
                    {
                        schussAbklingzeit = true;
                        StartCoroutine(Shooting());
                    }
                }
                else
                {
                    bewegtSich = false;
                }

                if (bewegtSich)
                {
                    passedFlipTime += Time.deltaTime;
                    if (passedFlipTime >= secondsUntilFlip)
                    {
                        bewegungy *= -1;
                        passedFlipTime = 0;
                        if (bewegungy < 0)
                        {
                            bewegungx *= -1;
                        }
                    }
                }
            }

            if (fadingOut)
            {
                sRenderer.color = new Color(sRenderer.color.r, sRenderer.color.g, sRenderer.color.b,
                    sRenderer.color.a - Time.deltaTime);
                if (sRenderer.color.a < 0.01f)
                {
                    Destroy(gameObject);
                }
            }
        }
        private void FixedUpdate()
        {
            if (!stats.dead)
            {
                checkDeath();
                if (bewegtSich == true && kannDurchWaende)
                {
                    transform.position = new Vector2(transform.position.x + 0.005f * speed * bewegungx, transform.position.y + 0.005f * speed * bewegungy);
                }
                else if (bewegtSich == true && !kannDurchWaende)
                {
                    rb.velocity = new Vector2(rb.velocity.x + 0.005f * speed * bewegungx, rb.velocity.y - 0.005f * speed * bewegungy);
                }
            }
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Ground") || other.gameObject.CompareTag("Object") || other.gameObject.CompareTag("Enemy") && !stats.dead)
            {
                bewegungy *= -1;
                passedFlipTime = 0;
                if (bewegungy < 0)
                {
                    bewegungx *= -1;
                }
            }
        }

        IEnumerator Shooting()//Gibt Schussfrequenz und Animationen an
        {
            Fire();
            schallSound.PlayDelayed(0);
            float sonicWaveCooldown = 2.75f;
            sonicWaveCooldown -= gv.difficulty * 0.25f;
            yield return new WaitForSeconds(sonicWaveCooldown);
            schussAbklingzeit = false;
        }

        void Fire()//erzeugt eine schallWelle am schallWelleSpawn
        {
            GameObject sonicWaveInstance = Instantiate(Schallwelle, schallWelleSpawn.position, Quaternion.identity);
            sonicWaveInstance.GetComponent<SchallWelleControl>().attackDamage = stats.attacks.GetValueOrDefault("SonicWave");
        }
        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }
        private void Death()
        {
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            StartCoroutine(GetComponent<AnimalDeathSequence>().Death());
        }
    }
}