﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class SchallWelleControl : MonoBehaviour
    {
        public int attackDamage=1;
        public float speed = 3f;
        
        private GameObject Player;
        private PlatformerCharacter2D Larry;
        private Rigidbody2D rbLarry;
        private Rigidbody2D rb;
        private Vector2 richtung;
        private float angle;
        private AudioSource hitSound;
        private bool disableHit;

        // Use this for initialization
        void Start()
        {
            Player = GameObject.FindWithTag("Player");
            Larry = Player.GetComponent<PlatformerCharacter2D>();
            rbLarry = Player.GetComponent<Rigidbody2D>();
            rb = GetComponent<Rigidbody2D>();
            richtung = rbLarry.position - rb.position;
            angle = (Mathf.Atan2(richtung.y, richtung.x)+84.2f)* Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = q;
            rb.velocity = richtung.normalized*speed;
            StartCoroutine(abschuss());
            hitSound = GetComponent<AudioSource>();
            //rb.velocity = Vector2.Scale(richtung.normalized, new Vector2(totalDuration,totalDuration).normalized);
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (!disableHit)
            {
                if (Physics2D.OverlapCircle(transform.position, 0.85f, 1 << 11)) //layer Ebene 9 = Enemy
                {
                    Destroy(gameObject); //zerstöre den Pfeil bevor er einen Enemy berührt, sonst verschiebt er die Figur
                }

                if (Physics2D.OverlapCircle(transform.position, 0.85f, 1 << 8)) //layer Ebene 9 = Enemy
                {
                    Player = GameObject.FindWithTag("Player");
                    Larry = Player
                        .GetComponent<
                            PlatformerCharacter2D>(); //playerChar ist nicht initialisiert, wenn der Collider direkt in Larry startet. Dann wird OnCollisionEnter2D zuerst aufgerufen
                    StartCoroutine(PlayerHit());
                }
            }
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!disableHit)
            {
                if (other.gameObject.CompareTag("Player"))
                {
                    Player = GameObject.FindWithTag("Player");
                    Larry = Player
                        .GetComponent<
                            PlatformerCharacter2D>(); //playerChar ist nicht initialisiert, wenn der Collider direkt in Larry startet. Dann wird OnCollisionEnter2D zuerst aufgerufen
                    StartCoroutine(PlayerHit());
                }
                else if (other.gameObject.CompareTag("Ground"))
                {
                    Destroy(gameObject);
                }
                else
                {
                    Destroy(gameObject);
                }
            }
        }
        IEnumerator abschuss()
        {
            yield return new WaitForSeconds(0.6f);
            if (Physics2D.OverlapCircle(transform.position, 0.85f, 1 << 11))//layer Ebene 9 = Enemy
            {
                Destroy(gameObject); //zerstöre den Pfeil bevor er einen Enemy berührt, sonst verschiebt er die Figur
            }
            GetComponent<BoxCollider2D>().enabled=true;
        }
        IEnumerator PlayerHit()
        {
            disableHit = true;
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            if(Larry.Hit(attackDamage, DamageTypeToPlayer.SonicWave, new Vector2(richtung.normalized.x, richtung.normalized.y), transform.position, gameObject, Guid.NewGuid()))
            {
                hitSound.PlayDelayed(0);
            }
            hitSound.enabled=true;
            yield return new WaitForSeconds(0.5f);
            Destroy(gameObject);
        }
    }
}
