﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FledermausEyeMovement : MonoBehaviour
{
    SpriteRenderer sRenderer;
    Transform batTransform;
    int spriteNumber;
    // Start is called before the first frame update
    void Start()
    {
        sRenderer = transform.parent.GetComponent<SpriteRenderer>();
        batTransform = transform.parent;
        spriteNumber = 0;
    }

    void Update()
    {
        if (sRenderer.sprite.name == "FledermausSprites_0" && spriteNumber != 0)
        {
            transform.position = new Vector3(transform.position.x, batTransform.position.y, transform.position.z);
            spriteNumber = 0;
        }
        if (sRenderer.sprite.name == "FledermausSprites_1" && spriteNumber != 1)
        {
            transform.position = new Vector3(transform.position.x, batTransform.position.y - 0.02f, transform.position.z);
            spriteNumber = 1;
        }
        if (sRenderer.sprite.name == "FledermausSprites_2" && spriteNumber != 2)
        {
            transform.position = new Vector3(transform.position.x, batTransform.position.y - 0.05f, transform.position.z);
            spriteNumber = 2;
        }
        if (sRenderer.sprite.name == "FledermausSprites_3" && spriteNumber != 3)
        {
            transform.position = new Vector3(transform.position.x, batTransform.position.y - 0.02f, transform.position.z);
            spriteNumber = 3;
        }
        if (sRenderer.sprite.name == "FledermausSprites_4" && spriteNumber != 4)
        {
            transform.position = new Vector3(transform.position.x, batTransform.position.y, transform.position.z);
            spriteNumber = 4;
        }
        if (sRenderer.sprite.name == "FledermausSprites_5" && spriteNumber != 5)
        {
            transform.position = new Vector3(transform.position.x, batTransform.position.y + 0.04f, transform.position.z);
            spriteNumber = 5;
        }
        if (sRenderer.sprite.name == "FledermausSprites_6" && spriteNumber != 6)
        {
            transform.position = new Vector3(transform.position.x, batTransform.position.y + 0.11f, transform.position.z);
            spriteNumber = 6;
        }
        if (sRenderer.sprite.name == "FledermausSprites_7" && spriteNumber != 7)
        {
            transform.position = new Vector3(transform.position.x, batTransform.position.y + 0.04f, transform.position.z);
            spriteNumber = 7;
        }
    }
}
