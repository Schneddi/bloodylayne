﻿using System.Collections;
using _1BloodyLayne.Control;
using Units.Enemys;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class ShyFledermaus : MonoBehaviour
    {
        public float bewegungx = 1, bewegungy = 1;
        public float speed;
        public float secondsUntilFlip;
        public bool bewegtSich = true, kannDurchWaende, alwaysActive;
        public GameObject detectorSchallwelle;

        [SerializeField] private bool tutorialBat;
        
        private Transform schallWelleSpawn;
        private bool schussAbklingzeit, flieht, fadingOut;
        private AudioSource schallSound, fluchtSound;
        private Rigidbody2D rb;
        private EnemyStats stats;
        private float fluchtX, fluchtY;
        private SpriteRenderer[] sRenderers;
        private Animator animator;
        private float passedFlipTime;
        private GlobalVariables gv;

        // Use this for initialization
        void Start()
        {
            //rb = GetComponent<Rigidbody2D>();
            schallWelleSpawn = transform.Find("schallWelleSpawn");
            schallSound = GetComponents<AudioSource>()[0];
            fluchtSound = GetComponents<AudioSource>()[1];
            rb = GetComponent<Rigidbody2D>();
            schallSound.volume = 1;
            stats = GetComponent<EnemyStats>();
            stats.isAnimal = true;
            animator = GetComponent<Animator>();
            sRenderers = transform.Find("HDBatExport").GetComponentsInChildren<SpriteRenderer>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        }

        // Update is called once per frame

        void Update()
        {
            if (!stats.dead)
            {
                if (Physics2D.OverlapCircle(transform.position, 30f, 1<<8))
                {
                    bewegtSich = true;
                    if (!tutorialBat && Physics2D.OverlapCircle(schallWelleSpawn.position, 8f, 1<<8) && schussAbklingzeit == false)//ist der Spieler in der Nähe?
                    {
                        schussAbklingzeit = true;
                        StartCoroutine(Shooting());
                    }
                }
                else if (!alwaysActive)
                {
                    bewegtSich = false;
                }
                passedFlipTime += Time.deltaTime;
                if (passedFlipTime >= secondsUntilFlip)
                {
                    bewegungy *= -1;
                    passedFlipTime = 0;
                    if (bewegungy < 0)
                    {
                        bewegungx *= -1;
                    }
                }
            }
            
            if (fadingOut)
            {
                foreach (SpriteRenderer sRenderer in sRenderers)
                {
                    sRenderer.color = new Color(sRenderer.color.r, sRenderer.color.g, sRenderer.color.b,
                        sRenderer.color.a - Time.deltaTime);
                    if (sRenderer.color.a < 0.01f)
                    {
                        Destroy(gameObject);
                    }
                }
            }
        }
        private void FixedUpdate()
        {
            if (!stats.dead)
            {
                checkDeath();
                if (!flieht) {
                    if (bewegtSich && kannDurchWaende)
                    {
                        transform.position = new Vector2(transform.position.x + 0.005f * speed * bewegungx, transform.position.y + 0.005f * speed * bewegungy);
                    }
                    else if (bewegtSich == true && !kannDurchWaende)
                    {
                        rb.velocity = new Vector2(rb.velocity.x + 0.001f * speed * bewegungx, rb.velocity.y - 0.001f * speed * bewegungy);
                    }
                }
                else if (bewegtSich)
                {
                    rb.velocity = new Vector2(rb.velocity.x + fluchtX * 0.01f, rb.velocity.y + fluchtY * 0.01f);
                }
            }
        }

        IEnumerator Shooting()//Gibt Schussfrequenz und Animationen an
        {
            Fire();
            schallSound.PlayDelayed(0);
            float sonicWaveCooldown = 3.5f;
            sonicWaveCooldown -= gv.difficulty * 0.5f;
            yield return new WaitForSeconds(sonicWaveCooldown);
            schussAbklingzeit = false;
        }

        void Fire()//erzeugt eine schallWelle am schallWelleSpawn
        {
            GameObject welle=Instantiate(detectorSchallwelle, schallWelleSpawn.position, Quaternion.identity);
            
            welle.GetComponent<DetectorSchallwelleControl>().bat = gameObject;
        }

        public void flee(float x, float y)
        {
            fluchtSound.PlayDelayed(0);
            flieht = true;
            animator.SetTrigger("Flee");
            fluchtX = x;
            fluchtY = y;
        }

        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }


        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Ground") || other.gameObject.CompareTag("Object") || other.gameObject.CompareTag("Enemy") && !animator.GetBool("Death"))
            {
                if (flieht)
                {
                    fadingOut = true;
                    GetComponent<BoxCollider2D>().enabled = false;
                    GetComponent<CircleCollider2D>().enabled = false;
                    rb.velocity = new Vector2(rb.velocity.normalized.x * 5, rb.velocity.normalized.y * 5);
                }
                else
                {
                    bewegungy *= -1;
                    passedFlipTime = 0;
                    if (bewegungy < 0)
                    {
                        bewegungx *= -1;
                    }
                }
            }
        }

        private void Death()
        {
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            fluchtSound.Stop();
            StartCoroutine(GetComponent<AnimalDeathSequence>().Death());
        }
    }
}