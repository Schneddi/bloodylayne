﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class DetectorSchallwelleControl : MonoBehaviour
    {
        GameObject Player;
        public GameObject bat;
        Rigidbody2D rb;
        Vector2 richtung;
        public float speed = 3f;
        float angle;
        public int attackDamage = 1;
        AudioSource hitSound;
        bool rueck=false;

        // Use this for initialization
        void Start()
        {
            Player = GameObject.FindWithTag("Player");
            rb = GetComponent<Rigidbody2D>();
            richtung = GameObject.FindWithTag("Player").GetComponent<Rigidbody2D>().position - rb.position;
            angle = (Mathf.Atan2(richtung.y, richtung.x) + 84.2f) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = q;
            rb.velocity = richtung.normalized * speed;
            StartCoroutine(abschuss());
            hitSound = GetComponent<AudioSource>();
            //rb.velocity = Vector2.Scale(richtung.normalized, new Vector2(totalDuration,totalDuration).normalized);
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (Physics2D.OverlapCircle(transform.position, 0.85f, 1 << 11))//layer Ebene 11 = Boden
            {
                Destroy(gameObject);
            }
            if (rueck)
            {
                Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.9f);
                foreach (Collider2D col in cols)
                {
                    if (col.gameObject == bat)
                    {
                        bat.GetComponent<ShyFledermaus>().flee(richtung.x, richtung.y);
                        Destroy(gameObject);
                    }
                }
            }
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                Player = GameObject.FindWithTag("Player");
                if (bat != null)
                {
                    richtung = bat.transform.position - transform.position;
                }
                else
                {
                    Destroy(gameObject);
                }
                rb.velocity = richtung.normalized * speed * 2;
                angle = (Mathf.Atan2(richtung.y, richtung.x) + 84.2f) * Mathf.Rad2Deg;
                Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
                transform.rotation = q;
                GetComponent<BoxCollider2D>().enabled = false;
                rueck = true;
            }
            else if (other.gameObject.CompareTag("Ground"))
            {
                Destroy(gameObject);
            }
        }

        IEnumerator abschuss()
        {
            yield return new WaitForSeconds(0.5f);
            if (Physics2D.OverlapCircle(transform.position, 0.85f, 1 << 11))//layer Ebene 9 = Enemy
            {
                Destroy(gameObject); //zerstöre den Pfeil bevor er einen Enemy berührt, sonst verschiebt er die Figur
            }
            GetComponent<BoxCollider2D>().enabled = true;
        }
    }
}
