﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Enemys;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Bombenwerfer : MonoBehaviour
    {
        public GameObject bombe;
        public Transform endeLinks, endeRechts;
        public float speed = 10, idleDrag = 30;
        
        private Transform player, abwurf;
        private EnemyStats stats;
        private bool idle = true, wirft, rennt, chasing, dead;
        private AudioSource throwSound, runSound;
        private Animator animator;
        private Rigidbody2D rb;
        private float dragStore;
        private GlobalVariables gv;
        
        // Use this for initialization
        void Start()
        {
            player = GameObject.FindWithTag("Player").transform;
            animator = GetComponent<Animator>();
            abwurf = transform.Find("BombSpawn");
            rb = GetComponent<Rigidbody2D>();
            stats = GetComponent<EnemyStats>();
            stats.isHuman = true;
            throwSound = GetComponents<AudioSource>()[0];
            runSound = GetComponents<AudioSource>()[1];
            dragStore = rb.drag;
            rb.drag = idleDrag;
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                speed *= -1;
                transform.localScale = theScale;
            }
        }

        // Update is called once per frame
        void Update()
        {
            checkDeath();
            if (!dead)
            {
                FlipCheck();
                if (idle)
                {
                    if (Physics2D.OverlapCircle(transform.position, 15f, 1 << 8) && (transform.position.y - 4f < player.position.y && player.position.y < transform.position.y + 1f) && checkIfLarryInSight() && !wirft)
                    {
                        idle = false;
                        rb.drag = dragStore;
                        animator.SetTrigger("Wurf");
                        StartCoroutine(BombenThrow());
                    }
                }
                if (idle && chasing)
                {
                    if (Physics2D.OverlapCircle(transform.position, 15f, 1 << 8) && (transform.position.y - 4f < player.position.y && player.position.y < transform.position.y + 1f) && !wirft)
                    {
                        animator.SetTrigger("Wurf");
                        StartCoroutine(BombenThrow());
                    }
                    //spieler ist zwischen den endTransform. Spielerhöhe ist höher als linker und niedriger als rechter Transform
                    else if (endeLinks.position.x < player.position.x && player.position.x < endeRechts.position.x && endeLinks.position.y < player.position.y && player.position.y < endeRechts.position.y)
                    {
                        idle = false;
                        rb.drag = dragStore;
                        rennt = true;
                        animator.SetTrigger("Run");
                        runSound.PlayDelayed(0);
                    }

                }
            }
        }

        private void FixedUpdate()
        {
            if (rennt && !dead)
            {
                if (Physics2D.OverlapCircle(transform.position, 15f, 1 << 8) && (transform.position.y - 4f < player.position.y && player.position.y < transform.position.y + 1.5f) && Mathf.Abs(player.position.x - transform.position.x) > 3 && !wirft)
                {
                    rennt = false;
                    wirft = true;
                    animator.SetTrigger("Wurf");
                    StartCoroutine(BombenThrow());
                    runSound.Stop();
                }
                else if (endeLinks.position.x < player.position.x && player.position.x < endeRechts.position.x && endeLinks.position.y < player.position.y && player.position.y < endeRechts.position.y && Mathf.Abs(player.position.x - transform.position.x) > 3)
                {
                    rb.AddForce(new Vector2(speed, 0));
                }
                else
                {
                    runSound.Stop();
                    animator.SetTrigger("Idle");
                    idle = true;
                    rb.drag = idleDrag;
                    rennt = false;
                }
            }
        }


        void FlipCheck()//z.B. in andere Richtung gucken
        {
            if (Math.Abs(transform.position.x - player.position.x) < 0.25f)
            {
                return; 
            }
            
            if (stats.facingRight && transform.position.x > player.position.x)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
            else if (!stats.facingRight && transform.position.x < player.position.x)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
        }

        private bool checkIfLarryInSight()
        {
            Vector3 richtung = player.position - transform.position;
            RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
            foreach (RaycastHit2D hitter in hit)
            {
                if (hitter.transform.tag == "Ground")
                {
                    break;
                }
                if (hitter.transform.tag == "Player")
                {
                    return true;
                }
            }
            return false;
        }

        IEnumerator BombenThrow()
        {
            wirft = true;
            FlipCheck();
            yield return new WaitForSeconds(0.3f);
            throwSound.PlayDelayed(0);
            yield return new WaitForSeconds(0.2f);
            if (stats.facingRight)
            {
                GameObject bombChild = Instantiate(bombe, abwurf.position, Quaternion.identity);
                bombChild.GetComponent<BombeControl>().speed = new Vector2(player.position.x - transform.position.x * 1f + 1, 7.5f);
                bombChild.GetComponent<BombeControl>().attackDamage = stats.attacks.GetValueOrDefault("BombThrow");
            }
            else
            {
                GameObject bombChild = Instantiate(bombe, abwurf.position, Quaternion.identity);
                bombChild.GetComponent<BombeControl>().speed = new Vector2(player.position.x - transform.position.x * 1f - 1, 7.5f);
                bombChild.GetComponent<BombeControl>().attackDamage = stats.attacks.GetValueOrDefault("BombThrow");
            }

            float reloadingCooldown = 3f;
            reloadingCooldown -= gv.difficulty * 0.5f;
            yield return new WaitForSeconds(reloadingCooldown);
            idle = true;
            rb.drag = idleDrag;
            chasing = true;
            wirft = false;
            animator.SetTrigger("Idle");
        }

        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                dead = true;
                Death();
            }
        }

        private void Death()
        {
            stats.dead = true;
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            rb.angularDrag = 0;
            rb.drag = 0.5f;
            GetComponent<HumanDeathSequence>().Death();
        }
    }
}
