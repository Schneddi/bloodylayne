﻿using System;
using System.Collections;
using System.Collections.Generic;
using UI.PlayerUI;
using Units.Enemys;
using Units.Player;
using UnityEditor;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class BombeControl : MonoBehaviour
    {
        public Vector2 speed;
        Rigidbody2D rb;
        GameObject Player;
        private PlatformerCharacter2D larry;
        public int attackDamage = 70;
        public GameObject explosion;
        private Camera2DFollow camerascript;

        // Use this for initialization
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            rb.velocity = speed;
            Player = GameObject.FindWithTag("Player");
            larry = Player.GetComponent<PlatformerCharacter2D>();
            rb.velocity = new Vector2(speed.x, speed.y);
            camerascript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
            StartCoroutine(Abschuss());
        }
        IEnumerator Abschuss()
        {
            yield return new WaitForSeconds(1.2f);
            Instantiate(explosion, transform.position, Quaternion.identity);
            if (Vector2.Distance(transform.position, larry.transform.position) < 10)
            {
                camerascript.ShakeCamera(0.1f,1f, 60);
            }
            
            if (Physics2D.OverlapCircle(transform.position, 4.5f, 1 << 8))//Spieler Hit
            {
                Vector3 richtung = Player.transform.position - transform.position;
                RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
                foreach (RaycastHit2D hitter in hit)
                {
                    if (hitter.transform.tag == "Ground")
                    {
                        break;
                    }
                    if (hitter.transform.tag == "Player")
                    {
                        larry.Hit(attackDamage, DamageTypeToPlayer.Explosion, transform.position, gameObject, Guid.NewGuid());
                        break;
                    }
                }
            }
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 4.5f, 1 << 9);//layer Ebene 9 = Enemys
            System.Guid guid = System.Guid.NewGuid();
            foreach (Collider2D col in cols)
            {
                if ((col.gameObject.GetComponent("EnemyStats") as EnemyStats) != null)
                {
                    Vector3 richtung = col.gameObject.transform.position - transform.position;
                    RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
                    foreach (RaycastHit2D hitter in hit)
                    {
                        if (hitter.transform.tag == "Ground")
                        {
                            break;
                        }
                        if (hitter.transform.tag == "Enemy")
                        {
                            if (hitter == col)
                            {
                                col.GetComponent<EnemyStats>().Hit(attackDamage, DamageTypeToEnemy.Explosion, guid, false);
                            }
                            break;
                        }
                    }
                }
            }

            cols = Physics2D.OverlapCircleAll(transform.position, 4.5f, 1<<14);//layer 14 = Objects
            {
                foreach (Collider2D col in cols)
                {
                    if (col.gameObject.GetComponent<ObjektStats>() != null)
                    {
                        Vector3 richtung = col.gameObject.transform.position - transform.position;
                        RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
                        foreach (RaycastHit2D hitter in hit)
                        {
                            if (hitter.transform.tag == "Ground")
                            {
                                break;
                            }
                            if (hitter.transform.tag == "Object")
                            {
                                col.gameObject.GetComponent<ObjektStats>().addDamage(attackDamage);
                            }
                        }
                    }
                }
            }
            Destroy(gameObject);
        }
    }
}