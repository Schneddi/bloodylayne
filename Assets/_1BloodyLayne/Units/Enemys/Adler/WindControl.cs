﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class WindControl : MonoBehaviour
    {
        GameObject Player;
        Rigidbody2D rbLarry;
        Rigidbody2D rb;
        Vector2 richtung;
        public float speed = 10f;
        float angle;
        public int attackDamage = 0;

        // Use this for initialization
        void Start()
        {
            Player = GameObject.FindWithTag("Player");
            rbLarry = Player.GetComponent<Rigidbody2D>();
            rb = GetComponent<Rigidbody2D>();
            richtung = rbLarry.position - rb.position;
            angle = ((Mathf.Atan2(richtung.y, richtung.x)) * Mathf.Rad2Deg)+ 170f;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = q;
            rb.velocity = richtung.normalized * speed;
            //rb.velocity = Vector2.Scale(richtung.normalized, new Vector2(totalDuration,totalDuration).normalized);
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Ground"))
            {
                Destroy(gameObject);
            }
            else if(!other.gameObject.CompareTag("Player") && !!other.gameObject.CompareTag("Player"))
            {
                Destroy(gameObject);
            }
        }
    }
}
