﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Enemys;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class Adler : MonoBehaviour
    {
        private Animator animator;
        public Transform windSpawn;
        public GameObject wind;
        float bewegungX, ursprungsY;
        public float range, speed;
        [SerializeField] private LayerMask isPlayer;                  // A mask determining what is the Player
        bool abklingzeit, fadingOut;
        AudioSource[] sounds;
        AudioSource windSound, adlerKreischen; 
        Rigidbody2D rb;
        EnemyStats stats;
        bool flattern = false, gleitfliegen = true, attacking = false, collision, kreischen;
        Transform player;
        public bool facingRight = false;
        int flatterZahl;
        public GameObject deathSound;
        SpriteRenderer sRenderer;
        private GlobalVariables gv;

        // Use this for initialization
        void Start()
        {
            bewegungX = -4f;
            ursprungsY = transform.position.y; //damit der Adler später wieder auf seine Höhe zurückfindet
            sounds = GetComponents<AudioSource>();
            windSound = sounds[0];
            adlerKreischen = sounds[1];
            rb = GetComponent<Rigidbody2D>();
            stats = GetComponent<EnemyStats>();
            stats.isAnimal = true;
            animator = GetComponent<Animator>();
            player = GameObject.FindGameObjectWithTag("Player").transform;
            windSpawn = transform.Find("WindSpawn");
            sRenderer = GetComponent<SpriteRenderer>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            if (facingRight)
            {
                Flip();
            }
        }

        // Update is called once per frame

        void Update()
        {
            if (fadingOut)
            {
                sRenderer.color = new Color(sRenderer.color.r, sRenderer.color.g, sRenderer.color.b,
                    sRenderer.color.a - Time.deltaTime);
                if (sRenderer.color.a < 0.01f)
                {
                    Destroy(gameObject);
                }
            }
        }
        
        private void FixedUpdate()
        {
            if (!animator.GetBool("Death") && !collision)
            {
                checkDeath();
                if (transform.position.y + 2 < ursprungsY && gleitfliegen)
                {
                    rb.velocity = new Vector2(rb.velocity.x, 0);
                    gleitfliegen = false;
                    flatterZahl++;
                }
                if (flatterZahl == 2)
                {
                    Flip();
                    flatterZahl = 0;
                }
                if (transform.position.y >= ursprungsY)
                {
                    rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y - 0.01f);
                }
                if (flattern) //eagle flies back to original height
                {
                    float yVelocity = 0.05f;
                    yVelocity += gv.difficulty * 0.02f;
                    rb.velocity = new Vector2(bewegungX, rb.velocity.y + yVelocity);
                }
                if (gleitfliegen) { //eagle looses height
                    float yVelocity = -1;
                    yVelocity -= gv.difficulty * 0.25f;
                    rb.velocity = new Vector2(bewegungX, yVelocity);
                }
                else if(!gleitfliegen)
                {
                    float detectionRadius = 10;
                    detectionRadius += gv.difficulty * 2f;
                    if (Physics2D.OverlapCircle(transform.position, detectionRadius, 1<<8) && abklingzeit == false && !flattern)//ist der Spieler in der Nähe?
                    {
                        abklingzeit = true;
                        StartCoroutine(WingAttackCooldown());
                        StartCoroutine(Fluegelschlag());
                        rb.velocity = new Vector2(0,0);
                    }
                    if (!flattern && !attacking)
                    {
                        flattern = true;
                        StartCoroutine(Flattern());
                    }
                    if (transform.position.y >= ursprungsY)
                    {
                        gleitfliegen = true;
                        flattern = false;
                    }

                }
                
            }
        }
        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            facingRight = !facingRight;
            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
            bewegungX *= -1;

            rb.velocity = new Vector2(bewegungX, rb.velocity.y);
        }
        IEnumerator WingAttackCooldown()
        {
            float cooldownTime = 6;
            cooldownTime -= gv.difficulty;
            yield return new WaitForSeconds(cooldownTime);
            abklingzeit = false;
        }
        IEnumerator Fluegelschlag()//Gibt Schussfrequenz und Animationen an
        {
            attacking = true;
            windSound.PlayDelayed(0);
            if (player.transform.position.x < transform.position.x && facingRight)
            {
                Flip();
                flatterZahl = 0;
            }else if (player.transform.position.x > transform.position.x && !facingRight)
            {
                Flip();
                flatterZahl = 0;
            }
            animator.SetTrigger("Fluegelschlag");
            yield return new WaitForSeconds(0.5f);
            Instantiate(wind, windSpawn.position, Quaternion.identity);
            yield return new WaitForSeconds(0.1f);
            attacking = false;
            //StartCoroutine(Sturzflug());
        }
        /*
        IEnumerator Sturzflug()//Gibt Schussfrequenz und Animationen an
        {
            adlerKreischen.PlayDelayed(0);
            yield return new WaitForSeconds(2f);
            attacking = false;
            StartCoroutine(Fluegelschlag());
        }*/
        IEnumerator Flattern()//Gibt Schussfrequenz und Animationen an
        {
            if (!animator.GetBool("Flug"))
            {
                animator.SetTrigger("Flug");
                yield return new WaitForSeconds(1f);
                animator.SetTrigger("Gleitflug");
            }
        }



        public void Hit(int damage, float x, float y)//der Schaden und Kraft in eine Richtung x y für einen Rückstoß
        {
            stats.health = stats.health - damage;
            rb.velocity = new Vector2(rb.velocity.y + x, rb.velocity.y + y);

        }
        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }


        private void Death()
        {
            Instantiate(deathSound, transform.position, Quaternion.identity);
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            StartCoroutine(GetComponent<AnimalDeathSequence>().Death());
        }

        IEnumerator Collision()
        {
            collision = true;
            yield return new WaitForSeconds(1f);
            collision = false;
        }

        IEnumerator Kreischen()
        {
            kreischen = true;
            adlerKreischen.PlayDelayed(0);
            yield return new WaitForSeconds(1f);
            kreischen = false;
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!animator.GetBool("Death"))
            {
                if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Ground") && !collision)
                {
                    Flip();
                    flatterZahl = 0;
                    StartCoroutine(Collision());
                    rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + 5);
                }
                if (other.gameObject.CompareTag("Player"))
                {
                    if (!kreischen)
                    {
                        StartCoroutine(Kreischen());
                    }
                    if (gleitfliegen)
                    {
                        rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + 5);
                        StartCoroutine(Flattern());
                    }
                    if (bewegungX < 10)
                    {
                        rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + 5);
                        bewegungX = bewegungX * 1.1f;
                    }
                }
            }
        }
    }
}