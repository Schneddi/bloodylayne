﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using Units.Player;
using Unity.Mathematics;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Captain : MonoBehaviour
    {
        EnemyStats stats;
        public GameObject deniedEffect;
        bool switching, hitZone, hitZoneSchaft, hitZoneTop, rennt, idle = true;
        private Transform player, schild, hit, hitSchaft, hitTop;
        public Transform endeRechts, endeLinks;
        Animator animator;
        AudioSource runSound, hitSound, switchSound, abwehrSound;
        Rigidbody2D rb;
        Vector2 speed = new Vector2(17, 0);

        // Use this for initialization
        void Start()
        {
            player = GameObject.FindWithTag("Player").transform;
            schild = transform.Find("Projektilabfang");
            hit = transform.Find("HitZone");
            hitSchaft = transform.Find("HitZoneSchaft");
            hitTop = transform.Find("HitZoneTop");
            stats = GetComponent<EnemyStats>();
            stats.isHuman = true;
            rb = GetComponent<Rigidbody2D>();
            runSound = GetComponents<AudioSource>()[0];
            hitSound = GetComponents<AudioSource>()[1];
            switchSound = GetComponents<AudioSource>()[2];
            abwehrSound = GetComponents<AudioSource>()[3];
            animator = GetComponent<Animator>();
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                speed *= -1;
                transform.localScale = theScale;
            }
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (!animator.GetBool("Death"))
            {
                checkDeath();
                if (!animator.GetBool("Death"))
                {

                    if (rennt)
                    {
                        if (stats.facingRight && !((endeRechts.position.x - 0.5f < transform.position.x) && (transform.position.x < endeRechts.position.x + 0.5f)))
                        {
                            rb.velocity = speed;
                        }
                        else if (!stats.facingRight && !((endeLinks.position.x - 0.5f < transform.position.x) && (transform.position.x < endeLinks.position.x + 0.5f)))
                        {
                            rb.velocity = speed;
                        }
                        else
                        {
                            rb.velocity = new Vector2(0, 0);
                            rennt = false;
                            runSound.Stop();
                            StartCoroutine(SwitchSide());
                        }
                    }
                }
            }
        }

        void Update()
        {

            if (!animator.GetBool("Death"))
            {
                if (hitZone)
                {
                    if (Physics2D.OverlapCircle(hit.position, 0.5f, 1 << 8))
                    {
                        if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeStab"), DamageTypeToPlayer.MeleeStab, new Vector2(speed.x * 4, 0), hit.position, gameObject, Guid.NewGuid())){
                            hitSound.PlayDelayed(0);
                        }
                    }
                    damageObjects(hit);
                }
                if (hitZoneSchaft)
                {
                    if (Physics2D.OverlapCircle(hitSchaft.position, 0.8f, 1 << 8))
                    {
                        if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeStab"), DamageTypeToPlayer.MeleeStab, new Vector2(speed.x * 4, 0), hitSchaft.position, gameObject, Guid.NewGuid())){
                            hitSound.PlayDelayed(0);
                        }
                    }
                    damageObjects(hitSchaft);
                }
                if (hitZoneTop)
                {
                    if (Physics2D.OverlapCircle(hitTop.position, 0.5f, 1 << 8))
                    {
                        if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeStab"), DamageTypeToPlayer.MeleeStab, new Vector2(speed.x * 4, 0), hitTop.position, gameObject, Guid.NewGuid())){
                            hitSound.PlayDelayed(0);
                        }
                    }
                    damageObjects(hitTop);
                }
                RaycastHit2D hitCol = Physics2D.Raycast(schild.position, Vector2.up, 2.6f, 1 << 20);
                if (hitCol.collider != null && (hitCol.collider.gameObject.GetComponent("BlutmagieChargedProjektilControl") as BlutmagieChargedProjektilControl) == null)
                {
                    destroyProjectile(hitCol.collider.gameObject);
                }
                hitCol = Physics2D.Raycast(new Vector2(schild.position.x + 0.1f, schild.position.y), Vector2.up, 2.6f, 1 << 20);
                if (hitCol.collider != null && (hitCol.collider.gameObject.GetComponent("BlutmagieChargedProjektilControl") as BlutmagieChargedProjektilControl) == null)
                {
                    destroyProjectile(hitCol.collider.gameObject);
                }
                hitCol = Physics2D.Raycast(new Vector2(schild.position.x - 0.1f, schild.position.y), Vector2.up, 2.6f, 1 << 20);
                if (hitCol.collider != null && (hitCol.collider.gameObject.GetComponent("BlutmagieChargedProjektilControl") as BlutmagieChargedProjektilControl) == null)
                {
                    destroyProjectile(hitCol.collider.gameObject);
                }
                if (idle)
                {
                    if (player.position.y < transform.position.y + 1 && (player.position.x > endeLinks.position.x && player.position.x < endeRechts.position.x))
                    {
                        idle = false;
                        StartCoroutine(SwitchDown());
                    }
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "BluntProjectile" && other.GetComponent<BlutmagieChargedProjektilControl>() == null && other.GetComponent<Witchbomb>() == null)
            {
                destroyProjectile(other.gameObject);
            }
        }

        void Flip()//z.B. in andere Richtung gucken
        {
            if (stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
            else if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
        }


        IEnumerator SwitchSide()//Gibt Schussfrequenz und Animationen an
        {
            hitZone = false;
            hitZoneSchaft = false;
            switchSound.PlayDelayed(0);
            animator.SetTrigger("Switch");
            yield return new WaitForSeconds(0.2f);
            hitZoneTop = true;
            yield return new WaitForSeconds(0.25f);
            if (!animator.GetBool("Death"))
            {
                Flip();
                idle = true;
            }
        }

        IEnumerator SwitchDown()//Gibt Schussfrequenz und Animationen an
        {
            hitZoneTop = false;
            switchSound.PlayDelayed(0);
            animator.SetTrigger("SwitchDown");
            yield return new WaitForSeconds(0.3f);
            if (!animator.GetBool("Death"))
            {
                hitZone = true;
                hitZoneSchaft = true;
                rennt = true;
                animator.SetTrigger("Run");
                runSound.PlayDelayed(0);
            }
        }

        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }
        private void Death()
        {
            stats.dead = true;
            runSound.Stop();
            rennt = false;
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            GetComponent<HumanDeathSequence>().Death();
        }

        private void destroyProjectile(GameObject projectile)
        {
            Instantiate(deniedEffect, projectile.transform.position, quaternion.identity);
            abwehrSound.PlayOneShot(abwehrSound.clip);
            Destroy(projectile);
        }

        void damageObjects(Transform hit)
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(hit.position, 0.5f, 1 << 14);
            {
                foreach (Collider2D col in cols)
                {
                    if (col.gameObject.GetComponent<ObjektStats>() != null)
                    {
                        if (col.gameObject.GetComponent<ObjektStats>().destructable)
                        {
                            col.gameObject.GetComponent<ObjektStats>().hitPoints -= stats.attacks.GetValueOrDefault("MeleeStab");
                            if (col.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                            {
                                col.gameObject.GetComponent<ObjektStats>().startDeath();
                            }
                        }
                    }
                }
            }
        }
    }
}