﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Baer : MonoBehaviour {

    // Use this for initialization
    Animator animator;
    bool walk;
    Rigidbody2D rb;
	void Start ()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }
    private void FixedUpdate()
    {
        if (walk) {
            rb.velocity = new Vector2(5,0);
        }
    }
    public void walkAway()
    {
        walk = true;
        animator.SetTrigger("Walk");

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;

        StartCoroutine(destroyWithDelay());
    }
    
    IEnumerator destroyWithDelay()
    {
        yield return new WaitForSeconds(8f);
        Destroy(gameObject);
    }
}
