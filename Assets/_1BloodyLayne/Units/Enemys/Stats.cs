﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]


public class Stats 
 {
	
	private  int health;

	public int MaxHealthPoint=5;
	 
	public int Health
	{
	get
	{
		return health;
	}
	set
	{
		health=Mathf.Clamp(value,0,MaxHealthPoint);

		
	}
	}
 
	public void setHealth()
	{
		Health=MaxHealthPoint;
	}

	

 }

 

	
  