﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using Units.Player;
using Unity.Mathematics;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Rabe : MonoBehaviour
    {
        public bool facingRight;
        public float detectionRange=35f;
        public GameObject deathSound;
        
        private Animator animator;
        private float bewegungx;
        private float speed= -4f;
        [SerializeField] private LayerMask isPlayer;                  // A mask determining what is the Player
        private AudioSource kraehenSound, hitSound, abflugSound;
        private AudioSource[] sounds;
        private Rigidbody2D rb;
        private EnemyStats stats;
        private bool attacked, attacking, abfliegen, collionsFading = true, fadingOut;
        private PlatformerCharacter2D playerChar;
        private float yFlug;
        private SpriteRenderer sRenderer;
        private Transform peaker;

        // Use this for initialization
        void Start()
        {
            bewegungx = 1;
            rb = GetComponent<Rigidbody2D>();
            sounds = GetComponents<AudioSource>();
            kraehenSound = sounds[0];
            hitSound = sounds[1];
            abflugSound = sounds[2];
            stats = GetComponent<EnemyStats>();
            stats.isAnimal = true;
            animator = GetComponent<Animator>();
            playerChar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            sRenderer = GetComponent<SpriteRenderer>();
            peaker = transform.Find("Schnabel");
            if (facingRight)
            {
                flip();
            }
        }

        // Update is called once per frame

        void Update()
        {
            if (fadingOut)
            {
                sRenderer.color = new Color(sRenderer.color.r, sRenderer.color.g, sRenderer.color.b,
                    sRenderer.color.a - Time.deltaTime);
                if (sRenderer.color.a < 0.01f)
                {
                    Destroy(gameObject);
                }
            }
        }
        private void FixedUpdate()
        {
            if (!animator.GetBool("Death"))
            {
                checkDeath();
                if ((Physics2D.OverlapCircle(transform.position, detectionRange, isPlayer) && !attacked) || abfliegen)//ist der Spieler in der Nähe?
                {
                    rb.velocity = new Vector2(speed * bewegungx, rb.velocity.y+yFlug);
                    Destroy(gameObject, 30);
                }

                if (Physics2D.OverlapCircle(transform.position, 8f, isPlayer) && !attacked)//ist der Spieler in der Nähe?
                {
                    attacked = true;
                    attacking = true;
                    StartCoroutine(sturzflug());
                }
                if (attacking)
                {
                    if (Physics2D.OverlapCircle(peaker.position, 0.5f, isPlayer))
                    {
                        hitSound.PlayDelayed(0);
                        attacking = false;
                        if (facingRight)
                        {
                            transform.Rotate(new Vector3(0, 0, 60));
                        }
                        else
                        {
                            transform.Rotate(new Vector3(0, 0, -60));
                        }
                        flip();
                        if (playerChar.Hit(stats.attacks.GetValueOrDefault("RavenStab"), DamageTypeToPlayer.RavenStab, peaker.position, gameObject, new Guid()))
                        {
                            hitSound.PlayDelayed(0);
                        }
                    }
                }
            }
        }
        
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Ground") && collionsFading && !animator.GetBool("Death"))
            {
                fadingOut = true;
                GetComponent<BoxCollider2D>().enabled = false;
                rb.velocity = new Vector2(rb.velocity.normalized.x * 5, rb.velocity.normalized.y * 5);
            }
            else if (other.gameObject.CompareTag("Ground") && !collionsFading && !attacking && !animator.GetBool("Death"))
            {
                flip();
            }
        }

        IEnumerator sturzflug()//Angriffssinkflug
        {
            collionsFading = false;
            animator.SetTrigger("Sturzflug");
            if (facingRight)
            {
                transform.Rotate(new Vector3(0, 0, -30));
            }
            else
            {
                transform.Rotate(new Vector3(0, 0, 30));
            }
            rb.velocity = new Vector2(speed * bewegungx, rb.velocity.y-6);
            kraehenSound.PlayDelayed(0);
            yield return new WaitForSeconds(0.5f);
            abflugSound.PlayDelayed(0);
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(abflug());
        }

        IEnumerator abflug()//Fliegt hoch außer Sichtweite und zerstört sich selbst
        {
            if (facingRight)
            {
                transform.Rotate(new Vector3(0, 0, 30));
            }
            else
            {
                transform.Rotate(new Vector3(0, 0, -30));
            }
            attacking = false;
            animator.SetTrigger("Flug");
            rb.velocity = new Vector2(speed * bewegungx, rb.velocity.y + 3);
            yFlug = 0.1f;
            abfliegen = true;
            yield return new WaitForSeconds(4f);
            collionsFading = true;
            //GetComponent<BoxCollider>().enabled=false;
            yield return new WaitForSeconds(8f);
            Destroy(gameObject);
        }
        private void flip()
        {
            // Switch the way the player is labelled as facing.
            facingRight = !facingRight;
            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
            bewegungx *= -1;
        }

        public void Hit(int damage, float x, float y)//der Schaden und Kraft in eine Richtung x y für einen Rückstoß
        {
            stats.health = stats.health - damage;
            rb.velocity = new Vector2(rb.velocity.y + x, rb.velocity.y + y);

        }
        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }
        private void Death()
        {
            Instantiate(deathSound, transform.position, Quaternion.identity);
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            StartCoroutine(GetComponent<AnimalDeathSequence>().Death());
        }
    }
}