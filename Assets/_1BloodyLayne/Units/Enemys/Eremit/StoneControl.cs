﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class StoneControl : MonoBehaviour
    {
        public Vector2 speed;
        Rigidbody2D rb;
        bool collision = false;
        GameObject Player;
        private PlatformerCharacter2D playerControl;
        public int attackDamage = 50;
        AudioSource hitSound;

        // Use this for initialization
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            if (speed.y > 17f)
            {
                speed.y = 17f;
            }else if(speed.y < 0)
            {
                speed.y = 5f;
            }
            if (speed.x > 20f)
            {
                speed.x = 20f;
            }
            rb.velocity = speed;
            StartCoroutine(Abschuss());
            Player = GameObject.FindWithTag("Player");
            playerControl = Player.GetComponent<PlatformerCharacter2D>();
            hitSound = GetComponent<AudioSource>();
            rb.velocity = new Vector2(speed.x, speed.y);
        }

        // Update is called once per frame
        void Update()
        {
        }

        private void FixedUpdate()
        {
            if (Physics2D.OverlapCircle(transform.position, 0.35f, 1 << 9) && collision == true)//layer Ebene 9 = Enemy
            {
                Destroy(gameObject); //zerstöre den Pfeil bevor er einen Enemy berührt, sonst verschiebt er die Figur
            }
            if (Physics2D.OverlapCircle(transform.position, 0.15f, 1 << 8) && collision == true)//layer Ebene 8 = Player
            {
                StartCoroutine(LarryHit());
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {

            if (collision == true)
            {
                if (other.gameObject.CompareTag("Enemy"))
                {
                    Destroy(gameObject);
                }
                if (other.gameObject.CompareTag("Object"))
                {
                    if (other.gameObject.GetComponent<ObjektStats>().destructable)
                    {
                        other.gameObject.GetComponent<ObjektStats>().hitPoints -= attackDamage;
                        if (other.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                        {
                            other.gameObject.GetComponent<ObjektStats>().startDeath();
                        }
                    }
                    Destroy(gameObject);
                }

                if (other.gameObject.CompareTag("Ground"))
                {
                    collision = false;
                }
            }
        }
        IEnumerator Abschuss()
        {
            yield return new WaitForSeconds(0.1f);
            collision = true;
        }
        IEnumerator LarryHit()
        {
            if (collision == true)
            {
                GetComponent<BoxCollider2D>().enabled = false;
                GetComponent<SpriteRenderer>().enabled = false;
                if(playerControl.Hit(attackDamage, DamageTypeToPlayer.BluntProjectile, transform.position, gameObject, Guid.NewGuid())){
                    hitSound.PlayDelayed(0);
                }
                yield return new WaitForSeconds(0.5f);
                Destroy(gameObject);
            }
        }
    }
}