﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Enemys;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Eremit : MonoBehaviour
    {
        EnemyStats stats;
        bool speermodus= false, rennt=false;
        private Transform player, steinabwurf, spearTip;
        public Transform endeLinks, endeRechts;
        public GameObject stone;
        Animator animator;
        AudioSource[] sounds;
        AudioSource runSound, throwSound, hitSound, switchSound, idleSound;
        Rigidbody2D rb;
        public Vector2 speed = new Vector2(19,0);
        private float dragStore, idleDrag = 30;
        private GlobalVariables gv;

        // Use this for initialization
        void Start()
        {
            player = GameObject.FindWithTag("Player").transform;
            spearTip = transform.Find("SpearTip");
            steinabwurf= transform.Find("StoneSpawn");
            stats = GetComponent<EnemyStats>();
            stats.isHuman = true;
            rb = GetComponent<Rigidbody2D>();
            sounds = GetComponents<AudioSource>();
            runSound = sounds[0];
            throwSound = sounds[1];
            hitSound = sounds[2];
            switchSound = sounds[3];
            idleSound = sounds[4];
            animator = GetComponent<Animator>();
            dragStore = rb.drag;
            rb.drag = idleDrag;
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            speed.x += gv.difficulty * 2;
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                speed *= -1;
                transform.localScale = theScale;
            }
            StartCoroutine(SteinIdle());
        }
        
        // Update is called once per frame
        void Update()
        {
            checkDeath();
            if (!animator.GetBool("Death"))
            {
                if (speermodus)
                {
                    if (Physics2D.OverlapCircle(spearTip.position, 0.85f, 1 << 8))
                    {
                        if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeStab"), DamageTypeToPlayer.MeleeStab, new Vector2(speed.x * 2, 0), spearTip.position, gameObject, Guid.NewGuid())){
                            hitSound.PlayDelayed(0);
                        }
                    }
                }
                if (rennt)
                {
                    if (stats.facingRight && !((endeRechts.position.x - 0.5f < transform.position.x) && (transform.position.x < endeRechts.position.x + 0.5f)))
                    {
                        rb.velocity = speed;
                    }
                    else if (!stats.facingRight && !((endeLinks.position.x - 0.5f < transform.position.x) && (transform.position.x < endeLinks.position.x + 0.5f)))
                    {
                        rb.velocity = speed;
                    }
                    else
                    {
                        rb.velocity = new Vector2(0, 0);
                        rennt = false;
                        runSound.Stop();
                        StartCoroutine(AttackIdle());
                    }
                }
            }
        }
        void FlipCheck()//z.B. in andere Richtung gucken
        {
            if (stats.facingRight && transform.position.x > player.position.x)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
            else if(!stats.facingRight && transform.position.x < player.position.x)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
        }

        IEnumerator SteinIdle()//Gibt Schussfrequenz und Animationen an
        {
            rb.drag = idleDrag;
            animator.SetTrigger("Idle");
            yield return new WaitForSeconds(0.45f);
            if (!animator.GetBool("Death"))
            {
                idleSound.PlayDelayed(0);
            }
            yield return new WaitForSeconds(0.05f);
            if (!animator.GetBool("Death"))
            {
                if ((Physics2D.OverlapCircle(transform.position, 10f, 1 << 8) && (transform.position.y < player.position.y + 3 || player.position.x > endeLinks.position.x && player.position.x < endeRechts.position.x)) || stats.health < stats.maxHealth && Physics2D.OverlapCircle(transform.position, 20f, 1 << 8))
                {
                    rb.drag = dragStore;
                    StartCoroutine(SteinThrow());
                }
                else
                {
                    if (rb.velocity.x < 1)
                    {
                        rb.velocity = new Vector2(0, rb.velocity.y);
                    }
                    StartCoroutine(SteinIdle());
                }
            }
        }

        IEnumerator SteinThrow()//Gibt Schussfrequenz und Animationen an
        {
            throwSound.PlayDelayed(0);
            FlipCheck();
            animator.SetTrigger("Throw");
            yield return new WaitForSeconds(0.3f);
            if (!animator.GetBool("Death"))
            {
                Vector2 steinSpeed = new Vector2(player.position.x - transform.position.x + 2, player.position.y - transform.position.y + 10.5f);
                if (Mathf.Abs(steinSpeed.x) > 20)
                {
                    steinSpeed = new Vector2(Mathf.Sign(steinSpeed.x) *  20, steinSpeed.y);
                }
                if (Mathf.Abs(steinSpeed.y) > 30)
                {
                    steinSpeed = new Vector2(steinSpeed.x, Mathf.Sign(steinSpeed.y) * 20);
                }
                if (stats.facingRight)
                {
                    GameObject stein = Instantiate(stone, steinabwurf.position, Quaternion.identity);
                    stein.GetComponent<StoneControl>().speed = steinSpeed;
                    stein.GetComponent<StoneControl>().attackDamage = stats.attacks.GetValueOrDefault("StoneThrow");
                }
                else
                {
                    GameObject stein = Instantiate(stone, steinabwurf.position, Quaternion.identity);
                    stein.GetComponent<StoneControl>().speed = new Vector2(player.position.x - transform.position.x - 2, steinSpeed.y);
                    stein.GetComponent<StoneControl>().attackDamage = stats.attacks.GetValueOrDefault("StoneThrow");
                }
            }
            yield return new WaitForSeconds(0.3f);

            if (!animator.GetBool("Death"))
            {
                FlipCheck();
                /*
                if (Physics2D.Raycast(transform.position, Vector2.left, transform.position.x - endeLinks.position.x, 1 << 8) && !facingRight || (Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 1), Vector2.left, transform.position.x - endeLinks.position.x + 5, 1 << 8)) && !facingRight)
                {
                    StartCoroutine(Speerswitch());
                }
                else if (Physics2D.Raycast(transform.position, Vector2.right, endeRechts.position.x - transform.position.x, 1 << 8) && facingRight || (Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 1), Vector2.right, endeRechts.position.x - transform.position.x + 5, 1 << 8)) && facingRight)
                {
                    StartCoroutine(Speerswitch());
                }
                */
                if (player.position.x > endeLinks.position.x && player.position.x < endeRechts.position.x)
                {
                    StartCoroutine(Speerswitch());
                }
                else
                {
                    StartCoroutine(PickUpStone());
                }
            }
        }


        IEnumerator PickUpStone()//Gibt Schussfrequenz und Animationen an
        {
            switchSound.PlayDelayed(0);
            FlipCheck();
            animator.SetTrigger("PickUp");
            yield return new WaitForSeconds(0.7f);

            if (!animator.GetBool("Death"))
            {
                StartCoroutine(SteinIdle());
            }
        }

        IEnumerator Speerswitch()//Gibt Schussfrequenz und Animationen an
        {
            switchSound.PlayDelayed(0);
            FlipCheck();
            animator.SetTrigger("SwitchToSpear");
            yield return new WaitForSeconds(0.4f);

            if (!animator.GetBool("Death"))
            {
                speermodus = true;
                GameObject.Find("SpearBoxCollider").GetComponent<BoxCollider2D>().enabled = true;
                Attackrun();
            }
        }

        IEnumerator Stoneswitch()//Gibt Schussfrequenz und Animationen an
        {
            switchSound.PlayDelayed(0);
            FlipCheck();
            animator.SetTrigger("SwitchToStone");
            speermodus = false;
            GameObject.Find("SpearBoxCollider").GetComponent<BoxCollider2D>().enabled = false;
            yield return new WaitForSeconds(0.4f);

            if (!animator.GetBool("Death"))
            {
                StartCoroutine(PickUpStone());
            }
        }

        void Attackrun()//Gibt Schussfrequenz und Animationen an
        {
            runSound.PlayDelayed(0);
            animator.SetTrigger("Attack");
            rennt = true;
        }

        IEnumerator AttackIdle()//Gibt Schussfrequenz und Animationen an
        {
            animator.SetTrigger("AttackIdle");
            yield return new WaitForSeconds(0.15f);
            if (!animator.GetBool("Death"))
            {
                if (Physics2D.Raycast(transform.position, Vector2.left, transform.position.x-endeLinks.position.x+5, 1 << 8) && stats.facingRight || (Physics2D.Raycast(new Vector2(transform.position.x+5, transform.position.y-1), Vector2.left, transform.position.x - endeLinks.position.x+5, 1 << 8)) && stats.facingRight)
                {
                    FlipCheck();
                    Attackrun();
                }
                else if (Physics2D.Raycast(transform.position, Vector2.right, endeRechts.position.x - transform.position.x+5, 1 << 8) && !stats.facingRight || (Physics2D.Raycast(new Vector2(transform.position.x+5, transform.position.y - 1), Vector2.right, endeRechts.position.x - transform.position.x+5, 1 << 8)) && !stats.facingRight)
                {
                    FlipCheck();
                    Attackrun();
                }
                else if (player.position.x < endeLinks.position.x || player.position.x > endeRechts.position.x)
                {
                    StartCoroutine(Stoneswitch());
                }
                else
                {
                    StartCoroutine(AttackIdle());
                }
            }
        }


        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }
        private void Death()
        {
            stats.dead = true;
            speermodus = false;
            rennt = false;
            runSound.Stop();
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            rb.angularDrag = 0;
            rb.drag = 0.5f;
            GetComponent<HumanDeathSequence>().Death();
        }
    }
}