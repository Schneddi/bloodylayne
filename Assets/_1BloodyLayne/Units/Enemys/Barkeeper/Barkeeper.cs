﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Barkeeper : MonoBehaviour
    {
        EnemyStats stats;
        private Transform flascheSpawn, player;    // A position marking where to check if the player is grounded.
        public GameObject flasche; //Geschosse
        AudioSource idleLoopSound, alarmedSound, bottleThrowSound;
        Animator animator;
        Rigidbody2D rb;
        bool idle, activeShooting, playerNear;
        private float dragStore, idleDrag = 30;

        // Use this for initialization
        void Start()
        {
            stats = GetComponent<EnemyStats>();
            flascheSpawn = transform.Find("FlascheSpawn");
            player = GameObject.FindWithTag("Player").transform;
            idleLoopSound = GetComponents<AudioSource>()[0];
            idleLoopSound.PlayDelayed(0);
            alarmedSound = GetComponents<AudioSource>()[1];
            bottleThrowSound = GetComponents<AudioSource>()[2];
            animator = GetComponent<Animator>();
            rb = GetComponent<Rigidbody2D>();
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
            }
            idle = true;
            dragStore = rb.drag;
            rb.drag = idleDrag;
        }

        // Update is called once per frame

        void Update()
        {
            checkDeath();
            if (!stats.dead)
            {
                checkPlayerNear();
                if (idle && playerNear)
                {
                    if (larrySightCheck())
                    {
                        idleLoopSound.Stop();
                        StartCoroutine(Alarmed());
                    }
                }
            }
        }

        void flipCheck()
        {
            if ((stats.facingRight && player.transform.position.x < transform.position.x) || (!stats.facingRight && player.transform.position.x > transform.position.x))
            {
                flip();
            }
        }

        void flip()
        {
            if ((Mathf.Abs(transform.position.x - player.transform.position.x) > 1f) || (transform.position.y + 0.5f > player.transform.position.y))
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
            }
        }
        
        private void checkPlayerNear()
        {
            if (Physics2D.OverlapCircle(transform.position, 30f, 1<<8))
            {
                playerNear = true;
                rb.drag = dragStore;
            }
            else
            {
                rb.drag = idleDrag;
                playerNear = false;
            }
        }

        //check if the Player is visible to the unit or behind a wall
        bool larrySightCheck()
        {
            if (Physics2D.OverlapCircle(transform.position, 14f, 1 << 8) && transform.position.y - 3 < player.transform.position.y && ((Mathf.Abs(transform.position.x - player.transform.position.x) > 1f) || (transform.position.y + 0.5f > player.transform.position.y)))
            {
                Vector3 richtungLarry = player.transform.position - flascheSpawn.transform.position;
                RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtungLarry, 14f);
                foreach (RaycastHit2D hitter in hit)
                {
                    if (hitter.transform.CompareTag("Ground"))
                    {
                        break;
                    }
                    if (hitter.transform.CompareTag("Object"))
                    {
                        break;
                    }
                    if (hitter.transform.CompareTag("Player"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        void Throw()//erzeugt einen Pfeil am pfeilSpawn
        {
            bottleThrowSound.PlayDelayed(0);
            GameObject flascheChild = Instantiate(flasche, flascheSpawn.position, Quaternion.identity);
            flascheChild.GetComponent<FlascheControl>().attackDamage = stats.attacks.GetValueOrDefault("BottleThrow");
            if (stats.facingRight)
            {
                flascheChild.GetComponent<FlascheControl>().speed = new Vector2(player.position.x - transform.position.x + 5.5f, player.position.y - transform.position.y + 4.2f);
            }
            else
            {
                flascheChild.GetComponent<FlascheControl>().speed = new Vector2(player.position.x - transform.position.x - 5.5f, player.position.y - transform.position.y + 4.2f);
            }
        }
        IEnumerator Alarmed()//Gibt Schussfrequenz und Animationen an
        {
            if (idle)
            {
                idle = false;
                flipCheck();
                alarmedSound.PlayDelayed(0);
                animator.SetTrigger("Alert");
                yield return new WaitForSeconds(0.41f);
                if (!stats.dead)
                {
                    StartCoroutine(Throwing());
                }
            }
        }

        IEnumerator Throwing()//Gibt Schussfrequenz und Animationen an
        {
            flipCheck();
            if (larrySightCheck())
            {
                animator.SetTrigger("Throw");
                yield return new WaitForSeconds(0.25f);
                if (!stats.dead)
                {
                    Throw();
                }
            }
            yield return new WaitForSeconds(0.25f);
            if (!stats.dead)
            {
                StartCoroutine(Throwing());
            }
        }
        public void Hit(int damage, float x, float y)//der Schaden und Kraft in eine Richtung x y für einen Rückstoß
        {
            stats.health = stats.health - damage;
            rb.velocity = new Vector2(rb.velocity.y + x, rb.velocity.y + y);

        }
        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }
        private void Death()
        {
            stats.dead = true;
            animator.SetTrigger("Death");
            GetComponent<HumanDeathSequence>().Death();
        }
    }
}
