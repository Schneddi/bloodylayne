﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using Units.Enemys;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Ghost : MonoBehaviour
    {
        public GameObject firebolt, listenDialog1, finishDialog1, listenDialogEnd1, finishDialog2, listenDialogEnd2;
        public bool facingRight, backToSpawn, chasePlayer, attackingSide, attackingDown, flipCooldown;
        public GameObject deathParticle, deathSound;
        public Transform finishingPosition;
        
        private EnemyStats stats;
        private Animator animator;
        private Rigidbody2D rb;
        private Transform fireboltSpawnSide, fireboltSpawnDown, player;
        private AudioSource spawnSound, flyStartSound, flySoundLoop, touchSound, transformSound, transformSurpriseSound, fireboltSpawnSound, fireboltDoubleSpawnSound;
        private Dialog dialog;
        private bool waitForDialogFinishBeginning, waitForDialogFinishEnd1, waitForDialogFinishEnd2, sideCastingPosition, sideCasting, downCastingPosition, downCasting, playerDeath, goToFinishingPosition;
        private Vector2 spawnPosition, attackDirection;
        private float downCastingPositionModX;
        private Camera2DFollow cameraScript;
        private BossUI bossUI;
        private GameController gc;
        private GlobalVariables gv;

        private LightDimming m_lightDimming;
        // Start is called before the first frame update
        void Start()
        {
            gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            fireboltSpawnSide = transform.Find("FireboltSpawnSide");
            fireboltSpawnDown = transform.Find("FireboltSpawnDown");
            player = GameObject.FindWithTag("Player").transform;
            spawnSound = GetComponents<AudioSource>()[0];
            flyStartSound = GetComponents<AudioSource>()[1];
            flySoundLoop = GetComponents<AudioSource>()[2];
            touchSound = GetComponents<AudioSource>()[3];
            transformSound = GetComponents<AudioSource>()[4];
            transformSurpriseSound = GetComponents<AudioSource>()[5];
            fireboltSpawnSound = GetComponents<AudioSource>()[6];
            fireboltDoubleSpawnSound = GetComponents<AudioSource>()[7];
            animator = GetComponent<Animator>();
            rb = GetComponent<Rigidbody2D>();
            stats = GetComponent<EnemyStats>();
            firebolt.GetComponent<Firebolt>().attackDamage = stats.attacks.GetValueOrDefault("FireBolt");
            spawnPosition = transform.position;
            cameraScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
            m_lightDimming = GetComponentInChildren<LightDimming>();
            bossUI = GameObject.Find("BossUI").GetComponent<BossUI>();
            if (!facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
            }
            dialog = FindObjectOfType<Dialog>();
        }

        // Update is called once per frame
        void Update()
        {
            if (listenDialog1.activeInHierarchy)
            {
                waitForDialogFinishBeginning = true;
            }
            if (waitForDialogFinishBeginning)
            {
                if (dialog.dialogActive == false)
                {
                    chasePlayer = true;
                    animator.SetTrigger("Fly");
                    GameObject.FindGameObjectWithTag("MainCamera").GetComponents<AudioSource>()[0].PlayDelayed(0);
                    flyStartSound.PlayDelayed(0);
                    flySoundLoop.PlayDelayed(0);
                    waitForDialogFinishBeginning = false;
                }
            }

            if (listenDialogEnd1.activeInHierarchy)
            {
                waitForDialogFinishEnd1 = true;
            }
            if (waitForDialogFinishEnd1)
            {
                if (dialog.dialogActive == false)
                {
                    transformSound.PlayDelayed(0);
                    waitForDialogFinishEnd1 = false;
                    StartCoroutine(dialogEnding2());
                }
            }


            if (listenDialogEnd2.activeInHierarchy)
            {
                waitForDialogFinishEnd2 = true;
            }
            if (waitForDialogFinishEnd2)
            {
                if (dialog.dialogActive == false)
                {
                    waitForDialogFinishEnd2 = false;
                    StartCoroutine(Death());
                }
            }

            if (!playerDeath && player.GetComponent<PlayerStats>().health <= 0)
            {
                playerDeath = true;
                StartCoroutine(larryKill());
            }

            if (chasePlayer && !GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PauseMenu>().pause)
            {
                flipCheck();
                Vector2 direction = player.GetComponent<Rigidbody2D>().position - rb.position;

                float distanceToPosition = Vector2.Distance(player.position, transform.position);
                float speedModifierForward = 1.6f;
                speedModifierForward += gv.difficulty * 0.1f;
                float speedModifierHeight = 2f;
                speedModifierHeight += gv.difficulty * 0.1f;
                rb.velocity = new Vector2(rb.velocity.x + Time.deltaTime * (direction.normalized.x * Mathf.Pow(distanceToPosition,speedModifierForward)) + 0.15f * direction.normalized.x, rb.velocity.y + Time.deltaTime * (direction.normalized.y * Mathf.Pow(distanceToPosition, speedModifierHeight)) + 0.1f * direction.normalized.y);

                if (Physics2D.OverlapCircle(transform.position, 0.5f, 1 << 8) && player.GetComponent<PlayerStats>().health > 0 && !player.GetComponent<PlatformerCharacter2D>().invulnerable)
                {
                    player.GetComponent<PlatformerCharacter2D>().KillPlayer(DamageTypeToPlayer.GhostTouch, new Vector2(direction.normalized.x * 30, direction.normalized.y * 30));
                    touchSound.PlayDelayed(0);
                }
            }

            if (backToSpawn)
            {
                Vector2 richtung = spawnPosition - rb.position;
                float distanceToSpawn = Vector2.Distance(spawnPosition, transform.position);
                rb.velocity = new Vector2(richtung.normalized.x * distanceToSpawn * 3, richtung.normalized.y * distanceToSpawn * 3);
            }

            if (sideCastingPosition)
            {
                Vector2 richtung = new Vector2(player.position.x - 10, player.position.y) - rb.position;
                float distanceToPosition = Vector2.Distance(new Vector2(player.position.x - 10, player.position.y), transform.position);
                rb.velocity = new Vector2(richtung.normalized.x * distanceToPosition * 25, richtung.normalized.y * distanceToPosition * 25);
            }

            if (sideCasting)
            {
                transform.position = new Vector2(player.position.x - 10, player.position.y);
            }

            if (downCastingPosition)
            {
                Vector2 richtung = new Vector2(player.position.x, player.position.y + 6) - rb.position;
                float distanceToPosition = Vector2.Distance(new Vector2(player.position.x, player.position.y + 6), transform.position);
                rb.velocity = new Vector2(richtung.normalized.x * distanceToPosition * 25, richtung.normalized.y * distanceToPosition * 25);
            }

            if (downCasting)
            {
                transform.position = new Vector2(player.position.x + downCastingPositionModX, player.position.y + 6);
                if (facingRight)
                {
                    downCastingPositionModX += (Time.deltaTime * 4f);
                    if (downCastingPositionModX > 14)
                    {
                        flip();
                    }
                }
                else
                {
                    downCastingPositionModX -= (Time.deltaTime * 4f);
                    if (downCastingPositionModX < -5)
                    {
                        flip();
                    }
                }
            }

            if (goToFinishingPosition)
            {
                Vector2 richtung = new Vector2(finishingPosition.position.x, finishingPosition.position.y) - rb.position;
                float distanceToSpawn = Vector2.Distance(finishingPosition.position, transform.position);
                rb.velocity = new Vector2(richtung.normalized.x * distanceToSpawn * 25, richtung.normalized.y * distanceToSpawn * 25);
            }
        }
        
        void flipCheck()
        {
            if ((facingRight && player.transform.position.x < transform.position.x) || (!facingRight && player.transform.position.x > transform.position.x) && !flipCooldown)
            {
                StartCoroutine(initiateFlipCooldown());
                flip();
            }
        }

        void flip()
        {
            if ((Mathf.Abs(transform.position.x - player.transform.position.x) > 1f) || (transform.position.y + 0.5f > player.transform.position.y))
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                facingRight = !facingRight;
            }
        }

        public void spawnGhost()
        {
            animator.SetTrigger("Spawn");
            spawnSound.PlayDelayed(0);
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().BlockInput = true;
            StartCoroutine(dialogStart());
            m_lightDimming.startDimming = true;
        }

        IEnumerator dialogStart()
        {
            yield return new WaitForSeconds(2f);
            dialog.ZeigeBox();
        }

        IEnumerator dialogEnding1()
        {
            bossUI.activateBossUI();
            goToFinishingPosition = true;
            yield return new WaitForSeconds(2f);
            goToFinishingPosition = false;
            transform.position = finishingPosition.position;
            dialog.dialogBox = finishDialog1;
            dialog.ZeigeBox();
            flipCheck();
        }

        IEnumerator dialogEnding2()
        {
            flySoundLoop.Stop();
            transformSound.PlayDelayed(0);
            animator.SetTrigger("Transform");
            m_lightDimming.intensityAim = 0;
            m_lightDimming.speed = -1;
            m_lightDimming.startDimming = true;
            for (int i=0;i < 10; i++)
            {
                GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, GetComponent<SpriteRenderer>().color.a + 0.05f);
                yield return new WaitForSeconds(0.1f);
            }
            GetComponent<BoxCollider2D>().enabled = true;
            rb.gravityScale = 1;
            yield return new WaitForSeconds(0.25f);
            stats.health = 0;
            yield return new WaitForSeconds(0.75f);
            transformSurpriseSound.PlayDelayed(0);
            animator.SetTrigger("TransformSurprise");
            transformSurpriseSound.PlayDelayed(0);
            yield return new WaitForSeconds(1f);
            dialog.dialogBox = finishDialog2;
            dialog.ZeigeBox();
        }

        public void chaseMode()
        {
            attackingDown = false;
            attackingSide = false;
            sideCasting = false;
            downCasting = false;
            chasePlayer = true;
            animator.SetTrigger("Fly");
        }

        public void ladderMode()
        {
            StartCoroutine(ladderWaitForPostion());
        }

        IEnumerator ladderWaitForPostion()
        {
            attackingDown = false;
            attackingSide = true;
            downCasting = false;
            chasePlayer = false;
            sideCastingPosition = true;
            yield return new WaitForSeconds(1f);
            if (!facingRight)
            {
                flip();
            }
            sideCastingPosition = false;
            sideCasting = true;
            StartCoroutine(attackSideWays());
            attackDirection = new Vector2(12,15);
        }

        IEnumerator attackSideWays()
        {
            animator.SetTrigger("Attack");
            yield return new WaitForSeconds(0.3f);
            if (sideCasting)
            {
                fireboltSpawnSound.PlayDelayed(0);
                GameObject fireboldChild = Instantiate(firebolt, fireboltSpawnSide.position, Quaternion.identity);
                fireboldChild.GetComponent<Firebolt>().abschuss2(attackDirection.x, attackDirection.y);
                attackDirection = new Vector2(attackDirection.x, attackDirection.y - 7);
                if (attackDirection.y < -3)
                {
                    attackDirection.y = 15;
                }
            }
            yield return new WaitForSeconds(0.25f);
            if (sideCasting) {
                StartCoroutine(attackSideWays());
            }
        }

        public void showDownMode()
        {
            StartCoroutine(attackDownForPostion());
        }

        IEnumerator attackDownForPostion()
        {
            attackingDown = true;
            attackingSide = false;
            chasePlayer = false;
            sideCasting = false;
            downCastingPosition = true;
            yield return new WaitForSeconds(1f);
            if (!facingRight)
            {
                flip();
            }
            downCastingPosition = false;
            downCasting = true;
            StartCoroutine(attackSideWays());
            attackDirection = new Vector2(0, -12);
            StartCoroutine(attackDownwards());
        }

        IEnumerator attackDownwards()
        {
            animator.SetTrigger("AttackDown");
            yield return new WaitForSeconds(0.75f);
            if (downCasting)
            {
                fireboltDoubleSpawnSound.PlayDelayed(0);
                GameObject fireboldChild1 = Instantiate(firebolt, new Vector2(fireboltSpawnDown.position.x + 0.5f, fireboltSpawnDown.position.y), Quaternion.identity);
                fireboldChild1.GetComponent<Firebolt>().abschuss2(attackDirection.x, attackDirection.y);
                GameObject fireboldChild2 = Instantiate(firebolt, new Vector2(fireboltSpawnDown.position.x - 0.5f, fireboltSpawnDown.position.y), Quaternion.identity);
                fireboldChild2.GetComponent<Firebolt>().abschuss2(attackDirection.x, attackDirection.y);
                StartCoroutine(attackDownwards());
            }
        }

        IEnumerator initiateFlipCooldown()
        {
            flipCooldown = true;
            yield return new WaitForSeconds(0.5f);
            flipCooldown = false;
        }

        public void finish()
        {
            attackingDown = false;
            attackingSide = false;
            sideCasting = false;
            downCasting = false;
            chasePlayer = false;
            animator.SetTrigger("Idle");
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().BlockInput = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            StartCoroutine(dialogEnding1());
        }

        IEnumerator larryKill()
        {
            sideCasting = false;
            downCasting = false;
            chasePlayer = false;
            yield return new WaitForSeconds(1f);
            backToSpawn = true;
            yield return new WaitForSeconds(1f);
            backToSpawn = false;
            chasePlayer = true;
            playerDeath = false;
            animator.SetTrigger("Fly");
            flyStartSound.PlayDelayed(0);
        }

        IEnumerator Death()
        {
            gc.SetAchievement("BEAT_GHOST");
            bossUI.deactivateBossUI();
            stats.dead = true;
            GameObject deathKind = Instantiate(deathSound, transform.position, Quaternion.identity);
            deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
            animator.SetTrigger("Death");
            rb.freezeRotation = false;
            rb.constraints = rb.constraints & ~RigidbodyConstraints2D.FreezePositionX;
            yield return new WaitForSeconds(1f);
            cameraScript.FadeOutMusic();
            Instantiate(deathParticle, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
