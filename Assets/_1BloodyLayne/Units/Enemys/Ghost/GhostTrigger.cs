﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class GhostTrigger : MonoBehaviour
    {
        public GameObject ghost, chaseTrigger, ladderTrigger;
        public bool spawn, chase, ladder, top, finish;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                if (spawn)
                {
                    ghost.GetComponent<Ghost>().spawnGhost();
                    Destroy(gameObject);
                }
                if (chase && !ghost.GetComponent<Ghost>().chasePlayer)
                {
                    ghost.GetComponent<Ghost>().chaseMode();
                }
                if (ladder && !ghost.GetComponent<Ghost>().attackingSide)
                {
                    ghost.GetComponent<Ghost>().ladderMode();
                }
                if (top && !ghost.GetComponent<Ghost>().attackingDown)
                {
                    ghost.GetComponent<Ghost>().showDownMode();
                }
                if (finish)
                {
                    ghost.GetComponent<Ghost>().finish();
                    Destroy(gameObject);
                }
            }
        }
    }
}