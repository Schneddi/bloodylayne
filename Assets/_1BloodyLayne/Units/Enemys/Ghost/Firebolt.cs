﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    public class Firebolt : MonoBehaviour
    {
        GameObject Player;
        private PlatformerCharacter2D playerControl;
        public GameObject deathSound;
        Rigidbody2D rbLarry;
        Rigidbody2D rb;
        Vector2 richtung;
        public float speed = 15f;
        public int attackDamage = 35;
        private bool died;

        // Use this for initialization
        void Start()
        {
            //rb.velocity = Vector2.Scale(richtung.normalized, new Vector2(totalDuration,totalDuration).normalized);
            Destroy(gameObject, 14f);
        }

        // Update is called once per frame
        void Update()
        {
            if (!died)
            {
                if (Physics2D.OverlapCircle(transform.position, 0.3f, 1 << 11))
                {
                    Instantiate(deathSound, transform.position, Quaternion.identity);
                    StartCoroutine(Death());
                }
            }
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!died)
            {
                if (other.gameObject.CompareTag("Ground"))
                {
                    Instantiate(deathSound, transform.position, Quaternion.identity);
                    StartCoroutine(Death());
                }
                else if (other.gameObject.CompareTag("Player"))
                {
                    if (playerControl.Hit(attackDamage, DamageTypeToPlayer.Burned, transform.position, gameObject, Guid.NewGuid()))
                    {
                        Instantiate(deathSound, transform.position, Quaternion.identity);
                    }
                    StartCoroutine(Death());
                }
            }
        }
        public void abschuss1()
        {
            Player = GameObject.FindWithTag("Player");
            playerControl = Player.GetComponent<PlatformerCharacter2D>();
            rbLarry = Player.GetComponent<Rigidbody2D>();
            rb = GetComponent<Rigidbody2D>();
            richtung = rbLarry.position - rb.position;
            rb.velocity = richtung.normalized * speed;
            setRotation(richtung.x, richtung.y);
        }
        public void abschuss2(float x, float y)
        {
            Player = GameObject.FindWithTag("Player");
            playerControl = Player.GetComponent<PlatformerCharacter2D>();
            rb = GetComponent<Rigidbody2D>();
            richtung = new Vector2(x, y);
            rb.velocity = new Vector2(richtung.x, richtung.y);
            setRotation(x, y);
        }

        private void setRotation(float x, float y)
        {
            transform.rotation = Quaternion.LookRotation(Vector3.back, new Vector3(x, y, 0));
            transform.Rotate(new Vector3(0,0,180));
        }

        private IEnumerator Death()
        {
            died = true;
            ParticleSystem[] pSystems = GetComponentsInChildren<ParticleSystem>();
            GetComponent<BoxCollider2D>().enabled = false;
            foreach (ParticleSystem pSystem in pSystems)
            {
                pSystem.Stop();
            }
            GetComponentInChildren<UnityEngine.Rendering.Universal.Light2D>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            rb.velocity = Vector2.zero;
            yield return new WaitForSeconds(2f);
            Destroy(gameObject);
        }

    }
}
