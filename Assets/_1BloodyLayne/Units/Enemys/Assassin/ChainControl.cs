using System;
using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class ChainControl : MonoBehaviour
{
    public GameObject splatterEffect;
    public float speed = 10;
    
    private float xDirection;
    private Rigidbody2D rb;
    private Transform maxStrechPos;
    private Vector2 despawnPosition;
    private bool movingBack, playerAttached;
    private Assassin assassin;
    private AudioSource hitSound;
    private GameObject player;
    private PlatformerCharacter2D playerControl;
    private LineRenderer lineRenderer;
    
    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        hitSound = transform.Find("Sounds").GetComponents<AudioSource>()[2];
        player = GameObject.FindWithTag("Player");
        playerControl = player.GetComponent<PlatformerCharacter2D>();
        lineRenderer = GetComponent<LineRenderer>();
    }

    public void spawn(float xDirection, Assassin assassin, Transform maxStrechPos, Vector2 despawnPosition)
    {
        this.xDirection = xDirection;
        this.assassin = assassin;
        this.maxStrechPos = maxStrechPos;
        this.despawnPosition = despawnPosition;
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(xDirection * speed, 0);
        Vector3 newPosition = new Vector3(-Math.Abs(transform.localPosition.x) - 0.5f,0,0);
        lineRenderer.SetPosition(1, newPosition);
        if (playerAttached)
        {
            player.transform.position = transform.position;
        }
        if (Physics2D.OverlapCircle(transform.position, 1.2f, 1 << 8))
        {
            playerAttached = true;
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            player.GetComponent<Animator>().SetTrigger("AssassinChainAttach");
            float xAdjustment = 1;
            if (transform.parent.localScale.x > 0)
            {
                xAdjustment *= -1;
            }
            Vector2 newPlayerPosition = new Vector2(transform.position.x + xAdjustment, transform.position.y);
            player.transform.position = newPlayerPosition;
            Instantiate(splatterEffect);
            hitSound.PlayDelayed(0);
            if ((!playerControl.m_FacingRight && transform.parent.localScale.x < 0) || (playerControl.m_FacingRight && transform.parent.localScale.x > 0))
            {
                playerControl.Flip();
            }
            if (playerControl.crouchIn)
            {
                playerControl.StandUp();
            }
            assassin.GetComponent<EnemyStats>().unverwundbar = true;
            if (!movingBack)
            {
                moveBack();
            }
        }
        if (!movingBack)
        {
            if (xDirection > 0 && transform.position.x > maxStrechPos.position.x)
            {
                moveBack();
            }
            else if (xDirection < 0 && transform.position.x < maxStrechPos.position.x)
            {
                moveBack();
            }
        }
        else
        {
            if (xDirection > 0 && transform.position.x > despawnPosition.x)
            {
                despawn();
            }
            else if (xDirection < 0 && transform.position.x < despawnPosition.x)
            {
                despawn();
            }
        }
    }

    private void moveBack()
    {
        xDirection *= -2;
        movingBack = true;
    }

    private void despawn()
    {
        if (playerAttached)
        {
            player.transform.position = despawnPosition;
            player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            assassin.chainKill();
        }
        else
        {
            assassin.chainFail();
        }
        Destroy(gameObject);
    }
}
