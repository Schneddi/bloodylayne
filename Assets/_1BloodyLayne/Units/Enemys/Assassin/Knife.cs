﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Knife : MonoBehaviour
    {
        GameObject Player;
        private PlatformerCharacter2D Larry;
        public GameObject deathSound;
        Rigidbody2D rbLarry;
        Rigidbody2D rb;
        Vector2 richtung;
        public float speed = 15f;
        public int attackDamage = 35;

        // Use this for initialization
        void Start()
        {
            Destroy(gameObject, 14f);
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (Physics2D.OverlapCircle(transform.position, 0.3f, 1 << 11))
            {
                GameObject deathKind = Instantiate(deathSound, transform.position, Quaternion.identity);
                deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
                Destroy(gameObject);
            }
            else if (Physics2D.OverlapCircle(transform.position, 0.3f, 1 << 14))
            {
                GameObject deathKind = Instantiate(deathSound, transform.position, Quaternion.identity);
                deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
                Destroy(gameObject);
            }

        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Object"))
            {
                GameObject deathKind = Instantiate(deathSound, transform.position, Quaternion.identity);
                deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
                Destroy(gameObject);
            }
            else if (other.gameObject.CompareTag("Ground"))
            {
                GameObject deathKind = Instantiate(deathSound, transform.position, Quaternion.identity);
                deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
                Destroy(gameObject);
            }
            else if (other.gameObject.CompareTag("Player"))
            {
                if (Larry.Hit(attackDamage, DamageTypeToPlayer.KnifeThrow, transform.position, gameObject, Guid.NewGuid()))
                {
                    GameObject deathKind = Instantiate(deathSound, transform.position, Quaternion.identity);
                    deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
                }
                Destroy(gameObject);
            }
        }
        public void abschuss1()
        {
            Player = GameObject.FindWithTag("Player");
            Larry = Player.GetComponent<PlatformerCharacter2D>();
            rbLarry = Player.GetComponent<Rigidbody2D>();
            rb = GetComponent<Rigidbody2D>();
            richtung = rbLarry.position - rb.position;
            rb.velocity = richtung.normalized * speed;
        }
        public void abschuss2(float x, float y)
        {
            Player = GameObject.FindWithTag("Player");
            Larry = Player.GetComponent<PlatformerCharacter2D>();
            rb = GetComponent<Rigidbody2D>();
            richtung = new Vector2(x, y);
            rb.velocity = new Vector2(richtung.x, richtung.y);

            float singleStep = 1.0f * Time.deltaTime;
            richtung = new Vector2(-x, y);
            Vector3 newDirection = Vector3.RotateTowards(transform.up, richtung, singleStep, 0.0f);
            Quaternion q = Quaternion.LookRotation(newDirection);
            q.y = 0;
            q.x = 0;
            transform.rotation = q;
        }
        public void abschuss3(float x, float y)
        {
            Player = GameObject.FindWithTag("Player");
            Larry = Player.GetComponent<PlatformerCharacter2D>();
            rb = GetComponent<Rigidbody2D>();
            richtung = new Vector2(x, y);
            rb.velocity = new Vector2(richtung.x, richtung.y);
            
            float angle = (Mathf.Atan2(y, x) + 1.5f * Mathf.PI) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = rotation;
        }

    }
}
