﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class AssassinGitterDialogReaction : MonoBehaviour
    {
        public GameObject gitter, listenDialog;
        bool listen;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (listenDialog.activeSelf)
            {
                listen = true;
            }
            if (!listenDialog.activeSelf && listen)
            {
                gitter.GetComponent<GitterCastlestart>().hochziehen = true;
                Destroy(gameObject);
            }
        }
    }
}
