﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using Units.Enemys;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;
using Random = UnityEngine.Random;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Assassin : MonoBehaviour
    {
        public GameObject deathSoundMaskOff, unmaskedDeathHead, unmaskedDeathRightArm, unmaskedDeathRightLeg, unmaskedDeathLeftArm, unmaskedDeathLeftLeg;
        public Transform leftBottom, leftTop, rightBottom, rightTop, ropeDropHeight;
        public GameObject knife, bomb, round2Dialog, endDialog, takeOffMaskDialog, bossPickUp, rope, chain, splatterEffect, smallSplatterEffect;

        private GameObject chainStrikeArm;
        private string currentMode;
        private EnemyStats stats;
        private Animator animator;
        private Rigidbody2D rb;
        private Transform knifeSpawn, bombSpawn, player, ropeSpawn, chainSpawn, bottomCheck, layneMeleeKillPos;
        private AudioSource spawnSound, jumpSound, throwSound, dropBombSound, ceilingClimpSound, jumpDownSound, exhaustedLoopSound, exhaustedMaskOffLoopSound, takeOffMaskSound, chainSwingSound, daggerHitSound, syncKillStabSound;
        private Dialog dialog;
        private bool waitForDialogFinishBeginning, rightSide, waitForDialogFinishEnd, dieWithMaskOff, round2TransitionRdy, round2TransitionStarted;
        private int ceilingClimbs, round;
        private Camera2DFollow cameraScript;
        private PlatformerCharacter2D playerChar;
        private GameObject bossUI;
        private GameController gc;
        private Vector2 boxColliderStartSize;
        private GlobalVariables gv;
        
#pragma warning disable 0414
        private bool leftSide; //Saves when, assassin is left. we never use it, but would be nice to have if needed later
#pragma warning restore 0414
        
        // Start is called before the first frame update
        void Start()
        {
            gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
            knifeSpawn = transform.Find("KnifeSpawn");
            chainStrikeArm = transform.Find("AssassinChainStrikeArm").gameObject;
            bombSpawn = transform.Find("BombSpawn");
            ropeSpawn = transform.Find("RopeSpawn");
            chainSpawn = transform.Find("ChainSpawn");
            bottomCheck = transform.Find("BottomCheck");
            layneMeleeKillPos  = transform.Find("LayneMeleeKillPos");
            player = GameObject.FindWithTag("Player").transform;
            playerChar = player.GetComponent<PlatformerCharacter2D>();
            spawnSound = transform.Find("Sounds").GetComponents<AudioSource>()[0];
            jumpSound = transform.Find("Sounds").GetComponents<AudioSource>()[1];
            throwSound = transform.Find("Sounds").GetComponents<AudioSource>()[2];
            dropBombSound = transform.Find("Sounds").GetComponents<AudioSource>()[3];
            ceilingClimpSound = transform.Find("Sounds").GetComponents<AudioSource>()[4];
            jumpDownSound = transform.Find("Sounds").GetComponents<AudioSource>()[5];
            exhaustedLoopSound = transform.Find("Sounds").GetComponents<AudioSource>()[6];
            takeOffMaskSound = transform.Find("Sounds").GetComponents<AudioSource>()[7];
            exhaustedMaskOffLoopSound = transform.Find("Sounds").GetComponents<AudioSource>()[8];
            chainSwingSound = transform.Find("Sounds").GetComponents<AudioSource>()[9];
            daggerHitSound = transform.Find("Sounds").GetComponents<AudioSource>()[10];
            syncKillStabSound  = transform.Find("Sounds").GetComponents<AudioSource>()[11];
            animator = GetComponent<Animator>();
            rb = GetComponent<Rigidbody2D>();
            stats = GetComponent<EnemyStats>();
            boxColliderStartSize = GetComponent<BoxCollider2D>().size;
            rightSide = true;
            cameraScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            bossUI = GameObject.Find("BossUI");
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
            }
            spawnSound.PlayDelayed(0);
            dialog = FindObjectOfType<Dialog>();
            StartCoroutine(dialogStart());
        }

        // Update is called once per frame
        void Update()
        {
            checkDeath();
            if (waitForDialogFinishBeginning)
            {
                if (dialog.dialogActive == false)
                {
                    waitForDialogFinishBeginning = false;
                    playerChar.InitializeSkills(true);
                    GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().assassinEncountered = true;
                    round = 1;
                    StartCoroutine(jumpToWall());
                }
            }

            if (waitForDialogFinishEnd && takeOffMaskDialog.GetComponent<Dialogbox>().finished)
            {
                takeOffMaskSound.PlayDelayed(0);
            }

            if (waitForDialogFinishEnd)
            {
                if (dialog.dialogActive == false)
                {
                    waitForDialogFinishEnd = false;
                    stats.unverwundbar = false;
                    exhaustedMaskOffLoopSound.PlayDelayed(0);
                }
            }
        }

        void flipCheck()
        {
            if ((stats.facingRight && player.transform.position.x < transform.position.x) || (!stats.facingRight && player.transform.position.x > transform.position.x))
            {
                flip();
            }
        }

        void flip()
        {
            if ((Mathf.Abs(transform.position.x - player.transform.position.x) > 1f) || (transform.position.y + 0.5f > player.transform.position.y))
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
            }
        }

        IEnumerator throwWeapon()
        {
            round2TransitionRdy = false;
            currentMode = "Ground";
            yield return new WaitForSeconds(0.5f);
            int rnd = Random.Range(0, 2);
            if (!stats.dead && stats.health > 0)
            {
                flipCheck();
                if (round == 1)
                {
                    chainSwingSound.PlayDelayed(0);
                    animator.SetTrigger("ChainStrikeStart");
                    yield return new WaitForSeconds(0.5f);
                    chainStrikeArm.SetActive(true);
                    animator.SetTrigger("ChainStrikeLoop");
                    GameObject chainInstance = Instantiate(chain, chainSpawn.position, Quaternion.identity, transform);
                    float chainXDirection=1;
                    Transform maxStrechPos = rightBottom;
                    if (rightSide)
                    {
                        maxStrechPos = leftBottom;
                        chainXDirection *= -1;
                    }

                    chainInstance.GetComponent<ChainControl>().spawn(chainXDirection,this, maxStrechPos, layneMeleeKillPos.position);
                    chainInstance.GetComponent<ChainControl>().speed -= gv.difficulty * 0.5f;
                }
                else
                {
                    if (rnd == 0)
                    {
                        throwSound.PlayDelayed(0);
                        animator.SetTrigger("JumpThrowKnife");
                    }
                    else
                    {
                        dropBombSound.PlayDelayed(0);
                        animator.SetTrigger("JumpDropBombs");
                    }
                }
                yield return new WaitForSeconds(0.5f);
            }
            if (!stats.dead && stats.health > 0 && round == 2)
            {
                Vector2 attackDirection;
                if (stats.facingRight)
                {
                    attackDirection = new Vector2(15, 0);
                }
                else
                {
                    attackDirection = new Vector2(-12, 0);
                }
                if (rnd == 0)
                {
                    GameObject daggerChild = Instantiate(knife, knifeSpawn.position, Quaternion.identity);
                    daggerChild.GetComponent<Knife>().abschuss2(attackDirection.x, attackDirection.y);
                }
                else
                {
                    GameObject bombChild = Instantiate(bomb, new Vector3(bombSpawn.position.x + 0.5f, bombSpawn.position.y, bombSpawn.position.z), Quaternion.identity);
                    bombChild.GetComponent<BombeControl>().speed = attackDirection;
                    bombChild.GetComponent<BombeControl>().attackDamage = stats.attacks.GetValueOrDefault("BombAttack");
                    GameObject bombChild2 = Instantiate(bomb, new Vector3(bombSpawn.position.x - 0.5f, bombSpawn.position.y, bombSpawn.position.z), Quaternion.identity);
                    bombChild2.GetComponent<BombeControl>().speed = attackDirection;
                    bombChild.GetComponent<BombeControl>().attackDamage = stats.attacks.GetValueOrDefault("BombAttack");
                }
            }

            if (!stats.dead && round == 2)
            {
                StartCoroutine(jumpToWall());
            }
        }

        public void chainKill()
        {
            chainStrikeArm.SetActive(false);
            StartCoroutine(chainPlayerSyncKill());
        }
        
        public void chainFail()
        {
            chainStrikeArm.SetActive(false);
            StartCoroutine(jumpToWall());
        }

        private IEnumerator chainPlayerSyncKill()
        {
            animator.SetTrigger("ChainSyncKill");
            player.GetComponent<Animator>().SetTrigger("AssassinStab");
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            playerChar.GetComponent<PlayerStats>().health = 1;
            for(int i = 0; i < 4; i++)
            {
                syncKillStabSound.PlayDelayed(0);
                Instantiate(smallSplatterEffect, layneMeleeKillPos.position, Quaternion.identity);
                yield return new WaitForSeconds(0.25f);
            }
            Instantiate(splatterEffect, layneMeleeKillPos.position, Quaternion.identity);
            animator.SetTrigger("Idle");
            playerChar.KillPlayer(DamageTypeToPlayer.AssassinStab);
            playerChar.GetComponent<BoxCollider2D>().enabled = false;
            playerChar.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            playerChar.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            playerChar.transform.position = layneMeleeKillPos.position;
            yield return new WaitForSeconds(1.75f);
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            playerChar.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            stats.unverwundbar = false;
            yield return new WaitForSeconds(0.25f);
            StartCoroutine(jumpToWall());
        }

        private IEnumerator jumpToWall()
        {
            rb.gravityScale = 0;
            Vector2 jumpDirection;
            if (rightSide)
            {
                jumpDirection = rightBottom.position - transform.position;
                jumpDirection *= 3;
                rb.velocity = new Vector2(jumpDirection.x, jumpDirection.y);
            }
            else
            {
                jumpDirection = leftBottom.position - transform.position;
                jumpDirection *= 3;
                rb.velocity = new Vector2(jumpDirection.x, jumpDirection.y);
            }
            yield return new WaitForSeconds(0.25f);
            if (!stats.dead && stats.health > 0)
            {
                rb.velocity = new Vector2(0, 0);
                if (rightSide)
                {
                    transform.position = rightBottom.position;
                    if (stats.facingRight)
                    {
                        flip();
                    }
                }
                else
                {
                    transform.position = leftBottom.position;
                    if (!stats.facingRight)
                    {
                        flip();
                    }
                }
                jumpSound.PlayDelayed(0);
                animator.SetTrigger("WallJump");
            }
            yield return new WaitForSeconds(0.25f);
            if (!stats.dead && stats.health > 0)
            {
                StartCoroutine(throwWeaponWall());
            }
            else if (!stats.dead)
            {
                round2TransitionRdy = true;
            }
        }

        IEnumerator throwWeaponWall()
        {
            round2TransitionRdy = false;
            currentMode = "Wall";
            if (!stats.dead && stats.health > 0)
            {
                animator.SetTrigger("WallThrow");
                throwSound.PlayDelayed(0);
                yield return new WaitForSeconds(0.5f);
            }
            if (!stats.dead && stats.health > 0)
            {
                Vector2 attackDirection;
                attackDirection = player.transform.position - transform.position;
                attackDirection *= 1.5f;
                GameObject daggerChild = Instantiate(knife, knifeSpawn.position, Quaternion.identity);
                daggerChild.GetComponent<Knife>().abschuss3(attackDirection.x, attackDirection.y);
                daggerChild.GetComponent<Knife>().attackDamage = stats.attacks.GetValueOrDefault("KnifeThrow");
                yield return new WaitForSeconds(0.25f);
            }
            if (!stats.dead && stats.health > 0)
            {
                animator.SetTrigger("WallToCeilingJump");
                jumpSound.PlayDelayed(0);
                Vector2 jumpDirection;
                if (rightSide)
                {
                    jumpDirection = rightTop.position - transform.position;
                    jumpDirection *= 3;
                    rb.velocity = new Vector2(jumpDirection.x, jumpDirection.y);
                }
                else
                {
                    jumpDirection = leftTop.position - transform.position;
                    jumpDirection *= 3;
                    rb.velocity = new Vector2(jumpDirection.x, jumpDirection.y);
                }
                yield return new WaitForSeconds(0.5f);
            }
            if (!stats.dead && stats.health > 0)
            {
                rb.velocity = new Vector2(0, 0);
                if (rightSide)
                {
                    transform.position = rightTop.position;
                }
                else
                {
                    transform.position = leftTop.position;
                }
                StartCoroutine(ceilingClimbing());
            }
            else if (!stats.dead)
            {
                round2TransitionRdy = true;
            }
        }

        IEnumerator ceilingClimbing()
        {
            round2TransitionRdy = false;
            currentMode = "Ceiling";
            
            if (round == 1)
            {
                if (!stats.dead)
                {
                    animator.SetTrigger("CeilingMove");
                    ceilingClimpSound.PlayDelayed(0);
                    Vector2 direction = Vector2.down;
                    RaycastHit2D downRay;
                    float ceilingMiddle = rightTop.position.x - leftTop.position.x;
                    bool exhausedChecked = false;
                    bool ropeDropped = false;
                    bool exhausted = false;
                    
                    //while not on the other side of the ceiling
                    while(!rightSide && transform.position.x < rightTop.position.x || rightSide && transform.position.x > leftTop.position.x && !stats.dead)
                    {
                        if (rightSide)
                        {
                            rb.velocity = new Vector2(-15, 0);
                        }
                        else
                        {
                            rb.velocity = new Vector2(15, 0);
                        }

                        Vector3 position = transform.position;
                        position = new Vector3(position.x, rightTop.position.y, position.z);
                        transform.position = position;
                        //if middle was crossed check if assassin is exhausted
                        if (!exhausedChecked && (!rightSide && transform.position.x < ceilingMiddle || rightSide && transform.position.x > ceilingMiddle))
                        {
                            if (ceilingClimbs > 13)
                            {
                                exhausted = true;
                                StartCoroutine(dropExhausted());
                            }
                            ceilingClimbs++;
                            exhausedChecked = true;
                        }
                        if (!ropeDropped && !stats.dead && stats.health > 0)
                        {
                            downRay = Physics2D.Raycast(bottomCheck.position, direction, 999, 1<<8);
                            if (downRay.collider != null)
                            {
                                ropeDropped = true;
                                yield return StartCoroutine(ropeAttack());
                            }
                        }
                        yield return null;
                    }
                    //reached end of ceiling
                    ceilingClimpSound.Stop();
                    ceilingClimbs++;
                    if (rightSide)
                    {
                        rightSide = false;
                        leftSide = true;
                        transform.position = leftTop.position;
                    }
                    else
                    {
                        rightSide = true;
                        leftSide = false;
                        transform.position = rightTop.position;
                    }
                    if (ceilingClimbs > 4)
                    {
                        yield return StartCoroutine(gaspForAir());
                    }
                    
                    if (!stats.dead && stats.health > 0 && !exhausted)
                    {
                        jumpDownSound.PlayDelayed(0);
                        rb.gravityScale = 7;
                        StartCoroutine(throwWeapon());
                    }
                    else if (!stats.dead)
                    {
                        round2TransitionRdy = true;
                    }
                }
            }
            else
            {
                yield return (StartCoroutine(ceilingShakeOffBombs()));
                if (!stats.dead)
                {
                    animator.SetTrigger("CeilingMove");
                    ceilingClimpSound.PlayDelayed(0);
                    if (rightSide)
                    {
                        rb.velocity = new Vector2(-15, 0);
                    }
                    else
                    {
                        rb.velocity = new Vector2(15, 0);
                    }
                }

                yield return new WaitForSeconds(0.5f);
                if (!stats.dead && stats.health > 0)
                {
                    ceilingClimpSound.Stop();
                    ceilingClimbs++;
                    rb.velocity = new Vector2(0, 0);
                    yield return (StartCoroutine(ceilingShakeOffBombs()));
                }

                if (!stats.dead)
                {
                    if (ceilingClimbs < 20)
                    {
                        animator.SetTrigger("CeilingMove");
                        ceilingClimpSound.PlayDelayed(0);
                        if (rightSide)
                        {
                            rb.velocity = new Vector2(-15, 0);
                        }
                        else
                        {
                            rb.velocity = new Vector2(15, 0);
                        }

                        yield return new WaitForSeconds(0.5f);
                        if (!stats.dead)
                        {
                            if (rightSide)
                            {
                                rightSide = false;
                                leftSide = true;
                                transform.position = leftTop.position;
                            }
                            else
                            {
                                rightSide = true;
                                leftSide = false;
                                transform.position = rightTop.position;
                            }

                            rb.velocity = new Vector2(0, 0);
                            ceilingClimpSound.Stop();
                            yield return (StartCoroutine(ceilingShakeOffBombs()));
                        }
                        yield return new WaitForSeconds(1f);

                        if (ceilingClimbs > 5 && !stats.dead && stats.health > 0)
                        {
                            yield return StartCoroutine(gaspForAir());
                        }

                        if (!stats.dead)
                        {
                            jumpDownSound.PlayDelayed(0);
                            rb.gravityScale = 7;
                            StartCoroutine(throwWeapon());
                        }
                    }
                    else
                    {
                        StartCoroutine(dropExhausted());
                    }
                }
            }
        }

        IEnumerator ropeAttack()
        {
            ceilingClimpSound.Stop();
            animator.SetTrigger("DropAttack");
            GetComponent<BoxCollider2D>().size = new Vector2(boxColliderStartSize.x, 2.6f);
            spawnSound.PlayDelayed(0);
            GameObject ropeInstance = Instantiate(rope, ropeSpawn.position, Quaternion.identity, transform);
            jumpDownSound.PlayDelayed(0);
            rb.velocity = Vector2.zero;
            float dropStrength = 5;
            dropStrength += gv.difficulty;
            rb.gravityScale = dropStrength;
            while (transform.position.y > ropeDropHeight.position.y && !stats.dead && stats.health > 0)
            {
                if (Physics2D.OverlapCircle(bottomCheck.position, 1.2f, 1 << 8))
                {
                    if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("RopeDrop"), DamageTypeToPlayer.MeleeSwing, bottomCheck.position, gameObject, Guid.NewGuid()))
                    {
                        daggerHitSound.PlayDelayed(0);
                    }
                    break;
                }
                yield return null;
            }

            if (!stats.dead && stats.health > 0)
            {
                rb.velocity = Vector2.zero;
                rb.gravityScale = -7;
            }

            while (transform.position.y < rightTop.position.y && !stats.dead && stats.health > 0)
            {
                yield return null;
            }

            if (!stats.dead && stats.health > 0)
            {
                rb.velocity = Vector2.zero;
                rb.gravityScale = 0;
                Destroy(ropeInstance);
                animator.SetTrigger("CeilingMove");
                ceilingClimpSound.PlayDelayed(0);
                if (rightSide)
                {
                    rb.velocity = new Vector2(-15, 0);
                }
                else
                {
                    rb.velocity = new Vector2(15, 0);
                }
            }
        }

        IEnumerator gaspForAir()
        {
            animator.SetTrigger("CeilingExhausted");
            exhaustedLoopSound.PlayDelayed(0);
            yield return new WaitForSeconds(0.5f);
            if (!stats.dead)
            {
                for (int i = 5; i < ceilingClimbs; i++)
                {
                    rb.velocity = new Vector2(0, 0);
                    yield return new WaitForSeconds(0.25f);
                }
            }
            exhaustedLoopSound.Stop();
        }

        IEnumerator dropExhausted()
        {
            animator.SetTrigger("CeilingExhausted");
            exhaustedLoopSound.PlayDelayed(0);
            yield return new WaitForSeconds(5f);
            exhaustedLoopSound.Stop();
            animator.SetTrigger("ExhaustedFallDown");
            jumpDownSound.PlayDelayed(0);
            rb.gravityScale = 7;
            yield return new WaitForSeconds(1f);
            if (!stats.dead)
            {
                round = 3;
                stats.unverwundbar = true;
                bossUI.GetComponent<BossUI>().deactivateBossUI();
                dieWithMaskOff = true;
                rb.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
                flipCheck();
                cameraScript.FadeOutMusic();
                dialog.ZeigeBox(endDialog);
                waitForDialogFinishEnd = true;
            }
        } 

        IEnumerator ceilingShakeOffBombs()
        {
            animator.SetTrigger("BombShake");
            Vector2 attackDirection = new Vector2(0, -2);
            GameObject bombChild;
            for (int i = 0; i < 3; i++)
            {
                if (!stats.dead && stats.health > 0)
                {
                    yield return new WaitForSeconds(0.075f);
                    bombChild = Instantiate(bomb, new Vector3(bombSpawn.position.x + Random.Range(-0.5f,0.5f), bombSpawn.position.y, bombSpawn.position.z), Quaternion.identity);
                    bombChild.GetComponent<BombeControl>().speed = attackDirection;
                    bombChild.GetComponent<BombeControl>().attackDamage = stats.attacks.GetValueOrDefault("BombAttack");
                    dropBombSound.PlayDelayed(0);
                }
            }
            if (!stats.dead && stats.health > 0)
            {
                animator.SetTrigger("CeilingIdle");
            }
            yield return new WaitForSeconds(0.35f);
        }

        IEnumerator dialogRound2()
        {
            rb.velocity = Vector2.zero;
            if (!stats.dead)
            {
                player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            }
            yield return new WaitForSeconds(0.5f);
            if (!stats.dead)
            {
                if (currentMode == "Ground")
                {
                    round2Dialog.GetComponent<Dialogbox>().emotionAnimationName = "Speak";
                    round2Dialog.GetComponent<Dialogbox>().mouthClosedEmotionName = "Idle";
                }
                if (currentMode == "Wall")
                {
                    round2Dialog.GetComponent<Dialogbox>().emotionAnimationName = "WallSpeak";
                    round2Dialog.GetComponent<Dialogbox>().mouthClosedEmotionName = "WallSpeakIdle";
                }
                if (currentMode == "Ceiling")
                {
                    round2Dialog.GetComponent<Dialogbox>().emotionAnimationName = "CeilingSpeak";
                    round2Dialog.GetComponent<Dialogbox>().mouthClosedEmotionName = "CeilingSpeakIdle";
                }
                dialog.ZeigeBox(round2Dialog);
                while (dialog.dialogActive)
                {
                    yield return null;
                }
                
                
                round = 2;
                spawnBossPickup();
                stats.health = stats.maxHealth;
                stats.noBloodied = false;
                player.GetComponent<Platformer2DUserControl>().BlockInput = false;
                if (currentMode == "Ground")
                {
                    StartCoroutine(throwWeaponWall());
                }
                else if (currentMode == "Wall")
                {
                    StartCoroutine(ceilingClimbing());
                }
                else if (currentMode == "Ceiling")
                {
                    jumpDownSound.PlayDelayed(0);
                    rb.gravityScale = 7;
                    StartCoroutine(throwWeapon());
                }
            }
            else
            {
                player.GetComponent<Platformer2DUserControl>().BlockInput = false;
            }
        }

        IEnumerator dialogStart()
        {

            if (!stats.dead)
            {
                player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            }
            yield return new WaitForSeconds(0.5f);
            if (!stats.dead)
            {
                dialog.ZeigeBox();
                waitForDialogFinishBeginning = true;
            }
            else
            {
                player.GetComponent<Platformer2DUserControl>().BlockInput = false;
            }
        }
        
        private void spawnBossPickup()
        {
            GameObject bossPickUpInstance = Instantiate(bossPickUp, rightTop.position, Quaternion.identity);
            Vector2 bossPickUpPush = new Vector2(500, 50);
            float rotation = 50;
            if (!stats.facingRight)
            {
                bossPickUpPush.x *= -1;
                rotation *= -1;
            }
            bossPickUpInstance.GetComponent<BossPickup>().spawn(bossPickUpPush, rotation);
        }

        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                if (round == 1 && round2TransitionRdy && !round2TransitionStarted)
                {
                    round2TransitionStarted = true;
                    StartCoroutine(dialogRound2());
                }
                else if(round != 1)
                {
                    round = 3;
                    Death();
                }
            }
        }

        private void Death()
        {
            gc.SetAchievement("ASSASSIN_1");
            stats.dead = true;
            ceilingClimpSound.Stop();
            GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().assassinKilled = true;
            exhaustedLoopSound.Stop();
            exhaustedMaskOffLoopSound.Stop();
            animator.SetBool("Dead", true);
            if (!dieWithMaskOff)
            {
                animator.SetTrigger("Death");
            }
            else
            {
                animator.SetTrigger("DeathMaskOff");
                GetComponent<HumanDeathSequence>().deathSound = deathSoundMaskOff;
                GetComponent<HumanDeathSequence>().deathHead = unmaskedDeathHead;
                GetComponent<HumanDeathSequence>().deathRightArm = unmaskedDeathRightArm;
                GetComponent<HumanDeathSequence>().deathLeftArm = unmaskedDeathLeftArm;
                GetComponent<HumanDeathSequence>().deathRightLeg = unmaskedDeathRightLeg;
                GetComponent<HumanDeathSequence>().deathLeftLeg = unmaskedDeathLeftLeg;
            }
            GetComponent<HumanDeathSequence>().Death();
        }
    }
}
