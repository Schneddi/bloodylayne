using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodEyesControl : MonoBehaviour
{
    public bool trackLarry;
    
    private Transform left2, left1, right1, right2, player;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        left2 = transform.Find("Sensors").Find("Left2");
        left1 = transform.Find("Sensors").Find("Left1");
        right1 = transform.Find("Sensors").Find("Right1");
        right2 = transform.Find("Sensors").Find("Right2");
        player = GameObject.FindGameObjectWithTag("Player").transform;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (trackLarry)
        {
            if (player.position.x < left2.position.x)
            {
                if (!animator.GetCurrentAnimatorStateInfo(0).IsTag("LookLeft2"))
                {
                    animator.SetTrigger("LookLeft2");
                }
            }
            else if (player.position.x < left1.position.x)
            {
                if (!animator.GetCurrentAnimatorStateInfo(0).IsTag("LookLeft1"))
                {
                    animator.SetTrigger("LookLeft1");
                }
            }
            else if (player.position.x < right1.position.x)
            {
                if (!animator.GetCurrentAnimatorStateInfo(0).IsTag("Idle"))
                {
                    animator.SetTrigger("Idle");
                }
            }
            else if (player.position.x < right2.position.x)
            {

                if (!animator.GetCurrentAnimatorStateInfo(0).IsTag("LookRight1"))
                {
                    animator.SetTrigger("LookRight1");
                }
            }
            else
            {

                if (!animator.GetCurrentAnimatorStateInfo(0).IsTag("LookRight2"))
                {
                    animator.SetTrigger("LookRight2");
                }
            }
        }
    }
}
