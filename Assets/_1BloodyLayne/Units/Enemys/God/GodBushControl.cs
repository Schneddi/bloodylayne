using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class GodBushControl : MonoBehaviour
{
    public float speed, rotationStrength, damageRadius, maxDuration;
    public int attackDamage;
    public GameObject deathParticles;
    
    private Rigidbody2D rb;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rb.velocity.magnitude < 2)
        {
            rb.velocity = rb.velocity * speed;
        }

        if (Physics2D.OverlapCircle(transform.position, damageRadius, 1<<8))
        {
            Vector2 midwayPoint = (transform.position + GameObject.FindGameObjectWithTag("Player").transform.position)/2;
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().Hit(attackDamage, DamageTypeToPlayer.Burned, midwayPoint,gameObject, Guid.NewGuid());
            death();
        }
    }

    public void launch(Vector2 direction)
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = direction * speed;
        rb.angularVelocity = direction.magnitude * rotationStrength;
        StartCoroutine(deathCountdown());
    }

    private IEnumerator deathCountdown()
    {
        yield return new WaitForSeconds(maxDuration);
        death();
    }

    private void death()
    {
        Instantiate(deathParticles, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
