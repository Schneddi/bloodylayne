using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using Units.Enemys;
using Unity.Mathematics;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;
using Random = UnityEngine.Random;

public class DevEnd2Control : MonoBehaviour
{
    public GameObject colloseum, god, clouds, whiteScreen, startDialog, endingScreenAllKill, endingScreenNormal, backgroundClouds;
    public bool testmode;

    private AudioSource earthQuakeSound, destructionSound, colloseumFadeOutSound;
    private Camera2DFollow cameraRef;
    private Transform rubbleSpawn;
    private Dialog dialog;
    private string status = "";
    private Platformer2DUserControl userControl;
    private PlatformerCharacter2D larry;
    private Vector2 larryStartPos;
    private GlobalVariables gv;
    private BossUI bossUI;
    
    // Start is called before the first frame update
    void Start()
    {
        gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        userControl = GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>();
        larry = userControl.GetComponent<PlatformerCharacter2D>();
        userControl.BlockInput = true;
        cameraRef = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
        dialog = FindObjectOfType<Dialog>();
        larryStartPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        earthQuakeSound = GetComponents<AudioSource>()[0];
        destructionSound = GetComponents<AudioSource>()[1];
        colloseumFadeOutSound = GetComponents<AudioSource>()[2];
        bossUI = GameObject.Find("BossUI").GetComponent<BossUI>();
        if (!testmode)
        {
            StartCoroutine(startSequence());
        }
        else
        {
            StartCoroutine(quickStart());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (startDialog.activeSelf)
        {
            status = "startDialog";
        }
        if (status.Equals("startDialog") && !startDialog.activeSelf)
        {
            status = "round1FightWindUp";
            cameraRef.GetComponent<Camera2DFollow>().ShakeCamera(0.5f,1f, 60);
            whiteScreen.GetComponent<WhiteFadeScreen>().startFadeScreen();
            colloseumFadeOutSound.PlayDelayed(0);
        }

        if (status.Equals("round1FightWindUp") && whiteScreen.GetComponent<WhiteFadeScreen>().fullWhite)
        {
            status = "fightStarted";
            StartCoroutine(startFight());
        }

        if (god.GetComponent<EnemyStats>().dead && status.Equals("fightStarted") && !larry.dead)
        {
            StartCoroutine(endSequence());
        }

        if (status.Equals("EndSequenceWhiteScreen") && whiteScreen.GetComponent<WhiteFadeScreen>().fullWhite)
        {
            status = "endSequenceShowEnding";
            StartCoroutine(endSequenceShowEnding());
        }
    }

    private IEnumerator startSequence()
    {
        cameraRef.GetComponent<Camera2DFollow>().ShakeCamera(4f,0.5f, 60);
        earthQuakeSound.PlayDelayed(0);
        yield return new WaitForSeconds(4f);
        cameraRef.GetComponent<Camera2DFollow>().ShakeCamera(4f,1f, 30);
        colloseum.GetComponent<Animator>().SetTrigger("Destroy");
        destructionSound.PlayDelayed(0);
        yield return new WaitForSeconds(2f);
        god.GetComponent<Animator>().SetTrigger("OpenEyes");
        yield return new WaitForSeconds(2f);
        dialog.ZeigeBox(startDialog);
    }
    
    private IEnumerator quickStart()
    {
        cameraRef.GetComponent<Camera2DFollow>().ShakeCamera(4f,0.5f, 60);
        earthQuakeSound.PlayDelayed(0);
        colloseum.GetComponent<Animator>().SetTrigger("Destroy");
        god.GetComponent<Animator>().SetTrigger("OpenEyes");
        yield return new WaitForSeconds(1f);
        dialog.ZeigeBox(startDialog);
        status = "startDialog";
    }

    private IEnumerator startFight()
    {
        cameraRef.GetComponents<AudioSource>()[4].PlayDelayed(0);
        cameraRef.GetComponents<AudioSource>()[0].PlayDelayed(0);
        colloseum.SetActive(false);
        clouds.SetActive(true);
        god.GetComponent<GodEyesControl>().trackLarry = true;
        userControl.BlockInput = false;
        larry.InitializeSkills(true);
        yield return new WaitForSeconds(2.5f);
        larry.StopInvulnerable();
        bossUI.activateBossUI();
        god.GetComponent<GodFightControl>().startFight();
        yield return new WaitForSeconds(1.5f);
        bossUI.shiftRight();
    }

    private IEnumerator endSequence()
    {
        cameraRef.GetComponent<Camera2DFollow>().FadeOutMusic();
        status = "EndSequence";
        bossUI.deactivateBossUI();
        userControl.BlockInput = true;
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().NewInvulnerable(9999, false, true);
        GameObject.FindGameObjectWithTag("Player").transform.position = larryStartPos;
        yield return new WaitForSeconds(6);
        cameraRef.GetComponents<AudioSource>()[4].Stop();
        status = "EndSequenceWhiteScreen";
        colloseumFadeOutSound.PlayDelayed(0);
        whiteScreen.GetComponent<WhiteFadeScreen>().startFadeScreen();
    }

    private IEnumerator endSequenceShowEnding()
    {
        backgroundClouds.SetActive(false);
        colloseum.SetActive(true);
        colloseum.GetComponent<Animator>().SetTrigger("Destroyed");
        clouds.SetActive(false);
        god.SetActive(false);
        yield return new WaitForSeconds(5);
        if (gv.humansKilled >= gv.humansCount - 2)
        {
            endingScreenAllKill.GetComponent<EndingScreen>().startEndingScreen();
        }
        else
        {
            endingScreenNormal.GetComponent<EndingScreen>().startEndingScreen();
        }
    }
}
