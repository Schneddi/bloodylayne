using System;
using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;
using Random = UnityEngine.Random;

public class GodFightControl : MonoBehaviour
{
    public GameObject rightHand, leftHand, godEyeRay, godThunder, godLaser, godBush, godNail, bossPickUp;
    
    private GameObject player, leftRayChild, rightRayChild, spawnedEffects;
    private PlatformerCharacter2D larry;
    private string nextState;
    private int round;
    private GodEyesControl eyeControl;
    private EnemyStats stats;
    private Transform leftEye, rightEye, rayEndLeft1, rayEndLeft2, rayEndRight1, rayEndRight2, thunderSpawn1u1, thunderSpawn2u1, thunderSpawn3u1, thunderSpawn4u1, thunderSpawn5u1, thunderSpawn1u2, thunderSpawn2u2, thunderSpawn3u2, thunderSpawn4u2, burningBushSpawn1, burningBushSpawn2, nailSpawn1, nailSpawn2, right1, left1;
    private Animator animator;
    private AudioSource eyeRayStartSound, deathSound, eyeRayLoopSound, thunderLoop;
    private bool thunderAnimStart, thunderAnimEnd;
#pragma warning disable 0414
    private string state; //Saves what the hand is doing right now. Not used, but would be nice to have if needed later
#pragma warning restore 0414
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        eyeControl = GetComponent<GodEyesControl>();
        larry = player.GetComponent<PlatformerCharacter2D>();
        stats = GetComponent<EnemyStats>();
        spawnedEffects = transform.Find("SpawnedEffects").gameObject;
        leftEye = transform.Find("Sensors/LeftEye");
        rightEye = transform.Find("Sensors/RightEye");
        rayEndLeft1 = transform.Find("Sensors/RayEndLeft1");
        rayEndLeft2 = transform.Find("Sensors/RayEndLeft2");
        rayEndRight1 = transform.Find("Sensors/RayEndRight1");
        rayEndRight2 = transform.Find("Sensors/RayEndRight2");
        thunderSpawn1u1 = transform.Find("Sensors/ThunderSpawn1u1");
        thunderSpawn2u1 = transform.Find("Sensors/ThunderSpawn2u1");
        thunderSpawn3u1 = transform.Find("Sensors/ThunderSpawn3u1");
        thunderSpawn4u1 = transform.Find("Sensors/ThunderSpawn4u1");
        thunderSpawn5u1 = transform.Find("Sensors/ThunderSpawn5u1");
        thunderSpawn1u2 = transform.Find("Sensors/ThunderSpawn1u2");
        thunderSpawn2u2 = transform.Find("Sensors/ThunderSpawn2u2");
        thunderSpawn3u2 = transform.Find("Sensors/ThunderSpawn3u2");
        thunderSpawn4u2 = transform.Find("Sensors/ThunderSpawn4u2");
        burningBushSpawn1 = transform.Find("Sensors/BurningBushSpawn1");
        burningBushSpawn2 = transform.Find("Sensors/BurningBushSpawn2");
        nailSpawn1 = transform.Find("Sensors/NailSpawn1");
        nailSpawn2 = transform.Find("Sensors/NailSpawn2");
        right1 = transform.Find("Sensors/Right1");
        left1 = transform.Find("Sensors/Left1");
        animator = GetComponent<Animator>();
        eyeRayStartSound = GetComponents<AudioSource>()[0];
        deathSound = GetComponents<AudioSource>()[1];
        eyeRayLoopSound = GetComponents<AudioSource>()[2];
        thunderLoop = GetComponents<AudioSource>()[3];
        thunderLoop.volume = 0;
        godEyeRay.GetComponent<GodRay>().attackDamagePerSecond = stats.attacks.GetValueOrDefault("GodRay");
        godThunder.GetComponent<GodThunderControl>().attackDamage = stats.attacks.GetValueOrDefault("GodThunder");
        godLaser.GetComponent<Todesmagie>().attackDamage = stats.attacks.GetValueOrDefault("GodLaser");
        godBush.GetComponent<GodBushControl>().attackDamage = stats.attacks.GetValueOrDefault("GodBush");
        godNail.GetComponent<GodNailControl>().attackDamage = stats.attacks.GetValueOrDefault("GodNail");
    }

    // Update is called once per frame
    void Update()
    {
        if (stats.health <= 0 && round == 3 && eyeControl.trackLarry)
        {
            larry.NewInvulnerable(9999, false, true);
            eyeControl.trackLarry = false;
            animator.SetTrigger("DeathTrigger");
            animator.SetBool("Death", true);
            leftHand.GetComponent<GodHand>().stompCharges = 0;
            rightHand.GetComponent<GodHand>().stompCharges = 0;
        }
        
        if (thunderAnimStart && thunderLoop.volume < 1)
        {
            thunderLoop.volume  += Time.deltaTime * 1f / godThunder.GetComponent<GodThunderControl>().startEndDuration;
        }
        if (thunderAnimEnd && thunderLoop.volume > 0)
        {
            thunderLoop.volume -= Time.deltaTime * 1f / godThunder.GetComponent<GodThunderControl>().startEndDuration;
        }

        if (stats.health <= 0 && round == 1 && leftHand.GetComponent<GodHand>().damageState == 1)
        {
            leftHand.GetComponent<GodHand>().nextDamageState();
            rightHand.GetComponent<GodHand>().nextDamageState();
        }
        if (stats.health <= 0 && round == 2 && leftHand.GetComponent<GodHand>().damageState == 2)
        {
            leftHand.GetComponent<GodHand>().nextDamageState();
            rightHand.GetComponent<GodHand>().nextDamageState();
        }
    }

    private void selectNextState()
    {
        if (stats.health <= 0 && round == 1)
        {
            SpawnBossPickup();
            stats.health = stats.maxHealth;
            round = 2;
            nextState = "EyeLaser";
        }
        else if (stats.health <= 0 && round == 2)
        {
            SpawnBossPickup();
            stats.health = stats.maxHealth;
            round = 3;
            leftHand.GetComponent<GodHand>().stompCharges = 99999;
            rightHand.GetComponent<GodHand>().stompCharges = 99999;
            nextState = "ThunderVar";
        }
        else if (stats.health <= 0 && round == 3)
        {
            round = 4;
            leftHand.GetComponent<GodHand>().nextDamageState();
            rightHand.GetComponent<GodHand>().nextDamageState();
            leftHand.transform.position = nailSpawn2.position;
            rightHand.transform.position = nailSpawn1.position;
            nextState = "Death";
        }

        switch (nextState)
        {
            case "FistStomps":
                StartCoroutine(fistStomps());
                break;
            case "EyeRay":
                StartCoroutine(eyeRay());
                break;
            case "Thunder":
                StartCoroutine(thunder());
                break;
            case "EyeLaser":
                StartCoroutine(eyeLaser());
                break;
            case "HandClap":
                StartCoroutine(handClap());
                break;
            case "ThunderVar":
                StartCoroutine(thunderVar());
                break;
            case "BurningBush":
                StartCoroutine(burningBush());
                break;
            case "NailAttack":
                StartCoroutine(nailAttack());
                break;
            case "Death":
                death();
                break;
        }
    }

    private IEnumerator fistStomps()
    {
        state = "FistStomps";
        leftHand.GetComponent<GodHand>().stompCharges++;
        rightHand.GetComponent<GodHand>().stompCharges++;
        if (round == 1)
        {
            nextState = "EyeRay";
            if (stats.health > 0)
            {
                yield return new WaitForSeconds(3);
            }
        }
        if (round == 2)
        {
            nextState = "HandClap";
            if (stats.health > 0)
            {
                yield return new WaitForSeconds(1);
            }
        }
        selectNextState();
    }
    
    private IEnumerator eyeRay()
    { 
        eyeControl.trackLarry = false;
        state = "EyeRay";
        nextState = "Thunder";
        animator.SetTrigger("EyesGlow");
        eyeRayStartSound.PlayDelayed(0);
        Quaternion q = Quaternion.LookRotation(Vector3.forward, Vector3.right);
        leftRayChild = Instantiate(godEyeRay, leftEye.position, q);
        rightRayChild = Instantiate(godEyeRay, rightEye.position, q);
        if (player.transform.position.x < transform.position.x)
        {
            leftRayChild.GetComponent<GodRay>().finalLocationGoal = rayEndLeft1.gameObject;
            rightRayChild.GetComponent<GodRay>().finalLocationGoal = rayEndLeft2.gameObject;
        }
        else
        {
            leftRayChild.GetComponent<GodRay>().finalLocationGoal = rayEndRight1.gameObject;
            rightRayChild.GetComponent<GodRay>().finalLocationGoal = rayEndRight2.gameObject;
        }
        yield return new WaitForSeconds(0.25f);
        eyeRayLoopSound.PlayDelayed(0);
        yield return new WaitForSeconds(2.5f);
        eyeRayLoopSound.Stop();
        eyeControl.trackLarry = true;
        selectNextState();
    }
    
    private IEnumerator thunder()
    {
        state = "Thunder";
        nextState = "FistStomps";
        thunderLoop.volume = 0;
        thunderLoop.PlayDelayed(0);
        thunderAnimStart = true;
        Instantiate(godThunder, thunderSpawn1u1.position, Quaternion.identity);
        Instantiate(godThunder, thunderSpawn2u1.position, Quaternion.identity);
        Instantiate(godThunder, thunderSpawn3u1.position, Quaternion.identity);
        Instantiate(godThunder, thunderSpawn4u1.position, Quaternion.identity);
        Instantiate(godThunder, thunderSpawn5u1.position, Quaternion.identity);
        yield return new WaitForSeconds(1);
        thunderAnimStart = false;
        yield return new WaitForSeconds(2);
        if (stats.health > 0)
        {
            Instantiate(godThunder, thunderSpawn1u2.position, Quaternion.identity);
            Instantiate(godThunder, thunderSpawn2u2.position, Quaternion.identity);
            Instantiate(godThunder, thunderSpawn3u2.position, Quaternion.identity);
            Instantiate(godThunder, thunderSpawn4u2.position, Quaternion.identity);
            yield return new WaitForSeconds(3);
        }
        thunderAnimEnd = true;
        selectNextState();
        yield return new WaitForSeconds(1);
        thunderAnimEnd = false;
        thunderLoop.Stop();
    }

    private IEnumerator eyeLaser()
    {
        state = "EyeLaser";
        nextState = "FistStomps";
        eyeControl.trackLarry = false;
        animator.SetTrigger("EyesGlow");
        GameObject leftLaserChild;
        GameObject rightLaserChild;
        for (int i=0; i <8; i++)
        {
            if (stats.health > 0)
            {
                leftLaserChild = Instantiate(godLaser, leftEye.position, Quaternion.identity);
                leftLaserChild.GetComponent<Todesmagie>().abschuss1();
                yield return new WaitForSeconds(0.25f);
                rightLaserChild = Instantiate(godLaser, rightEye.position, Quaternion.identity);
                rightLaserChild.GetComponent<Todesmagie>().abschuss1();
                yield return new WaitForSeconds(0.25f);
            }
        }
        eyeControl.trackLarry = true;
        if (stats.health > 0)
        {
            yield return new WaitForSeconds(0.75f);
        }
        selectNextState();
    }

    private IEnumerator handClap()
    {
        state = "HandClap";
        nextState = "EyeLaser";
        leftHand.GetComponent<GodHand>().clapCharges++;
        rightHand.GetComponent<GodHand>().clapCharges++;
        yield return new WaitForSeconds(3f);
        selectNextState();
    }

    private IEnumerator thunderVar()
    {
        state = "ThunderVar";
        nextState = "BurningBush";
        thunderLoop.volume = 0;
        thunderLoop.PlayDelayed(0);
        thunderAnimStart = true;
        Instantiate(godThunder, thunderSpawn3u1.position, Quaternion.identity).transform.parent = spawnedEffects.transform;
        yield return new WaitForSeconds(1);
        thunderAnimStart = false;
        if (stats.health > 0)
        {
            Instantiate(godThunder, thunderSpawn2u2.position, Quaternion.identity).transform.parent =
                spawnedEffects.transform;
            Instantiate(godThunder, thunderSpawn3u2.position, Quaternion.identity).transform.parent =
                spawnedEffects.transform;
            yield return new WaitForSeconds(1);
        }
        if (stats.health > 0)
        {
            Instantiate(godThunder, thunderSpawn2u1.position, Quaternion.identity).transform.parent =
                spawnedEffects.transform;
            Instantiate(godThunder, thunderSpawn4u1.position, Quaternion.identity).transform.parent =
                spawnedEffects.transform;
            yield return new WaitForSeconds(2);
        }
        if (stats.health > 0)
        {

            Instantiate(godThunder, thunderSpawn1u1.position, Quaternion.identity).transform.parent =
                spawnedEffects.transform;
            Instantiate(godThunder, thunderSpawn5u1.position, Quaternion.identity).transform.parent =
                spawnedEffects.transform;
            yield return new WaitForSeconds(1);
        }
        if (stats.health > 0)
        {
            Instantiate(godThunder, thunderSpawn1u2.position, Quaternion.identity).transform.parent =
                spawnedEffects.transform;
            Instantiate(godThunder, thunderSpawn4u2.position, Quaternion.identity).transform.parent =
                spawnedEffects.transform;
            yield return new WaitForSeconds(3);
        }
        thunderAnimEnd = true;
        selectNextState();
        yield return new WaitForSeconds(1);
        thunderAnimEnd = false;
        thunderLoop.Stop();
    }

    private IEnumerator burningBush()
    {
        state = "BurningBush";
        nextState = "NailAttack";
        GodBushControl godBushChild = Instantiate(godBush, burningBushSpawn1.position, Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f))).GetComponent<GodBushControl>();
        godBushChild.launch(Vector2.right);
        godBushChild.transform.parent = spawnedEffects.transform;
        yield return new WaitForSeconds(1);
        if (stats.health > 0)
        {
            godBushChild =
                Instantiate(godBush, burningBushSpawn2.position,
                    Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f))).GetComponent<GodBushControl>();
            godBushChild.launch(Vector2.left);
            godBushChild.transform.parent = spawnedEffects.transform;
            yield return new WaitForSeconds(1);
        }
        selectNextState();
    }

    private IEnumerator nailAttack()
    {
        state = "NailAttack";
        nextState = "ThunderVar";
        yield return new WaitForSeconds(1);
        for (int i = 0; i < 3; i++)
        {
            if (stats.health > 0)
            {
                Instantiate(godNail, nailSpawn1.position, Quaternion.identity).transform.parent =
                    spawnedEffects.transform;
                yield return new WaitForSeconds(1f);
                if (stats.health > 0)
                {
                    Instantiate(godNail, nailSpawn2.position, Quaternion.identity).transform.parent =
                        spawnedEffects.transform;
                    yield return new WaitForSeconds(2.5f);
                }
            }
        }
        selectNextState();
    }

    public void startFight()
    {
        /*
         ---LastRoundTestMode---
         
        leftHand.GetComponent<GodHand>().nextDamageState();
        leftHand.GetComponent<GodHand>().nextDamageState();
        rightHand.GetComponent<GodHand>().nextDamageState();
        rightHand.GetComponent<GodHand>().nextDamageState();
        leftHand.GetComponent<GodHand>().stompCharges = 99999;
        rightHand.GetComponent<GodHand>().stompCharges = 99999;
        round = 3;
        nextState = ("ThunderVar");
        selectNextState();
        */
        
        
        round = 1;
        StartCoroutine(fistStomps());
    }

    private void SpawnBossPickup()
    {
        
        Transform spawnPosition;
        int randomSide = Random.Range(0, 1);
        if (randomSide == 0)
        {
            spawnPosition = right1;
        }
        else
        {
            spawnPosition = left1;
        }
        GameObject bossPickUpInstance = Instantiate(bossPickUp, spawnPosition.position, Quaternion.identity);
        Vector2 bossPickUpPush = new Vector2(500, 50);
        float rotation = 50;
        if (randomSide == 1)
        {
            bossPickUpPush.x *= -1;
            rotation *= -1;
        }
        bossPickUpInstance.GetComponent<BossPickup>().spawn(bossPickUpPush, rotation);
    }
    

    private void death()
    {
        animator.SetTrigger("DeathTrigger");
        deathSound.PlayDelayed(0);
        Destroy(spawnedEffects);
        thunderLoop.Stop();
        stats.dead = true;
    }
}
