using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using Units.Enemys;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;
using Random = UnityEngine.Random;

public class GodHand : MonoBehaviour
{
    public int stompCharges, clapCharges;
    public float stompCooldown, speed, damagerLengthDown, damagerLengthFront;
    public GameObject god, splatterEffect, otherHand;
    public bool touchingPlayer;
    public string state;

    public int damageState = 1;
    private float fadeTime;
    private Transform leftLimit, rightLimit, damageCheckDown, damageCheckFront, player;
    private bool stompOnCooldown, fadingIn, fadingOut, fallDown, clapping, showDamage;
    private Animator animator;
    private Rigidbody2D rb;
    private EnemyStats stats;
    private PlatformerCharacter2D playerChar;
    private Vector3 startPos;
    private AudioSource stompSound, clapSound, spawnSound;
    private Color fadeColor, fadeOldColor;
    private SpriteRenderer sRenderer;
    private BoxCollider2D boxCollider;
    private Collider2D col;
    private ContactFilter2D contactFilterCancel, contactFilterDamage, contactFilterOtherHand;
    private Vector2 startCollSize, startCollOffset;
    private GlobalVariables gv;
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerChar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
        leftLimit = transform.Find("LeftLimit");
        rightLimit = transform.Find("RightLimit");
        rightLimit.parent = transform.parent;
        leftLimit.parent = transform.parent;
        damageCheckDown = transform.Find("DamageCheckDown");
        damageCheckFront = transform.Find("DamageCheckFront");
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        stats = GetComponent<EnemyStats>();
        stompSound = GetComponents<AudioSource>()[0];
        clapSound = GetComponents<AudioSource>()[1];
        spawnSound = GetComponents<AudioSource>()[2];
        sRenderer = GetComponent<SpriteRenderer>();
        col = GetComponent<Collider2D>();
        sRenderer.color = new Color(1, 1, 1, 0);
        startPos = transform.position;
        state = "Idle";
        contactFilterCancel = new ContactFilter2D();
        contactFilterCancel.SetLayerMask(1 << 11 | 1<< 9 | 1<<10);
        contactFilterDamage = new ContactFilter2D();
        contactFilterDamage.SetLayerMask(1 << 8);
        contactFilterOtherHand = new ContactFilter2D();
        contactFilterOtherHand.SetLayerMask(1 << 9);
        boxCollider = GetComponent<BoxCollider2D>();
        startCollSize = boxCollider.size;
        startCollOffset = boxCollider.offset;
        gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        if (!stats.facingRight)
        {
            speed *= -1;
        }
    }

    private void FixedUpdate()
    {
        if (fallDown)
        {
            rb.velocity = new Vector2(0, -Math.Abs(speed));
        }
        if (clapping)
        {
            rb.velocity = new Vector2(speed, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (state.Equals("Idle"))
        {
            if (clapCharges > 0)
            {
                if (otherHand.GetComponent<GodHand>().state.Equals("Idle") || otherHand.GetComponent<GodHand>().state.Equals("Clapping") && !otherHand.GetComponent<GodHand>().state.Equals("Stomping"))
                {
                    StartCoroutine(startClap());
                }
            }
            else if (stompCharges > 0 && player.position.x > leftLimit.position.x && player.position.x < rightLimit.position.x && player.position.y > leftLimit.position.y && !stompOnCooldown && Mathf.Abs(player.transform.position.x - otherHand.transform.position.x) > 5 && !playerChar.dead && (!playerChar.invulnerable || gv.forcedInvulnerable))
            {
                StartCoroutine(stomp());
            }
        }

        if (fallDown)
        {
            Vector2 directionVector = Vector2.right;
            if (!stats.facingRight)
            {
                directionVector = Vector2.left;
            }
            if(Physics2D.Raycast(damageCheckDown.position, directionVector, damagerLengthDown, 1 << 8) && player.GetComponent<Animator>().GetBool("Ground"))
            {
                playerChar.Hit(stats.attacks.GetValueOrDefault("SmashAttack"), DamageTypeToPlayer.Smashed, player.position, gameObject, Guid.NewGuid());
                if (playerChar.GetComponent<PlayerStats>().health <= 0 && !playerChar.dead)
                {
                    Instantiate(splatterEffect, new Vector3(playerChar.GetComponent<Transform>().position.x, playerChar.GetComponent<Transform>().position.y - 1, playerChar.GetComponent<Transform>().position.z), Quaternion.identity);
                }
                StartCoroutine(stompEnd(0));
            }
            else if(Physics2D.OverlapCircle(damageCheckDown.position, 0.4f, 1<<11))
            {
                StartCoroutine(stompEnd(1));
            }
            else if(col.IsTouching(contactFilterDamage))
            {
                ContactPoint2D[] contacts = new ContactPoint2D[1];
                col.GetContacts(contacts);
                Vector2 contactPoint = contacts[0].point;
                playerChar.Hit(stats.attacks.GetValueOrDefault("SmashAttack"), DamageTypeToPlayer.Smashed, contactPoint, gameObject, Guid.NewGuid());
                if (playerChar.GetComponent<PlayerStats>().health <= 0 && !playerChar.dead)
                {
                    Instantiate(splatterEffect, new Vector3(playerChar.GetComponent<Transform>().position.x, playerChar.GetComponent<Transform>().position.y - 1, playerChar.GetComponent<Transform>().position.z), Quaternion.identity);
                }
                StartCoroutine(stompEnd(0));
            }
            else if(col.IsTouching(contactFilterCancel))
            {
                StartCoroutine(stompEnd(1));
            }
        }
        
        if (clapping)
        {
            Vector2 directionVector = Vector2.right * speed;
            if (!stats.facingRight)
            {
                directionVector = Vector2.left * speed;
            }
            rb.velocity = directionVector;
            if(col.IsTouching(contactFilterDamage))
            {
                touchingPlayer = true;
                if (stats.facingRight && otherHand.GetComponent<GodHand>().touchingPlayer)
                {
                    playerChar.KillPlayer(DamageTypeToPlayer.ClappingHands);
                    Instantiate(splatterEffect, new Vector3(playerChar.GetComponent<Transform>().position.x, playerChar.GetComponent<Transform>().position.y - 1, playerChar.GetComponent<Transform>().position.z), Quaternion.identity);
                    clapEnd();
                    otherHand.GetComponent<GodHand>().clapEnd();
                }
                
            }
            else if(col.IsTouching(contactFilterOtherHand) || Physics2D.OverlapCircle(damageCheckFront.position, 0.1f, 1<<9) && !Physics2D.Raycast(damageCheckFront.position, Vector2.down, damagerLengthFront, 1 << 8) && !touchingPlayer)
            {
                clapEnd();
                otherHand.GetComponent<GodHand>().clapEnd();
            }
            else
            {
                touchingPlayer = false;
            }
        }

        if (fadingIn)
        {
            fadeTime += Time.deltaTime * 2;
            sRenderer.color = new Color(1f, 1, 1, fadeTime);
            if (fadeTime >= 1f)
            {
                sRenderer.color = new Color(1f, 1, 1, 1);
                fadingIn = false;
            }
        }
        else if (fadingOut)
        {
            fadeTime -= Time.deltaTime;
            sRenderer.color = new Color(1f, 1, 1, fadeTime * 2);
            if (fadeTime <= 0)
            {
                sRenderer.color = new Color(1f, 1, 1, 0);
                fadingOut = false;
                transform.position = startPos;
                state = "Idle";
                rb.bodyType = RigidbodyType2D.Dynamic;
                rb.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
                boxCollider.size = startCollSize;
                boxCollider.offset = startCollOffset;
            }
        }

        if (stats.health < stats.maxHealth)
        {
            System.Guid guid = System.Guid.NewGuid();
            god.GetComponent<EnemyStats>().Hit(stats.maxHealth - stats.health, 0, guid, false);
            stats.health = stats.maxHealth;
        }

        if (showDamage)
        {
            if (sRenderer.color.a > 0.25f && state.Equals("Stomping"))
            {
                showDamageEffects();
            }
        }
    }

    private IEnumerator stomp()
    {
        state = "Stomping";
        stompCharges--;
        transform.position = new Vector3(player.position.x, player.position.y + 6, player.position.z);
        stompOnCooldown = true;
        if (damageState == 1)
        {
            animator.SetTrigger("FistStage1");
        }
        else if (damageState == 2)
        {
            animator.SetTrigger("FistStage2");
        }
        else
        {
            animator.SetTrigger("FistStage3");
        }
        spawnSound.PlayDelayed(0);
        fadingIn = true;
        fadeTime = 0;
        yield return new WaitForSeconds(0.5f);
        if (damageState < 4)
        {
            fallDown = true;
            yield return new WaitForSeconds(3);
        }
        if (fallDown && damageState < 4)
        {
            stompEnd(0);
        }
    }

    private IEnumerator stompEnd(float seconds)
    {
        stompSound.PlayDelayed(0);
        fallDown = false;
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().ShakeCamera(0.2f,1f, 60);
        yield return new WaitForSeconds(seconds);
        if (damageState < 4)
        {
            fadeTime = 1;
            fadingOut = true;
            StartCoroutine(startStompCooldown(stompCooldown));
        }
    }

    private IEnumerator startClap()
    {
        rb.bodyType = RigidbodyType2D.Kinematic;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionY;
        boxCollider.size = new Vector2(1.69f,5.59f);
        boxCollider.offset = new Vector2(0.25f,1.59f);
        state = "Clapping";
        clapCharges--;
        animator.SetTrigger("ClapStage2");
        spawnSound.PlayDelayed(0);
        fadingIn = true;
        fadeTime = 0;
        if (stats.facingRight)
        {
            transform.position = new Vector3(player.position.x - 10, player.position.y, player.position.z);
        }
        else
        {
            transform.position = new Vector3(player.position.x + 10, player.position.y, player.position.z);
        }
        yield return new WaitForSeconds(1);
        touchingPlayer = false;
        clapping = true;
    }

    public void clapEnd()
    {
        if (stats.facingRight)
        {
            clapSound.PlayDelayed(0);
        }
        rb.velocity = new Vector2(0, 0);
        fadingOut = true;
        fadeTime = 1;
        clapping = false;
    }

    private IEnumerator startStompCooldown(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        stompOnCooldown = false;
    }

    public void nextDamageState()
    {
        damageState++;
        
        if (damageState == 4)
        {
            StartCoroutine(death());
        }
        else
        {
            showDamage = true;
        }
    }

    private void showDamageEffects()
    {
        showDamage = false;
        if (!otherHand.GetComponent<GodHand>().state.Equals("Stomping"))
        {
            otherHand.GetComponent<GodHand>().showDamage = false;
        }
        for (int i = 0; i < 6; i++)
        {
            Vector3 randomPos = new Vector3(transform.position.x + Random.Range(-1.5f, 1.5f),
                transform.position.y + Random.Range(-1.5f, 1.5f), transform.position.z);
            Instantiate(splatterEffect, randomPos, quaternion.identity);
        }

        if (damageState == 2)
        {
            animator.SetTrigger("FistStage2");
        }
        if (damageState == 3)
        {
            clapCharges = 0;
            animator.SetTrigger("FistStage3");
        }
    }

    private IEnumerator death()
    {
        rb.velocity = new Vector2(0, -4);
        fallDown = false;
        fadingOut = false;
        fadingIn = true;
        yield return new WaitForSeconds(2);
        for (int i = 0; i < 6; i++)
        {
            Vector3 randomPos = new Vector3(transform.position.x + Random.Range(-1.5f, 1.5f),
                transform.position.y + Random.Range(-1.5f, 1.5f), transform.position.z);
            Instantiate(splatterEffect, randomPos, quaternion.identity);
        }
        Destroy(gameObject);
    }
}
