using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    public class GodThunderControl : MonoBehaviour
    {
        public int attackDamage;
        public float mainDuration, startEndDuration;
        
        private GameObject player;
        private BoxCollider2D col;
        private AudioSource thunderHitSound;
        private bool animStart, animEnd, doDamage;
        private UnityEngine.Rendering.Universal.Light2D lightRef;

        private void Start()
        {
            thunderHitSound = GetComponents<AudioSource>()[1];
            col = GetComponent<BoxCollider2D>();
            player = GameObject.FindWithTag("Player");
            animStart = true;
            lightRef = transform.Find("Lightning").GetComponent<UnityEngine.Rendering.Universal.Light2D>();
            StartCoroutine(mainTimeControl());
        }

        private void Update()
        {
            if (animStart)
            {
                lightRef.intensity += Time.deltaTime * 0.4f / startEndDuration;
            }
            if (animEnd && lightRef.intensity > 0)
            {
                lightRef.intensity -= Time.deltaTime * 1f / startEndDuration;
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                doThunderDamage();
            }
        }

        private IEnumerator mainTimeControl()
        {
            yield return new WaitForSeconds(startEndDuration);
            animStart = false;
            lightRef.intensity = 0.4f;
            doDamage = true;
            if (col.IsTouching(player.GetComponent<BoxCollider2D>()))
            {
                doThunderDamage();
            }
            yield return new WaitForSeconds(mainDuration);
            animEnd = true;
            doDamage = false;
            yield return new WaitForSeconds(startEndDuration);
            Destroy(gameObject);
        }

        private void doThunderDamage()
        {
            if (doDamage)
            {
                if (player.GetComponent<PlatformerCharacter2D>().Hit(attackDamage, DamageTypeToPlayer.Electrocuted, player.transform.position, gameObject, Guid.NewGuid()))
                {
                    thunderHitSound.PlayDelayed(0);
                }
            }
        }
    }
}