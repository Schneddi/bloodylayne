using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class GodRay : MonoBehaviour
    {
        public GameObject finalLocationGoal;
        public float rayDuration;
        public int attackDamagePerSecond;
        
        private Color fadeColor = new Color(1f, 1f, 1f, 0f);
        private GameObject player, lightBallsEffect;
        private bool tick, rotationActive;
        private Vector2 direction;
        private AudioSource hitSound;
        private SpriteRenderer sRenderer, lightBallsRenderer;
        private float rayTimer;
        private Guid guid = Guid.NewGuid();

        private Transform directionCheck;
        // Use this for initialization
        void Start()
        {
            lightBallsEffect = transform.Find("LightBalls").gameObject;
            player = GameObject.FindGameObjectWithTag("Player");
            hitSound = GetComponent<AudioSource>();
            sRenderer = GetComponent<SpriteRenderer>();
            lightBallsRenderer = lightBallsEffect.GetComponent<SpriteRenderer>();
            sRenderer.color = fadeColor;
            lightBallsRenderer.color = fadeColor;
            directionCheck = transform.Find("DirectionCheck");
            StartCoroutine(startDamage());
        }

        // Update is called once per frame
        private void Update()
        {
            direction = directionCheck.position - transform.position;
            if (sRenderer.color.a < 1)
            {
                fadeColor = new Color(1f, 1f, 0f, sRenderer.color.a + Time.deltaTime * 4f);
                sRenderer.color = fadeColor;
                lightBallsRenderer.color = fadeColor;
            }
            lightBallsEffect.transform.position = new Vector3(lightBallsEffect.transform.position.x + Time.deltaTime * 3.5f * direction.normalized.x, lightBallsEffect.transform.position.y + Time.deltaTime * 3.5f * direction.normalized.y, lightBallsEffect.transform.position.z);
            if (tick)
            {
                tick = false;
                if (Physics2D.Raycast(transform.position, direction.normalized, 30f, 1 << 8))
                {
                    if (!hitSound.isPlaying)
                    {
                        hitSound.PlayDelayed(0);
                    }
                    player.GetComponent<PlatformerCharacter2D>().HitBeam((int)(attackDamagePerSecond * Time.deltaTime), DamageTypeToPlayer.Burned,new Vector2(direction.normalized.x * 2, direction.normalized.y * 2) * Time.deltaTime, player.transform.position, gameObject, guid);
                }
                else
                {
                    hitSound.Stop();
                }
            }

            if (rotationActive)
            {
                rayTimer += Time.deltaTime;
                Quaternion startRotation = Quaternion.LookRotation(Vector3.forward, Vector3.right);
                Vector2 endDirection = finalLocationGoal.transform.position - transform.position;
                endDirection = Rotate(endDirection, 90);
                Quaternion endRotation = Quaternion.LookRotation(Vector3.forward, endDirection);
                transform.localRotation = Quaternion.Lerp(startRotation, endRotation, rayTimer / rayDuration);
                if (rayTimer >= rayDuration)
                {
                    rotationActive = false;
                    death();
                }
            }
            if (hitSound.isPlaying && player.GetComponent<Animator>().GetBool("Death"))
            {
                hitSound.Stop();
            }
        }
        IEnumerator Ticker()//fliegt in die Luft und schießt Todesmagie auf die Spielerkoordinaten
        {
            while (true)
            {
                tick = true;
                yield return new WaitForSeconds(0.05f);
            }
        }

        IEnumerator startDamage()//fliegt in die Luft und schießt Todesmagie auf die Spielerkoordinaten
        {
            yield return new WaitForSeconds(0.25f);
            rotationActive = true;
            StartCoroutine(Ticker());
        }
        
        public static Vector2 Rotate(Vector2 v, float degrees) {
            float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
            float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
         
            float tx = v.x;
            float ty = v.y;
            v.x = (cos * tx) - (sin * ty);
            v.y = (sin * tx) + (cos * ty);
            return v;
        }

        private void death()
        {
            Destroy(gameObject);
        }
    }
}
