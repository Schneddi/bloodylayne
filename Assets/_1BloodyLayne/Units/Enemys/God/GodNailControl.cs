using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class GodNailControl : MonoBehaviour
{
    public int attackDamage;
    public float speed, startDuration, launchDelay;
    public GameObject splatterEffect;
    
    private bool fadingIn, fadingOut, rotating, launch;
    private float rotationTimer, fadeTime;
    private SpriteRenderer sRenderer;
    private Quaternion startRotation;
    private Transform player, damageCheck;
    private Rigidbody2D rb;
    private AudioSource hitSound, stuckSound, launchSound;
    private Vector2 endDirection;
    // Start is called before the first frame update
    void Start()
    {
        sRenderer = GetComponent<SpriteRenderer>();
        sRenderer.color = new Color(1f, 1, 1, 0);
        startRotation = transform.rotation;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
        damageCheck = transform.Find("DamageCheck");
        hitSound = GetComponents<AudioSource>()[1];
        stuckSound = GetComponents<AudioSource>()[2];
        launchSound = GetComponents<AudioSource>()[3];
        fadingIn = true;
        rotating = true;
        transform.localScale = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (fadingIn)
        {
            fadeTime += Time.deltaTime / startDuration;
            transform.localScale = new Vector3(fadeTime, fadeTime, fadeTime);
            sRenderer.color = new Color(1f, 1, 1, fadeTime);
            if (fadeTime >= 1f)
            {
                sRenderer.color = new Color(1f, 1, 1, 1);
                fadingIn = false;
            }
        }
        else if (fadingOut)
        {
            fadeTime -= Time.deltaTime;
            sRenderer.color = new Color(1f, 1, 1, fadeTime * 2);
            if (fadeTime <= 0)
            {
                sRenderer.color = new Color(1f, 1, 1, 0);
                fadingOut = false;
                Destroy(gameObject);
            }
        }

        if (rotating)
        {
            rotationTimer += Time.deltaTime;
            endDirection = transform.position - player.transform.position;
            Quaternion endRotation = Quaternion.LookRotation(Vector3.forward, endDirection);
            transform.localRotation = Quaternion.Lerp(startRotation, endRotation, rotationTimer / startDuration);
            if (rotationTimer >= startDuration)
            {
                StartCoroutine(launchNail());
            }
        }

        if (launch)
        {
            if (Physics2D.OverlapCircle(damageCheck.position, 2f, 1<<8))
            {
                if (player.GetComponent<PlatformerCharacter2D>().Hit(attackDamage, DamageTypeToPlayer.GodNailImpale, damageCheck.position, gameObject, Guid.NewGuid()))
                {
                    hitSound.PlayDelayed(0);
                    Instantiate(splatterEffect, damageCheck.position, quaternion.identity);
                }
                launch = false;
                fadingOut = true;
                fadeTime *= 0.25f;
                rb.velocity = rb.velocity/4;
                GetComponent<BoxCollider2D>().enabled = false;
            }
            else if (Physics2D.OverlapCircle(damageCheck.position, 2, 1<<11))
            {
                launch = false;
                StartCoroutine(stuckAndDie());
            }
        }
    }

    private IEnumerator launchNail()
    {
        endDirection = player.transform.position - transform.position;
        endDirection = endDirection.normalized;
        rotating = false;
        yield return new WaitForSeconds(launchDelay);
        rb.velocity = (endDirection * speed);
        launchSound.PlayDelayed(0);
        launch = true;
    }

    private IEnumerator stuckAndDie()
    {
        yield return new WaitForSeconds(0.1f);
        //gameObject.layer = 11;
        stuckSound.PlayDelayed(0);
        rb.velocity = Vector2.zero;
        yield return new WaitForSeconds(10);
        fadingOut = true;
    }
}
