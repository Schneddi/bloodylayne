using System;
using System.Collections;
using Units.Enemys;
using UnityEngine;
using Random = UnityEngine.Random;



public class HumanDeathSequence : MonoBehaviour
{
    public string specialDeathSpriteMode;
    public GameObject particle, particleHeal, deathSound, deathHead, deathRightArm, deathLeftArm, deathRightLeg, deathLeftLeg, bloodiedEffect, bloodiedEffectFront;
    public Action<HumanDeathSequence> DeathEvent;
    
    private Rigidbody2D rb;
    private EnemyStats stats;
    private bool playingDeathAnim, biteFade, bloodmagicFade, bloodiedFade, distort;
    private float deathStartTime, deathAnimDuration, bloodiedStartTime;
    private Vector3 startScale;
    private Material deathMaterial;
    private AudioSource[] dissolveSounds;
    private GameObject dissolveSoundsObject;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        stats = GetComponent<EnemyStats>();
        startScale = transform.localScale;
        if (specialDeathSpriteMode.Equals("BoneAnimated"))
        {
            deathMaterial = transform.Find("DeathSprite").GetComponent<SpriteRenderer>().material;
        }
        else
        {
            deathMaterial = GetComponent<SpriteRenderer>().material;
        }
        dissolveSoundsObject = GameObject.Find("DissolveSounds");
    }

    private void Update()
    {
        if (biteFade || bloodmagicFade)
        {
            float deathProgressPercent = (Time.time - deathStartTime) / deathAnimDuration;
            if (biteFade)
            {
                if (deathProgressPercent < 0.25f)
                {
                    deathMaterial.SetFloat("_BloodEffect", deathProgressPercent/0.25f);
                }
                if (deathProgressPercent > 0.75f)
                {
                    transform.localScale = new Vector3(startScale.x * (1.75f - deathProgressPercent), startScale.y * (1.75f - deathProgressPercent), startScale.z * (1.75f - deathProgressPercent));
                }
                if (deathProgressPercent * 5 < 5)
                {
                    deathMaterial.SetFloat("_DistortionStrength", deathProgressPercent * 5);
                }
                else
                {
                    deathMaterial.SetFloat("_DistortionStrength", 5);
                }
            }
            if (bloodmagicFade)
            {
                deathMaterial.SetFloat("_Fade", 1 - deathProgressPercent);
            }
        }
        if (bloodiedFade)
        {
            float bloodiedProgressPercent = (Time.time - bloodiedStartTime) / 1;
            if (bloodiedProgressPercent < 1)
            {
                deathMaterial.SetFloat("_Bloodied", bloodiedProgressPercent);
            }
            else
            {
                bloodiedFade = false;
                deathMaterial.SetFloat("_Bloodied", 1);
            }
        }
    }

    public void Death()
    {
        if (!playingDeathAnim)
        {
            DeathEvent?.Invoke(this);
            gameObject.layer = 17;
            playingDeathAnim = true;
            if (deathSound != null)
            {
                var deathKind = Instantiate(deathSound, transform.position, Quaternion.identity);
                deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
            }

            if (stats.lastDamageType == DamageTypeToEnemy.Bloodmagic) //bloodmagic Death
            {
                StartCoroutine(bloodmagicDeath());
            }
            else if (stats.lastDamageType == DamageTypeToEnemy.Bite) //bite Death
            {
                StartCoroutine(biteDeath());
            }
            else //splatter Death
            {
                StartCoroutine(splatterDeath());
            }
        }
    }

    private IEnumerator bloodmagicDeath()
    {
        yield return new WaitForSeconds(0.25f);
        GameObject dissolveInstance = Instantiate(dissolveSoundsObject, transform.position, Quaternion.identity);
        dissolveSounds = dissolveInstance.GetComponents<AudioSource>();
        int r = Random.Range(0, 3);
        dissolveSounds[r].PlayOneShot(dissolveSounds[r].clip);
        rb.gravityScale = 0;
        bloodmagicFade = true;
        deathStartTime = Time.time;
        deathAnimDuration = 1f;
        yield return new WaitForSeconds(deathAnimDuration);
        Destroy(gameObject);
    }

    private IEnumerator biteDeath()
    {
        startScale = transform.localScale;
        rb.gravityScale = 0;
        deathStartTime = Time.time;
        biteFade = true;
        deathAnimDuration = 0.6f;
        yield return new WaitForSeconds(deathAnimDuration * 0.5f);
        Instantiate(particleHeal, transform.position, Quaternion.identity);
        yield return new WaitForSeconds(deathAnimDuration * 0.5f);
        Destroy(gameObject);
    }

    private IEnumerator splatterDeath()
    {
        rb.freezeRotation = false;
        rb.constraints = RigidbodyConstraints2D.None;
        GetComponent<EnemyStats>().dead = true;
        yield return new WaitForSeconds(0.15f);
        Instantiate(particle, transform.position, transform.rotation);
        GameObject spawnedDeathHead = Instantiate(deathHead, transform.position, transform.rotation);
        GameObject spawnedDeathRightArm = Instantiate(deathRightArm, transform.position, transform.rotation);
        GameObject spawnedDeathLeftArm = Instantiate(deathLeftArm, transform.position, transform.rotation);
        GameObject spawnedDeathRightLeg = Instantiate(deathRightLeg, transform.position, transform.rotation);
        GameObject spawnedDeathLeftLeg = Instantiate(deathLeftLeg, transform.position, transform.rotation);
        if (stats.facingRight)
        {
            spawnedDeathHead.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * 50);
            spawnedDeathRightArm.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.left * 50);
            spawnedDeathLeftArm.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * 50);
            spawnedDeathRightLeg.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.left * Vector2.down * 50);
            spawnedDeathLeftLeg.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * Vector2.down * 50);
        }
        else
        {
            spawnedDeathHead.transform.localScale = new Vector3(spawnedDeathHead.transform.localScale.x * -1, spawnedDeathHead.transform.localScale.y, spawnedDeathHead.transform.localScale.z);
            spawnedDeathRightArm.transform.localScale = new Vector3(spawnedDeathRightArm.transform.localScale.x * -1, spawnedDeathRightArm.transform.localScale.y, spawnedDeathRightArm.transform.localScale.z);
            spawnedDeathLeftArm.transform.localScale = new Vector3(spawnedDeathLeftArm.transform.localScale.x * -1, spawnedDeathLeftArm.transform.localScale.y, spawnedDeathLeftArm.transform.localScale.z);
            spawnedDeathRightLeg.transform.localScale = new Vector3(spawnedDeathRightLeg.transform.localScale.x * -1, spawnedDeathRightLeg.transform.localScale.y, spawnedDeathRightLeg.transform.localScale.z);
            spawnedDeathLeftLeg.transform.localScale = new Vector3(spawnedDeathLeftLeg.transform.localScale.x * -1, spawnedDeathLeftLeg.transform.localScale.y, spawnedDeathLeftLeg.transform.localScale.z);
            spawnedDeathHead.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * 50);
            spawnedDeathRightArm.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * 50);
            spawnedDeathLeftArm.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.left * 50);
            spawnedDeathRightLeg.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * Vector2.down * 50);
            spawnedDeathLeftLeg.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.left * Vector2.down * 50);
        }
        Destroy(gameObject);
    }

    public void showBloodiedState()
    {
        Instantiate(bloodiedEffect, transform.position, Quaternion.identity, transform);
        Instantiate(bloodiedEffectFront, transform.position, Quaternion.identity, transform);
        
        //This shows a bloodiedState .gif over the texture. Disabled, since currently there is no good idea for a good bloodied .gif animation
        //bloodiedFade = true;
        //bloodiedStartTime = Time.time;
    }
}
