﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    public class BolzenControl : MonoBehaviour
    {
        public Vector2 speed;
        Rigidbody2D rb;
        bool collision = false;
        GameObject Player;
        private PlatformerCharacter2D Larry;
        public int attackDamage = 50;
        AudioSource hitSound;
        private Vector3 startPosition;

        // Use this for initialization
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            rb.velocity = speed;
            StartCoroutine(Abschuss());
            Player = GameObject.FindWithTag("Player");
            Larry = Player.GetComponent<PlatformerCharacter2D>();
            hitSound = GetComponent<AudioSource>();
            startPosition = transform.position;
        }

        private void FixedUpdate()
        {
            rb.velocity = new Vector2(speed.x, speed.y);
            if (Physics2D.OverlapCircle(transform.position, 0.35f, 1 << 9) && collision == true)//layer Ebene 9 = Enemy
            {
                Destroy(gameObject); //zerstöre den Pfeil bevor er einen Enemy berührt, sonst verschiebt er die Figur
            }
            if (Physics2D.OverlapCircle(transform.position, 0.35f, 1 << 11) && collision == true)//layer Ebene 1 = Boden
            {
                Destroy(gameObject); //zerstöre den Pfeil bevor er einen Enemy berührt, sonst verschiebt er die Figur
            }
            hitCheckCircle();
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                StartCoroutine(LarryHit());
            }
            if (collision == true)
            {
                if (other.gameObject.CompareTag("Enemy"))
                {
                    Destroy(gameObject);
                }
                if (other.gameObject.CompareTag("Object"))
                {
                    if (other.gameObject.GetComponent<ObjektStats>() != null)
                    {
                        if (other.gameObject.GetComponent<ObjektStats>().destructable)
                        {
                            other.gameObject.GetComponent<ObjektStats>().addDamage(attackDamage);
                        }
                    }
                    Destroy(gameObject);
                }
                Destroy(gameObject);
            }
        }
        IEnumerator Abschuss()
        {
            yield return new WaitForSeconds(0.06f);
            collision = true;
        }
        IEnumerator LarryHit()
        {
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            RaycastHit2D hit = Physics2D.Raycast(startPosition, transform.position - startPosition, Vector2.Distance(startPosition, transform.position), 1 << 10);
            if (hit.collider == null)
            {
                if (Larry.Hit(attackDamage, DamageTypeToPlayer.Arrow, transform.position, gameObject, Guid.NewGuid()))
                {
                    hitSound.PlayDelayed(0);
                }
            }
            yield return new WaitForSeconds(0.5f);
            Destroy(gameObject);
        }

        private void hitCheckCircle()
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.35f, 1 << 14);//layer Ebene 14 = Objects
            {
                foreach (Collider2D col in cols)
                {
                    if (col.gameObject.GetComponent<ObjektStats>() != null)
                    {
                        if (col.gameObject.GetComponent<ObjektStats>().destructable)
                        {
                            col.gameObject.GetComponent<ObjektStats>().addDamage(attackDamage);
                            Destroy(gameObject);
                        }
                    }
                }
            }
        }
    }
}