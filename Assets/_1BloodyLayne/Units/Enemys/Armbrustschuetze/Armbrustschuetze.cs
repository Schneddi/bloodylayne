﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Enemys;
using Units.Player;
using UnityEngine;
using UnityEngine.Serialization;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Armbrustschuetze : MonoBehaviour {

        EnemyStats stats;
        public int health = 100, maxHealth = 100, attackDamage = 35;
        [SerializeField] private LayerMask isPlayer;                  // A mask determining what is the Player
        [SerializeField] private float detectionRadius = 14;
        private Transform detector, pfeilSpawn, player, OberkoerperSpawn, ceilingCheck, cliffSensor;
        public GameObject pfeil, oberkoerper;
        public float speed, idleDrag = 30;
        GameObject upperBodyChild;
        AudioSource[] sounds;
        AudioSource pfeilSound, reload, topAttackSound, hitSound, runSound;
        Animator animator;
        Rigidbody2D rb;
        public bool active, activeShooting, noShooting;
        bool flipActive=true, rotateAim, running, aggro, onTop;
        Vector3 aimDirection;
        private float dragStore;
        private GlobalVariables gv;

        // Use this for initialization
        void Start()
        {
            stats = GetComponent<EnemyStats>();
            detector = transform.Find("Detector");
            OberkoerperSpawn = transform.Find("UpperBodySpawn");
            upperBodyChild = Instantiate(oberkoerper, OberkoerperSpawn.position, Quaternion.identity);
            upperBodyChild.transform.parent = gameObject.transform;
            pfeilSpawn = upperBodyChild.transform.Find("BolzenSpawn");
            ceilingCheck = transform.Find("CeilingCheck");
            cliffSensor = transform.Find("CliffSensor");
            player = GameObject.FindWithTag("Player").transform;
            //rb = GetComponent<Rigidbody2D>();
            sounds = GetComponents<AudioSource>();
            pfeilSound = sounds[0];
            reload = sounds[1];
            topAttackSound = GetComponents<AudioSource>()[2];
            hitSound = GetComponents<AudioSource>()[3];
            runSound = GetComponents<AudioSource>()[4];
            reload.enabled = false;
            animator = GetComponent<Animator>();
            rb = GetComponent<Rigidbody2D>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            aimDirection = new Vector3(1, 0, 0);
            speed += gv.difficulty * 60;
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                aimDirection.x *= -1;
                speed *= -1;
            }
            stats.health = health;
            stats.maxHealth = maxHealth;
            stats.isHuman = true;
            dragStore = rb.drag;
            rb.drag = idleDrag;
        }

        private void FixedUpdate()
        {
            checkDeath();
            if (!animator.GetBool("Death"))
            {
                if (!rotateAim && !running && !onTop && !activeShooting)
                {
                    if (Physics2D.OverlapCircle(ceilingCheck.position, 0.4f, 1 << 8))
                    {
                        StartCoroutine(TopAttack());
                    }
                    else if (Physics2D.OverlapCircle(detector.position, detectionRadius, isPlayer) && transform.position.y-3 < player.transform.position.y && ((Mathf.Abs(transform.position.x - player.transform.position.x) > 1f)||(transform.position.y + 0.5f > player.transform.position.y)))//ist der Spieler in der Nähe und in der höhe ds schuetzen oder höher? Dann fange an zu schießen!
                    {
                        Vector3 richtung = player.transform.position - detector.transform.position;
                        RaycastHit2D[] hit = Physics2D.RaycastAll(detector.position, richtung);
                        foreach (RaycastHit2D hitter in hit)
                        {
                            if (hitter.transform.tag == "Ground")
                            {
                                break;
                            }
                            if (hitter.transform.tag == "Player")
                            {
                                if (active == false)
                                {
                                    active = true;
                                    rb.drag = dragStore;
                                    rotateAim = true;
                                    aggro = true;
                                    StartCoroutine(Aiming());
                                }
                                break;
                            }
                        }

                        FlipCheck();
                    }
                    else if(aggro && Physics2D.OverlapCircle(detector.position, 40f, isPlayer) && !running)
                    {
                        if (upperBodyChild != null)
                        {
                            Destroy(upperBodyChild);
                            upperBodyChild = null;
                        }
                        animator.SetTrigger("Walk");
                        running = true;
                        runSound.PlayDelayed(0);
                    }
                }
                if (rotateAim)
                {
                    FlipCheck();
                    createUpperBody();

                    if(transform.position.y < player.transform.position.y + 3f && ((Mathf.Abs(transform.position.x - player.transform.position.x) > 1f) || (transform.position.y + 0.5f > player.transform.position.y)))
                    {
                        aimDirection = player.transform.position - transform.position;
                        Quaternion q = Quaternion.LookRotation(aimDirection);
                        q.y = 0;
                        q.x = 0;
                        upperBodyChild.transform.rotation = q;
                    }

                }
                else if (running)
                {
                    if (Physics2D.OverlapCircle(detector.position, 10f, isPlayer) && transform.position.y-3 < player.transform.position.y && ((Mathf.Abs(transform.position.x - player.transform.position.x) > 1f)||(transform.position.y + 0.5f > player.transform.position.y)))//ist der Spieler in der Nähe und in der höhe ds schuetzen oder höher? Dann fange an zu schießen!
                    {
                        running = false;
                        createUpperBody();
                        runSound.Stop();
                    }
                    else if(!Physics2D.OverlapCircle(detector.position, 40f, isPlayer) || !Physics2D.OverlapCircle(cliffSensor.position, 0.1f, 1<<11))
                    {
                        createUpperBody();
                        running = false;
                        runSound.Stop();
                        aggro = false;
                        rb.velocity = new Vector2(0,rb.velocity.y);
                    }
                    else
                    {
                        rb.AddForce(new Vector2(speed,0));
                    }

                    if (Math.Abs(detector.position.x - player.position.x) < 0.25f)
                    {
                        return; 
                    }
                    else
                    {
                        FlipCheck();
                    }
                }
            }
        }

        private void FlipCheck()
        {
            if ((detector.position.x > player.position.x) && stats.facingRight)
            {
                Flip();
            }
            else if ((detector.position.x < player.position.x) && !stats.facingRight)
            {
                Flip();
            }
        }
        
        void Flip()
        {
            if (flipActive && ((Mathf.Abs(transform.position.x - player.transform.position.x) > 1f) || (transform.position.y + 0.5f > player.transform.position.y))) {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                aimDirection.x *= -1;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
        }
        
        void Fire()
        {
            Quaternion q = Quaternion.LookRotation(aimDirection);
            q.y = 0;
            q.x = 0;
            aimDirection = aimDirection.normalized * 30f;
            GameObject pfeilKind = Instantiate(pfeil, pfeilSpawn.position, Quaternion.identity);
            pfeilKind.GetComponent<BolzenControl>().speed = new Vector2(aimDirection.x, aimDirection.y);
            pfeilKind.GetComponent<BolzenControl>().attackDamage = stats.attacks.GetValueOrDefault("BoltShot");
            pfeilKind.transform.rotation = q;
            pfeilKind.SetActive(true);
            if (!stats.facingRight)
            {
                Vector3 theScale = pfeilKind.transform.localScale;
                theScale.x *= -1;
                pfeilKind.transform.localScale = theScale;
            }
        }
        
        IEnumerator Aiming()
        {
            float aimingTime = 0.6f;
            aimingTime -= gv.difficulty * 0.1f;
            animator.SetTrigger("Attack");
            yield return new WaitForSeconds(aimingTime);
            if (animator.GetBool("Death")) yield break;
            
            if (Physics2D.OverlapCircle(pfeilSpawn.position, 0.2f, 1<<11))
            {
                rotateAim = false;
                aimDirection = player.transform.position - transform.position;
                Quaternion q = Quaternion.LookRotation(aimDirection);
                q.y = 0;
                q.x = 0;
                upperBodyChild.transform.rotation = q;
                flipActive = true;
                reload.enabled = false;
                animator.SetTrigger("Attack");
                active = false;
                rb.drag = idleDrag;
            }
            else if (active && !activeShooting)
            {
                StartCoroutine(Shooting());
            }
        }
        
        IEnumerator Shooting()
        {
            if (!animator.GetBool("Death") && active && !activeShooting && !noShooting)
            {
                activeShooting = true;
                createUpperBody();
                upperBodyChild.GetComponent<Animator>().SetTrigger("Attack");
                yield return new WaitForSeconds(0.1f);
                if (!animator.GetBool("Death"))
                {
                    Fire();
                    pfeilSound.PlayDelayed(0);
                    rotateAim = false;
                }
                yield return new WaitForSeconds(0.7f);
                if (!animator.GetBool("Death"))
                {
                    Destroy(upperBodyChild);
                    upperBodyChild = null;
                    flipActive = false;
                    animator.SetTrigger("Reload");
                    reload.enabled = true;
                }
                float reloadTime = 3.5f;
                reloadTime -= gv.difficulty * 0.5f;
                yield return new WaitForSeconds(reloadTime);
                if (!animator.GetBool("Death"))
                {
                    flipActive = true;
                    reload.enabled = false;
                    animator.SetTrigger("Loaded");
                    activeShooting = false;
                    active = false;
                    rb.drag = idleDrag;
                }
            }
        }

        private void createUpperBody()
        {
            if (upperBodyChild != null) return;
            
            animator.SetTrigger("Attack");
            upperBodyChild = Instantiate(oberkoerper, OberkoerperSpawn.position, Quaternion.identity);
            upperBodyChild.transform.parent = gameObject.transform;
            aimDirection = new Vector3(1,0,0);
            if (!stats.facingRight)
            {
                Vector3 theScale = upperBodyChild.transform.localScale;
                theScale.x *= -1;
                upperBodyChild.transform.localScale = theScale;
                aimDirection.x *= -1;
            }
            pfeilSpawn = upperBodyChild.transform.Find("BolzenSpawn");
        }
        
        IEnumerator TopAttack()
        {
            if (upperBodyChild != null)
            {
                Destroy(upperBodyChild);
                upperBodyChild = null;
            }
            onTop = true;
            rb.velocity = new Vector2(0, rb.velocity.y);
            animator.SetTrigger("TopAttack");
            topAttackSound.PlayDelayed(0);
            yield return new WaitForSeconds(1f);
            if (Physics2D.OverlapCircle(ceilingCheck.position, 0.4f, 1<<8))
            {
                if(player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("TopAttack"), DamageTypeToPlayer.MeleeFeetStab, new Vector2(0, 15f), ceilingCheck.position, gameObject, Guid.NewGuid()))
                {
                    hitSound.PlayDelayed(0);
                }
            }
            yield return new WaitForSeconds(1f);
            if (!animator.GetBool("Death"))
            {
                createUpperBody();
                onTop = false;
            }
        }
        
        public void Hit(int damage, float x, float y)
        {
            stats.health = stats.health - damage;
            rb.velocity = new Vector2(rb.velocity.y + x, rb.velocity.y + y);

        }
        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                StartCoroutine(Death());
            }
        }
        IEnumerator Death()
        {
            stats.dead = true;
            rotateAim = false;
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            reload.enabled = false;
            if (upperBodyChild != null)
            {
                Destroy(upperBodyChild);
                upperBodyChild = null;
            }
            yield return new WaitForSeconds(0.25f);
            if (upperBodyChild != null)
            {
                Destroy(upperBodyChild);
                upperBodyChild = null;
            }
            GetComponent<HumanDeathSequence>().Death();
        }
    }
}
