﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Priest : MonoBehaviour
    {
        EnemyStats stats;
        public int fleeHealth = 200;
        private bool fleeY = false, fleeX = false, underFleeHealth = false, stopAttack = false;
        bool idle=true;
        private Transform player, attackPoint;
        public Transform fleePosition;
        Animator animator;
        AudioSource attackSound, backswingSound, schluesselSound;
        Rigidbody2D rb;
        public float projectileSpeed = 6;
        public GameObject gottmagie, schluessel, schluesselEffekt;
        public GameObject holyStraceEffect;
        private GameObject holyStraceChild;

        // Use this for initialization
        void Start()
        {
            player = GameObject.FindWithTag("Player").transform;
            attackPoint = transform.Find("AttackPoint");
            stats = GetComponent<EnemyStats>();
            stats.isHuman = true;
            rb = GetComponent<Rigidbody2D>();
            attackSound = GetComponents<AudioSource>()[0];
            backswingSound = GetComponents<AudioSource>()[1];
            schluesselSound = GetComponents<AudioSource>()[2];
            animator = GetComponent<Animator>();
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                projectileSpeed *= -1;
            }
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (!animator.GetBool("Death"))
            {
                if (stats.health <= fleeHealth && underFleeHealth == false)
                {
                    stopAttack = true;
                    underFleeHealth = true;
                    fleeY = true;
                }
                if (fleeY)
                {
                    Instantiate(holyStraceEffect, transform.position, Quaternion.identity);
                    rb.velocity = new Vector3(1, 6, 0);
                    if (transform.position.y > fleePosition.transform.position.y)
                    {
                        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                        fleeX = true;
                        fleeY = false;
                    }
                }
                else if (fleeX)
                {
                    Instantiate(holyStraceEffect, transform.position, Quaternion.identity);
                    rb.velocity = new Vector3(12, 0.25f, 0);
                    if (transform.position.x > fleePosition.transform.position.x)
                    {
                        rb.velocity = new Vector3(0, 0, 0);
                        fleeX = false;
                        stopAttack = false;
                        rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
                    }
                }
            }
            checkDeath();
        }

        void Update()
        {
            if (!animator.GetBool("Death"))
            {
                if (Physics2D.OverlapCircle(transform.position, 20f, 1 << 8) && !(fleeX || fleeY))
                {
                    FlipCheck();
                    if (idle)
                    {
                        if (!stopAttack && Physics2D.OverlapCircle(transform.position, 15f, 1 << 8) && player.transform.position.y > transform.position.y - 2)
                        {
                            idle = false;
                            StartCoroutine(Attack());
                        }
                    }
                }
            }
        }
        void FlipCheck()//z.B. in andere Richtung gucken
        {
            if (stats.facingRight && transform.position.x > player.position.x)
            {
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                stats.facingRight = !stats.facingRight;
                projectileSpeed *= -1;
            }
            else if (!stats.facingRight && transform.position.x < player.position.x)
            {
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                stats.facingRight = !stats.facingRight;
                projectileSpeed *= -1;
            }
        }
        IEnumerator Attack()
        {
            animator.SetTrigger("Attack");
            yield return new WaitForSeconds(1f);
            if (!stopAttack)
            {
                    Vector3 projectileSpawnPositon = new Vector3(-attackPoint.transform.position.x, attackPoint.transform.position.y, attackPoint.transform.position.z);
                    GameObject gottmagieKugel = Instantiate(gottmagie, projectileSpawnPositon, Quaternion.identity);
                    gottmagieKugel.GetComponent<Todesmagie>().abschuss2(projectileSpeed, 0);
                    gottmagieKugel.GetComponent<Todesmagie>().attackDamage = stats.attacks.GetValueOrDefault("GodMagic");
                    gottmagieKugel = Instantiate(gottmagie, projectileSpawnPositon, Quaternion.identity);
                    gottmagieKugel.GetComponent<Todesmagie>().abschuss2(0, Mathf.Abs(projectileSpeed));
                    Vector2 richtung;
                for (int i = 0; i < 5; i++)
                {
                    yield return new WaitForSeconds(0.5f);
                    if (!stopAttack)
                    {
                        richtung = new Vector2(transform.position.x, transform.position.y);
                        richtung = richtung.normalized * 5;
                        gottmagieKugel = Instantiate(gottmagie, attackPoint.transform.position, Quaternion.identity);
                        gottmagieKugel.GetComponent<Todesmagie>().abschuss1();
                    }
                }
                if (!stopAttack)
                {
                    gottmagieKugel = Instantiate(gottmagie, attackPoint.transform.position, Quaternion.identity);
                    gottmagieKugel.GetComponent<Todesmagie>().abschuss2(projectileSpeed, 0);
                    gottmagieKugel = Instantiate(gottmagie, attackPoint.transform.position, Quaternion.identity);
                    gottmagieKugel.GetComponent<Todesmagie>().abschuss2(0, projectileSpeed);
                }
            }
            animator.SetTrigger("AttackBack");
            yield return new WaitForSeconds(2f);
            if (!animator.GetBool("Death"))
            {
                idle = true;
            }
        }

        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                stopAttack = true;
                Death();
            }
        }
        private void Death()
        {
            stats.dead = true;
            rb.freezeRotation = false;
            rb.constraints = rb.constraints & ~RigidbodyConstraints2D.FreezePositionX & ~RigidbodyConstraints2D.FreezePositionY;
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            if (schluessel != null)
            {
                schluesselSound.PlayDelayed(0);
                schluessel.SetActive(true);
                Instantiate(schluesselEffekt, schluessel.transform.position, Quaternion.identity);
            }
            GetComponent<HumanDeathSequence>().Death();
        }
    }
}