using System.Collections;
using UnityEngine;

namespace Units
{
    public class SpawnedSound : MonoBehaviour
    {
        private AudioSource _audioSource;
    
        // Start is called before the first frame update
        public void PlaySound(AudioClip audioClip)
        {
            _audioSource = GetComponent<AudioSource>();
            _audioSource.clip = audioClip;
            _audioSource.PlayOneShot(audioClip);
            StartCoroutine(DestroyAfterSound(audioClip.length));
        }
	
        IEnumerator DestroyAfterSound(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            Destroy(gameObject);
        }
    }
}
