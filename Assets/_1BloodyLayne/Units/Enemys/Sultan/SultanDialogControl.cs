using System;
using System.Collections;
using Units.Player;
using Unity.Mathematics;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class SultanDialogControl : MonoBehaviour
    {
        public GameObject sultan, player, guard, endingScreen, startDialog, endDialog, deathParticle;
        public GameObject bullet, smoke;
        public Dialogbox[] pistolEndings;
        public Dialogbox[] guardEndings;
        public Dialogbox endDialog1, endDialog2;
        public Dialog dialog;

        private AudioSource[] sounds;
        private AudioSource shotSound, executionSound, guardWalkingSound;
        private bool listen, guardWalking;
        private Transform bulletSpawn;
        private Rigidbody2D GuardRb;
        private bool standingUp;
        // Start is called before the first frame update
        void Awake()
        {
            bulletSpawn = sultan.transform.Find("BulletSpawn");
            sounds = GetComponents<AudioSource>();
            shotSound = sounds[0];
            executionSound = sounds[1];
            guardWalkingSound = sounds[2];
            GuardRb = guard.GetComponent<Rigidbody2D>();
            StartCoroutine(waitAndStartDialog());
        }

        // Update is called once per frame
        private void FixedUpdate()
        {

            if (guardWalking)
            {
                GuardRb.AddForce(new Vector2(300,0));
                if (Physics2D.OverlapCircle(player.transform.position, 1f, 1<<9))
                {
                    guardWalking = false;
                    GuardRb.velocity = new Vector2(0, 0);
                    StartCoroutine(playGuardEndingEnd());
                }
            }
        }

        void Update()
        {
            if (listen)
            {
                foreach (Dialogbox pistolEnding in pistolEndings)
                {
                    if (pistolEnding.finished)
                    {
                        listen = false;
                        StartCoroutine(playPistolEnding());
                    }
                }
                
                foreach (Dialogbox guardEnding in guardEndings)
                {
                    if (guardEnding.finished)
                    {
                        listen = false;
                        playGuardEndingStart();
                    }
                }
                if (endDialog1.finished && !standingUp)
                {
                    standingUp = true;
                    StartCoroutine(playHumanEndingDialog());
                }
                if (endDialog2.finished)
                {
                    listen = false;
                    StartCoroutine(playHumanFinalEnding());
                }
            }
        }

        IEnumerator playPistolEnding()
        {
            player.GetComponent<PlatformerCharacter2D>().InitializeHuman(2);
            sultan.GetComponent<Animator>().SetTrigger("Shoot");
            yield return new WaitForSeconds(0.666f);
            shotSound.PlayDelayed(0);
            GameObject bulletProjectile = Instantiate(bullet, bulletSpawn.position, Quaternion.identity);
            bulletProjectile.GetComponent<BulletControl>().speed = new Vector2(-60, 0);
            Instantiate(smoke, bulletSpawn.position, Quaternion.identity);
            player.GetComponent<PlatformerCharacter2D>().ForcedHit(9999, DamageTypeToPlayer.Bullet, player.transform.position, gameObject, new Guid());
            yield return new WaitForSeconds(5f);
        }
        
        public void playGuardEndingStart()
        {
            player.GetComponent<PlatformerCharacter2D>().InitializeHuman(2);
            guard.GetComponent<Animator>().SetTrigger("SwordWalk");
            guardWalking = true;
            guardWalkingSound.PlayDelayed(0);
        }
        
        IEnumerator playGuardEndingEnd()
        {
            guardWalkingSound.Stop();
            guardWalking = false;
            guard.GetComponent<Animator>().SetTrigger("Execute");
            executionSound.PlayDelayed(0);
            yield return new WaitForSeconds(0.5f);
            Instantiate(deathParticle, player.transform.position, quaternion.identity);
            player.GetComponent<PlatformerCharacter2D>().ForcedHit(9999, DamageTypeToPlayer.MeleeStab, player.transform.position, gameObject, new Guid());
        }
        
        IEnumerator waitAndStartDialog()
        {
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            yield return new WaitForSeconds(1);
            listen = true;
            dialog.ZeigeBox(startDialog);
        }
        
        IEnumerator playHumanEndingDialog()
        {
            yield return new WaitForSeconds(1);
            dialog.ZeigeBox(endDialog);
        }
        
        IEnumerator playHumanFinalEnding()
        {
            yield return new WaitForSeconds(1);
            endingScreen.GetComponent<EndingScreen>().startEndingScreen();
        }
    }
}
