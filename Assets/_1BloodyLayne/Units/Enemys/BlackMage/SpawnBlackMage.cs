﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class SpawnBlackMage : MonoBehaviour
    {
        public GameObject blackMage;
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                blackMage.GetComponent<BlackMage>().spawn();
                Destroy(gameObject);
            }
        }
    }
}
