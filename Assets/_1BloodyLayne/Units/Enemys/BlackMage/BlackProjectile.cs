﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class BlackProjectile : MonoBehaviour
    {
        GameObject Player;
        private PlatformerCharacter2D Larry;
        public GameObject deathSound;
        Rigidbody2D rbLarry;
        Rigidbody2D rb;
        Vector2 richtung;
        public float speed = 15f;
        float angle;
        public int attackDamage = 35;

        // Use this for initialization
        void Start()
        {
            //rb.velocity = Vector2.Scale(richtung.normalized, new Vector2(totalDuration,totalDuration).normalized);
            Destroy(gameObject, 14f);
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (Physics2D.OverlapCircle(transform.position, 0.3f, 1 << 11))
            {
                Destroy(gameObject);
            }

        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Ground"))
            {
                Destroy(gameObject);
            }
            else if (other.gameObject.CompareTag("Player"))
            {
                if (Larry.Hit(attackDamage, DamageTypeToPlayer.MagicProjectile, transform.position, gameObject, Guid.NewGuid()))
                {
                    GameObject deathKind = Instantiate(deathSound, transform.position, Quaternion.identity);
                    deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
                }
                Destroy(gameObject);
            }
            else if (other.gameObject.CompareTag("Object"))
            {
                if (other.gameObject.GetComponent<ObjektStats>().destructable)
                {
                    other.gameObject.GetComponent<ObjektStats>().hitPoints -= attackDamage;
                    if (other.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                    {
                        other.gameObject.GetComponent<ObjektStats>().startDeath();
                    }
                }
                Destroy(gameObject);
            }
        }
        public void Launch1()
        {
            Player = GameObject.FindWithTag("Player");
            Larry = Player.GetComponent<PlatformerCharacter2D>();
            rbLarry = Player.GetComponent<Rigidbody2D>();
            rb = GetComponent<Rigidbody2D>();
            richtung = rbLarry.position - rb.position;
            richtung.y += 0.7f;
            rb.velocity = richtung.normalized * speed;
        }
        public void Launch2(float x, float y)
        {
            Player = GameObject.FindWithTag("Player");
            Larry = Player.GetComponent<PlatformerCharacter2D>();
            rb = GetComponent<Rigidbody2D>();
            richtung = new Vector2(x, y);
            rb.velocity = new Vector2(richtung.x, richtung.y);
        }
    }
}
