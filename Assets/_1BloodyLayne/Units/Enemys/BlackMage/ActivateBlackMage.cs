﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class ActivateBlackMage : MonoBehaviour
    {
        public GameObject listenDialog;
        public BlackMage blackMage;
        bool listen;

        void Update()
        {
            if (listenDialog.activeSelf)
            {
                listen = true;
            }
            if (!listenDialog.activeSelf && listen)
            {
                blackMage.startFight();
                listen = false;
            }
        }
    }
}
