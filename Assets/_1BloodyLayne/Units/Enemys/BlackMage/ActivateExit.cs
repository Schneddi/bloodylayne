﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class ActivateExit : MonoBehaviour
    {
        public GameObject listenDialog, exitTrigger, blackMage;
        bool listen;

        void Update()
        {
            if (listenDialog.activeSelf)
            {
                listen = true;
            }
            if (!listenDialog.activeSelf && listen)
            {
                StartCoroutine(blackMage.GetComponent<BlackMage>().despawn(true));
                exitTrigger.SetActive(true);
                listen = false;
            }
        }
    }
}
