﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using Units.Enemys;
using Units.Player;
using UnityEngine;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class BlackMage : MonoBehaviour
    {
        public bool endingScene;
        EnemyStats stats;
        SpriteRenderer sRenderer;
        public GameObject spawnEffect, protectionField, shotAttack, waveAttack, exitTrigger, despawnParticle;
        public GameObject startDialog, dialog4Choice1, dialog4Choice2, dialog4Listener, dialogBox4Choice1Alt, dialogBox4Choice2Alt, violentEndingDialog;
        private Transform protectionSpawn, showAttackSpawn, chargeAttackSpawn;
        Rigidbody2D rb;
        Animator animator;
        private GameObject protectionFieldChild, bossUI;
        private PlatformerCharacter2D larry;
        int shotAttackCounter, attackType;
        GlobalVariables gv;
        private GameController gc;
        private Camera2DFollow cameraScript;
        private Dialog dialog;


        // Start is called before the first frame update
        void Start()
        {
            gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
            stats = GetComponent<EnemyStats>();
            animator = GetComponent<Animator>();
            sRenderer = GetComponent<SpriteRenderer>();
            sRenderer.enabled = false;
            rb = GetComponent<Rigidbody2D>();
            cameraScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
            larry = GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>();
            stats.unverwundbar = true;
            protectionSpawn = transform.Find("ProtectionSpawn");
            showAttackSpawn = transform.Find("ShowAttackSpawn");
            chargeAttackSpawn = transform.Find("ChargeAttackSpawn");
            bossUI = GameObject.Find("BossUI");
            dialog = FindObjectOfType<Dialog>();
            if (!endingScene)
            {
                handleBlackMageDialogue();
            }
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
            }
        }

        // Update is called once per frame
        void Update()
        {
            checkDeath();
        }

        void flipCheck()
        {
            if (larry.transform.position.x < rb.position.x && stats.facingRight)
            {
                flip();
            }
            else if (larry.transform.position.x > rb.position.x && !stats.facingRight)
            {
                flip();
            }
        }

        void flip()
        {
            stats.facingRight = !stats.facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

        public void spawn()
        {
            flipCheck();
            Instantiate(spawnEffect, transform.position, Quaternion.identity);
            sRenderer.enabled = true;
            if (GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().blackMageIntroDone)
            {
                bossUI.GetComponent<BossUI>().activateBossUI();
                startFight();
            }
            else
            {
                StartCoroutine(spawnAnimation());
            }
        }

        private IEnumerator spawnAnimation()
        {
            larry.GetComponent<Platformer2DUserControl>().BlockInput = true;
            larry.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            yield return new WaitForSeconds(0.5f);
            animator.SetTrigger("Spawn");
            yield return new WaitForSeconds(1f);
            dialog.ZeigeBox(startDialog);
        }

        public void startFight()
        {
            gv.blackMageIntroDone = true;
            flipCheck();
            stats.unverwundbar = false;
            larry.InitializeSkills(true);
            StartCoroutine(protection());
        }

        IEnumerator protection()
        {
            animator.SetTrigger("Protection");
            yield return new WaitForSeconds(0.5f);
            if (!stats.dead)
            {
                protectionFieldChild = Instantiate(protectionField, protectionSpawn.position, Quaternion.identity);
                protectionFieldChild.GetComponent<ProtectionField>().blackMage = gameObject;
            }
            yield return new WaitForSeconds(0.5f);

            if (!stats.dead)
            {
                animator.SetTrigger("ProtectionBack");
            }
            yield return new WaitForSeconds(0.75f);
            if (!stats.dead)
            {
                flipCheck();
                if (attackType == 0)
                {
                    attackType++;
                    StartCoroutine(chargedAttack());
                }
                else if (attackType == 1)
                {
                    attackType++;
                    StartCoroutine(shotAttacks());
                }
                else if (attackType == 2)
                {
                    attackType = 0;
                    StartCoroutine(waveAttacks());
                }
            }
        }

        IEnumerator chargedAttack()
        {
            animator.SetTrigger("ChargeAttack");
            yield return new WaitForSeconds(0.5f);
            for(int i=0; i<20; i++)
            {
                if (!stats.dead)
                {
                    animator.SetTrigger("ChargeAttackImpact");
                    flipCheck();
                    GameObject protectionChild = Instantiate(shotAttack, chargeAttackSpawn.position, Quaternion.identity);
                    protectionChild.GetComponent<BlackProjectile>().attackDamage = stats.attacks.GetValueOrDefault("BlackMagicShot");
                    protectionChild.GetComponent<BlackProjectile>().Launch1();
                    yield return new WaitForSeconds(0.25f);
                }
            }
            if (!stats.dead)
            {
                animator.SetTrigger("ChargeAttackBack");
            }
            yield return new WaitForSeconds(0.5f);

            if (!stats.dead)
            {
                StartCoroutine(protection());
            }
        }

        IEnumerator shotAttacks()
        {
            animator.SetTrigger("ShotAttack");
            yield return new WaitForSeconds(0.5f);
            for (int i = 0; i < 6; i++)
            {
                if (!stats.dead)
                {
                    float shotVelocityX = 10;
                    if (!stats.facingRight)
                    {
                        shotVelocityX *= -1;
                    }
                    animator.SetTrigger("ShotAttackImpact");
                    GameObject protectionChild1 = Instantiate(shotAttack, chargeAttackSpawn.position, Quaternion.identity);
                    protectionChild1.GetComponent<BlackProjectile>().Launch2(shotVelocityX, -2);
                    protectionChild1.GetComponent<BlackProjectile>().attackDamage = stats.attacks.GetValueOrDefault("BlackMagicShot");
                    yield return new WaitForSeconds(0.3f);
                    animator.SetTrigger("ShotAttackImpact");
                    GameObject protectionChild2 = Instantiate(shotAttack, chargeAttackSpawn.position, Quaternion.identity);
                    protectionChild2.GetComponent<BlackProjectile>().attackDamage = stats.attacks.GetValueOrDefault("BlackMagicShot");
                    protectionChild2.GetComponent<BlackProjectile>().Launch2(shotVelocityX, -0.25f);
                    yield return new WaitForSeconds(0.3f);
                    animator.SetTrigger("ShotAttackImpact");
                    GameObject protectionChild3 = Instantiate(shotAttack, chargeAttackSpawn.position, Quaternion.identity);
                    protectionChild3.GetComponent<BlackProjectile>().attackDamage = stats.attacks.GetValueOrDefault("BlackMagicShot");
                    protectionChild3.GetComponent<BlackProjectile>().Launch2(shotVelocityX, 2);
                    yield return new WaitForSeconds(1.5f);
                    flipCheck();
                }
            }
            if (!stats.dead)
            {
                animator.SetTrigger("ShotAttackBack");
            }
            yield return new WaitForSeconds(0.5f);
            if (!stats.dead)
            {
                StartCoroutine(protection());
            }
        }

        IEnumerator waveAttacks()
        {
            for (int i = 0; i < 6; i++)
            {
                if (!stats.dead)
                {
                    flipCheck();
                    animator.SetTrigger("Swing");
                    yield return new WaitForSeconds(0.5f);
                    GameObject protectionChild = Instantiate(waveAttack, chargeAttackSpawn.position, Quaternion.identity);
                    protectionChild.GetComponent<ChargedWave>().attackDamage = stats.attacks.GetValueOrDefault("BlackWave");
                    if (stats.facingRight)
                    {
                        protectionChild.GetComponent<ChargedWave>().right = true;
                        protectionChild.GetComponent<ChargedWave>().startFlight();
                    }
                    else
                    {
                        protectionChild.GetComponent<ChargedWave>().left = true;
                        protectionChild.GetComponent<ChargedWave>().startFlight();
                    }
                    yield return new WaitForSeconds(1.5f);
                }
            }
            yield return new WaitForSeconds(1f);
            if (!stats.dead)
            {
                StartCoroutine(protection());
            }
        }
        
        public IEnumerator despawn(bool turnOffMusic)
        {
            larry.InitializeSkills(true);
            gc.SetAchievement("BLACKMAGE_1");
            animator.SetTrigger("Smile");
            yield return new WaitForSeconds(1f);
            Instantiate(despawnParticle, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(0.5f);
            if (turnOffMusic)
            {
                cameraScript.FadeOutMusic();
            }
            Destroy(gameObject);
        }
        
        public IEnumerator badEndingDespawn()
        {
            Instantiate(despawnParticle, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(0.5f);
            Destroy(gameObject);
        }

        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }

        private void Death()
        {
            if (protectionFieldChild != null)
            {
                Destroy(protectionFieldChild);
            }
            gc.SetAchievement("BLACKMAGE_2");
            stats.dead = true;
            gv.blackMageKilled = true;
            exitTrigger.SetActive(true);
            animator.SetBool("Death", true);
            animator.SetTrigger("Dead");
            GetComponent<HumanDeathSequence>().Death();
        }

        void handleBlackMageDialogue()
        {
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            if (gv.humansKilled != 0)
            {
                dialog4Listener.GetComponent<DialogChoice>().nextDialogOption1 = dialogBox4Choice1Alt;
                dialog4Listener.GetComponent<DialogChoice>().nextDialogOption2 = dialogBox4Choice2Alt;
            }
            if (gv.humansKilled == 1)
            {
                dialog4Choice1.transform.Find("enText").GetComponent<Text>().text = "I killed no one! (Lie)";
                dialog4Choice1.transform.Find("deText").GetComponent<Text>().text = "Ich habe niemanden getötet! (Lügen)";
                dialog4Choice2.transform.Find("enText").GetComponent<Text>().text = "Only one!";
                dialog4Choice2.transform.Find("deText").GetComponent<Text>().text = "Nur einen!";
            }
            else if (gv.humansKilled == gv.humansCount-1)
            {
                dialog4Choice1.transform.Find("enText").GetComponent<Text>().text = "I killed no one! (Lie)";
                dialog4Choice1.transform.Find("deText").GetComponent<Text>().text = "Ich habe niemanden getötet! (Lügen)";
                dialog4Choice2.transform.Find("enText").GetComponent<Text>().text = "Everyone!";
                dialog4Choice2.transform.Find("deText").GetComponent<Text>().text = "Alle!";
                dialog4Listener.GetComponent<DialogChoice>().nextDialogOption2 = violentEndingDialog;
            }
            else if (gv.humansKilled > 1)
            {
                dialog4Choice1.transform.Find("enText").GetComponent<Text>().text = "I killed no one! (Lie)";
                dialog4Choice1.transform.Find("deText").GetComponent<Text>().text = "Ich habe niemanden getötet! (Lügen)";
                dialog4Choice2.transform.Find("enText").GetComponent<Text>().text = "I killed some, who attacked me!";
                dialog4Choice2.transform.Find("deText").GetComponent<Text>().text = "Ich habe jene getötet, welche mich angegriffen haben!";
            }
        }
    }
}