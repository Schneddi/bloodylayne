﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class ChargedWave : MonoBehaviour
    {
        public int attackDamage;
        Vector2 direction = new Vector2(6,0);
        public bool left, right;
        Rigidbody2D rb;
        public float damagerCheckLength;
        private Transform topDamagerCheck;
        PlatformerCharacter2D larry;
        bool hit;
        public GameObject hitSound;


        void Start()
        {
            topDamagerCheck = transform.Find("TopDamagerCheck");
            larry = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
        }
        
        void Update()
        {
            if (Physics2D.Raycast(topDamagerCheck.position, Vector2.down, damagerCheckLength, 1 << 8) && !hit)
            {
                hit = true;
                if(larry.Hit(attackDamage, DamageTypeToPlayer.MagicProjectile, transform.position, gameObject, Guid.NewGuid()))
                {
                    Instantiate(hitSound, larry.transform.position, Quaternion.identity);
                }
            }

            if (transform.localScale.y < 0.6f)
            {
                float sizeIncrease = Time.deltaTime;
                transform.localScale = new Vector3(transform.localScale.x + sizeIncrease * Mathf.Sign(transform.localScale.x), transform.localScale.y + sizeIncrease, transform.localScale.z);
            }
        }

        void flip()
        {
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

        public void startFlight()
        {
            if (left){
                flip();
                direction.x *= -1;
                rb = GetComponent<Rigidbody2D>();
                rb.velocity = new Vector2(direction.x, direction.y);
            }
            else if (right)
            {
                rb = GetComponent<Rigidbody2D>();
                rb.velocity = new Vector2(direction.x, direction.y);
            }
        }
    }
}
