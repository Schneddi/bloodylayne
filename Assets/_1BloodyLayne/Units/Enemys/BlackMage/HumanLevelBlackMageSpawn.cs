using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class HumanLevelBlackMageSpawn : MonoBehaviour
    {
        public GameObject blackMage, blackScreen;
        private void Update()
        {
            if (!blackScreen.activeSelf)
            {
                blackMage.GetComponent<BlackMage>().spawn();
                Destroy(gameObject);
            }
        }
    }
}
