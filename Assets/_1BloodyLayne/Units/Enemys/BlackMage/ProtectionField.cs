﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class ProtectionField : MonoBehaviour
    {
        float size = 1;
        GameObject player;
        public GameObject blackMage;
        bool getSmaller;
        AudioSource deniedSound;
        public GameObject deniedEffect;
        // Start is called before the first frame update
        void Start()
        {
            player = GameObject.FindWithTag("Player");
            deniedSound = GetComponents<AudioSource>()[1];
            blackMage.GetComponent<EnemyStats>().unverwundbar = true;
            StartCoroutine(protection());
        }

        // Update is called once per frame
        void Update()
        {
            if (size < 6)
            {
                size = size + Time.deltaTime * 2.5f;
            }
            if(getSmaller && size > 0)
            {
                size = size - Time.deltaTime * 3.5f;
            }
            if (Mathf.Abs(transform.position.magnitude - player.transform.position.magnitude) < size)
            {
                Vector2 pushDirection = new Vector2((player.transform.position.x - blackMage.transform.position.x), (player.transform.position.y - blackMage.transform.position.y));
                player.GetComponent<Rigidbody2D>().velocity = new Vector2(pushDirection.normalized.x * 25, pushDirection.normalized.y * 5);
                player.GetComponent<PlatformerCharacter2D>().InitiatePushedTimer(0.5f);
            }

            Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, size, 1 << 20);
            if(cols.Length > 0)
            {
                foreach(Collider2D col in cols)
                {
                    if ((col.gameObject.GetComponent("BlutmagieChargedProjektilControl") as BlutmagieChargedProjektilControl) != null)
                    {
                        deniedSound.PlayDelayed(0);
                        Instantiate(deniedEffect, col.transform.position, Quaternion.identity);
                        Destroy(col.gameObject);
                    }
                    else if ((col.gameObject.GetComponent("BlutmagieFarProjektilControl") as BlutmagieFarProjektilControl) != null)
                    {
                        deniedSound.PlayDelayed(0);
                        Instantiate(deniedEffect, col.transform.position, Quaternion.identity);
                        Destroy(col.gameObject);
                    }
                    else if ((col.gameObject.GetComponent("BlutmagieProjektilControl") as BlutmagieProjektilControl) != null)
                    {
                        deniedSound.PlayDelayed(0);
                        Instantiate(deniedEffect, col.transform.position, Quaternion.identity);
                        Destroy(col.gameObject);
                    }
                    else if ((col.gameObject.GetComponent("BlutmagieStandardControl") as BlutmagieStandardControl) != null)
                    {
                        deniedSound.PlayDelayed(0);
                        Instantiate(deniedEffect, col.transform.position, Quaternion.identity);
                        Destroy(col.gameObject);
                    }
                }
            }

        }


        IEnumerator protection()
        {
            yield return new WaitForSeconds(4f);
            getSmaller = true;
            yield return new WaitForSeconds(1.5f);
            blackMage.GetComponent<EnemyStats>().unverwundbar = false;
        }
    }
}
