using System;
using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Pater : MonoBehaviour
    {
        public GameObject godmagic, invulSpellEffect, invulSpellShield;
        public GameObject holyStraceEffect;
        public float speed;
        public string status = "Idle";
        
        private EnemyStats stats;
        private Transform player, attackSpawnPoint, flyingTargetHeigth;
        private Animator animator;
        private AudioSource attackSound, launchSound, invulSpellStartSound, invulSpellEndSound;
        private Rigidbody2D rb;
        private GameObject holyStraceChild;
        private float dragStore, idleDrag = 30;

        // Use this for initialization
        void Start()
        {
            player = GameObject.FindWithTag("Player").transform;
            attackSpawnPoint = transform.Find("AttackSpawnPoint");
            flyingTargetHeigth = transform.Find("FlyingTargetHeigth");
            flyingTargetHeigth.parent = transform.parent;
            stats = GetComponent<EnemyStats>();
            rb = GetComponent<Rigidbody2D>();
            attackSound = GetComponents<AudioSource>()[0];
            launchSound = GetComponents<AudioSource>()[1];
            invulSpellStartSound = GetComponents<AudioSource>()[2];
            invulSpellEndSound = GetComponents<AudioSource>()[3];
            animator = GetComponent<Animator>();
            dragStore = rb.drag;
            rb.drag = idleDrag;
            if (!stats.facingRight)
            {
                Flip();
            }
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (!animator.GetBool("Death"))
            {
                checkDeath();

                if (status.Equals("Launching"))
                {
                    rb.AddForce(new Vector2(0, Mathf.Abs(speed)));
                    if (transform.position.y > flyingTargetHeigth.position.y)
                    {
                        animator.SetTrigger("Fly");
                        status = "Flying";
                    }
                }

                if (status.Equals("Flying") || status.Equals("FlyAttacking"))
                {
                    rb.AddForce(new Vector2(speed, 0));
                    if (transform.position.y > flyingTargetHeigth.position.y + 0.5f)
                    {
                        rb.AddForce(new Vector2(0, -Mathf.Abs(speed)));
                    }
                    else if (transform.position.y < flyingTargetHeigth.position.y - 0.5f)
                    {
                        rb.AddForce(new Vector2(0, Mathf.Abs(speed)));
                    }
                }
            }
        }

        void Update()
        {
            if (!animator.GetBool("Death"))
            {
                if (status.Equals("Idle"))
                {
                    if (Physics2D.OverlapCircle(transform.position, 14f, 1 << 8))
                    {
                        if (checkIfLarryInSight())
                        {
                            startAggro();
                        }
                    }
                }

                if (status.Equals("Flying"))
                {
                    if (Physics2D.OverlapCircle(transform.position, 13f, 1 << 8))
                    {
                        if (checkIfLarryInSight())
                        {
                            StartCoroutine(FlyAttack());
                        }
                    }
                    else
                    {
                        FlipCheck();
                        if (!Physics2D.OverlapCircle(transform.position, 35f, 1 << 8))
                        {
                            status = "Idle";
                            rb.drag = idleDrag;
                            rb.gravityScale = 8;
                            animator.SetTrigger("Idle");
                        }
                        else
                        {
                            rb.drag = dragStore;
                        }
                    }
                }

                if (status.Equals("Casting"))
                {
                    if (Physics2D.OverlapCircle(transform.position, 2f, 1 << 8))
                    {
                        if (checkIfLarryInSight())
                        {
                            StartCoroutine(InvulSpellStop());
                        }
                    }
                }
            }
        }

        private void FlipCheck()
        {
            if (stats.facingRight && transform.position.x < player.position.x)
            {
                Flip();
            }
            else if (!stats.facingRight && transform.position.x > player.position.x)
            {
                Flip();
            }
        }

        private void Flip()
        {
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
            stats.facingRight = !stats.facingRight;
            speed *= -1;
        }
        
        public void startAggro()
        {
            if (status.Equals("Idle"))
            {
                FlipCheck();
                int allyCount = 0;
                RaycastHit2D[] castStar = Physics2D.CircleCastAll(transform.position, 17f, Vector2.zero, Mathf.Infinity, 1<<9);
                foreach (RaycastHit2D raycastHit in castStar)
                {
                    if (raycastHit.collider.GetComponent<EnemyStats>() != null &&
                        (raycastHit.collider.transform.position.x > transform.position.x && !stats.facingRight) ||
                        (raycastHit.collider.transform.position.x < transform.position.x && stats.facingRight))
                    {
                        if (raycastHit.collider.GetComponent<EnemyStats>().isHuman)
                        {
                            allyCount++;
                        }
                    }
                }
                if (allyCount > 2)
                {
                    StartCoroutine(InvulSpellStart());
                }
                else
                {
                    startLaunch();
                }
            }
        }

        public void startLaunch()
        {
            if (status.Equals("Idle"))
            {
                Instantiate(holyStraceEffect, transform.position, Quaternion.identity);
                launchSound.PlayDelayed(0);
                rb.gravityScale = 0;
                animator.SetTrigger("Launch");
                status = ("Launching");
            }
        }
        
        IEnumerator FlyAttack()
        {
            status = ("FlyAttacking");
            animator.SetTrigger("FlyAttack");
            yield return new WaitForSeconds(0.3f);
            if (!animator.GetBool("Death"))
            {
                attackSound.PlayDelayed(0);
                GameObject holyMagicBall = Instantiate(godmagic, attackSpawnPoint.transform.position, Quaternion.identity);
                holyMagicBall.GetComponent<Todesmagie>().abschuss1();
                holyMagicBall.GetComponent<Todesmagie>().attackDamage = stats.attacks.GetValueOrDefault("GodMagic");
            }
            yield return new WaitForSeconds(0.2f);
            if (!animator.GetBool("Death"))
            {
                status = ("Flying");
                animator.SetTrigger("Fly");
            }
        }

        IEnumerator InvulSpellStart()
        {
            status = ("Channeling");
            animator.SetTrigger("InvulSpellStart");
            yield return new WaitForSeconds(0.33f);
            if (!animator.GetBool("Death"))
            {
                Instantiate(invulSpellEffect, transform.position, Quaternion.identity);
                animator.SetTrigger("InvulSpellIdle");
                status = ("Casting");
                invulSpellStartSound.PlayDelayed(0);
                RaycastHit2D[] castStar = Physics2D.CircleCastAll(transform.position, 17f, Vector2.zero, Mathf.Infinity, 1<<9);
                foreach (RaycastHit2D raycastHit in castStar)
                {
                    if (raycastHit.collider.GetComponent<EnemyStats>() != null &&
                        (raycastHit.collider.transform.position.x > transform.position.x && !stats.facingRight) ||
                        (raycastHit.collider.transform.position.x < transform.position.x && stats.facingRight))
                    {
                        if (raycastHit.collider.GetComponent<EnemyStats>().isHuman)
                        {
                            raycastHit.collider.GetComponent<EnemyStats>().unverwundbar = true;
                            GameObject invulShield = Instantiate(invulSpellShield, raycastHit.collider.transform.position, Quaternion.identity);
                            invulShield.transform.parent = raycastHit.collider.transform;
                            invulShield.GetComponent<InvulShield>().target = raycastHit.collider.gameObject;
                            invulShield.GetComponent<InvulShield>().sourceCaster = gameObject;
                        }
                    }
                }
            }
        }

        IEnumerator InvulSpellStop()
        {
            animator.SetTrigger("InvulSpellStop");
            yield return new WaitForSeconds(0.33f);
            if (!animator.GetBool("Death"))
            {
                status = ("Idle");
                startLaunch();
            }
        }

        private bool checkIfLarryInSight()
        {
            Vector3 richtung = player.position - transform.position;
            RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
            foreach (RaycastHit2D hitter in hit)
            {
                if (hitter.transform.tag == "Ground")
                {
                    break;
                }
                if (hitter.transform.tag == "Player")
                {
                    return true;
                }
            }
            return false;
        }
        
        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }
        private void Death()
        {
            rb.freezeRotation = false;
            rb.constraints = rb.constraints & ~RigidbodyConstraints2D.FreezePositionX & ~RigidbodyConstraints2D.FreezePositionY;
            stats.dead = true;
            status = ("Dying");
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            GetComponent<HumanDeathSequence>().Death();
        }
    }
}