using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityStandardAssets._2D;

public class InvulShield : MonoBehaviour
{
    public GameObject sourceCaster, target;

    // Update is called once per frame
    void Update()
    {
        if (sourceCaster == null)
        {
            stopInvulShield();
        }
        else if (!sourceCaster.GetComponent<Pater>().status.Equals("Casting"))
        {
            stopInvulShield();
        }
    }

    private void stopInvulShield()
    {
        sourceCaster = null;
        RaycastHit2D[] castStar = Physics2D.CircleCastAll(transform.position, 20f, Vector2.zero, Mathf.Infinity, 1<<9);
        foreach (RaycastHit2D raycastHit in castStar)
        {
            if (raycastHit.collider.GetComponent<Pater>() != null)
            {
                if (raycastHit.collider.GetComponent<Pater>().status.Equals("Casting"))
                {
                    sourceCaster = raycastHit.collider.gameObject;
                }
            }
        }

        if (sourceCaster == null)
        {
            if (target != null)
            {
                if (target.GetComponent<EnemyStats>() != null)
                {
                    target.GetComponent<EnemyStats>().unverwundbar = false;
                }
            }
            Destroy(gameObject);
        }
    }
}
