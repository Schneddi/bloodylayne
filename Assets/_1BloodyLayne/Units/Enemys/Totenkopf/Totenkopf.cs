﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Totenkopf : MonoBehaviour
    {
        EnemyStats stats;
        public bool facingRight = false;
        bool chasing, hitZone, idle = true, comeBack, active;
        private Transform player, hit;
        public Transform center;
        Animator animator;
        AudioSource idleLoopSound, chaseSound, stopChaseSound, attackLoopSound, hitSound;
        Rigidbody2D rb;
        Vector2 speed = new Vector2(0, 0);
        public GameObject partikel, unholyFire;
        public GameObject deathSound;
        GameObject fireChild;

        // Use this for initialization
        void Start()
        {
            player = GameObject.FindWithTag("Player").transform;
            hit = transform.Find("Hitzone");
            stats = GetComponent<EnemyStats>();
            rb = GetComponent<Rigidbody2D>();
            idleLoopSound = GetComponents<AudioSource>()[0];
            chaseSound = GetComponents<AudioSource>()[1];
            stopChaseSound = GetComponents<AudioSource>()[2];
            attackLoopSound = GetComponents<AudioSource>()[3];
            hitSound = GetComponents<AudioSource>()[4];
            animator = GetComponent<Animator>();
            if (!facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                speed *= -1;
                transform.localScale = theScale;
            }
            idleLoopSound.PlayDelayed(0);
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (!animator.GetBool("Death"))
            {
                checkDeath();
                if (!animator.GetBool("Death"))
                {
                    if (hitZone)//fragt nacheinander die HitZonen ab, an denen das Schwert Schaden erzeugt, dabei wird am gesamten Schwertschwung abgefragt
                    {
                        if (Physics2D.OverlapCircle(hit.position, 1.5f, 1 << 8))
                        {
                            if(player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("SkullBite"), DamageTypeToPlayer.SkullBite, hit.position, gameObject, new Guid()))
                            {
                                hitSound.PlayDelayed(0);
                            }
                        }
                        damageObjects(hit);
                    }
                }
            }
            if (!active)
            {
                if (Physics2D.OverlapCircle(transform.position, 35f, 1 << 8))
                {
                    active = true;
                }
            }
            if (!animator.GetBool("Death")&& active)
            {
                if (idle)
                {
                    if (rb.velocity.x < 0 && facingRight)
                    {
                        Flip();
                    }
                    else if (rb.velocity.x > 0 && !facingRight)
                    {
                        Flip();
                    }
                    if (Physics2D.OverlapCircle(transform.position, 10f, 1 << 8))
                    {
                        Vector3 richtung = player.transform.position - transform.position;
                        RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
                        foreach (RaycastHit2D hitter in hit) {
                            if (hitter.transform.tag == "Ground")
                            {
                                break;
                            }
                            if (hitter.transform.tag == "Player")
                            {
                                idle = false;
                                animator.SetTrigger("StartChase");
                                chaseSound.PlayDelayed(0);
                                idleLoopSound.Stop();
                                rb.velocity = new Vector2(0, 0);
                                StartCoroutine(Chasing());
                                break;
                            }
                        }
                    }

                    if (transform.position.x < center.position.x)
                    {
                        rb.velocity = new Vector2(rb.velocity.x + 0.1f, rb.velocity.y);
                    }
                    else
                    {
                        rb.velocity = new Vector2(rb.velocity.x - 0.1f, rb.velocity.y);
                    }
                    if (transform.position.y < center.position.y)
                    {
                        rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + 0.1f);
                    }
                    else
                    {
                        rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y - 0.1f);
                    }
                }

                if (chasing)
                {
                    FlipCheck();
                    if (Physics2D.OverlapCircle(transform.position, 15f, 1 << 8))
                    {
                        Vector3 richtung = player.transform.position - transform.position;
                        RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, richtung);
                        foreach (RaycastHit2D hitter in hit)
                        {
                            if (hitter.transform.tag == "Ground")
                            {
                                chasing = false;
                                StartCoroutine(StopChasing());
                                break;
                            }
                            if (hitter.transform.tag == "Player")
                            {
                                Quaternion q = Quaternion.LookRotation(richtung);
                                q.y = 0;
                                q.x = 0;
                                transform.rotation = q;
                                fireChild.transform.rotation = q;
                                richtung = new Vector3(player.transform.position.x, player.transform.position.y+0.7f, player.transform.position.z) - transform.position;
                                richtung = richtung.normalized * 0.9f;
                                rb.velocity = 0.94f * rb.velocity + new Vector2(richtung.x, richtung.y);
                                break;
                            }
                        }
                    }
                    else
                    {
                        transform.rotation = Quaternion.identity;
                        chasing = false;
                        StartCoroutine(StopChasing());
                        rb.velocity = new Vector2(0, 0);
                        hitZone = false;
                        animator.SetTrigger("StopChase");
                        attackLoopSound.Stop();
                        stopChaseSound.PlayDelayed(0);
                    }
                }
                if (comeBack)
                {
                    Vector3 richtung = center.transform.position - transform.position;
                    Destroy(fireChild);
                    if (richtung.magnitude > 5)
                    {
                        richtung = richtung.normalized * 2;
                        rb.velocity = rb.velocity + new Vector2(richtung.x, richtung.y);
                    }
                    else
                    {
                        animator.SetTrigger("Idle");
                        idleLoopSound.PlayDelayed(0);
                        rb.velocity = new Vector2(0, 0);
                        comeBack = false;
                        idle = true;
                    }
                }
            }
        }

        void Flip()//z.B. in andere Richtung gucken
        {
            if (facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                facingRight = !facingRight;
                speed *= -1;
            }
            else if (!facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                facingRight = !facingRight;
                speed *= -1;
            }
        }

        void FlipCheck()//z.B. in andere Richtung gucken
        {
            if (facingRight && transform.position.x > player.position.x)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                facingRight = !facingRight;
                speed *= -1;
            }
            else if (!facingRight && transform.position.x < player.position.x)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                facingRight = !facingRight;
                speed *= -1;
            }
        }


        IEnumerator Chasing()//Gibt Schussfrequenz und Animationen an
        {
            yield return new WaitForSeconds(1.5f);
            hitZone = true;
            Vector3 richtung = player.transform.position - transform.position;
            Quaternion q2 = Quaternion.LookRotation(richtung);
            q2.y = 0;
            q2.x = 0;
            //transform.rotation = q2;
            if (facingRight)
            {
                fireChild = Instantiate(unholyFire, new Vector3(hit.position.x - 0.1f, hit.position.y + 0.0f, hit.position.z), q2);
            }
            else
            {
                fireChild = Instantiate(unholyFire, new Vector3(hit.position.x + 0.1f, hit.position.y + 0.0f, hit.position.z), q2);
            }
            fireChild.transform.parent = gameObject.transform;
            richtung = new Vector3(player.transform.position.x, player.transform.position.y + 0.7f, player.transform.position.z) - transform.position;
            richtung = richtung.normalized * 15;
            rb.velocity = new Vector2(richtung.x, richtung.y);
            animator.SetTrigger("Chase");
            chasing = true;
            attackLoopSound.PlayDelayed(0);
        }

        IEnumerator StopChasing()
        {
            yield return new WaitForSeconds(2f);
            transform.rotation = Quaternion.identity;
            rb.velocity = new Vector2(0, 0);
            hitZone = false;
            attackLoopSound.Stop();
            stopChaseSound.PlayDelayed(0);
            animator.SetTrigger("StopChase");
            yield return new WaitForSeconds(1f);
            comeBack = true;
        }


        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                StartCoroutine(Death());
            }
        }
        IEnumerator Death()
        {
            stats.dead = true;
            GameObject deathKind = Instantiate(deathSound, transform.position, Quaternion.identity);
            deathKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
            idleLoopSound.Stop();
            attackLoopSound.Stop();
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            rb.freezeRotation = false;
            rb.constraints = rb.constraints & ~RigidbodyConstraints2D.FreezePositionX;
            yield return new WaitForSeconds(1f);
            Instantiate(partikel, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

        void damageObjects(Transform hit)
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(hit.position, 1f, 1 << 14);
            {
                foreach (Collider2D col in cols)
                {
                    if (col.gameObject.GetComponent<ObjektStats>() != null)
                    {
                        if (col.gameObject.GetComponent<ObjektStats>().destructable)
                        {
                            col.gameObject.GetComponent<ObjektStats>().hitPoints -= stats.attacks.GetValueOrDefault("SkullBite");
                            if (col.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                            {
                                col.gameObject.GetComponent<ObjektStats>().startDeath();
                            }
                        }
                    }
                }
            }
        }
    }
}