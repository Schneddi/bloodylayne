﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(EnemyStats))]
    [RequireComponent(typeof(HumanDeathSequence))]
    public class Buttler : MonoBehaviour
    {
        EnemyStats stats;
        bool hitZoneDefault, hitZone1, hitZone2, hitZone3, hitZone4, hitZoneSchaft, idle = true, backswing;
        private Transform player, hitDefault, hit1, hit2, hit3, hit4, hitSchaft;
        Animator animator;
        AudioSource attackSound, backswingSound, hitSound;
        Rigidbody2D rb;
        public float speed = 6;

        // Use this for initialization
        void Start()
        {
            player = GameObject.FindWithTag("Player").transform;
            hitDefault = transform.Find("hitDefault");
            hit1 = transform.Find("Hit1");
            hit2 = transform.Find("Hit2");
            hit3 = transform.Find("Hit3");
            hit4 = transform.Find("Hit4");
            hitSchaft = transform.Find("HitSchaft");
            stats = GetComponent<EnemyStats>();
            stats.isHuman = true;
            rb = GetComponent<Rigidbody2D>();
            attackSound = GetComponents<AudioSource>()[0];
            backswingSound = GetComponents<AudioSource>()[1];
            hitSound = GetComponents<AudioSource>()[2];
            animator = GetComponent<Animator>();
            if (!stats.facingRight)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                speed *= -1;
            }
            hitZoneDefault = true;
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (!animator.GetBool("Death"))
            {
                checkDeath();
                if (!animator.GetBool("Death"))
                {
                    Guid guid = Guid.NewGuid();
                    if (hitZoneDefault)
                    {
                        if (Physics2D.OverlapCircle(hit1.position, 0.8f, 1 << 8))
                        {
                            if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeSwing"), DamageTypeToPlayer.MeleeSwing, new Vector2(speed * 6, 0), hit1.position, gameObject, guid))
                            {
                                hitSound.PlayDelayed(0);
                            }
                        }
                        damageObjects(hit1);
                    }
                    if (hitZone1)
                    {
                        if (Physics2D.OverlapCircle(hit1.position, 0.8f, 1 << 8))
                        {
                            if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeSwing"), DamageTypeToPlayer.MeleeSwing,  new Vector2(speed * 4, 0), hit1.position, gameObject, guid))
                            {
                                hitSound.PlayDelayed(0);
                            }
                            hitSound.PlayDelayed(0);
                        }
                        damageObjects(hit1);
                    }
                    if (hitZone2)
                    {
                        if (Physics2D.OverlapCircle(hit2.position, 0.8f, 1 << 8))
                        {
                            if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeSwing"), DamageTypeToPlayer.MeleeSwing,  new Vector2(speed * 4, 0), hit2.position, gameObject, guid))
                            {
                                hitSound.PlayDelayed(0);
                            }
                            hitSound.PlayDelayed(0);
                        }
                        damageObjects(hit2);
                    }
                    if (hitZone3)
                    {
                        if (Physics2D.OverlapCircle(hit3.position, 0.8f, 1 << 8))
                        {
                            if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeSwing"), DamageTypeToPlayer.MeleeSwing,  new Vector2(speed * 4, 0), hit3.position, gameObject, guid))
                            {
                                hitSound.PlayDelayed(0);
                            }
                            hitSound.PlayDelayed(0);
                        }
                        damageObjects(hit3);
                    }
                    if (hitZone4)
                    {
                        if (Physics2D.OverlapCircle(hit4.position, 1.2f, 1 << 8))
                        {
                            if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeSwing"), DamageTypeToPlayer.MeleeSwing,  new Vector2(speed * 4, 0), hit4.position, gameObject, guid))
                            {
                                hitSound.PlayDelayed(0);
                            }
                            hitSound.PlayDelayed(0);
                        }
                        damageObjects(hit4);
                    }
                    if (hitZoneSchaft)
                    {
                        if (Physics2D.OverlapCircle(hitSchaft.position, 1f, 1 << 8))
                        {
                            if (player.GetComponent<PlatformerCharacter2D>().Hit(stats.attacks.GetValueOrDefault("MeleeSwing"), DamageTypeToPlayer.MeleeSwing,  new Vector2(speed * 4, 0), hitSchaft.position, gameObject, guid))
                            {
                                hitSound.PlayDelayed(0);
                            }
                            hitSound.PlayDelayed(0);
                        }
                        damageObjects(hitSchaft);
                    }
                }
            }
        }

        void Update()
        {

            if (!animator.GetBool("Death"))
            {
                if (idle)
                {
                    if (Physics2D.OverlapCircle(transform.position, 12f, 1 << 8) && (Mathf.Abs(player.position.y - transform.position.y) < 2))
                    {
                        FlipCheck();

                        if (Physics2D.OverlapCircle(transform.position, 5f, 1 << 8) && (Mathf.Abs(player.position.y - transform.position.y) < 2))
                        {
                            idle = false;
                            StartCoroutine(Attack());
                        }
                    }
                }
            }
        }
        void FlipCheck()//z.B. in andere Richtung gucken
        {
            if (stats.facingRight && transform.position.x > player.position.x)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
            else if (!stats.facingRight && transform.position.x < player.position.x)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                stats.facingRight = !stats.facingRight;
                speed *= -1;
            }
        }
        IEnumerator Attack()
        {
            if (!animator.GetBool("Death"))
            {
                attackSound.PlayDelayed(0);
                animator.SetTrigger("Attack");
                yield return new WaitForSeconds(0.1f);
                hitZone1 = true;
                yield return new WaitForSeconds(0.1f);
                hitZoneDefault = false;
                hitZone1 = false;
                hitZone2 = true;
                hitZoneSchaft = true;
                yield return new WaitForSeconds(0.1f);
                hitZone2 = false;
                hitZone3 = true;
                yield return new WaitForSeconds(0.1f);
                hitZone3 = false;
                hitZone4 = true;
                yield return new WaitForSeconds(0.1f);
                hitZoneSchaft = false;
            }
            if (!animator.GetBool("Death"))
            {
                StartCoroutine(Backswing());
            }
        }
        IEnumerator Backswing()
        {
            if (!animator.GetBool("Death"))
            {
                backswingSound.PlayDelayed(0);
                animator.SetTrigger("Backswing");
                yield return new WaitForSeconds(0.1f);
                hitZone4 = false;
                hitZone3 = true;
                yield return new WaitForSeconds(0.1f);
                hitZone3 = false;
                hitZone2 = true;
                yield return new WaitForSeconds(0.1f);
                hitZone2 = false;
                hitZone1 = true;
                yield return new WaitForSeconds(0.1f);
                hitZone1 = false;
                hitZoneDefault = true;
            }
            if (!animator.GetBool("Death"))
            {
                animator.SetTrigger("Idle");
                idle = true;
            }
        }

        void checkDeath()
        {
            if (stats.health <= 0 && !stats.dead)
            {
                Death();
            }
        }
        private void Death()
        {
            stats.dead = true;
            animator.SetBool("Death", true);
            animator.SetTrigger("DeathTrigger");
            GetComponent<HumanDeathSequence>().Death();
        }

        void damageObjects(Transform hit)
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(hit.position, 0.5f, 1 << 14);
            {
                foreach (Collider2D col in cols)
                {
                    if ((col.gameObject.GetComponent("ObjektStats") as ObjektStats) != null)
                    {
                        if (col.gameObject.GetComponent<ObjektStats>().destructable)
                        {
                            col.gameObject.GetComponent<ObjektStats>().hitPoints -= stats.attacks.GetValueOrDefault("MeleeSwing");
                            if (col.gameObject.GetComponent<ObjektStats>().hitPoints <= 0)
                            {
                                col.gameObject.GetComponent<ObjektStats>().startDeath();
                            }
                        }
                    }
                }
            }
        }
    }
}