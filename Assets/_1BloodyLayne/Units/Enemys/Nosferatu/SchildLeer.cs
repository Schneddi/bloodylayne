﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class SchildLeer : MonoBehaviour
    {
        private Color fadeColor = new Color(1f, 1f, 1f, 0f);
        float a = 0;

        // Use this for initialization
        void Start()
        {
            GetComponent<Renderer>().material.color = fadeColor;
        }

        private void Update()
        {
            if (a < 1)
            {
                //Transparency.
                fadeColor = new Color(1f, 1f, 1f, a);
                GetComponent<Renderer>().material.color = fadeColor;
                a = a + 0.022f;
            }
        }
    }
}
