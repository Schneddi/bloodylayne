﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using Units.Player;
using UnityEditor;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Schild : MonoBehaviour
    {
        public GameObject hitBlutig;
        public GameObject hitSound;
        public bool friendlyScene;
        public int attackDamage;
        
        private Transform oben, unten;
        private GameObject player;
        private Nosferatu nosferatu;
        private Color fadeColor = new Color(1f, 1f, 1f, 0f);
        private float a;
        private System.Guid guid;

        // Use this for initialization
        void Start()
        {
            guid = System.Guid.NewGuid();
            oben = transform.Find("SchildOben");
            unten = transform.Find("SchildUnten");
            player = GameObject.FindGameObjectWithTag("Player");
            GetComponent<Renderer>().material.color = fadeColor;
            GameObject[] objects = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject enemy in objects) {
                if(enemy.gameObject.GetComponent<Nosferatu>() != null){
                    nosferatu = enemy.GetComponent<Nosferatu>();
                }

            }
        }

        private void Update()
        {
            if (a < 1)
            {
                //Transparency.
                fadeColor = new Color(1f, 1f, 1f, a);
                GetComponent<Renderer>().material.color = fadeColor;
                a = a + 0.022f;
            }
            
            if (!friendlyScene)
            {
                if (Physics2D.OverlapCircle(oben.position, 1.7f, 1 << 8) ||
                    Physics2D.OverlapCircle(unten.position, 2f, 1 << 8))
                {
                    Vector3 position = player.transform.position;
                    Vector3 position1 = transform.position;
                    Vector2 direction = position - position1;
                    Vector2 midwayPoint = (position + position1)/2;
                    if (player.GetComponent<PlatformerCharacter2D>().Hit(attackDamage, DamageTypeToPlayer.MagicShield, new Vector2(
                        nosferatu.rb.velocity.x + direction.normalized.x * 10,
                        nosferatu.rb.velocity.y + direction.normalized.y * 10), midwayPoint, gameObject, new Guid()))
                    {
                        GameObject hitKind = Instantiate(hitSound, transform.position, Quaternion.identity);
                        hitKind.GetComponent<DeathSound>().sourceUnit = gameObject.transform;
                    }
                }

                Collider2D[] cols = Physics2D.OverlapCircleAll(oben.position, 1.7f, 1 << 9 | 1 << 20); //ist bat am Schild? (oben)
                foreach (Collider2D col in cols)
                {
                    if ((col.gameObject.GetComponent("EnemyFledermaus") as EnemyFledermaus) != null)
                    {
                        if (col.GetComponent<EnemyStats>().health > 0)
                        {
                            Instantiate(hitBlutig, col.GetComponent<Rigidbody2D>().position, Quaternion.identity);
                            col.GetComponent<EnemyStats>().Hit(attackDamage, DamageTypeToEnemy.MagicShield, guid, false);
                        }

                        if ((col.gameObject.GetComponent("BlutmagieProjektilControl") as BlutmagieProjektilControl) !=
                            null ||
                            (col.gameObject.GetComponent(
                                "BlutmagieFarProjektilControl") as BlutmagieFarProjektilControl) != null ||
                            (col.gameObject.GetComponent("BlutmagieStandardControl") as BlutmagieStandardControl) !=
                            null || (col.gameObject.GetComponent("SchallWelleControl") as SchallWelleControl) != null)
                        {
                            Destroy(col.gameObject);
                        }
                    }
                }

                cols = Physics2D.OverlapCircleAll(unten.position, 2.2f, 1 << 9 | 1 << 20); //ist bat am Schild? (unten)
                foreach (Collider2D col in cols)
                {
                    if ((col.gameObject.GetComponent("EnemyFledermaus") as EnemyFledermaus) != null)
                    {

                        if (col.GetComponent<EnemyStats>().health > 0)
                        {
                            Instantiate(hitBlutig, col.GetComponent<Rigidbody2D>().position, Quaternion.identity);
                            col.GetComponent<EnemyStats>().Hit(attackDamage, DamageTypeToEnemy.MagicShield, guid, false);
                        }
                    }

                    if ((col.gameObject.GetComponent("BlutmagieProjektilControl") as BlutmagieProjektilControl) !=
                        null ||
                        (col.gameObject.GetComponent("BlutmagieFarProjektilControl") as BlutmagieFarProjektilControl) !=
                        null || (col.gameObject.GetComponent("BlutmagieStandardControl") as BlutmagieStandardControl) !=
                        null || (col.gameObject.GetComponent("SchallWelleControl") as SchallWelleControl) != null)
                    {
                        Destroy(col.gameObject);
                    }
                }
            }
        }
    }
}
