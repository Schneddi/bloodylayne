﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class ActivateNosferatuTestMode : MonoBehaviour
    {
        void Awake()
        {
            GameObject.Find("Nosferatu").GetComponent<Nosferatu>().StartScene();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
