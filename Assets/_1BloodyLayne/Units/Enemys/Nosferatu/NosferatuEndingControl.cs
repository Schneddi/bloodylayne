using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class NosferatuEndingControl : MonoBehaviour
{
    public GameObject listenDialog, shield, firstDialog;
    
    private bool listen, fly, kneeling;
    private Animator animator, larryAnimator;
    private Rigidbody2D rb, layneRb;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        larryAnimator = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
        GameObject.FindWithTag("Player").GetComponent<PlayerStats>().health = GameObject.FindWithTag("Player").GetComponent<PlayerStats>().maxHealth;
        layneRb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
        layneRb.constraints = RigidbodyConstraints2D.FreezeAll;
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (fly)
        {
            rb.AddForce(new Vector2(-200, 450));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (firstDialog.activeSelf && !kneeling)
        {
            kneeling = true;
            larryAnimator.SetTrigger("Kneel");
        }

        if (listenDialog.activeSelf && !listen)
        {
            listen = true;
        }
        if (!listenDialog.activeSelf && listen)
        {
            listen = false;
            StartCoroutine(flyAway());
        }
    }


    IEnumerator flyAway() //fliegt in die Luft und schiesst Todesmagie auf die Spielerkoordinaten
    {
        fly = true;
        animator.SetTrigger("Fliegen");
        GameObject shieldChild = Instantiate(shield, transform.position, Quaternion.identity);
        shieldChild.GetComponent<Schild>().friendlyScene = true;
        shieldChild.transform.parent = gameObject.transform;
        larryAnimator.SetTrigger("StandUp");
        layneRb.constraints = RigidbodyConstraints2D.FreezeRotation;
        yield return new WaitForSeconds(2f);
        larryAnimator.GetComponent<PlatformerCharacter2D>().InitializeSkills(true);
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        yield return new WaitForSeconds(18f);
        Destroy(gameObject);
    }
}
