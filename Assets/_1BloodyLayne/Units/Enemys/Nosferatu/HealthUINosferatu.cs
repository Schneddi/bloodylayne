﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;
using UnityEngine.UI;


namespace UnityStandardAssets._2D
{
    public class HealthUINosferatu : MonoBehaviour
    {

        public Image heart2;
        public Sprite[] heartSprite;
        DamageUIScript damageScript;
        EnemyStats stats;
        Text nosHealthText, nosMaxHealthText;
        bool activated;
        public Image lebenInnenImageNos;
        bool fadingIn, fadingOut;

        // Use this for initialization
        void Start()
        {
            stats = GameObject.Find("Nosferatu").GetComponent<EnemyStats>();
            nosHealthText = GameObject.Find("nosHealth").GetComponent<Text>();
            nosMaxHealthText = GameObject.Find("nosMaxHealth").GetComponent<Text>();
            //health text was for showing enemy HP, but has been disabled for immersion
            nosHealthText.text = null;
            nosHealthText.text = null;
        }

        // Update is called once per frame
        void Update()
        {
            if (activated == true)
            {
                ChangingUIHealth();
            }
            if (fadingIn)
            {
                Image[] images = GetComponentsInChildren<Image>();
                images[0].color = new Color(images[0].color.r, images[0].color.g, images[0].color.g,
                    images[0].color.a + Time.deltaTime);
                images[1].color = new Color(images[1].color.r, images[1].color.g, images[1].color.g,
                    images[1].color.a + Time.deltaTime);
                Text[] texts = GetComponentsInChildren<Text>();
                texts[0].color = new Color(texts[0].color.r, texts[0].color.g, texts[0].color.g,
                    texts[0].color.a + Time.deltaTime);
                texts[1].color = new Color(texts[1].color.r, texts[1].color.g, texts[1].color.g,
                    texts[1].color.a + Time.deltaTime);
                if (texts[1].color.a >= 1)
                {
                    fadingIn = false;
                }
            }
            if (fadingOut)
            {
                Image[] images = GetComponentsInChildren<Image>();
                images[0].color = new Color(images[0].color.r, images[0].color.g, images[0].color.g,
                    images[0].color.a - Time.deltaTime);
                images[1].color = new Color(images[1].color.r, images[1].color.g, images[1].color.g,
                    images[1].color.a - Time.deltaTime);
                Text[] texts = GetComponentsInChildren<Text>();
                texts[0].color = new Color(texts[0].color.r, texts[0].color.g, texts[0].color.g,
                    texts[0].color.a - Time.deltaTime);
                texts[1].color = new Color(texts[1].color.r, texts[1].color.g, texts[1].color.g,
                    texts[1].color.a - Time.deltaTime);
                if (texts[1].color.a >= 1)
                {
                    fadingOut = false;
                }
            }
        }

        void ChangingUIHealth()
        {
            if (stats.health >= 0)
            {
                heart2.enabled = true;
                /*
                nosHealthText.text = (stats.health.ToString());
                nosMaxHealthText.text = (stats.maxHealth.ToString());
                */
                lebenInnenImageNos.fillAmount = ((float)stats.health / (float)stats.maxHealth);
            }
            else
            {
            	heart2.enabled = false;
            }
        }
        public void aktivieren()
        {
            activated = true;
            fadeIn();

        }
        public void deaktivieren()
        {
            activated = false;
            fadeOut();
        }

        public void fadeIn()
        {
            fadingIn = true;
            fadingOut = false;
        }

        public void fadeOut()
        {
            fadingOut = true;
            fadingIn = false;
        }
    }
}