﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using Units.Enemys;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityEngine.SceneManagement;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Nosferatu : MonoBehaviour
    {
        public GameObject DialogAnf, DialogZwi, DialogGut, DialogBoe, DialogBoe2, DialogBoe3, DialogDae1, DialogServantEnding, servEndServantPosStart;
        public GameObject servant;
        public bool humanEndingScene, vampireEndingScene, devilEndingScene, servantEndingScene;
        public Rigidbody2D rb;
        public bool facingRight = true, batsAlive = true, startingDialog, servantDialog;
        public GameObject schild, strahl, todesmagie, fledermaus, schildLeer, blutEffekt, strahlEffekt, strahlEffekt2, listenStartDialog, larryChangeEffect, bossPickUp;

        private EnemyStats stats;
        private bool listenToDialog, round1, round2, round3, eRound4, round4, intermediateDialog, endDialogGoodEnd1, endDialogVampireEnd1, endDialogVampireEnd2, endDialogDaemonEnd1; //charakter geht Verhalten in dieser Reihenfolge durch
        private bool flipActive = true;
        private Transform beamSpawn, sideSpawn1, sideSpawn2, groundCheck, collisionCheck;
        private Animator animator;
        private NosferatuDialogControl nosDialogControl;
        private GameObject player, bueste;
        private float ursprungsX, ursprungsY;
        private bool still, mitteRaum, ursprung, ursprungY, verfolgen, larryWalking, larryFlug, servantWalking; //bewegungen
        private bool buestenwurf;
        private BossUI bossUI;
        private float todesmagieSpamX = 0, todesmagieSpamY = 1, todesmagieRadiant = 5.0f;
        private AudioSource[] sounds;
        private AudioSource lachen, buesteSchweben, deathSound, schildAus, strahlSound, larryVerwandlung, larryLachen, larryTeufel, splatterSound;
        private int dialoge = 0;
        private Coroutine flackerschildCoroutineRef;
        private GameObject flickerShieldChild;
        private GlobalVariables gv;
        private GameController gc;
        private Dialog dialog;
#pragma warning disable 0414
        private bool round5; //Saves when nosferatu is in last round. we never use it, but would be nice to have if needed later
#pragma warning restore 0414

        // Use this for initialization
        void Awake()
        {
            stats = GetComponent<EnemyStats>();
            animator = GetComponent<Animator>();
            rb = GetComponent<Rigidbody2D>();
            beamSpawn = GameObject.Find("StrahlSpawn").transform;
            sideSpawn1 = GameObject.Find("SeiteSpawn1").transform;
            sideSpawn2 = GameObject.Find("SeiteSpawn2").transform;
            groundCheck = GameObject.Find("groundCheck").transform;
            collisionCheck = GameObject.Find("CollisionCheck").transform;
            bueste = GameObject.Find("Bueste");
            player = GameObject.FindGameObjectWithTag("Player");
            ursprungsX = transform.position.x;
            ursprungsY = transform.position.y;
            bossUI = GameObject.Find("BossUI").GetComponent<BossUI>();
            nosDialogControl = GameObject.Find("NosferatuDialogControl").GetComponent<NosferatuDialogControl>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
            sounds = GetComponents<AudioSource>();
            lachen = sounds[0];
            buesteSchweben = sounds[1];
            deathSound = sounds[2];
            schildAus = sounds[3];
            strahlSound = sounds[4];
            larryVerwandlung = sounds[5];
            larryLachen = sounds[6];
            if (gv.gender == 0)
            {
                larryTeufel = sounds[7];
            }
            else if (gv.gender == 1)
            {
                larryTeufel = sounds[8];
            }
            splatterSound = sounds[9];
            dialog = FindObjectOfType<Dialog>();
        }

            // Update is called once per frame
            void Update()
        {
            CheckHealth();
            if ((transform.position.x > player.transform.position.x) && facingRight)
            {
                Flip();
            }
            else if ((transform.position.x < player.transform.position.x) && !facingRight)
            {
                Flip();
            }
        }
            
            
        private void FixedUpdate()
        {
            if (still)
            {
                rb.velocity = new Vector2(0, 0);
            }
            else if (mitteRaum)
            {
                if (transform.position.y - 4 < ursprungsY)
                {
                    rb.velocity = new Vector2(0f, 3f);
                }
                else
                {
                    rb.velocity = new Vector2(0, 0);
                }
            }
            else if (ursprungY)
            {
                if (transform.position.y != ursprungsY)
                {
                    float richtung = ursprungsY - rb.position.y;
                    rb.velocity = new Vector2(rb.velocity.x, richtung);
                }
                else
                {
                    rb.velocity = new Vector2(0, 0);
                }

            }
            else if (ursprung)
            {
                if (transform.position.x != ursprungsX || transform.position.y != ursprungsY)
                {
                    Vector2 richtung = new Vector2(ursprungsX, ursprungsY) - rb.position;
                    rb.velocity = richtung;
                }
                else
                {
                    rb.velocity = new Vector2(0, 0);
                }

            }
            else if (verfolgen)
            {
                Vector2 facingCheck;
                if (facingRight)
                {
                    facingCheck = Vector2.right;
                }
                else
                {
                    facingCheck = Vector2.left;
                }
                if (Physics2D.Raycast(collisionCheck.position, Vector2.up, 1.9f, 1 << 11) && !Physics2D.Raycast(groundCheck.position, facingCheck, 8f, 1 << 8))
                {
                    Vector2 richtung = player.GetComponent<Rigidbody2D>().position - rb.position;
                    rb.velocity = new Vector2(rb.velocity.x * richtung.normalized.x * 0.09f, richtung.normalized.y * 0.09f + 2.5f);
                }
                else if (Physics2D.OverlapCircle(groundCheck.position, 0.2f, 1 << 11) && player.transform.position.y < transform.position.y)
                {
                    Vector2 richtung = player.GetComponent<Rigidbody2D>().position - rb.position;
                    rb.velocity = new Vector2(rb.velocity.x + richtung.normalized.x * 0.09f, 1f);
                }
                else
                {
                    Vector2 richtung = player.GetComponent<Rigidbody2D>().position - rb.position;
                    rb.velocity = new Vector2(rb.velocity.x + richtung.normalized.x * 0.09f, richtung.normalized.y * 2f);
                }
            }


            //events (Chronologisch)
            if (startingDialog)
            {
                if (nosDialogControl.servantEnding && !listenStartDialog.activeSelf)
                {
                    startingDialog = false;
                    gc.SetAchievement("SERVANT_TRANSFORM");
                    StartCoroutine(ending());
                }
                else
                {
                    if (listenStartDialog.activeSelf)
                    {
                        listenToDialog = true;
                    }
                    if (!listenStartDialog.activeSelf && listenToDialog)
                    {
                        listenToDialog = false;
                        startingDialog = false;
                        StartCoroutine(AnfangsDialogEnde());
                    }
                }
            }
            if (intermediateDialog)
            {
                if (dialog.dialogActive == false)
                {
                    intermediateDialog = false;
                    round5 = true;
                    //dialoge = 0;
                    StartCoroutine(BuesteWerfen());
                }

            }
            if (buestenwurf)
            {
                Vector2 richtung = new Vector2(rb.position.x, rb.position.y + 4) - bueste.GetComponent<Rigidbody2D>().position;
                bueste.GetComponent<Rigidbody2D>().velocity = new Vector2(bueste.GetComponent<Rigidbody2D>().velocity.x / 2 + richtung.x, bueste.GetComponent<Rigidbody2D>().velocity.y / 2 + richtung.y);
            }
            if (endDialogGoodEnd1)
            {
                if (dialog.dialogActive == false)
                {
                    endDialogGoodEnd1 = false;
                    //dialoge = 0;
                    StartCoroutine(eDialogGut2u1());
                }
            }
            if (endDialogVampireEnd1)
            {
                if (dialog.dialogActive == false)
                {
                    endDialogVampireEnd1 = false;
                    //dialoge = 0;
                    StartCoroutine(eDialogBoese2u1());
                }
            }
            if (endDialogVampireEnd2)
            {
                if (dialog.dialogActive == false && dialoge == 0)
                {
                    endDialogVampireEnd2 = false;
                    StartCoroutine(eDialogBoese2u3());
                }
                if (dialog.dialogActive == false && dialoge == 1)
                {
                    endDialogVampireEnd2 = false;
                    dialoge = 0;
                    nosDialogControl.nosferatuEnding = true;
                    gc.SetAchievement("NOSFERATU_TRANSFORM");
                    StartCoroutine(ending());
                }
            }
            if (larryWalking)
            {
                if (player.transform.position.x > transform.position.x - 0.15f && player.transform.position.x < transform.position.x + 0.15f)
                {
                    larryWalking = false;
                    player.GetComponent<Platformer2DUserControl>().enabled = true;
                }
                else
                {
                    if (player.transform.position.x > transform.position.x)
                    {
                        player.GetComponent<PlatformerCharacter2D>().EvaluateInput(new Vector2(-0.2f, 0), false, false, false, false, false, Vector2.zero);
                    }
                    else
                    {
                        player.GetComponent<PlatformerCharacter2D>().EvaluateInput(new Vector2(0.2f,0), false, false, false, false, false, Vector2.zero);
                    }
                }
            }
            if (servantWalking)
            {
                if (servant.transform.position.x > transform.position.x - 5f && servant.transform.position.x < transform.position.x + 5f)
                {
                    servantWalking = false;
                    servant.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    servant.GetComponent<Animator>().SetTrigger("ServantKneel");
                }
                else
                {
                    servant.GetComponent<Rigidbody2D>().velocity = new Vector2(7, 0);
                }
            }
            if (endDialogDaemonEnd1)
            {
                if (dialog.dialogActive == false)
                {
                    endDialogDaemonEnd1 = false;
                    //dialoge = 0;
                    StartCoroutine(eDialogDaemon2u1());
                }
            }
            if (larryFlug) {
                if (player.transform.position.y - 4 < ursprungsY)
                {
                    player.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 3f);
                }
                else
                {
                    player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                }
            }
            if (servantDialog)
            {
                if (dialog.dialogActive == false)
                {
                    servantDialog = false;
                    gc.SetAchievement("SERVANT_TRANSFORM");
                    StartCoroutine(ending());
                }
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------------------------------
        //Verhalten:
        //Erster Dialog-
        IEnumerator AnfangsDialogStart()
        {
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            yield return new WaitForSeconds(1.4f);
            AudioSource[] camSounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponents<AudioSource>();
            camSounds[0].volume *= 0.25f;
            dialog.ZeigeBox();
        }

        IEnumerator AnfangsDialogEnde()
        {
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            animator.SetTrigger("Lachen");
            lachen.PlayDelayed(0);
            yield return new WaitForSeconds(1.4f);
            animator.SetTrigger("Idle");
            yield return new WaitForSeconds(2f);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = player.transform;
            yield return new WaitForSeconds(0.5f);
			round1 = true;
            bossUI.activateBossUI();
            StartCoroutine(Schild1());
            AudioSource[] camSounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponents<AudioSource>();
            camSounds[0].enabled = false;
            camSounds[1].enabled = true;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().CurrentMusicClip = camSounds[1];
            camSounds[1].PlayDelayed(0);
            camSounds[1].volume = gv.musicVolume;
            player.GetComponent<Platformer2DUserControl>().BlockInput = false;
            player.GetComponent<PlatformerCharacter2D>().InitializeSkills(true);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().ResetDamping();
            gv.nosferatuIntroDone = true;
        }

        //runde1
        IEnumerator Schild1()//fliegt in die Luft und schiesst Todesmagie auf die Spielerkoordinaten
        {
            mitteRaum = true;
            animator.SetTrigger("Fliegen");
            stats.unverwundbar = true;
            GameObject shieldChild = Instantiate(schild, transform.position, Quaternion.identity);
            shieldChild.GetComponent<Schild>().attackDamage = stats.attacks.GetValueOrDefault("Shield");
            shieldChild.transform.parent = gameObject.transform;
            int numberOfShots = 8;
            if (gv.difficulty >= 3)
            {
                numberOfShots += (gv.difficulty - 2) * 6;
            }
            for (int i=0;i<numberOfShots;i++) {
                float timeBetweenShots = 1f;
                if (gv.difficulty >= 3)
                {
                    timeBetweenShots -= (gv.difficulty - 2) * 0.3f;
                }
                yield return new WaitForSeconds(timeBetweenShots);
                GameObject deathBolt = Instantiate(todesmagie, sideSpawn2.position, Quaternion.identity);
                deathBolt.GetComponent<Todesmagie>().abschuss1();
                deathBolt.GetComponent<Todesmagie>().attackDamage = stats.attacks.GetValueOrDefault("DeathBolt");
            }
            animator.SetTrigger("Runterfliegen");
            mitteRaum = false;
            ursprungY = true;
            yield return new WaitForSeconds(4f);
            schildAus.PlayDelayed(0);
            Destroy(shieldChild);
            stats.unverwundbar = false;
            StartCoroutine(Seitenschuss());
        }
        
        IEnumerator Seitenschuss()//schiesst zu den Seiten
        {
            animator.SetTrigger("Seitenschuss");
            yield return new WaitForSeconds(0.3f);
            for (int i = 0; i < 2; i++)
            {
                float xPush = 2;
                if (facingRight)
                {
                    xPush *= -1;
                }
                GameObject deathBolt = Instantiate(todesmagie, sideSpawn2.position, Quaternion.identity);
                deathBolt.GetComponent<Todesmagie>().abschuss2(-xPush, 0);
                deathBolt.GetComponent<Todesmagie>().attackDamage = stats.attacks.GetValueOrDefault("DeathBolt");
                GameObject deathBolt2 = Instantiate(todesmagie, sideSpawn1.position, Quaternion.identity);
                deathBolt2.GetComponent<Todesmagie>().abschuss2(xPush, 0);
                deathBolt2.GetComponent<Todesmagie>().attackDamage = stats.attacks.GetValueOrDefault("DeathBolt");
                float timeBetweenShots = 0.7f;
                xPush = 2;
                if (gv.difficulty >= 3)
                {
                    xPush = 3;
                    timeBetweenShots = 0.4f;
                }
                yield return new WaitForSeconds(timeBetweenShots);
                if (facingRight)
                {
                    xPush *= -1;
                }
                deathBolt = Instantiate(todesmagie, sideSpawn2.position, Quaternion.identity);
                deathBolt.GetComponent<Todesmagie>().abschuss2(-xPush*10, 0);
                deathBolt.GetComponent<Todesmagie>().attackDamage = stats.attacks.GetValueOrDefault("DeathBolt");
                deathBolt2 = Instantiate(todesmagie, sideSpawn1.position, Quaternion.identity);
                deathBolt2.GetComponent<Todesmagie>().abschuss2(xPush*10, 0);
                deathBolt2.GetComponent<Todesmagie>().attackDamage = stats.attacks.GetValueOrDefault("DeathBolt");
                yield return new WaitForSeconds(timeBetweenShots);
            }
            if (round1) {
                StartCoroutine(Schild1());
            }
            else
            {
                StartCoroutine(Schild2());
            }
        }

        //Runde2
        IEnumerator Schild2()//fliegt in die Luft und spawnt Fledermäuse
        {
            GameObject bossPickUpInstance = Instantiate(bossPickUp, sideSpawn1.position, Quaternion.identity);
            Vector2 bossPickUpPush = new Vector2(500, 50);
            float rotation = 50;
            if (facingRight)
            {
                bossPickUpPush.x *= -1;
                rotation *= -1;
            }
            bossPickUpInstance.GetComponent<BossPickup>().spawn(bossPickUpPush, rotation);
            mitteRaum = true;
            animator.SetTrigger("Fliegen");
            stats.unverwundbar = true;
            GameObject shieldChild = Instantiate(schild, transform.position, Quaternion.identity);
            shieldChild.GetComponent<Schild>().attackDamage = stats.attacks.GetValueOrDefault("Shield");
            shieldChild.transform.parent = gameObject.transform;
            batsAlive = true;
            yield return new WaitForSeconds(1f);
            for (int i = 0; i < 6; i++)
            {
                yield return new WaitForSeconds(0.2f);
                float xSpawner = 2;
                if (facingRight)
                {
                    xSpawner *= -1;
                }
                GameObject fledermausSpawned1 = Instantiate(fledermaus, sideSpawn1.position + new Vector3(xSpawner, 2-i*0.7f, transform.position.z), Quaternion.identity);
                fledermausSpawned1.GetComponent<Rigidbody2D>().velocity = new Vector2(fledermausSpawned1.GetComponent<Rigidbody2D>().velocity.x + xSpawner * 2, fledermausSpawned1.GetComponent<Rigidbody2D>().velocity.y);
                fledermausSpawned1.GetComponent<EnemyFledermaus>().secondsUntilFlip=2;
                fledermausSpawned1.GetComponent<EnemyFledermaus>().bewegungx *= -1;
                yield return new WaitForSeconds(0.2f);
                xSpawner = 2;
                if (facingRight)
                {
                    xSpawner *= -1;
                }
                GameObject fledermausSpawned2 = Instantiate(fledermaus, sideSpawn2.position + new Vector3(-xSpawner, 2-i*0.7f, transform.position.z), Quaternion.identity);
                fledermausSpawned2.GetComponent<Rigidbody2D>().velocity = new Vector2(fledermausSpawned2.GetComponent<Rigidbody2D>().velocity.x-xSpawner*2, fledermausSpawned2.GetComponent<Rigidbody2D>().velocity.y);
                fledermausSpawned2.GetComponent<EnemyFledermaus>().secondsUntilFlip = 2;
            }
            mitteRaum = false;
            ursprungY = true;
            yield return new WaitForSeconds(5f);
            ursprungY = false;
            if (round2)
            {
                round2 = false;
                round3 = true;
                Destroy(shieldChild);
                StartCoroutine(Schild3());
            }
            if (eRound4)
            {
                eRound4 = false;
                Destroy(shieldChild);
                bossUI.shiftRight();
                StartCoroutine(Schild4());
            }
        }

        //Runde3
        IEnumerator Schild3()//fliegt dem Spieler hinterher
        {
            animator.SetTrigger("Fliegen");
            verfolgen = true;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            GameObject shieldChild = Instantiate(schild, transform.position, Quaternion.identity);
            shieldChild.GetComponent<Schild>().attackDamage = stats.attacks.GetValueOrDefault("Shield");
            shieldChild.transform.parent = gameObject.transform;
            stats.unverwundbar = true;
            yield return new WaitForSeconds(20f);
            animator.SetTrigger("Runterfliegen");
            verfolgen = false;
            ursprung = true;
            yield return new WaitForSeconds(6f);
            transform.position = new Vector3(ursprungsX,ursprungsY, transform.position.z);
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
            ursprung = false;
            stats.unverwundbar = false;
            Destroy(shieldChild);
            schildAus.PlayDelayed(0);
            StartCoroutine(StrahlLine());
        }


        IEnumerator StrahlLine()//schiesst einen Strahl nach links oder rechts richtung spieler
        {
            animator.SetTrigger("Strahlschuss");
            strahlSound.PlayDelayed(0);
            flipActive = false;
            float castingTime = 0.5f;
            if (gv.difficulty >= 3)
            {
                castingTime = 0.4f;
            }
            yield return new WaitForSeconds(castingTime);
            Instantiate(strahlEffekt, beamSpawn.position, Quaternion.identity);
            GameObject beamEffectsChild = Instantiate(strahlEffekt2, beamSpawn.position, Quaternion.identity);
            GameObject beamChild = Instantiate(strahl, beamSpawn.position, Quaternion.identity);
            beamChild.GetComponent<Strahl>().attackDamagePerSecond = stats.attacks.GetValueOrDefault("BeamAttack");
            beamChild.transform.parent = gameObject.transform;
            if (!facingRight)
            {
                beamChild.transform.localScale *= -1;
                beamEffectsChild.transform.localScale *= -1;
            }
            yield return new WaitForSeconds(3.0f);
            flipActive = true;
            animator.SetTrigger("Idle");
            Destroy(beamChild);
            Destroy(beamEffectsChild);
            strahlSound.Stop();
            if (round3)
            {
                StartCoroutine(Schild3());
            }
            else
            {
                eRound4 = true;
                StartCoroutine(Schild2());
            }
        }


        //Runde4
        IEnumerator Schild4()//fliegt in die Luft und schießt Todesmagie ueberall hin
        {
            animator.SetTrigger("Fliegen");
            mitteRaum = true;
            GameObject shieldChild = Instantiate(schild, transform.position, Quaternion.identity);
            shieldChild.GetComponent<Schild>().attackDamage = stats.attacks.GetValueOrDefault("Shield");
            shieldChild.transform.parent = gameObject.transform;
            stats.unverwundbar = true;
            yield return new WaitForSeconds(1.0f);
            animator.SetTrigger("Lachen");
            lachen.PlayDelayed(0);
            yield return new WaitForSeconds(1.4f);
            animator.SetTrigger("Fliegen");
            for (float i = 2; i < 402; i++)
            {
                TodesmagieSpam();
                float numerator = 2.5f;
                if (gv.difficulty <= 2)
                {
                    numerator = 4f;
                }
                yield return new WaitForSeconds(numerator/i);
            }
            animator.SetTrigger("Runterfliegen");
            mitteRaum = false;
            yield return new WaitForSeconds(2f);
            stats.unverwundbar = false;
            Destroy(shieldChild.gameObject);
            schildAus.PlayDelayed(0);
            StartCoroutine(StrahlLine2());
        }

        IEnumerator StrahlLine2()//schießt einen Strahl auf die Spielerkoordinaten
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
            mitteRaum = true;
            for (int i=0;i<2;i++) {
                flipActive = false;
                Vector3 richtung = player.transform.position - transform.position;
                Quaternion q = Quaternion.LookRotation(richtung);
                q.y = 0;
                q.x = 0;
                transform.rotation = q;
                animator.SetTrigger("Strahlschuss");
                strahlSound.PlayDelayed(0);
                float castingTime = 0.5f;
                if (gv.difficulty >= 3)
                {
                    castingTime = 0.4f;
                }
                yield return new WaitForSeconds(castingTime);
                Instantiate(strahlEffekt, beamSpawn.position, q);
                GameObject beamEffectsChild = Instantiate(strahlEffekt2, beamSpawn.position, q);
                GameObject beamChild = Instantiate(strahl, beamSpawn.position, q);
                beamChild.GetComponent<Strahl>().attackDamagePerSecond = stats.attacks.GetValueOrDefault("BeamAttack");
                beamChild.transform.parent = gameObject.transform;
                if (!facingRight)
                {
                    beamChild.transform.localScale *= -1;
                    beamEffectsChild.transform.localScale *= -1;
                }
                yield return new WaitForSeconds(0.5f);
                transform.rotation = Quaternion.identity;
                flipActive = true;
                animator.SetTrigger("Idle");
                Destroy(beamChild);
                Destroy(beamEffectsChild);
                strahlSound.Stop();
                yield return new WaitForSeconds(1.5f);
            }
            mitteRaum = false;
            if (round4)
            {
                StartCoroutine(Schild4());
            }
            else
            {
                ZwischenDialog();
            }
        }

        //Runde5

        void ZwischenDialog()
        {
            if (batsAlive)
            {
                EnemyFledermaus[] fledermaeuse = FindObjectsOfType<EnemyFledermaus>();
                foreach (EnemyFledermaus maus in fledermaeuse)
                {
                    maus.GetComponent<EnemyStats>().health = 0;
                }
                batsAlive = false;
            }
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = GetComponent<Transform>();
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().SetDamping(0.2f);
            dialog.ZeigeBox(DialogZwi);
            intermediateDialog = true;
        }

        IEnumerator BuesteWerfen()//der nosferatu nimmt seine Bueste und bedroht damit den Spieler, schafft es der Spieler, seinen geschwaechten schild zu ueberwinden, hat er gewonnen
        {
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            bossUI.shiftMiddle();
            stats.health = 20;
            ursprungY = true;
            mitteRaum = false;
            stats.unverwundbar = true;
            yield return new WaitForSeconds(2f);
            animator.SetTrigger("BuesteWerfen1");
            buestenwurf = true;
            buesteSchweben.PlayDelayed(0);
            yield return new WaitForSeconds(2f);
            stats.unverwundbar = false;
            flackerschildCoroutineRef = StartCoroutine(FlackerSchild());
            yield return new WaitForSeconds(4.5f);
            StartCoroutine(BuesteWerfen2());
        }
        
        IEnumerator BuesteWerfen2()
        {
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = player.transform;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().ResetDamping();
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().orthographicSize=8f;
            player.GetComponent<Platformer2DUserControl>().BlockInput = false;
            player.GetComponent<PlatformerCharacter2D>().StopInvulnerable();
            yield return new WaitForSeconds(4f);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = GetComponent<Transform>();
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().SetDamping(0.2f);
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            //rb.gravityScale = 1;
            if (stats.health<300)
            {
                StopCoroutine(flackerschildCoroutineRef);
                Destroy(flickerShieldChild.gameObject);
                stats.health = 0;
                buestenwurf = false;
                bueste.GetComponent<Bueste>().fallen = true;
                yield return new WaitForSeconds(0.3f);
                bueste.GetComponent<Animator>().SetTrigger("Bruch");
                bueste.GetComponent<Bueste>().bruch = true;
                yield return new WaitForSeconds(0.5f);
                GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                animator.SetTrigger("Stunned");
                yield return new WaitForSeconds(1.1f);
                bueste.GetComponent<Bueste>().fallen = false;
                bueste.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                bossUI.deactivateBossUI();
                if (nosDialogControl.humanEnding)
                {
                    eDialogGut1u1();
                }
                else if(nosDialogControl.devilEnding)
                {
                    eDialogDaemon1u1();
                }
                else if(nosDialogControl.nosferatuEnding)
                {
                    eDialogBoese1u1();
                }
            }
            else
            {
                buestenwurf = false;
                animator.SetTrigger("BuesteWerfen2");
                yield return new WaitForSeconds(0.5f);
                player.GetComponent<Platformer2DUserControl>().BlockInput = false;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = player.transform;
                bueste.GetComponent<Bueste>().wurf = true;
                bueste.GetComponent<BoxCollider2D>().enabled=true;
            }
        }

        
        void eDialogGut1u1()
        {
            AudioSource[] camSounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponents<AudioSource>();
            camSounds[1].enabled = false;
            camSounds[2].enabled = true;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().CurrentMusicClip = camSounds[2];
            camSounds[2].PlayDelayed(0);
            camSounds[2].volume = gv.musicVolume;
            dialog.ZeigeBox(DialogGut);
            endDialogGoodEnd1 = true;
        }
        

        IEnumerator eDialogGut2u1()
        {
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            animator.SetTrigger("Death");
            deathSound.PlayDelayed(0);
            yield return new WaitForSeconds(1f);
            larryVerwandlung.PlayDelayed(0.5f);
            Instantiate(larryChangeEffect, player.transform.position, Quaternion.identity);
            yield return new WaitForSeconds(0.25f);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = player.transform;
            
            GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().playerSkin = 1;
            SkinSelector[] skinSelectors = GameObject.FindGameObjectWithTag("Player").GetComponentsInChildren<SkinSelector>(true);
            foreach (SkinSelector skinSelector in skinSelectors)
            {
                skinSelector.InitSkin();
            }
            yield return new WaitForSeconds(3f);
            gc.SetAchievement("HUMAN_TRANSFORM");
            StartCoroutine(ending());
        }
        void eDialogBoese1u1()
        {
            AudioSource[] camSounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponents<AudioSource>();
            camSounds[1].enabled = false;
            camSounds[4].enabled = true;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().CurrentMusicClip = camSounds[4];
            camSounds[4].PlayDelayed(0);
            camSounds[4].volume = gv.musicVolume;
            endDialogVampireEnd1 = true;
            dialog.ZeigeBox(DialogBoe);
        }
        IEnumerator eDialogBoese2u1()
        {
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            animator.SetTrigger("Death");
            deathSound.PlayDelayed(0);
            yield return new WaitForSeconds(1f);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = player.transform;
            endDialogVampireEnd2 = true;
            dialog.ZeigeBox(DialogBoe2);
        }
        IEnumerator eDialogBoese2u3()
        {
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            GetComponent<BoxCollider2D>().enabled = false;
            larryWalking = true;
            player.GetComponent<Platformer2DUserControl>().enabled = false;
            servant.transform.position = servEndServantPosStart.transform.position;
            yield return new WaitForSeconds(3.5f);
            servantWalking = true;
            larryWalking = false;
            player.GetComponent<Platformer2DUserControl>().enabled = true;
            servant.GetComponent<Animator>().SetTrigger("Walk");
            player.GetComponent<Animator>().SetTrigger("PickMantel");
            servant.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            yield return new WaitForSeconds(1.33f);
            animator.SetTrigger("Strip");
            GetComponent<BoxCollider2D>().size = new Vector2(1.6f, 3.6f);
            
            GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().playerSkin = 2;
            SkinSelector[] skinSelectors = GameObject.FindGameObjectWithTag("Player").GetComponentsInChildren<SkinSelector>(true);
            foreach (SkinSelector skinSelector in skinSelectors)
            {
                skinSelector.InitSkin();
                Debug.Log("Init Nos");
            }
            yield return new WaitForSeconds(0.53f);
            dialog.ZeigeBox(DialogBoe3);
            yield return new WaitForSeconds(2.5f);
            larryLachen.PlayDelayed(0);
            endDialogVampireEnd2 = true;
            dialoge++;
        }

        void eDialogDaemon1u1()
        {
            AudioSource[] camSounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponents<AudioSource>();
            camSounds[1].enabled = false;
            camSounds[3].enabled = true;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().CurrentMusicClip = camSounds[3];
            camSounds[3].PlayDelayed(0);
            camSounds[3].volume = gv.musicVolume;
            endDialogDaemonEnd1 = true;
            dialog.ZeigeBox(DialogDae1);
        }

        IEnumerator eDialogDaemon2u1()
        {
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            animator.SetTrigger("Death");
            deathSound.PlayDelayed(0);
            yield return new WaitForSeconds(1f);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = player.transform;
            player.GetComponent<Animator>().SetTrigger("VerwTeufel1");
            player.GetComponent<PlatformerCharacter2D>().overideGrounded = true;
            larryFlug = true;
            yield return new WaitForSeconds(5f);
            GetComponent<BoxCollider2D>().enabled = false;
            StartCoroutine(eDialogDaemon2u2());
            splatterSound.PlayDelayed(0);
            Instantiate(blutEffekt, player.transform.position, Quaternion.identity);
            Instantiate(blutEffekt, new Vector3(player.transform.position.x, player.transform.position.y + 0.5f, player.transform.position.z), Quaternion.identity);
            Instantiate(blutEffekt, new Vector3(player.transform.position.x, player.transform.position.y - 0.5f, player.transform.position.z), Quaternion.identity);
            Instantiate(blutEffekt, new Vector3(player.transform.position.x, player.transform.position.y + 1f, player.transform.position.z), Quaternion.identity);
            Instantiate(blutEffekt, new Vector3(player.transform.position.x, player.transform.position.y - 1f, player.transform.position.z), Quaternion.identity);
            
            GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().playerSkin = 3;
            SkinSelector[] skinSelectors = GameObject.FindGameObjectWithTag("Player").GetComponentsInChildren<SkinSelector>(true);
            foreach (SkinSelector skinSelector in skinSelectors)
            {
                skinSelector.InitSkin();
            }
            player.GetComponent<Animator>().SetTrigger("TeufelLachen");
            larryTeufel.PlayDelayed(0);
            yield return new WaitForSeconds(5f);
            larryFlug = false;
            player.GetComponent<Animator>().SetTrigger("VerwTeufel2");
            yield return new WaitForSeconds(5f);
            nosDialogControl.devilEnding = true;
            gc.SetAchievement("DEVIL_TRANSFORM");
            StartCoroutine(ending());
        }

        IEnumerator eDialogDaemon2u2()
        {
            Instantiate(blutEffekt, new Vector3(player.transform.position.x + Random.Range(-10f, 10.0f), player.transform.position.y + 2f + Random.Range(3f, 4.0f), player.transform.position.z), Quaternion.identity);
            yield return new WaitForSeconds(0.1f);
            Instantiate(blutEffekt, new Vector3(player.transform.position.x + Random.Range(-10f, 10.0f), player.transform.position.y + 2f + Random.Range(2f, 3.0f), player.transform.position.z), Quaternion.identity);
            yield return new WaitForSeconds(0.1f);
            StartCoroutine(eDialogDaemon2u2());
        }

        IEnumerator ending()
        {
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            int nextScene = 0;
            gv.firstPlaythroughDone = true;

            if (nosDialogControl.humanEnding)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Save>().saveHumanEnding();
                nextScene = 15;
            }
            if (nosDialogControl.servantEnding)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Save>().saveServantEnding();
                nextScene = 17;
            }
            if (nosDialogControl.nosferatuEnding)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Save>().saveVampireEnding();
                nextScene = 19;
            }
            if (nosDialogControl.devilEnding)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Save>().saveDevilEnding();
                nextScene = 21;
            }
            yield return new WaitForSeconds(5f);
            GameController gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
            gc.SetFirstPlaythroughDone();
            gc.NextLevel(nextScene);
        }


//-----------------------------------------------------------------------------------------------------------------------------------------------------------
//Methoden
IEnumerator FlackerSchild()//geschwaechter schild, der kurzzeitig angreifbar ist
        {
            while (stats.health != 0)
            {
                flickerShieldChild = Instantiate(schild, transform.position, Quaternion.identity);
                flickerShieldChild.GetComponent<Schild>().attackDamage = stats.attacks.GetValueOrDefault("Shield");
                flickerShieldChild.transform.parent = gameObject.transform;
                stats.unverwundbar = true;
                yield return new WaitForSeconds(2f);
                stats.unverwundbar = false;
                Destroy(flickerShieldChild.gameObject);
                for (int i=0; i<10; i++) {
                    flickerShieldChild = Instantiate(schildLeer, transform.position, Quaternion.identity);
                    flickerShieldChild.transform.parent = gameObject.transform;
                    yield return new WaitForSeconds(0.2f);
                    Destroy(flickerShieldChild.gameObject);
                }
            }
        }

        IEnumerator nextRound(float zeit)//gibt einen Schild, damit der nosferatu Zeit hat seine letzte Attacke abzuschließen und in die nächste runde zu gehen
        {
            GameObject shieldChild = Instantiate(schild, transform.position, transform.rotation);
            shieldChild.GetComponent<Schild>().attackDamage = stats.attacks.GetValueOrDefault("Shield");
            shieldChild.transform.parent = gameObject.transform;
            stats.unverwundbar = true;
            yield return new WaitForSeconds(zeit);
            stats.unverwundbar = false;
            Destroy(shieldChild.gameObject);
        }

        void TodesmagieSpam()
        {
            todesmagieRadiant += 0.2f * 1/Time.deltaTime;
            if(todesmagieRadiant > 2*Mathf.PI)
            {
                todesmagieRadiant = todesmagieRadiant- 2 * Mathf.PI;
            }
            todesmagieSpamX = Mathf.Sin(todesmagieRadiant);
            todesmagieSpamY = Mathf.Cos(todesmagieRadiant);
            GameObject deathBolt = Instantiate(todesmagie, new Vector2(transform.position.x + todesmagieSpamX * 1.5f, transform.position.y + todesmagieSpamY * 1.5f), Quaternion.identity);
            deathBolt.GetComponent<Todesmagie>().abschuss2(todesmagieSpamX*15, todesmagieSpamY*15);
            deathBolt.GetComponent<Todesmagie>().attackDamage = stats.attacks.GetValueOrDefault("DeathBolt");
        }

        void Flip()//z.B. in andere Richtung gucken
        {
            if (flipActive)
            {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                facingRight = !facingRight;
            }
        }
        void CheckHealth()
        {
            if (stats.health <= 0)
            {
                if (round1)
                {
                    StartCoroutine(nextRound(2f));
                    stats.health = stats.maxHealth;
                    round1 = false;
                    round2 = true;
                }
                else if (round3)
                {
                    StartCoroutine(nextRound(2f));
                    stats.health = stats.maxHealth;
                    round3 = false;
                    round4 = true;
                }
                else if (round4)
                {
                    stats.health = stats.maxHealth;
                    round4 = false;
                    StartCoroutine(nextRound(2f));
                }
            }
        }

        public void StartScene()
        {
            if (humanEndingScene)
            {
                nosDialogControl.humanEnding = true;
                animator.SetTrigger("Stunned");
                eDialogGut1u1();
            }
            else if (vampireEndingScene)
            {
                nosDialogControl.nosferatuEnding = true;
                AudioSource[] camSounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponents<AudioSource>();
                gv.werewolfIntroDone = false;
                camSounds[0].Stop();
                animator.SetTrigger("Stunned");
                eDialogBoese1u1();
            }
            else if (devilEndingScene)
            {
                nosDialogControl.devilEnding = true;
                AudioSource[] camSounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponents<AudioSource>();
                camSounds[0].Stop();
                animator.SetTrigger("Stunned");
                eDialogDaemon1u1();
            }
            else if (servantEndingScene)
            {
                AudioSource[] camSounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponents<AudioSource>();
                gv.dragonEndingScore = 0;
                camSounds[0].Stop();
                dialog.ZeigeBox(DialogServantEnding);
                //servantDialog = true;
            }
            else if (gv.nosferatuIntroDone)
            {
                nosDialogControl.decideEnding();
                StartCoroutine(AnfangsDialogEnde());
            }
            else
            {
                AudioSource[] camSounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponents<AudioSource>();
                camSounds[0].PlayDelayed(0);
                StartCoroutine(AnfangsDialogStart());
            }
        }
    }
}
 