﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Strahl : MonoBehaviour
    {
        public int attackDamagePerSecond;
        
        private Color fadeColor = new Color(1f, 1f, 1f, 0f);
        private float a = 0;
        private GameObject player, lichtkugeln;
        private bool tick=false;
        private Vector2 richtung;
        private AudioSource hitSound;
        private Guid guid = new();
        
        // Use this for initialization
        void Start()
        {
            lichtkugeln = GameObject.Find("Strahlkugeln");
            GetComponent<Renderer>().material.color = fadeColor;
            lichtkugeln.GetComponent<Renderer>().material.color = fadeColor;
            player = GameObject.FindGameObjectWithTag("Player");
            richtung = transform.position - GameObject.Find("Nosferatu").transform.position;
            hitSound = GetComponent<AudioSource>();
            StartCoroutine(SchadenStarten());
        }

        // Update is called once per frame
        private void Update()
        {
            if (a < 1)
            {
                //Transparency.
                fadeColor = new Color(1f, 1f, 1f, a);
                GetComponent<Renderer>().material.color = fadeColor;
                lichtkugeln.GetComponent<Renderer>().material.color = fadeColor;
                a = a + 0.02f;
            }
            int damage = (int)(attackDamagePerSecond * Time.deltaTime);
            lichtkugeln.transform.position = new Vector3(lichtkugeln.transform.position.x + 0.1f * richtung.normalized.x, lichtkugeln.transform.position.y + 0.1f * richtung.normalized.y, lichtkugeln.transform.position.z);
            if (tick)
            {
                tick = false;
                if (Physics2D.Raycast(transform.position, richtung.normalized, 30f, 1 << 8))
                {
                    if (!hitSound.isPlaying)
                    {
                        hitSound.PlayDelayed(0);
                    }
                    player.GetComponent<PlatformerCharacter2D>().HitBeam(damage, DamageTypeToPlayer.Burned, new Vector2(richtung.normalized.x*300 * Time.deltaTime, richtung.normalized.y * 300 * Time.deltaTime), player.transform.position, gameObject, guid);
                }
                else
                {
                    hitSound.Stop();
                }
            }
            if (hitSound.isPlaying && player.GetComponent<Animator>().GetBool("Death"))
            {
                hitSound.Stop();
            }
        }
        IEnumerator Ticker()//fliegt in die Luft und schießt Todesmagie auf die Spielerkoordinaten
        {
            while (true)
            {
                tick = true;
                yield return new WaitForSeconds(0.05f);
            }
        }

        IEnumerator SchadenStarten()//fliegt in die Luft und schießt Todesmagie auf die Spielerkoordinaten
        {
            yield return new WaitForSeconds(0.2f);
            StartCoroutine(Ticker());
        }
    }
}
