﻿using System.Collections;
using System.Collections.Generic;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Bueste : MonoBehaviour {
        public bool wurf = false, fallen=false, bruch=false;
        Rigidbody2D rb;
        Vector2 richtung;
        GameObject player;
        AudioSource bruchSound;

        // Use this for initialization
        void Start() {
            player = GameObject.FindGameObjectWithTag("Player");
            rb = GetComponent<Rigidbody2D>();
            bruchSound = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void FixedUpdate() {
            if (wurf)
            {
                richtung = player.GetComponent<Rigidbody2D>().position - rb.position;
                rb.velocity = richtung.normalized * 30f;
                if (Physics2D.OverlapCircle(transform.position, 1.5f, 1 << 8))//layer Ebene 8 = player
                {
                    player.GetComponent<PlayerStats>().lifes=-1;
                    player.GetComponent<PlatformerCharacter2D>().KillPlayer(DamageTypeToPlayer.BluntProjectile);
                    bruch=true;
                    rb.velocity = new Vector2(0,0);
                    wurf = false;
                    rb.gravityScale = 3;
                    rb.freezeRotation = false;
                }
            }
            if (fallen)
            {
                richtung = new Vector2(0,-5);
                rb.velocity = richtung;
            }
            if (bruch)
            {
                bruch = !bruch;
                bruchSound.PlayDelayed(0);
            }
        }
    }
}
