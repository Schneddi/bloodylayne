﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class NosferatuDialogControl : MonoBehaviour
    {
        private Dialog dialog;
        int sameQuestionNumber, nextScene;
        public GameObject[] sameQuestionDialog;
        public DialogChoice skillListenerA1, skillListenerA3, skillListenerA4o3;
        public GameObject dialogA1, dialogA2o1, dialogA2o2, dialogA4o1u1, dialogA4o1u2, dialogA4o2, dialogA5Bo1;
        public GameObject dialogA3, dialogA3e1, dialogA3e2o1, dialogA3e2o2, dialogA3e3, dialogA3e4;
        public GameObject dialog4o3G1, dialog4o3G2, dialog4o3D, dialog5Bo2u1, dialog5Bo2u2, dialog5Bo3; //ending dialog
        //final Ending Dialog
        public GameObject DialogBoxG7, DialogBoxB1u5, DialogBoxB2u2, DialogBoxD1u5;
        public GameObject endingDialog;
        private DialogChoice currentListener;
        PlatformerCharacter2D larry;
        GlobalVariables gv;
        public bool humanEnding, servantEnding, nosferatuEnding, devilEnding;
        Nosferatu nosferatu;
        private bool listenFordialogA1;
        
        // Start is called before the first frame update
        void Start()
        {
            dialog = FindObjectOfType<Dialog>();
            larry = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            nosferatu = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Nosferatu>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        }

        // Update is called once per frame
        void Update()
        {
            if (sameQuestionDialog[sameQuestionNumber].activeSelf && sameQuestionNumber < 8)
            {
                if (currentListener.nextDialogOption1 == sameQuestionDialog[sameQuestionNumber])
                {
                    currentListener.nextDialogOption1 =  sameQuestionDialog[sameQuestionNumber + 1];
                }
                if (currentListener.nextDialogOption2 == sameQuestionDialog[sameQuestionNumber])
                {
                    currentListener.nextDialogOption2 = sameQuestionDialog[sameQuestionNumber + 1];
                }
                if (currentListener.nextDialogOption3 == sameQuestionDialog[sameQuestionNumber])
                {
                    currentListener.nextDialogOption3 = sameQuestionDialog[sameQuestionNumber + 1];
                }
                currentListener.listenDialog = sameQuestionDialog[sameQuestionNumber].GetComponent<Dialogbox>();
                currentListener.listen = true;
                currentListener.isActivated = true;
                sameQuestionNumber++;
            }
            if (sameQuestionDialog[sameQuestionNumber].activeSelf && sameQuestionNumber == 8 && currentListener.listen == false)
            {
                sameQuestionDialog[sameQuestionNumber].GetComponent<Dialogbox>().finished = false;
                currentListener.listenDialog = sameQuestionDialog[sameQuestionNumber].GetComponent<Dialogbox>();
                currentListener.listen = true;
                currentListener.isActivated = true;
            }

            if (dialogA1.activeSelf)
            {
                listenFordialogA1 = true;
            }
            
            
            if (!dialogA1.activeSelf && listenFordialogA1)
            {
                listenFordialogA1 = false;
                initializeNosferatuLvL();
            }

            if (dialogA2o1.activeSelf)
            {
                skillListenerA1.listenDialog = dialogA2o1.GetComponent<Dialogbox>();
                skillListenerA1.nextDialogOption1 = sameQuestionDialog[sameQuestionNumber];
                skillListenerA1.listen = true;
                skillListenerA1.isActivated = true;
            }
            if (dialogA2o2.activeSelf)
            {
                skillListenerA1.listenDialog = dialogA2o2.GetComponent<Dialogbox>();
                skillListenerA1.nextDialogOption2 = sameQuestionDialog[sameQuestionNumber];
                skillListenerA1.listen = true;
                skillListenerA1.isActivated = true;
            }
            if (dialogA4o1u1.activeSelf)
            {
                currentListener = skillListenerA3;
                skillListenerA3.listenDialog = dialogA4o1u2.GetComponent<Dialogbox>();
                skillListenerA3.nextDialogOption1 = sameQuestionDialog[sameQuestionNumber];
                skillListenerA3.listen = true;
                skillListenerA3.isActivated = true;
            }
            if (dialogA4o2.activeSelf)
            {
                currentListener = skillListenerA3;
                skillListenerA3.listenDialog = dialogA4o2.GetComponent<Dialogbox>();
                skillListenerA3.nextDialogOption2 = sameQuestionDialog[sameQuestionNumber];
                skillListenerA3.listen = true;
                skillListenerA3.isActivated = true;
            }
            if (dialogA5Bo1.activeSelf)
            {
                currentListener = skillListenerA4o3;
                skillListenerA4o3.listenDialog = dialogA5Bo1.GetComponent<Dialogbox>();
                skillListenerA4o3.nextDialogOption1 = sameQuestionDialog[sameQuestionNumber];
                skillListenerA4o3.listen = true;
                skillListenerA4o3.isActivated = true;
            }
            if (dialog5Bo2u2.activeSelf && !servantEnding)
            {
                currentListener = skillListenerA4o3;
                servantEnding = true;
                //nosferatu.servantDialog = true;
                nosferatu.listenStartDialog = dialog5Bo2u2;
                nosferatu.startingDialog = true;
                larry.GetComponent<Animator>().SetTrigger("Kneel");
            }
            if (dialog5Bo3.activeSelf)
            {
                currentListener = skillListenerA4o3;
                nosferatuEnding = true;
                nosferatu.listenStartDialog = dialog5Bo3;
                nosferatu.startingDialog = true;
            }
        }

        public void decideEnding()
        {
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            nosferatu = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Nosferatu>();
            if (gv.humansKilled == 0)
            {
                humanEnding = true;
                skillListenerA3.nextDialogOption3 = dialog4o3G1;
                nosferatu.listenStartDialog = dialog4o3G2;
            }
            else if (gv.humansKilled >= gv.humansCount - 5 && gv.blackMageKilled && (!gv.jaegerEncountered || gv.jaegerKilled) && (!gv.assassinEncountered || gv.assassinKilled))
            {
                devilEnding = true;
                skillListenerA3.nextDialogOption3 = dialog4o3D;
                nosferatu.listenStartDialog = dialog4o3D;
            }
            else
            {
                nosferatuEnding = true;
            }
        }

        private void initializeNosferatuLvL()
        {
            GameObject currentDialog = dialogA3;
            currentListener = skillListenerA1;
            decideEnding();
            nosferatuEnding = false;
            if (humanEnding || devilEnding)
            {
                nosferatu.startingDialog = true;
            }

            if (!devilEnding)
            {
                if (!larry.skill2choice1 && !larry.skill2choice2)
                {
                    currentDialog.GetComponent<Dialogbox>().nextDialog = dialogA3e1;
                    currentDialog = dialogA3e1;
                }
                if (larry.skill3choice1 && gv.humansKilled == 0)
                {
                    currentDialog.GetComponent<Dialogbox>().nextDialog = dialogA3e2o1;
                    currentDialog = dialogA3e2o1;
                }
                else if (larry.skill3choice1 && gv.humansKilled > 0)
                {
                    currentDialog.GetComponent<Dialogbox>().nextDialog = dialogA3e2o2;
                    currentDialog = dialogA3e2o2;
                }
                if (!gv.blackMageKilled)
                {
                    currentDialog.GetComponent<Dialogbox>().nextDialog = dialogA3e3;
                    currentDialog = dialogA3e3;
                }
                if (gv.fartedToDeath && gv.humansKilled == 0)
                {
                    currentDialog.GetComponent<Dialogbox>().nextDialog = dialogA3e4;
                    currentDialog = dialogA3e4;
                }
            }
            skillListenerA3.listenDialog = currentDialog.GetComponent<Dialogbox>();
        }
    }
}
