using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingShadowControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 9.1f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Continue"))
        {
            Destroy(gameObject);
        }
    }
}
