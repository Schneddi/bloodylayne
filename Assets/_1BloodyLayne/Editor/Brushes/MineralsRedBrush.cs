using Editor.Brushes;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Minerals Red Brush")]
    public class MineralsRedBrush : MineralBrush
    {
    }

    [CustomEditor(typeof(MineralsRedBrush))]
    public class MineralsRedBrushEditor : RandomGameObjectEditor
    {
    }
}