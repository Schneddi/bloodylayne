using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Vegetation Short Brush")]
    public class VegetationShortBrush : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(VegetationShortBrush))]
    public class VegetationShortBrushEditor : RandomGameObjectEditor
    {
    }
}