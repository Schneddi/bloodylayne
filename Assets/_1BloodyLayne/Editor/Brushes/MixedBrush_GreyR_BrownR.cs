using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Mixed Brush (Grey R & Brown R)")]
    public class MixedBrushGreyRBrownR : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(MixedBrushGreyRBrownR))]
    public class MixedBrushGreyRBrownREditor : RandomGameObjectEditor
    {
    }
}