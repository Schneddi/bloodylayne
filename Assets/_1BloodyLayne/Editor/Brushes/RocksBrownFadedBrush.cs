using _1BloodyLayne.Editor.Brushes;
using UnityEditor;
using UnityEditor.Tilemaps;
using UnityEngine;

namespace Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Rocks Brown Faded Brush")]
    public class RocksBrownFadedBrush : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(RocksBrownFadedBrush))]
    public class RocksBrownBrushFadedEditor : RandomGameObjectEditor
    {
    }
}