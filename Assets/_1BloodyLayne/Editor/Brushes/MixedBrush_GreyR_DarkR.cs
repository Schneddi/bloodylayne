using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Mixed Brush (Grey R & Dark R)")]
    public class MixedBrushGreyRDarkR : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(MixedBrushGreyRDarkR))]
    public class MixedBrushGreyRDarkREditor : RandomGameObjectEditor
    {
    }
}