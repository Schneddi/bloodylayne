using Editor.Brushes;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Minerals MixedColor Brush")]
    public class MineralsMixedColorBrush : MineralBrush
    {
    }

    [CustomEditor(typeof(MineralsMixedColorBrush))]
    public class MineralsMixedColorBrushEditor : RandomGameObjectEditor
    {
    }
}