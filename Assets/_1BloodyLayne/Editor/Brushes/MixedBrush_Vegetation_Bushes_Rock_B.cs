using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Mixed Brush (Vegetation & Bushes & Rock B)")]
    public class MixedBrushVegetationBushesRockB : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(MixedBrushVegetationBushesRockB))]
    public class MixedBrushVegetationBushesRockBEditor : RandomGameObjectEditor
    {
    }
}