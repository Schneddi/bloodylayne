using Editor.Brushes;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Leaf Brush")]
    public class LeafBrush : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(LeafBrush))]
    public class LeafBrushEditor : RandomGameObjectEditor
    {
    }
}