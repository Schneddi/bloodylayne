using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Minerals Yellow Brush")]
    public class MineralsYellowBrush : MineralBrush
    {
    }

    [CustomEditor(typeof(MineralsYellowBrush))]
    public class MineralsYellowBrushEditor : RandomGameObjectEditor
    {
    }
}