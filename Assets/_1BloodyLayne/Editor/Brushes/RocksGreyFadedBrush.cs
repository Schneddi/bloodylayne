using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Rocks Grey Faded Brush")]
    public class RocksGreyFadedBrush : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(RocksGreyFadedBrush))]
    public class RocksGreyBrushFadedEditor : RandomGameObjectEditor
    {
    }
}