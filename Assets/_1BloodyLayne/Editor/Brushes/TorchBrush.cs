using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Torch Brush")]
    public class TorchBrush : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(TorchBrush))]
    public class TorchBrushEditor : RandomGameObjectEditor
    {
    }
}