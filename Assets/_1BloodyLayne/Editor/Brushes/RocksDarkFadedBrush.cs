using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Rocks Dark Faded Brush")]
    public class RocksDarkFadedBrush : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(RocksDarkFadedBrush))]
    public class RocksDarkBrushFadedEditor : RandomGameObjectEditor
    {
    }
}