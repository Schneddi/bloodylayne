using _1BloodyLayne.Editor.Brushes;
using UnityEditor;
using UnityEditor.Tilemaps;
using UnityEngine;

namespace Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Rocks Brown Brush")]
    public class RocksBrownBrush : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(RocksBrownBrush))]
    public class RocksBrownBrushEditor : RandomGameObjectEditor
    {
    }
}