using Editor.Brushes;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Minerals Blue Brush")]
    public class MineralsBlueBrush : MineralBrush
    {
    }

    [CustomEditor(typeof(MineralsBlueBrush))]
    public class MineralsBlueBrushEditor : RandomGameObjectEditor
    {
    }
}