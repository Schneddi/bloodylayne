using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Mixed Brush (Brown R & Dark R)")]
    public class MixedBrushBrownRDarkR : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(MixedBrushBrownRDarkR))]
    public class MixedBrushBrownRDarkREditor : RandomGameObjectEditor
    {
    }
}