using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Mixed Brush (Vegetation & Bushes & Trees)")]
    public class MixedBrushVegetationBushesTrees : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(MixedBrushVegetationBushesTrees))]
    public class MixedBrushVegetationBushesTreesEditor : RandomGameObjectEditor
    {
    }
}