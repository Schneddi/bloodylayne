using Editor.Brushes;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{  
    [CustomGridBrush(true, false, false, "Bush Brush")]
    public class BushBrush : RandomGameObjectBrush
    {
    }
    
    [CustomEditor(typeof(BushBrush))]
    public class BushBrushEditor : RandomGameObjectEditor
    {
    }
}