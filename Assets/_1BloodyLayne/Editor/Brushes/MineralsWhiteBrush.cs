using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Minerals White Brush")]
    public class MineralsWhiteBrush : MineralBrush
    {
    }

    [CustomEditor(typeof(MineralsWhiteBrush))]
    public class MineralsWhiteBrushEditor : RandomGameObjectEditor
    {
    }
}