using Editor.Brushes;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Minerals Purple Brush")]
    public class MineralsPurpleBrush : MineralBrush
    {
    }

    [CustomEditor(typeof(MineralsPurpleBrush))]
    public class MineralsPurpleBrushEditor : RandomGameObjectEditor
    {
    }
}