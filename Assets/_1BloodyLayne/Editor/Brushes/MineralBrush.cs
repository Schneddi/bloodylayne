using Editor.Brushes;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace _1BloodyLayne.Editor.Brushes
{
    public class MineralBrush : RandomGameObjectBrush
    {
        [SerializeField] private bool enableLight;

        protected override void DoAdditionalStuff(GameObject instance)
        {
            if (!enableLight)
            {
                foreach (Light2D light in instance.GetComponentsInChildren<Light2D>())
                {
                    light.gameObject.SetActive(false);
                }
            }
            else
            {
                foreach (Light2D light in instance.GetComponentsInChildren<Light2D>(true))
                {
                    light.gameObject.SetActive(true);
                }
            }
        }
    }
}
