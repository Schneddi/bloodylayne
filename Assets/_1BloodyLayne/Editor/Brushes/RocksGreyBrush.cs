using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Rocks Grey Brush")]
    public class RocksGreyBrush : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(RocksGreyBrush))]
    public class RocksGreyBrushEditor : RandomGameObjectEditor
    {
    }
}