using System;
using System.Collections.Generic;
using System.Linq;
using Maps.Doodads;
using UnityEditor;
using UnityEditor.EditorTools;
using UnityEditor.SceneManagement;
using UnityEditor.Tilemaps;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "RandomGameObject Brush")]
    public class RandomGameObjectBrush : GridBrushBase
    {
        //TODO: Sort Variables by accessibility and add [Header] Attributes to topics
        
        [Serializable]
        internal class HiddenGridLayout
        {
            public Vector3 cellSize = Vector3.one;
            public Vector3 cellGap = Vector3.zero;
            public GridLayout.CellLayout cellLayout = GridLayout.CellLayout.Rectangle;
            public GridLayout.CellSwizzle cellSwizzle = GridLayout.CellSwizzle.XYZ;
        }

        [SerializeField] private BrushCell[] cellsField;

        [SerializeField] public GameObject[] dragInObjectField;

        private Vector3Int _size;

        [SerializeField] public bool constrainProportionsToX = true;
        [SerializeField] public bool usePrefabScaleAsProportions = true;
        [SerializeField] public Vector3 minScale = Vector3.one;
        [SerializeField] public Vector3 maxScale = Vector3.one;
        [SerializeField] private bool disableRandom;
        [SerializeField] private bool disablePreview;
        [SerializeField] private int pickElement;
        
        
        public Quaternion minOrientation = Quaternion.identity;
        public Quaternion maxOrientation = Quaternion.identity;
        private float _previewRotation;
        
        [SerializeField] private Vector3 minOffset = Vector3.zero;
        [SerializeField] private Vector3 maxOffset = Vector3.zero;

        [SerializeField] public bool overrideSortingLayer = true;
        [SerializeField] public int sortingLayer;
        [SerializeField] public int lowestOrderInLayer;
        
        [Tooltip("Adds Masks to reveal Units behind it.")]
        [SerializeField] private bool foreGroundObject;
        [SerializeField] public bool overrideColors;
        [SerializeField] public Color newColor;
        [SerializeField] private Material newMaterial;


        [Tooltip("If rotating with the mouse cursor function, the offset is being rotated, but not the individual GameObject.")]
        [SerializeField] protected bool keepRotation;

        [Tooltip("Use this when you want a random pickChance for drawChance and not to draw in every Cells you click. Good for just drawing a line with random objects.")]
        [SerializeField] protected bool drawChanceMode;
        [Range(0, 1)] [SerializeField] protected float pickChance;
        [SerializeField]
        public float drawEmptyCellsAfterEachDraw;

        [HideInInspector] public Vector3 mouseWorldPositionRef;
        [HideInInspector] public GameObject offsetPoint;
        [HideInInspector] public bool changeMode;

        [SerializeField] [HideInInspector] private bool canChangeZPositionValue;

        [SerializeField] [HideInInspector] internal HiddenGridLayout hiddenGridLayout = new HiddenGridLayout();

        /// <summary>
        /// GameObject used for painting onto the Scene root
        /// </summary>
        [HideInInspector] public GameObject hiddenGrid;
        
        [Tooltip("Paint on the whole Tilemap group, with their current brush settings.")]
        [SerializeField] private bool paintOnTilemapGroup;
        private bool _paintOnTilemapGroup;

        [NonSerialized] private readonly List<PreviewInfo> _previewInfos;
        
        private bool _useDrawBox;
        private const string PaintOnSceneRoot = "(Paint on SceneRoot)";

        /// <summary>
        /// Anchor Point of the Instantiated GameObject in the cell when painting
        /// </summary>
        public Vector3 anchor = new Vector3(0.5f, 0.5f, 0.5f);
        /// <summary>Pivot of the brush. </summary>
        public Vector3Int Pivot { get; private set; }

        /// <summary>All the brush cells the brush holds. </summary>
        private BrushCell[] CellsField { get { return cellsField; } }
        /// <summary>Number of brush cells in the brush.</summary>
        public int CellCount { get { return cellsField != null ? cellsField.Length : 0; } }
        /// <summary>Number of brush cells based on size.</summary>
        public int SizeCount
        {
            get { return _size.x * _size.y * _size.z; }
        }
        /// <summary>Whether the brush can change Z Position</summary>
        public bool CanChangeZPosition
        {
            get { return canChangeZPositionValue; }
            set { canChangeZPositionValue = value; }
        }
        
        /// <summary>Size of the brush in cells. </summary>
        private Vector3Int Size { get => _size; set => _size = value; }

        private bool _pickedObjects;
        private bool _valuesChanged;

        /// <summary>
        /// This Brush instances, places and manipulates GameObjects onto the scene.
        /// </summary>
        public RandomGameObjectBrush()
        {
            _size = Vector3Int.one;
            Pivot = Vector3Int.zero;
            _previewInfos = new List<PreviewInfo>();
            SceneView.duringSceneGui += OnScene;
            GridPaintingState.scenePaintTargetChanged += OnScenePaintTargetChanged;
        }

        private void OnEnable()
        {
            hiddenGrid = new GameObject();
            hiddenGrid.name = PaintOnSceneRoot;
            hiddenGrid.hideFlags = HideFlags.HideAndDontSave;
            hiddenGrid.transform.position = Vector3.zero;
            Grid grid = hiddenGrid.AddComponent<Grid>();
            grid.cellSize = hiddenGridLayout.cellSize;
            grid.cellGap = hiddenGridLayout.cellGap;
            grid.cellSwizzle = hiddenGridLayout.cellSwizzle;
            grid.cellLayout = hiddenGridLayout.cellLayout;
            _valuesChanged = true;
            changeMode = false;
            _previewRotation = 0;
        }
        
        private void OnValidate()
        {
            if (ToolManager.activeToolType != typeof(PickingTool))
            {
                _valuesChanged = true;
            }
        }
        
        #region Methods required to set up the preview Object
        
        //OnScene will handle the preview of the next drawn object
        private void OnScene(SceneView scene)
        {
            //scenePaintTarget has been destroyed
            if (GridPaintingState.scenePaintTarget == null)
            {
                return;
            }
            
            if (_paintOnTilemapGroup != paintOnTilemapGroup)
            {
                _paintOnTilemapGroup = paintOnTilemapGroup;
                if(!_pickedObjects) DestroyPreviewObjects();
            }
            
            Vector3 mouseWorldPosition = MouseToWorld(Event.current.mousePosition, scene.camera);
            
            if (changeMode)
            {
                mouseWorldPosition = mouseWorldPositionRef;
            }
            SetOffsetPointDisplay(mouseWorldPosition);
            
            //if the user is using a function to set a new offset or rotation
            if (changeMode)
            {
                return;
            }
            
            //first handle what's happening, if this is not the current selected brush
            if (GridPaintingState.gridBrush != this || ToolManager.activeToolType != typeof(PaintTool) || CellCount == 0)
            {
                DestroyPreviewObjects();
                return;
            }

            if (_valuesChanged)
            {
                _valuesChanged = false;
                if(!_pickedObjects) DestroyPreviewObjects();
            }

            SetupPreviewObjects(mouseWorldPosition);
            
            Vector3 MouseToWorld(Vector3 mousePosRef, Camera camera)
            {
                mousePosRef.y = camera.pixelHeight - mousePosRef.y;
                Vector3 mouseWorldPos = camera.ScreenToWorldPoint(new Vector3(mousePosRef.x, mousePosRef.y, 0));
                mouseWorldPos = Vector3.Scale(mouseWorldPos, new Vector3(1, 1, 0));
                if (!changeMode)
                {
                    mouseWorldPositionRef = mouseWorldPos;
                }
                return mouseWorldPos;
            }
        }
        
        private void OnScenePaintTargetChanged(GameObject obj)
        {
            if (_pickedObjects)
            {
                List<PreviewInfo> previewInfosCopy = new(_previewInfos);
                _previewInfos.Clear();
                foreach (PreviewInfo previewInfoCopy in previewInfosCopy)
                {
                    PreviewInfo previewInfo = previewInfoCopy;
                    previewInfo.TileMapSelectionInfoRef.TransformRef = obj.transform;
                    _previewInfos.Add(previewInfo);
                }
                return;
            }
            
            DestroyPreviewObjects();
        }

        private void SetOffsetPointDisplay(Vector3 mousePosition)
        {
            if (_paintOnTilemapGroup && GridPaintingState.gridBrush == this && ToolManager.activeToolType == typeof(PaintTool) && GridPaintingState.scenePaintTarget.GetComponent<TileMapSelectionInfo>())
            {
                float highestOffset = 0;

                foreach (TileMapSelectionInfo relatedTilemapInfo in GridPaintingState.scenePaintTarget.GetComponent<TileMapSelectionInfo>().relatedTilemaps)
                {
                    if (Math.Abs(relatedTilemapInfo.layerOffset) > highestOffset)
                    {
                        highestOffset = relatedTilemapInfo.layerOffset;
                    }
                }
                
                if (!offsetPoint)
                {
                    offsetPoint = new GameObject("OffsetReferencePoint");
                    GUIContent gameObjectIconContent1 = EditorGUIUtility.IconContent("sv_icon_dot6_pix16_gizmo");
                    EditorGUIUtility.SetIconForObject(offsetPoint, (Texture2D)gameObjectIconContent1.image);
                    offsetPoint.hideFlags = HideFlags.DontSave;

                    GameObject description = new GameObject("Highest Offset Distance");
                    description.transform.SetParent(offsetPoint.transform);
                    GUIContent gameObjectIconContent2 = EditorGUIUtility.IconContent("sv_label_6");
                    EditorGUIUtility.SetIconForObject(description, (Texture2D)gameObjectIconContent2.image);
                    Vector3 localPosition = description.transform.localPosition;
                    localPosition = new Vector3(localPosition.x,
                        localPosition.y - 0.84f, localPosition.z);
                    description.transform.localPosition = localPosition;
                }
                offsetPoint.transform.position = mousePosition + Quaternion.Euler(0, 0, _previewRotation) * new Vector3(0,highestOffset,0);
            }
            else if (offsetPoint != null)
            {
                DestroyImmediate(offsetPoint);
            }
        }
        
        private void SetupPreviewObjects(Vector3 previewObjectsPosition = default)
        {
            //setup a list with all brushes, that should be previewed
            List<(RandomGameObjectBrush, TileMapSelectionInfoRef)> usedBrushes = new List<(RandomGameObjectBrush, TileMapSelectionInfoRef)>();
            TileMapSelectionInfoRef tileMapSelectionInfoRef;
            
            if (!_paintOnTilemapGroup || !GridPaintingState.scenePaintTarget.GetComponent<TileMapSelectionInfo>())
            {
                if (GridPaintingState.scenePaintTarget == null || GridPaintingState.scenePaintTarget.name.Equals(PaintOnSceneRoot))
                {
                    tileMapSelectionInfoRef = new TileMapSelectionInfoRef(null, name, sortingLayer, lowestOrderInLayer, newColor, minScale.x, drawEmptyCellsAfterEachDraw, 0, null);
                }
                else
                {
                    tileMapSelectionInfoRef = new TileMapSelectionInfoRef(GridPaintingState.scenePaintTarget.transform, name, sortingLayer, lowestOrderInLayer, newColor, minScale.x, drawEmptyCellsAfterEachDraw, 0, null);
                }
                usedBrushes.Add((this, tileMapSelectionInfoRef));
            }
            else
            {
                foreach (TileMapSelectionInfo relatedTilemap in GridPaintingState.scenePaintTarget.GetComponent<TileMapSelectionInfo>().relatedTilemaps)
                {
                    GridBrushBase brushBase = null;
                    foreach (GridBrushBase usedBrush in GridPaintingState.brushes)
                    {
                        if (usedBrush.name == relatedTilemap.brush)
                        {
                            brushBase = usedBrush;
                        }
                    }
                    tileMapSelectionInfoRef = new (relatedTilemap);
                    usedBrushes.Add((brushBase as RandomGameObjectBrush, tileMapSelectionInfoRef));
                }
            }

            //look if all brushes have created previewObjects
            foreach ((RandomGameObjectBrush, TileMapSelectionInfoRef) usedBrush in usedBrushes)
            {
                if (!_previewInfos.Any(eri => eri.TileMapSelectionInfoRef.Equals(usedBrush.Item2)) && !_pickedObjects)
                {
                    Tuple<RandomGameObjectBrush, TileMapSelectionInfoRef> brushTuple = new Tuple<RandomGameObjectBrush, TileMapSelectionInfoRef>(usedBrush.Item1,usedBrush.Item2);
                    CreatePreviewObject(previewObjectsPosition, brushTuple);
                }
                
                //if the user deleted a previewObject
                if (!_previewInfos.FirstOrDefault(eri => eri.TileMapSelectionInfoRef.Equals(usedBrush.Item2)).PreviewObject && !_pickedObjects)
                {
                    _previewInfos.Remove(_previewInfos.FirstOrDefault(eri => eri.TileMapSelectionInfoRef.Equals(usedBrush.Item2)));
                    Tuple<RandomGameObjectBrush, TileMapSelectionInfoRef> brushTuple = new Tuple<RandomGameObjectBrush, TileMapSelectionInfoRef>(usedBrush.Item1,usedBrush.Item2);
                    CreatePreviewObject(previewObjectsPosition, brushTuple);
                }
            }

            foreach (PreviewInfo previewInfo in _previewInfos)
            {
                previewInfo.PreviewObject.transform.position = previewObjectsPosition + previewInfo.Offset;
            }
        }

        private void CreatePreviewObject(Vector3 mouseWorldPosition, Tuple<RandomGameObjectBrush, TileMapSelectionInfoRef> brushTuple, GameObject existingObject = null, Vector3 offset = default)
        {
            brushTuple.Item1._previewRotation = _previewRotation;
            BrushCell cell = brushTuple.Item1.PickBrushCell();
            GameObject go = cell.CellGameObject;
            if (existingObject != default)
            {
                go = existingObject;
            }
            
            GameObject instance;
            if (disablePreview)
            {
                instance = go;
            }
            else if (PrefabUtility.IsPartOfPrefabAsset(go))
            {
                instance = (GameObject) PrefabUtility.InstantiatePrefab(go, null);
            }
            else
            {
                instance = Instantiate(go, mouseWorldPosition, Quaternion.identity);
                instance.name = go.name;
                instance.SetActive(true);
                
                foreach (Renderer renderer in instance.GetComponentsInChildren<Renderer>())
                {
                    renderer.enabled = true;
                }
            }
            instance.hideFlags = HideFlags.DontSave | HideFlags.HideInHierarchy;

            DrawChanceInfo drawChanceInfo = new (Vector3Int.zero, Vector3Int.zero, 0);
            PreviewInfo previewInfo = new PreviewInfo(brushTuple.Item1, instance, cell, brushTuple.Item2, drawChanceInfo, offset);
            
            
            RandomParameters randomParameters = SetupRandomParameters(previewInfo);
            previewInfo.Offset = randomParameters.Offset;
            SetupDrawnObject(randomParameters, previewInfo, previewInfo.Offset);
            
            //additional setup for previewObjects, so that they are transparent
            foreach (SpriteRenderer renderer in instance.GetComponentsInChildren<SpriteRenderer>())
            {
                Color color = renderer.color;
                color = new Color(color.r, color.g, color.b, 0.5f);
                renderer.color = color;
            }

            RandomGameObjectBrush brushBase = GridPaintingState.gridBrush as RandomGameObjectBrush;
            
            if (brushBase != null) brushBase._previewInfos.Add(previewInfo);
        }

        private void DestroyPreviewObjects()
        {
            if (_previewInfos.Count == 0)
            {
                _pickedObjects = false;
                return;
            }

            foreach (PreviewInfo previewInfo in _previewInfos)
            {
                if(!disablePreview && !AssetDatabase.Contains(previewInfo.PreviewObject)) DestroyImmediate(previewInfo.PreviewObject);
            }
            _previewInfos.Clear();

            _pickedObjects = false;
            if (ToolManager.activeToolType != typeof(EraseTool))
            {
                _size = Vector3Int.one;
                Pivot = Vector3Int.zero;
            }
        }

        public void ChangeOffset(float originMouseDistance)
        {
            if (originMouseDistance == 0)
            {
                return;
            }
            
            float currentHighestOffsetDistance = 0;
            
            foreach (TileMapSelectionInfo relatedTilemap in GridPaintingState.scenePaintTarget.GetComponent<TileMapSelectionInfo>().relatedTilemaps)
            {
                if (Math.Abs(relatedTilemap.layerOffset) > currentHighestOffsetDistance)
                {
                    currentHighestOffsetDistance = Math.Abs(relatedTilemap.layerOffset);
                }
            }
            
            if (currentHighestOffsetDistance == 0)
            {
                List<TileMapSelectionInfo> relatedTilemaps = GridPaintingState.scenePaintTarget.GetComponent<TileMapSelectionInfo>().relatedTilemaps;
                float offsetIncreasePerLayer = originMouseDistance/(relatedTilemaps.Count-1);
                
                for (int i = 0; i < relatedTilemaps.Count; i++)
                {
                    relatedTilemaps[i].layerOffset = i * offsetIncreasePerLayer;
                }

                currentHighestOffsetDistance = (relatedTilemaps.Count - 1) * originMouseDistance;
            }
            

            float newOffsetFactor = originMouseDistance / currentHighestOffsetDistance;
            
            foreach (TileMapSelectionInfo relatedTilemapInfo in GridPaintingState.scenePaintTarget.GetComponent<TileMapSelectionInfo>().relatedTilemaps)
            {
                relatedTilemapInfo.layerOffset *= newOffsetFactor;
                PreviewInfo previewInfo = _previewInfos.FirstOrDefault(pre => pre.TileMapSelectionInfoRef.TransformRef.Equals(relatedTilemapInfo.transform));
                _previewInfos.Remove(_previewInfos.FirstOrDefault(pre => pre.TileMapSelectionInfoRef.TransformRef.Equals(relatedTilemapInfo.transform)));
                previewInfo.TileMapSelectionInfoRef.LayerOffset = relatedTilemapInfo.layerOffset;
                
                Vector3 brushOffset = new (Random.Range(previewInfo.Brush.minOffset.x, previewInfo.Brush.maxOffset.x), Random.Range(previewInfo.Brush.minOffset.y, previewInfo.Brush.maxOffset.y), Random.Range(previewInfo.Brush.minOffset.z, previewInfo.Brush.maxOffset.z));
                brushOffset += previewInfo.Cell.Offset;
                Vector3 layeredOffset = brushOffset + new Vector3(0,previewInfo.TileMapSelectionInfoRef.LayerOffset,0);
                Vector3 rotatedOffset = Quaternion.Euler(0, 0, _previewRotation) * layeredOffset;
                previewInfo.Offset = rotatedOffset;
                
                _previewInfos.Add(previewInfo);
            }
            SetupPreviewObjects(mouseWorldPositionRef);
        }

        public void ChangePreviewToAngle(float degrees)
        {
            float newRotationDiff = _previewRotation % 360 - degrees;
            RotateCurrentPreviewObject(-newRotationDiff);
        }

        public void RotateCurrentPreviewObject(float degrees)
        {
            if (_previewInfos.Count == 0)
            {
                return;
            }
            
            _previewRotation += degrees;
            if (Math.Abs(degrees) == 90)
            {
                Vector3Int newSize = _size;
                newSize.x = _size.y;
                newSize.y = _size.x;
                _size = newSize;
                Pivot = _size / 2;
            }
            
            List<RandomGameObjectBrush> usedBrushes = new();

            List<PreviewInfo> previewInfosCopy = new(_previewInfos);

            foreach (PreviewInfo previewInfoCopy in previewInfosCopy)
            {
                RandomGameObjectBrush brushClass = previewInfoCopy.Brush;

                brushClass._previewRotation = _previewRotation;
                
                if (!usedBrushes.Contains(brushClass))
                {
                    usedBrushes.Add(brushClass);
                }

                Vector3 minRandomVector = new (brushClass.minOrientation.eulerAngles.x, brushClass.minOrientation.eulerAngles.y, Mathf.Round(brushClass.minOrientation.eulerAngles.z + _previewRotation));
                Vector3 maxRandomVector = new (brushClass.maxOrientation.eulerAngles.x, brushClass.maxOrientation.eulerAngles.y, Mathf.Round(brushClass.maxOrientation.eulerAngles.z + _previewRotation));
                
                if (maxRandomVector.z < minRandomVector.z)
                {
                    maxRandomVector.z = 360 + maxRandomVector.z;
                }
                
                Quaternion orientation = Quaternion.Euler(new (Random.Range(minRandomVector.x, maxRandomVector.x), Random.Range(minRandomVector.y, maxRandomVector.y), Random.Range(minRandomVector.z, maxRandomVector.z)));

                PreviewInfo previewInfo = _previewInfos.FirstOrDefault(pre => pre.Equals(previewInfoCopy));
                _previewInfos.Remove(_previewInfos.FirstOrDefault(pre => pre.Equals(previewInfoCopy)));
                if (!keepRotation)
                {
                    previewInfo.PreviewObject.transform.rotation = orientation;
                }
                previewInfo.Offset = Quaternion.Euler(0, 0, degrees) * previewInfoCopy.Offset;
                previewInfo.PreviewObject.transform.position = mouseWorldPositionRef + previewInfo.Offset;
                _previewInfos.Add(previewInfo);
            }
        }

        private void SetupDrawnObject(RandomParameters randomParameters, PreviewInfo previewInfo, Vector3 offset, GameObject go = null)
        {
            GameObject instance = previewInfo.PreviewObject;
            if (go != null)
            {
                instance = go;
            }
            
            instance.transform.localRotation = randomParameters.Orientation;
            
            instance.transform.localScale = randomParameters.Scale;

            instance.transform.Translate(offset);

            if (previewInfo.Brush.overrideColors)
            {
                foreach (SpriteRenderer renderer in instance.GetComponentsInChildren<SpriteRenderer>())
                {
                    renderer.color = previewInfo.TileMapSelectionInfoRef.TileMapColor;
                    if (!_paintOnTilemapGroup)
                    {
                        renderer.color = newColor;
                    }
                }
            }
            
            if (overrideSortingLayer)
            {
                int lowestPreviousOrderInLayer = 99999999;
                foreach (SpriteRenderer renderer in instance.GetComponentsInChildren<SpriteRenderer>())
                {
                    if (renderer.sortingOrder < lowestPreviousOrderInLayer)
                    {
                        lowestPreviousOrderInLayer = renderer.sortingOrder;
                    }
                }
                
                foreach (SpriteRenderer renderer in instance.GetComponentsInChildren<SpriteRenderer>())
                {
                    renderer.sortingLayerName = SortingLayer.layers[sortingLayer].name;
                    renderer.sortingOrder = renderer.sortingOrder - lowestPreviousOrderInLayer +  previewInfo.TileMapSelectionInfoRef.OrderInLayer;
                }

                previewInfo.PreviewObject = instance;
            }

            if (foreGroundObject)
            {
                SpriteRenderer[] spriteRenderers = instance.GetComponentsInChildren<SpriteRenderer>();

                foreach (SpriteRenderer renderer in spriteRenderers)
                {
                    renderer.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
                    if (newMaterial != null)
                    {
                        renderer.material = newMaterial;
                    }

                    SpriteMask mask = renderer.gameObject.AddComponent<SpriteMask>();
                    mask.sprite = renderer.sprite;
                    mask.isCustomRangeActive = true;
                    mask.frontSortingLayerID = SortingLayer.layers[5].id;
                    mask.frontSortingOrder = 1002;
                    mask.backSortingLayerID = SortingLayer.layers[5].id;
                    mask.backSortingOrder = 1000;

                    GameObject spriteChild = new GameObject();
                    spriteChild.transform.SetParent(renderer.transform);
                    spriteChild.transform.localScale = Vector3.one;
                    spriteChild.transform.localPosition = Vector3.zero;

                    spriteChild.name = renderer.gameObject.name + "_ForegroundBackground";
                    SpriteRenderer objectBackgroundRenderer = spriteChild.AddComponent<SpriteRenderer>();
                    objectBackgroundRenderer.sprite = renderer.sprite;
                    objectBackgroundRenderer.material = renderer.sharedMaterial;

                    int lowestPreviousOrderInLayer = 99999999;
                    foreach (SpriteRenderer sortRenderer in spriteRenderers)
                    {
                        if (sortRenderer.sortingOrder < lowestPreviousOrderInLayer)
                        {
                            lowestPreviousOrderInLayer = sortRenderer.sortingOrder;
                        }
                    }

                    objectBackgroundRenderer.sortingLayerID = SortingLayer.layers[5].id;
                    objectBackgroundRenderer.sortingOrder = lowestPreviousOrderInLayer - 1;
                    Color color = renderer.color;
                    objectBackgroundRenderer.color = new Color(color.r, color.g, color.b,
                        color.a * 0.5f);
                }
            }

            DoAdditionalStuff(instance);
        }

        private RandomParameters SetupRandomParameters(PreviewInfo previewInfo)
        {
            Vector3 minRandomVector = previewInfo.Brush.minOrientation.eulerAngles;
            Vector3 maxRandomVector = previewInfo.Brush.maxOrientation.eulerAngles;
            if (maxRandomVector.z < minRandomVector.z)
            {
                maxRandomVector.z = 360 + maxRandomVector.z;
            }

            float rotationFactor = _previewRotation;
            if (keepRotation)
            {
                rotationFactor = 0;
            }
            Quaternion orientation = Quaternion.Euler(new (Random.Range(minRandomVector.x, maxRandomVector.x), Random.Range(minRandomVector.y, maxRandomVector.y), Random.Range(minRandomVector.z, maxRandomVector.z) + rotationFactor));

            Vector3 scale = new(Random.Range(previewInfo.Brush.minScale.x, previewInfo.Brush.maxScale.x), Random.Range(previewInfo.Brush.minScale.y, previewInfo.Brush.maxScale.y), Random.Range(previewInfo.Brush.minScale.z, previewInfo.Brush.maxScale.z));
            
            if (_paintOnTilemapGroup)
            {
                float scaleCorrelation = 1;
                if (previewInfo.TileMapSelectionInfoRef.MinScale != 0 && previewInfo.Brush.minScale.x != 0 && previewInfo.Brush.minScale.x != previewInfo.Brush.maxScale.x)
                {
                    scaleCorrelation = previewInfo.TileMapSelectionInfoRef.MinScale/previewInfo.Brush.minScale.x;
                }
                float maxScaleAdjusted = previewInfo.TileMapSelectionInfoRef.MinScale * scaleCorrelation;
                
                float scaleValues = Random.Range(previewInfo.TileMapSelectionInfoRef.MinScale, maxScaleAdjusted);
                scale = new (scaleValues, scaleValues, scaleValues);
            }
            else if (constrainProportionsToX)
            {
                float scaleValues = Random.Range(previewInfo.Brush.minScale.x, previewInfo.Brush.maxScale.x);
                scale = new (scaleValues, scaleValues, scaleValues);
            }
            
            if (previewInfo.Brush.usePrefabScaleAsProportions)
            {
                Vector3 localScale = previewInfo.Cell.CellGameObject.transform.localScale;
                scale = new (scale.x * localScale.x, scale.y * localScale.y, scale.z * localScale.z);
            }

            Vector3 brushOffset = new (Random.Range(previewInfo.Brush.minOffset.x, previewInfo.Brush.maxOffset.x), Random.Range(previewInfo.Brush.minOffset.y, previewInfo.Brush.maxOffset.y), Random.Range(previewInfo.Brush.minOffset.z, previewInfo.Brush.maxOffset.z));
            brushOffset += previewInfo.Cell.Offset;
            Vector3 layeredOffset = brushOffset + new Vector3(0,previewInfo.TileMapSelectionInfoRef.LayerOffset,0);
            Vector3 rotatedOffset = Quaternion.Euler(0, 0, _previewRotation) * layeredOffset;
            rotatedOffset += previewInfo.Offset;

            RandomParameters randomParameters = new (orientation, rotatedOffset, scale);
            return randomParameters;
        }

        #endregion
        
        #region Drawing Methods called by unity
        /// <summary>
        /// Paints GameObjects into a given position within the selected layers.
        /// The GameObjectBrush overrides this to provide GameObject painting functionality.
        /// </summary>
        /// <param name="gridLayout">Grid used for layout.</param>
        /// <param name="brushTarget">Target of the paint operation. By default the currently selected GameObject.</param>
        /// <param name="position">The coordinates of the cell to paint data to.</param>
        public override void Paint(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
        {
            Vector3Int min = position - Pivot;
            BoundsInt bounds = new BoundsInt(min, _size);

            GetGrid(ref gridLayout, ref brushTarget);
            BoxFill(gridLayout, brushTarget, bounds);
        }

        private void PaintCell(GridLayout grid, Vector3Int position, PreviewInfo previewInfo)
        {
            GameObject existingGo = GetObjectInCell(grid, previewInfo.TileMapSelectionInfoRef.TransformRef, position);

            RandomParameters randomParameters = SetupRandomParameters(previewInfo);
            if (existingGo == null)
            {
                SetSceneCell(grid, position, previewInfo, randomParameters);
            }
        }

        /// <summary>
        /// Erases GameObjects in a given position within the selected layers.
        /// The GameObjectBrush overrides this to provide GameObject erasing functionality.
        /// </summary>
        /// <param name="gridLayout">Grid used for layout.</param>
        /// <param name="brushTarget">Target of the erase operation. By default the currently selected GameObject.</param>
        /// <param name="position">The coordinates of the cell to erase data from.</param>
        public override void Erase(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
        {
            Vector3Int min = position - Pivot;
            BoundsInt bounds = new BoundsInt(min, _size);

            GetGrid(ref gridLayout, ref brushTarget);
            BoxErase(gridLayout, brushTarget, bounds);
        }

        /// <summary>
        /// Box fills GameObjects into given bounds within the selected layers.
        /// The GameObjectBrush overrides this to provide GameObject box-filling functionality.
        /// </summary>
        /// <param name="gridLayout">Grid to box fill data to.</param>
        /// <param name="brushTarget">Target of the box fill operation. By default the currently selected GameObject.</param>
        /// <param name="position">The bounds to box fill data into.</param>
        public override void BoxFill(GridLayout gridLayout, GameObject brushTarget, BoundsInt position)
        {
            GetGrid(ref gridLayout, ref brushTarget);

            bool drawBox = !(position.size == Vector3Int.one);
            
            if (_pickedObjects)
            {
                List<PreviewInfo> previewInfoCopy = new(_previewInfos);

                foreach (PreviewInfo previewInfo in previewInfoCopy)
                {
                    Vector3Int location = gridLayout.WorldToCell(previewInfo.PreviewObject.transform.position);
                    PaintCell(gridLayout, location, previewInfo);
                }
                return;
            }

            if (CellCount == 0)
            {
                return;
            }
            
            foreach (Vector3Int location in position.allPositionsWithin)
            {
                //make sure each possible has been setup, in case OnSceneView has not been called, yet
                SetupPreviewObjects();
                
                //if any of these conditions are true, don't go into group functions
                if (!brushTarget || !_paintOnTilemapGroup || !brushTarget.GetComponent<TileMapSelectionInfo>())
                {
                    _useDrawBox = drawBox;
                    PreviewInfo previewInfo = _previewInfos.First();
                    PaintCell(gridLayout, location, previewInfo);
                }
                else
                {
                    List<PreviewInfo> previewInfoCopy = new(_previewInfos);

                    foreach (PreviewInfo previewInfo in previewInfoCopy)
                    {
                        previewInfo.Brush._paintOnTilemapGroup = true;
                        previewInfo.Brush._useDrawBox = drawBox;
                        PaintCell(gridLayout, location, previewInfo);
                    }
                }
            }
        }


        /// <summary>
        /// Erases GameObjects from given bounds within the selected layers.
        /// The GameObjectBrush overrides this to provide GameObject box-erasing functionality.
        /// </summary>
        /// <param name="gridLayout">Grid to erase data from.</param>
        /// <param name="brushTarget">Target of the erase operation. By default the currently selected GameObject.</param>
        /// <param name="position">The bounds to erase data from.</param>
        public override void BoxErase(GridLayout gridLayout, GameObject brushTarget, BoundsInt position)
        {
            GetGrid(ref gridLayout, ref brushTarget);
            
            foreach (Vector3Int location in position.allPositionsWithin)
            {
                EraseCell(gridLayout, location, brushTarget != null ? brushTarget.transform : null);
            }
        }

        private void EraseCell(GridLayout grid, Vector3Int position, Transform parent)
        {
            ClearSceneCell(grid, parent, position);
        }
        
        private void ClearSceneCell(GridLayout grid, Transform parent, Vector3Int position)
        {
            List<Transform> tilemaps = new List<Transform>();

            if (!parent || !_paintOnTilemapGroup || !parent.GetComponent<TileMapSelectionInfo>())
            {
                if (!parent)
                {
                    parent = GridPaintingState.scenePaintTarget.transform;
                }
                tilemaps.Add(parent);
            }
            else
            {
                foreach (TileMapSelectionInfo relatedTilemapInfo in parent.GetComponent<TileMapSelectionInfo>().relatedTilemaps)
                {
                    tilemaps.Add(relatedTilemapInfo.transform);
                }
            }
            
            foreach (Transform tilemap in tilemaps)
            {
                GameObject erased = GetObjectInCell(grid, tilemap, new Vector3Int(position.x, position.y, position.z));
                if (erased != null) Undo.DestroyObjectImmediate(erased);
            }
        }
        
        /// <summary>
        /// This is not supported but it should floodfill GameObjects starting from a given position within the selected layers.
        /// </summary>
        /// <param name="gridLayout">Grid used for layout.</param>
        /// <param name="brushTarget">Target of the flood fill operation. By default the currently selected GameObject.</param>
        /// <param name="position">Starting position of the flood fill.</param>
        public override void FloodFill(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
        {
            Debug.LogWarning("FloodFill not supported");
        }

        /// <summary>
        /// Rotates the brush by 90 degrees in the given direction.
        /// </summary>
        /// <param name="direction">Direction to rotate by.</param>
        /// <param name="layout">Cell Layout for rotating.</param>
        public override void Rotate(RotationDirection direction, GridLayout.CellLayout layout)
        {
            Vector3Int oldSize = _size;
            BrushCell[] oldCells = cellsField.Clone() as BrushCell[];
            Size = new Vector3Int(oldSize.y, oldSize.x, oldSize.z);
            BoundsInt oldBounds = new BoundsInt(Vector3Int.zero, oldSize);

            foreach (Vector3Int oldPos in oldBounds.allPositionsWithin)
            {
                int newX = direction == RotationDirection.Clockwise ? oldSize.y - oldPos.y - 1 : oldPos.y;
                int newY = direction == RotationDirection.Clockwise ? oldPos.x : oldSize.x - oldPos.x - 1;
                int toIndex = GetCellIndex(newX, newY, oldPos.z);
                int fromIndex = GetCellIndex(oldPos.x, oldPos.y, oldPos.z, oldSize.x, oldSize.y, oldSize.z);
                if (oldCells != null) cellsField[toIndex] = oldCells[fromIndex];
            }

            int newPivotX = direction == RotationDirection.Clockwise ? oldSize.y - Pivot.y - 1 : Pivot.y;
            int newPivotY = direction == RotationDirection.Clockwise ? Pivot.x : oldSize.x - Pivot.x - 1;
            Pivot = new Vector3Int(newPivotX, newPivotY, Pivot.z);

            Matrix4x4 rotation = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0f, 0f, direction == RotationDirection.Clockwise ? 90f : -90f), Vector3.one);
            Quaternion orientation = Quaternion.Euler(0f, 0f, direction == RotationDirection.Clockwise ? 90f : -90f);
            minOrientation *= orientation;
            maxOrientation *= orientation;
            foreach (BrushCell cell in cellsField)
            {
                cell.Offset = rotation * cell.Offset;
            }
        }

        /// <summary>Flips the brush in the given axis.</summary>
        /// <param name="flip">Axis to flip by.</param>
        /// <param name="layout">Cell Layout for flipping.</param>
        public override void Flip(FlipAxis flip, GridLayout.CellLayout layout)
        {
            if (flip == FlipAxis.X)
                FlipX();
            else
                FlipY();
        }

        /// <summary>
        /// Picks child GameObjects given the coordinates of the cells.
        /// The GameObjectBrush overrides this to provide GameObject picking functionality.
        /// </summary>
        /// <param name="gridLayout">Grid to pick data from.</param>
        /// <param name="brushTarget">Target of the picking operation. By default the currently selected GameObject.</param>
        /// <param name="position">The coordinates of the cells to paint data from.</param>
        /// <param name="pivot">Pivot of the picking brush.</param>
        public override void Pick(GridLayout gridLayout, GameObject brushTarget, BoundsInt position, Vector3Int pivot)
        {
            _pickedObjects = true;
            GetGrid(ref gridLayout, ref brushTarget);
            if (brushTarget == null)
            {
                _size = position.size;
                Pivot = position.size/2;
                return;
            }

            List<Transform> parentTransforms = new List<Transform>();
            if (_paintOnTilemapGroup && brushTarget != null && brushTarget.GetComponent<TileMapSelectionInfo>())
            {
                foreach (TileMapSelectionInfo relatedTilemapInfo in brushTarget.GetComponent<TileMapSelectionInfo>().relatedTilemaps)
                {
                    parentTransforms.Add(relatedTilemapInfo.transform);
                }
            }
            else
            {
                parentTransforms.Add(brushTarget != null ? brushTarget.transform : null);
            }

            foreach (Transform parentTransform in parentTransforms)
            {
                foreach (Vector3Int pos in position.allPositionsWithin)
                {
                    Vector3Int brushPosition = new Vector3Int(pos.x - position.x, pos.y - position.y, 0);
                    PickCell(pos, brushPosition, gridLayout, parentTransform, position.center);
                }
            }

            _size = position.size;
            Pivot = position.size/2;
            if (_size == Vector3Int.one)
            {
                _pickedObjects = false;
            }
        }

        private void PickCell(Vector3Int position, Vector3Int brushPosition, GridLayout grid, Transform parent, Vector3? center)
        {
            Vector3 cellCenter = grid.LocalToWorld(grid.CellToLocalInterpolated(position + anchor));
            if (center != null)
            {
                cellCenter = grid.LocalToWorld(grid.CellToLocalInterpolated((Vector3)center));
            }

            if (parent == null)
            {
                parent = hiddenGrid.transform;
            }
            GameObject go = GetObjectInCell(grid, parent, position);

            if (go != null)
            {
                Vector3 offset = go.transform.position - cellCenter;

                if (go)
                {
                    SetGameObject(brushPosition, go, offset);
                }
            }
        }

        /// <summary>
        /// MoveStart is called when user starts moving the area previously selected with the selection marquee.
        /// The GameObjectBrush overrides this to provide GameObject moving functionality.
        /// </summary>
        /// <param name="gridLayout">Grid used for layout.</param>
        /// <param name="brushTarget">Target of the move operation. By default the currently selected GameObject.</param>
        /// <param name="position">Position where the move operation has started.</param>
        public override void MoveStart(GridLayout gridLayout, GameObject brushTarget, BoundsInt position)
        {

            GetGrid(ref gridLayout, ref brushTarget);

            Transform targetTransform = brushTarget != null ? brushTarget.transform : null;
            foreach (Vector3Int pos in position.allPositionsWithin)
            {
                Vector3Int brushPosition = new Vector3Int(pos.x - position.x, pos.y - position.y, 0);
                PickCell(pos, brushPosition, gridLayout, targetTransform, pos);
                ClearSceneCell(gridLayout, targetTransform, pos);
            }
        }

        /// <summary>
        /// MoveEnd is called when user has ended the move of the area previously selected with the selection marquee.
        /// The GameObjectBrush overrides this to provide GameObject moving functionality.
        /// </summary>
        /// <param name="gridLayout">Grid used for layout.</param>
        /// <param name="brushTarget">Target of the move operation. By default the currently selected GameObject.</param>
        /// <param name="position">Position where the move operation has ended.</param>
        // ReSharper disable once RedundantAssignment
        public override void MoveEnd(GridLayout gridLayout, GameObject brushTarget, BoundsInt position)
        {
            brushTarget = GridPaintingState.scenePaintTarget;
            GetGrid(ref gridLayout, ref brushTarget);
            Paint(gridLayout, brushTarget, position.min);
        }

        private List<Tuple<RandomGameObjectBrush, TileMapSelectionInfoRef>> GetTilemapGroup(GameObject brushTarget)
        {
            //check if one of the default tilemaps are used
            if (brushTarget == null || brushTarget == hiddenGrid)
            {
                TileMapSelectionInfoRef tileMapSelectionInfoRef = new TileMapSelectionInfoRef(null, name, sortingLayer, lowestOrderInLayer, newColor, minScale.x, drawEmptyCellsAfterEachDraw, 0, null);
                List<Tuple<RandomGameObjectBrush, TileMapSelectionInfoRef>> defaultTupleList = new() { new (this, tileMapSelectionInfoRef) };
                return defaultTupleList;
            }
            if (!brushTarget.GetComponent<TileMapSelectionInfo>())
            {
                TileMapSelectionInfoRef tileMapSelectionInfoRef = new TileMapSelectionInfoRef(brushTarget.transform, name, sortingLayer, lowestOrderInLayer, newColor, minScale.x, drawEmptyCellsAfterEachDraw, 0, null);
                List<Tuple<RandomGameObjectBrush, TileMapSelectionInfoRef>> defaultTupleList = new() { new (this, tileMapSelectionInfoRef) };
                return defaultTupleList;
            }
            
            //assume we use a tilemap of this framework
            List<Tuple<RandomGameObjectBrush, TileMapSelectionInfoRef>> brushClasses = new();
            
            foreach (TileMapSelectionInfo relatedTilemapInfo in brushTarget.GetComponent<TileMapSelectionInfo>().relatedTilemaps)
            {
                GridBrushBase brushBase = null;
                foreach (GridBrushBase usedBrush in GridPaintingState.brushes)
                {
                    if (usedBrush.name == relatedTilemapInfo.brush)
                    {
                        brushBase = usedBrush;
                    }
                }

                Tuple<RandomGameObjectBrush, TileMapSelectionInfoRef> infoTuple = new(brushBase as RandomGameObjectBrush, new TileMapSelectionInfoRef(relatedTilemapInfo));
                brushClasses.Add(infoTuple);
            }
            return brushClasses;
        }
        
        #endregion
        
        #region Basic Methods
        private void GetGrid(ref GridLayout gridLayout, ref GameObject brushTarget)
        {
            if (brushTarget == hiddenGrid)
            {
                brushTarget = null;
            }
            else if (brushTarget != null)
            {
                GridLayout targetGridLayout = brushTarget.GetComponent<GridLayout>();
                if (targetGridLayout != null)
                    gridLayout = targetGridLayout;
            }
            else if (brushTarget == null)
            {
                if (ToolManager.activeToolType == typeof(PickingTool))
                {
                    Debug.LogWarning("Cannot get Grid!");
                }
            }
        }

        private void FlipX()
        {
            BrushCell[] oldCells = cellsField.Clone() as BrushCell[];
            BoundsInt oldBounds = new BoundsInt(Vector3Int.zero, _size);

            foreach (Vector3Int oldPos in oldBounds.allPositionsWithin)
            {
                int newX = _size.x - oldPos.x - 1;
                int toIndex = GetCellIndex(newX, oldPos.y, oldPos.z);
                int fromIndex = GetCellIndex(oldPos);
                if (oldCells != null) cellsField[toIndex] = oldCells[fromIndex];
            }

            int newPivotX = _size.x - Pivot.x - 1;
            Pivot = new Vector3Int(newPivotX, Pivot.y, Pivot.z);
            Matrix4x4 flip = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(-1f, 1f, 1f));
            
            foreach (BrushCell cell in cellsField)
            {
                Vector3 oldOffset = cell.Offset;
                cell.Offset = flip * oldOffset;
            }
        }

        private void FlipY()
        {
            BrushCell[] oldCells = cellsField.Clone() as BrushCell[];
            BoundsInt oldBounds = new BoundsInt(Vector3Int.zero, _size);

            foreach (Vector3Int oldPos in oldBounds.allPositionsWithin)
            {
                int newY = _size.y - oldPos.y - 1;
                int toIndex = GetCellIndex(oldPos.x, newY, oldPos.z);
                int fromIndex = GetCellIndex(oldPos);
                if (oldCells != null) cellsField[toIndex] = oldCells[fromIndex];
            }

            int newPivotY = _size.y - Pivot.y - 1;
            Pivot = new Vector3Int(Pivot.x, newPivotY, Pivot.z);
            Matrix4x4 flip = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(1f, -1f, 1f));
            foreach (BrushCell cell in cellsField)
            {
                Vector3 oldOffset = cell.Offset;
                cell.Offset = flip * oldOffset;
            }
        }

        /// <summary>
        /// Sets a GameObject at the position in the brush.
        /// </summary>
        /// <param name="position">Position to set the GameObject in the brush.</param>
        /// <param name="go">GameObject to set in the brush.</param>
        /// <param name="offset">Offset used for the preview.</param>
        public void SetGameObject(Vector3Int position, GameObject go, Vector3 offset)
        {
            RandomGameObjectBrush randomGameObjectBrush;
            TileMapSelectionInfoRef tileMapSelectionInfoRef;
            
            if (!go.transform.parent || !go.transform.parent.GetComponent<TileMapSelectionInfo>())
            {
                randomGameObjectBrush = this;
                Transform transformRef = null;
                if (go.transform.parent)
                {
                    transformRef = go.transform.parent;
                }
                tileMapSelectionInfoRef = new TileMapSelectionInfoRef(transformRef, name, sortingLayer, lowestOrderInLayer, newColor, minScale.x, drawEmptyCellsAfterEachDraw, 0, null);
            }
            else
            {
                GridBrushBase brushBase = null;
                foreach (GridBrushBase usedBrush in GridPaintingState.brushes)
                {
                    if (usedBrush.name == go.transform.parent.GetComponent<TileMapSelectionInfo>().brush)
                    {
                        brushBase = usedBrush;
                    }
                }
                randomGameObjectBrush = brushBase as RandomGameObjectBrush;
                tileMapSelectionInfoRef = new(go.transform.parent.GetComponent<TileMapSelectionInfo>());
            }
            
            GameObject prefab = PrefabUtility.GetCorrespondingObjectFromSource(go);
            Tuple<RandomGameObjectBrush, TileMapSelectionInfoRef> brushTuple= new(randomGameObjectBrush, tileMapSelectionInfoRef);
            CreatePreviewObject(Vector3.zero, brushTuple, prefab, offset);
        }

        /// <summary>Gets the index to the GameObjectBrush::ref::BrushCell based on the position of the BrushCell.</summary>
        /// <param name="brushPosition">Position of the BrushCell.</param>
        /// <returns>The cell index for the position of the BrushCell.</returns>
        public int GetCellIndex(Vector3Int brushPosition)
        {
            return GetCellIndex(brushPosition.x, brushPosition.y, brushPosition.z);
        }

        /// <summary>Gets the index to the GameObjectBrush::ref::BrushCell based on the position of the BrushCell.</summary>
        /// <param name="x">X Position of the BrushCell.</param>
        /// <param name="y">Y Position of the BrushCell.</param>
        /// <param name="z">Z Position of the BrushCell.</param>
        /// <returns>The cell index for the position of the BrushCell.</returns>
        public int GetCellIndex(int x, int y, int z)
        {
            return x + _size.x * y + _size.x * _size.y * z;
        }

        /// <summary>Gets the index to the GameObjectBrush::ref::BrushCell based on the position of the BrushCell.</summary>
        /// <param name="x">X Position of the BrushCell.</param>
        /// <param name="y">Y Position of the BrushCell.</param>
        /// <param name="z">Z Position of the BrushCell.</param>
        /// <param name="sizex">X Size of Brush.</param>
        /// <param name="sizey">Y Size of Brush.</param>
        /// <param name="sizez">Z Size of Brush.</param>
        /// <returns>The cell index for the position of the BrushCell.</returns>
        public int GetCellIndex(int x, int y, int z, int sizex, int sizey, int sizez)
        {
            return x + sizex * y + sizex * sizey * z;
        }

        /// <summary>Gets the index to the GameObjectBrush::ref::BrushCell based on the position of the BrushCell. Wraps each coordinate if it is larger than the size of the GameObjectBrush.</summary>
        /// <param name="x">X Position of the BrushCell.</param>
        /// <param name="y">Y Position of the BrushCell.</param>
        /// <param name="z">Z Position of the BrushCell.</param>
        /// <returns>The cell index for the position of the BrushCell.</returns>
        public int GetCellIndexWrapAround(int x, int y, int z)
        {
            return (x % _size.x) + _size.x * (y % _size.y) + _size.x * _size.y * (z % _size.z);
        }

        protected GameObject GetObjectInCell(GridLayout grid, Transform parent, Vector3Int position)
        {
            int childCount;
            GameObject[] sceneChildren = null;
            if (parent == null)
            {
                Scene scene = SceneManager.GetActiveScene();
                sceneChildren = scene.GetRootGameObjects();
                childCount = scene.rootCount;
            }
            else
            {
                childCount = parent.childCount;
            }
            Vector3Int anchorCellOffset = Vector3Int.FloorToInt(anchor);
            Vector3 cellSize = grid.cellSize;
            anchorCellOffset.x = cellSize.x == 0 ? 0 : anchorCellOffset.x;
            anchorCellOffset.y = cellSize.y == 0 ? 0 : anchorCellOffset.y;
            anchorCellOffset.z = cellSize.z == 0 ? 0 : anchorCellOffset.z;

            for (int i = 0; i < childCount; i++)
            {
                Transform child = sceneChildren == null ? parent.GetChild(i) : sceneChildren[i].transform;
                if (position == grid.WorldToCell(child.position) - anchorCellOffset)
                {
                    return child.gameObject;
                }
            }
            return null;
        }
        
        public Vector3Int GetSize()
        {
            return Size;
        }

        /// <summary>
        /// Hashes the contents of the brush.
        /// </summary>
        /// <returns>A hash code of the brush</returns>
        public override int GetHashCode()
        {
            int hash = 2;
            unchecked
            {
                if (CellsField != null)
                {
                    foreach (BrushCell cell in CellsField)
                    {
                        hash = hash * 33 + cell.GetHashCode();
                    }
                }
                else
                {
                    //if no cells have been initialized define the HashCode with the ClassName
                    hash = hash * 33 + this.GetType().Name.GetHashCode();
                }
            }
            return hash;
        }

        internal void UpdateHiddenGridLayout()
        {
            Grid grid = hiddenGrid.GetComponent<Grid>();
            hiddenGridLayout.cellSize = grid.cellSize;
            hiddenGridLayout.cellGap = grid.cellGap;
            hiddenGridLayout.cellSwizzle = grid.cellSwizzle;
            hiddenGridLayout.cellLayout = grid.cellLayout;
        }
        
        #endregion
        
        private void SetSceneCell(GridLayout grid, Vector3Int position, PreviewInfo previewInfo, RandomParameters randomParameters)
        {
            PreviewInfo previewInfoRef = _previewInfos.FirstOrDefault(eri => eri.TileMapSelectionInfoRef.Equals(previewInfo.TileMapSelectionInfoRef));
            
            Transform parent = previewInfo.TileMapSelectionInfoRef.TransformRef;
            
            if (previewInfo.PreviewObject == null) return;
            
            if (drawChanceMode)
            {
                if (!_useDrawBox)
                {
                    if (position == previewInfoRef.DrawChanceInfo.CurrentCell ||
                        position == previewInfoRef.DrawChanceInfo.LastCell)
                    {
                        return;
                    }
                }
                previewInfoRef.DrawChanceInfo.LastCell = previewInfoRef.DrawChanceInfo.CurrentCell;
                previewInfoRef.DrawChanceInfo.CurrentCell = position;
                
                float rng = Random.Range(0f, 1f);

                if (rng > pickChance)
                {
                    SafePreviewObject(previewInfoRef);
                    return;
                }
                
                if (previewInfoRef.DrawChanceInfo.CellDrawnCounter < previewInfoRef.TileMapSelectionInfoRef.EmptyCellsAfterDraw)
                {
                    previewInfoRef.DrawChanceInfo.CellDrawnCounter++;
                    SafePreviewObject(previewInfoRef);
                    return;
                }
                
                previewInfoRef.DrawChanceInfo.CellDrawnCounter -= drawEmptyCellsAfterEachDraw;
                
                if (previewInfoRef.DrawChanceInfo.CellDrawnCounter < 0)
                {
                    previewInfoRef.DrawChanceInfo.CellDrawnCounter = 0;
                }

                SafePreviewObject(previewInfoRef);
            }
            
            if (!_pickedObjects) _previewInfos.Remove(previewInfoRef);

            GameObject gameObjectRef;
            if (!_useDrawBox && !disablePreview)
            {
                gameObjectRef = PrefabUtility.GetCorrespondingObjectFromSource(previewInfo.PreviewObject);
            }
            else
            {
                gameObjectRef = previewInfo.Brush.PickBrushCell().CellGameObject;
            }
            
            GameObject instance;
            if (PrefabUtility.IsPartOfPrefabAsset(gameObjectRef))
            {
                if (parent == null)
                {
                    instance = (GameObject) PrefabUtility.InstantiatePrefab(gameObjectRef, SceneManager.GetActiveScene());
                }
                else
                {
                    Scene scene;
                    if (parent.name.Equals(PaintOnSceneRoot))
                    {
                        scene = SceneManager.GetActiveScene();
                    }
                    else
                    {
                        scene = parent.root.gameObject.scene;
                    }
                    instance = (GameObject) PrefabUtility.InstantiatePrefab(gameObjectRef, scene);
                }
                instance.transform.parent = parent;
            }
            else
            {
                instance = Instantiate(gameObjectRef, parent);
                instance.name = gameObjectRef.name;
                instance.hideFlags = HideFlags.None;
                instance.SetActive(true);
                foreach (Renderer renderer in instance.GetComponentsInChildren<Renderer>())
                {
                    renderer.enabled = true;
                }
            }
            Undo.RegisterCreatedObjectUndo(instance, "Paint GameObject");
            instance.transform.position = grid.LocalToWorld(grid.CellToLocalInterpolated(new Vector3Int(position.x, position.y, position.z) + previewInfo.Brush.anchor));
            
            Vector3 offset = previewInfo.Offset;
            if (_paintOnTilemapGroup && !keepRotation) offset = new (0,previewInfo.TileMapSelectionInfoRef.LayerOffset,0);
            if (_pickedObjects) offset = Vector3.zero;
            SetupDrawnObject(randomParameters, previewInfo, offset, instance);
            
            _useDrawBox = false;

            if (!_pickedObjects)
            {
                if(!disablePreview) DestroyImmediate(previewInfo.PreviewObject);
                _previewInfos.Remove(previewInfo);
            }

            void SafePreviewObject(PreviewInfo previewInfoToSafe)
            {
                PreviewInfo previewInfoToRemove = _previewInfos.FirstOrDefault(eri => eri.TileMapSelectionInfoRef.Equals(previewInfo.TileMapSelectionInfoRef));
                _previewInfos.Remove(previewInfoToRemove);
                _previewInfos.Add(previewInfoToSafe);
            }
        }
        
        #region additional Methods required for drawing
        public void SetupNewCellsGameObjects()
        {
            BrushCell[] oldCells = cellsField;
            cellsField = new BrushCell[dragInObjectField.Length];

            for (int i = 0; i < dragInObjectField.Length; i++)
            {
                if (i < oldCells.Length)
                {
                    cellsField[i] = oldCells[i];
                    if (oldCells[i].CellGameObject != dragInObjectField[i])
                    {
                        cellsField[i].CellGameObject = dragInObjectField[i];
                    }
                }
                else
                {
                    cellsField[i] = new BrushCell();
                    cellsField[i].CellGameObject = dragInObjectField[i];
                }
            }
        }
        
        private BrushCell PickBrushCell()
        {
            int cellNumber;
            if (!disableRandom)
            {
                int rng = Random.Range(0, cellsField.Length);
                cellNumber = rng;
            }
            else
            {
                if (pickElement > cellsField.Length - 1)
                    if (pickElement > cellsField.Length - 1)
                    {
                        throw new Exception("Pick Element is bigger then Cell Size - 1");
                    }
                cellNumber = pickElement;
            }
                
            return cellsField[cellNumber];
        }
        
        private List<RandomGameObjectBrush> GetAllRandomGameObjectBrushes()
        {
            List<RandomGameObjectBrush> brushClasses = new();
            
            foreach (GridBrushBase brush in GridPaintingState.brushes)
            {
                RandomGameObjectBrush randomBrush = brush as RandomGameObjectBrush;
                if (randomBrush != null)
                {
                    brushClasses.Add(randomBrush);
                }
            }
            return brushClasses;
        }
        
        
        //a function to override for special cases, if you want certain function in classes that inherit this class, but dont do generic stuff
        protected virtual void DoAdditionalStuff(GameObject go)
        {
            //override this
        }
        #endregion
        
        #region Structs
        
        private struct RandomParameters
        {
            public RandomParameters(Quaternion orientation, Vector3 offset, Vector3 scale)
            {
                Orientation = orientation;
                Offset = offset;
                Scale = scale;
            }
            public readonly Quaternion Orientation;
            public readonly Vector3 Offset;
            public readonly Vector3 Scale;
        }

        public struct PreviewInfo
        {
            public PreviewInfo(RandomGameObjectBrush brush, GameObject previewObject, BrushCell cell, TileMapSelectionInfoRef tileMapSelectionInfoRef, DrawChanceInfo drawChanceInfo, Vector3 offset)
            {
                Brush = brush;
                PreviewObject = previewObject;
                Cell = cell;
                TileMapSelectionInfoRef = tileMapSelectionInfoRef;
                DrawChanceInfo = drawChanceInfo;
                Offset = offset;
            }
            public readonly RandomGameObjectBrush Brush;
            public GameObject PreviewObject;
            public readonly BrushCell Cell;
            public TileMapSelectionInfoRef TileMapSelectionInfoRef;
            public DrawChanceInfo DrawChanceInfo;
            public Vector3 Offset;
        }

        public struct TileMapSelectionInfoRef
        {
            public Transform TransformRef;
            public string Brush;
            public int SortingLayer;
            public int OrderInLayer;
            public Color TileMapColor;
            public float MinScale;
            public float EmptyCellsAfterDraw;
            public float LayerOffset;
            public List<TileMapSelectionInfo> RelatedTilemaps;
            
            public TileMapSelectionInfoRef(Transform transformRef, string brush, int sortingLayer, int orderInLayer, Color tileMapColor, float minScale, float emptyCellsAfterDraw, float layerOffset, List<TileMapSelectionInfo> relatedTilemaps)
            {
                TransformRef = transformRef;
                Brush = brush;
                SortingLayer = sortingLayer;
                OrderInLayer = orderInLayer;
                TileMapColor = tileMapColor;
                MinScale = minScale;
                EmptyCellsAfterDraw = emptyCellsAfterDraw;
                LayerOffset = layerOffset;
                RelatedTilemaps = relatedTilemaps;
            }

            public TileMapSelectionInfoRef(TileMapSelectionInfo tileMapSelectionInfo)
            {
                TransformRef = tileMapSelectionInfo.transform;
                Brush = tileMapSelectionInfo.brush;
                SortingLayer = tileMapSelectionInfo.sortingLayer;
                OrderInLayer = tileMapSelectionInfo.orderInLayer;
                TileMapColor = tileMapSelectionInfo.tileMapColor;
                MinScale = tileMapSelectionInfo.minScale;
                EmptyCellsAfterDraw = tileMapSelectionInfo.emptyCellsAfterDraw;
                LayerOffset = tileMapSelectionInfo.layerOffset;
                RelatedTilemaps = tileMapSelectionInfo.relatedTilemaps;
            }
        }
        
        public struct DrawChanceInfo
        {
            public DrawChanceInfo(Vector3Int lastCell, Vector3Int currentCell, int cellDrawnCounter)
            {
                LastCell = lastCell;
                CurrentCell = currentCell;
                CellDrawnCounter = cellDrawnCounter;
            }
            public Vector3Int LastCell;
            public Vector3Int CurrentCell;
            public float CellDrawnCounter;
        }

        #endregion

        private void OnDisable()
        {
            foreach (PreviewInfo previewInfo in _previewInfos)
            {
                DestroyImmediate(previewInfo.PreviewObject);
            }
            _previewInfos.Clear();
            if (offsetPoint)
            {
                DestroyImmediate(offsetPoint);
            }
        }

        /// <summary>
        ///Brush Cell stores the data to be painted in a grid cell.
        /// </summary>
        [Serializable]
        public class BrushCell
        {
            /// <summary>
            /// GameObject to be placed when painting.
            /// </summary>
            public GameObject CellGameObject { get { return mCellGameObject; } set { mCellGameObject = value; } }
            /// <summary>
            /// Position offset of the GameObject when painted.
            /// </summary>
            public Vector3 Offset { get { return mOffset; } set { mOffset = value; } }
            
            [SerializeField] private GameObject mCellGameObject;
            [SerializeField] Vector3 mOffset = Vector3.zero;

            /// <summary>
            /// Hashes the contents of the brush cell.
            /// </summary>
            /// <returns>A hash code of the brush cell.</returns>
            public override int GetHashCode()
            {
                int hash;
                unchecked
                {
                    hash = CellGameObject != null ? CellGameObject.GetInstanceID() : 0;
                    hash = hash * 33 + Offset.GetHashCode();
                }
                return hash;
            }
        }
    }

    /// <summary>
    /// The Brush Editor for a GameObject Brush.
    /// </summary>
    [CustomEditor(typeof(RandomGameObjectBrush))]
    public class RandomGameObjectEditor : GridBrushEditorBase
    {
        private bool _hiddenGridFoldout;
        private UnityEditor.Editor _hiddenGridEditor;

        /// <summary>
        /// The GameObjectBrush for this Editor
        /// </summary>
        private RandomGameObjectBrush Brush { get { return target as RandomGameObjectBrush; } }

        private GameObject[] _oldDragInObjectField;
        private GameObject _oldPreviewObject;

        /// <summary> Whether the GridBrush can change Z Position. </summary>
        public override bool canChangeZPosition
        {
            get { return Brush.CanChangeZPosition; }
            set { Brush.CanChangeZPosition = value; }
        }
        
        /// <summary>
        /// Callback for painting the GUI for the GridBrush in the Scene View.
        /// The GameObjectBrush Editor overrides this to draw the preview of the brush when drawing lines.
        /// </summary>
        /// <param name="gridLayout">Grid that the brush is being used on.</param>
        /// <param name="brushTarget">Target of the GameObjectBrush::ref::Tool operation. By default the currently selected GameObject.</param>
        /// <param name="position">Current selected location of the brush.</param>
        /// <param name="tool">Current GameObjectBrush::ref::Tool selected.</param>
        /// <param name="executing">Whether brush is being used.</param>
        public override void OnPaintSceneGUI(GridLayout gridLayout, GameObject brushTarget, BoundsInt position, GridBrushBase.Tool tool, bool executing)
        {
            BoundsInt gizmoRect = position;

            if (tool == GridBrushBase.Tool.Paint || tool == GridBrushBase.Tool.Erase)
                gizmoRect = new BoundsInt(position.min - Brush.Pivot, Brush.GetSize());
            
            base.OnPaintSceneGUI(gridLayout, brushTarget, gizmoRect, tool, executing);
        }

        /// <summary>
        /// Callback for painting the inspector GUI for the GameObjectBrush in the tilemap palette.
        /// The GameObjectBrush Editor overrides this to show the usage of this Brush.
        /// </summary>
        public override void OnPaintInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();
            base.OnInspectorGUI();
            bool conditionMet = Brush.dragInObjectField != _oldDragInObjectField;

            if(conditionMet)
            {
                GUI.changed = true;
                _oldDragInObjectField = Brush.dragInObjectField;
                Brush.SetupNewCellsGameObjects();
                Brush.UpdateHiddenGridLayout();
            }
            EditorGUI.EndChangeCheck();

            _hiddenGridFoldout = EditorGUILayout.Foldout(_hiddenGridFoldout, "SceneRoot Grid");
            if (_hiddenGridFoldout)
            {
                EditorGUI.indentLevel++;
                using (new EditorGUI.DisabledScope(GridPaintingState.scenePaintTarget != Brush.hiddenGrid))
                {
                    if (_hiddenGridEditor == null)
                    {
                        _hiddenGridEditor = UnityEditor.Editor.CreateEditor(Brush.hiddenGrid.GetComponent<Grid>());
                    }
                    Brush.hiddenGrid.hideFlags = HideFlags.None;
                    EditorGUI.BeginChangeCheck();
                    _hiddenGridEditor.OnInspectorGUI();
                    if (EditorGUI.EndChangeCheck())
                    {
                        Brush.UpdateHiddenGridLayout();
                        EditorUtility.SetDirty(Brush);
                        SceneView.RepaintAll();
                    }
                    Brush.hiddenGrid.hideFlags = HideFlags.HideAndDontSave;
                }
                EditorGUI.indentLevel--;
            }
        }

        /// <summary>
        /// The targets that the GameObjectBrush can paint on
        /// </summary>
        public override GameObject[] validTargets
        {
            get
            {
                StageHandle currentStageHandle = StageUtility.GetCurrentStageHandle();
                IEnumerable<GameObject> results = currentStageHandle.FindComponentsOfType<GridLayout>().Where(x =>
                {
                    GameObject gameObject;
                    return (gameObject = x.gameObject).scene.isLoaded
                           && gameObject.activeInHierarchy;
                }).Select(x => x.gameObject);
                return results.Prepend(Brush.hiddenGrid).ToArray();
            }
        }
    }
}