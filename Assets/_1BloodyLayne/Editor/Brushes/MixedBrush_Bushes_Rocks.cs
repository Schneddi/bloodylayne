using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Mixed Brush (Bushes & Rocks)")]
    public class MixedBrushBushesRocks : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(MixedBrushBushesRocks))]
    public class MixedBrushBushesRocksEditor : RandomGameObjectEditor
    {
    }
}