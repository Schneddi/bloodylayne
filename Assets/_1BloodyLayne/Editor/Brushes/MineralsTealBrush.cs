using Editor.Brushes;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Minerals Teal Brush")]
    public class MineralsTealBrush : MineralBrush
    {
    }

    [CustomEditor(typeof(MineralsTealBrush))]
    public class MineralsTealBrushEditor : RandomGameObjectEditor
    {
    }
}