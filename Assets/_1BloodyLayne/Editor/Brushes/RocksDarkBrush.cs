using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Rocks Dark Brush")]
    public class RocksDarkBrush : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(RocksDarkBrush))]
    public class RocksDarkBrushEditor : RandomGameObjectEditor
    {
    }
}