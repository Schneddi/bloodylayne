using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Vegetation Long Brush")]
    public class VegetationLongBrush : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(VegetationLongBrush))]
    public class VegetationLongBrushEditor : RandomGameObjectEditor
    {
    }
}