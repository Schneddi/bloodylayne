using Editor.Brushes;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Empty Brush")]
    public class EmptyBrush : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(EmptyBrush))]
    public class EmptyBrushEditor : RandomGameObjectEditor
    {
    }
}