using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{  
    [CustomGridBrush(true, false, false, "Tree Brush")]
    public class TreeBrush : RandomGameObjectBrush
    {
    }
    
    [CustomEditor(typeof(TreeBrush))]
    public class TreeBrushEditor : RandomGameObjectEditor
    {
    }
}