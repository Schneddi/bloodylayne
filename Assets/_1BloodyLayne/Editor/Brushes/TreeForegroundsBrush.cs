using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{  
    [CustomGridBrush(true, false, false, "Tree Foregrounds Brush")]
    public class TreeForegroundsBrush : RandomGameObjectBrush
    {
    }
    
    [CustomEditor(typeof(TreeForegroundsBrush))]
    public class TreeForegroundsBrushEditor : RandomGameObjectEditor
    {
    }
}