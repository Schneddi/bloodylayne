using Editor.Brushes;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Minerals Orange Brush")]
    public class MineralsOrangeBrush : MineralBrush
    {
    }

    [CustomEditor(typeof(MineralsOrangeBrush))]
    public class MineralsOrangeBrushEditor : RandomGameObjectEditor
    {
    }
}