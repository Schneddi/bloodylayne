using Editor.Brushes;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Minerals Green Brush")]
    public class MineralsGreenBrush : MineralBrush
    {
    }

    [CustomEditor(typeof(MineralsGreenBrush))]
    public class MineralsGreenBrushEditor : RandomGameObjectEditor
    {
    }
}