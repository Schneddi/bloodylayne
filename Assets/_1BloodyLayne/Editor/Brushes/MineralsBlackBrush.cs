using Editor.Brushes;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Minerals Black Brush")]
    public class MineralsBlackBrush : MineralBrush
    {
    }

    [CustomEditor(typeof(MineralsBlackBrush))]
    public class MineralsBlackBrushEditor : RandomGameObjectEditor
    {
    }
}