using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.Brushes
{
    [CustomGridBrush(true, false, false, "Sticks Brush")]
    public class StickBrush : RandomGameObjectBrush
    {
    }

    [CustomEditor(typeof(StickBrush))]
    public class StickBrushEditor : RandomGameObjectEditor
    {
    }
}