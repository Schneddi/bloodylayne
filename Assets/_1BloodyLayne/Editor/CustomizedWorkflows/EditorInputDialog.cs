using System;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.CustomizedWorkflows
{
    public class EditorInputDialog : EditorWindow
    {
        public StringWindowReturnValue StringWindowReturnValue;
    
        private string  _description, _inputText, _okButton, _cancelButton;
        private bool    _initializedPosition, _shouldClose;
        private Action  _onOk, _onCancel;
 
        #region WindowFunctions()
        void OnGUI()
        {
            // Check if Esc/Return have been pressed
            Event e = Event.current;
            if( e.type == EventType.KeyDown )
            {
                switch( e.keyCode )
                {
                    // Escape pressed
                    case KeyCode.Escape:
                        _onCancel?.Invoke();
                        _shouldClose = true;
                        break;
 
                    // Enter pressed
                    case KeyCode.Return:
                    case KeyCode.KeypadEnter:
                        _onOk?.Invoke();
                        _shouldClose = true;
                        break;
                }
            }
 
            if( _shouldClose ) {  // Close this dialog
                //return;
                Close();
            }
 
            // Draw our control
            Rect rect = EditorGUILayout.BeginVertical();
 
            EditorGUILayout.Space( 12 );
            GUILayoutOption[] options = new GUILayoutOption[1];
            options[0] = GUILayout.Height(30);
            EditorGUILayout.LabelField( _description, options );
 
            EditorGUILayout.Space( 8 );
            GUI.SetNextControlName( "inText" );
            _inputText = EditorGUILayout.TextField( "", _inputText );
            GUI.FocusControl( "inText" );   // Focus text field
            EditorGUILayout.Space( 12 );
 
            // Draw OK / Cancel buttons
            Rect r = EditorGUILayout.GetControlRect();
            r.width /= 2;
        
            if( GUI.Button( r, _okButton ) ) {
                _onOk?.Invoke();
            
            }
 
            r.x += r.width;
            if( GUI.Button( r, _cancelButton ) ) {
                _onCancel?.Invoke();
                _inputText = null;   // Cancel - delete inputText
            }
 
            EditorGUILayout.Space( 8 );
            EditorGUILayout.EndVertical();

            // Force change size of the window
            if( rect.width != 0 && minSize != rect.size ) {
                minSize = maxSize = rect.size;
            }
        
            //set the dialog position
            if( !_initializedPosition ) {
                Vector2 windowPos = new Vector2((Screen.width), (Screen.height));
                position = new Rect( windowPos.x, windowPos.y, position.width, position.height);
                _initializedPosition = true;
            }
        }

        private void OnDestroy()
        {
            if (!StringWindowReturnValue.Done)
            {
                _onCancel?.Invoke();
            }
        }
        #endregion WindowFunctions()
 
        #region Show()
        /// <summary>
        /// Returns text player entered, or null if player cancelled the dialog.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="inputText"></param>
        /// <param name="okButton"></param>
        /// <param name="cancelButton"></param>
        /// <returns></returns>
        public static EditorInputDialog Show( string title, string description, string inputText, string okButton = "OK", string cancelButton = "Cancel" )
        {
            EditorInputDialog window = CreateInstance<EditorInputDialog>();
            window.StringWindowReturnValue = new StringWindowReturnValue();
            window.titleContent = new GUIContent( title );
            window._description = description;
            window._inputText = inputText;
            window._okButton = okButton;
            window._cancelButton = cancelButton;
            window._onOk += PressOk;
            window._onCancel += PressCancel;
            //ShowModal would be better, but will cause the GridTilePalette to throw an Error!
            window.Show();
            return window;
        
            void PressOk()
            {
                window.StringWindowReturnValue.ReturnValue = window._inputText;
                window.StringWindowReturnValue.Done = true;
                window._shouldClose = true;
            }
            void PressCancel()
            {
                window.StringWindowReturnValue.Cancel = true;
                window.StringWindowReturnValue.Done = true;
                window._shouldClose = true;
            }
        }

        #endregion Show()
    }

    public struct StringWindowReturnValue
    {
        public bool Done;
        public bool Cancel;
        public string ReturnValue;
    }
}