using System;
using System.Collections.Generic;
using _1BloodyLayne.Editor.Brushes;
using Maps.Doodads;
using UnityEditor;
using UnityEditor.Tilemaps;
using UnityEngine;

namespace Editor.CustomizedWorkflows
{
// Creates an instance of a primitive depending on the option selected by the user.
    public class PickBrushEditorWindow : EditorWindow
    {
        public static BrushWindowReturnValue BrushWindowReturnValue;

        private static int _selected;
        private static readonly List<string> OptionList = new List<string>();
        private static readonly List<GridBrushBase> Brushes = new List<GridBrushBase>();
        private TileMapSelectionInfo _tileMapSelectionInfo;
        private Action _onOk, _onCancel;
        private bool _initializedPosition, _shouldClose;
        private string _description;

        public static void ShowDropdown(string title, string description, string gridBrushBaseName = null)
        {
            BrushWindowReturnValue = new BrushWindowReturnValue();

            PickBrushEditorWindow window = CreateInstance<PickBrushEditorWindow>();
            window.titleContent = new GUIContent(title);
            window._onOk += PressOk;
            window._onCancel += PressCancel;
            window._description = description;
            window.Show();

            OptionList.Add("None");
            Brushes.Add(null);
            foreach (GridBrushBase brush in GridPaintingState.brushes)
            {
                if (brush is RandomGameObjectBrush)
                {
                    OptionList.Add(brush.name);
                    Brushes.Add(brush);
                }

                if (!String.IsNullOrEmpty(gridBrushBaseName) && brush.name.Equals(gridBrushBaseName))
                {
                    _selected = OptionList.Count - 1;
                }
            }

            void PressOk()
            {
                BrushWindowReturnValue.ReturnValue = Brushes[_selected];
                BrushWindowReturnValue.Done = true;
            }

            void PressCancel()
            {
                BrushWindowReturnValue.Cancel = true;
                BrushWindowReturnValue.Done = true;
            }
        }

        void OnGUI()
        {
            Event e = Event.current;
            if (e.type == EventType.KeyDown)
            {
                switch (e.keyCode)
                {
                    // Escape pressed
                    case KeyCode.Escape:
                        _onCancel?.Invoke();
                        _shouldClose = true;
                        break;

                    //commented out, because I like to skip most of the settings with enter most of the time, except picking a brush
                    /*
                    // Enter pressed
                    case KeyCode.Return:
                    case KeyCode.KeypadEnter:
                        onOK?.Invoke();
                        shouldClose = true;
                        break;
                    */
                }
            }

            GUILayoutOption[] options = new GUILayoutOption[1];
            options[0] = GUILayout.Height(30);
            EditorGUILayout.LabelField(_description, options);

            _selected = EditorGUILayout.Popup("Brush", _selected, OptionList.ToArray());

            if (GUILayout.Button("Pick"))
            {
                _onOk?.Invoke();
                _shouldClose = true;
            }

            if (_shouldClose)
            {
                Close();
            }

            //set the dialog position
            if (!_initializedPosition)
            {
                Vector2 windowPos = new Vector2((Screen.width / 2), (Screen.height / 4));
                position = new Rect(windowPos.x, windowPos.y, 300, position.height);
                _initializedPosition = true;
            }
        }

        private void OnDestroy()
        {
            if (!BrushWindowReturnValue.Done)
            {
                _onCancel?.Invoke();
            }
        }
    }
}

public struct BrushWindowReturnValue
{
    public bool Done;
    public bool Cancel;
    public GridBrushBase ReturnValue;
}