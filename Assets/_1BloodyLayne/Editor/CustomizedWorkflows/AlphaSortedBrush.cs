using System;
using UnityEditor;
using UnityEditor.Tilemaps;
using UnityEngine;

namespace _1BloodyLayne.Editor.CustomizedWorkflows
{
    [CustomGridBrush(true, false, false, "Alpha Sorted Brush")]
    public class AlphaSortedBrush : GridBrush {}
 
    [CustomEditor(typeof(AlphaSortedBrush))]
    public class AlphaSortedBrushEditor : GridBrushEditor
    {
        public override GameObject[] validTargets
        {
            get
            {
                GameObject[] vt = base.validTargets;
                Array.Sort(vt, (go1, go2) => String.Compare(go1.name, go2.name));
                return vt;
            }
        }
    }
}