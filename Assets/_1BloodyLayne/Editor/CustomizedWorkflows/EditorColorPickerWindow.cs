using System;
using Maps.Doodads;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.CustomizedWorkflows
{
    public class EditorColorPickerWindow : EditorWindow
    {
        public static ColorPickerWindowReturnValue ColorPickerWindowReturnValue;
    
        private Color _selectedColor;
        private Action  _onOk, _onCancel;
        private bool _shouldClose, _initializedPosition;
        private static bool _changeColorOption;

        [MenuItem("Custom Functions/Set TileMap GameObjects Color")]
        static void Init()
        {
            _changeColorOption = true;
            EditorColorPickerWindow window = CreateInstance<EditorColorPickerWindow>();
            foreach (GameObject t in Selection.objects)
            {
                foreach (TileMapSelectionInfo tileMapSelectionInfo in t.GetComponentsInChildren<TileMapSelectionInfo>())
                {
                    window._selectedColor = tileMapSelectionInfo.tileMapColor;
                }
            }
            window.Show();
        }
    
        public static void ShowColorPicker(string title, Color startColor)
        {
            ColorPickerWindowReturnValue = new ColorPickerWindowReturnValue();
            _changeColorOption = false;
        
            EditorColorPickerWindow window = CreateInstance<EditorColorPickerWindow>();
            window._selectedColor = startColor;
            window.titleContent = new GUIContent( title );
            window._onOk += PressOk;
            window._onCancel += PressCancel;
            window.Show();
        
            void PressOk()
            {
                ColorPickerWindowReturnValue.returnValue = window._selectedColor;
                ColorPickerWindowReturnValue.done = true;
            }
            void PressCancel()
            {
                ColorPickerWindowReturnValue.cancel = true;
                ColorPickerWindowReturnValue.done = true;
            }
        }

        void OnGUI()
        {
            Event e = Event.current;
            if( e.type == EventType.KeyDown )
            {
                switch( e.keyCode )
                {
                    // Escape pressed
                    case KeyCode.Escape:
                        _shouldClose = true;
                        _onCancel?.Invoke();
                        break;
 
                    // Enter pressed
                    case KeyCode.Return:
                    case KeyCode.KeypadEnter:
                        if (_changeColorOption)
                        {
                            ChangeColors(Selection.gameObjects);
                        }
                        _onOk?.Invoke();
                        _shouldClose = true;
                        break;
                }
            }
 
        
            _selectedColor = EditorGUILayout.ColorField("New Color", _selectedColor);

            if (GUILayout.Button("Change!"))
            {
                if (_changeColorOption)
                {
                    ChangeColors(Selection.gameObjects);
                }
                _onOk?.Invoke();
                _shouldClose = true;
            }
        
            if( _shouldClose ) {  // Close this dialog
                Close();
                //return;
            }
        
            //set the dialog position
            if( !_initializedPosition ) {
                Vector2 windowPos = new Vector2((Screen.width*2), (Screen.height/4));
                position = new Rect( windowPos.x, windowPos.y, position.width, position.height );
                _initializedPosition = true;
            }
        }
    
        private void OnDestroy()
        {
            if (!ColorPickerWindowReturnValue.done)
            {
                _onCancel?.Invoke();
            }
        }

        private void ChangeColors(GameObject[] changeGameObject)
        {
            foreach (GameObject t in changeGameObject)
            {
                foreach (TileMapSelectionInfo tileMapSelectionInfo in t.GetComponentsInChildren<TileMapSelectionInfo>())
                {
                    Undo.RecordObject(tileMapSelectionInfo, "Set TileMap GameObjects Color");
                    tileMapSelectionInfo.tileMapColor = _selectedColor;
                }
                foreach (SpriteRenderer spriteRenderer in t.GetComponentsInChildren<SpriteRenderer>())
                {
                    Undo.RecordObject(spriteRenderer, "Set TileMap GameObjects Color");
                    spriteRenderer.color = _selectedColor;
                }
            }
        }
    }

    public struct ColorPickerWindowReturnValue
    {
        public bool done;
        public bool cancel;
        public Color returnValue;
    }
}