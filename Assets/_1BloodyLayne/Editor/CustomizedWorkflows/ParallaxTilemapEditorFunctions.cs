using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using _1BloodyLayne.Editor.Brushes;
using Editor.CustomizedWorkflows;
using Maps.Doodads;
using TMPro.EditorUtilities;
using UnityEditor;
using UnityEditor.Tilemaps;
using UnityEngine;
using UnityEngine.Tilemaps;
using Object = UnityEngine.Object;
using Vector3 = UnityEngine.Vector3;

namespace _1BloodyLayne.Editor.CustomizedWorkflows
{
    //default Hotkey for stopping mouseShiftingMode = escape && pause mouseShiftingMode = control+shift+3 TODO:write this into README
    [InitializeOnLoad]
    public class CalculateTilemapTemplateCenterPosition : MonoBehaviour
    {
        #region Variables, Initialization

        private static readonly string ShiftingDoodadsPrefabPath = "ShiftingAssetsTemplate";
        
        
        //this is to load default values for making a group
        private struct TileMapGroupSetting
        {
            public int AmountOfLayers;
            public int SortingLayer;
            public int StartOrderInLayer;
            public int OrderInLayerIncreasePerLayer;
            public float StartXShiftingSpeedInput;
            public float XShiftingSpeedInputIncreasePerLayer;
            public float StartScale;
            public float ScaleIncreasePerLayer;
            public float StartEmptyCells;
            public float EmptyCellsIncreasePerLayer;
            public Color StartColor;
            public Color ColorIncreasePerLayer;
            public float StartOffset;
            public float OffsetIncreasePerLayer;
        }

        //the settings for background or foreground
        private static readonly TileMapGroupSetting TileMapGroupSettingBackground, TileMapGroupSettingForeground;
        private static readonly List<Tilemap> ColorChangedTileMaps;
        private static readonly List<SpriteRenderer> ChangedTileMapObjects;
        private static MouseShiftingMode _mouseShiftingMode, _mouseShiftingModeBeforePause;
        private static AdditionalMode _additionalMode;
        private static GameObject _additionalModeOriginPoint;
        private static bool _listenToClick;
        private static Vector3 _mouseShiftOffset;
        
        
        static CalculateTilemapTemplateCenterPosition()
        {
            //listen to the activeTileMap if changed call this function
            GridPaintingState.scenePaintTargetChanged += PickAndSetupBrush;
            
            ColorChangedTileMaps = new List<Tilemap>();
            ChangedTileMapObjects = new List<SpriteRenderer>();
            
            TileMapGroupSettingBackground.AmountOfLayers = 6;
            TileMapGroupSettingBackground.SortingLayer = 1;
            TileMapGroupSettingBackground.StartOrderInLayer = 0;
            TileMapGroupSettingBackground.OrderInLayerIncreasePerLayer = -10;
            TileMapGroupSettingBackground.StartXShiftingSpeedInput = 0;
            TileMapGroupSettingBackground.XShiftingSpeedInputIncreasePerLayer = 2.5f;
            TileMapGroupSettingBackground.StartScale = 1;
            TileMapGroupSettingBackground.ScaleIncreasePerLayer = -0.05f;
            TileMapGroupSettingBackground.StartEmptyCells = 4;
            TileMapGroupSettingBackground.EmptyCellsIncreasePerLayer = -0.3f;
            TileMapGroupSettingBackground.StartColor = new Color(0.75f, 0.75f, 0.75f, 1);
            TileMapGroupSettingBackground.ColorIncreasePerLayer = new Color(-0.075f, -0.075f, -0.075f, 0);
            TileMapGroupSettingBackground.StartOffset = 0;
            TileMapGroupSettingBackground.OffsetIncreasePerLayer = 0;

            TileMapGroupSettingForeground.AmountOfLayers = 6;
            TileMapGroupSettingForeground.SortingLayer = 5;
            TileMapGroupSettingForeground.StartOrderInLayer = 0;
            TileMapGroupSettingForeground.OrderInLayerIncreasePerLayer = 10;
            TileMapGroupSettingForeground.StartXShiftingSpeedInput = 0;
            TileMapGroupSettingForeground.XShiftingSpeedInputIncreasePerLayer = -7.5f;
            TileMapGroupSettingForeground.StartScale = 1;
            TileMapGroupSettingForeground.ScaleIncreasePerLayer = 0.5f;
            TileMapGroupSettingForeground.StartEmptyCells = 1;
            TileMapGroupSettingForeground.EmptyCellsIncreasePerLayer = 1f;
            TileMapGroupSettingForeground.StartColor = Color.white;
            TileMapGroupSettingForeground.ColorIncreasePerLayer = new Color(-0.15f, -0.15f, -0.15f, 0);
            TileMapGroupSettingForeground.StartOffset = 0;
            TileMapGroupSettingForeground.OffsetIncreasePerLayer = -1f;

            _additionalMode = AdditionalMode.None;
            _mouseShiftingMode = MouseShiftingMode.None;
            
            SceneView.duringSceneGui += OnScene;
            GlobalKeyEventHandler.OnKeyEvent += HandleKeyPressEvents;
        }

        private static void HandleKeyPressEvents(Event e)
        {
            if (e.keyCode == KeyCode.Escape)
            {
                EscapeInput();
            }
            
            if (!e.type.Equals(EventType.KeyDown))
            {
                return;
            }
            
            if (e.keyCode == KeyCode.W && e.control)
            {
                MouseShiftingModeSwitch(false);
            }
            
            if ((e.keyCode == KeyCode.E || e.keyCode == KeyCode.R) && e.shift)
            {
                float degrees = -90;
                if (e.keyCode == KeyCode.E)
                {
                    degrees = 90;
                }
                SetBrushRotation(degrees);
            }
            
            if (e.keyCode == KeyCode.E && e.control)
            {
                switch (_mouseShiftingMode)
                {
                    case MouseShiftingMode.MouseShiftWithOffset:
                        MouseShiftingModeNewOffset();
                        break;
                    case MouseShiftingMode.MouseShiftWithXLocked:
                        _mouseShiftingMode = MouseShiftingMode.MouseShiftWithYLocked;
                        break;
                    case MouseShiftingMode.MouseShiftWithYLocked:
                        _mouseShiftingMode = MouseShiftingMode.MouseShiftWithXLocked;
                        break;
                    default:
                        MouseShiftingModeNewOffset();
                        break;
                }
            }
            
            if (e.keyCode == KeyCode.T && e.control)
            {
                ChangeOffset();
            }
            
            if (e.keyCode == KeyCode.G && e.control)
            {
                ChangeRotation();
            }
            
            if (e.keyCode == KeyCode.H && e.control)
            {
                ResetRotation();
            }
        }

        //TODO: Implement this instead of yield break every time
        private static IEnumerator WaitForInputWindowReturnString(Action<string> action, string title, string description, string inputText)
        {
            EditorInputDialog inputWindow = EditorInputDialog.Show(title, description, inputText);
            while (!inputWindow.StringWindowReturnValue.Done)
            {
                yield return null;
            }
            if (inputWindow.StringWindowReturnValue.Cancel)
            {
                yield break;
            }
            action( inputWindow.StringWindowReturnValue.ReturnValue);
            yield return action;
        }

        private static void OnScene(SceneView scene)
        {
            if (Application.isPlaying)
            {
                return;
            }
            
            Event e = Event.current;
            Vector3 mousePos = e.mousePosition;
            Vector3 newDynamicPointPosition = Vector3.zero;
            GameObject dynamicReferencePoint = null;
            GameObject offsetReferencePoint;
            Vector3 mouseWorldPosition;
            
            if (e.keyCode == KeyCode.Q && e.control)
            {
                mouseWorldPosition = MouseToWorld(mousePos, scene.camera);
                MoveCenterPointToPosition(mouseWorldPosition);
            }

            //calculate the mouse position inside 3D Space
            mouseWorldPosition = MouseToWorld(mousePos, scene.camera);
            
            switch (_additionalMode)
            {
                case AdditionalMode.None:
                    break;
                case AdditionalMode.ChangeOffset:
                    if (!GetOffsetReferencePoint(out offsetReferencePoint)) return; //TODO: if offset Reference point is deleted, don't create new one
                    SetNewOffsetPosition(mouseWorldPosition, offsetReferencePoint.transform.position);
                    break;
                case AdditionalMode.ChangeRotation:
                    SetNewRotationPosition(mouseWorldPosition);
                    break;
            }
            
            if (_listenToClick)
            {
                if (e.type == EventType.MouseDown)
                {
                    if (_mouseShiftingMode == MouseShiftingMode.WaitForOffsetClick)
                    {
                        SetDynamicOffset();
                    }
                    _listenToClick = false;
                    EndAdditionalMode();
                }
                else
                {
                    HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
                }
            }
            
            switch (_mouseShiftingMode)
            {
                case MouseShiftingMode.None:
                    return;
                case MouseShiftingMode.MouseShiftWithOffset:
                    if (GetDynamicReferencePoint(out dynamicReferencePoint)) return; //TODO: if dynamic Reference point is deleted, don't create new one
                    newDynamicPointPosition = new Vector3(mouseWorldPosition.x - _mouseShiftOffset.x, mouseWorldPosition.y - _mouseShiftOffset.y, dynamicReferencePoint.transform.position.z);
                    break;
                case MouseShiftingMode.MouseShiftWithXLocked:
                    if (GetDynamicReferencePoint(out dynamicReferencePoint)) return; //TODO: if dynamic Reference point is deleted, don't create new one
                    newDynamicPointPosition = new Vector3(dynamicReferencePoint.transform.position.x, mouseWorldPosition.y, 0);
                    break;
                case MouseShiftingMode.MouseShiftWithYLocked:
                    if (GetDynamicReferencePoint(out dynamicReferencePoint)) return; //TODO: if dynamic Reference point is deleted, don't create new one
                    newDynamicPointPosition = new Vector3(mouseWorldPosition.x, dynamicReferencePoint.transform.position.y, 0);
                    break;
                case MouseShiftingMode.WaitForOffsetClick:
                    return;
                case MouseShiftingMode.MouseShiftPaused:
                    return;
            }

            //shift the dynamicReference Point and recalculate all Objects according to that point position
            if (dynamicReferencePoint != null)
            {
                dynamicReferencePoint.transform.position = newDynamicPointPosition;
            }
            ShiftToDynamicReferencePosition(false, dynamicReferencePoint);

            void SetDynamicOffset()
            {
                HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
                
                if (GetDynamicReferencePoint(out dynamicReferencePoint)) return;
                _mouseShiftOffset = mouseWorldPosition - dynamicReferencePoint.transform.position;
                _mouseShiftingMode = MouseShiftingMode.MouseShiftWithOffset;
            }
        }

        private static Vector3 MouseToWorld(Vector3 mousePos, Camera camera)
        {
            mousePos.y = camera.pixelHeight - mousePos.y;
            Vector3 mouseWorldPosition = camera.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 0));
            return Vector3.Scale(mouseWorldPosition, new Vector3(1, 1, 0));
        }

        #endregion
        
        #region Input functions

        private static void SetBrushRotation(float degrees)
        {
            GridBrushBase brushBase = GridPaintingState.gridBrush;

            RandomGameObjectBrush brushClass = brushBase as RandomGameObjectBrush;

            if (brushClass != null && brushClass.hiddenGrid != null)
            {
                Undo.RecordObject(brushClass, "Change Brush Rotation");
                GridPaintingState.gridBrush = brushBase;
                brushClass.RotateCurrentPreviewObject(degrees);
                EditorUtility.SetDirty(brushClass);
                GridBrushEditorBase brushEditorBase = GridPaintingState.activeBrushEditor;
                RandomGameObjectEditor brushClassEditor = brushEditorBase as RandomGameObjectEditor;
                if (brushClassEditor != null)
                {
                    EditorUtility.SetDirty(brushClassEditor);
                    brushClassEditor.OnPaintInspectorGUI();
                }
                else Debug.Log("Error brushClassEditor failed to initialize");
            }
            else
            {
                throw new Exception("Error brushClass failed to initialize");
            }
        }

        private static void MoveCenterPointToPosition(Vector3 position)
        {
            
            if (GetDynamicReferencePoint(out GameObject dynamicReferencePoint)) return;
            dynamicReferencePoint.transform.position = position;
            ShiftToDynamicReferencePosition(false, dynamicReferencePoint);
        }
        
        private static void EscapeInput()
        {
            _listenToClick = false;
            EndAdditionalMode();
            if (_mouseShiftingMode == MouseShiftingMode.None)
            {
                return;
            }
            MouseShiftNone();
        }
        
        private static void MouseShiftingModeSwitch(bool alwaysPause)
        {
            if (_mouseShiftingMode == MouseShiftingMode.None)
            {
                return;
            }
            if (_mouseShiftingMode != MouseShiftingMode.MouseShiftPaused || alwaysPause)
            {
                if(_mouseShiftingModeBeforePause != MouseShiftingMode.MouseShiftPaused) _mouseShiftingModeBeforePause = _mouseShiftingMode;
                _mouseShiftingMode = MouseShiftingMode.MouseShiftPaused;
                return;
            }
            _mouseShiftingMode = _mouseShiftingModeBeforePause;
        }
        
        private static void MouseShiftingModeNewOffset()
        {
            _mouseShiftingMode = MouseShiftingMode.WaitForOffsetClick;
            _listenToClick = true;
        }

        #endregion

        #region Tile Palette Functions

        private static void PickAndSetupBrush(GameObject paintTarget)
        {
            if (paintTarget == null)
            {
                return;
            }
            if (paintTarget.GetComponent<TileMapSelectionInfo>() != null)
            {
                if (String.IsNullOrEmpty(paintTarget.GetComponent<TileMapSelectionInfo>().brush))
                {
                    return;
                }

                GridBrushBase brushBase = null;
                foreach (GridBrushBase usedBrush in GridPaintingState.brushes)
                {
                    if (usedBrush.name == paintTarget.GetComponent<TileMapSelectionInfo>().brush)
                    {
                        brushBase = usedBrush;
                    }
                }

                RandomGameObjectBrush brushClass = brushBase as RandomGameObjectBrush;

                if (brushClass != null && brushClass.hiddenGrid != null)
                {
                    Undo.RecordObject(brushClass, "Change Brush");
                    GridPaintingState.gridBrush = brushBase;
                    brushClass.constrainProportionsToX = true;
                    brushClass.usePrefabScaleAsProportions = true;
                    brushClass.overrideSortingLayer = true;
                    brushClass.sortingLayer = paintTarget.GetComponent<TileMapSelectionInfo>().sortingLayer;
                    brushClass.lowestOrderInLayer = paintTarget.GetComponent<TileMapSelectionInfo>().orderInLayer;
                    brushClass.overrideColors = true;
                    brushClass.newColor = paintTarget.GetComponent<TileMapSelectionInfo>().tileMapColor;

                    //calculate the new maxScale to keep the proportions between min and max
                    if (brushClass.minScale.x <= 0.1f)
                    {
                        //don't use values lower than 0.1 in our case
                        brushClass.minScale.x = 0.1f;
                    }

                    float newMaxScale = paintTarget.GetComponent<TileMapSelectionInfo>().minScale * (brushClass.maxScale.x / brushClass.minScale.x);

                    brushClass.minScale = new Vector3(paintTarget.GetComponent<TileMapSelectionInfo>().minScale,
                        brushClass.minScale.y, brushClass.minScale.z);
                    brushClass.maxScale = new Vector3(newMaxScale,
                        brushClass.maxScale.y, brushClass.maxScale.z);
                    brushClass.drawEmptyCellsAfterEachDraw = paintTarget.GetComponent<TileMapSelectionInfo>().emptyCellsAfterDraw;
                    brushClass.UpdateHiddenGridLayout();
                    EditorUtility.SetDirty(brushClass);
                }
                else
                {
                    throw new Exception("Error brushClass failed to initialize");
                }
            }
        }
        #endregion

        #region CoreMenu Functions
        [MenuItem("Custom Functions/Setup Tilemap Group")]
        static void SetupTilemapGroup()
        {
            CheckCurrentPointReferences();
            TMP_EditorCoroutine.StartCoroutine(TilemapGroupEditingRoutine(false));
        }

        static IEnumerator TilemapGroupEditingRoutine(bool editChildren){
            MouseShiftingModeSwitch(true);

            GameObject[] selectedObjects = Selection.gameObjects;
            if (selectedObjects.Length != 1)
            {
                EditorUtility.DisplayDialog("Error",
                    "Please only select one GameObject in the Hierarchy!", "Ok");
                yield break;
            }
            
            GameObject pointReferencePosition = GameObject.Find("CurrentPointReferences");
            
            GameObject hierarchyParent = selectedObjects[0];
            if (editChildren)
            {
                if (hierarchyParent.GetComponentInChildren<ParallaxTransform>() == null || hierarchyParent.GetComponentInChildren<TileMapSelectionInfo>() == null)
                {
                    EditorUtility.DisplayDialog("Error",
                        "Selected Parent has no valid TilemapGroups!", "Ok");
                    yield break;
                }
            }

            string title = "Fast Construct";
            EditorInputDialog inputWindow;
            TileMapGroupSetting tileMapGroupSetting = TileMapGroupSettingBackground;
            string parentName = "";
            if (!editChildren)
            {
                inputWindow = EditorInputDialog.Show(title, "Fast Construct Multiple Layers" + "\nParent Name",
                    "Background");
                while (!inputWindow.StringWindowReturnValue.Done)
                {
                    yield return null;
                }

                if (inputWindow.StringWindowReturnValue.Cancel)
                {
                    yield break;
                }

                parentName = inputWindow.StringWindowReturnValue.ReturnValue;
                if (!parentName.Equals("Background"))
                {
                    tileMapGroupSetting = TileMapGroupSettingForeground;
                }
            }

            int amountOfLayers;
            if (editChildren)
            {
                amountOfLayers = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>().Length;
                tileMapGroupSetting.SortingLayer = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>()[0].sortingLayer;
                
                tileMapGroupSetting.StartOrderInLayer = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>()[0].orderInLayer;
                if(amountOfLayers > 1) tileMapGroupSetting.OrderInLayerIncreasePerLayer = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>()[1].orderInLayer - tileMapGroupSetting.StartOrderInLayer;
                
                tileMapGroupSetting.StartXShiftingSpeedInput = CalculateCurrentXShiftingSpeed(hierarchyParent.GetComponentsInChildren<ParallaxTransform>()[0]) * 100;
                if (amountOfLayers > 1) tileMapGroupSetting.XShiftingSpeedInputIncreasePerLayer = CalculateCurrentXShiftingSpeed(hierarchyParent.GetComponentsInChildren<ParallaxTransform>()[1]) * 100 - tileMapGroupSetting.StartXShiftingSpeedInput;
                
                tileMapGroupSetting.StartScale = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>()[0].minScale;
                if(amountOfLayers > 1) tileMapGroupSetting.ScaleIncreasePerLayer = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>()[1].minScale - tileMapGroupSetting.StartScale;
                
                tileMapGroupSetting.StartOffset = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>()[0].layerOffset;
                if(amountOfLayers > 1) tileMapGroupSetting.OffsetIncreasePerLayer = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>()[1].layerOffset - tileMapGroupSetting.StartOffset;
                
                tileMapGroupSetting.StartEmptyCells = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>()[0].emptyCellsAfterDraw;
                if(amountOfLayers > 1) tileMapGroupSetting.EmptyCellsIncreasePerLayer = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>()[1].emptyCellsAfterDraw - tileMapGroupSetting.StartEmptyCells;
                
                tileMapGroupSetting.StartColor = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>()[0].tileMapColor;
                if(amountOfLayers > 1) tileMapGroupSetting.ColorIncreasePerLayer = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>()[1].tileMapColor - tileMapGroupSetting.StartColor;
            }
            else
            {
                inputWindow = EditorInputDialog.Show(title, "Amount Of Layers",
                    tileMapGroupSetting.AmountOfLayers.ToString());
                while (!inputWindow.StringWindowReturnValue.Done)
                {
                    yield return null;
                }

                if (inputWindow.StringWindowReturnValue.Cancel)
                {
                    yield break;
                }

                amountOfLayers = int.Parse(inputWindow.StringWindowReturnValue.ReturnValue);
            }

            inputWindow = EditorInputDialog.Show(title, "Sorting Layer", tileMapGroupSetting.SortingLayer.ToString());
            while (!inputWindow.StringWindowReturnValue.Done)
            {
                yield return null;
            }
            if (inputWindow.StringWindowReturnValue.Cancel)
            {
                yield break;
            }
            int sortingLayer = int.Parse(inputWindow.StringWindowReturnValue.ReturnValue);
            
            inputWindow = EditorInputDialog.Show(title, "Start Order in Layer", tileMapGroupSetting.StartOrderInLayer.ToString());
            while (!inputWindow.StringWindowReturnValue.Done)
            {
                yield return null;
            }
            if (inputWindow.StringWindowReturnValue.Cancel)
            {
                yield break;
            }
            int startOrderInLayer = int.Parse(inputWindow.StringWindowReturnValue.ReturnValue);
            
            
            inputWindow = EditorInputDialog.Show(title, "End Order in Layer",(tileMapGroupSetting.StartOrderInLayer + tileMapGroupSetting.OrderInLayerIncreasePerLayer * (amountOfLayers - 1)).ToString());
            while (!inputWindow.StringWindowReturnValue.Done)
            {
                yield return null;
            }
            if (inputWindow.StringWindowReturnValue.Cancel)
            {
                yield break;
            }
            int endOrderInLayer = int.Parse(inputWindow.StringWindowReturnValue.ReturnValue);
            
            inputWindow = EditorInputDialog.Show(title, "Start Parallax Speed", tileMapGroupSetting.StartXShiftingSpeedInput.ToString(CultureInfo.InvariantCulture));
            while (!inputWindow.StringWindowReturnValue.Done)
            {
                yield return null;
            }
            if (inputWindow.StringWindowReturnValue.Cancel)
            {
                yield break;
            }
            string startXShiftingSpeedInput = inputWindow.StringWindowReturnValue.ReturnValue;
            
            
            inputWindow = EditorInputDialog.Show(title, "End Parallax Speed", (tileMapGroupSetting.StartXShiftingSpeedInput + tileMapGroupSetting.XShiftingSpeedInputIncreasePerLayer * (amountOfLayers - 1)).ToString(CultureInfo.InvariantCulture));
            while (!inputWindow.StringWindowReturnValue.Done)
            {
                yield return null;
            }
            if (inputWindow.StringWindowReturnValue.Cancel)
            {
                yield break;
            }
            string endXShiftingSpeedInput = inputWindow.StringWindowReturnValue.ReturnValue;
             
            startXShiftingSpeedInput = startXShiftingSpeedInput.Replace('.', ',');
            endXShiftingSpeedInput = endXShiftingSpeedInput.Replace('.', ',');
            float startXShiftingSpeed = float.Parse(startXShiftingSpeedInput);
            float endXShiftingSpeed = float.Parse(endXShiftingSpeedInput);
            
            inputWindow = EditorInputDialog.Show(title, "Start Scale", tileMapGroupSetting.StartScale.ToString(CultureInfo.InvariantCulture));
            while (!inputWindow.StringWindowReturnValue.Done)
            {
                yield return null;
            }
            if (inputWindow.StringWindowReturnValue.Cancel)
            {
                yield break;
            }
            string startScaleInput = inputWindow.StringWindowReturnValue.ReturnValue;
            
            inputWindow = EditorInputDialog.Show(title, "End Scale",
                (tileMapGroupSetting.StartScale + tileMapGroupSetting.ScaleIncreasePerLayer * (amountOfLayers - 1))
                .ToString(CultureInfo.InvariantCulture));
            while (!inputWindow.StringWindowReturnValue.Done)
            {
                yield return null;
            }
            if (inputWindow.StringWindowReturnValue.Cancel)
            {
                yield break;
            }
            string endScaleInput = inputWindow.StringWindowReturnValue.ReturnValue;
            startScaleInput = startScaleInput.Replace('.', ',');
            endScaleInput = endScaleInput.Replace('.', ',');
            float startScale = float.Parse(startScaleInput);
            float endScale = float.Parse(endScaleInput);
            
            //don't allow a scale of 0 by accident
            if (startScale == 0)
            {
                startScale = 0.1f;
            }

            if (endScale == 0)
            {
                endScale = 0.1f;
            }

            inputWindow = EditorInputDialog.Show(title, "Start Offset",
                tileMapGroupSetting.StartOffset.ToString(CultureInfo.InvariantCulture));
            while (!inputWindow.StringWindowReturnValue.Done)
            {
                yield return null;
            }
            if (inputWindow.StringWindowReturnValue.Cancel)
            {
                yield break;
            }
            string startOffsetInput = inputWindow.StringWindowReturnValue.ReturnValue;
            
            inputWindow = EditorInputDialog.Show(title, "End Offset",
                (tileMapGroupSetting.StartOffset + tileMapGroupSetting.OffsetIncreasePerLayer * (amountOfLayers - 1))
                .ToString(CultureInfo.InvariantCulture));
            while (!inputWindow.StringWindowReturnValue.Done)
            {
                yield return null;
            }
            if (inputWindow.StringWindowReturnValue.Cancel)
            {
                yield break;
            }
            string endOffsetInput = inputWindow.StringWindowReturnValue.ReturnValue;
            startOffsetInput = startOffsetInput.Replace('.', ',');
            endOffsetInput = endOffsetInput.Replace('.', ',');
            float startOffset = float.Parse(startOffsetInput);
            float endOffset = float.Parse(endOffsetInput);
            
            inputWindow = EditorInputDialog.Show(title, "Start Empty Cells per Draw",
                tileMapGroupSetting.StartEmptyCells.ToString(CultureInfo.InvariantCulture));
            while (!inputWindow.StringWindowReturnValue.Done)
            {
                yield return null;
            }
            if (inputWindow.StringWindowReturnValue.Cancel)
            {
                yield break;
            }
            string startEmptyCellsInput = inputWindow.StringWindowReturnValue.ReturnValue;
            
            inputWindow = EditorInputDialog.Show(title, "End Empty Cells per Draw",
                (tileMapGroupSetting.StartEmptyCells + tileMapGroupSetting.EmptyCellsIncreasePerLayer * (amountOfLayers - 1))
                .ToString(CultureInfo.InvariantCulture));
            while (!inputWindow.StringWindowReturnValue.Done)
            {
                yield return null;
            }
            if (inputWindow.StringWindowReturnValue.Cancel)
            {
                yield break;
            }
            string emptyCellsIncreaseInput = inputWindow.StringWindowReturnValue.ReturnValue;
            
            startEmptyCellsInput = startEmptyCellsInput.Replace('.', ',');
            emptyCellsIncreaseInput = emptyCellsIncreaseInput.Replace('.', ',');
            float startEmptyCells = float.Parse(startEmptyCellsInput);
            float emptyCellsIncrease = float.Parse(emptyCellsIncreaseInput);

            EditorColorPickerWindow.ShowColorPicker("Start Color", tileMapGroupSetting.StartColor);
            while (!EditorColorPickerWindow.ColorPickerWindowReturnValue.done)
            {
                yield return null;
            }
            if (EditorColorPickerWindow.ColorPickerWindowReturnValue.cancel)
            {
                yield break;
            }
            Color startColor = EditorColorPickerWindow.ColorPickerWindowReturnValue.returnValue;
            
            Color defaultEndColor = new Color(
                tileMapGroupSetting.StartColor.r + tileMapGroupSetting.ColorIncreasePerLayer.r * (amountOfLayers - 1),
                tileMapGroupSetting.StartColor.g + tileMapGroupSetting.ColorIncreasePerLayer.g * (amountOfLayers - 1),
                tileMapGroupSetting.StartColor.b + tileMapGroupSetting.ColorIncreasePerLayer.b * (amountOfLayers - 1),
                tileMapGroupSetting.StartColor.a);
            EditorColorPickerWindow.ShowColorPicker("End Color", defaultEndColor);
            while (!EditorColorPickerWindow.ColorPickerWindowReturnValue.done)
            {
                yield return null;
            }
            if (EditorColorPickerWindow.ColorPickerWindowReturnValue.cancel)
            {
                yield break;
            }
            Color endColor = EditorColorPickerWindow.ColorPickerWindowReturnValue.returnValue;

            GameObject parent = null;
            if (!editChildren)
            {
                if (string.IsNullOrEmpty(parentName))
                {
                    EditorUtility.DisplayDialog("Error",
                        "No Name is allowed to be null!", "Ok");
                    yield break;
                }

                parent = new GameObject(parentName);
                Undo.RegisterCreatedObjectUndo(parent, "Setup Tilemap Group");
                parent.transform.SetParent(hierarchyParent.transform);
            }

            List<GameObject> tilemaps = new List<GameObject>();
            
            for (int i = 0; i < amountOfLayers; i++)
            {
                GameObject tilemapPrefab;
                if (editChildren)
                {
                    tilemapPrefab = hierarchyParent.GetComponentsInChildren<ParallaxTransform>()[i].gameObject;
                }
                else
                {
                    Object prefab = Resources.Load<GameObject>(ShiftingDoodadsPrefabPath);
                    tilemapPrefab = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
                    Undo.RegisterCreatedObjectUndo(tilemapPrefab, "Setup Tilemap Group");
                    Undo.RecordObject(tilemapPrefab, "Setup Tilemap Group");
                    Undo.SetTransformParent(tilemapPrefab.transform, parent.transform, "Setup Tilemap Group");
                }

                ParallaxTransform parallaxTransform = tilemapPrefab.GetComponent<ParallaxTransform>();

                int orderInLayer;
                if (amountOfLayers - 1 == 0)
                {
                    orderInLayer = startOrderInLayer;
                }
                else
                {
                    orderInLayer = startOrderInLayer +
                                   i * ((endOrderInLayer - startOrderInLayer) / (amountOfLayers - 1));
                }

                string completeTileMapName = "TileMap" + " (SO " + sortingLayer + ") (OIL " + orderInLayer + ")";

                float xShiftingSpeed;
                if (amountOfLayers - 1 == 0)
                {
                    xShiftingSpeed = startXShiftingSpeed;
                }
                else
                {
                    xShiftingSpeed = startXShiftingSpeed +
                                     i * ((endXShiftingSpeed - startXShiftingSpeed) / (amountOfLayers - 1));
                }

                float scale;
                if (amountOfLayers - 1 == 0)
                {
                    scale = startScale;
                }
                else
                {
                    scale = startScale + i * ((endScale - startScale) / (amountOfLayers - 1));
                }
                float offset;
                if (amountOfLayers - 1 == 0)
                {
                    offset = startOffset;
                }
                else
                {
                    offset = startOffset + i * ((endOffset - startOffset) / (amountOfLayers - 1));
                }

                float emptyCellsAfterDraw;
                if (amountOfLayers - 1 == 0)
                {
                    emptyCellsAfterDraw = startEmptyCells;
                }
                else
                {
                    emptyCellsAfterDraw = startEmptyCells +
                                          i * ((emptyCellsIncrease - startEmptyCells) / (amountOfLayers - 1));
                }

                Color tileMapColor;

                if (amountOfLayers - 1 == 0)
                {
                    tileMapColor = startColor;
                }
                else
                {
                    tileMapColor = startColor + i * ((endColor - startColor) / (amountOfLayers - 1));
                }

                GameObject tilemapCopy;
                if(!editChildren){
                    tilemapCopy = Instantiate(parallaxTransform.mainTilemap);
                    Undo.RegisterCreatedObjectUndo(tilemapCopy, "Setup Tilemap Group");
                    tilemapCopy.name = completeTileMapName;

                    tilemapCopy.AddComponent<TileMapSelectionInfo>();
                }
                else
                {
                    tilemapCopy = hierarchyParent.GetComponentsInChildren<TileMapSelectionInfo>()[i].gameObject;
                }

                string dropdownName = "Pick Brush";
                string description = "Pick Brush for\nTileMap OrderInLayer (" + orderInLayer + ")";
                Undo.RecordObject(tilemapCopy.GetComponent<TileMapSelectionInfo>(), "Setup Tilemap Group");

                tilemapCopy.GetComponent<TilemapRenderer>().sortingLayerID = SortingLayer.layers[sortingLayer].id;
                tilemapCopy.GetComponent<TilemapRenderer>().sortingOrder = orderInLayer;
                Undo.RecordObject(tilemapCopy.GetComponent<TilemapRenderer>(), "Setup Tilemap Group");

                if (editChildren)
                {
                    PickBrushEditorWindow.ShowDropdown(dropdownName, description, tilemapCopy.GetComponent<TileMapSelectionInfo>().brush);
                }
                else
                {
                    PickBrushEditorWindow.ShowDropdown(dropdownName, description);
                }

                while (!PickBrushEditorWindow.BrushWindowReturnValue.Done)
                {
                    yield return null;
                }

                if (PickBrushEditorWindow.BrushWindowReturnValue.Cancel)
                {
                    for (int j = 0; j < i; j++)
                    {
                        Undo.PerformUndo();
                    }

                    Undo.PerformUndo();
                    yield break;
                }

                string brushType = "";
                if (PickBrushEditorWindow.BrushWindowReturnValue.ReturnValue != null)
                {
                    brushType = PickBrushEditorWindow.BrushWindowReturnValue.ReturnValue.name;
                }


                tilemapCopy.GetComponent<TileMapSelectionInfo>().brush = brushType;
                tilemapCopy.GetComponent<TileMapSelectionInfo>().sortingLayer = sortingLayer;
                tilemapCopy.GetComponent<TileMapSelectionInfo>().orderInLayer = orderInLayer;
                tilemapCopy.GetComponent<TileMapSelectionInfo>().tileMapColor = tileMapColor;
                tilemapCopy.GetComponent<TileMapSelectionInfo>().minScale = scale;
                tilemapCopy.GetComponent<TileMapSelectionInfo>().layerOffset = offset;
                tilemapCopy.GetComponent<TileMapSelectionInfo>().emptyCellsAfterDraw = emptyCellsAfterDraw;
                tilemapCopy.GetComponent<Tilemap>().color = tileMapColor;
                
                tilemaps.Add(tilemapCopy);

                if (!editChildren)
                {
                    GameObject tileMapParent = new GameObject();
                    Undo.RegisterCreatedObjectUndo(tileMapParent, "Setup Tilemap Group");
                    Undo.SetTransformParent(tileMapParent.transform, parallaxTransform.transform.parent,
                        "Setup Tilemap Group");
                    Undo.SetTransformParent(tilemapCopy.transform, tileMapParent.transform, "Setup Tilemap Group");
                    Undo.SetTransformParent(parallaxTransform.transform, tileMapParent.transform,
                        "Setup Tilemap Group");
                    Undo.RecordObject(tilemapCopy, "Setup Tilemap Group");
                    tilemapCopy.SetActive(true);

                    Undo.RecordObject(parallaxTransform, "Setup Tilemap Group");
                    parallaxTransform.mainTilemap = tilemapCopy;
                    EditorUtility.SetDirty(parallaxTransform);
                    Undo.RegisterCompleteObjectUndo(tileMapParent, "Setup Tilemap Group");
                    tileMapParent.name = completeTileMapName + " (Objects)";
                    Undo.RecordObject(parallaxTransform.referencePointsParent,
                        "Setup Tilemap Group");
                }

                SetReferencePoints(parallaxTransform, xShiftingSpeed, pointReferencePosition.transform);
            }

            List<TileMapSelectionInfo> tileMapSelectionInfoRefs = new();
            foreach (GameObject tilemap in tilemaps)
            {
                tileMapSelectionInfoRefs.Add(tilemap.GetComponent<TileMapSelectionInfo>());
            }
            
            foreach (TileMapSelectionInfo tileMapSelectionInfoRef in tileMapSelectionInfoRefs)
            {
                tileMapSelectionInfoRef.GetComponent<TileMapSelectionInfo>().relatedTilemaps = tileMapSelectionInfoRefs;
            }
            
            MakeCenterPoints();
            MouseShiftingModeSwitch(false);
        }

        [MenuItem("Custom Functions/Change Reference Point")]
        static void ChangeReferencePoint()
        {
            CheckCurrentPointReferences();

            GameObject pointReferencePosition = GameObject.Find("CurrentPointReferences");

            GameObject[] parallaxGameObjects = Selection.gameObjects;
            List<ParallaxTransform> parallaxTransforms = new List<ParallaxTransform>();
            foreach (GameObject parallaxGameObject in parallaxGameObjects)
            {
                foreach (ParallaxTransform parallaxTransform in parallaxGameObject
                    .GetComponentsInChildren<ParallaxTransform>())
                {
                    parallaxTransforms.Add(parallaxTransform);
                }
            }

            if (parallaxTransforms.Count == 0)
            {
                EditorUtility.DisplayDialog("Error",
                    "No Selected ParallaxTransform found. Please select a ParallaxTransform or one of its parents!",
                    "Ok");
                return;
            }
            ShowCenterTilemaps(parallaxTransforms.ToArray());

            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                SetReferencePoints(parallaxTransform, pointReferencePosition.transform);
            }
        }

        [MenuItem("Custom Functions/Init New Tile Maps")]
        static void InitNewTileMaps()
        {
            TMP_EditorCoroutine.StartCoroutine(InitNewTileMapsRoutine());
        }

        static IEnumerator InitNewTileMapsRoutine()
        {
            CheckCurrentPointReferences();

            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            List<ParallaxTransform> uninitializedParallaxTransforms = new List<ParallaxTransform>();
            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                if (!parallaxTransform.transform.parent.name.EndsWith(" (Objects)"))
                {
                    uninitializedParallaxTransforms.Add(parallaxTransform);
                }
            }

            if (uninitializedParallaxTransforms.Count == 0)
            {
                EditorUtility.DisplayDialog("Error",
                    "No Uninitialized TileSets found!", "Ok");
            }

            GameObject pointReferencePosition = GameObject.Find("CurrentPointReferences");

            foreach (ParallaxTransform parallaxTransform in uninitializedParallaxTransforms)
            {
                string title = "Fast Construct";
                string description = parallaxTransform.gameObject.name + " Parent: " +
                               parallaxTransform.transform.parent.gameObject.name + ":\n";
                EditorInputDialog inputWindow;
                
                inputWindow = EditorInputDialog.Show(title, description + "Name of the TileMap", "TileMap Pos(0)");
                //unfortunately we will have to wait for the return value every time zzz...
                while (!inputWindow.StringWindowReturnValue.Done)
                {
                    yield return null;
                }
                if (inputWindow.StringWindowReturnValue.Cancel)
                {
                    yield break;
                }
                string tileMapName = inputWindow.StringWindowReturnValue.ReturnValue;
                
                inputWindow = EditorInputDialog.Show(title, description + "Sorting Layer", "1");
                while (!inputWindow.StringWindowReturnValue.Done)
                {
                    yield return null;
                }
                if (inputWindow.StringWindowReturnValue.Cancel)
                {
                    yield break;
                }
                string sortingOrder = inputWindow.StringWindowReturnValue.ReturnValue;
                
                inputWindow = EditorInputDialog.Show(title, description + "Order in Layer", "0");
                while (!inputWindow.StringWindowReturnValue.Done)
                {
                    yield return null;
                }
                if (inputWindow.StringWindowReturnValue.Cancel)
                {
                    yield break;
                }
                
                string orderInLayer = inputWindow.StringWindowReturnValue.ReturnValue;
                
                inputWindow = EditorInputDialog.Show(title, description + "Parallax Speed", "0");
                while (!inputWindow.StringWindowReturnValue.Done)
                {
                    yield return null;
                }
                if (inputWindow.StringWindowReturnValue.Cancel)
                {
                    yield break;
                }
                string xShiftingSpeed = inputWindow.StringWindowReturnValue.ReturnValue;
                
                xShiftingSpeed = xShiftingSpeed.Replace('.', ',');

                if (string.IsNullOrEmpty(tileMapName) || string.IsNullOrEmpty(sortingOrder) ||
                    string.IsNullOrEmpty(orderInLayer))
                {
                    EditorUtility.DisplayDialog("Error",
                        "No Name is allowed to be null!", "Ok");
                    yield break;
                }

                GameObject tilemapCopy = Instantiate(parallaxTransform.mainTilemap);
                Undo.RegisterCreatedObjectUndo(tilemapCopy, "Created tilemapCopy!");

                GameObject tileMapParent = new GameObject();
                Undo.RegisterCreatedObjectUndo(tileMapParent, "Created tileMapParent!");
                Undo.SetTransformParent(tileMapParent.transform, parallaxTransform.transform.parent, "Set new parent");
                Undo.SetTransformParent(tilemapCopy.transform, tileMapParent.transform, "Set new parent");
                Undo.SetTransformParent(parallaxTransform.transform, tileMapParent.transform, "Set new parent");
                Undo.RecordObject(tilemapCopy, "Set tilemapCopy to true");
                tilemapCopy.SetActive(true);
                
                tilemapCopy.GetComponent<TilemapRenderer>().sortingLayerID = SortingLayer.layers[int.Parse(sortingOrder)].id;
                tilemapCopy.GetComponent<TilemapRenderer>().sortingOrder = int.Parse(orderInLayer);
                Undo.RecordObject(tilemapCopy.GetComponent<TilemapRenderer>(), "Setup Tilemap Group");

                Undo.RecordObject(parallaxTransform, "Set tilemapCopy into parallaxTransform mainTilemap field");
                parallaxTransform.mainTilemap = tilemapCopy;
                EditorUtility.SetDirty(parallaxTransform);
                string completeTileMapName = tileMapName + " (SO " + sortingOrder + ") (OIL " + orderInLayer + ")";
                Undo.RegisterCompleteObjectUndo(tilemapCopy, "Set tilemapCopy name");
                tilemapCopy.name = completeTileMapName;
                Undo.RegisterCompleteObjectUndo(tileMapParent, "Set tileMapParent name");
                tileMapParent.name = completeTileMapName + " (Objects)";
                Undo.RecordObject(parallaxTransform.referencePointsParent,
                    "Set referencePointsParent to CurrentPointReferences");

                SetReferencePoints(parallaxTransform, float.Parse(xShiftingSpeed), pointReferencePosition.transform);
                MakeCenterPoints();
            }
        }

        static void MakeCenterPoints()
        {
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                //check if object has already been set up
                if (!parallaxTransform.transform.parent.name.EndsWith(" (Objects)"))
                {
                    continue;
                }

                //create center point
                Vector3 position = parallaxTransform.leftReferencePoint.position;
                Vector3 position1 = parallaxTransform.rightReferencePoint.position;
                float pointsXDistance = position1.x -
                                        position.x;
                float centerXPosition = position.x + pointsXDistance * 0.5f;
                Vector3 centerPointPosition = new Vector3(centerXPosition,
                    position1.y, position1.z);
                GameObject centerPoint;
                if (parallaxTransform.centerReferencePoint == null)
                {
                    centerPoint = new GameObject("CenterPoint");
                    Undo.RegisterCreatedObjectUndo(centerPoint, "MakeCenterPoints");

                    Undo.RecordObject(parallaxTransform, "MakeCenterPoints");
                    parallaxTransform.centerReferencePoint = centerPoint.transform;
                    EditorUtility.SetDirty(parallaxTransform);
                }
                else
                {
                    centerPoint = parallaxTransform.centerReferencePoint.gameObject;
                }

                GUIContent iconContent = EditorGUIUtility.IconContent("sv_icon_dot3_pix16_gizmo");
                EditorGUIUtility.SetIconForObject(centerPoint, (Texture2D)iconContent.image);

                Undo.RecordObject(centerPoint.transform, "MakeCenterPoints");
                centerPoint.transform.position = centerPointPosition;
                Undo.SetTransformParent(centerPoint.transform, parallaxTransform.referencePointsParent,
                    "MakeCenterPoints");
                centerPoint.SetActive(false);


                //setup the player right reference point
                Vector3 position2 = parallaxTransform.leftPlayerReferencePoint.position;
                float playerPointsXDistance = parallaxTransform.centerReferencePoint.position.x -
                                              position2.x;
                float rightPlayerXPosition =
                    position2.x + playerPointsXDistance * 2f;
                Vector3 rightPlayerPointPosition = new Vector3(rightPlayerXPosition,
                    parallaxTransform.lastCurrentPointReferences.position.y,
                    position2.z);
                GameObject rightPlayerPoint;
                if (parallaxTransform.rightPlayerReferencePoint == null)
                {
                    rightPlayerPoint = new GameObject("RightPlayerReferencePoint");
                    Undo.RegisterCreatedObjectUndo(rightPlayerPoint, "MakeCenterPoints");

                    Undo.RecordObject(parallaxTransform, "MakeCenterPoints");
                    parallaxTransform.rightPlayerReferencePoint = rightPlayerPoint.transform;
                    EditorUtility.SetDirty(parallaxTransform);
                }
                else
                {
                    rightPlayerPoint = parallaxTransform.rightPlayerReferencePoint.gameObject;
                }

                GUIContent rightPlayerIconContent = EditorGUIUtility.IconContent("sv_label_3");
                EditorGUIUtility.SetIconForObject(rightPlayerPoint, (Texture2D)rightPlayerIconContent.image);
                Undo.RecordObject(rightPlayerPoint, "MakeCenterPoints");
                rightPlayerPoint.transform.position = rightPlayerPointPosition;
                Undo.SetTransformParent(rightPlayerPoint.transform, parallaxTransform.lastCurrentPointReferences,
                    "MakeCenterPoints");
            }
        }

        private static void CheckCurrentPointReferences()
        {
            GameObject newPointReferencePosition = GameObject.Find("CurrentPointReferences");
            if (newPointReferencePosition == null)
            {
                Debug.Log("Did not find 'CurrentPointReferences' GameObject. Creating new one....");
                
                GameObject currentPointReferences = new GameObject("CurrentPointReferences");
                Undo.RegisterCreatedObjectUndo(currentPointReferences, "Created tilemapCopy!");
                if (Selection.gameObjects.Length == 1)
                {
                    currentPointReferences.transform.SetParent(Selection.gameObjects[0].transform);
                }
                else
                {
                    currentPointReferences.transform.SetParent(GameObject.Find("Grid").transform);
                }
                GUIContent gameObjectIconContent1 = EditorGUIUtility.IconContent("sv_icon_dot0_pix16_gizmo");
                EditorGUIUtility.SetIconForObject(currentPointReferences, (Texture2D)gameObjectIconContent1.image);

                GameObject description = new GameObject("Current Point References");
                description.transform.SetParent(currentPointReferences.transform);
                GUIContent gameObjectIconContent2 = EditorGUIUtility.IconContent("sv_label_0");
                EditorGUIUtility.SetIconForObject(description, (Texture2D)gameObjectIconContent2.image);
                Vector3 localPosition = description.transform.localPosition;
                localPosition = new Vector3(localPosition.x, localPosition.y - 0.84f, localPosition.z);
                description.transform.localPosition = localPosition;
                Object[] selection = new Object[1];
                selection[0] = currentPointReferences;
                Selection.objects = selection;
            }
        }

        [MenuItem("Custom Functions/Change Selected ParallaxTransforms Speed")]
        static void ChangeSelectedPointDistance()
        {
            TMP_EditorCoroutine.StartCoroutine(ChangeSelectedPointDistanceRoutine());
        }

        static IEnumerator ChangeSelectedPointDistanceRoutine()
        {
            GameObject[] parallaxGameObjects = Selection.gameObjects;
            List<ParallaxTransform> parallaxTransforms = new List<ParallaxTransform>();
            foreach (GameObject parallaxGameObject in parallaxGameObjects)
            {
                foreach (ParallaxTransform parallaxTransform in parallaxGameObject
                    .GetComponentsInChildren<ParallaxTransform>())
                {
                    parallaxTransforms.Add(parallaxTransform);
                }
            }

            if (parallaxTransforms.Count == 0)
            {
                EditorUtility.DisplayDialog("Error",
                    "No Selected ParallaxTransform found. Please select a ParallaxTransform or one of its parents!",
                    "Ok");
                yield break;
            }
            
            ShowCenterTilemaps(parallaxTransforms.ToArray());

            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                float currentParallaxSpeed = CalculateCurrentXShiftingSpeed(parallaxTransform) * 100;

                string title = "Change Distance";
                string description = parallaxTransform.transform.parent.gameObject.name + ":\n";
                EditorInputDialog inputWindow = EditorInputDialog.Show(title, description + "New Parallax Speed", currentParallaxSpeed.ToString(CultureInfo.InvariantCulture));
                while (!inputWindow.StringWindowReturnValue.Done)
                {
                    yield return null;
                }
                if (inputWindow.StringWindowReturnValue.Cancel)
                {
                    yield break;
                }
                string xShiftingSpeed = inputWindow.StringWindowReturnValue.ReturnValue;
                
                xShiftingSpeed = xShiftingSpeed.Replace('.', ',');
                SetReferencePoints(parallaxTransform, float.Parse(xShiftingSpeed),
                    parallaxTransform.lastCurrentPointReferences);
            }
        }
        
        private static void SetReferencePoints(ParallaxTransform parallaxTransform, float xShiftingSpeed, Transform pointReferencePosition)
        {
            Vector3 position = pointReferencePosition.position;
            position = new (position.x, position.y, 0);
            pointReferencePosition.position = position;
            Undo.RecordObject(parallaxTransform, "Set referencePoint Distance");
            parallaxTransform.mainTileMapLocalPosition = parallaxTransform.mainTilemap.transform.localPosition;
            parallaxTransform.lastCurrentPointReferences.position = position;
            EditorUtility.SetDirty(parallaxTransform);
            Vector3 position1 = parallaxTransform.referencePointsParent.position;
            float playerReferenceDistance = position1.x -
                                            parallaxTransform.leftPlayerReferencePoint.position.x;

            //scaling down the shifting speed value to have more user friendly Input
            
            Undo.RecordObject(parallaxTransform.referencePointsParent, "Set referencePoint Distance");
            position1 = position;
            
            float scaledShiftingSpeed = 0.01f * xShiftingSpeed;

            //calculate the shifting Distance
            float pointShift = scaledShiftingSpeed * playerReferenceDistance;

            Undo.RecordObject(parallaxTransform, "Set referencePoint Distance");
            parallaxTransform.parallaxSpeed = xShiftingSpeed;

            //shift the points pointDistance/2 to the left and right
            Undo.RecordObject(parallaxTransform.rightReferencePoint, "Set referencePoint Distance");
            parallaxTransform.rightReferencePoint.position = new Vector3(
                position1.x + pointShift,
                position1.y, position1.z);
            Undo.RecordObject(parallaxTransform.leftReferencePoint, "Set referencePoint Distance");
            parallaxTransform.leftReferencePoint.position = new Vector3(
                position1.x - pointShift,
                position1.y, position1.z);
            Undo.RecordObject(parallaxTransform.referencePointsParent, "Set referencePoint Distance");
            position1 = new Vector3(
                position1.x,
                position1.y + pointShift / 2,
                position1.z);
            parallaxTransform.referencePointsParent.position = position1;

            ChangeTopPosition(parallaxTransform, scaledShiftingSpeed);
        }

        private static void SetReferencePoints(ParallaxTransform parallaxTransform, Transform pointReferencePosition)
        {
            Vector3 position = pointReferencePosition.position;
            position = new Vector3(position.x, position.y, 0);
            pointReferencePosition.position = position;
            Undo.RecordObject(parallaxTransform, "Set referencePoint Distance");
            parallaxTransform.mainTileMapLocalPosition = parallaxTransform.mainTilemap.transform.localPosition;
            parallaxTransform.lastCurrentPointReferences.position = position;
            EditorUtility.SetDirty(parallaxTransform);

            float xShiftingSpeed = CalculateCurrentXShiftingSpeed(parallaxTransform);
            
            float scaledShiftingSpeed = xShiftingSpeed;
            ChangeTopPosition(parallaxTransform, scaledShiftingSpeed);
        }

        private static float CalculateCurrentXShiftingSpeed(ParallaxTransform parallaxTransform)
        {
            float pointsDistance = parallaxTransform.rightReferencePoint.position.x - parallaxTransform.leftReferencePoint.position.x;
            float playerReferenceDistance = parallaxTransform.centerReferencePoint.position.x - parallaxTransform.leftPlayerReferencePoint.position.x;
            float xShiftingSpeed = (0.5f * pointsDistance)/playerReferenceDistance;
            return xShiftingSpeed;
        }
        
        private static void ChangeTopPosition(ParallaxTransform parallaxTransform, float scaledShiftingSpeed)
        {
            Vector3 position = parallaxTransform.lastCurrentPointReferences.position;
            float topDistance = parallaxTransform.topPlayerReferencePoint.position.y - position.y;
            float topReferencePointPositionY = position.y + parallaxTransform.yShiftingSpeedMod * scaledShiftingSpeed * topDistance;
            Vector3 topReferencePointPosition = new Vector3(position.x,
                topReferencePointPositionY, position.z);
            GameObject topReferencePoint;
            if (parallaxTransform.topReferencePoint == null)
            {
                topReferencePoint = new GameObject("TopReferencePoint");
                Undo.RegisterCreatedObjectUndo(topReferencePoint, "MakeCenterPoints");

                Undo.RecordObject(parallaxTransform, "MakeCenterPoints");
                parallaxTransform.topReferencePoint = topReferencePoint.transform;
                EditorUtility.SetDirty(parallaxTransform);
            }
            else
            {
                topReferencePoint = parallaxTransform.topReferencePoint.gameObject;
            }

            GUIContent topReferencePointIconContent = EditorGUIUtility.IconContent("sv_icon_dot4_pix16_gizmo");
            EditorGUIUtility.SetIconForObject(topReferencePoint, (Texture2D)topReferencePointIconContent.image);
            Undo.RecordObject(topReferencePoint, "MakeCenterPoints");
            topReferencePoint.transform.position = topReferencePointPosition;
            Undo.SetTransformParent(topReferencePoint.transform, parallaxTransform.lastCurrentPointReferences,
                "MakeCenterPoints");
        }
        #endregion

        #region Show Information
        [MenuItem("Custom Functions/Show Information/Show all ParallaxTransform Information")]
        static void ShowAllParallaxTransformInformation()
        {
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            ChangeParallaxTransformInformation(true, parallaxTransforms, "Show all ParallaxTransform Information");
        }

        [MenuItem("Custom Functions/Show Information/Hide all ParallaxTransform Information")]
        static void HideAllParallaxTransformInformation()
        {
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            ChangeParallaxTransformInformation(false, parallaxTransforms, "Hide all ParallaxTransform Information");
        }

        static void ChangeParallaxTransformInformation(bool activeState, ParallaxTransform[] parallaxTransforms,
            string undoName)
        {
            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                SetTransform(parallaxTransform.lastCurrentPointReferences);
            }

            void SetTransform(Transform changedTransform)
            {
                Undo.RecordObject(changedTransform, undoName);
                changedTransform.gameObject.SetActive(activeState);
            }
        }
        
        [MenuItem("Custom Functions/Show Information/Show Only Selected ParallaxTransform Information")]
        static void ShowOnlySelectedParallaxTransformInformation()
        {
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            ChangeParallaxTransformInformation(false, parallaxTransforms, "Show Only Selected Parallax TileMap Group");

            GameObject[] parallaxGameObjects = Selection.gameObjects;
            List<ParallaxTransform> selectedParallaxTransforms = new List<ParallaxTransform>();
            foreach (GameObject parallaxGameObject in parallaxGameObjects)
            {
                foreach (ParallaxTransform parallaxTransform in parallaxGameObject
                    .GetComponentsInChildren<ParallaxTransform>())
                {
                    selectedParallaxTransforms.Add(parallaxTransform);
                }
            }

            ChangeParallaxTransformInformation(true, selectedParallaxTransforms.ToArray(),
                "Show Only Selected Parallax TileMap Group");
        }

        #endregion

        #region Show Tilemaps
        [MenuItem("Custom Functions/Show Tilemaps/Show All ParallaxTransform Tilemaps")]
        static void ShowAllTilemaps()
        {
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                Undo.RecordObject(parallaxTransform.mainTilemap, "Show All ParallaxTransform Tilemaps");
                parallaxTransform.mainTilemap.SetActive(true);
            }
        }

        [MenuItem("Custom Functions/Show Tilemaps/Hide All ParallaxTransform Tilemaps")]
        static void HideAllTilemaps()
        {
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                Undo.RecordObject(parallaxTransform.mainTilemap, "Change ParallaxTransform TileMap Group Visibility");
                parallaxTransform.mainTilemap.SetActive(false);
            }
        }
        
        [MenuItem("Custom Functions/Show Tilemaps/Show Only Selected ParallaxTransform TileMap Group")]
        static void ShowOnlySelectedParallaxTileMapGroup()
        {
            HideAllTilemaps();

            GameObject[] parallaxGameObjects = Selection.gameObjects;
            List<ParallaxTransform> selectedParallaxTransforms = new List<ParallaxTransform>();
            foreach (GameObject parallaxGameObject in parallaxGameObjects)
            {
                foreach (ParallaxTransform parallaxTransform in parallaxGameObject
                    .GetComponentsInChildren<ParallaxTransform>())
                {
                    selectedParallaxTransforms.Add(parallaxTransform);
                }
            }

            foreach (ParallaxTransform parallaxTransform in selectedParallaxTransforms)
            {
                Undo.RecordObject(parallaxTransform.mainTilemap, "Show Only Selected ParallaxTransform TileMap Group");
                parallaxTransform.mainTilemap.SetActive(true);
            }
        }


        [MenuItem("Custom Functions/Show Tilemaps/Change other TileMaps Alpha")]
        static void OnlyShowThisTileMap()
        {
            TMP_EditorCoroutine.StartCoroutine(OnlyShowThisTileMapRoutine());
        }
        
        static IEnumerator OnlyShowThisTileMapRoutine()
        {
            //if old TileMap changes have not been revered abort!
            if (ColorChangedTileMaps.Count != 0)
            {
                ReverseOnlyShowThisTileMap();
            }

            float startValue = 0;
            EditorGUILayoutSlider.ShowSlider("Change other TileMaps Alpha", startValue);
            while (!EditorGUILayoutSlider.ColorSliderWindowReturnValue.Done)
            {
                yield return null;
            }
            if (EditorGUILayoutSlider.ColorSliderWindowReturnValue.Cancel)
            {
                yield break;
            }
            float newTileMapAlpha = EditorGUILayoutSlider.ColorSliderWindowReturnValue.ReturnValue;
            
            foreach (Tilemap tileMap in FindObjectsOfType<Tilemap>(false))
            {
                if (tileMap.color.a == 1)
                {
                    ColorChangedTileMaps.Add(tileMap);
                }
            }
            
            GameObject[] selectedGameObjects = Selection.gameObjects;
            foreach (GameObject selectedGameObject in selectedGameObjects)
            {
                foreach (Tilemap tileMap in selectedGameObject.GetComponentsInChildren<Tilemap>())
                {
                    ColorChangedTileMaps.Remove(tileMap);
                }
            }

            foreach (Tilemap tileMap in ColorChangedTileMaps)
            {
                Undo.RecordObject(tileMap, "Only Show This TileMap");
                Color color = tileMap.color;
                color = new Color(color.r, color.g, color.g, newTileMapAlpha);
                tileMap.color = color;

                foreach (SpriteRenderer spriteRenderer in tileMap.GetComponentsInChildren<SpriteRenderer>())
                {
                    if (spriteRenderer.color.a == 1)
                    {
                        Undo.RecordObject(spriteRenderer, "Only Show This TileMap");
                        Color color1 = spriteRenderer.color;
                        color1 = new Color(color1.r, color1.g, color1.g, newTileMapAlpha);
                        spriteRenderer.color = color1;
                        ChangedTileMapObjects.Add(spriteRenderer);
                    }
                }
            }
        }
        
        
        [MenuItem("Custom Functions/Show Tilemaps/Reverse Only Show This TileMap")]
        static void ReverseOnlyShowThisTileMap()
        {
            foreach (Tilemap tileMap in ColorChangedTileMaps)
            {
                Undo.RecordObject(tileMap, "Reverse Only Show This TileMap!");
                Color color = tileMap.color;
                color = new Color(color.r, color.g, color.g, 1);
                tileMap.color = color;
            }
            foreach (SpriteRenderer spriteRenderer in ChangedTileMapObjects)
            {
                Undo.RecordObject(spriteRenderer, "Reverse Only Show This TileMap");
                Color color = spriteRenderer.color;
                color = new Color(color.r, color.g, color.g, 1);
                spriteRenderer.color = color;
            }
            ColorChangedTileMaps.Clear();
            ChangedTileMapObjects.Clear();
        }
        #endregion

        #region Shift TileMaps
        static void ShiftMainTilemap(Transform mainTilemap, Transform centerReferencePoint, Vector3 targetPointPosition,
            Vector3 defaultPosition, bool record)
        {
            mainTilemap.transform.localPosition = defaultPosition;
            if (record)
            {
                Undo.RecordObject(mainTilemap, "Show Left Position");
            }

            Transform transform1 = mainTilemap.transform;
            Vector3 position = transform1.position;
            Vector3 position1 = centerReferencePoint.position;
            Vector3 newPosition = new Vector3(
                position.x + (targetPointPosition.x - position1.x),
                position.y + (targetPointPosition.y - position1.y),
                position.z + (targetPointPosition.z - position1.z));
            position = newPosition;
            transform1.position = position;
        }

        [MenuItem("Custom Functions/Show Position/Shift To Dynamic Reference Position")]
        private static void ShiftToDynamicReferencePositionMenuCall()
        {
            if (GetDynamicReferencePoint(out GameObject dynamicReferencePoint)) return;
            ShiftToDynamicReferencePosition(true, dynamicReferencePoint);
        }
        
        
        private static void ShiftToDynamicReferencePosition(bool record, GameObject dynamicReferencePoint)
        {
            
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                Vector3 currentReferencePos = parallaxTransform.lastCurrentPointReferences.position;

                float xShiftingSpeed = CalculateCurrentXShiftingSpeed(parallaxTransform);
                float yShiftingSpeed = xShiftingSpeed * parallaxTransform.yShiftingSpeedMod;
                Vector3 position = dynamicReferencePoint.transform.position;
                float sideDistance = position.x - currentReferencePos.x;
                float topDistance = position.y - currentReferencePos.y;
                float newXPosition = currentReferencePos.x + xShiftingSpeed * sideDistance;
                float newYPosition = currentReferencePos.y + parallaxTransform.yShiftingSpeedMod * yShiftingSpeed * topDistance;
                
                Vector3 newTileMapPosition = new Vector3(newXPosition,
                    newYPosition, 0);
                
                ShiftMainTilemap(parallaxTransform.mainTilemap.transform, parallaxTransform.lastCurrentPointReferences,
                    newTileMapPosition, parallaxTransform.mainTileMapLocalPosition, record);
            }
        }

        [MenuItem("Custom Functions/Show Position/Show Left Position")]
        static void ShowLeftTilemaps()
        {
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                ShiftMainTilemap(parallaxTransform.mainTilemap.transform, parallaxTransform.centerReferencePoint,
                    parallaxTransform.leftReferencePoint.position, parallaxTransform.mainTileMapLocalPosition, true);
            }
        }

        [MenuItem("Custom Functions/Show Position/Show Center ParallaxTransform")]
        static void ShowCenterTilemaps()
        {
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            ShowCenterTilemaps(parallaxTransforms);
        }
        
        static void ShowCenterTilemaps(ParallaxTransform[] parallaxTransforms)
        {
            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                ShiftMainTilemap(parallaxTransform.mainTilemap.transform, parallaxTransform.centerReferencePoint,
                    parallaxTransform.centerReferencePoint.position, parallaxTransform.mainTileMapLocalPosition, true);
            }
        }

        [MenuItem("Custom Functions/Show Position/Show Right ParallaxTransform")]
        static void ShowRightTilemaps()
        {
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                ShiftMainTilemap(parallaxTransform.mainTilemap.transform, parallaxTransform.centerReferencePoint,
                    parallaxTransform.rightReferencePoint.position, parallaxTransform.mainTileMapLocalPosition, true);
            }
        }
        
        [MenuItem("Custom Functions/Show Position/Show Top ParallaxTransform")]
        static void ShowTopTilemaps()
        {
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                ShiftMainTilemap(parallaxTransform.mainTilemap.transform, parallaxTransform.lastCurrentPointReferences,
                    parallaxTransform.topReferencePoint.position, parallaxTransform.mainTileMapLocalPosition, true);
            }
        }
        
        [MenuItem("Custom Functions/Show Position/Show TopRight ParallaxTransform")]
        static void ShowTopRightTilemaps()
        {
            
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                Vector3 position = parallaxTransform.topReferencePoint.position;
                Vector3 newReferencePoint = new Vector3(parallaxTransform.rightReferencePoint.position.x, position.y, position.z);
                ShiftMainTilemap(parallaxTransform.mainTilemap.transform, parallaxTransform.lastCurrentPointReferences,
                    newReferencePoint, parallaxTransform.mainTileMapLocalPosition, true);
            }
        }
        
        [MenuItem("Custom Functions/Show Position/Show TopLeft ParallaxTransform")]
        static void ShowTopLeftTilemaps()
        {
            ParallaxTransform[] parallaxTransforms = FindObjectsOfType<ParallaxTransform>(false);
            foreach (ParallaxTransform parallaxTransform in parallaxTransforms)
            {
                Vector3 position = parallaxTransform.topReferencePoint.position;
                Vector3 newReferencePoint = new Vector3(parallaxTransform.leftReferencePoint.position.x, position.y, position.z);
                ShiftMainTilemap(parallaxTransform.mainTilemap.transform, parallaxTransform.lastCurrentPointReferences,
                    newReferencePoint, parallaxTransform.mainTileMapLocalPosition, true);
            }
        }
        #endregion

        #region Dynamic Mouse Shifting
        
        [MenuItem("Custom Functions/Dynamic Mouse Shifting/Shift with locked y Position")]
        private static void MouseShiftWithYLocked()
        {
            if (GetDynamicReferencePoint(out GameObject _)) return;
            _mouseShiftingMode = MouseShiftingMode.MouseShiftWithYLocked;
        }
        
        [MenuItem("Custom Functions/Dynamic Mouse Shifting/Shift with locked X Position")]
        private static void MouseShiftWithXLocked()
        {
            if (GetDynamicReferencePoint(out GameObject _)) return;
            _mouseShiftingMode = MouseShiftingMode.MouseShiftWithXLocked;
        }
        [MenuItem("Custom Functions/Dynamic Mouse Shifting/Shift with locked Offset")]
        private static void MouseShiftWithOffset()
        {
            if (GetDynamicReferencePoint(out GameObject _)) return;
            _listenToClick = true;
            _mouseShiftingMode = MouseShiftingMode.WaitForOffsetClick;
        }
        [MenuItem("Custom Functions/Dynamic Mouse Shifting/None")]
        private static void MouseShiftNone()
        {
            if (GetDynamicReferencePoint(out GameObject _)) return;
            ShowCenterTilemaps();
            _mouseShiftingMode = MouseShiftingMode.None;
        }

        private static bool GetDynamicReferencePoint(out GameObject dynamicReferencePoint)
        {
            dynamicReferencePoint = GameObject.Find("DynamicReferencePoint");
            if (dynamicReferencePoint == null)
            {
                EditorUtility.DisplayDialog("Error",
                    "Did not find 'DynamicReferencePoint' GameObject. Creating new one. Setting its parent to Grid and Abort...",
                    "Ok");
                dynamicReferencePoint = new GameObject("DynamicReferencePoint");
                Undo.RegisterCreatedObjectUndo(dynamicReferencePoint, "Show Dynamic Reference Position");
                dynamicReferencePoint.transform.SetParent(GameObject.Find("Grid").transform);
                GUIContent gameObjectIconContent1 = EditorGUIUtility.IconContent("sv_icon_dot3_pix16_gizmo");
                EditorGUIUtility.SetIconForObject(dynamicReferencePoint, (Texture2D)gameObjectIconContent1.image);

                GameObject description = new GameObject("Dynamic Reference Point");
                description.transform.SetParent(dynamicReferencePoint.transform);
                GUIContent gameObjectIconContent2 = EditorGUIUtility.IconContent("sv_label_3");
                EditorGUIUtility.SetIconForObject(description, (Texture2D)gameObjectIconContent2.image);
                Vector3 localPosition = description.transform.localPosition;
                localPosition = new Vector3(localPosition.x,
                    localPosition.y - 0.84f, localPosition.z);
                description.transform.localPosition = localPosition;
                Object[] selection = new Object[1];
                selection[0] = dynamicReferencePoint;
                Selection.objects = selection;
                return true;
            }

            return false;
        }

        private enum MouseShiftingMode
        {
            None,
            MouseShiftWithYLocked,
            MouseShiftWithXLocked,
            MouseShiftWithOffset,
            WaitForOffsetClick,
            MouseShiftPaused
        }
        #endregion
        
        #region Additional Functions
        
        [MenuItem("Custom Functions/AdditionalFunctions/Edit TilemapGroup Settings")]
        private static void EditTilemapGroupSettings()
        {
            TMP_EditorCoroutine.StartCoroutine(TilemapGroupEditingRoutine(true));
        }
        
        [MenuItem("Custom Functions/AdditionalFunctions/Change Offset")]
        private static void ChangeOffset()
        {
            if (!GetOffsetReferencePoint(out GameObject _))
            {
                Debug.LogWarning("Offset Editing only supported for custom brush group mode!");
                return;
            }
            AdditionalModeSetup(AdditionalMode.ChangeOffset);
        }
        
        [MenuItem("Custom Functions/AdditionalFunctions/Reset Rotation")]
        private static void ResetRotation()
        {
            if (!GetCurrentBrush())
            {
                Debug.LogWarning("No Custom Brush currently in use!");
                return;
            }
            GetCurrentBrush().ChangePreviewToAngle(0);
        }
        
        [MenuItem("Custom Functions/AdditionalFunctions/Change Rotation")]
        private static void ChangeRotation()
        {
            if (!GetCurrentBrush())
            {
                Debug.LogWarning("No Custom Brush currently in use!");
                return;
            }
            AdditionalModeSetup(AdditionalMode.ChangeRotation);
        }

        private static void AdditionalModeSetup(AdditionalMode newMode)
        {
            RandomGameObjectBrush brushClass = GetCurrentBrush();

            if (brushClass == null)
            {
                Debug.LogWarning("No Custom Brush currently in use!");
                return;
            }
            
            _additionalMode = newMode;
            _listenToClick = true;
            
            //make a point for the currentOffsetOrigin
            if (_additionalModeOriginPoint)
            {
                DestroyImmediate(_additionalModeOriginPoint);
            }
            
            _additionalModeOriginPoint = new GameObject("OriginReferencePoint");
            GUIContent gameObjectIconContent1 = EditorGUIUtility.IconContent("sv_icon_dot7_pix16_gizmo");
            EditorGUIUtility.SetIconForObject(_additionalModeOriginPoint, (Texture2D)gameObjectIconContent1.image);
            _additionalModeOriginPoint.hideFlags = HideFlags.DontSave;

            GameObject description = new GameObject("Origin");
            description.transform.SetParent(_additionalModeOriginPoint.transform);
            GUIContent gameObjectIconContent2 = EditorGUIUtility.IconContent("sv_label_7");
            EditorGUIUtility.SetIconForObject(description, (Texture2D)gameObjectIconContent2.image);
            Vector3 localPosition = description.transform.localPosition;
            localPosition = new Vector3(localPosition.x,
                localPosition.y - 0.84f, localPosition.z);
            description.transform.localPosition = localPosition;
            
            _additionalModeOriginPoint.transform.position = brushClass.mouseWorldPositionRef;
            
            GetCurrentBrush().changeMode = true;
        }
        
        private static bool GetOffsetReferencePoint(out GameObject offsetReferencePoint)
        {
            offsetReferencePoint = null;
            GridBrushBase brushBase = GridPaintingState.gridBrush;

            RandomGameObjectBrush brushClass = brushBase as RandomGameObjectBrush;

            if (brushClass == null || !brushClass.offsetPoint)
            {
                return false;
            }
            
            offsetReferencePoint = brushClass.offsetPoint;
            return true;
        }

        private static RandomGameObjectBrush GetCurrentBrush()
        {
            GridBrushBase brushBase = GridPaintingState.gridBrush;
            RandomGameObjectBrush brushClass = brushBase as RandomGameObjectBrush;
            if (brushClass == null)
            {
                return null;
            }

            return brushClass;
        }
        
        private static void SetNewOffsetPosition(Vector3 mouseWorldPosition, Vector3 offsetReferencePosition)
        {
            //don't record clicks
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            
            //calculates the new rotation according to the origin and new mousePosition
            Vector3 originPosition = _additionalModeOriginPoint.transform.position;

            Vector2 originMouseDir = originPosition - mouseWorldPosition;
            Vector2 originOffsetDir = originPosition - offsetReferencePosition;

            Vector3 newOffsetPosition = CalculateRotatedOffsetPoint( mouseWorldPosition, offsetReferencePosition);
            
            float newOffsetDistance = Vector3.Distance(originPosition, newOffsetPosition);
            
            //the direction will always be positive, however the offset could change from positive to negative or vice versa
            if (Vector2.Angle(originOffsetDir, originMouseDir) > 90)
            {
                newOffsetDistance *= -1;
            }

            if (newOffsetDistance == 0)
            {
                newOffsetDistance = 1;
            }
            
            //set new offset
            GetCurrentBrush().ChangeOffset(newOffsetDistance);
        }
        
        
        private static Vector2 CalculateRotatedOffsetPoint(Vector2 mousePosition, Vector2 offsetPointPosition)
        {
            Vector2 pointA = offsetPointPosition;
            Vector2 pointB = mousePosition;
            Vector2 pointC = _additionalModeOriginPoint.transform.position;
            //We search for Point C in a Triangle. Point P is above point B, where the aim line in default position hits its x Axis

            Vector2 CA = pointA - pointC;
            Vector2 CB = pointB - pointC;
            
            return _additionalModeOriginPoint.transform.position + Vector3.Project(CB, CA);
        }
        
        private static void SetNewRotationPosition(Vector3 mouseWorldPosition)
        {
            //don't record clicks
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            
            //calculates the new rotation according to the origin and new mousePosition
            Vector3 originPosition = _additionalModeOriginPoint.transform.position;
            Vector3 direction = originPosition - mouseWorldPosition;
            
            float angle = Vector3.SignedAngle(Vector3.down, direction, Vector3.back);
            GetCurrentBrush().ChangePreviewToAngle(-angle);
        }

        private static void EndAdditionalMode()
        {
            _additionalMode = AdditionalMode.None;
            _listenToClick = false;
                    
            RandomGameObjectBrush brushClass = GetCurrentBrush();
            if (brushClass == null)
            {
                return;
            }
            brushClass.changeMode = false;
            if (_additionalModeOriginPoint)
            {
                DestroyImmediate(_additionalModeOriginPoint);
            }
        }

        enum AdditionalMode
        {
            ChangeOffset,
            ChangeRotation,
            None
        }
        
        #endregion

        private void OnDisable()
        {
            if (_additionalModeOriginPoint)
            {
                DestroyImmediate(_additionalModeOriginPoint);
            }
        }
    }
}
