using System;
using UnityEditor;
using UnityEngine;

namespace _1BloodyLayne.Editor.CustomizedWorkflows
{
    public class EditorGUILayoutSlider : EditorWindow
    {
        public static ColorSliderWindowReturnValue ColorSliderWindowReturnValue;
    
        private float _sliderValue;
        private Action  _onOk, _onCancel;
        private bool _shouldClose, _initializedPosition;
    
        public static void ShowSlider(string title, float startValue)
        {
            ColorSliderWindowReturnValue = new ColorSliderWindowReturnValue();
            EditorGUILayoutSlider window = CreateInstance<EditorGUILayoutSlider>();
            window._sliderValue = startValue;
            window.titleContent = new GUIContent( title );
            window._onOk += PressOk;
            window._onCancel += PressCancel;
            window.Show();
        
            void PressOk()
            {
                ColorSliderWindowReturnValue.ReturnValue = window._sliderValue;
                ColorSliderWindowReturnValue.Done = true;
            }
            void PressCancel()
            {
                ColorSliderWindowReturnValue.Cancel = true;
                ColorSliderWindowReturnValue.Done = true;
            }
        }

        void OnGUI()
        {
            Event e = Event.current;
            if( e.type == EventType.KeyDown )
            {
                switch( e.keyCode )
                {
                    // Escape pressed
                    case KeyCode.Escape:
                        _shouldClose = true;
                        _onCancel?.Invoke();
                        break;
 
                    // Enter pressed
                    case KeyCode.Return:
                    case KeyCode.KeypadEnter:
                        _onOk?.Invoke();
                        _shouldClose = true;
                        break;
                }
            }
 
            if( _shouldClose ) {  // Close this dialog
                Close();
                //return;
            }
        
            _sliderValue = EditorGUILayout.Slider("New Alpha Values",_sliderValue, 0, 1);

            if (GUILayout.Button("Change!"))
            {
                _onOk?.Invoke();
                _shouldClose = true;
            }
        
            //set the dialog position
            if( !_initializedPosition ) {
                Vector2 windowPos = new Vector2((Screen.width*2), (Screen.height/4));
                position = new Rect( windowPos.x, windowPos.y, position.width, position.height );
                _initializedPosition = true;
            }
        }
    
        private void OnDestroy()
        {
            if (!ColorSliderWindowReturnValue.Done)
            {
                _onCancel?.Invoke();
            }
        }
    }

    public struct ColorSliderWindowReturnValue
    {
        public bool Done;
        public bool Cancel;
        public float ReturnValue;
    }
}