﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class ExitStonesLever : MonoBehaviour
    {
        public GameObject lever, ExitStone1, ExitStone2, ExitStone3, ExitStone4, ExitStone5, ExitStone6;
        bool used;
        AudioSource pullLeverSound;
        Animator leverAnim;

        void Start()
        {
            pullLeverSound = GetComponents<AudioSource>()[0];
            leverAnim = lever.GetComponent<Animator>();
        }

            private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player" && !used)
            {
                leverAnim.SetTrigger("SwitchToActivated");
                used = true;
                //Kettentrommel.GetComponent<Animator>().SetBool("Gitterziehen", true);
                StartCoroutine(StartExitStones());
                pullLeverSound.PlayDelayed(0);
                StartCoroutine(Stop());
            }
        }

        IEnumerator StartExitStones()
        {
            ExitStone6.GetComponent<ExitStone>().raise();
            yield return new WaitForSeconds(0.75f);
            ExitStone5.GetComponent<ExitStone>().raise();
            yield return new WaitForSeconds(0.75f);
            ExitStone4.GetComponent<ExitStone>().raise();
            yield return new WaitForSeconds(0.75f);
            ExitStone3.GetComponent<ExitStone>().raise();
            yield return new WaitForSeconds(0.75f);
            ExitStone2.GetComponent<ExitStone>().raise();
            yield return new WaitForSeconds(0.75f);
            ExitStone1.GetComponent<ExitStone>().raise();
        }

        IEnumerator Stop()
        {
            yield return new WaitForSeconds(0.45f);
            leverAnim.SetTrigger("Activated");
        }
    }
}
