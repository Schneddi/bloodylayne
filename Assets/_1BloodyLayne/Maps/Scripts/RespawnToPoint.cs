﻿using System;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityEngine.SceneManagement;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class RespawnToPoint : MonoBehaviour
    {
        public bool onlyX;
        PlatformerCharacter2D player;
        private void Start()
        {
            player = GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player" && player.GetComponent<PlayerStats>().health > 0)
            {
                if (onlyX)
                {
                    other.transform.position = new Vector3(transform.GetChild(0).position.x, other.transform.position.y, other.transform.position.z);
                }
                else
                {
                    other.transform.position = transform.GetChild(0).position;
                }
            }
            if (other.gameObject.CompareTag("Enemy"))
            {
                if (onlyX)
                {
                    other.transform.position = new Vector3(transform.GetChild(0).position.x, other.transform.position.y, other.transform.position.z);
                }
                else
                {
                    other.transform.position = transform.GetChild(0).position;
                }
            }
        }
    }
}
