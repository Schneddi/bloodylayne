using UnityEngine;

namespace _1BloodyLayne.Maps.Background
{
    public class BackgroundControl : MonoBehaviour
    {
        public GameObject backgroundHighestPositions, backgroundLowestPositions;
    
        // Start is called before the first frame update
        void Awake()
        {
            float totalDistance = backgroundHighestPositions.transform.position.y - backgroundLowestPositions.transform.position.y;
            foreach (BackgroundParallaxEffect parallaxScript in backgroundLowestPositions.GetComponentsInChildren<BackgroundParallaxEffect>())
            {
                GameObject correspondingHighestLayer = null;
                foreach (Transform layer in backgroundHighestPositions.transform)
                {
                    if (parallaxScript.gameObject.name.Equals(layer.gameObject.name))
                    {
                        correspondingHighestLayer = layer.gameObject;
                    }
                }

                if (correspondingHighestLayer == null)
                {
                    Debug.Log("Error: Could not find corresponding highest Position of Background Layer: " + parallaxScript.name);
                    return;
                }

                float adjustedLowerRelativePosition = backgroundLowestPositions.transform.position.y - parallaxScript.GetComponent<Transform>().position.y;
                float adjustedHigherRelativePosition = backgroundHighestPositions.transform.position.y - correspondingHighestLayer.GetComponent<Transform>().position.y;
                float ySpeed = (adjustedLowerRelativePosition - adjustedHigherRelativePosition)/totalDistance;
                parallaxScript.yShiftingSpeed = ySpeed;
                parallaxScript.InitializeBackgroundParallaxEffect();
            }
            
            backgroundLowestPositions.transform.localPosition = Vector3.zero;

            Destroy(backgroundHighestPositions);
        }
    }
}
