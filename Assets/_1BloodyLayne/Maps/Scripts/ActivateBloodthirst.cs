using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityStandardAssets._2D;

public class ActivateBloodthirst : MonoBehaviour
{
    public LvL1Control lvl1Control;
    public bool deactived;
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!deactived)
        {
            lvl1Control.ActivateBloodthirst(false);
        }
    }
}
