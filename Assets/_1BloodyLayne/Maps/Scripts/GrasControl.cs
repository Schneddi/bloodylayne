using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GrasControl : MonoBehaviour
{
    private AudioSource[] wiggleSounds;
    private Animator animator;
    
    // Start is called before the first frame update
    void Start()
    {
        wiggleSounds = GetComponents<AudioSource>();
        animator = GetComponent<Animator>();
    }


    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.name == "Player" || other.CompareTag("Enemy"))
        {
            if (Math.Abs(transform.position.x - 0.125f - other.transform.position.x) > 0.4f)
            {
                playRandomSound();
                if (other.transform.position.x < transform.position.x)
                {
                    animator.SetTrigger("WiggleLeft");
                }
                else
                {
                    animator.SetTrigger("WiggleRight");
                }
            }
        }
    }

    private void playRandomSound()
    {
        int randomSoundIndex = Random.Range(0, wiggleSounds.Length);
        wiggleSounds[randomSoundIndex].PlayDelayed(0);
    }
}
