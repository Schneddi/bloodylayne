﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveObject : MonoBehaviour {
    public GameObject removeThis;

	// Use this for initialization
	void Start () {
        Destroy(removeThis);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
