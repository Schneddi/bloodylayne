using System;
using Units.Enemys;
using UnityEditor;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class SpikyGridControl : MonoBehaviour
{
    private Transform damagerCheck;
    private AudioSource hitSound, killSound;
    private PlatformerCharacter2D larry;

    public float damagerCheckLength, upwardsForce;
    public int attackDamage;
    
    // Start is called before the first frame update
    void Start()
    {
        damagerCheck = transform.Find("DamagerCheck");
        hitSound = GetComponents<AudioSource>()[0];
        killSound = GetComponents<AudioSource>()[1];
        larry = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D raycast = Physics2D.Raycast(damagerCheck.position, Vector2.right, damagerCheckLength, 1 << 8);
        if (!killSound.isPlaying && !hitSound.isPlaying &&  raycast && !larry.dead)
        {
            larry.Hit(attackDamage, DamageTypeToPlayer.ImpaledFeet, new Vector2(0,upwardsForce), raycast.point, gameObject, Guid.NewGuid());
            if (larry.dead)
            {
                killSound.PlayDelayed(0);
            }
            else
            {
                hitSound.PlayDelayed(0);
            }
        }
        RaycastHit2D hit = Physics2D.Raycast(damagerCheck.position, Vector2.right, damagerCheckLength, 1 << 9);
        if (hit)
        {
            if (!hitSound.isPlaying)
            {
                
                System.Guid guid = System.Guid.NewGuid();
                hit.collider.GetComponent<EnemyStats>().PushHit(attackDamage,DamageTypeToEnemy.Impaled, new Vector2(0,upwardsForce), guid, false);
                hitSound.PlayDelayed(0);
            }
        }
    }
}
