﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class LowerCaveEntrance : MonoBehaviour
    {
        public bool lower = false;
        float startY;
        AudioSource lowerSound, lockSound;
        public float distanceTillLock;

        // Use this for initialization
        void Start()
        {
            startY = transform.position.y;
            lowerSound = GetComponents<AudioSource>()[0];
            lockSound = GetComponents<AudioSource>()[1];
        }

        // Update is called once per frame
        void Update()
        {
            if (lower)
            {
                if (!lowerSound.isPlaying)
                {
                    lowerSound.PlayDelayed(0);
                }
                transform.position = new Vector3(transform.position.x, transform.position.y - 0.03f, transform.position.z);
                if (lower && transform.position.y < startY - distanceTillLock)
                {
                    lower = false;
                    StartCoroutine(stopWallLoopSound());
                    lockSound.PlayDelayed(0);
                }
            }
        }

        public void startLower()
        {
            lower = true;
        }

        IEnumerator stopWallLoopSound()//Gibt Schussfrequenz und Animationen an
        {
            yield return new WaitForSeconds(0.35f);
            lowerSound.Stop();
        }
    }
}
