using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Enemys;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class AchievementTigger : MonoBehaviour
{
    public int endingAchievementNumber;
    public GameObject relevantEnemy;
    
    private GameController gc;
    // Start is called before the first frame update
    void Start()
    {
        gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player" && !other.GetComponent<PlatformerCharacter2D>().dead)
        {
            switch (endingAchievementNumber)
            {
                case 18:
                    gc.SetAchievement("FIRST_LEVEL");
                    break;
                case 25:
                    if (relevantEnemy == null)
                    {
                        gc.SetAchievement("ASSASSIN_2");
                    }
                    else if (!relevantEnemy.GetComponent<EnemyStats>().dead)
                    {
                        gc.SetAchievement("ASSASSIN_2");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
