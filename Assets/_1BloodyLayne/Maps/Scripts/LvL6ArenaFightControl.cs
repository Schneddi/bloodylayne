using System;
using System.Collections.Generic;
using Maps.ServantEnding;
using Units.Enemys;
using UnityEngine;
using UnityEngine.Serialization;
using UnityStandardAssets._2D;

namespace Maps.Castle
{
    public class LvL6ArenaFightControl : MonoBehaviour
    {
        [Tooltip("If any of these keys are picked up, start the Arena fight")]
        public MultipleKeysControl[] keys;

        public Plattformbewegung_1 platform;
        public List<HumanDeathSequence> firstSpawnEnemies;
        public List<HumanDeathSequence> secondSpawnEnemies;
        public List<HumanDeathSequence> thirdSpawnEnemies;
        
        private void OnEnable()
        {
            foreach (MultipleKeysControl key in keys)
            {
                key.OnPickedUp += OnKeyPickedUp;
            }
        }

        private void OnKeyPickedUp()
        {
            foreach (MultipleKeysControl key in keys)
            {
                key.OnPickedUp -= OnKeyPickedUp;
            }

            platform.enabled = true;
            platform.GetComponent<AudioSource>().PlayDelayed(0);
            platform.GetComponents<AudioSource>()[1].PlayDelayed(0);
            SpawnEnemy(firstSpawnEnemies[0]);
            SpawnEnemy(secondSpawnEnemies[0]);
            SpawnEnemy(thirdSpawnEnemies[0]);
        }

        private void SpawnEnemy(HumanDeathSequence humanDeathSequence)
        {
            humanDeathSequence.gameObject.SetActive(true);
            humanDeathSequence.DeathEvent += UnitDied;
        }

        private void UnitDied(HumanDeathSequence deadEnemy)
        {
            deadEnemy.DeathEvent -= UnitDied;
            
            foreach (List<HumanDeathSequence> spawnEnemies in new[] { firstSpawnEnemies, secondSpawnEnemies, thirdSpawnEnemies })
            {
                if(spawnEnemies.Count > 0) CheckNextEnemySpawnIn(spawnEnemies);
            }

            void CheckNextEnemySpawnIn(List<HumanDeathSequence> spawnEnemies)
            {
                if (deadEnemy == spawnEnemies[0])
                {
                    spawnEnemies.Remove(deadEnemy);

                    if (spawnEnemies.Count == 0) return;
                    
                    SpawnEnemy(spawnEnemies[0]);
                }
            }
        }

        private void OnDisable()
        {
            foreach (MultipleKeysControl key in keys)
            {
                key.OnPickedUp -= OnKeyPickedUp;
            }
            
            foreach (List<HumanDeathSequence> spawnEnemies in new[] { firstSpawnEnemies, secondSpawnEnemies, thirdSpawnEnemies })
            {
                UnsubscribeUnitDied(spawnEnemies);
            }

            void UnsubscribeUnitDied(List<HumanDeathSequence> spawnEnemies)
            {
                foreach (HumanDeathSequence enemy in spawnEnemies)
                {
                    enemy.DeathEvent -= UnitDied;
                }
            }
        }
    }
}
