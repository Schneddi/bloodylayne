using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudsBackgroundControl : MonoBehaviour
{
    public float cloudSpeed;
    public float minSize = 2.5f, maxSize = 3f;
    
    private GameObject[] clouds = new GameObject[8];
    private bool[] cloudActive = new bool[8];
    private float[] randomMaxSize = new float[8];
    
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 8; i++)
        {
            clouds[i] = transform.GetChild(i).gameObject;
            cloudActive[i] = true;
            float randomSize = Random.Range(0, 3f);
            randomMaxSize[i] = Random.Range(minSize, maxSize);
            clouds[i].GetComponent<RectTransform>().localScale = new Vector3(randomSize, randomSize, randomSize);
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < 8; i++)
        {
            if (cloudActive[i])
            {
                clouds[i].GetComponent<RectTransform>().localScale = new Vector3(clouds[i].GetComponent<RectTransform>().localScale.x + Time.deltaTime * cloudSpeed, clouds[i].GetComponent<RectTransform>().localScale.y + Time.deltaTime * cloudSpeed, clouds[i].GetComponent<RectTransform>().localScale.z + Time.deltaTime * cloudSpeed);
                if (clouds[i].GetComponent<RectTransform>().localScale.x > randomMaxSize[i])
                {
                    clouds[i].GetComponent<RectTransform>().localScale = new Vector3(0, 0, 0);
                    randomMaxSize[i] = Random.Range(minSize, maxSize);
                }
            }
        }
    }
}
