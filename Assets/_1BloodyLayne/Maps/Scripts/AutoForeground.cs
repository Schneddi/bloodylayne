﻿using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Maps.Castle
{
    public class AutoForeground : MonoBehaviour
    {
        Transform player;
        [ReadOnly] public bool playerInBounds;
        bool fadingIn, fadingOut, playerBatForm;
        float fadeSpeed = 0.1f;
        Color fadeColor;
        Tilemap grid;
        CompositeCollider2D compositeCollider;
        public GameObject[] rainObects;
        private Collider2D col;

        // Start is called before the first frame update
        void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
            grid = GetComponent<Tilemap>();
            compositeCollider = GetComponent<CompositeCollider2D>();
            col = GetComponent<CompositeCollider2D>();
            compositeCollider.bounds.Expand(5);
            CheckStartState();
        }
        
        // Update is called once per frame
        void Update()
        {
            if (playerBatForm)
            {
                CheckBatLarry();
            }
            if (fadingIn)
            {
                fadeColor = new Color(grid.color.r, grid.color.g, grid.color.b, 1f);
                grid.color = Color.Lerp(grid.color, fadeColor, fadeSpeed);
                if(rainObects.Length > 0)
                {
                    foreach(GameObject rainObject in rainObects)//rain[2] is the background rain
                    {
                        ParticleSystem.MainModule settings = rainObject.GetComponent<ParticleSystem>().main;
                        settings.startColor = new ParticleSystem.MinMaxGradient(fadeColor);
                    }
                }
                if (grid.color.a > 0.99f)
                {
                    grid.color = new Color(grid.color.r, grid.color.g, grid.color.b, 1f);
                    fadingIn = false;
                    if(rainObects.Length > 0)
                    {
                        foreach(GameObject rainObject in rainObects)//rain[2] is the background rain
                        {
                            ParticleSystem.MainModule settings = rainObject.GetComponent<ParticleSystem>().main;
                            settings.startColor = new ParticleSystem.MinMaxGradient(grid.color);
                        }
                        rainObects[0].transform.parent.GetComponent<AudioSource>().PlayDelayed(0);
                    }
                }
            }
            if (fadingOut)
            {
                fadeColor = new Color(grid.color.r, grid.color.g, grid.color.b, 0f);
                grid.color = Color.Lerp(grid.color, fadeColor, fadeSpeed);
                if (rainObects.Length > 0)
                {
                    foreach (GameObject rainObject in rainObects)//rain[2] is the background rain
                    {
                        ParticleSystem.MainModule settings = rainObject.GetComponent<ParticleSystem>().main;
                        settings.startColor = new ParticleSystem.MinMaxGradient(fadeColor);
                    }
                }
                if (grid.color.a < 0.01f)
                {
                    grid.color = new Color(grid.color.r, grid.color.g, grid.color.b, 0f);
                    fadingOut = false;
                    if (rainObects.Length > 0)
                    {
                        foreach (GameObject rainObject in rainObects) //rain[2] is the background rain
                        {
                            rainObject.GetComponent<ParticleSystem>().Stop();
                        }
                        rainObects[0].transform.parent.GetComponent<AudioSource>().Stop();
                    }
                }
            }
        }
        
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player" || other.name == "PlayerBat")
            {
                SwitchToFadingOut();
            }
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                SwitchToFadingIn();
            }

            if (other.name == "PlayerBat" && col.bounds.Contains(player.transform.position))
            {
                playerBatForm = true;
            }
        }

        private void SwitchToFadingIn()
        {
            if (grid.color.a != 1f)
            {
                playerInBounds = false;
                fadingIn = true;
                fadingOut = false;
                foreach(GameObject rainObject in rainObects)
                {
                    rainObject.GetComponent<ParticleSystem>().Play();
                }
            }
        }
        
        private void SwitchToFadingOut()
        {
            if (grid.color.a != 0f)
            {
                playerInBounds = true;
                fadingIn = false;
                fadingOut = true;
            }
        }

        private void CheckBatLarry()
        {
            if (!col.bounds.Contains(player.transform.position))
            {
                playerBatForm = false;
                SwitchToFadingIn();
            }
            if (player.name == "Player")
            {
                playerBatForm = false;
            }
        }

        private void CheckStartState()
        {
            if (col.bounds.Contains(player.transform.position))
            {
                if (grid.color.a == 0f) return;
                
                grid.color = new Color(grid.color.r, grid.color.g, grid.color.b, 0f);
                playerInBounds = true;
            }
            else
            {
                if (grid.color.a == 1f) return;
                
                grid.color = new Color(grid.color.r, grid.color.g, grid.color.b, 1f);
                playerInBounds = false;
            }
        }
    }
}
