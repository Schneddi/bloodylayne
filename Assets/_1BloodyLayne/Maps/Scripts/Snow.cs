using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class Snow : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.GetComponent<PlatformerCharacter2D>() != null)
        {
            other.gameObject.GetComponent<PlatformerCharacter2D>().onIce = true;
        }
    }
    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.GetComponent<PlatformerCharacter2D>() != null)
        {
            other.gameObject.GetComponent<PlatformerCharacter2D>().onIce = false;
        }
    }
}
