﻿using System.Collections;
using System.Collections.Generic;
using UI.PlayerUI;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class CameraFollowChange : MonoBehaviour
    {
        public Transform newTarget;
        public float damping = 0f, timeLimit = 0;
        public bool waitForTrigger = false;
        Transform oldTarget;
        bool skipEnabled = false;
        // Use this for initialization
        void Start()
        {
            if (!waitForTrigger)
            {
                startCameraChange();
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetButtonDown("Continue") && skipEnabled)
            {
                skipEnabled = false;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = oldTarget;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().ResetDamping();
                GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().BlockInput = false;
            }
        }

        IEnumerator waitAndGoBack()
        {
            yield return new WaitForSeconds(timeLimit);
            if (skipEnabled)
            {
                skipEnabled = false;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = oldTarget;
                Camera2DFollow camera2DFollowScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
                for (int i = 30; i > 0; i--)
                {
                    camera2DFollowScript.SetDamping((float)i / 100);
                    yield return new WaitForSeconds(1f / 30f);
                }
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().ResetDamping();
                GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().BlockInput = false;
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player" && waitForTrigger)
            {
                waitForTrigger = false;
                startCameraChange();
            }
        }

        void startCameraChange()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().BlockInput = true;
            oldTarget = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target;
            skipEnabled = true;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = newTarget;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().SetDamping(damping);
            if (timeLimit > 0)
            {
                StartCoroutine(waitAndGoBack());
            }
        }
    }
}