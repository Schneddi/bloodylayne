﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class SpikeStone : MonoBehaviour
    {
        public bool moving;
        public float damagerCheckLength = 2.8f, startDelay, directionSideSpeed = 1f, attackDamage = 175;
        public bool startActivated, justOneStomp;
        public GameObject startPosition, endPosition, splatterEffect, splatterEffectKlein;
        
        private float directionSideSpeedCurrent;
        private int stompCounter;
        private bool larryDamaged;
        private Rigidbody2D rb;
        private PlatformerCharacter2D player;
        private Transform damagerCheck;
        private AudioSource activationSound, movementLoop, hitSound;

        // Start is called before the first frame update
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            damagerCheck = transform.Find("DamagerCheck");
            activationSound = damagerCheck.GetComponents<AudioSource>()[0];
            movementLoop = damagerCheck.GetComponents<AudioSource>()[1];
            hitSound = damagerCheck.GetComponents<AudioSource>()[2];
            directionSideSpeedCurrent = directionSideSpeed;
            if (startActivated)
            {
                StartCoroutine(waitForStartDelay());
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (moving)
            {
                if (directionSideSpeed > 0)
                {
                    transform.Translate((startPosition.transform.position - endPosition.transform.position) * -Time.deltaTime * directionSideSpeedCurrent);
                }
                else if (directionSideSpeed < 0)
                {
                    transform.Translate((startPosition.transform.position - endPosition.transform.position) * Time.deltaTime * directionSideSpeedCurrent);
                }

                if (Physics2D.Raycast(damagerCheck.position, Vector2.down, damagerCheckLength, 1 << 8) && !larryDamaged)
                {
                    larryDamaged = true;
                    DamageLarry(damagerCheck.position);
                }
                if (((damagerCheck.position.x < endPosition.transform.position.x && directionSideSpeedCurrent < 0) || (startPosition.transform.position.x < damagerCheck.position.x && directionSideSpeedCurrent > 0)) && directionSideSpeed < 0)
                {
                    CheckChangeDirection();
                }
                else if (((damagerCheck.position.x < startPosition.transform.position.x && directionSideSpeedCurrent < 0) || (endPosition.transform.position.x < damagerCheck.position.x && directionSideSpeedCurrent > 0)) && directionSideSpeed > 0)
                {
                    CheckChangeDirection();
                }
            }
        }
        public void StartStone()
        {
            StartCoroutine(waitForStartDelay());
        }

        public void StopStone()
        {
            moving = false;
        }

        public void Raise()
        {
            activationSound.PlayDelayed(0);
            movementLoop.PlayDelayed(0);
            moving = true;
            CheckChangeDirection();
        }

        private void CheckChangeDirection()
        {
            if (stompCounter < 2)
            {
                if (justOneStomp)
                {
                    stompCounter++;
                }
                directionSideSpeedCurrent *= -1;
            }
            else
            {
                stompCounter = 0;
                moving = false;
                rb.velocity = new Vector2(0, 0);
            }
        }

        private void DamageLarry(Vector2 hitPosition)
        {
            if (player.GetComponent<PlayerStats>().health < attackDamage)
            {
                StartCoroutine(killLarry());
            }
            else
            {
                Vector2 pushDirection = new Vector2(Mathf.Sign(directionSideSpeed) * 40, 0);
                player.Hit((int)attackDamage, DamageTypeToPlayer.ImpaledFeet, pushDirection, hitPosition,gameObject, Guid.NewGuid());
                Instantiate(splatterEffectKlein, new Vector3(player.GetComponent<Transform>().position.x, player.GetComponent<Transform>().position.y - 1, player.GetComponent<Transform>().position.z), Quaternion.identity);
                larryDamaged = false;
            }
        }

        private IEnumerator waitForStartDelay()
        {
            yield return new WaitForSeconds(startDelay);
            Raise();
        }

        private IEnumerator killLarry()
        {
            player.KillPlayer(DamageTypeToPlayer.ImpaledFeet);
            Instantiate(splatterEffect, new Vector3(player.GetComponent<Transform>().position.x, player.GetComponent<Transform>().position.y, player.GetComponent<Transform>().position.z), Quaternion.identity);
            hitSound.PlayDelayed(0);
            GetComponent<Collider2D>().enabled = false;
            yield return new WaitForSeconds(3.5f);
            GetComponent<Collider2D>().enabled = true;
            larryDamaged = false;
        }
    }
}
