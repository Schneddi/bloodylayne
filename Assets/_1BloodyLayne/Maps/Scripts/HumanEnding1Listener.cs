using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using Units.Player;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class HumanEnding1Listener : MonoBehaviour
    {
        public GameObject dialogchoice1, dialogchoice2, dialogchoice3, dialogChoices, dialogChoiceListener, choice1Button, choice2Button, blackMage, newCameraTarget, endingScreen;
        [FormerlySerializedAs("camera")] public GameObject cameraRef;
        public GameObject[] burnEffects;
        private bool waitForListener, waitForDialog2, waitForDialog3;
        private AudioSource badEndingAudioSource;
        private GameController gc;
        private PlatformerCharacter2D larry;

        void Start()
        {
            larry = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            larry.InitializeHuman(1);
            badEndingAudioSource = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
            if (dialogchoice1.activeSelf)
            {
                dialogChoiceListener.GetComponent<DialogChoice>().listenDialog = dialogchoice1.GetComponent<Dialogbox>();
                dialogChoiceListener.GetComponent<DialogChoice>().listen = true;
                dialogChoiceListener.GetComponent<DialogChoice>().isActivated = true;
                choice1Button.GetComponent<Button>().interactable = false;
            }
            if (dialogchoice1.activeSelf)
            {
                waitForListener = true;
            }
            if (dialogchoice2.activeSelf)
            {
                waitForDialog2 = true;
            }
            if (dialogchoice3.activeSelf)
            {
                waitForDialog3 = true;
            }
            if (dialogChoices.activeSelf && waitForListener)
            {
                waitForListener = false;
                choice2Button.GetComponent<Button>().Select();
            }
            if (!dialogchoice2.activeSelf && waitForDialog2)
            {
                waitForDialog2 = false;
                StartCoroutine(blackMage.GetComponent<BlackMage>().despawn(false));
            }
            if (!dialogchoice3.activeSelf && waitForDialog3)
            {
                waitForDialog3 = false;
                StartCoroutine(playHumanBadEnding());
            }
        }
        
        IEnumerator playHumanBadEnding()
        {
            StartCoroutine(blackMage.GetComponent<BlackMage>().badEndingDespawn());
            cameraRef.GetComponent<Camera2DFollow>().target = newCameraTarget.transform;
            yield return new WaitForSeconds(2);
            foreach (GameObject burnEffect in burnEffects)
            {
                burnEffect.GetComponent<ParticleSystem>().Play();
            }
            badEndingAudioSource.PlayDelayed(0);
            yield return new WaitForSeconds(4);
            endingScreen.GetComponent<EndingScreen>().startEndingScreen();
        }
    }
}