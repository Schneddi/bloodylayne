using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering.Universal;

//adjust size and intensity of a light in a given timeframe
public class LightAdjustment : MonoBehaviour
{
    public bool startImmediatly = true;
    public float startDelay;
    public float animationDuration;
    public float intensityAim;
    public float sizeAim;
    
    private Light2D m_Lightsource;
    private bool adjusting;
    private float startLightIntensity, differenceStartAimIntensity, startSize, differenceStartAimSize;
    
    // Start is called before the first frame update
    void Start()
    {
        m_Lightsource = GetComponent<Light2D>();
        startLightIntensity = m_Lightsource.intensity;
        startSize = m_Lightsource.pointLightOuterRadius;
        differenceStartAimIntensity = intensityAim - startLightIntensity;
        differenceStartAimSize = sizeAim - startSize;
        if (startDelay > 0)
        {
            StartCoroutine(startDelayTimer(startDelay));
        }
        else if (startImmediatly)
        {
            adjusting = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (adjusting)
        {
            if (animationDuration == 0)
            {
                Debug.Log("animationDuration is set to 0 in BloodmagicHitScan in GameObject " + gameObject.name);
            }
            if (m_Lightsource.intensity != intensityAim)
            {
                float nextIntensityStep = differenceStartAimIntensity * (Time.deltaTime / animationDuration);
                if (math.distance(m_Lightsource.intensity, intensityAim) < math.abs(nextIntensityStep))
                {
                    m_Lightsource.intensity = intensityAim;
                }
                else
                {
                    m_Lightsource.intensity += nextIntensityStep;
                }
            }
            if (m_Lightsource.pointLightOuterRadius != sizeAim)
            {
                float nextSizeStep = differenceStartAimSize * (Time.deltaTime / animationDuration);
                if (math.distance(m_Lightsource.pointLightOuterRadius, sizeAim) < math.abs(nextSizeStep))
                {
                    m_Lightsource.pointLightOuterRadius = sizeAim;
                }
                else
                {
                    m_Lightsource.pointLightOuterRadius += nextSizeStep;
                }
            }
            if (Math.Abs(m_Lightsource.intensity - intensityAim) < 0.0000005f && Math.Abs(m_Lightsource.pointLightOuterRadius - sizeAim) < 0.0000005f)
            {
                adjusting = false;
            }
        }
    }

    public void startAdjusting()
    {
        adjusting = true;
    }

    public IEnumerator startDelayTimer(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        adjusting = true;
    }
}
