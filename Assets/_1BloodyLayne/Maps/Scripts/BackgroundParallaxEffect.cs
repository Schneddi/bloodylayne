﻿using UnityEngine;
using UnityEngine.Serialization;

namespace _1BloodyLayne.Maps.Background
{
    public class BackgroundParallaxEffect : MonoBehaviour
    {
        public float xShiftingSpeed, yShiftingSpeed;
        public MovementMode movementMode;
        
        private RectTransform[] _bgLayers;
        private int _leftLayer, _rightLayer;
        private float _leftThreshold, _rightThreshold;
        private const float XSpeedAdjust = 0.01f;
        private Vector2 _startPosition, _lastPosition;
        public Vector2[] bgLayersOriginPosition;

        public void InitializeBackgroundParallaxEffect()
        {
            _bgLayers = new RectTransform[transform.childCount];
            bgLayersOriginPosition = new Vector2[transform.childCount];
            for (int i = 0; i < transform.childCount; i++)
            {
                _bgLayers[i] = transform.GetChild(i).GetComponent<RectTransform>();
                bgLayersOriginPosition[i] = transform.GetChild(i).GetComponent<RectTransform>().position;
            }

            _leftThreshold = _bgLayers[0].anchorMin.x - 0.5f;
            _rightThreshold = _bgLayers[transform.childCount-1].anchorMin.x + 0.5f;
            
            _leftLayer = 0;
            _rightLayer = transform.childCount-1;
            
            _startPosition = transform.position;
            _lastPosition = _startPosition;
        }

        void Update()
        {
            Vector2 currentPosition = transform.position;
            if (currentPosition.x != _lastPosition.x && (movementMode == MovementMode.Both || movementMode == MovementMode.Horizontally))
            {
                ShiftHorizontally(currentPosition);
            }
            if (movementMode == MovementMode.Both || movementMode == MovementMode.Vertically)
            {
                ShiftVertically(currentPosition);
            }
            _lastPosition = currentPosition;
        }
        
        
        private void ShiftHorizontally(Vector3 currentPosition)
        {
            //shift the anchor Points to the left or right
            float shift = (_lastPosition.x - currentPosition.x) * xShiftingSpeed * XSpeedAdjust;
            shift %= _bgLayers.Length;
            
            //we always want to start at the most left Layer and go to the right
            int currentLayer = _leftLayer;
            
            //first shift the images
            for (int i = 0; i < _bgLayers.Length; i++)
            {
                //if there is a layer to the left, always use its anchorMax value, to make sure there is no gap
                if (currentLayer != _leftLayer)
                {
                    int imageToTheLeftIndex = currentLayer - 1;
                    if (imageToTheLeftIndex < 0)
                    {
                        imageToTheLeftIndex = _bgLayers.Length-1;
                    }
                    _bgLayers[currentLayer].anchorMin = new Vector2(_bgLayers[imageToTheLeftIndex].anchorMax.x ,_bgLayers[currentLayer].anchorMin.y);
                }
                else
                {
                    _bgLayers[currentLayer].anchorMin = new Vector2(_bgLayers[currentLayer].anchorMin.x + shift ,_bgLayers[currentLayer].anchorMin.y);
                }
                _bgLayers[currentLayer].anchorMax = new Vector2(_bgLayers[currentLayer].anchorMax.x + shift ,_bgLayers[currentLayer].anchorMax.y);
                currentLayer++;
                if (currentLayer > _bgLayers.Length - 1)
                {
                    currentLayer = 0;
                }
            }
            
            //checkIf they need to be scrolled
            for (int i = 0; i < _bgLayers.Length; i++)
            {
                if (i==_leftLayer || i == _rightLayer)
                {
                    if (_bgLayers[i].anchorMin.x < _leftThreshold)
                    {
                        ScrollImages(_bgLayers[i], i, true);
                    }
                    else if (_bgLayers[i].anchorMin.x > _rightThreshold)
                    {
                        ScrollImages(_bgLayers[i], i, false);
                    }
                }
            }
        }

        private void ScrollImages(RectTransform layer, int layerIndex, bool right)
        {
            if (!right)
            {
                layer.anchorMin = new Vector2(_bgLayers[_leftLayer].anchorMin.x - 1, _bgLayers[_leftLayer].anchorMin.y);
                layer.anchorMax = new Vector2(_bgLayers[_leftLayer].anchorMax.x - 1, _bgLayers[_leftLayer].anchorMax.y);
                _leftLayer = layerIndex;
                int rightIndex = layerIndex - 1;
                if (rightIndex < 0)
                {
                    rightIndex = _bgLayers.Length - 1;
                }
                _rightLayer = rightIndex;
            }
            else if (right)
            {
                layer.anchorMin = new Vector2(_bgLayers[_rightLayer].anchorMin.x + 1, _bgLayers[_rightLayer].anchorMin.y);
                layer.anchorMax = new Vector2(_bgLayers[_rightLayer].anchorMax.x + 1, _bgLayers[_rightLayer].anchorMax.y);
                _rightLayer = layerIndex;
                int leftIndex = layerIndex + 1;
                if (leftIndex > _bgLayers.Length - 1)
                {
                    leftIndex = 0;
                }
                _leftLayer = leftIndex;
            }
        }
        
        private void ShiftVertically(Vector3 currentPosition)
        {
            //shift up or down from origin position
            float shift = (currentPosition.y - _startPosition.y) + (currentPosition.y - _startPosition.y) * yShiftingSpeed;

            for (int i = 0; i < _bgLayers.Length; i++)
            {
                //shift the y position only
                _bgLayers[i].transform.position = new (_bgLayers[i].position.x,bgLayersOriginPosition[i].y + shift);
                
                //set bgLayerAnchoredPosition.x to 0
                Vector3 bgLayerAnchoredPosition = _bgLayers[i].GetComponent<RectTransform>().anchoredPosition;
                _bgLayers[i].GetComponent<RectTransform>().anchoredPosition = new(0,bgLayerAnchoredPosition.y);
                Vector3 rectTransformPosition = _bgLayers[i].GetComponent<RectTransform>().localPosition;
                rectTransformPosition = new (rectTransformPosition.x, rectTransformPosition.y, 0);
                _bgLayers[i].GetComponent<RectTransform>().localPosition = rectTransformPosition;
            }
        }
    }

    public enum MovementMode
    {
        Both,
        Horizontally,
        Vertically
    }
}