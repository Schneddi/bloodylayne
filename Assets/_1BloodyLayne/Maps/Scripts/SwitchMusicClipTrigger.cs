using System;
using System.Collections;
using System.Collections.Generic;
using UI.PlayerUI;
using UnityEngine;
using UnityStandardAssets._2D;

public class SwitchMusicClipTrigger : MonoBehaviour
{
    public AudioClip newClip;
    
    private Camera2DFollow cameraScript;
    
    // Start is called before the first frame update
    void Start()
    {
        cameraScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            cameraScript.SwitchSound(newClip);
            Destroy(gameObject);
        }
    }
}
