﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Schluessel : MonoBehaviour {
    AudioSource collect;
    public GameObject schluesselloch;
    public GameObject pickUpParticleSystem;
    private void Start()
    {
        collect = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            schluesselloch.GetComponent<Schluesselloch>().hatSchluessel = true;
            StartCoroutine(Death());
        }
    }

    IEnumerator Death()
    {
        Instantiate(pickUpParticleSystem, transform.position, Quaternion.identity, null);
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<ParticleSystem>().Stop(false, ParticleSystemStopBehavior.StopEmitting);
        collect.PlayDelayed(0);
        yield return new WaitForSeconds(3f);
        Destroy(gameObject);
    }
}
