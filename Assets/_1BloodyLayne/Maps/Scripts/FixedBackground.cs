﻿using UnityEngine;

namespace _1BloodyLayne.Maps.Background
{
    public class FixedBackground : MonoBehaviour
    {
        Transform _bgLayer;
        float _camX;
        public float shifting = 0.05f;

        // Use this for initialization
        void Start()
        {
            _bgLayer = transform;
            //bgLayer.transform.position = new Vector3(bgLayer.transform.position.x, Camera.main.transform.position.y, 10);
            _camX = Camera.main.transform.position.x;
        }
        // Update is called once per frame
        void Update()
        {
            if (Camera.main.transform.position.x > _camX)
            {
                ShiftRight(_camX - Camera.main.transform.position.x);
                _camX = Camera.main.transform.position.x;
            }
            if (Camera.main.transform.position.x < _camX)
            {
                ShiftLeft(_camX - Camera.main.transform.position.x);
                _camX = Camera.main.transform.position.x;
            }
        }

        void ShiftRight(float shift)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                _bgLayer.transform.position = new Vector3(_bgLayer.transform.position.x + shift * shifting, _bgLayer.transform.position.y, 10);
            }
        }


        void ShiftLeft(float shift)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                _bgLayer.transform.position = new Vector3(_bgLayer.transform.position.x + shift * shifting, _bgLayer.transform.position.y, 10);
            }
        }
    }
}