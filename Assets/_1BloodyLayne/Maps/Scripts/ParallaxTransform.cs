using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class ParallaxTransform : MonoBehaviour
{
    [SerializeField]
    public GameObject mainTilemap;
    //centerReferencePoint & rightPlayerReferencePoint are only used for visual guidance
    [SerializeField]
    public Transform leftPlayerReferencePoint, leftReferencePoint, rightReferencePoint, centerReferencePoint, rightPlayerReferencePoint, referencePointsParent, lastCurrentPointReferences, topReferencePoint, topPlayerReferencePoint;
    [SerializeField]
    public Vector3 mainTileMapLocalPosition;
    public float yShiftingSpeedMod;
    public float activationDistance = 50;

    //a variable for the user, to read out the current used speed.
    [ReadOnly] [SerializeField] public float parallaxSpeed;
    
    private float xShiftingSpeed, yShiftingSpeed;
    private float camX, camY;
    private Transform camTransform;
    [Tooltip("Used for optimisation. If the player is further away than this value, the tilemap will not shift.")]
    private float activationDistanceSqr;

    public void Start()
    {
        
        if(mainTilemap.transform.localPosition != mainTileMapLocalPosition)
        {
            mainTilemap.transform.localPosition = mainTileMapLocalPosition;
        }
        
        camTransform = GameObject.FindWithTag("MainCamera").transform;
        Vector3 leftReferencePosition = leftReferencePoint.position;
        Vector3 centerPointPosition = referencePointsParent.transform.position;
        Vector3 centerGameObjectPosition = mainTilemap.transform.position;
        
        float pointsDistance = rightReferencePoint.position.x - leftReferencePosition.x;
        float playerReferenceDistance = centerPointPosition.x - leftPlayerReferencePoint.position.x;
        xShiftingSpeed = (0.5f * pointsDistance)/playerReferenceDistance;
        parallaxSpeed = xShiftingSpeed * 100;
        yShiftingSpeed = yShiftingSpeedMod * xShiftingSpeed;
        
        camX = camTransform.position.x;
        camY = camTransform.position.y;
        
        float newXPosition = centerGameObjectPosition.x + (camX - centerPointPosition.x) * xShiftingSpeed;
        centerGameObjectPosition = new Vector3(newXPosition, centerGameObjectPosition.y, centerGameObjectPosition.z);
        mainTilemap.transform.position = centerGameObjectPosition;
        
        
        float newYPosition = centerGameObjectPosition.y + (camY - lastCurrentPointReferences.position.y) * yShiftingSpeed;
        centerGameObjectPosition = new Vector3(centerGameObjectPosition.x, newYPosition, centerGameObjectPosition.z);
        mainTilemap.transform.position = centerGameObjectPosition;
        
        if (rightReferencePoint != null)
        {
            rightReferencePoint.gameObject.SetActive(false);
        }
        if (leftReferencePoint != null)
        {
            leftReferencePoint.gameObject.SetActive(false);
        }

        activationDistanceSqr = Mathf.Pow(activationDistance, 2);
    }
    
    

    void Update()
    {
        Vector3 camPosition = camTransform.position;
        Vector3 lastCurrentPointReferencesPosition = lastCurrentPointReferences.position;
        Vector2 shiftedPosition = lastCurrentPointReferencesPosition + (mainTilemap.transform.position - mainTileMapLocalPosition);
        if (Vector2.SqrMagnitude((Vector3)shiftedPosition - camPosition) < activationDistanceSqr || Vector2.SqrMagnitude(lastCurrentPointReferencesPosition - camPosition) < activationDistanceSqr)
        {
            Vector3 position = mainTilemap.transform.position;
            if (camPosition.x != camX)
            {
                position = ShiftHorizontally(mainTilemap.transform.position);
            }
            if (camPosition.y != camY)
            {
                position = ShiftVertically(position);
            }
            mainTilemap.transform.position = position;
        }
    }


    private Vector3 ShiftHorizontally(Vector3 position)
    {
        //shift the anchor Points to the left or right
        Vector3 camXPosition = camTransform.position;
        float shift = (camXPosition.x - camX) * xShiftingSpeed;
        camX = camXPosition.x;

        position = new (position.x + shift, position.y, position.z);
        return position;
    }
    
    private Vector3 ShiftVertically(Vector3 position)
    {
        //shift the anchor Points to the left or right
        Vector3 camYPosition = camTransform.position;
        float shift = (camYPosition.y - camY) * yShiftingSpeed;
        camY = camYPosition.y;

        return new (position.x, position.y + shift, position.z);
    }
}