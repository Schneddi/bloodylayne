using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using Units.Enemys;
using Units.Player;
using UnityEngine;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class NosEnd2Control : MonoBehaviour
    {
        public GameObject startDialog,
            endDialog,
            servant,
            werewolf;
        public GameObject[] startDialogChoices, endDialogChoices, endingScreens;
        public AudioClip battleMusic;

        private Dialog dialog;
        private bool waitForStartDialogFinish, waitForEndDialogFinish, walkingServant, listenDialog;
        private GameObject player, endingScreen;
        private Transform endPosServant, endPosLarry, endPosWerewolf;
        private Camera2DFollow cameraScript;
        private GlobalVariables gv;
        private BossUI bossUI;

        private void Start()
        {
            dialog = FindObjectOfType<Dialog>();
            player = GameObject.FindWithTag("Player");
            endPosServant = transform.Find("EndPosServant");
            endPosLarry = transform.Find("EndPosLarry");
            endPosWerewolf = transform.Find("EndPosWerewolf");
            cameraScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            bossUI = GameObject.Find("BossUI").GetComponent<BossUI>();
            if (gv.werewolfIntroDone)
            {
                werewolf.GetComponent<Werewolf>().initVars();
                startWerewolfFight();
            }
            else
            {
                startStartDialogControl();
            }
        }

        // Update is called once per frame
        void Update()
        {
            foreach (GameObject startDialogChoice in startDialogChoices)
            {
                if (startDialogChoice.activeSelf)
                {
                    waitForStartDialogFinish = true;
                }
            }
            if (!startDialogChoices[0].activeSelf &&!startDialogChoices[1].activeSelf &&!startDialogChoices[2].activeSelf && waitForStartDialogFinish)
            {
                //werewolf.SetActive(false);
                waitForStartDialogFinish = false;
                gv.werewolfIntroDone = true;
                startWerewolfFight();
            }
            
            for (int i = 0; i < endDialogChoices.Length; i++)
            {
                if (endDialogChoices[i].activeSelf)
                {
                    endingScreen = endingScreens[i];
                    waitForEndDialogFinish = true;
                }
            }
            if (!endDialogChoices[0].activeSelf &&!endDialogChoices[1].activeSelf &&!endDialogChoices[2].activeSelf && waitForEndDialogFinish)
            {
                waitForEndDialogFinish = false;
                endingScreen.GetComponent<EndingScreen>().startEndingScreen();
            }
        }

        private void startStartDialogControl()
        {
            dialog.ZeigeBox(startDialog);
        }

        private void startWerewolfFight()
        {
            bossUI.activateBossUI();
            werewolf.GetComponent<Werewolf>().startFight();
            player.GetComponent<PlatformerCharacter2D>().InitializeSkills(true);
            cameraScript.SwitchSound(battleMusic);
        }
        
        public IEnumerator startEndDialogControl()
        {
            bossUI.deactivateBossUI();
            yield return new WaitForSeconds(1f);
            player.GetComponent<Platformer2DUserControl>().BlockInput = true;
            servant.transform.position = endPosServant.transform.position;
            servant.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            player.transform.position = endPosLarry.transform.position;
            player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            werewolf.transform.position = endPosWerewolf.transform.position;
            werewolf.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            if (!player.GetComponent<PlatformerCharacter2D>().m_FacingRight)
            {
                player.GetComponent<PlatformerCharacter2D>().Flip();
            }
            if (!werewolf.GetComponent<EnemyStats>().facingRight)
            {
                werewolf.GetComponent<Werewolf>().Flip();
            }
            yield return new WaitForSeconds(2f);
            dialog.ZeigeBox(endDialog);
        }
    }
}