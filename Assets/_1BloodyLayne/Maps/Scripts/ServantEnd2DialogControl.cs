using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using Units.Enemys;
using UnityEngine;
using UnityEngine.Serialization;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class ServantEnd2DialogControl : MonoBehaviour
{
    public GameObject startDialog,
        choice1Dialog,
        choice2Dialog,
        choice3Dialog,
        choice3KneelDialog,
        dragon,
        startCamPosition,
        endingDialog,
        nosferatu,
        shield,
        endChoice1Dialog,
        endChoice2Dialog,
        endChoice3Dialog,
        endScreenGood,
        endScreenMedium,
        endScreenBad,
        FadeScreen;

    [FormerlySerializedAs("camera")] public GameObject cameraRef;
    public AudioClip battleMusic;
    
    private Dialog dialog;
    private bool listenForBattleStart, nosFlyToFinalPos, listenForEnd, kneeling;
    private GameObject player, shieldChild;
    private Transform finalPlayerPosition, finalNosferatuPosition, finalDragonPosition;
    private Animator nosAnimator;
    private Rigidbody2D nosRb;
    private Camera2DFollow cameraScript;
    private GlobalVariables gv;
    private BossUI bossUI;
    
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        dialog = FindObjectOfType<Dialog>();
        nosAnimator = nosferatu.GetComponent<Animator>();
        nosRb = nosferatu.GetComponent<Rigidbody2D>();
        finalPlayerPosition = transform.Find("FinalPlayerPosition");
        finalNosferatuPosition = transform.Find("FinalNosferatuPosition");
        finalDragonPosition = transform.Find("FinalDragonPosition");
        player.GetComponent<Platformer2DUserControl>().BlockInput = true;
        cameraScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
        gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        bossUI = GameObject.Find("BossUI").GetComponent<BossUI>();
        StartCoroutine(initialDelayAndStart());
    }

    // Update is called once per frame
    void Update()
    {
        if ((choice1Dialog.activeSelf || choice2Dialog.activeSelf || choice3Dialog.activeSelf) && !listenForBattleStart)
        {
            listenForBattleStart = true;
            gv.dragonEndingScore++;
            if (choice2Dialog.activeSelf || choice3Dialog.activeSelf)
            {
                gv.dragonEndingScore++;
            }

            
        }
        if (choice3KneelDialog.activeSelf && !kneeling)
        {
            kneeling = true;
            player.GetComponent<Animator>().SetTrigger("Kneel");
        }
        if ((!choice1Dialog.activeSelf && !choice2Dialog.activeSelf && !choice3Dialog.activeSelf) && listenForBattleStart)
        {
            if (kneeling)
            {
                player.GetComponent<Animator>().SetTrigger("StandUp");
                kneeling = false;
            }
            listenForBattleStart = false;
            startDragonFight();
        }

        if (nosFlyToFinalPos)
        {
            //Vector2 direction = nosferatu.transform.position - finalNosferatuPosition.position;
            //nosRb.AddForce(direction);
            if (Vector2.Distance(nosferatu.transform.position, finalNosferatuPosition.position) < 0.2f)
            {
                nosFlyToFinalPos = false;
                landNosferatu();
            }
        }
        if ((endChoice1Dialog.activeSelf || endChoice2Dialog.activeSelf || endChoice3Dialog.activeSelf) && !listenForBattleStart)
        {
            listenForEnd = true;
            if (endChoice2Dialog.activeSelf || endChoice3Dialog.activeSelf)
            {
                gv.dragonEndingScore++;
            }
        }
        if ((!endChoice1Dialog.activeSelf && !endChoice2Dialog.activeSelf && !endChoice3Dialog.activeSelf) && listenForEnd)
        {
            listenForEnd = false;
            if (gv.dragonEndingScore == 1)
            {
                endScreenGood.GetComponent<EndingScreen>().startEndingScreen();
            }
            else if (gv.dragonEndingScore == 2)
            {
                endScreenMedium.GetComponent<EndingScreen>().startEndingScreen();
            }
            else
            {
                endScreenBad.GetComponent<EndingScreen>().startEndingScreen();
            }
        }
    }
    
    
    IEnumerator initialDelayAndStart()
    {
        yield return new WaitForSeconds(0.1f);
        cameraRef.GetComponent<Camera2DFollow>().SetDamping(0.1f);
        cameraRef.GetComponent<Camera2DFollow>().target = startCamPosition.transform;
        yield return new WaitForSeconds(0.75f);
        dragon.GetComponent<Dragon>().showLandingEffect();
        yield return new WaitForSeconds(2.25f);
        if (gv.dragonEndingScore > 0)
        {
            cameraRef.GetComponent<Camera2DFollow>().target = player.transform;
            cameraRef.GetComponent<Camera2DFollow>().ResetDamping();
            player.GetComponent<Platformer2DUserControl>().BlockInput = false;
            startDragonFight();
        }
        else
        {
            dialog.ZeigeBox(startDialog);
        }
    }

    private void startDragonFight()
    {
        bossUI.activateBossUI();
        dragon.GetComponent<Dragon>().startFight();
        cameraScript.SwitchSound(battleMusic);
    }


    private void landNosferatu()
    {
        Destroy(shieldChild);
        nosferatu.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        nosferatu.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        nosAnimator.SetTrigger("Idle");
        nosferatu.transform.position = finalNosferatuPosition.position;
        dialog.ZeigeBox(endingDialog);
        player.GetComponent<PlatformerCharacter2D>().Flip();
    }
    

    public IEnumerator playEndingDialog()
    {
        FadeScreen.GetComponent<FadeScreen>().startFadeScreen();
        bossUI.deactivateBossUI();
        player.GetComponent<Platformer2DUserControl>().BlockInput = true;
        yield return new WaitForSeconds(1f);
        player.transform.position = finalPlayerPosition.position;
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        player.GetComponent<PlatformerCharacter2D>().NewInvulnerable(9999, false, true);
        dragon.transform.position = finalDragonPosition.position;
        dragon.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        dragon.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        cameraRef.GetComponent<Camera2DFollow>().target = dragon.transform;
        if(!player.GetComponent<PlatformerCharacter2D>().m_FacingRight)
        {
            player.GetComponent<PlatformerCharacter2D>().Flip();
        }

        if (dragon.GetComponent<EnemyStats>().facingRight)
        {
            dragon.GetComponent<Dragon>().Flip();
        }
        yield return new WaitForSeconds(1f);
        nosFlyToFinalPos = true;
        nosferatu.SetActive(true);
        nosferatu.GetComponent<Rigidbody2D>().gravityScale = 0;
        nosAnimator.SetTrigger("Fliegen");
        Vector2 direction = finalNosferatuPosition.position - nosferatu.transform.position;
        nosRb.velocity = direction.normalized * 4;
        shieldChild = Instantiate(shield, nosferatu.transform.position, Quaternion.identity);
        shieldChild.transform.parent = nosferatu.gameObject.transform;
    }
}
