﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Player;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
	public class Szenenuebergang : MonoBehaviour //der Szenenübergang macht eigentlich nichts, außer dem Gamecontroller eine Szene zu übergeben. Die Klasse liegt auf einer Triggerzone der Map und muss daher unabhängig sein
	{
		//load a random scene, out of a given number of scenes. Prefered Way are String names for safety
		public int[] sceneNumbers, bossSceneNumbers;
		public bool firstPlaythroughScene;
		GameController gc;
        GlobalVariables gv;

		void OnTriggerEnter2D(Collider2D other)
		{
            if (other.name == "Player" && !other.GetComponent<PlatformerCharacter2D>().dead)
            {
	            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
                gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
                gc.NextLevel(selectScene());
            }
		}

		private int selectScene()
		{
			int sceneInt=0;
			int randomArraySelector;
			bool sceneSelected = false;
			if (bossSceneNumbers.Length > 0)
			{
				randomArraySelector = Random.Range(0, bossSceneNumbers.Length);
				sceneSelected = Random.Range(0, 2) == 1;
				if (gv.noRandomLvL)
				{
					randomArraySelector = 0;
					sceneSelected = true;
				}
				if(firstPlaythroughScene && !gv.firstPlaythroughDone)
				{
					randomArraySelector = 0;
					sceneSelected = true;
				}
				sceneInt = bossSceneNumbers[randomArraySelector];
			}
			if (sceneNumbers.Length > 0 && !sceneSelected)
			{
				randomArraySelector = Random.Range(0, sceneNumbers.Length);
				if (gv.noRandomLvL)
				{
					randomArraySelector = 0;
				}
				sceneInt = sceneNumbers[randomArraySelector];
			}
			return sceneInt;
		}
	}
}