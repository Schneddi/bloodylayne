using System;
using System.Collections;
using System.Collections.Generic;
using Maps.Objects.Gitter;
using UnityEngine;

public class GunnerTrap : MonoBehaviour
{
    public GridReworked[] grids;
    public GunnerSpawnPoint[] gunnerSpawnPoints;
    public bool battleFinished;

    private bool waitForBattleEnd;
    private bool activated;
    // Start is called before the first frame update
    void Update()
    {
        if (waitForBattleEnd)
        {
            int remainingSpawnPoints = 0;
            foreach (GunnerSpawnPoint gunnerSpawnPoint in gunnerSpawnPoints)
            {
                if (gunnerSpawnPoint.remainingSpawnCount != 0 || gunnerSpawnPoint.spawnedGunner != null)
                {
                    remainingSpawnPoints++;
                }
            }

            if (remainingSpawnPoints == 0)
            {
                waitForBattleEnd = false;
                battleFinished = true;
                foreach (GridReworked grid in grids)
                {
                    grid.raiseGrid();
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player" && !activated)
        {
            activated = true;
            waitForBattleEnd = true;
            foreach (GridReworked grid in grids)
            {
                grid.lowerGrid();
            }
        }
    }

    public void customActivation()
    {
        activated = true;
        waitForBattleEnd = true;
        foreach (GridReworked grid in grids)
        {
            grid.lowerGrid();
        }
    }
}
