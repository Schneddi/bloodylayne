using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Tilemaps;

public class AutoBloodEffectGrid : MonoBehaviour
{
    public Color bloodColor = Color.red;
    
    // Start is called before the first frame update
    async void Start()
    {
        await GenerateBloodTileMap(); //do this as Task, as generating Tilemaps can take some time
    }
    
    private Task GenerateBloodTileMap()
    {
        GameObject bloodTileMapObject = new GameObject();
        bloodTileMapObject.name = gameObject.name + "BloodTileMap";
        bloodTileMapObject.transform.parent = transform;
        bloodTileMapObject.transform.localScale = new Vector3(1, 1, 1);
        bloodTileMapObject.transform.localPosition = Vector3.zero;
        
        CopyComponent(GetComponent<Tilemap>(), bloodTileMapObject);
        CopyComponent(GetComponent<TilemapRenderer>(), bloodTileMapObject);
        
        Tilemap tilemap = GetComponent<Tilemap>();

        BoundsInt bounds = tilemap.cellBounds;
        
        Vector3Int[] positions = new Vector3Int[bounds.size.x * bounds.size.y];

        Tilemap bloodTileMap = bloodTileMapObject.GetComponent<Tilemap>();
        
        int index = 0;
        foreach (Vector3Int boundInt in bounds.allPositionsWithin)
        {
            positions[index] = boundInt;
            index++;
        }
        
        bloodTileMap.SetTiles(positions,tilemap.GetTilesBlock(bounds));
        
        foreach (Vector3Int boundInt in bounds.allPositionsWithin)
        {
            Matrix4x4 tilematrix = tilemap.GetTransformMatrix(boundInt);
            bloodTileMap.SetTransformMatrix(boundInt, tilematrix);
        }
        
        bloodTileMap.color = bloodColor;
        bloodTileMapObject.GetComponent<TilemapRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        bloodTileMapObject.GetComponent<TilemapRenderer>().sortingOrder += 1;
        return Task.CompletedTask;
    }
    
    
    T CopyComponent<T>(T original, GameObject destination) where T : Component
    {
        System.Type type = original.GetType();
        T dst = destination.GetComponent(type) as T;
        if (!dst) dst = destination.AddComponent(type) as T;
        FieldInfo[] fields = type.GetFields();
        foreach (FieldInfo field in fields)
        {
            if (field.IsStatic) continue;
            field.SetValue(dst, field.GetValue(original));
        }
        PropertyInfo[] props = type.GetProperties();
        foreach (PropertyInfo prop in props)
        {
            if (!prop.CanWrite || !prop.CanWrite || prop.Name == "name") continue;
            prop.SetValue(dst, prop.GetValue(original, null), null);
        }
        return dst as T;
    }
}
