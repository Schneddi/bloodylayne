﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class FallingStoneTrigger : MonoBehaviour
    {
        public GameObject[] stones;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if(other.name == "Player")
            {
                foreach (GameObject stone in stones)
                {
                    if (!stone.GetComponent<ExitStone>().moving)
                    {
                        stone.GetComponent<ExitStone>().startStone();
                    }
                }
                Destroy(gameObject);
            }
        }
    }
}
