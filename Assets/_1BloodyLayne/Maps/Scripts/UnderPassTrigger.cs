using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnderPassTrigger : MonoBehaviour
{
    public BanditControl bandit;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (bandit != null)
            {
                bandit.MoveToRightGuardPosition();
            }

            Destroy(gameObject);
        }
    }
}
