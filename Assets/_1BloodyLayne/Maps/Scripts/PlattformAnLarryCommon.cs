﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class PlattformAnLarryCommon : MonoBehaviour
    {
        public GameObject pScript;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                other.transform.parent = transform.parent;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                other.transform.parent = null;
            }
        }
    }
}
