﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class KettenTrommell : MonoBehaviour
    {
        public GameObject Kettentrommel, Gitter;
        bool used;
        AudioSource hebelSound;
        // Use this for initialization
        void Start()
        {
            hebelSound = GetComponent<AudioSource>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player" && !used)
            {
                used = true;
                hebelSound.PlayDelayed(0);
                Gitter.GetComponent<GitterCastlestart>().hochziehen = true;
                Kettentrommel.GetComponent<Animator>().SetBool("Gitterziehen", true);
                StartCoroutine(Stop());
            }
        }

        IEnumerator Stop()
        {
            yield return new WaitForSeconds(2f);
            Kettentrommel.GetComponent<Animator>().SetBool("Gitterziehen", false);
        }
    }
}
