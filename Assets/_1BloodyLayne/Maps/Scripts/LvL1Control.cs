using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Enemys;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class LvL1Control : MonoBehaviour
{
    public GameObject introDialog, farmer, bandit, farmerDialog, banditDialog, bloodThirstDialogTrigger, peasantEndDialog, activatePeasant, tutorialBloodThirstDialog;
    public AudioClip evilLaugh, farmerDeathSound;
    public GameObject[] removePeasants;
    public float farmerDeathTimeEng, farmerDeathTimeGer;

    private GlobalVariables gv;
    private GameController gc;
    private bool listenForPeasantDialog, bloodmagicEnabled, farmerDialogEndTriggered;
    private PlatformerCharacter2D playerChar;
    private bool listenForBloodthirstDialog, bandidDead, farmerDialogTriggered, bandidDialogFinished;
    private AudioSource audioSource;
    private Dialog dialog;
    
    // Start is called before the first frame update
    void Start()
    {
        gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
        playerChar = GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>();
        audioSource = GetComponent<AudioSource>();
        dialog = FindObjectOfType<Dialog>();
        playerChar.GetComponent<Platformer2DUserControl>().disableBite = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (tutorialBloodThirstDialog.activeSelf && !listenForBloodthirstDialog)
        {
            listenForBloodthirstDialog = true;
        }
        if (!tutorialBloodThirstDialog.activeSelf && !gv.firstPlaythroughDone && listenForBloodthirstDialog)
        {
            listenForBloodthirstDialog = false;
            ActivateBloodthirst(true);
        }

        if (peasantEndDialog.activeSelf && !listenForPeasantDialog)
        {
            listenForPeasantDialog = true;
        }

        if(!peasantEndDialog.activeSelf && listenForPeasantDialog)
        {
            listenForPeasantDialog = false;
            StartCoroutine(PeasantDiaFinished());
        }
        
        if (farmerDialog.activeSelf && !farmerDialogTriggered)
        {
            farmerDialogTriggered = true;
            StartCoroutine(FarmerKillStart());
        }
        
        if (banditDialog.GetComponent<Dialogbox>().finished && !bandidDialogFinished)
        {
            bandidDialogFinished = true;
            bandit.GetComponent<Animator>().SetTrigger("MouthSerious");
        }
        if (!bandidDead)
        {
            if (bandit.GetComponent<EnemyStats>().dead)
            {
                bandidDead = true;
                StartCoroutine(BanditKilled());
            }
        }

        if (farmerDialog.GetComponent<Dialogbox>().finished && !farmerDialogEndTriggered)
        {
            farmer.GetComponent<Animator>().SetTrigger("SpeakIdle");
            farmer.GetComponent<Animator>().SetBool("Dead", true);
            bandit.transform.Find("Sounds").GetComponents<AudioSource>()[1].Stop();
            StartCoroutine(FarmerKillEnd());
        }
    }

    public void setTutorialDone(bool tutorialDone)
    {
        if (tutorialDone)
        {
            /*
            GameObject.FindWithTag("Player").GetComponent<Platformer2DUserControl>().disableBloodmagic = false;
            Destroy(bogoTrigger);
            Destroy(bloodThirstDialogTrigger.GetComponent<DialogTrigger>());
            introDialog.GetComponent<Dialogbox>().showLarryUI = true;
            */
        }
        else if(!tutorialDone)
        {
            bloodThirstDialogTrigger.GetComponent<ActivateBloodthirst>().deactived = true;
        }
    }
    
    private IEnumerator FarmerKillStart()
    {
        farmer.GetComponent<Animator>().SetTrigger("DialogBodyLanguage");
        float farmerDeathTime = 0;
        if (gv.Language == 0)
        {
            farmerDeathTime = farmerDeathTimeEng;
        }
        else
        {
            farmerDeathTime = farmerDeathTimeGer;
        }
        yield return new WaitForSeconds(farmerDeathTime);
        if (!farmerDialogEndTriggered)
        {
            StartCoroutine(FarmerKillEnd());
        }
    }
    
    private IEnumerator FarmerKillEnd()
    {
        farmerDialogEndTriggered = true;
        bandit.GetComponent<Animator>().SetTrigger("FarmerKill");
        bandit.transform.Find("Sounds").GetComponents<AudioSource>()[1].PlayDelayed(0);
        yield return new WaitForSeconds(2f);
        farmer.GetComponent<Animator>().SetBool("Dead", true);
        farmer.GetComponent<Animator>().SetTrigger("DeathMouth");
        audioSource.PlayOneShot(farmerDeathSound);
        farmer.GetComponent<Animator>().SetTrigger("Death1");
        yield return new WaitForSeconds(1.75f);
        farmer.GetComponent<Animator>().SetTrigger("Death");
        bandit.GetComponent<Animator>().SetTrigger("DialogBodyLanguage");
        dialog.ZeigeBox(banditDialog);
    }

    private IEnumerator BanditKilled()
    {
        yield return new WaitForSeconds(1f);
        audioSource.PlayOneShot(evilLaugh);
    }

    private IEnumerator PeasantDiaFinished()
    {
        StartCoroutine(activatePeasant.GetComponent<MistgabelBauer>().Patroulieren());
        foreach (GameObject removePeasant in removePeasants)
        {
            StartCoroutine(removePeasant.GetComponent<MistgabelBauer>().Patroulieren());
        }
        yield return new WaitForSeconds(2);
        foreach (GameObject removePeasant in removePeasants)
        {
            Destroy(removePeasant);
        }
    }

    public void ActivateBloodthirst(bool showUIEffect)
    {
        playerChar.GetComponent<Platformer2DUserControl>().disableBite = false;
        playerChar.activeBloodthirst = true;
        playerChar.ChangeBloodthirst(-playerChar.GetComponent<PlayerStats>().maxBloodlust, Guid.Empty);
    }
}
