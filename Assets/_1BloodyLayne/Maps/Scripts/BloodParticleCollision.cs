using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Tilemaps;

public class BloodParticleCollision : MonoBehaviour
{
    public GameObject splatPrefab;
    public AudioSource audioSource;
    public AudioClip[] sounds;
    public float soundCapResetSpeed = 0.55f;
    public float soundMaxCap = 1f;
    public float effectCapResetSpeed = 5f;
    public int maxSounds = 3;
    public int initialBloodEffects = 10;
    
    private ParticleSystem particle;
    private Transform splatHolder;
    private List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();
    private float timePassedLastSound, timePassedLastEffect;
    private int soundsPlayed, effectsSpawned;

    private void Start()
    {
        splatHolder = GameObject.Find("BloodGroundEffects").transform;
        particle = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        timePassedLastSound += Time.deltaTime;
        timePassedLastEffect += Time.deltaTime;
        soundMaxCap += Time.deltaTime;
        if (timePassedLastSound > soundCapResetSpeed)
        {
            timePassedLastSound = 0;
        }
    }
    
    private void OnParticleCollision(GameObject other)
    {
        if (other.layer != 11 && other.layer != 14)
        {
            return;
        }
        
        //limit the amount of particles spawned
        if (effectsSpawned > initialBloodEffects)
        {
            if (timePassedLastEffect < effectCapResetSpeed)
            {
                return;
            }
        }
        particle.GetCollisionEvents(other, collisionEvents);
        
        int count = collisionEvents.Count;

        for (int i = 0; i < count; i++)
        {
            if (effectsSpawned < initialBloodEffects || timePassedLastEffect >= effectCapResetSpeed)
            {
                GameObject spawnedSplatter = Instantiate(splatPrefab, collisionEvents[i].intersection,
                    Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f)), splatHolder);
                timePassedLastEffect = 0;
                effectsSpawned++;
                splatHolder.GetComponent<BloodGroundEffectsControl>().RegisterSplatterEffects(spawnedSplatter);
                spawnedSplatter.transform.SetParent(other.transform);
                
                if (other.GetComponent<SortingGroup>() == null)
                {
                    other.AddComponent<SortingGroup>();
                    int sortingLayer = 0;
                    int sortingOrder = 0;
                    if (other.GetComponent<SpriteRenderer>() != null)
                    {
                        sortingLayer = other.GetComponent<SpriteRenderer>().sortingLayerID;
                        sortingOrder = other.GetComponent<SpriteRenderer>().sortingOrder;
                    }

                    if (other.GetComponent<TilemapRenderer>() != null)
                    {
                        sortingLayer = other.GetComponent<TilemapRenderer>().sortingLayerID;
                        sortingOrder = other.GetComponent<TilemapRenderer>().sortingOrder;
                    }
                    other.GetComponent<SortingGroup>().sortingLayerID = sortingLayer;
                    other.GetComponent<SortingGroup>().sortingOrder = sortingOrder;
                }
            }

            if (soundsPlayed < maxSounds && soundsPlayed < soundMaxCap)
            {
                if (audioSource == null || !audioSource.enabled) return;
                soundsPlayed += 1;
                audioSource.pitch = Random.Range(0.9f, 1.1f);
                audioSource.PlayOneShot(sounds[Random.Range(0, sounds.Length)], Random.Range(0.1f, 0.35f));
            }
        }
    }
}