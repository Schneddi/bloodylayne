using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Maps.Objects
{
    public class LightFlickerSprite : MonoBehaviour
    {
        [Tooltip("This field determines the rate at which the light changes")]
        public float speed = 0.5f;
        [Tooltip("Minimal alpha value for the spriteRenderer, whilst changing.")]
        public float minAlphaValue = 0.35f; 
        [Tooltip("The minimal lightning change per frame * deltaTime")]
        public float randomMin=0.5f;
        [Tooltip("The maximal lightning change per frame * deltaTime")]
        public float randomMax=1f;
        
        public bool IsActivated { get; private set; }
        
        [Tooltip("False, if the the glow will be disabled at start")]
        [SerializeField]private bool startActivated = true;
    
        private float _startIntensity;
        private SpriteRenderer _lightSource;
    
        // Start is called before the first frame update
        void Start()
        {
            _lightSource = GetComponent<SpriteRenderer>();
            _startIntensity = _lightSource.color.a;
            _startIntensity = _lightSource.color.a;
            IsActivated = startActivated;
            
            Color lightningColor = _lightSource.color;
            lightningColor.a = Random.Range(minAlphaValue, lightningColor.a);
            if (!IsActivated) lightningColor.a = 0;
            _lightSource.color = lightningColor;
        }

        // Update is called once per frame
        void Update()
        {
            if (!IsActivated && _lightSource.color.a == 0) return;
            
            if (!IsActivated || _lightSource.color.a > _startIntensity)
            {
                speed = -math.abs(speed);
            }
            else if (_lightSource.color.a < minAlphaValue)
            {
                speed = math.abs(speed);
            }

            Color lightningColor = _lightSource.color;
            lightningColor.a += speed * Time.deltaTime * Random.Range(randomMin, randomMax);
            if (lightningColor.a < 0) lightningColor.a = 0;
            _lightSource.color = lightningColor;
        }

        public void ActivateLight()
        {
            IsActivated = true;
        }
        
        public void DeactivateLight()
        {
            IsActivated = false;
        }
    }
}
