using UnityEngine;

namespace Maps.Objects
{
    public class MagicLightControl : MonoBehaviour
    {
        [SerializeField]private float activationDistance = 8f;
    
        private bool _activated;
        private AudioSource _activationSound;
        private ParticleSystem _pSystem;
        private float _startIntensity;
        private Transform _player;
        private LightFlickerSprite _lightFlickerSprite;
    
        void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player").transform;
            _lightFlickerSprite = GetComponentInChildren<LightFlickerSprite>();
            _pSystem = GetComponent<ParticleSystem>();
            _activationSound = GetComponent<AudioSource>();
            _pSystem.Stop();
        }

        void Update()
        {
            float playerToObjectDistance = Vector2.Distance(transform.position, _player.position);
            if (!_activated && playerToObjectDistance < activationDistance)
            {
                _activated = true;
                _pSystem.Play();
                _activationSound.PlayOneShot(_activationSound.clip);
                if(!_lightFlickerSprite.IsActivated) _lightFlickerSprite.ActivateLight();
            }
            else if(_activated && playerToObjectDistance > activationDistance * 1.25f)
            {
                _activated = false;
                _pSystem.Stop();
                if(_lightFlickerSprite.IsActivated) _lightFlickerSprite.DeactivateLight();
            }
        }
    }
}
