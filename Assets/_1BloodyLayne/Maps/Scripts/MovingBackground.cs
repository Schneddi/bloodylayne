﻿using UnityEngine;

namespace _1BloodyLayne.Maps.Background
{
    public class MovingBackground : MonoBehaviour
    {
        public float speed;
        
        private RectTransform[] _bgLayers;
        private int _leftLayer, _rightLayer;
        private float _camX, _leftThreshold, _rightThreshold;
        private const float SpeedAdjust = 0.01f;

        // Use this for initialization
        void Awake()
        {
            _bgLayers = new RectTransform[transform.childCount];
            for (int i = 0; i < transform.childCount; i++)
            {
                _bgLayers[i] = transform.GetChild(i).GetComponent<RectTransform>();
            }

            _leftThreshold = _bgLayers[0].anchorMin.x - 0.5f;
            _rightThreshold = _bgLayers[transform.childCount-1].anchorMin.x + 0.5f;
            
            _leftLayer = 0;
            _rightLayer = transform.childCount-1;
            Update();
        }

        void Update()
        {
            ShiftHorizontally();
        }
        
        
        private void ShiftHorizontally()
        {
            //shift the anchor Points to the left or right
            float shift = Time.deltaTime * speed * SpeedAdjust;
            
            //first shift the images
            for (int i = 0; i < _bgLayers.Length; i++)
            {
                _bgLayers[i].anchorMin = new Vector2(_bgLayers[i].anchorMin.x + shift ,_bgLayers[i].anchorMin.y);
                _bgLayers[i].anchorMax = new Vector2(_bgLayers[i].anchorMax.x + shift ,_bgLayers[i].anchorMax.y);
            }
            
            //checkIf they need to be scrolled
            for (int i = 0; i < _bgLayers.Length; i++)
            {
                if (_bgLayers[i].anchorMin.x < _leftThreshold)
                {
                    ScrollImages(_bgLayers[i], i, true);
                }
                else if (_bgLayers[i].anchorMin.x > _rightThreshold)
                {
                    ScrollImages(_bgLayers[i], i, false);
                }
            }
        }

        private void ScrollImages(RectTransform layer, int layerIndex, bool right)
        {
            if (!right)
            {
                layer.anchorMin = new Vector2(_bgLayers[_leftLayer].anchorMin.x - 1, _bgLayers[_leftLayer].anchorMin.y);
                layer.anchorMax = new Vector2(_bgLayers[_leftLayer].anchorMax.x - 1, _bgLayers[_leftLayer].anchorMax.y);
                _leftLayer = layerIndex;
                int rightIndex = layerIndex - 1;
                if (rightIndex < 0)
                {
                    rightIndex = _bgLayers.Length - 1;
                }
                _rightLayer = rightIndex;
            }
            else
            {
                layer.anchorMin = new Vector2(_bgLayers[_rightLayer].anchorMin.x + 1, _bgLayers[_rightLayer].anchorMin.y);
                layer.anchorMax = new Vector2(_bgLayers[_rightLayer].anchorMax.x + 1, _bgLayers[_rightLayer].anchorMax.y);
                _rightLayer = layerIndex;
                int leftIndex = layerIndex + 1;
                if (leftIndex > _bgLayers.Length - 1)
                {
                    leftIndex = 0;
                }
                _leftLayer = leftIndex;
            }
        }
    }
}
