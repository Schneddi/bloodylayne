﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class ActivationAufstehen : MonoBehaviour
    {
        Dialog dialog;
        Platformer2DUserControl userControl;
        public GameObject dialogbox, reactionTarget;
        bool listen;
        // Use this for initialization
        void Start()
        {
            dialog = FindObjectOfType<Dialog>();
            userControl = GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>();
        }

        // Update is called once per frame
        void Update()
        {
            if (reactionTarget.activeSelf)
            {
                listen = true;
            }
            if (!reactionTarget.activeSelf && listen)
            {
                userControl.BlockInput = true;
            }
        }


        IEnumerator aufstehen()
        {
            yield return new WaitForSeconds(1f);
            dialog.ZeigeBox(dialogbox);
            gameObject.SetActive(false);
        }
    }
}
