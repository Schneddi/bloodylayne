using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class BloodGroundEffectsControl : MonoBehaviour
{
    public int maxSplatterEffects = 100;
    
    private Queue<GameObject> splatterEffects;

    // Start is called before the first frame update
    void Start()
    {
        splatterEffects = new Queue<GameObject>();
    }

    public void RegisterSplatterEffects(GameObject splatterEffect)
    {
        splatterEffects.Enqueue(splatterEffect);
        if (splatterEffects.Count > maxSplatterEffects)
        {
            Destroy(splatterEffects.Dequeue());
        }
    }
}
