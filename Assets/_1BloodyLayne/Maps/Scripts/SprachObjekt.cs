﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.Serialization;

namespace UnityStandardAssets._2D
{
    public class SprachObjekt : MonoBehaviour
    {
        public Sprite englisch;
        [FormerlySerializedAs("deutsch")] public Sprite german;

        private GlobalVariables gv;
        
        // Use this for initialization
        void Start()
        {
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();

            if (gv.Language == 0)
            {
                GetComponent<SpriteRenderer>().sprite = englisch;
            }
            if (gv.Language == 1)
            {
                GetComponent<SpriteRenderer>().sprite = german;
            }
            gv.OnLanguageChange += ChangeSpriteLanguage;
        }

        private void ChangeSpriteLanguage(int language)
        {
            if (language == 0)
            {
                GetComponent<SpriteRenderer>().sprite = englisch;
            }
            if (language == 1)
            {
                GetComponent<SpriteRenderer>().sprite = german;
            }
        }
    }
}
