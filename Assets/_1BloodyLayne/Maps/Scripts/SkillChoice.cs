﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class SkillChoice : MonoBehaviour
    {
        public GameObject skillChoice, kristallkugel, deathPartikel, deathPartikel2, deathSound, selectedChoice, videoBackButton;
        public int skill;
        
        private Platformer2DUserControl userControl;
        private GameController gc;
        private GlobalVariables gv;
        private bool listen, isInit;
        private UnityAction method1;
        private UnityAction method2;
        private AudioSource _activationSound;
        
        // Use this for initialization
        void Awake()
        {
            if(isInit) return;
            _activationSound = GetComponent<AudioSource>();
            userControl = GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>();
            gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            foreach(SkillChoiceAutoText skillDescribtionText in skillChoice.GetComponentsInChildren<SkillChoiceAutoText>())
            {
                skillDescribtionText.GetComponent<SkillChoiceAutoText>().skill = skill;
                skillDescribtionText.GetComponent<SkillChoiceAutoText>().updateText();
            }
            Image[] skillImages = transform.parent.GetChild(0).GetComponentsInChildren<Image>();
            skillImages[1].sprite = gv.transform.GetChild(0).GetChild(skill).transform.Find("Image1").GetComponent<Image>().sprite;
            skillImages[3].sprite = gv.transform.GetChild(0).GetChild(skill).transform.Find("Image2").GetComponent<Image>().sprite;
            addSkillListener();
            isInit = true;
        }

        public void addSkillListener()
        {
            switch (skill)
            {
                case 0:
                    method1 = skill0choice1;
                    method2 = skill0choice2;
                    break;
                case 1:
                    method1 = skill1choice1;
                    method2 = skill1choice2;
                    break;
                case 2:
                    method1 = skill2choice1;
                    method2 = skill2choice2;
                    break;
                case 3:
                    method1 = skill3choice1;
                    method2 = skill3choice2;
                    break;
                case 4:
                    method1 = skill4choice1;
                    method2 = skill4choice2;
                    break;
                case 5:
                    method1 = skill5choice1;
                    method2 = skill5choice2;
                    break;
                case 6:
                    method1 = skill6choice1;
                    method2 = skill6choice2;
                    break;
                case 7:
                    method1 = skill7choice1;
                    method2 = skill7choice2;
                    break;
                case 8:
                    method1 = skill8choice1;
                    method2 = skill8choice2;
                    break;
                case 9:
                    method1 = skill9choice1;
                    method2 = skill9choice2;
                    break;
                case 10:
                    method1 = skill10choice1;
                    method2 = skill10choice2;
                    break;
                case 11:
                    method1 = skill11choice1;
                    method2 = skill11choice2;
                    break;
                case 12:
                    method1 = skill12choice1;
                    method2 = skill12choice2;
                    break;
                case 13:
                    method1 = skill13choice1;
                    method2 = skill13choice2;
                    break;
                default:
                    method1 = skill1choice1;
                    method2 = skill1choice2;
                    break;
            }
        }

        public void openVideoMenue(int option)
        {
            skillChoice.transform.Find("VideoPanel").gameObject.SetActive(true);
            selectedChoice = EventSystem.current.currentSelectedGameObject;
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(videoBackButton);
            videoBackButton.GetComponent<Button>().Select();
            Button confirmButton = skillChoice.transform.Find("VideoPanel").Find("Confirm").GetComponent<Button>();
            VideoPlayer videoPlayer = skillChoice.transform.Find("VideoPanel").Find("Video").Find("VideoPlayer").GetComponent<VideoPlayer>();
            if (option == 1)
            {
                confirmButton.onClick.RemoveAllListeners();
                confirmButton.onClick.AddListener(method1);
                videoPlayer.clip = gv.transform.GetChild(0).GetChild(skill).GetComponent<SkillClips>().clip1;
            }
            else
            {
                confirmButton.onClick.RemoveAllListeners();
                confirmButton.onClick.AddListener(method2);
                videoPlayer.clip = gv.transform.GetChild(0).GetChild(skill).GetComponent<SkillClips>().clip2;
            }
        }

        public void closeVideoMenue()
        {
            skillChoice.transform.Find("VideoPanel").gameObject.SetActive(false);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(selectedChoice);
            selectedChoice.GetComponent<Button>().Select();
        }

        public void skill0choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill0choice1 = true;
            gv.skill0Known = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().disableBloodmagic = false;
            skillChoiceFinished();
        }
        public void skill0choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill0choice2 = true;
            gv.skill0Known = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().disableBloodmagic = false;
            skillChoiceFinished();
        }
        public void skill1choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill1choice1 = true;
            gv.skill1Known = true;
            skillChoiceFinished();
        }
        public void skill1choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill1choice2 = true;
            gv.skill1Known = true;
            skillChoiceFinished();
        }
        public void skill2choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill2choice1 = true;
            gv.skill2Known = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().disableBloodmagic = false;
            skillChoiceFinished();
        }
        public void skill2choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill2choice2 = true;
            gv.skill2Known = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().disableBloodmagic = false;
            skillChoiceFinished();
        }
        public void skill3choice1()
        {
            PlatformerCharacter2D player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            player.skill3choice1 = true;
            player.GetComponent<PlayerStats>().maxBloodlust *= 2;
            player.ChangeBloodthirst(player.GetComponent<PlayerStats>().maxBloodlust, Guid.NewGuid());
            gv.skill3Known = true;
            skillChoiceFinished();
        }
        public void skill3choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill3choice2 = true;
            gv.skill3Known = true;
            skillChoiceFinished();
        }
        public void skill4choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill4choice1 = true;
            gv.skill4Known = true;
            skillChoiceFinished();
        }
        public void skill4choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill4choice2 = true;
            gv.skill4Known = true;
            skillChoiceFinished();
        }
        public void skill5choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill5choice1 = true;
            gv.skill5Known = true;
            skillChoiceFinished();
        }
        public void skill5choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill5choice2 = true;
            gv.skill5Known = true;
            skillChoiceFinished();
        }
        public void skill6choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill6choice1 = true;
            gv.skill6Known = true;
            skillChoiceFinished();
        }
        public void skill6choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill6choice2 = true;
            gv.skill6Known = true;
            skillChoiceFinished();
        }
        public void skill7choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill7choice1 = true;
            gv.skill7Known = true;
            skillChoiceFinished();
        }
        public void skill7choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill7choice2 = true;
            gv.skill7Known = true;
            skillChoiceFinished();
        }
        public void skill8choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill8choice1 = true;
            gv.skill8Known = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().disableBloodmagic = false;
            skillChoiceFinished();
        }
        public void skill8choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill8choice2 = true;
            gv.skill8Known = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().disableBloodmagic = false;
            skillChoiceFinished();
        }
        public void skill9choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill9choice1 = true;
            gv.skill9Known = true;
            skillChoiceFinished();
        }
        public void skill9choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill9choice2 = true;
            gv.skill9Known = true;
            skillChoiceFinished();
        }

        public void skill10choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill10choice1 = true;
            gv.skill10Known = true;
            skillChoiceFinished();
        }
        public void skill10choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill10choice2 = true;
            gv.skill10Known = true;
            skillChoiceFinished();
        }

        public void skill11choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill11choice1 = true;
            gv.skill11Known = true;
            skillChoiceFinished();
        }
        public void skill11choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill11choice2 = true;
            gv.skill11Known = true;
            skillChoiceFinished();
        }

        public void skill12choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill12choice1 = true;
            gv.skill12Known = true;
            skillChoiceFinished();
        }
        public void skill12choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill12choice2 = true;
            gv.skill12Known = true;
            skillChoiceFinished();
        }

        public void skill13choice1()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill13choice1 = true;
            gv.skill13Known = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().disableBloodmagic = false;
            skillChoiceFinished();
        }
        public void skill13choice2()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().skill13choice2 = true;
            gv.skill13Known = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().disableBloodmagic = false;
            skillChoiceFinished();
        }

        public void activateSkillUI(Button selectButton)
        {
            if(!isInit) Awake();
            _activationSound.PlayOneShot(_activationSound.clip);
            Time.timeScale = 0;
            PlatformerCharacter2D playerChar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            playerChar.CheckSoundLoopStates();
            skillChoice.SetActive(true);
            playerChar.StopBloodthirstDamage();
            playerChar.NewInvulnerable(9999, false, true);
            userControl.BlockInput = true;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PauseMenu>().skillChoiceCanvasObject = skillChoice;
            if (selectButton != null)
            {
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(selectButton.gameObject);
                selectButton.Select();
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(selectedChoice);
            }
        }

        private void skillChoiceFinished()
        {
            Time.timeScale = 1;
            checkSkillAchievement();
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PauseMenu>().skillChoiceCanvasObject = null;
            Cursor.visible = false;
            skillChoice.SetActive(false);
            Instantiate(deathPartikel, kristallkugel.transform.position, Quaternion.identity);
            Instantiate(deathPartikel2, kristallkugel.transform.position, Quaternion.identity);
            Instantiate(deathSound, kristallkugel.transform.position, Quaternion.identity);
            userControl.continuePress = true;
            userControl.BlockInput = false;
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().RestartBloodthirstDamage();
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().StopInvulnerable();
            Destroy(kristallkugel);
            Destroy(gameObject);
        }

        private void checkSkillAchievement()
        {
            if (gv.skill0Known && gv.skill1Known && gv.skill2Known && gv.skill3Known && gv.skill4Known && gv.skill5Known && gv.skill6Known && gv.skill7Known && gv.skill8Known && gv.skill9Known && gv.skill10Known && gv.skill11Known && gv.skill12Known && gv.skill13Known)
            {
                gc.SetAchievement("EVERY_ARTIFACT");
            }
        }
    }
}
