﻿using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using UnityEngine.Tilemaps;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class CoverWall : MonoBehaviour
    {
        public GameObject grid;
        ParticleSystem[] rain;
        private TopWall topWall;
        private InsideOutsideStatus statusScript;

        public Color imageColor = new Color(0f, 0, 0, 0f);
        private Color fadeColor = new Color(0f, 0, 0, 0f);
        public float fadeSpeed = 4;
        PlatformerCharacter2D larry;
        private Coroutine currentCoroutine = null;
        public bool onlyFadeWall, raining;

        private void Start()
        {
            larry = GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>();
            topWall = grid.GetComponent<TopWall>();
            statusScript = GameObject.Find("Vordergruende").GetComponent<InsideOutsideStatus>();
            if (raining)
            {
                rain = GameObject.Find("Regen").transform.GetComponentsInChildren<ParticleSystem>();
            }

        }
        // Update is called once per frame
        void Update()
        {
            if (topWall.fadeIn == true && statusScript.inside)
            {
                //grid.GetComponent<Tilemap>().color = imageColor;
                grid.GetComponent<Tilemap>().color = Color.Lerp(grid.GetComponent<Tilemap>().color, fadeColor, fadeSpeed);
                if (currentCoroutine != null)
                {
                    StopCoroutine(currentCoroutine);
                }
                currentCoroutine = StartCoroutine(Stop());
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player" && !statusScript.inside)
            {
                topWall.fadeIn = true;
                topWall.fadeOut = false;
                statusScript.inside = true;
                statusScript.outside = false;
                if (raining && !onlyFadeWall)
                {
                    for (int i = 0; i < rain.Length - 1; i++)//rain[2] is the background rain
                    {
                        ParticleSystem.MainModule settings = rain[i].GetComponent<ParticleSystem>().main;
                        settings.startColor = new ParticleSystem.MinMaxGradient(fadeColor);
                    }
                }
            }
            else if (statusScript.inside)
            {
                grid.GetComponent<Tilemap>().color = fadeColor;
            }
        }

        IEnumerator Stop()
        {
            yield return new WaitForSeconds(1f);
            topWall.fadeIn = false;
        }
    }
}
