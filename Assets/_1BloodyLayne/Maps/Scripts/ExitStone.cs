﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using Units.Player;
using UnityEditor;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class ExitStone : MonoBehaviour
    {
        public bool moving;
        public float damagerCheckLength = 2.8f, startDelay, directionSpeed = 1f;
        public bool startActivated, justOneStomp;
        int stompCounter;
        public GameObject upperLimit, lowerLimit, splatterEffect;
        bool larryKilled, enemyKilled;
        Rigidbody2D rb;
        PlatformerCharacter2D larry;
        Transform damagerCheck;
        AudioSource activationSound, movementLoop, hitSound;

        // Start is called before the first frame update
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            larry = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            damagerCheck = transform.Find("DamagerCheck");
            activationSound = damagerCheck.GetComponents<AudioSource>()[0];
            movementLoop = damagerCheck.GetComponents<AudioSource>()[1];
            hitSound = damagerCheck.GetComponents<AudioSource>()[2];
            if (startActivated)
            {
                StartCoroutine(waitForStartDelay());
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (moving)
            {
                rb.velocity = new Vector2(rb.velocity.x, directionSpeed);

                if (Physics2D.Raycast(damagerCheck.position, Vector2.right, damagerCheckLength, 1 << 8) && larry.GetComponent<Animator>().GetBool("Ground") && !larryKilled && directionSpeed < 0)
                {
                    larryKilled = true;
                    StartCoroutine(killLarry());
                }
                RaycastHit2D enemyHit = Physics2D.Raycast(damagerCheck.position, Vector2.right, damagerCheckLength, 1 << 9);
                if (enemyHit.collider != null && !enemyKilled && directionSpeed < 0 && !enemyHit.transform.GetComponent<EnemyStats>().dead)
                {
                    enemyKilled = true;
                    StartCoroutine(killEnemy(enemyHit.collider));
                }
                if (damagerCheck.position.y < lowerLimit.transform.position.y && directionSpeed < 0)
                {
                    checkChangeDirection();
                }
                else if (damagerCheck.position.y > upperLimit.transform.position.y && directionSpeed > 0)
                {
                    checkChangeDirection();
                }

            }
        }
        public void startStone()
        {
            StartCoroutine(waitForStartDelay());
        }

        public void raise()
        {
            activationSound.PlayDelayed(0);
            movementLoop.PlayDelayed(0);
            moving = true;
           checkChangeDirection();
        }

        void checkChangeDirection()
        {
            if (stompCounter < 2)
            {
                if (justOneStomp)
                {
                    stompCounter++;
                }
                directionSpeed *= -1;
            }
            else
            {
                stompCounter = 0;
                moving = false;
                rb.velocity = new Vector2(0, 0);
            }
        }

        IEnumerator waitForStartDelay()
        {
            yield return new WaitForSeconds(startDelay);
            raise();
        }

        IEnumerator killLarry()
        {
            larry.KillPlayer(DamageTypeToPlayer.Smashed);
            Instantiate(splatterEffect, new Vector3(larry.GetComponent<Transform>().position.x, larry.GetComponent<Transform>().position.y - 1, larry.GetComponent<Transform>().position.z), Quaternion.identity);
            hitSound.PlayDelayed(0);
            GetComponent<Collider2D>().enabled = false;
            yield return new WaitForSeconds(3.5f);
            GetComponent<Collider2D>().enabled = true;
            larryKilled = false;
        }

        IEnumerator killEnemy(Collider2D enemyHit)
        {
            System.Guid guid = System.Guid.NewGuid();
            enemyHit.transform.GetComponent<EnemyStats>().Hit(999, DamageTypeToEnemy.Smashed, guid, false);
            Instantiate(splatterEffect, new Vector3(enemyHit.GetComponent<Transform>().position.x, enemyHit.GetComponent<Transform>().position.y - 1, enemyHit.GetComponent<Transform>().position.z), Quaternion.identity);
            hitSound.PlayDelayed(0);
            GetComponent<Collider2D>().enabled = false;
            yield return new WaitForSeconds(3.5f);
            GetComponent<Collider2D>().enabled = true;
            enemyKilled = false;
        }
    }
}
