﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ObjektStats : MonoBehaviour {
    [FormerlySerializedAs("zerstoerbar")] public bool destructable;
    public int hitPoints;
    Animator animator;
    Rigidbody2D rb;
    AudioSource deathSound; //sollte immer der erste Sound sein
    
	void Start () {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        deathSound = GetComponents<AudioSource>()[0]; //sollte immer der erste Sound sein
    }

    public void addDamage(int damage)
    {
        if (destructable)
        {
            if(hitPoints - damage <= 0)
            {
                hitPoints = 0;
            }
            else
            {
                hitPoints = hitPoints - damage;
            }
            if (hitPoints <= 0)
            {
                StartCoroutine(death());
            }
        }
    }

    public void startDeath()
    {
        StartCoroutine(death());
    }

    IEnumerator death()
    {
        destructable = false;
        animator.SetTrigger("Death");
        rb.isKinematic = true;
        deathSound.PlayDelayed(0);
        gameObject.layer = 12;
        gameObject.tag = "Background";
        transform.Find("BloodEffects").GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(0.35f);
        Destroy(gameObject);
    }
}
