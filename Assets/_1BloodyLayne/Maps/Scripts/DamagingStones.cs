﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class DamagingStones : MonoBehaviour
    {
        Rigidbody2D rb;
        public GameObject deathparticles;
        AudioSource bounceSound;
        bool dead, bounced;
        // Start is called before the first frame update
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            bounceSound = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
            //rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + 0.1f);
            if (rb.velocity.magnitude < 0.2f && !dead && bounced)
            {
                dead = true;
                death();
            }
        }


        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Player") && rb.velocity.magnitude > 0.5f && other.transform.position.y < transform.position.y + 1f)
            {
                other.gameObject.GetComponent<PlatformerCharacter2D>().Hit(35, DamageTypeToPlayer.Smashed, other.contacts[0].point, gameObject, Guid.NewGuid());
                death();
            }
            if (other.gameObject.CompareTag("Ground") && rb.velocity.magnitude > 0.5f)
            {
                bounced = true;
                bounceSound.PlayDelayed(0);
            }
        }

        public void death()
        {
            Instantiate(deathparticles, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
