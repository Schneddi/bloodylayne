using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

public class TutorialPopUp : MonoBehaviour
{
    public float questionMarkAnimationSpeed;
    private GameObject tutorialMessage, tutorialButton;
    private Transform[] scalingObjects;
    private Platformer2DUserControl userControl;
    private PlatformerCharacter2D larry;
    private int questionMarkScaleDirection = 1;

    void Start()
    {
        tutorialMessage = transform.Find("Canvas").gameObject;
        tutorialButton = tutorialMessage.transform.Find("BackgroundImg").Find("ContinueButton").gameObject;
        larry = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
        userControl = GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>();
        scalingObjects = new[] { transform.Find("QuestionMark") , transform.Find("FlickerParticle"), transform.Find("BeamsParticles")};
    }

    private void Update()
    {
        if (scalingObjects[0].localScale.x > 0.35f && questionMarkScaleDirection == 1 || scalingObjects[0].localScale.x < 0.25f && questionMarkScaleDirection == -1)
        {
            questionMarkScaleDirection *= -1;
        }
        foreach (Transform scalingObject in scalingObjects)
        {
            Vector3 localScale = scalingObject.localScale;
            localScale = new Vector3(localScale.x + questionMarkAnimationSpeed * Time.deltaTime * questionMarkScaleDirection, localScale.y + questionMarkAnimationSpeed * Time.deltaTime * questionMarkScaleDirection, localScale .z + questionMarkAnimationSpeed * Time.deltaTime * questionMarkScaleDirection);
            scalingObject.localScale = localScale;
        }
        scalingObjects[2].Rotate(0,0,Time.deltaTime * 20);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player" && !tutorialMessage.activeSelf)
        {
            activateTutorialCanvas();
        }
    }

    public void activateTutorialCanvas()
    {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PauseMenu>().tutorialCanvas = gameObject;
        userControl.BlockInput = true;
        larry.StopBloodthirstDamage();
        larry.NewInvulnerable(9999, false, true);
        tutorialMessage.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(tutorialButton);
    }
    
    public void continueButton()
    {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PauseMenu>().tutorialCanvas = null;
        tutorialMessage.SetActive(false);
        userControl.continuePress = true;
        userControl.BlockInput = false;
        larry.RestartBloodthirstDamage();
        larry.StopInvulnerable();
    }
}
