using UnityEngine;

namespace Maps.HumanEnding
{
    public class SwitchBackground : MonoBehaviour
    {
        public GameObject[] mountainBackGrounds;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                foreach(GameObject mountainBackGround in mountainBackGrounds)
                {
                    mountainBackGround.SetActive(false);
                }
                Destroy(gameObject);
            }
        }
    }
}
