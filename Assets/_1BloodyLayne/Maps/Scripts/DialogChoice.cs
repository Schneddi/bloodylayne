﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityStandardAssets._2D
{
    public class DialogChoice : MonoBehaviour
    {
        public GameObject dialogChoices;
        public Dialogbox listenDialog;
        Platformer2DUserControl userControl;
        AudioSource klickSound;
        int language;
        GameObject gc, globalVariables;
        public bool listen, isActivated;
        public GameObject nextDialogOption1, nextDialogOption2, nextDialogOption3;
        private GameObject nextDialog;
        DialogChoiceManager dialogChoiceManager;
        Dialog dialog;
        // Use this for initialization
        void Start()
        {
            userControl = GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>();
            klickSound = GetComponent<AudioSource>();
            gc = GameObject.FindGameObjectWithTag("Global");
            dialogChoiceManager = GameObject.Find("Canvas DialogChoice System").GetComponent<DialogChoiceManager>();
            dialog = FindObjectOfType<Dialog>();
            listen = true;
            gc.GetComponent<GlobalVariables>().OnLanguageChange += ChangeLanguage;
        }

        // Update is called once per frame
        void Update()
        {
            if (listenDialog.finished && listen)
            {
                dialogChoices.SetActive(true);
                userControl.BlockInput = true;
                isActivated = true;
                dialogChoiceManager.registerSkillChoice(dialogChoices);
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(dialogChoices.transform.Find("Choice1").gameObject);
                listen = false;
            }
        }

        private void ChangeLanguage(int language)
        {
            this.language = language;
        }

        private void dialogChoiceFinished()
        {
            if (nextDialog != null)
            {
                Cursor.visible = false;
                userControl.continuePress = true;
                dialog.ZeigeBox(nextDialog);
            }
            else
            {
                userControl.continuePress = true;
                userControl.BlockInput = false;
                Cursor.visible = false;
            }
            dialogChoices.SetActive(false);
            isActivated = false;
            dialogChoiceManager.unregisterSkillChoice();
        }

        public void dialogChoice1()
        {
            nextDialog = nextDialogOption1;
            dialogChoiceFinished();
        }

        public void dialogChoice2()
        {
            nextDialog = nextDialogOption2;
            dialogChoiceFinished();
        }

        public void dialogChoice3()
        {
            nextDialog = nextDialogOption3;
            dialogChoiceFinished();
        }
    }
}