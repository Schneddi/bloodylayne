﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructable : MonoBehaviour {

    public AudioClip[] strongBounceSounds, weakBounceSounds;
    AudioSource audioSource;
    Rigidbody2D rb;
    float startingY;


    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        startingY = transform.position.y;
        audioSource = GetComponents<AudioSource>()[1];
    }

    // Update is called once per frame

    private void Update()
    {
        if (transform.position.y < startingY - 150)
        {
            Destroy(gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player") && !audioSource.isPlaying && other.relativeVelocity.magnitude > 15)
        {
            playStrongBounce();
        }
        else
        {
            if(rb.velocity.magnitude > 3 && !audioSource.isPlaying)
            {
                playWeakBounce();
            }
        }
    }

    void playStrongBounce()
    {
        if (strongBounceSounds.Length > 0)
        {
            int randomSoundIndex = Random.Range(0, strongBounceSounds.Length);
            audioSource.clip = strongBounceSounds[randomSoundIndex];
            audioSource.PlayDelayed(0);
        }
        else
        {
            int randomSoundIndex = Random.Range(0, weakBounceSounds.Length);
            audioSource.clip = weakBounceSounds[randomSoundIndex];
            audioSource.PlayDelayed(0);
        }
    }

    void playWeakBounce()
    {
        if (weakBounceSounds.Length > 0)
        {
            int randomSoundIndex = Random.Range(0, weakBounceSounds.Length);
            audioSource.clip = weakBounceSounds[randomSoundIndex];
            audioSource.PlayDelayed(0);
        }
        else
        {
            int randomSoundIndex = Random.Range(0, strongBounceSounds.Length);
            audioSource.clip = strongBounceSounds[randomSoundIndex];
            audioSource.PlayDelayed(0);
        }
    }
}
