﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TruheTrigger : MonoBehaviour
{
    Truhe truheScript;
    // Start is called before the first frame update
    void Start()
    {
        truheScript = transform.parent.GetComponent<Truhe>();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            truheScript.OpenChest();
        }
    }
}
