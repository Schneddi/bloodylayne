﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UnityStandardAssets._2D
{
    public class YellBack : MonoBehaviour
    {
        public short dialog;
        public GameObject jaeger;
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player" && jaeger != null)
            {
                if (dialog == 0)
                {
                    jaeger.GetComponent<Jaeger>().yellBack1();
                }
                if (dialog == 1)
                {
                    jaeger.GetComponent<Jaeger>().yellBack2();
                }
                if (dialog == 2)
                {
                    jaeger.GetComponent<Jaeger>().yellBack3();
                }
            }
            Destroy(gameObject);
        }
    }
}
