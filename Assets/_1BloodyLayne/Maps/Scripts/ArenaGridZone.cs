using System.Collections;
using UnityEngine;
using UnityStandardAssets._2D;

namespace Maps.Objects.Gitter
{
    public class ArenaGridZone : MonoBehaviour
    {
        [SerializeField] private GridReworked grid;
        [Tooltip("If true, this will raise the grid, else the grid will get lowered.")]
        [SerializeField] private bool open;
        [Tooltip("This will make the attached Grid open/close on proximity. The open value will be ignored.")]
        [SerializeField] private bool autoDoor;
        [Tooltip("The sound made when the zone is triggered.")]
        private AudioSource _triggerSound;
        // Use this for initialization
        void Start()
        {
            _triggerSound = GetComponent<AudioSource>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name != "Player") return;
            
            if (open || autoDoor)
            {
                if (grid.RaiseUp || grid.risen) return;
                _triggerSound.PlayDelayed(0);
                grid.raiseGrid();
            }
            else
            {
                if (grid.FallDown || grid.fallen) return;
                _triggerSound.PlayDelayed(0);
                grid.lowerGrid();
            }
        }
        
        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.name != "Player" || !autoDoor) return;
            
            if (grid.FallDown || grid.fallen) return;
            _triggerSound.PlayDelayed(0);
            grid.lowerGrid();
        }
    }
}
