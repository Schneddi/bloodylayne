﻿using System.Collections;
using System.Collections.Generic;
using Units.Enemys;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class GitterCastlestart : MonoBehaviour
    {
        public bool hochziehen, enemyKilled;
        public GameObject reactionEnemy;
        float startY;
        AudioSource zugSound, fallImpactSound;

        // Use this for initialization
        void Start()
        {
            startY = transform.position.y;
            zugSound = GetComponents<AudioSource>()[0];
            fallImpactSound = GetComponents<AudioSource>()[1];
        }

        // Update is called once per frame
        void Update()
        {
            if (hochziehen)
            {
                if (!zugSound.isPlaying)
                {
                    zugSound.PlayDelayed(0);
                }
                transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f * Time.deltaTime, transform.position.z);
                if (transform.position.y > startY + 3)
                {
                    zugSound.Stop();
                    fallImpactSound.PlayDelayed(0);
                    hochziehen = false;
                }
            }
            else if (!enemyKilled)
            {
                if (reactionEnemy.GetComponent<EnemyStats>().dead)
                {
                    hochziehen = true;
                    enemyKilled = true;
                }
            }
        }
    }
}
