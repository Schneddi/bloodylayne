﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class StoneDestroyer : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if ((other.gameObject.GetComponent("DamagingStones") as DamagingStones) != null)
            {
                other.gameObject.GetComponent<DamagingStones>().death();
            }
        }
    }
}
