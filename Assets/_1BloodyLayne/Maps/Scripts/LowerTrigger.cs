﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class LowerTrigger : MonoBehaviour
    {
        public GameObject ChalkEntrance;
        public bool activateEntrance = false;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (activateEntrance)
            {
                ChalkEntrance.GetComponent<LowerCaveEntrance>().lower = true;
                Destroy(gameObject);
            }
        }
    }
}