using Unity.Mathematics;
using UnityEngine;

using Random = UnityEngine.Random;

public class LightFlicker : MonoBehaviour
{
    [Tooltip("The Speed of lightning change")]
    public float speed;
    [Tooltip("How much the lightning is changing at minimum/maximum")]
    public float intensityAdjustmentCap; 
    [Tooltip("The minimal lightning change per frame * deltaTime")]
    public float randomMin=0.5f;
    
    private float startIntensity;
    private UnityEngine.Rendering.Universal.Light2D m_Lightsource;
    private Transform player;
    
    // Start is called before the first frame update
    void Start()
    {
        m_Lightsource = GetComponent<UnityEngine.Rendering.Universal.Light2D>();
        startIntensity = m_Lightsource.intensity;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(transform.position, player.position) < 40) //ist der Spieler in der Nähe?
        {
            if (math.distance(startIntensity, m_Lightsource.intensity) > intensityAdjustmentCap)
            {
                if (m_Lightsource.intensity > startIntensity)
                {
                    speed = -math.abs(speed);
                }
                else
                {
                    speed = math.abs(speed);
                }
            }

            m_Lightsource.intensity += speed * Time.deltaTime * Random.Range(randomMin, 1f);
        }
        else
        {
            m_Lightsource.intensity = 0;
        }
    }
}