﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class SpikeStoneTrigger : MonoBehaviour
    {
        public bool isStopTrigger;
        public GameObject[] stones;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                foreach (GameObject stone in stones)
                {
                    if (!isStopTrigger)
                    {
                        if (!stone.GetComponent<SpikeStone>().moving)
                        {
                            stone.GetComponent<SpikeStone>().StartStone();
                        }
                    }
                    else
                    {
                        if (!stone.GetComponent<SpikeStone>().moving)
                        {
                            stone.GetComponent<SpikeStone>().StopStone();
                        }
                    }
                }
                Destroy(gameObject);
            }
        }
    }
}
