﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UnityStandardAssets._2D
{
    public class StoneSpawner : MonoBehaviour
    {
        public GameObject stone;
        GameObject playerCheckArea, playerCheckArea2;
        Collider2D checkCollider, checkCollider2;
        Transform larry;
        // Start is called before the first frame update
        void Start()
        {
            playerCheckArea = GameObject.Find("SpawnerPlayerCheckArea");
            playerCheckArea2 = GameObject.Find("SpawnerPlayerCheckArea2");
            checkCollider = playerCheckArea.GetComponent<Collider2D>();
            checkCollider2 = playerCheckArea2.GetComponent<Collider2D>();
            larry = GameObject.FindWithTag("Player").transform;
            StartCoroutine(SpawnStone());
        }

        IEnumerator SpawnStone()
        {
            larry.position = larry.position;
            if (checkCollider.bounds.Contains(larry.transform.position) || checkCollider2.bounds.Contains(larry.transform.position))
            {
                Instantiate(stone, transform.position, Quaternion.identity);
            }
            yield return new WaitForSeconds(2f);
            StartCoroutine(SpawnStone());
        }
    }
}
