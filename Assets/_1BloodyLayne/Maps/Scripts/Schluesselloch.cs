﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Schluesselloch : MonoBehaviour {
    public GameObject Gitter;
    public bool hatSchluessel = false;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player" && hatSchluessel && !Gitter.GetComponent<GitterBossVorraum>().hochziehen)
        {
            Gitter.GetComponent<GitterBossVorraum>().hochziehen = true;
            GetComponent<AudioSource>().PlayDelayed(0);
        }
    }
}
