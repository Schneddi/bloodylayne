using System;
using System.Collections;
using Units.Enemys;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Restarter_own : MonoBehaviour
    {
        public GameObject splatter, splatterKlein;
        public bool spikes, killZone, smashKillOnGrounded;
        public int attackDamage, upwardsForce;
        
        private AudioSource hitSound, killSound;
        private bool spikesOnColldown, larryKilledCooldown;
        private PlatformerCharacter2D playerChar;

        private void Start()
        {
            playerChar = GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>();
            if (spikes || smashKillOnGrounded)
            {
                hitSound = GetComponents<AudioSource>()[0];
                killSound = GetComponents<AudioSource>()[1];
            }
        }

        private void Update()
        {
            if (spikes && GetComponent<Collider2D>().IsTouching(playerChar.GetComponent<Collider2D>()) && playerChar.GetComponent<PlayerStats>().health > 0 && !spikesOnColldown && !larryKilledCooldown && !playerChar.GetComponent<PlatformerCharacter2D>().dead)
            {
                spikesOnColldown = true;
                StartCoroutine(spikeCooldownStart(0.1f));
                Vector2 midwayPoint = (transform.position + playerChar.transform.position)/2;
                playerChar.Hit(attackDamage, DamageTypeToPlayer.Normal,new Vector2(0, upwardsForce), midwayPoint, gameObject, Guid.NewGuid());
                if (playerChar.dead)
                {
                    killSound.PlayDelayed(0);
                }
                else
                {
                    hitSound.PlayDelayed(0);
                }
                if (playerChar.GetComponent<PlayerStats>().health <= 0)
                {
                    larryKilledCooldown = true;
                    StartCoroutine(killCooldownStart(5f));
                }
                Instantiate(splatterKlein, new Vector3(playerChar.GetComponent<Transform>().position.x, playerChar.GetComponent<Transform>().position.y - 1, playerChar.GetComponent<Transform>().position.z), Quaternion.identity);
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                if (spikes && !spikesOnColldown && !killZone && !other.GetComponent<PlatformerCharacter2D>().dead)
                {
                    spikesOnColldown = true;
                    StartCoroutine(spikeCooldownStart(0.3f));
                    ContactPoint2D[] contacts = new ContactPoint2D[1];
                    other.GetContacts(contacts);
                    Vector2 contactPoint = contacts[0].point;
                    if (other.GetComponent<PlayerStats>().health - attackDamage <= 0 || (smashKillOnGrounded && other.GetComponent<PlatformerCharacter2D>().grounded))
                    {
                        int damage = attackDamage;
                        if(smashKillOnGrounded && other.GetComponent<PlatformerCharacter2D>().grounded) damage = other.GetComponent<PlayerStats>().health;
                        killSound.PlayDelayed(0);
                        other.GetComponent<PlatformerCharacter2D>().Hit(damage, DamageTypeToPlayer.ImpaledFeet, new Vector2(0, 5), contactPoint, gameObject, Guid.NewGuid());
                        Instantiate(splatter, new Vector3(other.GetComponent<Transform>().position.x, other.GetComponent<Transform>().position.y - 1, other.GetComponent<Transform>().position.z), Quaternion.identity);
                    }
                    else
                    {
                        hitSound.PlayDelayed(0);
                        other.GetComponent<PlatformerCharacter2D>().Hit(attackDamage, DamageTypeToPlayer.ImpaledFeet, new Vector2(0, upwardsForce), contactPoint, gameObject, Guid.NewGuid());
                        Instantiate(splatterKlein, new Vector3(other.GetComponent<Transform>().position.x, other.GetComponent<Transform>().position.y - 1, other.GetComponent<Transform>().position.z), Quaternion.identity);
                    }
                }
                else if (killZone)
                {
                    other.GetComponent<PlatformerCharacter2D>().KillPlayer(DamageTypeToPlayer.Normal);
                }
                else if (smashKillOnGrounded && other.GetComponent<PlatformerCharacter2D>().grounded)
                {
                    Debug.Log("1");
                    ContactPoint2D[] contacts = new ContactPoint2D[1];
                    other.GetContacts(contacts);
                    Vector2 contactPoint = contacts[0].point;
                    int damage = attackDamage;
                    if(smashKillOnGrounded && other.GetComponent<PlatformerCharacter2D>().grounded) damage = other.GetComponent<PlayerStats>().health;
                    killSound.PlayDelayed(0);
                    other.GetComponent<PlatformerCharacter2D>().Hit(damage, DamageTypeToPlayer.Smashed, new Vector2(0, 5), contactPoint, gameObject, Guid.NewGuid());
                    Instantiate(splatter, new Vector3(other.GetComponent<Transform>().position.x, other.GetComponent<Transform>().position.y - 1, other.GetComponent<Transform>().position.z), Quaternion.identity);
                }
            }
            if (other.gameObject.CompareTag("Enemy"))
            {
                if (other.GetComponent<EnemyStats>().isHuman || other.GetComponent<EnemyStats>().isAnimal)
                {
                    int currentEnemyHealth = other.GetComponent<EnemyStats>().health;
                    if (spikes && currentEnemyHealth > 0)
                    {
                        Vector3 enemyPosition = other.GetComponent<Transform>().position;
                        Instantiate(splatter, new Vector3(enemyPosition.x, enemyPosition.y - 1, enemyPosition.z), Quaternion.identity);
                        Guid guid = Guid.NewGuid();
                        other.GetComponent<EnemyStats>().Hit(currentEnemyHealth, DamageTypeToEnemy.Impaled, guid, false);
                    }
                    else if (smashKillOnGrounded && currentEnemyHealth > 0)
                    {
                        Vector3 enemyPosition = other.GetComponent<Transform>().position;
                        Instantiate(splatter, new (enemyPosition.x, enemyPosition.y - 1, enemyPosition.z), Quaternion.identity);
                        Guid guid = Guid.NewGuid();
                        other.GetComponent<EnemyStats>().Hit(currentEnemyHealth, DamageTypeToEnemy.Smashed, guid, false);
                    }
                }
            }
        }
        
        IEnumerator spikeCooldownStart(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            spikesOnColldown = false;
        }

        IEnumerator killCooldownStart(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            larryKilledCooldown = false;
        }
    }
}
