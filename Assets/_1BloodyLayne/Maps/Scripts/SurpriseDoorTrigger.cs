﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace UnityStandardAssets._2D
{
    public class SurpriseDoorTrigger : MonoBehaviour
    {
        public GameObject[] doors, posXDoors, negXDoors, enemies;
        bool moving, activated;
        public float distance, yDirection, xSpeed;
        float startY, posStartX, negStartX;

        void Start()
        {
            if (doors.Length != 0)
            {
                startY = doors[0].transform.position.y;
            }
            if (posXDoors.Length != 0)
            {
                posStartX = posXDoors[0].transform.position.x;
            }
            if (negXDoors.Length != 0)
            {
                negStartX = negXDoors[0].transform.position.x;
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player" && !activated)
            {
                foreach (GameObject door in doors)
                {
                    if (door.GetComponent<Collider2D>() != null)
                    {
                        door.GetComponent<Collider2D>().enabled = false;
                    }
                }
                foreach (GameObject door in posXDoors)
                {
                    if (door.GetComponent<Collider2D>() != null)
                    {
                        door.GetComponent<Collider2D>().enabled = false;
                    }
                }
                foreach (GameObject door in negXDoors)
                {
                    if (door.GetComponent<Collider2D>() != null)
                    {
                        door.GetComponent<Collider2D>().enabled = false;
                    }
                }
                GetComponent<AudioSource>().PlayDelayed(0);
                activated = true;
                moving = true;
                foreach (GameObject enemy in enemies)
                {
                    enemy.SetActive(true);
                }
            }
        }

        void Update()
        {
            if (moving)
            {
                foreach (GameObject door in doors)
                {
                    door.GetComponent<Rigidbody2D>().velocity = new Vector3(door.GetComponent<Rigidbody2D>().velocity.x, yDirection);
                }
                foreach (GameObject door in posXDoors)
                {
                    door.GetComponent<Rigidbody2D>().velocity = new Vector3(xSpeed, door.GetComponent<Rigidbody2D>().velocity.y);
                }
                foreach (GameObject door in negXDoors)
                {
                    door.GetComponent<Rigidbody2D>().velocity = new Vector3(-xSpeed, door.GetComponent<Rigidbody2D>().velocity.y);
                }
                if (doors.Length != 0)
                {
                    if (Mathf.Abs(startY - doors[0].transform.position.y) > Mathf.Abs(distance))
                    {
                        stopDoors();
                    }
                }
                if (posXDoors.Length != 0)
                {
                    if (Mathf.Abs(posStartX - posXDoors[0].transform.position.x) > Mathf.Abs(distance))
                    {
                        stopDoors();
                    }
                }
                if (negXDoors.Length != 0)
                {
                    if (Mathf.Abs(negStartX - negXDoors[0].transform.position.x) > Mathf.Abs(distance))
                    {
                        stopDoors();
                    }
                }
            }
        }

        private void stopDoors()
        {
            moving = false;
            foreach (GameObject door in doors)
            {
                door.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0);
                door.GetComponent<TilemapRenderer>().sortingLayerName = "2Map";
            }
            foreach (GameObject door in posXDoors)
            {
                door.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0);
                door.GetComponent<TilemapRenderer>().sortingLayerName = "2Map";
            }
            foreach (GameObject door in negXDoors)
            {
                door.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0);
                door.GetComponent<TilemapRenderer>().sortingLayerName = "2Map";
            }
            gameObject.SetActive(false);
        }
    }
}
