﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GitterBossVorraum : MonoBehaviour {
    public bool hochziehen = false;
    float startY;
    AudioSource zugSound, endSound;

    // Use this for initialization
    void Start()
    {
        startY = transform.position.y;
        zugSound = GetComponents<AudioSource>()[0];
        endSound = GetComponents<AudioSource>()[1];
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (hochziehen)
        {
            if (!zugSound.isPlaying)
            {
                zugSound.PlayDelayed(0);
            }
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.03f, transform.position.z);
            if (transform.position.y > startY + 5)
            {
                endSound.PlayDelayed(0);
                zugSound.Stop();
                this.enabled = false;
            }
        }
    }
}
