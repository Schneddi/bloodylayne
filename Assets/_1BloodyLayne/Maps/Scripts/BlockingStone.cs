using UnityEngine;

namespace _1BloodyLayne.Maps.Berge
{
    public class BlockingStone : MonoBehaviour
    {
        private AudioSource _collisionSound;

        void Start()
        {
            _collisionSound = GetComponent<AudioSource>();
        }
    
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Ground"))
            {
                _collisionSound.PlayDelayed(0);
            }
        }
    }
}
