﻿using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using Units.Player.Scripts.PlayerControl.PlatformCharacter2D;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class Plattformbewegung_1 : MonoBehaviour
    {

        public GameObject startpos;
        public GameObject endpos;
        public float speed = 0.3f;
        public bool active;
        public bool bewegtSichImmer;
        private Vector3 _direction;
        public AudioSource startSound;
        private Rigidbody2D _rb;
        private float _usedSpeed;
        private Collider2D _playerCollider, _platformCollider;
        private PlatformerCharacter2D _playerMainScript;
        
        [ReadOnly] public bool playerOnPlatform;

        // Use this for initialization
        void Start()
        {
            transform.position = startpos.transform.position;
            _direction = (endpos.transform.position - startpos.transform.position).normalized;
            startSound = GetComponent<AudioSource>();
            _rb = GetComponent<Rigidbody2D>();
            _usedSpeed = speed * 14;
            GameObject player = GameObject.FindWithTag("Player");
            _platformCollider = GetComponent<Collider2D>();
            _playerCollider = player.GetComponent<Collider2D>();
            _playerMainScript = player.GetComponent<PlatformerCharacter2D>();
            
            if (bewegtSichImmer)
            {
                active = true;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (active)
            {
                if ((endpos.transform.position - transform.position).magnitude > (startpos.transform.position - endpos.transform.position).magnitude || _direction == Vector3.zero)
                {
                    transform.position = startpos.transform.position;
                    _direction = (endpos.transform.position - startpos.transform.position).normalized;
                    if (!playerOnPlatform && !bewegtSichImmer && !_platformCollider.IsTouching(_playerCollider) && Vector2.Distance(transform.position, startpos.transform.position) < 2)
                    {
                        _direction = Vector3.zero;
                        active = false;
                    }
                }
                else if ((startpos.transform.position - transform.position).magnitude > (startpos.transform.position - endpos.transform.position).magnitude)
                {
                    transform.position = endpos.transform.position;
                    _direction = (startpos.transform.position - endpos.transform.position).normalized;
                    if (!playerOnPlatform && !bewegtSichImmer && !_platformCollider.IsTouching(_playerCollider) && Vector2.Distance(transform.position, startpos.transform.position) < 2)
                    {
                        _direction = Vector3.zero;
                        active = false;
                    }
                }
                _rb.velocity = _direction * _usedSpeed;
                if (playerOnPlatform)
                {
                    _playerMainScript.additionalPlatformVelocity = _rb.velocity;
                }
            }
        }
        
    }
}
