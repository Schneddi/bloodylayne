﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Felsen_respawn : MonoBehaviour {

    private Vector2 origTransform;
  

    private Rigidbody2D m_Rigidbody2D;
  


    // Use this for initialization
    void Start () {

        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        origTransform = transform.position;
      
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
     if(other.gameObject.tag =="Killzone")
            {
            transform.position = origTransform;
            m_Rigidbody2D.velocity = new Vector2(0f, 0.1f);
        }
    }
}
