﻿using System;
using Units.Enemys;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Restarter_Top : MonoBehaviour
    {
        public GameObject splatter, splatterKlein;
        public int damage = 175;
        private AudioSource hitSound, killSound;

        public void Start()
        {
            if (splatter != null && splatterKlein != null)
            {
                hitSound = GetComponents<AudioSource>()[0];
                killSound = GetComponents<AudioSource>()[1];
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                ContactPoint2D[] contacts = new ContactPoint2D[1];
                other.GetContacts(contacts);
                Vector2 contactPoint = contacts[0].point;
                if (splatter != null && other.GetComponent<PlayerStats>().health - damage <= 0)
                {
                    killSound.PlayDelayed(0);
                    other.GetComponent<PlatformerCharacter2D>().Hit(damage, DamageTypeToPlayer.Normal, new Vector2(0, -5), contactPoint, gameObject, Guid.NewGuid());
                    Instantiate(splatter, new Vector3(other.GetComponent<Transform>().position.x, other.GetComponent<Transform>().position.y - 1, other.GetComponent<Transform>().position.z), Quaternion.identity);
                }
                else if (splatterKlein != null)
                {
                    hitSound.PlayDelayed(0);
                    other.GetComponent<PlatformerCharacter2D>().Hit(damage, DamageTypeToPlayer.Normal, new Vector2(0, -20), contactPoint, gameObject, Guid.NewGuid());
                    Instantiate(splatterKlein, new Vector3(other.GetComponent<Transform>().position.x, other.GetComponent<Transform>().position.y - 1, other.GetComponent<Transform>().position.z), Quaternion.identity);
                }
                else
                {
                    other.GetComponent<PlatformerCharacter2D>().Hit(999, DamageTypeToPlayer.Normal, contactPoint, gameObject, Guid.NewGuid());
                }
            }
            if (other.gameObject.CompareTag("Enemy"))
            {
                if (other.GetComponent<EnemyStats>().isHuman || other.GetComponent<EnemyStats>().isAnimal)
                {
                    if (splatter != null)
                    {
                        Instantiate(splatter, new Vector3(other.GetComponent<Transform>().position.x, other.GetComponent<Transform>().position.y - 1, other.GetComponent<Transform>().position.z), Quaternion.identity);
                    }

                    System.Guid guid = System.Guid.NewGuid();
                    other.GetComponent<EnemyStats>().Hit(999, 0, guid, false);
                }
            }
        }
    }
}
