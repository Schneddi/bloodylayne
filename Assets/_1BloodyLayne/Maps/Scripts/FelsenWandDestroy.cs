﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class FelsenWandDestroy : MonoBehaviour {

    public GameObject objtodestroy, destroyedGround;
    private AudioSource fallingStonesSound;

    private void Start()
    {
        fallingStonesSound = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "Player")
        {
            Destroy(objtodestroy);
            destroyedGround.GetComponent<Rigidbody2D>().constraints =
                RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
            fallingStonesSound.PlayDelayed(0);
        }
    }
}
