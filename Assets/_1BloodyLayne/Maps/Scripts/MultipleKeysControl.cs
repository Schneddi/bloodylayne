using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

namespace Maps.ServantEnding
{
    public class MultipleKeysControl : MonoBehaviour {
        public GameObject pickUpParticleSystem;
        [FormerlySerializedAs("fiveKeysTrigger")] public FiveKeysTrigger keysTrigger;
        public Action OnPickedUp; 
    
        private AudioSource collectSound;
    
        private void Start()
        {
            if (keysTrigger == null)
            {
                keysTrigger = GameObject.Find("KeyTrigger").GetComponent<FiveKeysTrigger>();
            }
            collectSound = GetComponent<AudioSource>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                OnPickedUp?.Invoke();
                keysTrigger.collectedKeys += 1;
                StartCoroutine(Death());
            }
        }

        IEnumerator Death()
        {
            Instantiate(pickUpParticleSystem, transform.position, Quaternion.identity, null);
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<ParticleSystem>().Stop(false, ParticleSystemStopBehavior.StopEmitting);
            collectSound.PlayDelayed(0);
            yield return new WaitForSeconds(3f);
            Destroy(gameObject);
        }
    }
}