using System;
using System.Collections.Generic;
using UnityEngine;

namespace Maps.Doodads
{
    [ExecuteInEditMode]
    public class TileMapSelectionInfo : MonoBehaviour
    {
        [SerializeField]
        public string brush;
        [SerializeField]
        public int sortingLayer;
        [SerializeField]
        public int orderInLayer;
        [SerializeField]
        public Color tileMapColor;
        [SerializeField]
        public float minScale;
        [SerializeField]
        public float emptyCellsAfterDraw;
        [SerializeField]
        public float layerOffset;
        [SerializeField]
        public List<TileMapSelectionInfo> relatedTilemaps;
    }
}
