using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using Units.Enemys;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;
using Random = UnityEngine.Random;

public class DevEnd1Control : MonoBehaviour
{
    public GameObject pope, colosseumSpawner, startDialogFinish, collosseumEntryDialog, popeProvokeDialog, popeDeathDialog, bloodSkill13Effect;
    [FormerlySerializedAs("camera")] public GameObject cameraRef;
    public GameObject[] firstKills, secondKills, thirdKills, rubbleObjects,colloseumStageKillObjects;
    public float initialKillingDelay = 1;
    public int dev2SceneNumber;
    public bool testLastStage;
    public Transform bloodrainPosition;

    private Transform popeColosseumPos, larryColloseumPos, popeLastPos, firstScenePos, rubbleSpawn;
    private GameObject player;
    private Dialog dialog;
    private bool listenForStartFinish, listenForColloseumFight, lastDialog, listenForOutro, endFightStart, popeDying; //should have used a stage or something
    private GunnerTrap gunnerTrap;
    private EnemyStats popeStats;
    private AudioSource earthQuakeSound;
    private PlatformerCharacter2D playerChar;
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerChar = player.GetComponent<PlatformerCharacter2D>();
        playerChar.GetComponent<PlayerStats>().health = playerChar.GetComponent<PlayerStats>().maxHealth;
        gunnerTrap = colosseumSpawner.GetComponent<GunnerTrap>();
        popeStats = pope.GetComponent<EnemyStats>();
        popeColosseumPos = transform.Find("PopeColosseumPos");
        larryColloseumPos = transform.Find("LarryColloseumPos");
        popeLastPos = transform.Find("PopeLastPos");
        firstScenePos = transform.Find("FirstScenePos");
        rubbleSpawn = transform.Find("RubbleSpawn");
        dialog = FindObjectOfType<Dialog>();
        earthQuakeSound = GetComponents<AudioSource>()[0];
    if (testLastStage)
        {
            player.transform.position = larryColloseumPos.position;
        }
        else
        {
            player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }

    private void Update()
    {
        if (startDialogFinish.activeSelf)
        {
            listenForStartFinish = true;
        }
        
        if (!startDialogFinish.activeSelf && listenForStartFinish)
        {
            listenForStartFinish = false;
            StartCoroutine(PlayerEntrance());
        }
        if (collosseumEntryDialog.activeSelf)
        {
            listenForColloseumFight = true;
        }
        
        if (!collosseumEntryDialog.activeSelf && listenForColloseumFight)
        {
            listenForColloseumFight = false;
            colosseumSpawner.GetComponent<GunnerSpawner>().customActivation();
            gunnerTrap.customActivation();
        }

        if (!popeProvokeDialog.activeSelf && lastDialog)
        {
            popeStats.unverwundbar = false;
        }

        if (gunnerTrap.battleFinished && !lastDialog && !playerChar.dead)
        {
            playerChar.NewInvulnerable(9999, false, true);
            StartCoroutine(provokeDialog());
        }

        if (popeStats.health <= 0 && !popeDying && !playerChar.dead)
        {
            StartCoroutine(deathDialog());
            popeDying = true;
        }
        
        if (!popeDeathDialog.activeSelf && listenForOutro && !playerChar.dead)
        {
            listenForOutro = false;
            StartCoroutine(endingSequence());
        }
    }

    private IEnumerator PlayerEntrance()
    {
        player.GetComponent<Platformer2DUserControl>().BlockInput = true;
        player.GetComponent<PlatformerCharacter2D>().NewInvulnerable(9999, false, true);
        cameraRef.GetComponent<Camera2DFollow>().SetDamping(1);
        cameraRef.GetComponent<Camera2DFollow>().target = firstScenePos;
        yield return new WaitForSeconds(1f);
        StartCoroutine(player.GetComponent<PlatformerCharacter2D>().Skill13u1());
        player.GetComponent<Animator>().SetBool("HoldTransitions", true);
        player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        yield return new WaitForSeconds(0.1f);
        player.GetComponent<PlatformerCharacter2D>().EvaluateInput(Vector2.zero, false, false, true, false, false, Vector2.zero);
        yield return new WaitForSeconds(initialKillingDelay - 0.1f);
        Instantiate(bloodSkill13Effect,bloodrainPosition.position, Quaternion.identity);
        System.Guid guid = System.Guid.NewGuid();
        foreach (GameObject firstKill in firstKills)
        {
            firstKill.GetComponent<EnemyStats>().Hit(99999, DamageTypeToEnemy.Explosion, guid, false);
            firstKill.GetComponent<EnemyStats>().Hit(99999, DamageTypeToEnemy.Explosion, guid, false);
        }
        cameraRef.GetComponent<Camera2DFollow>().target = player.transform;
        yield return new WaitForSeconds(0.25f);
        player.GetComponent<Animator>().SetBool("HoldTransitions", false);
        foreach (GameObject secondKill in secondKills)
        {
            secondKill.GetComponent<EnemyStats>().Hit(99999, DamageTypeToEnemy.Explosion, guid, false);
            secondKill.GetComponent<EnemyStats>().Hit(99999, DamageTypeToEnemy.Explosion, guid, false);
        }
        yield return new WaitForSeconds(0.25f);
        foreach (GameObject thirdKill in thirdKills)
        {
            thirdKill.GetComponent<EnemyStats>().Hit(99999, DamageTypeToEnemy.Explosion, guid, false);
            thirdKill.GetComponent<EnemyStats>().Hit(99999, DamageTypeToEnemy.Explosion, guid, false);
        }
        yield return new WaitForSeconds(1f);
        player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        player.GetComponent<Platformer2DUserControl>().BlockInput = false;
        player.GetComponent<PlatformerCharacter2D>().StopInvulnerable();
        player.GetComponent<PlatformerCharacter2D>().InitializeSkills(true);
        cameraRef.GetComponent<Camera2DFollow>().ResetDamping();
        pope.transform.position = popeColosseumPos.position;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "Player" && !endFightStart)
        {
            endFightStart = true;
            StartCoroutine(popePointing());
        }
    }

    private IEnumerator provokeDialog()
    {
        yield return new WaitForSeconds(2f);
        pope.transform.position = popeLastPos.position;
        pope.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        player.transform.position = larryColloseumPos.position;
        player.GetComponent<Platformer2DUserControl>().BlockInput = true;
        if (!player.GetComponent<PlatformerCharacter2D>().m_FacingRight)
        {
            player.GetComponent<PlatformerCharacter2D>().Flip();
        }
        yield return new WaitForSeconds(2f);
        lastDialog = true;
        dialog.ZeigeBox(popeProvokeDialog);
    }

    private IEnumerator deathDialog()
    {
        player.GetComponent<Platformer2DUserControl>().BlockInput = true;
        if (!player.GetComponent<PlatformerCharacter2D>().m_FacingRight)
        {
            player.GetComponent<PlatformerCharacter2D>().Flip();
        }
        pope.GetComponent<Animator>().SetTrigger("Wounded");
        player.transform.position = larryColloseumPos.position;
        yield return new WaitForSeconds(1f);
        dialog.ZeigeBox(popeDeathDialog);
        listenForOutro = true;
    }

    private IEnumerator popePointing()
    {
        pope.transform.position = popeColosseumPos.position;
        cameraRef.GetComponent<Camera2DFollow>().SetDamping(1);
        cameraRef.GetComponent<Camera2DFollow>().target = pope.transform;
        foreach (GameObject colloseumStageKillObject in colloseumStageKillObjects)
        {
            Destroy(colloseumStageKillObject);
        }
        yield return new WaitForSeconds(1f);
        pope.GetComponent<Animator>().SetTrigger("Pointing");
        yield return new WaitForSeconds(1f);
        player.transform.position = larryColloseumPos.position;
        dialog.ZeigeBox(collosseumEntryDialog);
    }

    private IEnumerator endingSequence()
    {
        yield return new WaitForSeconds(4f);
        pope.GetComponent<Animator>().SetTrigger("DeathTrigger");
        pope.GetComponent<Animator>().SetBool("Death", true);
        cameraRef.GetComponent<Camera2DFollow>().ShakeCamera(100f,1f, 60);
        earthQuakeSound.PlayDelayed(0);
        for (int i = 0; i < 10; i++)
        {
            int randomRubble = Random.Range(0, rubbleObjects.Length);
            float randomXPos = Random.Range(-12, 12);
            Vector2 randomPos = new Vector2(rubbleSpawn.position.x + randomXPos, rubbleSpawn.position.y);
            Instantiate(rubbleObjects[randomRubble], randomPos, quaternion.identity);
            yield return new WaitForSeconds(0.2f);
        }
        yield return new WaitForSeconds(4f);
        GameObject.Find("GameController").transform.GetComponent<GameController>().NextLevel(dev2SceneNumber);
    }
}
