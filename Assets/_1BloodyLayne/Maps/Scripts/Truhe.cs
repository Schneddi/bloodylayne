﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Truhe : MonoBehaviour
{
    Animator animator;
    AudioSource openSound, lootSound;
    public GameObject lootItem;
    Transform itemSpawnposition;
    bool opened;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        openSound = GetComponents<AudioSource>()[0];
        lootSound = GetComponents<AudioSource>()[1];
        itemSpawnposition = transform.Find("ItemSpawnPosition");
    }

    public void OpenChest()
    {
        if (opened == false)
        {
            opened = true;
            animator.SetTrigger("Open");
            openSound.PlayDelayed(0);
            if (lootItem != null)
            {
                StartCoroutine(SpawnItemWithDelay());
            }
        }
    }



    IEnumerator SpawnItemWithDelay()
    {
        lootSound.PlayDelayed(0);
        yield return new WaitForSeconds(0.5f);
        GameObject item = Instantiate(lootItem, itemSpawnposition.position, Quaternion.identity);
        item.GetComponent<PowerUp>().startFadedOut = true;
        item.GetComponent<PowerUp>().spawn();
        lootItem = null;
    }
}
