using UnityEngine;

namespace Maps.Objects.Gitter
{
    public class GridReworked : MonoBehaviour {
        public bool risen;
        public bool fallen;
        public float speed;
        public bool isUpsideDown;
    
        public bool RaiseUp {get;private set; }
        public bool FallDown {get;private set; }
    
        private AudioSource[] sounds;
        private AudioSource raiseLoop, fallSound;
        private Transform bottom, upperLimit, lowerLimit;

        // Use this for initialization
        void Start()
        {
            sounds = GetComponents<AudioSource>();
            raiseLoop = sounds[0];
            fallSound = sounds[1];
            bottom = transform.Find("Bottom");
            upperLimit = transform.Find("UpperLimit");
            lowerLimit = transform.Find("LowerLimit");
            upperLimit.transform.parent = transform.parent;
            lowerLimit.transform.parent = transform.parent;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (FallDown)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - speed * 2 * Time.deltaTime, transform.position.z);
                if (bottom.position.y < lowerLimit.position.y)
                {
                    FallDown = false;
                    fallen = true;
                    risen = false;
                    if (!isUpsideDown)
                    {
                        fallSound.PlayDelayed(0);
                    }
                    else
                    {
                        fallSound.PlayDelayed(0);
                        raiseLoop.Stop();
                    }
                }
            }
            else if (RaiseUp)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + speed * Time.deltaTime, transform.position.z);
                if (bottom.position.y > upperLimit.position.y)
                {
                    risen = true;
                    fallen = false;
                    RaiseUp = false;
                    if (!isUpsideDown)
                    {
                        raiseLoop.Stop();
                        fallSound.PlayDelayed(0);
                    }
                    else
                    {
                        fallSound.PlayDelayed(0);
                    }
                }
            }
        }

        public void raiseGrid()
        {
            raiseLoop.PlayDelayed(0);
            fallen = false;
            risen = false;
            if (!isUpsideDown)
            {
                RaiseUp = true;
            }
            else
            {
                FallDown = true;
            }
        }
    
        public void lowerGrid()
        {
            fallen = false;
            risen = false;
            if (!isUpsideDown)
            {
                FallDown = true;
            }
            else
            {
                RaiseUp = true;
            }
        }
    }
}