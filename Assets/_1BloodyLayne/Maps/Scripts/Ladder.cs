﻿using Units.Enemys;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace _1BloodyLayne.Control
{
    public class Ladder : MonoBehaviour {
    
        private PlatformerCharacter2D _thePlayer;

        // Use this for initialization
        void Start () {
            _thePlayer = FindObjectOfType<PlatformerCharacter2D>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if(other.name =="Player")
            {  
                _thePlayer.onLadder = true;
            }
            if (other.gameObject.CompareTag("Enemy") && other.GetComponent<EnemyStats>() != null)
            {
                other.GetComponent<EnemyStats>().onLadder = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.name == "Player" || other.name == "PlayerBat")
            {
                _thePlayer.onLadder = false;
            }
            if (other.gameObject.CompareTag("Enemy") && other.GetComponent<EnemyStats>() != null)
            {
                other.GetComponent<EnemyStats>().onLadder = false;
            }
        }
    }
}
