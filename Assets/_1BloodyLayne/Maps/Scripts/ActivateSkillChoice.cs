﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class ActivateSkillChoice : MonoBehaviour //name ist irreführend, die Klasse isActivated lediglich einen Bauern
    {
        public GameObject activationTarget, reactionTarget;
        bool listen;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (reactionTarget.activeSelf)
            {
                listen = true;
            }
            if (!reactionTarget.activeSelf && listen)
            {
                activationTarget.GetComponent<MistgabelBauer>().enabled = true;
                gameObject.SetActive(false);
            }
        }
    }
}
