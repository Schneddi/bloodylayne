﻿using System;
using Units.Enemys;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class RespawnBoden : MonoBehaviour
    {
        PlatformerCharacter2D Player;
        private void Start()
        {
            Player = GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player" && Player.GetComponent<PlayerStats>().health > 0)
            {
                Player.Respawn2();
            }
            if (other.gameObject.CompareTag("Enemy"))
            {
                System.Guid guid = System.Guid.NewGuid();
                other.GetComponent<EnemyStats>().Hit(999, DamageTypeToEnemy.Impaled, guid, false);
            }
            if (other.gameObject.CompareTag("Object"))
            {
                other.GetComponent<ObjektStats>().startDeath();
            }
        }
    }
}
