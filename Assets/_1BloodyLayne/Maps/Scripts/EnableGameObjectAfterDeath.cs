using UnityEngine;

namespace Maps
{
    public class EnableGameObjectAfterDeath : MonoBehaviour
    {
        [SerializeField] private HumanDeathSequence[] listenToDeathObjects;
        [SerializeField] private GameObject enableThisAfterDeath;

        private void Awake()
        {
            foreach (HumanDeathSequence listenToDeath in listenToDeathObjects)
            {
                listenToDeath.DeathEvent += UnitDied;
            }
        }
        
        private void UnitDied(HumanDeathSequence deadEnemy)
        {
            enableThisAfterDeath.SetActive(true);
            foreach (HumanDeathSequence listenToDeath in listenToDeathObjects)
            {
                listenToDeath.DeathEvent -= UnitDied;
            }
        }

        private void OnDisable()
        {
            foreach (HumanDeathSequence listenToDeath in listenToDeathObjects)
            {
                listenToDeath.DeathEvent -= UnitDied;
            }
        }
    }
}
