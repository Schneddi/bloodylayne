﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class ActivateObject : MonoBehaviour //name ist irreführend, die Klasse isActivated lediglich einen Bauern
    {
        public GameObject activationTarget, reactionTarget;
        bool listen;

        // Update is called once per frame
        void Update()
        {
            if (reactionTarget.activeSelf)
            {
                listen = true;
            }
            if (!reactionTarget.activeSelf && listen)
            {
                activationTarget.GetComponent<Animator>().SetTrigger("Death");
                gameObject.SetActive(false);
            }
        }
    }
}
