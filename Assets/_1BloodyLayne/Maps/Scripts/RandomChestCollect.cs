using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomChestCollect : MonoBehaviour
{
    public Truhe[] chests;
    public GameObject key;
    // Start is called before the first frame update
    void Start()
    {
        int randomChest = Random.Range(0,chests.Length);
        chests[randomChest].lootItem = key;
    }
}