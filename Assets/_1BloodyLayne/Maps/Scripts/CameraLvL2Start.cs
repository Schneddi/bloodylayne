﻿
using System.Collections;
using System.Collections.Generic;
using UI.PlayerUI;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class CameraLvL2Start : MonoBehaviour
    {
        public GameObject reactionTarget;
        bool listen;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (reactionTarget.activeSelf)
            {
                listen = true;
            }
            if (!reactionTarget.activeSelf && listen)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target = GameObject.FindGameObjectWithTag("Player").transform;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().SetDamping(0.2f);
                StartCoroutine(CameraLock());
            }
        }

            IEnumerator CameraLock()
            {
                yield return new WaitForSeconds(0.5f);
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().ResetDamping();
                gameObject.SetActive(false);
            }
        }
}
