﻿using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
	public class AutosaveZone : MonoBehaviour {

        [SerializeField] private bool multisave, noParticles; //if true, save will not disable after player walks through, enabling switching between two or more savepoints
        
        [SerializeField] private Transform mainEffect;
        [SerializeField] private Color activatedColorMin, activatedColorMax;
        [SerializeField] private float animationTime, heightChangeSpeed;

        private Transform _playerPosition;
        private bool _isActiveRespawn;
        private float _activationTime;
        private ParticleSystem[] _pSystems;
        private BoxCollider2D _savingZoneCollider;
        private AudioSource _activationSound, _respawnSound;

        private void Start()
        {
	        _playerPosition = GameObject.FindWithTag("Player").transform;
	        _pSystems = GetComponentsInChildren<ParticleSystem>();
	        _savingZoneCollider = GetComponent<BoxCollider2D>();
	        _activationSound = GetComponents<AudioSource>()[0];
	        _respawnSound = GetComponents<AudioSource>()[1];
	        if (noParticles)
	        {
		        foreach (ParticleSystem pSystem in _pSystems)
		        {
			        pSystem.gameObject.SetActive(false);
		        }
	        }
        }

        private void Update()
        {
	        //change Effect Position to match Player y pos
	        Vector3 mainEffectPosition = mainEffect.position;
	        float newMainEffectYPosition = mainEffectPosition.y + (_playerPosition.position.y - mainEffectPosition.y) * Time.deltaTime * heightChangeSpeed;
	        Vector3 newMainEffectPosition = new (mainEffectPosition.x, newMainEffectYPosition, mainEffectPosition.z);
	        if (newMainEffectYPosition > _savingZoneCollider.transform.position.y + _savingZoneCollider.offset.y - _savingZoneCollider.size.y/2 && newMainEffectYPosition < _savingZoneCollider.transform.position.y + _savingZoneCollider.offset.y + _savingZoneCollider.size.y/2)
	        {
		        mainEffect.position = newMainEffectPosition;
	        }

	        if (_isActiveRespawn)
	        {
		        float animationProgress = (Time.time - _activationTime)/animationTime;
		        if (animationProgress > 1 && _pSystems[0].main.startColor.colorMin == activatedColorMin)
		        {
			        return;
		        }
		        mainEffect.rotation = Quaternion.Euler(new Vector3(0,0,360 * animationProgress));
		        
		        foreach (ParticleSystem pSystem in _pSystems)
		        {
			        ParticleSystem.Particle[] particles = new ParticleSystem.Particle[pSystem.main.maxParticles];
			        
			        int numParticlesAlive = pSystem.GetParticles(particles);
			        
			        for (int i = 0; i < numParticlesAlive; i++)
			        {
				        particles[i].startColor = Color32.Lerp(particles[i].startColor, activatedColorMin, animationProgress/20);
			        }
			        
			        pSystem.SetParticles(particles, numParticlesAlive);
		        }
	        }
        }

        private void OnTriggerEnter2D(Collider2D other)
		{
			if (other.name == "Player" && (!_isActiveRespawn || multisave))
			{
				PlatformerCharacter2D playerChar = other.GetComponent<PlatformerCharacter2D>();
				if (playerChar.LastRespawnPoint == gameObject)
				{
					return;
				}

				if (!noParticles)
				{
					_activationSound.PlayOneShot(_activationSound.clip);
				}
				playerChar.RespawnPosition = transform.position;
				_activationTime = Time.time;
				_isActiveRespawn = true;
				for (int i = 0; i< _pSystems.Length;i++)
				{
					ParticleSystem.MinMaxGradient mainStartColor = _pSystems[i].main.startColor;
					mainStartColor.colorMin = activatedColorMin;
					mainStartColor.colorMax = activatedColorMax;
			        
					ParticleSystem.MainModule mainModule = _pSystems[i].main;
					mainModule.startColor = mainStartColor;
				}

				if (playerChar.LastRespawnPoint != null && !playerChar.LastRespawnPoint.GetComponent<AutosaveZone>().multisave)
				{
					Destroy(playerChar.LastRespawnPoint);
				}

				playerChar.LastRespawnPoint = gameObject;
			}
		}

        public void PlayRespawnEffect()
        {
	        _respawnSound.PlayOneShot(_respawnSound.clip);
	        _activationTime = Time.time;
        }
	}
}
