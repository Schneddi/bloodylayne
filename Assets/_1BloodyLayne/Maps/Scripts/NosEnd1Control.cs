using System;
using System.Collections;
using System.Collections.Generic;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class NosEnd1Control : MonoBehaviour
    {
        public GameObject listenDialog, servant;
        public float servantSpeed = 5;
        private bool waitForDialog, walkingServant;
        private Rigidbody2D servantRb;

        private void Start()
        {
            servantRb = servant.GetComponent<Rigidbody2D>();
            GameObject.FindWithTag("Player").GetComponent<PlayerStats>().health = GameObject.FindWithTag("Player").GetComponent<PlayerStats>().maxHealth;
        }

        private void FixedUpdate()
        {
            if (walkingServant)
            {
                servantRb.velocity = new Vector2(servantSpeed, 0);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (listenDialog.activeSelf)
            {
                waitForDialog = true;
            }
            if (!listenDialog.activeSelf && waitForDialog)
            {
                waitForDialog = false;
                StartCoroutine(despawnServant());
            }
        }
        
        IEnumerator despawnServant()
        {
            Vector3 theScale = servant.transform.localScale;
            theScale.x *= -1;
            servant.transform.localScale = theScale;
            servant.GetComponent<Animator>().SetTrigger("Walk");
            walkingServant = true;
            GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>().InitializeSkills(true);
            GameObject.FindWithTag("Player").GetComponent<PlayerStats>().health = GameObject.FindWithTag("Player").GetComponent<PlayerStats>().maxHealth;
            yield return new WaitForSeconds(2);
            Destroy(servant);
            Destroy(gameObject);
        }
    }
}