using System;
using System.Collections;
using System.Collections.Generic;
using Maps.Objects.Gitter;
using UnityEngine;

public class FiveKeysTrigger : MonoBehaviour
{
    public GridReworked grid;
    public int keyAmount = 5;
    public int collectedKeys;
    private AudioSource activateSound;

    private void Start()
    {
        activateSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player" && collectedKeys >= keyAmount)
        {
            grid.raiseGrid();
            activateSound.PlayDelayed(0);
            Destroy(gameObject);
        }
    }
}
