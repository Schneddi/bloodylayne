using System.Collections;
using System.Collections.Generic;
using Maps.Objects.Gitter;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class LeverControl : MonoBehaviour
    {
        public GridReworked[] grids;
        public bool raise, lower;
        bool used;
        AudioSource pullLeverSound;
        Animator leverAnim;

        void Start()
        {
            pullLeverSound = GetComponents<AudioSource>()[0];
            leverAnim = GetComponent<Animator>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player" && !used)
            {
                leverAnim.SetTrigger("SwitchToActivated");
                used = true;
                foreach (GridReworked grid in grids)
                {
                    if (raise)
                    {
                        grid.raiseGrid();
                    }
                    else if (lower)
                    {
                        grid.lowerGrid();
                    }
                }
                pullLeverSound.PlayDelayed(0);
            }
        }
        
    }
}