﻿using System.Collections;
using System.Collections.Generic;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class Regen : MonoBehaviour
    {
        Transform playerTransform;
        PlatformerCharacter2D playerScript;
        bool respawning;
        // Start is called before the first frame update
        void Start()
        {
            playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
            playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
        }

        // Update is called once per frame
        void Update()
        {
            if (playerTransform.position.y < transform.position.y-50)
            {
                transform.position = new Vector2(playerTransform.position.x, playerTransform.position.y + 25);
            }
            else if (playerTransform.position.y > transform.position.y-15)
            {
                transform.position = new Vector2(playerTransform.position.x, playerTransform.position.y + 25);
            }
            else
            {
                transform.position = new Vector2(playerTransform.position.x, transform.position.y);
            }
            if (playerScript.GetComponent<PlayerStats>().health == 0 &&  !respawning)
            {
                respawning = true;
                StartCoroutine(Respawn());
            }
            else
            {
                respawning = false;
            }
        }

        IEnumerator Respawn()
        {
            yield return new WaitForSeconds(1f);
            transform.position = new Vector2(playerScript.RespawnPosition.x, transform.position.y);
        }
    }
}
