using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

using UnityEngine.Serialization;

public class LightDimming : MonoBehaviour
{
    public bool size;
    public bool startDimming = true;

    public float speed;
    public float intensityAim;
    private UnityEngine.Rendering.Universal.Light2D m_Lightsource;
    // Start is called before the first frame update
    void Start()
    {
        m_Lightsource = GetComponent<UnityEngine.Rendering.Universal.Light2D>();
        if (intensityAim < m_Lightsource.intensity)
        {
            speed = -math.abs(speed);
        }
        else
        {
            speed = math.abs(speed);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (startDimming)
        {
            if (!size)
            {
                if (m_Lightsource.intensity != intensityAim)
                {
                    if (math.distance(m_Lightsource.intensity, intensityAim) < math.abs(speed * Time.deltaTime))
                    {
                        m_Lightsource.intensity = intensityAim;
                        startDimming = false;
                    }
                    else
                    {
                        m_Lightsource.intensity += speed * Time.deltaTime;
                    }
                }
            }
            else
            {
                if (m_Lightsource.pointLightOuterRadius != intensityAim)
                {
                    if (math.distance(m_Lightsource.pointLightOuterRadius, intensityAim) <
                        math.abs(speed * Time.deltaTime))
                    {
                        m_Lightsource.pointLightOuterRadius = intensityAim;
                        startDimming = false;
                    }
                    else
                    {
                        m_Lightsource.pointLightOuterRadius += speed * Time.deltaTime;
                    }
                }
            }
        }
    }
}