﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnityStandardAssets._2D
{
    public class SzenenuebergangJaeger : MonoBehaviour
    {
        public GameObject Jaeger;
        GameController gc;
        public int[] sceneNumbers;
        GlobalVariables gv;
        private int selectedScene;

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player" && GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>().health > 0)
            {
                if (Jaeger != null)
                {
                    StartCoroutine(SadEnd());
                }
                else
                {
                    gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
                    selectedScene = selectScene();
                    gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
                    gc.NextLevel(selectedScene);
                }
            }
        }

        IEnumerator SadEnd()
        {
            Jaeger.GetComponent<Jaeger>().tearsOfSadness();
            yield return new WaitForSeconds(1f);
            Jaeger.GetComponent<Animator>().SetTrigger("Cry");
            yield return new WaitForSeconds(2.8f);
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            selectedScene = selectScene();
            gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
            gc.NextLevel(selectedScene);
        }

        private int selectScene()
        {
            int sceneInt=0;
            int randomArraySelector;
            bool sceneSelected = false;
            if (sceneNumbers.Length > 0 && !sceneSelected)
            {
                randomArraySelector = Random.Range(0, sceneNumbers.Length);
                if (gv.noRandomLvL)
                {
                    randomArraySelector = 0;
                }
                sceneInt = sceneNumbers[randomArraySelector];
            }
            return sceneInt;
        }
    }
}