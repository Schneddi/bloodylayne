﻿using Units.Player.Scripts.PlayerControl.PlatformCharacter2D;
using UnityEngine;
using UnityStandardAssets._2D;

namespace Maps.Berge
{
    public class PlatformToPlayer : MonoBehaviour
    {
        public Plattformbewegung_1 pScript;
        public bool waitForPlatformToBeActive;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (waitForPlatformToBeActive && !pScript.active) return;
            if (other.name == "Player")
            {
                if (!pScript.active)
                {
                    pScript.startSound.PlayDelayed(0);
                }
                pScript.active = true;
                pScript.playerOnPlatform = true;
                other.transform.parent = transform.parent;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (waitForPlatformToBeActive && !pScript.active) return;
            if (other.name == "Player" || other.name == "PlayerBat")
            {
                other.transform.parent = null;
                other.gameObject.GetComponent<PlatformerCharacter2D>().additionalPlatformVelocity = Vector2.zero;
                pScript.playerOnPlatform = false;
            }
        }
    }
}
