﻿using System.Collections;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class FlipLarry : MonoBehaviour
    {
        PlatformerCharacter2D larry;
        public bool lookToTheLeft, lookbehind;
        // Use this for initialization
        void Start()
        {
            larry = GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>();
            if (lookToTheLeft && larry.m_FacingRight)
            {
                larry.Flip();
            }else if (!lookToTheLeft && !larry.m_FacingRight && !lookbehind)
            {
                larry.Flip();
            }
            if (lookbehind)
            {
                StartCoroutine(lookBehind());
            }
        }

        IEnumerator lookBehind()
        {
            larry.Flip();
            yield return new WaitForSeconds(0.6f);
            larry.Flip();
        }
    }
}
