﻿using System.Collections;
using UnityEngine;
using UnityEngine.Video;

namespace _1BloodyLayne.Control
{
    public class ChangeScene : MonoBehaviour
    {
        VideoPlayer _vPlayer;
        void Start()
        {
            _vPlayer = GetComponent<VideoPlayer>();
            Cursor.visible = false;
            StartCoroutine(MovieTransition());
        }

        IEnumerator MovieTransition()
        {
            yield return new WaitForSeconds((float)(_vPlayer.clip.length));
            Cursor.visible = true;
            GameObject.Find("GameController").transform.GetComponent<GameController>().StartNewGame();
        }
    }
}