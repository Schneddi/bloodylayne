﻿using System;
using Units.Enemys;
using UnityEngine;
using UnityEngine.Serialization;

namespace _1BloodyLayne.Control
{
    public class GlobalVariables : MonoBehaviour
    {
        public bool developerBuild;
        public bool steamBuild;
        public int difficulty; //0=english, 1=german ----- 0=very easy, 1=easy, 2=normal, 3=hard, 4=very hard
        public int humansCount, humansKilled, humanKilledLocal, humanCountLocal; // local mean only in this Level
        public int dragonEndingScore;
        public int playerSkin; //0=default,1=human,2=nosferatu, 3=demon
        public int gender; // 0=male, 1=female
        public bool lightningEffects, noRandomLvL, hudOn, forcedInvulnerable;
        public float volume = 1, musicVolume = 0.1f;
        public static GlobalVariables Instance;
        public bool skill0Known, skill1Known, skill2Known, skill3Known, skill4Known, skill5Known, skill6Known, skill7Known, skill8Known, skill9Known, skill10Known, skill11Known, skill12Known, skill13Known;
        public bool firstPlaythroughDone, blackMageIntroDone, nosferatuIntroDone, werewolfIntroDone;
        public bool blackMageKilled, jaegerEncountered, jaegerKilled, assassinEncountered, assassinKilled, fartedToDeath;
        public bool aimAssist = true, aimVisualization = true;
        public bool displayChangeNumbers = true;
        [FormerlySerializedAs("statChangeNumber")] public GameObject healthChangeNumber;
        public bool difficultyIsSetup;
        public CurrentControlDevice currentControlDevice = CurrentControlDevice.MouseKeyboard;

        public event Action OnBindingsChange;
        public event OnVariableChangeDelegate OnLanguageChange;
        public delegate void OnVariableChangeDelegate(int newVal);
        
        public int subtitles;

        private int _language;
        public int Language
        {
            get
            {
                return _language;
            }
            set
            {
                if (_language == value) return;
                _language = value;
                if (OnLanguageChange != null)
                    OnLanguageChange(_language);
            }
        }

        // Use this for initialization
        void Awake()
        {
            {
                //If we don't currently have globalvariables...
                if (Instance == null)
                    //...set this one to be it...
                    Instance = this;
                //...otherwise...
                else if (Instance != this)
                    //...destroy this one because it is a duplicate.
                    Destroy(gameObject);
            }
            DontDestroyOnLoad(transform.gameObject);
        }

        public void SetupEnemyDifficulty()
        {
            EnemyStats[] enemys = FindObjectsOfType<EnemyStats>(true);
            foreach (EnemyStats enemy in enemys)
            {
                enemy.SetupDifficulty(difficulty);
            }
            difficultyIsSetup = true;
        }

        private void CountHumans()
        {
            EnemyStats[] enemys = FindObjectsOfType<EnemyStats>(true);
            foreach (EnemyStats enemy in enemys)
            {
                if (enemy.isHuman)
                {
                    humanCountLocal++;
                }
            }
        }

        public void StartNewHumanCount()
        {
            humanKilledLocal = 0;
            humanCountLocal = 0;
            CountHumans();
            humansCount += humanCountLocal;
        }

        public void InvokeBindingsDisplayChangedEvent()
        {
            if (OnBindingsChange != null) OnBindingsChange.Invoke();
        }
    }
    
    public enum CurrentControlDevice{
        MouseKeyboard,
        Gamepad,
        XInputController
    }
}
