﻿using UnityEngine;

namespace _1BloodyLayne.Control
{
	public class DestroyWithDelay : MonoBehaviour {
		public float delay;

		void Start () {
			Destroy(gameObject, delay);
		}
	}
}