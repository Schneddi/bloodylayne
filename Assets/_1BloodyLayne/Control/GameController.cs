﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Steamworks;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets._2D;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace _1BloodyLayne.Control
{
    public class GameController : MonoBehaviour
    {
        public int loadScreenScene;
        public InputActionAsset inputActionAsset;
        public float loadingProgress;

        public int scene = 5;
        public static HealthUI PlayerHealthUI;
        
        private bool _loaded, _mainMenu, _gameOver, _firstLevel, _normalLevel;
        private AsyncOperation _asyncLoadLevel;
        private Dialog _dialog;
        private bool _humanEndingScene, _servantEndingScene, _vampireEndingScene, _devilEndingScene;
        private string _saveString;
        private Image[] _originalHudImages;
        private GlobalVariables _gv;
        private Coroutine _loadingScreenRoutine;
        private List<InputBinding> _addedBindings = new();

        void Awake()
        {
            Cursor.visible = false;
            scene = SceneManager.GetActiveScene().buildIndex;
            DontDestroyOnLoad(transform.gameObject);
            int? languageCheck = PlayerPrefs.GetInt("sprache");
            _gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            if (languageCheck.GetValueOrDefault() != 0)
            {
                _gv.Language = PlayerPrefs.GetInt("sprache");
            }
            int? subtitleCheck = PlayerPrefs.GetInt("subtitles");
            if (subtitleCheck.GetValueOrDefault() != 0)
            {
                _gv.subtitles = PlayerPrefs.GetInt("subtitles");
            }
            _gv.lightningEffects = PlayerPrefs.GetInt("lightningEffects", 1) == 1;
            _gv.hudOn = PlayerPrefs.GetInt("hudOn", 1) == 1;
            _gv.forcedInvulnerable = PlayerPrefs.GetInt("forcedInvulnerable", 0) == 1;
            _gv.noRandomLvL = PlayerPrefs.GetInt("noRandomLvL", 0) == 1;
            _gv.aimAssist = PlayerPrefs.GetInt("aimAssist", 1) == 1;
            _gv.aimVisualization = PlayerPrefs.GetInt("aimVisualization", 1) == 1;
            _gv.displayChangeNumbers = PlayerPrefs.GetInt("displayChangeNumbers", 1) == 1;
            _gv.volume = PlayerPrefs.GetFloat("volume", 1f);
            AudioListener.volume = PlayerPrefs.GetFloat("volume", 1f);
            _gv.musicVolume = PlayerPrefs.GetFloat("musicVolume", 0.35f);
            _gv.difficulty = PlayerPrefs.GetInt("difficulty", 2);
            _gv.SetupEnemyDifficulty();
            
            if (_gv.forcedInvulnerable)
            {
                TurnOnInvulnerable();
            }

            if (scene != 1 && scene != 2 && scene != 3) //loadingScreen and deathScreen dont need an UI
            {
                if (PlayerPrefs.HasKey("playerBindings"))
                {
                    LoadControlOverrides(inputActionAsset, "playerBindings");
                }
            }
        }

        void Update()
        {
            if (loadingProgress == 1)
            { 
                if (_loaded)
                {
                    _loaded = false;
                    loadingProgress = 0;
                    Debug.Log("loading complete");
                    if (_firstLevel)
                    {
                        _dialog = FindObjectOfType<Dialog>();
                        _dialog.ZeigeBox();
                        GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().BlockInput = true;
                        GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().SetTrigger("Liegt");
                        GameObject.Find("LvL1Control").GetComponent<LvL1Control>().setTutorialDone(PlayerPrefs.GetInt("firstPlaythroughDone") == 1);
                    }
                    if (_normalLevel)
                    {
                        _normalLevel = false;
                        InitLvLVars();
                        if (_firstLevel)
                        {
                            _firstLevel = false;
                        }
                        else
                        {
                            if (_saveString.Equals("defaultSave"))
                            {
                                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Save>().defaultSave();
                            }
                        }
                        _gv.StartNewHumanCount();

                        if (_humanEndingScene)
                        {
                            GameObject.Find("Nosferatu").GetComponent<Nosferatu>().humanEndingScene = true;
                        }
                        if (_servantEndingScene)
                        {
                            GameObject.Find("Nosferatu").GetComponent<Nosferatu>().servantEndingScene = true;
                        }
                        if (_vampireEndingScene)
                        {
                            GameObject.Find("Nosferatu").GetComponent<Nosferatu>().vampireEndingScene = true;
                        }
                        if (_devilEndingScene)
                        {
                            GameObject.Find("Nosferatu").GetComponent<Nosferatu>().devilEndingScene = true;
                        }
                        if (scene == 14)
                        {
                            GameObject.Find("Nosferatu").GetComponent<Nosferatu>().StartScene();
                        }
                        Cursor.visible = false;
                    }
                    if (_mainMenu)
                    {
                        _mainMenu = false;

                    }
                    if (_gameOver)
                    {
                        _gameOver = false;
                    }
                    Destroy(gameObject);
                }
                if (_asyncLoadLevel.isDone)
                {
                    _asyncLoadLevel.allowSceneActivation = true;
                    _loaded = true;
                    Time.timeScale = 1;
                }
            }
        }

        public void NextLevel(int scene)
        {
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Save>().defaultSave();
            _normalLevel = true;
            this.scene = scene;
            _saveString = "defaultSave";
            _gv.playerSkin = PlayerPrefs.GetInt("playerSkin" + _saveString, 0);
            _gv.gender = PlayerPrefs.GetInt("gender" + _saveString, 0);
            _loadingScreenRoutine ??= StartCoroutine(LoadScreen(this.scene));
        }

        public void LevelLaden()
        {
            int? sceneCheck = PlayerPrefs.GetInt("scene");
            if (sceneCheck.GetValueOrDefault() != 0)
            {
                if (PlayerPrefs.GetInt("scene") == 4)
                {
                    StartNewGame();
                }
                else
                {
                    _saveString = "defaultSave";
                    _normalLevel = true;
                    _gv.playerSkin = PlayerPrefs.GetInt("playerSkin" + _saveString, 0);
                    _gv.gender = PlayerPrefs.GetInt("gender" + _saveString, 0);
                    scene = PlayerPrefs.GetInt("scene");
                    _loadingScreenRoutine ??= StartCoroutine(LoadScreen(scene));
                }
            }
        }

        private void InitLvLVars()
        {
            _gv.difficulty = PlayerPrefs.GetInt("difficulty" + _saveString, 2);
            _gv.SetupEnemyDifficulty();
            PlatformerCharacter2D playerChar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            PlayerStats playerStats = playerChar.GetComponent<PlayerStats>();
            playerChar.skill0choice1 = PlayerPrefs.GetInt("skill0choice1" + _saveString) == 1;
            playerChar.skill0choice2 = PlayerPrefs.GetInt("skill0choice2" + _saveString) == 1;
            playerChar.skill1choice1 = PlayerPrefs.GetInt("skill1choice1" + _saveString) == 1;
            playerChar.skill1choice2 = PlayerPrefs.GetInt("skill1choice2" + _saveString) == 1;
            playerChar.skill2choice1 = PlayerPrefs.GetInt("skill2choice1" + _saveString) == 1;
            playerChar.skill2choice2 = PlayerPrefs.GetInt("skill2choice2" + _saveString) == 1;
            playerChar.skill3choice1 = PlayerPrefs.GetInt("skill3choice1" + _saveString) == 1;
            playerChar.skill3choice2 = PlayerPrefs.GetInt("skill3choice2" + _saveString) == 1;
            playerChar.skill4choice1 = PlayerPrefs.GetInt("skill4choice1" + _saveString) == 1;
            playerChar.skill4choice2 = PlayerPrefs.GetInt("skill4choice2" + _saveString) == 1;
            playerChar.skill5choice1 = PlayerPrefs.GetInt("skill5choice1" + _saveString) == 1;
            playerChar.skill5choice2 = PlayerPrefs.GetInt("skill5choice2" + _saveString) == 1;
            playerChar.skill6choice1 = PlayerPrefs.GetInt("skill6choice1" + _saveString) == 1;
            playerChar.skill6choice2 = PlayerPrefs.GetInt("skill6choice2" + _saveString) == 1;
            playerChar.skill7choice1 = PlayerPrefs.GetInt("skill7choice1" + _saveString) == 1;
            playerChar.skill7choice2 = PlayerPrefs.GetInt("skill7choice2" + _saveString) == 1;
            playerChar.skill8choice1 = PlayerPrefs.GetInt("skill8choice1" + _saveString) == 1;
            playerChar.skill8choice2 = PlayerPrefs.GetInt("skill8choice2" + _saveString) == 1;
            playerChar.skill9choice1 = PlayerPrefs.GetInt("skill9choice1" + _saveString) == 1;
            playerChar.skill9choice2 = PlayerPrefs.GetInt("skill9choice2" + _saveString) == 1;
            playerChar.skill10choice1 = PlayerPrefs.GetInt("skill10choice1" + _saveString) == 1;
            playerChar.skill10choice2 = PlayerPrefs.GetInt("skill10choice2" + _saveString) == 1;
            playerChar.skill11choice1 = PlayerPrefs.GetInt("skill11choice1" + _saveString) == 1;
            playerChar.skill11choice2 = PlayerPrefs.GetInt("skill11choice2" + _saveString) == 1;
            playerChar.skill12choice1 = PlayerPrefs.GetInt("skill12choice1" + _saveString) == 1;
            playerChar.skill12choice2 = PlayerPrefs.GetInt("skill12choice2" + _saveString) == 1;
            playerChar.skill12u1Loaded = PlayerPrefs.GetInt("skill12u1Loaded" + _saveString) == 1;
            playerChar.skill12u2Loaded = PlayerPrefs.GetInt("skill12u2Loaded" + _saveString) == 1;
            playerChar.skill13choice1 = PlayerPrefs.GetInt("skill13choice1" + _saveString) == 1;
            playerChar.skill13choice2 = PlayerPrefs.GetInt("skill13choice2" + _saveString) == 1;

            playerChar.extraDamage7u1 = PlayerPrefs.GetInt("extraDamage7u1" + _saveString) == 1;
            playerChar.skill9u1Spawned = PlayerPrefs.GetInt("skill9u1Spawned" + _saveString) == 1;
            playerChar.skill9u2Spawned = PlayerPrefs.GetInt("skill9u2Spawned" + _saveString) == 1;
            if (!playerStats.human)
            {
                playerStats.health = PlayerPrefs.GetInt("health" + _saveString);
                playerStats.maxHealth = PlayerPrefs.GetInt("maxHealth" + _saveString);
                playerStats.lifes = PlayerPrefs.GetInt("lifes" + _saveString);
            }


            _gv.fartedToDeath = PlayerPrefs.GetInt("fartedToDeath" + _saveString) == 1;
            _gv.firstPlaythroughDone = PlayerPrefs.GetInt("firstPlaythroughDone") == 1;
            _gv.blackMageIntroDone = PlayerPrefs.GetInt("blackMageIntroDone" + _saveString) == 1;
            _gv.nosferatuIntroDone = PlayerPrefs.GetInt("nosferatuIntroDone" + _saveString) == 1;

            _gv.blackMageKilled = PlayerPrefs.GetInt("blackMageKilled" + _saveString) == 1;
            _gv.jaegerKilled = PlayerPrefs.GetInt("jaegerKilled" + _saveString) == 1;
            _gv.assassinKilled = PlayerPrefs.GetInt("assassinKilled" + _saveString) == 1;
            _gv.jaegerEncountered = PlayerPrefs.GetInt("jaegerEncountered" + _saveString) == 1;
            _gv.assassinEncountered = PlayerPrefs.GetInt("assassinEncountered" + _saveString) == 1;
            _gv.werewolfIntroDone = PlayerPrefs.GetInt("werewolfIntroDone" + _saveString) == 1;
            _gv.dragonEndingScore = PlayerPrefs.GetInt("dragonEndingScore" + _saveString);

            _gv.skill0Known = PlayerPrefs.GetInt("skill0known" + _saveString) == 1;
            _gv.skill1Known = PlayerPrefs.GetInt("skill1known" + _saveString) == 1;
            _gv.skill2Known = PlayerPrefs.GetInt("skill2known" + _saveString) == 1;
            _gv.skill3Known = PlayerPrefs.GetInt("skill3known" + _saveString) == 1;
            _gv.skill4Known = PlayerPrefs.GetInt("skill4known" + _saveString) == 1;
            _gv.skill5Known = PlayerPrefs.GetInt("skill5known" + _saveString) == 1;
            _gv.skill6Known = PlayerPrefs.GetInt("skill6known" + _saveString) == 1;
            _gv.skill7Known = PlayerPrefs.GetInt("skill7known" + _saveString) == 1;
            _gv.skill8Known = PlayerPrefs.GetInt("skill8known" + _saveString) == 1;
            _gv.skill9Known = PlayerPrefs.GetInt("skill9known" + _saveString) == 1;
            _gv.skill10Known = PlayerPrefs.GetInt("skill10known" + _saveString) == 1;
            _gv.skill11Known = PlayerPrefs.GetInt("skill11known" + _saveString) == 1;
            _gv.skill12Known = PlayerPrefs.GetInt("skill12known" + _saveString) == 1;
            _gv.skill13Known = PlayerPrefs.GetInt("skill13known" + _saveString) == 1;
            _gv.humansKilled = PlayerPrefs.GetInt("humansKilled" + _saveString);
            _gv.humansCount = PlayerPrefs.GetInt("humansCount" + _saveString);
            _gv.playerSkin = PlayerPrefs.GetInt("playerSkin" + _saveString, 0);
            _gv.gender = PlayerPrefs.GetInt("gender" + _saveString, 0);
            
            SkinSelector[] skinSelectors = GameObject.FindGameObjectWithTag("Player").GetComponentsInChildren<SkinSelector>(true);
            foreach (SkinSelector skinSelector in skinSelectors)
            {
                skinSelector.InitSkin();
            }
            playerChar.InitializeSkills(false);
        }

        public void Intro(int gender)
        {
            Cursor.visible = false;
            scene = 3;
            _saveString = "newGameSave";
            PlayerPrefs.SetInt("gender" + _saveString, gender);
            _gv.gender = gender;
            _loadingScreenRoutine ??= StartCoroutine(LoadScreen(scene));
        }

        public void StartNewGame()
        {
            _firstLevel = true;
            _normalLevel = true;
            scene = 4;
            _saveString = "newGameSave";
            PlayerPrefs.SetInt("health" + _saveString, 100);
            PlayerPrefs.SetInt("maxHealth" + _saveString, 100);
            PlayerPrefs.SetInt("lifes" + _saveString, 0);
            _gv.difficulty = PlayerPrefs.GetInt("difficulty", 2);
            
            _loadingScreenRoutine ??= StartCoroutine(LoadScreen(scene));
        }

        public void MainMenue()
        {
            _mainMenu = true;
            scene = 0;
            StartCoroutine(AddLevel(0));
        }

        public void ShowHumanEnding()
        {
            _normalLevel = true;
            scene = 14;
            _humanEndingScene = true;
            _saveString = "HumanEnding";
            _gv.playerSkin = 0;
            _gv.gender = PlayerPrefs.GetInt("gender" + _saveString, 0);
            _loadingScreenRoutine ??= StartCoroutine(LoadScreen(scene));
        }

        public void ShowServantEnding()
        {
            _normalLevel = true;
            scene = 14;
            _servantEndingScene = true;
            _saveString = "ServantEnding";
            _gv.playerSkin = 0;
            _gv.gender = PlayerPrefs.GetInt("gender" + _saveString, 0);
            _loadingScreenRoutine ??= StartCoroutine(LoadScreen(scene));
        }

        public void ShowVampireEnding()
        {
            _normalLevel = true;
            scene = 14;
            _vampireEndingScene = true;
            _saveString = "VampireEnding";
            _gv.playerSkin = 0;
            _gv.gender = PlayerPrefs.GetInt("gender" + _saveString, 0);
            _loadingScreenRoutine ??= StartCoroutine(LoadScreen(scene));
        }

        public void ShowDevilEnding()
        {
            _normalLevel = true;
            scene = 14;
            _devilEndingScene = true;
            _saveString = "DevilEnding";
            _gv.playerSkin = 0;
            _gv.gender = PlayerPrefs.GetInt("gender" + _saveString, 0);
            _loadingScreenRoutine ??= StartCoroutine(LoadScreen(scene));
        }

        public void SetLanguageEnglish()
        {
            _gv.Language = 0;
            PlayerPrefs.SetInt("sprache", 0);
        }

        public void SetLanguageGerman()
        {
            _gv.Language = 1;
            PlayerPrefs.SetInt("sprache", 1);
        }

        public void SetFirstPlaythroughDone()
        {
            _gv.firstPlaythroughDone = true;
            PlayerPrefs.SetInt("firstPlaythroughDone", 1);
            SetAchievement("FIRST_PLAYTHROUGH");
        }

        public void SetStat(string stat, int amount)
        {
            if (_gv.steamBuild && SteamManager.Initialized)
            {
                SteamUserStats.RequestCurrentStats();
                SteamUserStats.GetStat(stat, out int oldAmount);
                SteamUserStats.SetStat(stat, oldAmount + amount);
                SteamUserStats.StoreStats();
            }
        }
        
        public void SetAchievement(string achievementName)
        {
            if (_gv.steamBuild && SteamManager.Initialized)
            {
                SteamUserStats.RequestCurrentStats();
                SteamUserStats.SetAchievement(achievementName);
                SteamUserStats.StoreStats();
            }
        }

        public void Fullscreen()
        {
            if (Screen.fullScreen)
            {
                Screen.SetResolution(640, 480, false);
                Screen.fullScreen = false;
            }
            else
            {
                Screen.SetResolution(1920, 1080, true);
                Screen.fullScreen = true;
            }
        }

        public void GameOver()
        {
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Save>().deathSave();
            _gameOver = true;
            scene = 2;
            StartCoroutine(AddLevel(scene));
        }

        IEnumerator LoadScreen(int scene)
        {
            _asyncLoadLevel = SceneManager.LoadSceneAsync(loadScreenScene, LoadSceneMode.Single);
            _asyncLoadLevel!.allowSceneActivation = false;
            Debug.Log("Loading Level :" + scene + " : " + SceneManager.GetSceneByBuildIndex(scene).name);
            loadingProgress = 0;
            //When the load is still in progress, output the Text and progress bar
            while (!_asyncLoadLevel.isDone)
            {
                //Output the current progress
               Debug.Log("Loading progress: " + (_asyncLoadLevel.progress * 100) + "%");
                // Check if the load has finished
                if (_asyncLoadLevel.progress >= 0.9f)
                {
                    //Change the Text to show the Scene is ready
                    _asyncLoadLevel.allowSceneActivation = true;
                }

                yield return null;
            }
            StartCoroutine(AddLevel(scene));
        }

        IEnumerator AddLevel(int scene)
        {
            SkinSelector[] skinSelectors = GameObject.FindGameObjectWithTag("Player").GetComponentsInChildren<SkinSelector>(true);
            foreach (SkinSelector skinSelector in skinSelectors)
            {
                skinSelector.InitSkin();
            }
            _asyncLoadLevel = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Single);
            _asyncLoadLevel!.allowSceneActivation = false;

            //Fade the loading screen out here

            while (!_asyncLoadLevel.isDone)
            {
                if (_asyncLoadLevel.progress < 0.9f)
                {
                    loadingProgress = _asyncLoadLevel.progress;
                    yield return null;
                }
                else
                {
                    yield return null;
                    loadingProgress = 1;
                    _asyncLoadLevel.allowSceneActivation = true;
                    yield return null;
                }
            }
        }

        /// <summary>
        /// Private wrapper class for json serialization of the overrides
        /// </summary>
        [Serializable]
        class BindingWrapperClass
        {
            public List<BindingSerializable> bindingList = new List<BindingSerializable>();
        }

        /// <summary>
        /// internal struct to store an id overridePath pair for a list
        /// </summary>
        [Serializable]
        private struct BindingSerializable
        {
            public string id;
            public string path;
            public InputAction action;

            public BindingSerializable(string bindingId, string bindingPath, InputAction actionRef)
            {
                id = bindingId;
                path = bindingPath;
                action = actionRef;
            }
        }

        /// <summary>
        /// stores the active control overrides to player prefs
        /// </summary>
        public void StoreControlOverrides(InputActionAsset control, string prefabString)
        {
            //saving
            BindingWrapperClass bindingList = new BindingWrapperClass();
            foreach (InputActionMap map in control.actionMaps)
            {
                foreach (InputBinding binding in map.bindings)
                {
                    if (!string.IsNullOrEmpty(binding.overridePath))
                    {
                        bindingList.bindingList.Add(new BindingSerializable(binding.id.ToString(), binding.overridePath, map.FindAction(binding.action)));
                    }
                }
            }

            PlayerPrefs.SetString(prefabString, JsonUtility.ToJson(bindingList));
            PlayerPrefs.Save();
            
            PlayerInput playerInput = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInput>();
            if(playerInput == null) return;
            playerInput.actions = control;
        }

        /// <summary>
        /// Loads control overrides from playerPrefs
        /// </summary>
        public void LoadControlOverrides(InputActionAsset control, string prefabString)
        {
            if (PlayerPrefs.HasKey(prefabString))
            {
                BindingWrapperClass bindingList = JsonUtility.FromJson(PlayerPrefs.GetString(prefabString), typeof(BindingWrapperClass)) as BindingWrapperClass;

                //create a dictionary to easier check for existing overrides
                /*
                Dictionary<System.Guid, string> overrides = new Dictionary<System.Guid, string>();
                foreach (BindingSerializable item in bindingList.bindingList)
                {
                    overrides.Add(new System.Guid(item.id), item.path);
                }

                //walk through action maps check dictionary for overrides
                foreach (InputActionMap map in control.actionMaps)
                {
                    ReadOnlyArray<InputBinding> bindings = map.bindings;
                    for (int i = 0; i < bindings.Count; ++i)
                    {
                        if (overrides.TryGetValue(bindings[i].id, out string overridePath))
                        {
                            //if there is an override apply it
                            map.ApplyBindingOverride(i, new InputBinding { overridePath = overridePath });
                        }
                    }
                }*/
                
                //walk through action maps check dictionary for overrides
                if (bindingList != null)
                    foreach (BindingSerializable item in bindingList.bindingList)
                    {
                        if (control.actionMaps[0].bindings.Any(x => x.id == new Guid(item.id)))
                        {
                            int index = control.actionMaps[0].bindings.IndexOf(x => x.id == new Guid(item.id));
                            control.actionMaps[0]
                                .ApplyBindingOverride(index, new InputBinding { overridePath = item.path });
                        }
                        else
                        {
                            //create new Binding
                            InputBinding newBinding = new InputBinding(item.path, item.action.name);
                            _addedBindings.Add(newBinding);
                            control.actionMaps[0].AddBinding(newBinding);
                        }
                    }

                GameObject.Find("Canvas PauseUI").gameObject.transform.Find("Controls").GetComponent<ControlsMenue>().RefreshBindTexts();
            }
        }
        public void ResetKeybindings(InputActionAsset control)
        {
            foreach (InputActionMap map in control.actionMaps)
            {
                map.RemoveAllBindingOverrides();
                foreach (InputBinding addedBinding in _addedBindings)
                {
                    if (map.bindings.Contains(addedBinding))
                    {
                        //the erase function will throw a null exception, if used after starting the game
                        try
                        {
                            map.ChangeBinding(map.bindings.IndexOf(x => x.effectivePath.Equals(addedBinding.effectivePath))).Erase();
                        }
                        catch (Exception)
                        {
                            Debug.Log("Keybinding threw an Error");
                        }
                    }
                }
            }
            _addedBindings.Clear();
            PlayerPrefs.DeleteKey("playerBindings");

            GameObject.Find("Canvas PauseUI").gameObject.transform.Find("Controls").GetComponent<ControlsMenue>().RefreshBindTexts();
            _gv.GetComponent<GlobalVariables>().InvokeBindingsDisplayChangedEvent();
        }

        public void TurnOnInvulnerable()
        {
            if (GameObject.FindGameObjectWithTag("Player") != null)
            {

                if (GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>())
                {
                    GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>()
                        .NewInvulnerable(99999, false, true);
                }
            }
        }
        
        public void TurnOffInvulnerable()
        {
            if (GameObject.FindGameObjectWithTag("Player") !=null)
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().StopInvulnerable();
            }
        }
    }
}