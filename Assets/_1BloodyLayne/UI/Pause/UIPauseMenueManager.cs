﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Player;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
public class UIPauseMenueManager : MonoBehaviour
{
        GameObject mainPauseMenue, options, secrets, skills, controls, controlsListenWindow, credits, levelSubMenue, quitSubMenue;
        GameObject optionsFirstButton, controlsFirstButton, skillsFirstButton, secretsFirstButton, levelSubMenueFirstButton, quitSubMenueFirstButton, creditsBackButton;
        public bool pause;
        public bool endboss;
        public bool mainMenue;
        PauseMenu pauseMenu;
        GameController gc;
        Button lastActiveMainButton, lastActiveOptionsButton, lastActiveLevelsSubButton;

        void Start()
        {
            if (!mainMenue)
            {
                pauseMenu = GameObject.Find("Camera").GetComponent<PauseMenu>();
                mainPauseMenue = transform.Find("PauseUI").gameObject;
                levelSubMenue = transform.Find("LevelSubMenue").gameObject;
                levelSubMenueFirstButton = levelSubMenue.transform.Find("Back").gameObject;
                quitSubMenue = transform.Find("QuitSubMenue").gameObject;
                quitSubMenueFirstButton = quitSubMenue.transform.Find("Back").gameObject;
                skills = transform.Find("SkillTree").gameObject;
                skillsFirstButton = skills.transform.Find("Back").gameObject;
                options = transform.Find("Optionen").gameObject;
                optionsFirstButton = options.transform.Find("Back").gameObject;
                controls = transform.Find("Controls").gameObject;
                controlsFirstButton = controls.transform.Find("Back").gameObject;
                controlsListenWindow = controls.transform.Find("ListeningWindow").gameObject;
                credits = transform.Find("Credits").gameObject;
                creditsBackButton = credits.transform.Find("Back").gameObject;
                if (!endboss)
                {
                    secrets = transform.Find("Secrets").gameObject;
                    secretsFirstButton = secrets.transform.Find("Back").gameObject;
                }
            }
            gc = GameObject.Find("GameController").GetComponent<GameController>();
        }

        public void continueGame()
        {
            pauseMenu.pauseOnOff();
        }

        public void openOptions()
        {
            if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
            {
                lastActiveMainButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
            }
            mainPauseMenue.SetActive(false);
            options.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(optionsFirstButton);
        }

        public void openControls()
        {
            if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
            {
                lastActiveOptionsButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
            }
            options.SetActive(false);
            controlsListenWindow.SetActive(false);
            controls.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(controlsFirstButton);
        }

        public void openSecrets()
        {
            if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
            {
                lastActiveLevelsSubButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
            }
            levelSubMenue.SetActive(false);
            secrets.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(secretsFirstButton);
            DynamicSecretsText[] secretTexte = secrets.GetComponentsInChildren<DynamicSecretsText>();
            foreach (DynamicSecretsText secretText in secretTexte)
            {
                secretText.countFoundSecrets();
            }
        }

        public void openSkills()
        {
            if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
            {
                lastActiveMainButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
            }
            mainPauseMenue.SetActive(false);
            skills.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(skillsFirstButton);
            SkillOption[] secretOptions = skills.GetComponentsInChildren<SkillOption>();
            foreach (SkillOption skillOption in secretOptions)
            {
                skillOption.updateSkillTree();
            }
        }

        public void openLevelSubmenue()
        {
            if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
            {
                lastActiveMainButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
            }
            mainPauseMenue.SetActive(false);
            levelSubMenue.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(levelSubMenueFirstButton);
        }

        public void openQuitSubMenue()
        {
            if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
            {
                lastActiveMainButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
            }
            mainPauseMenue.SetActive(false);
            quitSubMenue.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(quitSubMenueFirstButton);
        }

        public void openCredits()
        {
            if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
            {
                lastActiveOptionsButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
            }
            options.SetActive(false);
            credits.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(creditsBackButton);
        }

        public void backToMainMenue()
        {
            mainPauseMenue.SetActive(true);
            quitSubMenue.SetActive(false);
            levelSubMenue.SetActive(false);
            options.SetActive(false);
            controls.SetActive(false);
            skills.SetActive(false);
            if (!endboss)
            {
                secrets.SetActive(false);
            }
            lastActiveMainButton.Select();
        }

        public void backToOptions()
        {
            options.SetActive(true);
            controls.SetActive(false);
            credits.SetActive(false);
            lastActiveOptionsButton.Select();
        }

        public void backToLevelSubMenue()
        {
            levelSubMenue.SetActive(true);
            secrets.SetActive(false);
            skills.SetActive(false);
            lastActiveLevelsSubButton.GetComponent<Button>().Select();
        }

        public void killPlayer()
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().KillPlayer(DamageTypeToPlayer.KilledByUser);
            pauseMenu.pauseOnOff();
        }

        public void setLanguageEnglish()
        {
            gc.SetLanguageEnglish();
        }

        public void setLanguageGerman()
        {
            gc.SetLanguageGerman();
        }

        public void fullscreen()
        {
            gc.Fullscreen();
        }

        public void endGame()
        {
            Application.Quit();
        }

        public void loadMainMenueScene()
        {
            SceneManager.LoadScene(0);
        }
    }
}
