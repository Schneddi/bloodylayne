using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class DropdownToggleSound : MonoBehaviour
    {
        AudioSource clickSound;

        public void OnValueChanged()
        {
            if (gameObject.activeInHierarchy)
            {
                GameObject.Find("UISounds").GetComponents<AudioSource>()[1].PlayDelayed(0);
            }
        }
    }
}