using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets._2D;

public class InvulnerableToggle : MonoBehaviour
{
    Toggle toggle;
    AudioSource clickSound;
    GlobalVariables gv;
    private GameController gc;
    // Use this for initialization
    void Start()
    {
        clickSound = GameObject.Find("UISounds").GetComponents<AudioSource>()[1];
        toggle = GetComponent<Toggle>();
        gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
        if (!gv.developerBuild)
        {
            gv.forcedInvulnerable = false;
            PlayerPrefs.SetInt("forcedInvulnerable", 0);
            PlayerPrefs.Save();
            gc.TurnOffInvulnerable();
            gameObject.SetActive(false);
        }
        if (!gv.forcedInvulnerable)
        {
            toggle.isOn = false;
        }
        else if (gv.forcedInvulnerable)
        {
            toggle.isOn = true;
        }
    }

    public void OnValueChanged()
    {
        if (gameObject.activeInHierarchy)
        {
            clickSound.PlayDelayed(0);
            if (!toggle.isOn)
            {
                gv.forcedInvulnerable = false;
                PlayerPrefs.SetInt("forcedInvulnerable", 0);
                PlayerPrefs.Save();
                gc.TurnOffInvulnerable();
            }
            if (toggle.isOn)
            {
                gv.forcedInvulnerable = true;
                PlayerPrefs.SetInt("forcedInvulnerable", 1);
                PlayerPrefs.Save();
                gc.TurnOnInvulnerable();
            }
        }
    }
}