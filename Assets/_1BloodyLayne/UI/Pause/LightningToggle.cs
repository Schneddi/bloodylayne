using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class LightningToggle : MonoBehaviour
    {
        Toggle toggle;
        AudioSource clickSound;
        GlobalVariables gv;
        // Use this for initialization
        void Start()
        {
            clickSound = GameObject.Find("UISounds").GetComponents<AudioSource>()[1];
            toggle = GetComponent<Toggle>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            if (!gv.lightningEffects)
            {
                toggle.isOn = false;
            }
            else if (gv.lightningEffects)
            {
                toggle.isOn = true;
            }
        }

        public void OnValueChanged()
        {
            if (gameObject.activeInHierarchy)
            {
                clickSound.PlayDelayed(0);
                if (!toggle.isOn)
                {
                    gv.lightningEffects = false;
                    PlayerPrefs.SetInt("lightningEffects", 0);
                    PlayerPrefs.Save();
                }
                if (toggle.isOn)
                {
                    gv.lightningEffects = true;
                    PlayerPrefs.SetInt("lightningEffects", 1);
                    PlayerPrefs.Save();
                }
            }
        }
    }
}