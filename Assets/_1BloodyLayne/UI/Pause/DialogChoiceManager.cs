﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogChoiceManager : MonoBehaviour
{
    public bool dialogChoiceActive;
    public GameObject currentDialogChoice;

    public void registerSkillChoice(GameObject currentSkillChoice)
    {
        dialogChoiceActive = true;
        this.currentDialogChoice = currentSkillChoice;
    }
    public void unregisterSkillChoice()
    {
        dialogChoiceActive = false;
        currentDialogChoice = null;
    }
}
