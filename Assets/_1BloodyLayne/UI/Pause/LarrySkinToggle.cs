using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class LarrySkinToggle : MonoBehaviour
    {
        public int skinNumber;
        public Toggle otherToggle;
        Toggle toggle;
        AudioSource clickSound;
        GlobalVariables gv;
        // Use this for initialization
        void Start()
        {
            clickSound = GameObject.Find("UISounds").GetComponents<AudioSource>()[1];
            toggle = GetComponent<Toggle>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            if (gv.playerSkin == skinNumber)
            {
                toggle.isOn = true;
            }
            else if (gv.playerSkin != skinNumber)
            {
                toggle.isOn = false;
            }
        }

        public void OnValueChanged()
        {
            if (gameObject.activeInHierarchy)
            {
                clickSound.PlayDelayed(0);
                if (!toggle.isOn)
                {
                    gv.playerSkin = 0;
                    PlayerPrefs.SetInt("playerSkin", 0);
                    PlayerPrefs.Save();
                }
                if (toggle.isOn)
                {
                    gv.playerSkin = skinNumber;
                    if (otherToggle.isOn)
                    {
                        otherToggle.isOn = false;
                    }
                    PlayerPrefs.SetInt("playerSkin", skinNumber);
                    PlayerPrefs.Save();
                }
            }
        }
    }
}