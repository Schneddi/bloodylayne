﻿using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class VolumeSlider : MonoBehaviour
    {
        Slider volumeSlider;
        GlobalVariables gv;
        // Use this for initialization
        void Start()
        {
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            volumeSlider = GetComponent<Slider>();
            volumeSlider.value = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().volume;
        }

        public void OnValueChanged()
        {
            if (gameObject.activeInHierarchy)
            {
                PlayerPrefs.SetFloat("volume", volumeSlider.value);
                PlayerPrefs.Save();
                AudioListener.volume = volumeSlider.value;
                gv.volume = volumeSlider.value;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
