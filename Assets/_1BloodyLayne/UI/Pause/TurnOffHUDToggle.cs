using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets._2D;

public class TurnOffHUDToggle : MonoBehaviour
{
    Toggle toggle;
    AudioSource clickSound;
    GlobalVariables gv;
    // Use this for initialization
    void Start()
    {
        clickSound = GameObject.Find("UISounds").GetComponents<AudioSource>()[1];
        toggle = GetComponent<Toggle>();
        gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        if (!gv.developerBuild)
        {
            gv.hudOn = true;
            PlayerPrefs.SetInt("hudOn", 1);
            PlayerPrefs.Save();
            gameObject.SetActive(false);
        }
        if (!gv.hudOn)
        {
            toggle.isOn = false;
        }
        else if (gv.hudOn)
        {
            toggle.isOn = true;
        }
    }

    public void OnValueChanged()
    {
        if (gameObject.activeInHierarchy)
        {
            clickSound.PlayDelayed(0);
            if (!toggle.isOn)
            {
                gv.hudOn = false;
                PlayerPrefs.SetInt("hudOn", 0);
                PlayerPrefs.Save();
            }
            if (toggle.isOn)
            {
                gv.hudOn = true;
                PlayerPrefs.SetInt("hudOn", 1);
                PlayerPrefs.Save();
            }
        }
    }
}