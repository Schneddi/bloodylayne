using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets._2D;

public class DifficultyDropdown : MonoBehaviour
{
    public string[] germanNames;
    
    private Dropdown dropdown;
    private GlobalVariables gv;
    private string[] englishNames;
    private Text labelText;
    
    // Start is called before the first frame update
    void Start()
    {
        gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        labelText = transform.Find("Label").GetComponent<Text>();
        dropdown = GetComponent<Dropdown>();
        dropdown.value = gv.difficulty;
        dropdown.onValueChanged.AddListener(delegate(int arg0)
        {
            setDifficulty(arg0);
        });
        List<Dropdown.OptionData> optionDatas = dropdown.options;
        englishNames = new string[optionDatas.Count];
        int i = 0;
        foreach (Dropdown.OptionData optionData in optionDatas)
        {
            englishNames[i] = optionData.text;
            i++;
        }
        ChangeLanguage(gv.Language);
        gv.OnLanguageChange += ChangeLanguage;
    }
 
    private void ChangeLanguage(int newLanguage)
    {
        
        List<Dropdown.OptionData> optionDatas = dropdown.options;
        int i = 0;
        switch (newLanguage)
        {
            case 0:
                labelText.text = englishNames[dropdown.value];
                foreach (Dropdown.OptionData optionData in optionDatas)
                {
                    optionData.text = englishNames[i];
                    i++;
                }
                break;
            case 1:
                labelText.text = germanNames[dropdown.value];
                foreach (Dropdown.OptionData optionData in optionDatas)
                {
                    optionData.text = germanNames[i];
                    i++;
                }
                break;
        }
    }

    public void setDifficulty(int difficulty)
    {
        gv.difficulty = difficulty;
        PlayerPrefs.SetInt("difficulty", difficulty);
        string saveString = "defaultSave";
        PlayerPrefs.SetInt("difficulty" + saveString, difficulty);
        saveString = "newGameSave";
        PlayerPrefs.SetInt("difficulty" + saveString, difficulty);
        PlayerPrefs.Save();
    }
}
