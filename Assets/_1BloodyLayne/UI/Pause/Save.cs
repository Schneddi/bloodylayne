﻿using _1BloodyLayne.Control;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityEngine.SceneManagement;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D{
	public class Save : MonoBehaviour {

	    PlatformerCharacter2D playerChar;
	    GameObject player;
        GlobalVariables gv; 
        int health;
        string saveString;


		void Start()
        {
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            player = GameObject.FindWithTag("Player");
			playerChar = player.GetComponent<PlatformerCharacter2D>();
		}

		private void SavePlayer(bool death)
        {
            PlayerPrefs.SetInt("scene", GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().scene);
            PlayerPrefs.SetInt("sprache", gv.Language);
            PlayerPrefs.SetInt("difficulty" + saveString, gv.difficulty);
            if (!death)
            {
                PlayerPrefs.SetInt("health" + saveString, playerChar.GetComponent<PlayerStats>().health);
                PlayerPrefs.SetInt("maxHealth" + saveString, playerChar.GetComponent<PlayerStats>().maxHealth);
                PlayerPrefs.SetInt("lifes" + saveString, playerChar.GetComponent<PlayerStats>().lifes);
                PlayerPrefs.SetInt("humansKilled" + saveString, gv.humansKilled);
                PlayerPrefs.SetInt("humansCount" + saveString, gv.humansCount);
                PlayerPrefs.SetInt("fartedToDeath" + saveString, gv.fartedToDeath == true ? 1 : 0);
                PlayerPrefs.SetInt("skill0choice1" + saveString, playerChar.skill0choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill0choice2" + saveString, playerChar.skill0choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill1choice1" + saveString, playerChar.skill1choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill1choice2" + saveString, playerChar.skill1choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill2choice1" + saveString, playerChar.skill2choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill2choice2" + saveString, playerChar.skill2choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill3choice1" + saveString, playerChar.skill3choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill3choice2" + saveString, playerChar.skill3choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill4choice1" + saveString, playerChar.skill4choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill4choice2" + saveString, playerChar.skill4choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill5choice1" + saveString, playerChar.skill5choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill5choice2" + saveString, playerChar.skill5choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill6choice1" + saveString, playerChar.skill6choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill6choice2" + saveString, playerChar.skill6choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill7choice1" + saveString, playerChar.skill7choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill7choice2" + saveString, playerChar.skill7choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill8choice1" + saveString, playerChar.skill8choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill8choice2" + saveString, playerChar.skill8choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill9choice1" + saveString, playerChar.skill9choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill9choice2" + saveString, playerChar.skill9choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill10choice1" + saveString, playerChar.skill10choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill10choice2" + saveString, playerChar.skill10choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill11choice1" + saveString, playerChar.skill11choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill11choice2" + saveString, playerChar.skill11choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill12choice1" + saveString, playerChar.skill12choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill12choice2" + saveString, playerChar.skill12choice2 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill13choice1" + saveString, playerChar.skill13choice1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill13choice2" + saveString, playerChar.skill13choice2 == true ? 1 : 0);

                PlayerPrefs.SetInt("extraDamage7u1" + saveString, playerChar.extraDamage7u1 == true ? 1 : 0);
                PlayerPrefs.SetInt("skill9u1Spawned" + saveString, playerChar.skill9u1Spawned == true ? 1 : 0);
                PlayerPrefs.SetInt("skill9u2Spawned" + saveString, playerChar.skill9u2Spawned == true ? 1 : 0);
                PlayerPrefs.SetInt("skill12u1Loaded" + saveString, playerChar.skill12u1Loaded == true ? 1 : 0);
                PlayerPrefs.SetInt("skill12u2Loaded" + saveString, playerChar.skill12u2Loaded == true ? 1 : 0);
                     
                PlayerPrefs.SetInt("skill0known" + saveString, gv.skill0Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill1known" + saveString, gv.skill1Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill2known" + saveString, gv.skill2Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill3known" + saveString, gv.skill3Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill4known" + saveString, gv.skill4Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill5known" + saveString, gv.skill5Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill6known" + saveString, gv.skill6Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill7known" + saveString, gv.skill7Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill8known" + saveString, gv.skill8Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill9known" + saveString, gv.skill9Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill10known" + saveString, gv.skill10Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill11known" + saveString, gv.skill11Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill12known" + saveString, gv.skill12Known == true ? 1 : 0);
                PlayerPrefs.SetInt("skill13known" + saveString, gv.skill13Known == true ? 1 : 0);
                PlayerPrefs.SetInt("firstPlaythroughDone", gv.firstPlaythroughDone == true ? 1 : 0);
                
                PlayerPrefs.SetInt("blackMageKilled" + saveString, gv.blackMageKilled == true ? 1 : 0);
                PlayerPrefs.SetInt("jaegerKilled" + saveString, gv.jaegerKilled == true ? 1 : 0);
                PlayerPrefs.SetInt("assassinKilled" + saveString, gv.assassinKilled == true ? 1 : 0);
                PlayerPrefs.SetInt("jaegerEncountered" + saveString, gv.jaegerEncountered == true ? 1 : 0);
                PlayerPrefs.SetInt("assassinEncountered" + saveString, gv.assassinEncountered == true ? 1 : 0);
                if (saveString.Equals("defaultSave"))
                {
                    PlayerPrefs.SetInt("playerSkin" + saveString, gv.playerSkin);
                }
                else
                {
                    PlayerPrefs.SetInt("playerSkin" + saveString, 0);
                }
                PlayerPrefs.SetInt("gender" + saveString, gv.gender);
            }
            PlayerPrefs.SetInt("dragonEndingScore" + saveString, gv.dragonEndingScore);
            PlayerPrefs.SetInt("werewolfIntroDone" + saveString, gv.werewolfIntroDone == true ? 1 : 0);
            PlayerPrefs.SetInt("blackMageIntroDone" + saveString, gv.blackMageIntroDone == true ? 1 : 0);
            PlayerPrefs.SetInt("nosferatuIntroDone" + saveString, gv.nosferatuIntroDone == true ? 1 : 0);

            PlayerPrefs.Save();
        }

        public void defaultSave()
        {
            saveString = "defaultSave";
            SavePlayer(false);
        }

        public void deathSave()
        {
            saveString = "defaultSave";
            SavePlayer(true);
        }

        public void saveHumanEnding()
        {
            PlayerPrefs.SetInt("HumanEnding", 1);
            saveString = "HumanEnding";
            SavePlayer(false);
        }

        public void saveServantEnding()
        {
            PlayerPrefs.SetInt("ServantEnding", 1);
            saveString = "ServantEnding";
            SavePlayer(false);
        }

        public void saveVampireEnding()
        {
            PlayerPrefs.SetInt("VampireEnding", 1);
            saveString = "VampireEnding";
            SavePlayer(false);
        }

        public void saveDevilEnding()
        {
            PlayerPrefs.SetInt("DevilEnding", 1);
            saveString = "DevilEnding";
            SavePlayer(false);
        }
        
        public void saveDefaultBindings(string bindingsString)
        {
            PlayerPrefs.SetString("DefaultBindings", bindingsString);
            PlayerPrefs.Save();
        }
    }
}