using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets._2D;

public class ControlSchemeDropdown : MonoBehaviour
{
    public string[] germanNames;

    private PlayerInputHandler _playerInputHandler;
    private Dropdown dropdown;
    private GlobalVariables gv;
    private string[] englishNames;
    private Text labelText;
    private ControlsMenue _controlsMenu;
    
    // Start is called before the first frame update
    void Awake()
    {
        gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        _controlsMenu = GameObject.Find("Canvas PauseUI").gameObject.transform.Find("Controls").GetComponent<ControlsMenue>();
        _playerInputHandler = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerInputHandler>();
        labelText = transform.Find("Label").GetComponent<Text>();
        dropdown = GetComponent<Dropdown>();
        dropdown.onValueChanged.AddListener(delegate(int arg0)
        {
            SetInputDeviceScheme(arg0);
        });
        List<Dropdown.OptionData> optionDatas = dropdown.options;
        englishNames = new string[optionDatas.Count];
        int i = 0;
        foreach (Dropdown.OptionData optionData in optionDatas)
        {
            englishNames[i] = optionData.text;
            i++;
        }
        ChangeLanguage(gv.Language);
        gv.OnLanguageChange += ChangeLanguage;
    }

    private void OnEnable()
    {
        _playerInputHandler.listenForInput = false;
    }

    private void ChangeLanguage(int newLanguage)
    {
        List<Dropdown.OptionData> optionDatas = dropdown.options;
        int i = 0;
        switch (newLanguage)
        {
            case 0:
                labelText.text = englishNames[dropdown.value];
                foreach (Dropdown.OptionData optionData in optionDatas)
                {
                    optionData.text = englishNames[i];
                    i++;
                }
                break;
            case 1:
                labelText.text = germanNames[dropdown.value];
                foreach (Dropdown.OptionData optionData in optionDatas)
                {
                    optionData.text = germanNames[i];
                    i++;
                }
                break;
        }
    }

    public void SetInputDeviceScheme(int option)
    {
        switch (option)
        {
            case 0:
                _playerInputHandler.SetCurrentControlDevice(CurrentControlDevice.MouseKeyboard);
                break;
            case 1:
                _playerInputHandler.SetCurrentControlDevice(CurrentControlDevice.Gamepad);
                break;
            case 2:
                _playerInputHandler.SetCurrentControlDevice(CurrentControlDevice.XInputController);
                break;
        }
        _controlsMenu.RefreshBindTexts();
    }
    
    private void OnDisable()
    {
        if(_playerInputHandler) _playerInputHandler.listenForInput = true;
    }
}
