using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets._2D;

public class AimToggles : MonoBehaviour
{
    public bool aimAssist, aimVisualization;
    
    Toggle toggle;
    AudioSource clickSound;
    GlobalVariables gv;
    
    // Use this for initialization
    void Start()
    {
        clickSound = GameObject.Find("UISounds").GetComponents<AudioSource>()[1];
        toggle = GetComponent<Toggle>();
        gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        if (aimAssist)
        {
            if (!gv.aimAssist)
            {
                toggle.isOn = false;
            }
            else if (gv.aimAssist)
            {
                toggle.isOn = true;
            }
        }

        if (aimVisualization)
        {
            if (!gv.aimVisualization)
            {
                toggle.isOn = false;
            }
            else if (gv.aimVisualization)
            {
                toggle.isOn = true;
            }
        }
    }

    public void OnValueChanged()
    {
        if (gameObject.activeInHierarchy)
        {
            clickSound.PlayDelayed(0);
            if (aimAssist)
            {
                if (!toggle.isOn)
                {
                    gv.aimAssist = false;
                    PlayerPrefs.SetInt("aimAssist", 0);
                    PlayerPrefs.Save();
                }
                if (toggle.isOn)
                {
                    gv.aimAssist = true;
                    PlayerPrefs.SetInt("aimAssist", 1);
                    PlayerPrefs.Save();
                }
            }
            if (aimVisualization)
            {
                if (!toggle.isOn)
                {
                    gv.aimVisualization = false;
                    PlayerPrefs.SetInt("aimVisualization", 0);
                    PlayerPrefs.Save();
                }
                if (toggle.isOn)
                {
                    gv.aimVisualization = true;
                    PlayerPrefs.SetInt("aimVisualization", 1);
                    PlayerPrefs.Save();
                }
            }
        }
    }
}
