﻿using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using TMPro;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;



namespace UnityStandardAssets._2D
{
    public class PauseUIText : MonoBehaviour
    {
        [SerializeField] private Text[] dynamicTextReferences1, dynamicTextReferences2;
        
        private Text enText, gerText;
        private TMP_Text enTmpText, getTmpText;
        private GlobalVariables gv;
        private string[] textCopies;

        // Use this for initialization
        void Start()
        {
            enText = gameObject.transform.GetChild(0).GetComponent<Text>();
            gerText = gameObject.transform.GetChild(1).GetComponent<Text>();
            textCopies = new string[2];
            if (enText == null) enTmpText = gameObject.transform.GetChild(0).GetComponent<TMP_Text>();
            else textCopies[0] = enText.text;
            if(gerText == null) getTmpText = gameObject.transform.GetChild(1).GetComponent<TMP_Text>();
            else textCopies[1] = gerText.text;
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            ChangeLanguage(gv.Language);
            gv.OnLanguageChange += ChangeLanguage;
            RefreshBindingsTexts();
            gv.OnBindingsChange += RefreshBindingsTexts;
        }

        private void ChangeLanguage(int language)
        {
            switch (language)
            {
                case 0:
                    if(enText != null) enText.enabled = true;
                    if(gerText != null) gerText.enabled = false;
                    if(enTmpText != null) enTmpText.enabled = true;
                    if(getTmpText != null) getTmpText.enabled = false;
                    break;
                case 1:
                    if(enText != null) enText.enabled = false;
                    if(gerText != null) gerText.enabled = true;
                    if(enTmpText != null) enTmpText.enabled = false;
                    if(getTmpText != null) getTmpText.enabled = true;
                    break;
            }
        }

        private void RefreshBindingsTexts()
        {
            if (dynamicTextReferences1 == null || dynamicTextReferences1.Length == 0) return;

            string[] newTexts = new string[] {textCopies[0], textCopies[1]};

            int refReplacementIndex = 0;
            if (dynamicTextReferences1.Length > 1)
            {
                bool writtenOr = false;
                refReplacementIndex = dynamicTextReferences1.Length-1;
                string replacementString = "";
                
                for (int i=0; i<4; i++)
                {
                    if (!String.IsNullOrEmpty(dynamicTextReferences1[i].text) && !dynamicTextReferences1[i].text.Equals("-"))
                    {
                        if (!String.IsNullOrEmpty(replacementString)) replacementString += ", ";
                        replacementString += dynamicTextReferences1[i].text;
                    }
                }
                
                for (int i=0; i<dynamicTextReferences2.Length - 1; i++)
                {
                    if (!String.IsNullOrEmpty(dynamicTextReferences2[i].text) && !dynamicTextReferences2[i].text.Equals("-"))
                    {
                        if (!writtenOr)
                        {
                            replacementString += " / ";
                        }
                            
                        if (!String.IsNullOrEmpty(replacementString) && writtenOr) replacementString += ", ";
                        replacementString += dynamicTextReferences2[i].text;
                            
                        if (!writtenOr)
                        {
                            writtenOr = true;
                        }
                    }
                }
                
                for (int i=0; i<newTexts.Length; i++)
                {
                    newTexts[i] = newTexts[i].Replace("$Ref0$, $Ref1$, $Ref2$, $Ref3$", replacementString);
                }
            }
            
            for (int i=0; i<newTexts.Length; i++)
            {
                string replaceString = "$Ref" + (refReplacementIndex) + "$";
                string replacementString = "";
                bool writtenFirstBinding = false;
                if (!String.IsNullOrEmpty(dynamicTextReferences1[refReplacementIndex].text) && !dynamicTextReferences1[refReplacementIndex].text.Equals("-"))
                {
                    replacementString = dynamicTextReferences1[refReplacementIndex].text;
                    writtenFirstBinding = true;
                }
                if (!String.IsNullOrEmpty(dynamicTextReferences2[refReplacementIndex].text) && !dynamicTextReferences2[refReplacementIndex].text.Equals("-"))
                {
                    if (writtenFirstBinding ) replacementString += "/ ";
                    replacementString += dynamicTextReferences2[refReplacementIndex].text;
                }
                newTexts[i] = newTexts[i].Replace(replaceString, replacementString);
            }

            enText.text = newTexts[0];
            gerText.text = newTexts[1];
        }
    }
}
