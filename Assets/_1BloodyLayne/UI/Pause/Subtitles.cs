﻿using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class Subtitles : MonoBehaviour
    {
        Toggle toggle;
        AudioSource clickSound;
        GlobalVariables gv;
        // Use this for initialization
        void Start()
        {
            clickSound = GameObject.Find("UISounds").GetComponents<AudioSource>()[1];
            toggle = GetComponent<Toggle>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            if (gv.subtitles == 0)
            {
                toggle.isOn = false;
            }
            else if (gv.subtitles == 1)
            {
                toggle.isOn = true;
            }
        }

        public void OnValueChanged()
        {
            if (gameObject.activeInHierarchy)
            {
                clickSound.PlayDelayed(0);
                if (!toggle.isOn)
                {
                    gv.subtitles = 0;
                    PlayerPrefs.SetInt("subtitles", 0);
                    PlayerPrefs.Save();
                }
                if (toggle.isOn)
                {
                    gv.subtitles = 1;
                    PlayerPrefs.SetInt("subtitles", 1);
                    PlayerPrefs.Save();
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
