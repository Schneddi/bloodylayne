﻿using System.Collections;
using System.Collections.Generic;
using Steamworks;
using Units.Player;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class PauseMenu : MonoBehaviour
    {
        GameObject PauseUI, pausemenue, options, secrets, skills, controls, levelSubMenue, quitSubMenue, credits;
        GameObject continueButton, optionsFirstButton, controlsFirstButton, skillsFirstButton, secretsFirstButton, lastPauseButton, levelSubMenueFirstButton, quitSubMenueFirstButton;
        public bool pause;
        public bool endboss, blockMenue;
        Platformer2DUserControl userControl;
        PlatformerCharacter2D playerCharacter;
        Dialog dialogManager;
        DialogChoiceManager dialogChoiceManager;
        public GameObject skillChoiceCanvasObject, tutorialCanvas;
        Button disabledButton;
        private AudioSource openMenueSound;
        protected Callback<GameOverlayActivated_t> m_GameOverlayActivated;

        void Start()
        {
            playerCharacter = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            if (GameObject.Find("Canvas DialogChoice System") != null)
            {
                dialogChoiceManager = GameObject.Find("Canvas DialogChoice System").GetComponent<DialogChoiceManager>();
            }
            PauseUI = GameObject.Find("Canvas PauseUI").gameObject;
            PauseUI.SetActive(false);
            userControl = GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>();
            pausemenue = PauseUI.transform.Find("PauseUI").gameObject;
            continueButton = pausemenue.transform.Find("Weiter").gameObject;
            options = PauseUI.transform.Find("Optionen").gameObject;
            optionsFirstButton = options.transform.Find("Back").gameObject;
            controls = PauseUI.transform.Find("Controls").gameObject;
            controlsFirstButton = controls.transform.Find("Back").gameObject;
            levelSubMenue = PauseUI.transform.Find("LevelSubMenue").gameObject;
            levelSubMenueFirstButton = levelSubMenue.transform.Find("Back").gameObject;
            quitSubMenue = PauseUI.transform.Find("QuitSubMenue").gameObject;
            quitSubMenueFirstButton = quitSubMenue.transform.Find("Back").gameObject;
            credits = PauseUI.transform.Find("Credits").gameObject;
            if (!endboss)
            {
                secrets = PauseUI.transform.Find("Secrets").gameObject;
                secretsFirstButton = secrets.transform.Find("Back").gameObject;
            }
            DynamicSecretsText[] secretTexte = secrets.GetComponentsInChildren<DynamicSecretsText>();
            foreach (DynamicSecretsText secretText in secretTexte)
            {
                secretText.countSecrets();
            }
            skills = PauseUI.transform.Find("SkillTree").gameObject;
            skillsFirstButton = skills.transform.Find("Back").gameObject;
            dialogManager = GameObject.Find("CanvasDialogManager").GetComponent<Dialog>();
            openMenueSound = GameObject.Find("UISounds").GetComponents<AudioSource>()[3];
            if (SteamManager.Initialized)
            {
                m_GameOverlayActivated = Callback<GameOverlayActivated_t>.Create(OnGameOverlayActivated);
            }
        }
        
        private void OnGameOverlayActivated(GameOverlayActivated_t pCallback)
        {
            if (pCallback.m_bActive != 0)
            {
                if (!pause)
                {
                    pauseOnOff();
                }
                //Debug.Log("Steam Overlay has been activated");
            }
            else
            {
                
                //Debug.Log("Steam Overlay has been closed");
            }
        }

        void Update()
        {
            if (Input.GetButtonDown("Pause"))
            {
                pauseOnOff();
            }
        }

        public void Weiter()
        {
            pauseOnOff();
        }

        public void Optionen()
        {
            pausemenue.SetActive(false);
            options.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(optionsFirstButton);
        }

        public void openControls()
        {
            options.SetActive(false);
            controls.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(controlsFirstButton);
        }

        public void Secrets()
        {
            pausemenue.SetActive(false);
            secrets.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(secretsFirstButton);
            DynamicSecretsText[] secretTexte = secrets.GetComponentsInChildren<DynamicSecretsText>();
            foreach(DynamicSecretsText secretText in secretTexte)
            {
                secretText.countFoundSecrets();
            }
        }

        public void Skills()
        {
            pausemenue.SetActive(false);
            skills.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(skillsFirstButton);
            SkillOption[] secretOptions = skills.GetComponentsInChildren<SkillOption>();
            foreach (SkillOption skillOption in secretOptions)
            {
                skillOption.updateSkillTree();
            }
        }

        public void zurueck()
        {
            pausemenue.SetActive(true);
            options.SetActive(false);
            skills.SetActive(false);
            controls.SetActive(false);
            levelSubMenue.SetActive(false);
            quitSubMenue.SetActive(false);
            credits.SetActive(false);
            if (!endboss)
            {
                secrets.SetActive(false);
            }
        }

        public void zurueckControls()
        {
            options.SetActive(true);
            controls.SetActive(false);
        }

        public void Neustart()
        {
            playerCharacter.Restart();
			pauseOnOff();
        }

        public void Ende()
        {
            Application.Quit();
        }

        public void MainMenu()
        {
            SceneManager.LoadScene(0);
        }
        public bool getPause()
        {
            return pause;
        }

		public void pauseOnOff()
		{
            if (!blockMenue)
            {
                pause = !pause;
                zurueck();
                if (pause)
                {
                    PauseUI.SetActive(true);
                    Time.timeScale = 0;
                    userControl.BlockInput = true;
                    playerCharacter.Pause();
                    if (dialogManager.dialogActive)
                    {
                        dialogManager.dBox.GetComponent<Dialogbox>().GamePaused();
                    }
                    if (dialogChoiceManager != null)
                    {
                        if (dialogChoiceManager.dialogChoiceActive)
                        {
                            if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
                            {
                                disabledButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
                            }
                            dialogChoiceManager.currentDialogChoice.SetActive(false);
                        }
                    }
                    if (skillChoiceCanvasObject != null)
                    {
                        if (skillChoiceCanvasObject.activeInHierarchy)
                        {
                            if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
                            {
                                disabledButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
                                //skillChoiceCanvasObject.transform.parent.GetComponentInChildren<SkillChoice>().selectedChoice = disabledButton.gameObject;
                            }
                            skillChoiceCanvasObject.SetActive(false);
                        }
                    }
                    if (tutorialCanvas != null)
                    {
                        if (tutorialCanvas.activeInHierarchy)
                        {
                            tutorialCanvas.transform.GetChild(0).gameObject.SetActive(false);
                        }
                    }

                    if (lastPauseButton == null)
                    {
                        lastPauseButton = continueButton;
                    }
                    lastPauseButton.GetComponent<EventTrigger>().enabled = false;
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(continueButton);
                    lastPauseButton.GetComponent<EventTrigger>().enabled = true;
                    openMenueSound.PlayDelayed(0);
                }
                if (!pause)
                {
                    lastPauseButton = EventSystem.current.currentSelectedGameObject;
                    PauseUI.SetActive(false);
                    Time.timeScale = 1;
                    userControl.continuePress = true;
                    if (!playerCharacter.dead && !dialogManager.dialogActive && skillChoiceCanvasObject == null && tutorialCanvas == null)
                    {
                        if (dialogChoiceManager == null)
                        {
                            userControl.BlockInput = false;
                        }
                        else if (!dialogChoiceManager.dialogChoiceActive)
                        {
                            userControl.BlockInput = false;
                        }
                    }
                    Cursor.visible = false;
                    playerCharacter.Resume();
                    if (dialogManager.dialogActive)
                    {
                        dialogManager.dBox.GetComponent<Dialogbox>().GameResumed();
                    }
                    if (dialogChoiceManager != null)
                    {
                        if (dialogChoiceManager.dialogChoiceActive)
                        {
                            dialogChoiceManager.currentDialogChoice.SetActive(true);
                            EventSystem.current.SetSelectedGameObject(null);
                            EventSystem.current.SetSelectedGameObject(disabledButton.gameObject);
                            disabledButton.Select();
                        }
                    }
                    if (skillChoiceCanvasObject != null)
                    {
                        skillChoiceCanvasObject.SetActive(true);
                        skillChoiceCanvasObject.transform.parent.GetComponentInChildren<SkillChoice>().activateSkillUI(disabledButton);
                        skillChoiceCanvasObject.transform.parent.GetComponentInChildren<SkillChoice>().addSkillListener();
                    }
                    if (tutorialCanvas != null)
                    {
                        tutorialCanvas.transform.GetChild(0).gameObject.SetActive(true);
                        tutorialCanvas.transform.parent.GetComponentInChildren<TutorialPopUp>().activateTutorialCanvas();
                    }
                }
            }	
		}
    }
}
