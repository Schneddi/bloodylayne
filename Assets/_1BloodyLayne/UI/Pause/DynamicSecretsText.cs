﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Enemys;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class DynamicSecretsText : MonoBehaviour
    {
        private int chaliceCount, chalicesCollected, soulCount, soulsCollected, crystallCount, crystallCollected;
        public int killCount, killsDone
        ;
        private string chaliceText, soulText, crystallText, killText;
        public bool chalice, soul, crystall, kill;

        public void countSecrets()
        {
            GameObject[] powerUps = GameObject.FindGameObjectsWithTag("PowerUp");
            EnemyStats[] enemys = FindObjectsOfType<EnemyStats>(true);
            GameObject[] objects = GameObject.FindGameObjectsWithTag("Object");

            foreach (GameObject powerUp in powerUps)
            {
                if ((powerUp.gameObject.GetComponent("HealthKit") as HealthKit) != null)
                {
                    chaliceCount++;
                }
                else if ((powerUp.gameObject.GetComponent("LebensKit") as LebensKit) != null)
                {
                    soulCount++;
                }
                else if ((powerUp.gameObject.GetComponent("Kristallkugel") as Kristallkugel) != null)
                {
                    crystallCount++;
                }
            }

            foreach (EnemyStats enemy in enemys)
            {
                if (enemy.isHuman)
                {
                    killCount++;
                }
            }
            
            foreach (GameObject secretsObject in objects)
            {
                if (secretsObject.gameObject.GetComponent<Truhe>() != null)
                {
                    if (secretsObject.gameObject.GetComponent<Truhe>().lootItem != null)
                    {
                        if (secretsObject.gameObject.GetComponent<Truhe>().lootItem.GetComponent<HealthKit>() != null)
                        {
                            chaliceCount++;
                        }
                        else if (secretsObject.gameObject.GetComponent<Truhe>().lootItem.GetComponent<LebensKit>() != null)
                        {
                            soulCount++;
                        }
                        else if (secretsObject.gameObject.GetComponent<Truhe>().lootItem.GetComponent<Kristallkugel>() != null)
                        {
                            crystallCount++;
                        }
                    }
                }
            }
        }
        
        public void countFoundSecrets()
        {
            GameObject[] powerUps = GameObject.FindGameObjectsWithTag("PowerUp");
            EnemyStats[] enemys = FindObjectsOfType<EnemyStats>(true);
            GameObject[] objects = GameObject.FindGameObjectsWithTag("Object");
            chalicesCollected = chaliceCount;
            soulsCollected = soulCount;
            crystallCollected = crystallCount;
            killsDone = killCount;

            foreach (GameObject powerUp in powerUps)
            {
                if ((powerUp.gameObject.GetComponent("HealthKit") as HealthKit) != null)
                {
                    chalicesCollected--;
                }
                else if ((powerUp.gameObject.GetComponent("LebensKit") as LebensKit) != null)
                {
                    soulsCollected--;
                }
                else if ((powerUp.gameObject.GetComponent("Kristallkugel") as Kristallkugel) != null)
                {
                    crystallCollected--;
                }
            }
            
            foreach (EnemyStats enemy in enemys)
            {
                if (enemy.isHuman)
                {
                    killsDone--;
                }
            }
            
            foreach (GameObject objectGameObject in objects)
            {
                if (objectGameObject.gameObject.GetComponent<Truhe>() != null)
                {
                    if (objectGameObject.gameObject.GetComponent<Truhe>().lootItem != null)
                    {
                        if (objectGameObject.gameObject.GetComponent<Truhe>().lootItem.GetComponent<HealthKit>() != null)
                        {
                            chalicesCollected--;
                        }
                        else if (objectGameObject.gameObject.GetComponent<Truhe>().lootItem.GetComponent<LebensKit>() != null)
                        {
                            soulsCollected--;
                        }
                        else if (objectGameObject.gameObject.GetComponent<Truhe>().lootItem.GetComponent<Kristallkugel>() != null)
                        {
                            crystallCollected--;
                        }
                    }
                }
            }

            chaliceText = chalicesCollected + " / " + chaliceCount;
            soulText = soulsCollected + " / " + soulCount;
            crystallText = crystallCollected + " / " + crystallCount;
            killText = killsDone + " / " + killCount;

            if (chalice == true)
            {
                gameObject.transform.GetChild(0).GetComponent<Text>().text = chaliceText;
            }
            else if (soul == true)
            {
                gameObject.transform.GetChild(0).GetComponent<Text>().text = soulText;
            }
            else if (crystall == true)
            {
                gameObject.transform.GetChild(0).GetComponent<Text>().text = crystallText;
            }
            else if (kill == true)
            {
                if(killsDone < killCount-10 || killsDone == 0 || GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().humansKilled == 0)
                {
                    gameObject.transform.GetChild(0).GetComponent<Text>().text = "";
                    gameObject.transform.GetChild(1).GetComponent<Text>().text = "";
                }
                else
                {
                    gameObject.transform.GetChild(0).GetComponent<Text>().text = killText;
                    gameObject.transform.GetChild(1).GetComponent<Text>().text = "Kills";
                }
            }
        }
    }
}
