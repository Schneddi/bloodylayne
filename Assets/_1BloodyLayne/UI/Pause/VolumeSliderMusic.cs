﻿using _1BloodyLayne.Control;
using UI.PlayerUI;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class VolumeSliderMusic : MonoBehaviour
    {
        Slider volumeSlider;
        GlobalVariables gv;
        // Use this for initialization
        void Start()
        {
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            volumeSlider = GetComponent<Slider>();
            volumeSlider.value = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().musicVolume;
        }

        public void OnValueChanged()
        {
            if (gameObject.activeInHierarchy)
            {
                PlayerPrefs.SetFloat("musicVolume", volumeSlider.value);
                PlayerPrefs.Save();
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().CurrentMusicClip.volume = volumeSlider.value;
                gv.musicVolume = volumeSlider.value;
            }
        }
    }
}
