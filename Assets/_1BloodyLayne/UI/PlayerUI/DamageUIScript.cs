﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class DamageUIScript : MonoBehaviour
    {
        [SerializeField] private float minimumBloodScreenDuration, maximumBloodScreenDuration, minimumBloodScreenAlpha, maximumBloodScreenAlpha;
        [Range(0, 1)] [SerializeField] private float longerBloodScreenPoint, minimumAlphaTotalDamage, maximumAlphaTotalDamage;
        
        private Color nextImageColor = new Color(255f, 255f, 255f, 0f);
        private Color fadedOutColor = new Color(255f, 255f, 255f, 0f);
        private Color startingColor = new Color(255f, 255f, 255f, 0f);
        Image imageDamage;
        private float nextAlphaValue, nextFadingMidPoint, nextFadedOutPoint, nextFadingDuration, bloodScreenStartingTime;
        private bool hurt;
        
        private void Start()
        {
            imageDamage = GameObject.Find("DamageBloodEffectScreen").GetComponent<Image>();
            
        }
        
        // Update is called once per frame
        void Update()
        {
            if (hurt)
            {
                float timeLerp = (Time.time - bloodScreenStartingTime) / minimumBloodScreenDuration;
                if (timeLerp < 1 && nextImageColor.a > imageDamage.color.a)
                {
                    imageDamage.color = Color.Lerp(startingColor, nextImageColor, timeLerp);
                }
                else
                {
                    float fadeOutTimeLerp = (Time.time - nextFadingMidPoint) / nextFadingDuration;
                    if (fadeOutTimeLerp < 1)
                    {
                        nextAlphaValue = 0;
                        Color nextColor = Color.Lerp(nextImageColor, fadedOutColor, fadeOutTimeLerp);
                        if (nextColor.a < imageDamage.color.a)
                        {
                            imageDamage.color = nextColor;
                        }
                    }
                    else
                    {
                        if (fadedOutColor.a < imageDamage.color.a)
                        {
                            imageDamage.color = fadedOutColor;
                        }
                        hurt = false;
                    }
                }
            }
        }
        
        public void DamagePlayer(float damagePercentage, float remainingHealth)
        {
            float newFadingMidPoint = Time.time + minimumBloodScreenDuration;
            float newFadingDuration = minimumBloodScreenDuration;
            if (remainingHealth < longerBloodScreenPoint)
            {
                float lerp = (longerBloodScreenPoint - remainingHealth)/(longerBloodScreenPoint);
                newFadingDuration = Mathf.Lerp(minimumBloodScreenDuration, maximumBloodScreenDuration, lerp) ;
            }
            
            float newAlphaMaxValue = minimumBloodScreenAlpha;
            if (damagePercentage > minimumAlphaTotalDamage)
            {
                float lerp = (damagePercentage - minimumAlphaTotalDamage)/(maximumAlphaTotalDamage - minimumAlphaTotalDamage);
                newAlphaMaxValue = Mathf.Lerp(minimumBloodScreenAlpha, maximumBloodScreenAlpha, lerp) ;
            }
            
            //compare if the new attackDamage and time is over old ones
            if (nextFadingMidPoint < newFadingMidPoint)
            {
                hurt = true;
                nextFadingDuration = newFadingDuration;
                bloodScreenStartingTime = Time.time;
                nextFadingMidPoint = newFadingMidPoint;
            }

            if (nextAlphaValue < newAlphaMaxValue)
            {
                hurt = true;
                startingColor = imageDamage.color;
                nextAlphaValue = newAlphaMaxValue;
                nextImageColor = new Color(nextImageColor.r, nextImageColor.g, nextImageColor.b, newAlphaMaxValue);
            }
        }
        
        public void StopAllDamageUIEffects()
        {
            hurt = true;
            nextFadingDuration = minimumBloodScreenDuration;
            bloodScreenStartingTime = Time.time;
            nextFadingMidPoint = Time.time + minimumBloodScreenDuration;
            startingColor = imageDamage.color;
            nextAlphaValue = 0;
            nextImageColor = new Color(nextImageColor.r, nextImageColor.g, nextImageColor.b, 0);
        }


    }
}

