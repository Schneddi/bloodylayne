using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class ImageFunctions : MonoBehaviour
    {
        public ImageType imageType;
        
        private Image[] images;
        private SpriteRenderer[] sRenderers;
        private Color fadeColor;
        private bool fadingIn, fadingOut;

        // Use this for initialization
        void Start()
        {
            switch (imageType)
            {
                case ImageType.image:
                    images = GetComponentsInChildren<Image>();
                    break;
                case ImageType.sprite:
                    sRenderers = GetComponentsInChildren<SpriteRenderer>();
                    break;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (fadingIn)
            {
                switch (imageType)
                {
                    case ImageType.image:
                        foreach (Image image in images)
                        {
                            image.color = new Color(image.color.r, image.color.g, image.color.b,
                                image.color.a + Time.deltaTime);
                        }
                        if (images[0].color.a >= 1)
                        {
                            fadingIn = false;
                        }
                        break;
                    case ImageType.sprite:
                        foreach (SpriteRenderer sRenderer in sRenderers)
                        {
                            sRenderer.color = new Color(sRenderer.color.r, sRenderer.color.g, sRenderer.color.b,
                                sRenderer.color.a + Time.deltaTime);
                        }
                        if (sRenderers[0].color.a >= 1)
                        {
                            fadingIn = false;
                        }
                        break;
                }
            }
            if (fadingOut)
            {
                
                switch (imageType)
                {
                    case ImageType.image:
                        foreach (Image image in images)
                        {
                            image.color =  new Color(image.color.r, image.color.g, image.color.b,
                                image.color.a - Time.deltaTime);
                        }
                        if (images[0].color.a <= 0)
                        {
                            fadingOut = false;
                        }
                        break;
                    case ImageType.sprite:
                        foreach (SpriteRenderer sRenderer in sRenderers)
                        {
                            sRenderer.color =  new Color(sRenderer.color.r, sRenderer.color.g, sRenderer.color.b,
                                sRenderer.color.a - Time.deltaTime);
                        }
                        if (sRenderers[0].color.a <= 0)
                        {
                            fadingOut = false;
                        }
                        break;
                }
            }
        }
        
        public void fadeIn()
        {
            fadingIn = true;
            fadingOut = false;
        }

        public void fadeOut()
        {
            fadingOut = true;
            fadingIn = false;
        }
    } 
}