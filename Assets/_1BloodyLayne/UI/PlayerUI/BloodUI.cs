using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    public class BloodUI : MonoBehaviour
    {
        public Image emptyVein;
        public float fillChangeSpeed;
        public Image currentBlood;
        
        private DamageUIScript damageScript;
        private PlatformerCharacter2D player;
        private PlayerStats playerStats;
        private Color fadeColor;
        bool fadingIn, fadingOut;
        private AudioSource bloodlossSound;
        private ParticleSystem bloodthirstEffect;
        private GlobalVariables gv;

        // Use this for initialization
        void Start()
        {
            damageScript = GameObject.FindGameObjectWithTag("Player").GetComponent<DamageUIScript>();
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            playerStats = player.GetComponent<PlayerStats>();
            bloodlossSound = GetComponent<AudioSource>();
            bloodthirstEffect = transform.Find("BloodThirstEffect").GetComponent<ParticleSystem>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        }

        // Update is called once per frame
        void Update()
        {
            if (damageScript != null)
            {
                ChangingUIHealth();
            }
            else
            {
                player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
                damageScript = FindObjectOfType<DamageUIScript>();
                emptyVein.enabled = false;
            }
            if (fadingIn)
            {
                fadeColor = new Color(1f, 1, 1, 1f);
                Image[] images = GetComponentsInChildren<Image>();
                foreach (Image image in images)
                {
                    image.color = new Color(image.color.r, image.color.g, image.color.b,
                        image.color.a + Time.deltaTime);
                }
                if (images[0].color.a >= 1)
                {
                    fadingIn = false;
                }
            }
            if (fadingOut)
            {
                fadeColor = new Color(0f, 0, 0, 0f);
                Image[] images = GetComponentsInChildren<Image>();
                foreach (Image image in images)
                {
                    image.color = new Color(image.color.r, image.color.g, image.color.b,
                        image.color.a - Time.deltaTime);
                }
                if (images[0].color.a <= 0)
                {
                    fadingOut = false;
                }
            }
        }
        
        void ChangingUIHealth()
        {
            if(playerStats.health >= 0)
            {
                currentBlood.enabled= true;
                //currentBlood.fillAmount = (Larry.bloodlust/Larry.maxBloodlust);
                
                if (currentBlood.fillAmount < (float)player.bloodlust / (float)playerStats.maxBloodlust)
                {
                    float fillChange = fillChangeSpeed * Time.deltaTime;
                    if (currentBlood.fillAmount + fillChange > (float)player.bloodlust / (float)playerStats.maxBloodlust)
                    {
                        currentBlood.fillAmount = (float)player.bloodlust / (float)playerStats.maxBloodlust;
                    }
                    else
                    {
                        currentBlood.fillAmount += fillChange;
                    }
                }
                else if (currentBlood.fillAmount > (float)player.bloodlust / (float)playerStats.maxBloodlust)
                {
                    float fillChange = fillChangeSpeed * Time.deltaTime;
                    if (currentBlood.fillAmount - fillChange < (float)player.bloodlust / (float)playerStats.maxBloodlust)
                    {
                        currentBlood.fillAmount = (float)player.bloodlust / (float)playerStats.maxBloodlust;
                    }
                    else
                    {
                        currentBlood.fillAmount -= fillChange;
                    }
                }
            }
        }

        public void bloodthirstStartEffect()
        {
            if (gv.hudOn)
            {
                bloodlossSound.PlayDelayed(0);
                bloodthirstEffect.Play();
            }
        }

        public void fadeIn()
        {
            fadingIn = true;
            fadingOut = false;
        }

        public void fadeOut()
        {
            fadingOut = true;
            fadingIn = false;
        }
    } 
}