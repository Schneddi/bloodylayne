using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets._2D;

public class HuDUISprite : MonoBehaviour
{
    public Sprite originalSprite, nothingSprite;
    public ImageType imageType;
    
    private GlobalVariables gv;
    private bool activated;
    private SpriteRenderer sRenderer;
    private Image image;

    // Start is called before the first frame update
    void Start()
    {
        gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
        switch (imageType)
        {
            case ImageType.image:
                image = GetComponent<Image>();
                originalSprite = image.sprite;
                break;
            case ImageType.sprite:
                sRenderer = GetComponent<SpriteRenderer>();
                originalSprite = sRenderer.sprite;
                break;
        }
        if (gv.hudOn)
        {
            activated = true;
        }
        else
        {
            sRenderer.sprite = nothingSprite;
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (imageType)
        {
            case ImageType.image:
                if (activated && !gv.hudOn)
                {
                    image.sprite = nothingSprite;
                    activated = false;
                }

                if (!activated && gv.hudOn)
                {
                    image.sprite = originalSprite;
                    activated = true;
                }
                break;
            case ImageType.sprite:
                if (activated && !gv.hudOn)
                {
                    sRenderer.sprite = nothingSprite;
                    activated = false;
                }

                if (!activated && gv.hudOn)
                {
                    sRenderer.sprite = originalSprite;
                    activated = true;
                }
                break;
        }
    }
}

public enum ImageType
{
    sprite,
    image,
}