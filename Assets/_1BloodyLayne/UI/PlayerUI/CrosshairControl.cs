using UnityEngine;
using UnityEngine.InputSystem;

public class CrosshairControl : MonoBehaviour
{
    public Vector3 cursorSize;
    // Start is called before the first frame update
    void Start()
    {
        transform.SetParent(transform.parent.parent);
        transform.localScale = cursorSize;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
        //mousePosition.z = Camera.main.transform.position.z + Camera.main.nearClipPlane;
        transform.position = mousePosition;
    }
}
