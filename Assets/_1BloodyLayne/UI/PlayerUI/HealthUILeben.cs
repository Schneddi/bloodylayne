﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using TMPro;
using Units.Player;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    public class HealthUILeben : MonoBehaviour
    {

        public Image heart;
        
        private DamageUIScript damageScript;
        private PlatformerCharacter2D player;
        private PlayerStats playerStats;
        private TMP_Text healthText;
        private Color fadeColor;
        private bool fadingIn, fadingOut;
        private GlobalVariables gv;

        // Use this for initialization
        void Start()
        {
            damageScript = GameObject.FindGameObjectWithTag("Player").GetComponent<DamageUIScript>();
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            playerStats = player.GetComponent<PlayerStats>();
            healthText = GameObject.Find("CurrentRespawnsText").GetComponent<TMP_Text>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            fadeColor = new Color(1f, 1, 1, 1f);
        }

        // Update is called once per frame
        void Update()
        {
            if (damageScript != null)
            {
                ChangingUIHealth();
            }
            else
            {
                player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
                damageScript = FindObjectOfType<DamageUIScript>();
                heart.enabled = false;
            }
            if (fadingIn)
            {
                fadeColor = new Color(1f, 1, 1, 1f);
                Image[] images = GetComponentsInChildren<Image>();
                foreach (Image image in images)
                {
                    image.color = new Color(image.color.r, image.color.g, image.color.b,
                        image.color.a + Time.deltaTime);
                }
                TMP_Text[] texts = GetComponentsInChildren<TMP_Text>();
                foreach (TMP_Text text in texts)
                {
                    text.color = new Color(text.color.r, text.color.g, text.color.b,
                        text.color.a + Time.deltaTime);
                }
                if (texts[0].color.a >= 1)
                {
                    fadingIn = false;
                }
            }
            if (fadingOut)
            {
                fadeColor = new Color(0f, 0, 0, 0f);
                Image[] images = GetComponentsInChildren<Image>();
                foreach (Image image in images)
                {
                    image.color = new Color(image.color.r, image.color.g, image.color.b,
                        image.color.a - Time.deltaTime);
                }
                TMP_Text[] texts = GetComponentsInChildren<TMP_Text>();
                foreach (TMP_Text text in texts)
                {
                    text.color = new Color(text.color.r, text.color.g, text.color.b,
                        text.color.a - Time.deltaTime);
                }
                if (texts[0].color.a <= 0)
                {
                    fadingOut = false;
                }
            }
        }
        
        void ChangingUIHealth()
        {
            if (!gv.hudOn && !healthText.text.Equals("") )
            {
                healthText.text = "";
            }
            else if(playerStats.lifes >= 0 && gv.hudOn)
            {
                heart.enabled= true;
                healthText.text = ("x " + playerStats.lifes.ToString());
            }
        }

        public void fadeIn()
        {
            fadingIn = true;
            fadingOut = false;
        }

        public void fadeOut()
        {
            fadingOut = true;
            fadingIn = false;
        }
    }
}