﻿using _1BloodyLayne.Control;
using TMPro;
using Units.Player.Scripts.PlayerControl;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    public class HealthUI : MonoBehaviour
    {
        public SpriteRenderer emptyHeartImage;
        public float fillChangeSpeed;
        public SpriteRenderer fullHealthImage;
        
        [SerializeField] private Material emptyHeartMaterial, filledHeartMaterial;
        
        private DamageUIScript damageScript;
        private PlatformerCharacter2D playerChar;
        private PlayerStats playerStats;
        private TMP_Text healthText, maxHealthText;
        private Color fadeColor;
        private bool fadingIn, fadingOut;
        private GlobalVariables gv;
        private SpriteRenderer[] sRenderers;
        private float startingBeatsPerSecond;
        private static readonly int Fade = Shader.PropertyToID("_Fade");
        private static readonly int BeatsPerSecond = Shader.PropertyToID("_BeatsPerSecond");
        private Animator emptyHeartAnimator, filledHeartAnimator;


        void Awake()
        {
            damageScript = GameObject.FindGameObjectWithTag("Player").GetComponent<DamageUIScript>();
            playerChar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            playerStats = playerChar.GetComponent<PlayerStats>();
            healthText = GameObject.Find("CurrentHealthText").GetComponent<TMP_Text>();
            maxHealthText = GameObject.Find("MaximumHealthText").GetComponent<TMP_Text>();
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            sRenderers = GetComponentsInChildren<SpriteRenderer>();
            startingBeatsPerSecond = filledHeartMaterial.GetFloat(BeatsPerSecond);
            emptyHeartAnimator = GetComponent<Animator>();
            filledHeartAnimator = fullHealthImage.GetComponent<Animator>();
            GameController.PlayerHealthUI = this;
        }

        // Update is called once per frame
        void Update()
        {
                
            if (damageScript != null)
            {
                ChangingUIHealth();
            }
            else
            {
                playerChar = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
                damageScript = FindObjectOfType<DamageUIScript>();
                emptyHeartImage.enabled = false;
            }
            if (fadingIn)
            {
                foreach (SpriteRenderer sRenderer in sRenderers)
                {
                    sRenderer.color = new Color(sRenderer.color.r, sRenderer.color.g, sRenderer.color.b,
                        sRenderer.color.a + Time.deltaTime);
                }
                TMP_Text[] texts = {healthText, maxHealthText};
                fadeColor = new Color(1f, 1, 1, 1f);
                texts[0].color = new Color(texts[0].color.r, texts[0].color.g, texts[0].color.b, texts[0].color.a + Time.deltaTime);
                texts[1].color = new Color(texts[1].color.r, texts[1].color.g, texts[1].color.b, texts[1].color.a + Time.deltaTime);
                if (texts[1].color.a > 0.99f)
                {
                    foreach (SpriteRenderer sRenderer in sRenderers)
                    {
                        sRenderer.color = new Color(1f, 1, 1, 1f);
                    }
                    texts[0].color = new Color(1f, 1, 1, 1f);
                    texts[1].color = fadeColor;
                    fadingIn = false;
                }
            }
            if (fadingOut)
            {
                foreach (SpriteRenderer sRenderer in sRenderers)
                {
                    sRenderer.color = new Color(sRenderer.color.r, sRenderer.color.g, sRenderer.color.b,
                        sRenderer.color.a - Time.deltaTime);
                }
                TMP_Text[] texts = {healthText, maxHealthText};
                texts[0].color = new Color(texts[0].color.r, texts[0].color.g, texts[0].color.b, texts[0].color.a - Time.deltaTime);
                texts[1].color = new Color(texts[1].color.r, texts[1].color.g, texts[1].color.b, texts[1].color.a - Time.deltaTime);
                fadeColor = new Color(1f, 1, 1, 0f);
                if (texts[1].color.a < 0.01f)
                {
                    foreach (SpriteRenderer sRenderer in sRenderers)
                    {
                        sRenderer.color = fadeColor;
                    }
                    texts[0].color = fadeColor;
                    texts[1].color = fadeColor;
                    fadingOut = false;
                }
            }
        }
        
        private void ChangingUIHealth()
        {
            if (!gv.hudOn)
            {
                healthText.text = "";
                maxHealthText.text = "";
            }
            else
            {
                emptyHeartImage.enabled= true;
                healthText.text = (playerStats.health.ToString());
                maxHealthText.text = (playerStats.maxHealth.ToString());

                float currentHealthRatio = (float)playerStats.health / (float)playerStats.maxHealth;
                if (playerStats.health > playerStats.maxHealth)
                {
                    currentHealthRatio = 1;
                }
                                
                if (filledHeartMaterial.GetFloat(Fade) < (float)currentHealthRatio)
                {
                    float fillChange = fillChangeSpeed * Time.deltaTime;
                    if (filledHeartMaterial.GetFloat(Fade) + fillChange > (float)currentHealthRatio)
                    {
                        filledHeartMaterial.SetFloat(Fade, currentHealthRatio);
                    }
                    else
                    {
                        filledHeartMaterial.SetFloat(Fade, filledHeartMaterial.GetFloat(Fade) + fillChange);
                    }
                }
                else if (filledHeartMaterial.GetFloat(Fade) > currentHealthRatio)
                {
                    float fillChange = fillChangeSpeed * Time.deltaTime;
                    if (filledHeartMaterial.GetFloat(Fade) - fillChange < currentHealthRatio)
                    {
                        filledHeartMaterial.SetFloat(Fade, currentHealthRatio);
                    }
                    else
                    {
                        filledHeartMaterial.SetFloat(Fade, filledHeartMaterial.GetFloat(Fade) - fillChange);
                    }
                }
            }
        }

        public void fadeIn()
        {
            fadingIn = true;
            fadingOut = false;
        }

        public void fadeOut()
        {
            fadingOut = true;
            fadingIn = false;
        }
        

        public void SetHeartRate(float beatsPerSecond)
        {
            filledHeartMaterial.SetFloat(BeatsPerSecond, beatsPerSecond);
            emptyHeartMaterial.SetFloat(BeatsPerSecond, beatsPerSecond);
            emptyHeartAnimator.SetFloat("BeatsPerSecond", beatsPerSecond);
            filledHeartAnimator.SetFloat("BeatsPerSecond", beatsPerSecond);
        }

        public void ResetHeartRate()
        {
            filledHeartMaterial.SetFloat(BeatsPerSecond, startingBeatsPerSecond);
            emptyHeartMaterial.SetFloat(BeatsPerSecond, startingBeatsPerSecond);
            emptyHeartAnimator.SetFloat("BeatsPerSecond", startingBeatsPerSecond);
            filledHeartAnimator.SetFloat("BeatsPerSecond", startingBeatsPerSecond);
        }

        public void showDeadHeart()
        {
            SetHeartRate(0);
            emptyHeartAnimator.SetTrigger("Die");
            filledHeartAnimator.SetTrigger("Die");
        }
    } 
}