using System;
using System.Collections;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets._2D;

namespace UI.PlayerUI
{
    [RequireComponent(typeof(AudioSource))]
    public class Camera2DFollow : MonoBehaviour
    {
        public Transform target;
        public float lookAheadFactor = 3;
        public float lookAheadReturnSpeed = 0.5f;
        public float lookAheadMoveThreshold = 0.1f;
        [NonSerialized] public bool BlockLookAhead;
        [NonSerialized] public bool LookAheadAiming;
        
        [NonSerialized]
        public AudioSource CurrentMusicClip;
        
        [SerializeField] private float dampingOverwrite;
        [SerializeField] private float shakeDamping;
        [SerializeField] private float normalGameDamping;

        private bool _mainMenu;
        private float _startLookAheadFactor;
        private float _mOffsetZ, _shakeIntensity, _shakingStartTime, _shakeFrequency, _shakeDuration;
        private Vector3 _mLastTargetPosition;
        private Vector3 _mCurrentVelocity;
        private Vector3 _mLookAheadPos;
        private Vector2 _mLookAheadAimDirection;
        private bool _shakingCamera, _musicFadingOut;
        private PauseMenu _pauseMenu;
        private Coroutine _shakeCoroutine;

        // Use this for initialization
        private void Start()
        {
            _startLookAheadFactor = lookAheadFactor;
            _mainMenu = SceneManager.GetActiveScene().name.Equals("MainMenu");
            if (!_mainMenu)
            {
                target = GameObject.FindGameObjectWithTag("Player").transform;
                Vector3 position = transform.position;
                _mLastTargetPosition = position;
                _mOffsetZ = (position - target.position).z;
            }
            transform.parent = null;
            CurrentMusicClip = GetComponent<AudioSource>();
            _pauseMenu = GetComponent<PauseMenu>();
            CurrentMusicClip.volume = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().musicVolume;
        }


        // Update is called once per frame
        private void Update()
        {
            if (!_mainMenu)
            {
                // only update lookahead pos if accelerating or changed direction
                float xMoveDelta = (target.position - _mLastTargetPosition).x;
                Vector3 aheadTargetPos;
                if (LookAheadAiming)
                {
                    if (!BlockLookAhead)
                    {
                        _mLookAheadPos = -_mLookAheadAimDirection.normalized * lookAheadFactor;
                    }
                    else
                    {
                        _mLookAheadPos = Vector3.MoveTowards(_mLookAheadPos, Vector3.zero,
                            Time.deltaTime * lookAheadReturnSpeed);
                    }
                    aheadTargetPos = target.position + _mLookAheadPos + Vector3.forward * _mOffsetZ;
                }
                else
                {
                    bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

                    if (updateLookAheadTarget && !BlockLookAhead)
                    {
                        _mLookAheadPos = Vector3.right * (lookAheadFactor * Mathf.Sign(xMoveDelta));
                    }
                    else
                    {
                        _mLookAheadPos = Vector3.MoveTowards(_mLookAheadPos, Vector3.zero,
                            Time.deltaTime * lookAheadReturnSpeed);
                    }
                    aheadTargetPos = target.position + _mLookAheadPos + Vector3.forward * _mOffsetZ;
                }
                Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref _mCurrentVelocity, dampingOverwrite);

                if (_shakingCamera && !_pauseMenu.pause)
                {
                    Vector3 shakedNewPos = new Vector3(newPos.x, target.position.y + (float)Math.Sin((Time.time - _shakingStartTime) * _shakeFrequency) * _shakeIntensity, newPos.z);
                    newPos = Vector3.SmoothDamp(newPos, shakedNewPos, ref _mCurrentVelocity, shakeDamping);
                }
                
                transform.position = newPos;

                _mLastTargetPosition = target.position;
            }

            if (_musicFadingOut)
            {
                if (CurrentMusicClip.volume > 0)
                {
                    CurrentMusicClip.volume -= Time.deltaTime;
                }
                else
                {
                    _musicFadingOut = false;
                    CurrentMusicClip.Stop();
                }
            }
        }

        public void LookIntoAimDirection(Vector2 direction)
        {
            _mLookAheadAimDirection = direction;
            LookAheadAiming = true;
        }
        
        public void ShakeCamera(float seconds, float intensity, float frequency)
        {
            if (_shakeCoroutine != null)
            {
                if (seconds > _shakeDuration - (Time.time - _shakingStartTime))
                {
                    StopCoroutine(_shakeCoroutine);
                    _shakingStartTime = Time.time;
                    _shakingCamera = true;
                    _shakeIntensity = Math.Max(intensity, _shakeIntensity);
                    _shakeFrequency = Math.Max(frequency, _shakeFrequency);
                    _shakeDuration = seconds;
                    _shakeCoroutine = StartCoroutine(ShakeTime(seconds));
                }
            }
            else
            {
                _shakingStartTime = Time.time;
                _shakingCamera = true;
                _shakeIntensity = intensity;
                _shakeFrequency = frequency;
                _shakeDuration = seconds;
                _shakeCoroutine = StartCoroutine(ShakeTime(seconds));
            }
        }

        private IEnumerator ShakeTime(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            _shakingCamera = false;
        }

        public void FadeOutMusic()
        {
            _musicFadingOut = true;
        }

        public void SwitchSound(AudioClip newClip)
        {
            StartCoroutine(SwitchMusic(newClip));
        }
        
        public void SetDamping(float newDamping, float newLookAhead = 999)
        {
            dampingOverwrite = newDamping;
            if (newLookAhead != 999)
            {
                lookAheadFactor = newLookAhead;
            }
        }

        public void ResetDamping()
        {
            dampingOverwrite = normalGameDamping;
            lookAheadFactor = _startLookAheadFactor;
        }

        public bool IsStartDamping()
        {
            return dampingOverwrite == normalGameDamping;
        }

        private IEnumerator SwitchMusic(AudioClip newClip)
        {
            FadeOutMusic();
            yield return new WaitForSeconds(1);
            CurrentMusicClip.clip = newClip;
            CurrentMusicClip.PlayDelayed(0);
            CurrentMusicClip.volume = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().musicVolume;
        }
    }
}
