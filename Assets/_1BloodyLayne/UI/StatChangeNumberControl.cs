using System;
using TMPro;
using UnityEngine;

//fades out the number displayed when dealing attackDamage, receiving attackDamage or healing
public class StatChangeNumberControl : MonoBehaviour
{
    public float fadingDelay, totalLifeTime, totalHeightIncreaseOverTime, sizeLowerLimit, sizeUpperLimit;
    public Color dealingDamageColor, receivingDamageColor, healingColor;
    public int lowDamageCap, highDamageCap, lowHealingCap, highHealingCap;

    private float startTime, heightYValue;
    private TMP_Text textComponent;
    private RectTransform rectTransform;
    
    void Awake()
    {
        startTime = Time.time; 
        textComponent = GetComponent<TMP_Text>();
        rectTransform = GetComponent<RectTransform>();
        heightYValue = rectTransform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        //check if fading delay is over
        if(Time.time - startTime > fadingDelay)
        {
            float newColorAlpha;

            //calculate new alpha color
            newColorAlpha = 1 - (((Time.time - startTime - fadingDelay) / (totalLifeTime - fadingDelay)) * 1);

            //check if alpha color is 0 or less and if so destroy
            if (newColorAlpha <= 0)
            {
                Destroy(gameObject);
            }

            textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b,
                newColorAlpha);
        }
        
        //set new height
        float newHeight = heightYValue + (((Time.time - startTime) / totalLifeTime) * totalHeightIncreaseOverTime);
        rectTransform.position = new Vector3(rectTransform.position.x, newHeight, rectTransform.position.z);
    }

    //setup Text and color by info type
    public void setupInfoNumber(int damageDealt, DamageNumberType numberType)
    {
        int lowCap = 0, highCap = 0;
        
        textComponent.text = damageDealt.ToString();
        Color newColor = Color.black;

        switch (numberType)
        {
            case DamageNumberType.DamageDealt:
                newColor = dealingDamageColor;
                lowCap = lowDamageCap;
                highCap = highDamageCap;
                break;
            case DamageNumberType.DamageReceived:
                newColor = receivingDamageColor;
                lowCap = lowDamageCap;
                highCap = highDamageCap;
                break;
            case DamageNumberType.HealingDone:
                newColor = healingColor;
                lowCap = lowHealingCap;
                highCap = highHealingCap;
                break;
        }

        //sets every color value to 0 or 1, depending on if it is 0  or not
        //fullBrightnessColor = new Color(newColor.r > 0 ? 1f : 0f, newColor.g > 0 ? 1f : 0f, newColor.b > 0 ? 1f : 0f, 1);

        float h, s, v;
        Color.RGBToHSV(newColor, out h, out s, out v);
        v = 1;
        Color fullBrightnessColor = Color.HSVToRGB(h, s, v);
        
        
        //set the brightness of the color according to to the amount amount of attackDamage done
        newColor = Color.Lerp(newColor, fullBrightnessColor, (float)(damageDealt - lowCap)/(highCap - lowCap));
        textComponent.color = newColor;

        float newSize = Mathf.Lerp(sizeLowerLimit, sizeUpperLimit, (float)(damageDealt - lowCap)/(highCap - lowCap));
        rectTransform.localScale = new Vector3(newSize, newSize, newSize);
    }
}

public enum DamageNumberType
{
    DamageDealt,
    DamageReceived,
    HealingDone
}