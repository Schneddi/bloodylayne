using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;

using UnityStandardAssets._2D;

public class LightningEffectSetting : MonoBehaviour
{
    private GlobalVariables gv;
    private bool isEnabled = true;
    // Start is called before the first frame update
    void Start()
    {
        gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gv.lightningEffects && !isEnabled)
        {
            isEnabled = true;
            GetComponent<UnityEngine.Rendering.Universal.Light2D>().enabled = true;
        }

        if (!gv.lightningEffects && isEnabled)
        {
            isEnabled = false;
            GetComponent<UnityEngine.Rendering.Universal.Light2D>().enabled = false;
        }
    }
}
