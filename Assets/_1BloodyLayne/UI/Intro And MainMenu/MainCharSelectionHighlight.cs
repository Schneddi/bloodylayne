using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainCharSelectionHighlight : MonoBehaviour
{
    private Button selectionButton;
    private SpriteRenderer[] charSprites;
    private float colorRGBGoal = 0.25f;
    // Start is called before the first frame update
    void Start()
    {
        selectionButton = transform.parent.parent.parent.GetComponent<Button>();
        charSprites = GetComponentsInChildren<SpriteRenderer>();foreach (SpriteRenderer charSprite in charSprites)
        {
            charSprite.color = new Color(colorRGBGoal, colorRGBGoal,colorRGBGoal, 1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
        {
            if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() == selectionButton)
            {
                if (charSprites[0].color.r < 1)
                {
                    foreach (SpriteRenderer charSprite in charSprites)
                    {
                        charSprite.color = new Color(charSprite.color.r + Time.deltaTime*4, charSprite.color.g + Time.deltaTime*4, charSprite.color.b + Time.deltaTime*4, 1);
                    }
                }
            }
            else
            {
                if (charSprites[0].color.r > colorRGBGoal)
                {
                    foreach (SpriteRenderer charSprite in charSprites)
                    {
                        charSprite.color = new Color(charSprite.color.r - Time.deltaTime*4, charSprite.color.g - Time.deltaTime*4, charSprite.color.b - Time.deltaTime*4, 1);
                    }
                }
            }
        }
    }
}
