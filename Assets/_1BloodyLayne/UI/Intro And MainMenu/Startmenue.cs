﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Startmenue : MonoBehaviour {

    public GameObject mainMenue, mainMenueUnlockedEnds, mainMenueNoContinue, options, controls, endingMenue, humanEndingButton, servantEndingButton, vampireEndingButton, devilEndingButton, credits, characterSelection;
    private GameObject loadingButton, startButton, endingsBackButton, optionsBackButton, controlsBackButton, creditsBackButton, characterSelectionButton;
    Button lastActiveMainButton, lastActiveOptionsButton, mainMenueSelectedFirstButton, currentActiveButton;
    private GameObject lastActiveMainButtonGO;


    // Use this for initialization
    void Start ()
    {
        Cursor.visible = false;
        //set Up mainMenue Grid gameObject. If no endings are unlocked use default mainMenue
        bool endingUnlocked = (int?)PlayerPrefs.GetInt("HumanEnding") != 0 ? true : ((int?)PlayerPrefs.GetInt("ServantEnding") != 0 ? true : ((int?)PlayerPrefs.GetInt("VampireEnding") != 0 ? true : ((int?)PlayerPrefs.GetInt("DevilEnding") != 0 ? true : false)));
        
        //set up loading Button, depending on if na safe is detected. Set it as first Button if possible, else firstButton = newGame
        int? szeneCheck = (int?)PlayerPrefs.GetInt("scene");

        if (szeneCheck.GetValueOrDefault() == 0)
        {
            mainMenueNoContinue.SetActive(true);
            mainMenue = mainMenueNoContinue;
        }
        else if (!endingUnlocked)
        {
            loadingButton = mainMenue.transform.Find("ButtonGrid").Find("ButtonLaden").gameObject;
            mainMenue.SetActive(true);
        }
        else if (endingUnlocked)
        {
            mainMenueUnlockedEnds.SetActive(true);
            mainMenue = mainMenueUnlockedEnds;
            loadingButton = mainMenue.transform.Find("ButtonGrid").Find("ButtonLaden").gameObject;
        }
        
        //setUpButtons
        startButton = mainMenue.transform.Find("ButtonGrid").Find("ButtonNeuesSpiel").gameObject;
        endingsBackButton = endingMenue.transform.Find("ButtonGrid").Find("ButtonBack").gameObject;
        optionsBackButton = options.transform.Find("Back").gameObject;
        controlsBackButton = controls.transform.Find("Back").gameObject;
        creditsBackButton = credits.transform.Find("Back").gameObject;
        characterSelectionButton =  characterSelection.transform.Find("Back").gameObject;
        Time.timeScale = 1;


        if (szeneCheck.GetValueOrDefault() == 0)
        {
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(startButton);
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(loadingButton);
        }
        
        if ((int?)PlayerPrefs.GetInt("HumanEnding") == 0)
        {
            humanEndingButton.SetActive(false);
        }
        if ((int?)PlayerPrefs.GetInt("ServantEnding") == 0)
        {
            servantEndingButton.SetActive(false);
        }
        if ((int?)PlayerPrefs.GetInt("VampireEnding") == 0)
        {
            vampireEndingButton.SetActive(false);
        }
        if ((int?)PlayerPrefs.GetInt("DevilEnding") == 0)
        {
            devilEndingButton.SetActive(false);
        }
    }
    
    public void showCharacterSelection()
    {
        if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
        {
            lastActiveMainButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        }
        mainMenue.SetActive(false);
        characterSelection.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(characterSelectionButton);
    }
    
    public void showEndingsMenue()
    {
        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
        {
            lastActiveMainButton = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        }
        mainMenue.SetActive(false);
        endingMenue.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(endingsBackButton);
    }

    public void showOptionsMenue()
    {
        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
        {
            lastActiveMainButton = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        }
        mainMenue.SetActive(false);
        options.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(optionsBackButton);
    }

    public void showControlsMenue()
    {
        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
        {
            lastActiveOptionsButton = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        }
        options.SetActive(false);
        controls.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(controlsBackButton);
    }

    public void openCredits()
    {
        if (EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
        {
            lastActiveOptionsButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        }
        options.SetActive(false);
        credits.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(creditsBackButton);
    }

    public void backControls()
    {
        controls.SetActive(false);
        options.SetActive(true);
        credits.SetActive(false);
        lastActiveOptionsButton.Select();
    }

    public void zurueck()
    {
        mainMenue.SetActive(true);
        endingMenue.SetActive(false);
        options.SetActive(false);
        characterSelection.SetActive(false);
        lastActiveMainButton.Select();
    }

    public void Ende()
    {
        Application.Quit();
    }
}
