﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClickSound : MonoBehaviour
{
    public bool clickSound;
    Button someButton;
    AudioSource click, menueSwitchSound;

    void OnEnable()
    {
        //Register Button Events
        someButton = GetComponent<Button>();
        someButton.onClick.AddListener(() => buttonCallBack());
        menueSwitchSound = GameObject.Find("UISounds").GetComponents<AudioSource>()[0];
        click = GameObject.Find("UISounds").GetComponents<AudioSource>()[1];
    }

    private void buttonCallBack()
    {
        if (clickSound)
        {
            click.PlayDelayed(0);
        }
        else
        {
            menueSwitchSound.PlayDelayed(0);
        }
    }

    void OnDisable()
    {
        //Un-Register Button Events
        someButton.onClick.RemoveAllListeners();
    }
}