﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHoverSound : MonoBehaviour
{
    AudioSource hoverSound;
    void Awake()
    {
        hoverSound = GameObject.Find("UISounds").GetComponents<AudioSource>()[2];
    }
    
    public void playHover()
    {
        hoverSound.PlayOneShot(hoverSound.clip);
    }

}