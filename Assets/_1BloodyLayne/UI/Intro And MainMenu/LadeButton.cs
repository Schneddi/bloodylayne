﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LadeButton : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        int? szeneCheck = (int?)PlayerPrefs.GetInt("scene");
        if (szeneCheck.GetValueOrDefault() == 0)
        {
            GetComponent<UnityEngine.UI.Image>().color = new Color32(30, 0, 0, 200); ;
            GetComponent<UnityEngine.UI.Button>().enabled=false;
            //gameObject.active = false;
        }
        else
        {
	        transform.parent.GetComponentInChildren<EventSystem>().firstSelectedGameObject = gameObject;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
