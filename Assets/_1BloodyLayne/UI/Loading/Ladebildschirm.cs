﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class Ladebildschirm : MonoBehaviour
    {
        Text loadingText;

        // Use this for initialization
        void Start()
        {
            loadingText = GetComponent<Text>();
            StartCoroutine(ladePunkte());
        }

        IEnumerator ladePunkte()
        {
            if (GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().Language == 0)
            {
                while (true)
                {
                    loadingText.text = ("Loading");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = (".Loading.");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = ("..Loading..");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = ("...Loading...");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = ("....Loading....");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = (".....Loading.....");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = ("......Loading......");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = (".......Loading.......");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = ("........Loading........");
                    yield return new WaitForSeconds(0.1f);
                }
            }
            if (GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>().Language == 1)
            {
                while (true)
                {
                    loadingText.text = ("Lade");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = (".Lade.");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = ("..Lade..");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = ("...Lade...");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = ("....Lade....");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = (".....Lade.....");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = ("......Lade......");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = (".......Lade.......");
                    yield return new WaitForSeconds(0.1f);
                    loadingText.text = ("........Lade........");
                    yield return new WaitForSeconds(0.1f);
                }
            }
        }
    }
}
