using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets._2D;

public class LoadingBar : MonoBehaviour
{
    private GameController gc;
    private Slider slider;
    // Start is called before the first frame update
    void Start()
    {
        gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
        slider = GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = gc.loadingProgress;
    }
}
