﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class BossUIControl : MonoBehaviour
    {
        public GameObject listenDialog;
        public BossUI bossUI;
        public bool fadeIn, fadeOut;
        bool listen;
        
        void Update()
        {
            if (listenDialog.activeSelf)
            {
                listen = true;
            }
            if (!listenDialog.activeSelf && listen)
            {
                if (fadeIn)
                {
                    bossUI.activateBossUI();
                }
                else if (fadeOut)
                {
                    bossUI.deactivateBossUI();
                }
                listen = false;
            }
        }
    }
}
