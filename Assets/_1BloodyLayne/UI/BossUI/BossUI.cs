﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Enemys;
using UnityEngine;
using UnityEngine.UI;


namespace UnityStandardAssets._2D
{
    public class BossUI : MonoBehaviour
    {
        public EnemyStats stats;
        public GameObject listenDialog;
        public float fillChangeSpeed;

        private Image filledHealthBar, empyHealthBar, borderImage;
        private Image[] images;
        private Text[] bossNames;
        private bool fadingIn, fadingOut, listen, checkDialog, checkHealth, shiftingRight, shiftingMiddle;
        private float lastFillAmount;
        private RectTransform barArea;
        private Vector2 startAnchorMin, startAnchorMax;
        private GlobalVariables gv;

        void Start()
        {
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            empyHealthBar = GetComponentsInChildren<Image>()[0];
            filledHealthBar = GetComponentsInChildren<Image>()[1];
            borderImage = GetComponentsInChildren<Image>()[2];
            images = new[] { empyHealthBar, filledHealthBar, borderImage };
            bossNames = GetComponentsInChildren<Text>();
            if (listenDialog != null)
            {
                checkDialog = true;
            }

            filledHealthBar.fillAmount = 0;
            foreach (Image image in images)
            {
                image.color = new Color(image.color.r, image.color.g, image.color.b, 0);
            }
            foreach (Text bossName in bossNames)
            {
                bossName.color = new Color(bossName.color.r, bossName.color.g, bossName.color.b, 0);
            }
            barArea = transform.Find("BossStatusUI").GetComponent<RectTransform>();
            startAnchorMin = barArea.anchorMin;
            startAnchorMax = barArea.anchorMax;
        }

        // Update is called once per frame
        void Update()
        {
            if (checkDialog)
            {
                checkDialogActivation();
            }
            if (checkHealth)
            {
                ChangingUIHealth();
            }
            if (fadingIn)
            {
                float alphaChange = 2 * Time.deltaTime;
                foreach (Image image in images)
                {
                    image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a + alphaChange);
                }
                foreach (Text bossName in bossNames)
                {
                    bossName.color = new Color(bossName.color.r, bossName.color.g, bossName.color.b, bossName.color.a + alphaChange);
                }
                if (bossNames[0].color.a >= 1f)
                {
                    fadingIn = false;
                    checkHealth = true;
                }
            }
            if (fadingOut)
            {
                float alphaChange = 2 * Time.deltaTime;
                foreach (Image image in images)
                {
                    image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a - alphaChange);
                }
                foreach (Text bossName in bossNames)
                {
                    bossName.color = new Color(bossName.color.r, bossName.color.g, bossName.color.b, bossName.color.a - alphaChange);
                }
                if (bossNames[0].color.a <= 0f)
                {
                    checkHealth = false;
                    fadingOut = false;
                }
            }

            if (shiftingRight)
            {
                if (barArea.anchorMin.x + Time.deltaTime > startAnchorMin.x + 0.35f)
                {
                    barArea.anchorMin = new Vector2(startAnchorMin.x + 0.35f, barArea.anchorMin.y);
                    barArea.anchorMax = new Vector2(startAnchorMax.x + 0.35f, barArea.anchorMax.y);
                    shiftingRight = false;
                }
                else
                {
                    barArea.anchorMin = new Vector2(barArea.anchorMin.x + Time.deltaTime * 0.75f, barArea.anchorMin.y);
                    barArea.anchorMax = new Vector2(barArea.anchorMax.x + Time.deltaTime * 0.75f, barArea.anchorMax.y);
                }
            }

            if (shiftingMiddle)
            {
                if (barArea.anchorMin.x + Time.deltaTime < startAnchorMin.x)
                {
                    barArea.anchorMin = new Vector2(startAnchorMin.x, barArea.anchorMin.y);
                    barArea.anchorMax = new Vector2(startAnchorMax.x, barArea.anchorMax.y);
                    shiftingMiddle = false;
                }
                else
                {
                    barArea.anchorMin = new Vector2(barArea.anchorMin.x - Time.deltaTime * 0.75f, barArea.anchorMin.y);
                    barArea.anchorMax = new Vector2(barArea.anchorMax.x - Time.deltaTime * 0.75f, barArea.anchorMax.y);
                }
            }
        }

        private void checkDialogActivation()
        {
            if (listenDialog.activeSelf)
            {
                listen = true;
            }
            if (!listenDialog.activeSelf && listen)
            {
                activateBossUI();
                listen = false;
            }
        }

        private void ChangingUIHealth()
        {
            if (stats.dead && !fadingOut)
            {
                fadingOut = true;
            }

            if (filledHealthBar.fillAmount < (float)stats.health / (float)stats.maxHealth)
            {
                float fillchange = fillChangeSpeed * Time.deltaTime;
                if (filledHealthBar.fillAmount + fillchange > (float)stats.health / (float)stats.maxHealth)
                {
                    filledHealthBar.fillAmount = (float)stats.health / (float)stats.maxHealth;
                }
                else
                {
                    filledHealthBar.fillAmount += fillchange;
                }
            }
            else if (filledHealthBar.fillAmount > (float)stats.health / (float)stats.maxHealth)
            {
                float fillchange = fillChangeSpeed * Time.deltaTime;
                if (filledHealthBar.fillAmount - fillchange < (float)stats.health / (float)stats.maxHealth)
                {
                    filledHealthBar.fillAmount = (float)stats.health / (float)stats.maxHealth;
                }
                else
                {
                    filledHealthBar.fillAmount -= fillchange;
                }
            }
        }

        public void shiftMiddle()
        {
            shiftingMiddle = true;
        }

        public void shiftRight()
        {
            shiftingRight = true;
        }
        
        public void activateBossUI()
        {
            if (gv.hudOn)
            {
                fadingIn = true;
                fadingOut = false;
            }
        }
        public void deactivateBossUI()
        {
            checkHealth = false;
            fadingOut = true;
            fadingIn = false;
        }
    }
}