using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class WhiteFadeScreen : MonoBehaviour
    {
        public bool fullWhite;
        private bool fadingOut, fadingIn;
        Color fadeColor;
    
        // Update is called once per frame
        void Update()
        {
            if (fadingIn)
            {
                fadeColor = new Color(1f, 1, 1, 1f);
                Image image = GetComponent<Image>();
                image.color = Color.Lerp(image.color, fadeColor, 0.1f);
                if (image.color.a > 0.99f)
                {
                    fadingIn = false;
                    fullWhite = true;
                    fadingOut = true;
                }
            }
            if (fadingOut)
            {
                fadeColor = new Color(1f, 1, 1, 0f);
                Image image = GetComponent<Image>();
                image.color = Color.Lerp(image.color, fadeColor, 0.1f);
                if (image.color.a < 0.01f)
                {
                    fadingOut = false;
                    gameObject.SetActive(false);
                }
            }
        }

        public void startFadeScreen()
        {
            gameObject.SetActive(true);
            fadingIn = true;
            fullWhite = false;
        }
    }
}
