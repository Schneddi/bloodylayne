﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UnityStandardAssets._2D
{
    public class DialogEndEmotion : MonoBehaviour
    {
        public GameObject character;
        public string animationString;
        public float animationTime;
        Platformer2DUserControl userControl;
        bool aktiv;

        // Use this for initialization
        void Start()
        {
            character.GetComponent<Animator>().SetTrigger(animationString);
            userControl = GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>();
        }
    }
}
