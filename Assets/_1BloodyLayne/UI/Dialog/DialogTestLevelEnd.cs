﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class DialogTestLevelEnd : MonoBehaviour
    {
        public GameObject listenDialog;
        bool listen;
        // Start is called before the first frame update
        void Start()
        {
            listen = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (listenDialog.GetComponent<Dialogbox>().finished && listen)
            {
                listen = false;
                GameController gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PauseMenu>().MainMenu();
            }
        }
    }
}
