﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class DialogChoiceTrigger : MonoBehaviour
    {
        public GameObject dialogChoices;
        Platformer2DUserControl userControl;
        GameObject gc, globalVariables;
        private GameObject nextDialog;
        Dialog dialog;
        // Use this for initialization
        void Start()
        {
            userControl = GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>();
            gc = GameObject.FindGameObjectWithTag("Global");
            dialog = FindObjectOfType<Dialog>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                dialogChoices.SetActive(true);
                userControl.BlockInput = true;
                Cursor.visible = true;
            }
        }

        private void dialogChoiceFinished()
        {
            if (nextDialog != null)
            {
                Cursor.visible = false;
                userControl.continuePress = true;
                dialog.ZeigeBox(nextDialog);
            }
            else
            {
                userControl.continuePress = true;
                userControl.BlockInput = false;
                Cursor.visible = false;
            }
            dialogChoices.SetActive(false);
        }
    }
}
