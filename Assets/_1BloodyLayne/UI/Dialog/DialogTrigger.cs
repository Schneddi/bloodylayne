﻿using System.Collections;
using System.Collections.Generic;
using Units.Player;
using UnityEngine;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class DialogTrigger : MonoBehaviour
    {
        Dialog dialog;
        public GameObject dialogbox, skillListener;
        public bool triggerSkillChoice, faceLeft;
        // Use this for initialization
        void Start()
        {
            dialog = FindObjectOfType<Dialog>();
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                GameObject.FindWithTag("Player").GetComponent<Rigidbody2D>().velocity = new Vector2(0, GameObject.FindWithTag("Player").GetComponent<Rigidbody2D>().velocity.y);
                if (!triggerSkillChoice)
                {
                    dialog.ZeigeBox(dialogbox);
                    this.gameObject.SetActive(false);
                }
                else
                {
                    skillListener.GetComponent<SkillChoice>().activateSkillUI(null);
                    this.gameObject.SetActive(false);
                }
                if (GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>().m_FacingRight && faceLeft)
                {
                    GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>().Flip();
                }
                else if(!GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>().m_FacingRight && !faceLeft)
                {
                    GameObject.FindWithTag("Player").GetComponent<PlatformerCharacter2D>().Flip();
                }
            }
        }
    }
}
