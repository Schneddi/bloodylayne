﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UI.PlayerUI;
using UnityEngine;
using UnityEngine.UI;



namespace UnityStandardAssets._2D
{
    public class Dialogbox : MonoBehaviour
    {
        Text enUeberschriftText, enMainText, enInfoText, deUeberschriftText, deMainText, deInfoText;
        string textBuffer;
        PauseMenu pauseControl;
        AudioSource dialogTextSound, dialogVoiceSoundEng, dialogVoiceSoundGer, currentPlayedSound;

        public float textTimer = 0.05f,
            cameraSize,
            cameraZoomSpeed = 1,
            cameraDamping,
            blockInputLength,
            waitAfterDialog;

        float voiceEntryPoint;
        float[] clipFloatData;

        public bool voiced,
            cameraChangeZoom,
            cameraInstantZoom,
            newCameraTarget,
            changeCameraDamping,
            emotion,
            noEndEmotion,
            noCameraReset,
            showLarryUI,
            hideLarryUI,
            blockInputLonger,
            dontReturnUserControl,
            noEndZooming,
            finished;

        public int animationLayer = 0;
        public string emotionAnimationName, startEmotionName, endEmotionName, mouthClosedEmotionName;
        public GameObject nextDialog, emotionTarget, cameraTarget;
        GlobalVariables gv;
        Coroutine voicingCoroutine, textCoroutine;
        Color[] imageColors, textColors;
        Animator emotionTargetAnim;
        public int sampleDataLength = 1024;
        public float closeMouthLoudness = 0.3f;
        private float currentUpdateTime = 0f;
        public float updateStep = 0.1f;
        private float clipLoudness;
        private float[] clipSampleData;
        private int language;

        // Use this for initialization
        void Awake()
        {
            enUeberschriftText = gameObject.transform.GetChild(0).GetComponent<Text>();
            enMainText = gameObject.transform.GetChild(1).GetComponent<Text>();
            enInfoText = gameObject.transform.GetChild(2).GetComponent<Text>();
            deUeberschriftText = gameObject.transform.GetChild(3).GetComponent<Text>();
            deMainText = gameObject.transform.GetChild(4).GetComponent<Text>();
            deInfoText = gameObject.transform.GetChild(5).GetComponent<Text>();
            Image[] images = GetComponentsInChildren<Image>();
            imageColors = new Color[images.Length];
            for (int i = 0; i< images.Length; i++)
            {
                imageColors[i] = images[i].color;
            }
            
            Text[] texts = GetComponentsInChildren<Text>();
            textColors = new Color[texts.Length];
            for (int i = 0; i< texts.Length; i++)
            {
                textColors[i] = texts[i].color;
            }
                
            if (!voiced)
            {
                dialogTextSound = gameObject.transform.GetComponents<AudioSource>()[0];
            }
            else
            {
                dialogVoiceSoundEng = gameObject.transform.GetComponents<AudioSource>()[1];
                dialogVoiceSoundGer = gameObject.transform.GetComponents<AudioSource>()[2];
                StartCoroutine(SelectVoiceLineAndPlay());
            }
            clipSampleData = new float[sampleDataLength];
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            pauseControl = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PauseMenu>();
            language = gv.Language;
            ChangeLanguage(gv.Language);
            ChangeSubtitles(gv.subtitles);
        }


        private void ChangeLanguage(int language)
        {
            if (this.language == language)
            {
                if (language == 0)
                {
                    if (textCoroutine == null)
                    {
                        textBuffer = enMainText.text;
                        enMainText.text = "";
                        textCoroutine = StartCoroutine(ShowText());
                    }
                }
                else if (language == 1)
                {
                    if (textCoroutine == null)
                    {
                        textBuffer = deMainText.text;
                        deMainText.text = "";
                        textCoroutine = StartCoroutine(ShowText());
                    }
                }
            }

            if (this.language != language)
            {
                if (language == 0)
                {
                    deMainText.text = textBuffer;
                    textBuffer = enMainText.text;
                    enMainText.text = "";
                }
                else if (language == 1)
                {
                    enMainText.text = textBuffer;
                    textBuffer = deMainText.text;
                    deMainText.text = "";
                }
                textCoroutine = StartCoroutine(ShowText());
                this.language = language;
            }

            if (language == 0)
            {
                enUeberschriftText.enabled = true;
                enMainText.enabled = true;
                enInfoText.enabled = true;
                deUeberschriftText.enabled = false;
                deMainText.enabled = false;
                deInfoText.enabled = false;
            }
            else if (language == 1)
            {
                enUeberschriftText.enabled = false;
                enMainText.enabled = false;
                enInfoText.enabled = false;
                deUeberschriftText.enabled = true;
                deMainText.enabled = true;
                deInfoText.enabled = true;
            }
        }

        private void ChangeSubtitles(int subtitles)
        {
            if (voiced && subtitles == 0)
            {
                Text[] texts = GetComponentsInChildren<Text>();
                foreach (Text text in texts)
                {
                    text.color = Color.clear;
                }

                Image[] images = GetComponentsInChildren<Image>();
                foreach (Image image in images)
                {
                    image.color = Color.clear;
                }

                if (textCoroutine != null)
                {
                    StopCoroutine(textCoroutine);
                }
            }
            
            if (voiced && subtitles == 1)
            {
                Text[] texts = GetComponentsInChildren<Text>();
                for (int i = 0; i< texts.Length; i++)
                {
                    texts[i].color = textColors[i];
                }
                Image[] images = GetComponentsInChildren<Image>();
                for (int i = 0; i< images.Length; i++)
                {
                    images[i].color = imageColors[i];
                }
            }
        }


        void Update()
        {
            if (emotion && !voiced)
            {
                emotionTargetAnim = emotionTarget.GetComponent<Animator>();
                emotionTargetAnim.SetTrigger(emotionAnimationName);
            }

            if (changeCameraDamping)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().SetDamping(cameraDamping);
            }

            if (newCameraTarget)
            {
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>().target =
                    cameraTarget.transform;
            }
            
            if (emotionTargetAnim != null)
            {
                if (!pauseControl.pause && voiced &&
                    (emotionTargetAnim.GetCurrentAnimatorStateInfo(animationLayer).IsName(emotionAnimationName) ||
                     emotionTargetAnim.GetCurrentAnimatorStateInfo(animationLayer).IsName(mouthClosedEmotionName)) && currentPlayedSound != null)
                {
                    currentUpdateTime += Time.deltaTime;
                    if (currentUpdateTime >= updateStep)
                    {
                        currentUpdateTime = 0f;
                        currentPlayedSound.clip.GetData(clipSampleData, currentPlayedSound.timeSamples); //I read 1024 samples, which is about 80 ms on a 44khz stereo clip, beginning at the current sample position of the clip.
                        clipLoudness = 0f;
                        foreach (float sample in clipSampleData)
                        {
                            clipLoudness += Mathf.Abs(sample);
                        }

                        clipLoudness /= sampleDataLength;
                        
                        if (clipLoudness <= closeMouthLoudness && !emotionTargetAnim.GetCurrentAnimatorStateInfo(animationLayer)
                            .IsName(mouthClosedEmotionName))
                        {
                            emotionTargetAnim.SetTrigger(mouthClosedEmotionName);
                        }
                        else if (clipLoudness > closeMouthLoudness && !emotionTargetAnim
                            .GetCurrentAnimatorStateInfo(animationLayer)
                            .IsName(emotionAnimationName))
                        {
                            emotionTargetAnim.SetTrigger(emotionAnimationName);
                        }
                    }
                }
            }
        }

        IEnumerator SelectVoiceLineAndPlay()
        {
            yield return StartCoroutine(SetStartEmotion());
            if (emotion)
            {
                emotionTargetAnim = emotionTarget.GetComponent<Animator>();
                emotionTargetAnim.SetTrigger(emotionAnimationName);
            }
            gv = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            if (gv.Language == 0)
            {
                currentPlayedSound = dialogVoiceSoundEng;
                voicingCoroutine = StartCoroutine(PlayVoiceAndGonext(0f));
            }
            if (gv.Language == 1)
            {
                currentPlayedSound = dialogVoiceSoundGer;
                voicingCoroutine = StartCoroutine(PlayVoiceAndGonext(0f));
            }
        }

        IEnumerator PlayVoiceAndGonext(float audioClipEntry)
        {
            currentPlayedSound.PlayDelayed(0);
            currentPlayedSound.time = audioClipEntry;
            float remainingLength = currentPlayedSound.clip.length - audioClipEntry;
            yield return new WaitForSeconds(remainingLength);
            if (gameObject.activeSelf)
            {
                gameObject.transform.parent.GetComponent<Dialog>().nextBox();
            }
        }

        private IEnumerator SetStartEmotion()
        {
            if (emotion && !string.IsNullOrEmpty(startEmotionName))
            {
                emotionTarget.GetComponent<Animator>().SetTrigger(startEmotionName);
                yield return new WaitForSeconds(0.1f);
                yield return new WaitForSeconds(emotionTarget.GetComponent<Animator>().GetCurrentAnimatorStateInfo(animationLayer).length);
            }
        }

        public void SetEndEmotion()
        {
            if (emotion && string.IsNullOrEmpty(endEmotionName) && !noEndEmotion)
            {
                emotionTarget.GetComponent<Animator>().ResetTrigger(emotionAnimationName);
                emotionTarget.GetComponent<Animator>().ResetTrigger(mouthClosedEmotionName);
                emotionTarget.GetComponent<Animator>().SetTrigger("Idle");
            }
            else if (emotion && !string.IsNullOrEmpty(endEmotionName) && !noEndEmotion)
            {
                if (!string.IsNullOrEmpty(emotionAnimationName))
                {
                    emotionTarget.GetComponent<Animator>().ResetTrigger(emotionAnimationName);
                }
                if(!string.IsNullOrEmpty(mouthClosedEmotionName))
                {
                    emotionTarget.GetComponent<Animator>().ResetTrigger(mouthClosedEmotionName);
                }
                emotionTarget.GetComponent<Animator>().SetTrigger(endEmotionName);
            }
        }

        public void GamePaused()
        {
            if (currentPlayedSound != null)
            {
                voiceEntryPoint = currentPlayedSound.time;
                currentPlayedSound.Stop();
            }
            foreach (Image image in GetComponentsInChildren<Image>())
            {
                image.color = new Color(0, 0, 0, 0);
            }

            foreach (Text text in GetComponentsInChildren<Text>())
            {
                text.color = new Color(0, 0, 0, 0);
            }
            if (textCoroutine != null)
            {
                StopCoroutine(textCoroutine);
            }
            if (voicingCoroutine != null && voiced)
            {
                StopCoroutine(voicingCoroutine);
            }
        }

        public void GameResumed()
        {
            Image[] images = GetComponentsInChildren<Image>();
            for (int i = 0; i< images.Length; i++)
            {
                images[i].color = imageColors[i];
            }
            ChangeSubtitles(gv.subtitles);
            ChangeLanguage(gv.Language);
            
            if ((voiced && gv.subtitles == 1) || !voiced)
            {
                Text[] texts = GetComponentsInChildren<Text>();
                for (int i = 0; i< texts.Length; i++)
                {
                    texts[i].color = textColors[i];
                }
            }

            if (voiced)
            {
                voicingCoroutine = StartCoroutine(PlayVoiceAndGonext(voiceEntryPoint));
            }
        }

        private IEnumerator ShowText()
        {
            if ((enMainText.text.Length < textBuffer.Length) && gv.Language == 0)
            {
                enMainText.text = textBuffer.Substring(0, enMainText.text.Length + 1);
                if (!voiced)
                {
                    dialogTextSound.PlayDelayed(0);
                }
                yield return new WaitForSeconds(textTimer);
                StartCoroutine(ShowText());
            }
            else if ((deMainText.text.Length < textBuffer.Length) && gv.Language == 1)
            {
                deMainText.text = textBuffer.Substring(0, deMainText.text.Length + 1);
                if (!voiced)
                {
                    dialogTextSound.PlayDelayed(0);
                }
                yield return new WaitForSeconds(textTimer);
                StartCoroutine(ShowText());
            }
        }
    }
}
