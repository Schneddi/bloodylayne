﻿using System;
using System.Collections;
using UI.PlayerUI;
using Units.Player;
using UnityEngine;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;


namespace UnityStandardAssets._2D
{
    public class Dialog : MonoBehaviour
    {
        public GameObject dialogBox;
        GameObject pauseUI;
        Platformer2DUserControl userControl;
        public bool dialogActive = false;
        AudioSource clickSound;
        public Dialogbox dBox;
        public float cameraOriginalSize;
        private bool zooming, endZooming, waiting;
        Camera cameraReference;
        Coroutine zoomCoroutine;
        private InputSystemUIInputModule inputModule;
        private bool submitPressed;

        void Start()
        {
            dialogBox.SetActive(false);
            userControl = GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>();
            clickSound = GetComponent<AudioSource>();
            cameraReference = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            cameraOriginalSize = cameraReference.orthographicSize;
            inputModule = GameObject.Find("EventSystem").GetComponent<InputSystemUIInputModule>();
        }

        void Awake()
        {
            pauseUI = GameObject.Find("Canvas PauseUI").gameObject;
        }
        
        protected void OnEnable()
        {
            inputModule = GameObject.Find("EventSystem").GetComponent<InputSystemUIInputModule>();
            inputModule.submit.action.performed += ctx => submitPressed = true;
        }

        void Update()
        {
            if (submitPressed && dialogActive && !pauseUI.activeSelf && !userControl.continuePress && !waiting)
            {
                nextBox();
            }
            submitPressed = false;
            if (dialogActive && !waiting)
            {
                dialogBox.SetActive(true);
                userControl.BlockInput = true;
                userControl.GetComponent<PlatformerCharacter2D>().NewInvulnerable(9999,false,true);
            }
            if (!dialogActive && !endZooming && cameraReference.orthographicSize != cameraOriginalSize)
            {
                dialogBox.SetActive(false);
                if (!dBox.noCameraReset)
                {
                    if (dBox.cameraInstantZoom && dBox.noEndZooming)
                    {
                        cameraReference.orthographicSize = cameraOriginalSize;
                    }
                    else if (cameraReference.orthographicSize != cameraOriginalSize && !endZooming)
                    {
                        endZooming = true;
                        if (zooming)
                        {
                            StopCoroutine(zoomCoroutine);
                        }
                        zoomCoroutine = StartCoroutine(zoom(cameraOriginalSize, 1));
                    }
                }
            }
        }
        public void ZeigeBox()
        {
            dBox = dialogBox.GetComponent<Dialogbox>();
            ZeigeBox(dBox.gameObject);
        }

        public void ZeigeBox(GameObject dialogBox)
        {
            if (checkEmotionTargetAlive())
            {
                this.dialogBox = dialogBox;
                dBox = dialogBox.GetComponent<Dialogbox>();
                dialogActive = true;
                userControl = GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>();
                userControl.BlockInput = true;
                userControl.GetComponent<PlatformerCharacter2D>().StopBloodthirstDamage();
                userControl.GetComponent<PlatformerCharacter2D>().NewInvulnerable(9999,false,true);
                checkZoomAndUI(dBox, false);
                cameraReference.GetComponent<Camera2DFollow>().BlockLookAhead = true;
            }
        }

        private bool checkEmotionTargetAlive()
        {
            if (dBox.emotion && dBox.emotionTarget != null)
            {
                return true;
            }
            else if (!dBox.emotion)
            {
                return true;
            }
            return false;
        }

        public void nextBox()
        {
            dBox.finished = true;
            if (dBox.nextDialog == null || dBox.waitAfterDialog > 0)
            {
                dBox.SetEndEmotion();
            }
            else if (dBox.nextDialog != null)
            {
                if (dBox.nextDialog.GetComponent<Dialogbox>().emotionTarget != dBox.emotionTarget)
                {
                    dBox.SetEndEmotion();
                }
            }
            dialogBox.SetActive(false);
            if(dBox.waitAfterDialog > 0)
            {
                StartCoroutine(waitAfterDialog(dBox.waitAfterDialog));
            }
            else if (dBox.nextDialog != null)
            {
                if (!dBox.voiced)
                {
                    clickSound.PlayDelayed(0);
                }
                dialogActive = true;
                dialogBox = dBox.nextDialog;
                dBox = dialogBox.GetComponent<Dialogbox>();
                dialogBox.SetActive(true);
                checkZoomAndUI(dBox, false);
            }
            else
            {
                if (!dBox.voiced)
                {
                    clickSound.PlayDelayed(0);
                }
                if (!dBox.blockInputLonger && !dBox.dontReturnUserControl)
                {
                    userControl.continuePress = true;
                    userControl.BlockInput = false;
                    GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().RestartBloodthirstDamage();
                    userControl.GetComponent<PlatformerCharacter2D>().StopInvulnerable();
                }
                else if (dBox.blockInputLonger && !dBox.dontReturnUserControl)
                {
                    StartCoroutine(blockInputForSeconds(dBox.blockInputLength));
                }
                dialogActive = false;
                actionsAfterDialog();
                if (!dBox.noCameraReset)
                {
                    if (!dBox.noEndZooming && cameraReference.GetComponent<Camera2DFollow>().target != userControl.transform)
                    {
                        StartCoroutine(cameraBackToPlayer());
                    }
                    else if (!cameraReference.GetComponent<Camera2DFollow>().IsStartDamping())
                    {
                        cameraReference.GetComponent<Camera2DFollow>().ResetDamping();
                    }
                }
                cameraReference.GetComponent<Camera2DFollow>().BlockLookAhead = false;
            }
        }

        private void checkZoomAndUI(Dialogbox currentDBox, bool afterDialog)
        {
            if (currentDBox.cameraInstantZoom && !afterDialog)
            {
                cameraReference.orthographicSize = currentDBox.cameraSize;
            }
            else if (currentDBox.cameraChangeZoom)
            {
                if (zooming)
                {
                    StopCoroutine(zoomCoroutine);
                }
                zoomCoroutine = StartCoroutine(zoom(currentDBox.cameraSize, currentDBox.cameraZoomSpeed));
            }
            if (currentDBox.hideLarryUI)
            {
                GameObject.Find("Souls").GetComponent<HealthUILeben>().fadeOut();
                GameObject.Find("PlayerStatusUI").GetComponentInChildren<HealthUI>().fadeOut();
                GameObject.Find("VeinsTopSprite").GetComponent<ImageFunctions>().fadeOut();
                if (!GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().noBloodThirstUI)
                {
                    GameObject.Find("VeinsBackground").GetComponent<ImageFunctions>().fadeOut();
                    GameObject.Find("BloodUI").GetComponent<BloodUI>().fadeOut();
                }
            }
        }

        void actionsAfterDialog()
        {
            if (dBox.showLarryUI)
            {
                GameObject.Find("Souls").GetComponent<HealthUILeben>().fadeIn();
                GameObject.Find("PlayerStatusUI").GetComponentInChildren<HealthUI>().fadeIn();
                GameObject.Find("VeinsTopSprite").GetComponent<ImageFunctions>().fadeIn();

                if (!GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().noBloodThirstUI)
                {
                    GameObject.Find("VeinsBackground").GetComponent<ImageFunctions>().fadeIn();
                    GameObject.Find("BloodUI").GetComponent<BloodUI>().fadeIn();
                }
            }
        }

        public IEnumerator zoom(float size, float speed)
        {
            zooming = true;
            yield return new WaitForSeconds(0.02f);
            if (cameraReference.orthographicSize - 0.06f < size && size < cameraReference.orthographicSize + 0.06f)
            {
                zooming = false;
                endZooming = false;
            }
            else if (cameraReference.orthographicSize > size)
            {
                cameraReference.orthographicSize -= 0.05f * speed;
                zoomCoroutine = StartCoroutine(zoom(size, speed));
            }
            else if(cameraReference.orthographicSize < size)
            {
                cameraReference.orthographicSize += 0.05f * speed;
                zoomCoroutine = StartCoroutine(zoom(size, speed));
            }
        }

        IEnumerator cameraBackToPlayer()
        {
            cameraReference.GetComponent<Camera2DFollow>().target = userControl.transform;
            for (int i = 30; i > 0; i--)
            {
                cameraReference.GetComponent<Camera2DFollow>().SetDamping((float)i / 100);
                yield return new WaitForSeconds(1f / 30f);
            }
            cameraReference.GetComponent<Camera2DFollow>().ResetDamping();
        }

        IEnumerator waitAfterDialog(float seconds)
        {
            waiting = true;
            yield return new WaitForSeconds(seconds);
            dBox.waitAfterDialog = 0;
            waiting = false;
            nextBox();
        }

        IEnumerator blockInputForSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            userControl.BlockInput = false;
            userControl.GetComponent<PlatformerCharacter2D>().StopInvulnerable();
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>().RestartBloodthirstDamage();
        }
    }
}
