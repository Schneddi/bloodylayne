using System;
using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class EndingScreen : MonoBehaviour
    {
        public float scrollSpeed, scrollEndHeigth;
        public int endingAchievementNumber;

        private GameObject scrollText, credits;
        private Image blackScreenImage;
        private Image[] endingImages;
        private bool fadeIn, fadeOutText, fadeOut, scrolling;
        private int shownCredits, remainingCreditsCount;
        private GameController gc;

        // Start is called before the first frame update
        void Start()
        {
            gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
            blackScreenImage = GetComponent<Image>();
            endingImages = new []{transform.Find("EndingImage").GetComponent<Image>(), transform.Find("BlackFade").GetComponent<Image>()};
            scrollText = transform.Find("ScrollText").gameObject;
            credits = transform.Find("CreditsTexts").gameObject;
            remainingCreditsCount = transform.Find("CreditsTexts").childCount;
            Text[] texts = scrollText.GetComponentsInChildren<Text>();
            foreach (Text text in texts)
            {
                text.color = new Color(1f, 1, 1, 0f);
            }
        }

        private void Update()
        {
            if (fadeIn)
            {
                GameObject.Find("Camera").GetComponent<PauseMenu>().blockMenue = true;
                foreach (Image endingImage in endingImages)
                {
                    endingImage.color = new Color(1, 1, 1, endingImage.color.a + Time.deltaTime * 1f);
                }
                blackScreenImage.color = new Color(blackScreenImage.color.r, blackScreenImage.color.g, blackScreenImage.color.b, blackScreenImage.color.a + Time.deltaTime * 1f);
                Text[] texts = scrollText.GetComponentsInChildren<Text>();
                foreach (Text text in texts)
                {
                    text.color = new Color(1, 1, 1, text.color.a + Time.deltaTime * 1f);
                }
                if (texts[1].color.a > 0.99f)
                {
                    foreach (Image endingImage in endingImages)
                    {
                        endingImage.color = new Color(1f, 1, 1, 1f);
                    }
                    blackScreenImage.color = new Color(0f, 0, 0, 1f);
                    foreach (Text text in texts)
                    {
                        text.color = new Color(1f, 1, 1, 1f);
                    }
                    fadeIn = false;
                    startScrolling();
                }
            }

            if (scrolling)
            {
                scrollText.GetComponent<RectTransform>().anchorMin = new Vector2(scrollText.GetComponent<RectTransform>().anchorMin.x, scrollText.GetComponent<RectTransform>().anchorMin.y + Time.deltaTime * scrollSpeed);
                scrollText.GetComponent<RectTransform>().anchorMax = new Vector2(scrollText.GetComponent<RectTransform>().anchorMax.x, scrollText.GetComponent<RectTransform>().anchorMax.y + Time.deltaTime * scrollSpeed);
                if (scrollText.GetComponent<RectTransform>().anchorMin.y > scrollEndHeigth)
                {
                    scrolling = false;
                    fadeOutText = true;
                }
            }
            
            if (fadeOutText)
            {
                Text[] texts = scrollText.GetComponentsInChildren<Text>();
                foreach (Text text in texts)
                {
                    text.color = new Color(1, 1, 1, text.color.a - Time.deltaTime * 4);
                }
                if (texts[1].color.a < 0.01f)
                {
                    foreach (Text text in texts)
                    {
                        text.color = new Color(1f, 1, 1, 0);
                    }
                    fadeOut = true;
                    fadeOutText = false;
                }
            }
            
            if (fadeOut)
            {
                foreach (Image endingImage in endingImages)
                {
                    endingImage.color = new Color(1, 1, 1, endingImage.color.a - Time.deltaTime * 0.25f);
                }
                if (endingImages[1].color.a < 0.01f)
                {
                    foreach (Image endingImage in endingImages)
                    {
                        endingImage.color = new Color(1f, 1, 1, 0f);
                    }
                    Text[] texts = scrollText.GetComponentsInChildren<Text>();
                    foreach (Text text in texts)
                    {
                        text.color = new Color(1f, 1, 1, 0f);
                    }
                    fadeOut = false;
                    StartCoroutine(playCredits());
                }
            }
        }

        public void startEndingScreen()
        {
            GameObject.FindWithTag("Player").GetComponent<Platformer2DUserControl>().BlockInput = true;
            fadeIn = true;
        }

        public void startScrolling()
        {
            scrolling = true;
        }
        
        IEnumerator playCredits()
        {
            setEndingAchievement();
            credits.transform.GetChild(shownCredits).gameObject.SetActive(true);
            yield return new WaitForSeconds(5);
            credits.transform.GetChild(shownCredits).gameObject.SetActive(false);
            shownCredits += 1;
            if (shownCredits >= remainingCreditsCount)
            {
                gc = GameObject.Find("GameController").transform.GetComponent<GameController>();
                gc.MainMenue();
            }
            else
            {
                StartCoroutine(playCredits());
            }
        }

        private void setEndingAchievement()
        {
            switch (endingAchievementNumber)
            {
                case 8:
                    gc.SetAchievement("HUMAN_ENDING_1");
                    break;
                case 9:
                    gc.SetAchievement("HUMAN_ENDING_2");
                    break;
                case 10:
                    gc.SetAchievement("SERVANT_ENDING_1");
                    break;
                case 11:
                    gc.SetAchievement("SERVANT_ENDING_2");
                    break;
                case 12:
                    gc.SetAchievement("SERVANT_ENDING_3");
                    break;
                case 13:
                    gc.SetAchievement("NOSFERATU_ENDING_1");
                    break;
                case 14:
                    gc.SetAchievement("NOSFERATU_ENDING_2");
                    break;
                case 15:
                    gc.SetAchievement("NOSFERATU_ENDING_3");
                    break;
                case 16:
                    gc.SetAchievement("DEVIL_ENDING_1");
                    break;
                case 17:
                    gc.SetAchievement("DEVIL_ENDING_2");
                    break;
                default:
                    break;
            }
        }
    }
}
