using System.Collections;
using System.Collections.Generic;
using UI.PlayerUI;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class BlackScreen : MonoBehaviour
    {
        public float fadeStartDelay;
        public GameObject dialogbox;
        public Transform cameraTarget;
        
        Dialog dialog;
        private bool fading;
        private Text fadeTextEng, fadeTextGer;
        Color fadeColor;
        private Camera2DFollow cameraRef;

        void Start()
        {
            dialog = FindObjectOfType<Dialog>();
            fadeTextEng = transform.Find("BlackScreenText").Find("enText").GetComponent<Text>();
            fadeTextGer = transform.Find("BlackScreenText").Find("deText").GetComponent<Text>();
            StartCoroutine(delayAndFade(fadeStartDelay));
            GameObject.FindGameObjectWithTag("Player").GetComponent<Platformer2DUserControl>().BlockInput = true;
            cameraRef = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DFollow>();
            cameraRef.GetComponent<PauseMenu>().blockMenue = true;
        }
    
        // Update is called once per frame
        void Update()
        {
            if (fading)
            {
                fadeColor = new Color(0f, 0, 0, 0f);
                Image image = GetComponent<Image>();
                image.color = Color.Lerp(image.color, fadeColor, 0.1f);
                Text[] texts = {fadeTextEng, fadeTextGer};
                fadeColor = new Color(1f, 1, 1, 0f);
                texts[0].color = Color.Lerp(texts[0].color, fadeColor, 0.1f);
                texts[1].color = Color.Lerp(texts[1].color, fadeColor, 0.1f);
                if (texts[1].color.a < 0.01f)
                {
                    if (dialogbox != null)
                    {
                        dialog.ZeigeBox(dialogbox);
                    }
                    cameraRef.GetComponent<PauseMenu>().blockMenue = false;
                    gameObject.SetActive(false);
                }
            }
        }
        
        
        IEnumerator delayAndFade(float delay)
        {
            yield return new WaitForSeconds(delay);
            if (cameraTarget != null)
            {
                cameraRef.target = cameraTarget;
            }
            else
            {
                cameraRef.target = dialogbox.GetComponent<Dialogbox>().cameraTarget.transform;
            }
            fading = true;
        }
    }
}
