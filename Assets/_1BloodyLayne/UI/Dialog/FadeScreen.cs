using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class FadeScreen : MonoBehaviour
    {
        private bool fadingOut, fadingIn;
        public float totalFadeTime = 2;

        // Update is called once per frame
        void Update()
        {
            if (fadingIn)
            {
                Image image = GetComponent<Image>();
                image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a + Time.deltaTime * totalFadeTime/2);
                if (image.color.a > 0.99f)
                {
                    image.color = new Color(image.color.r, image.color.g, image.color.b,1);
                    fadingIn = false;
                    fadingOut = true;
                }
            }
            if (fadingOut)
            {
                Image image = GetComponent<Image>();
                image.color = new Color(image.color.r, image.color.g, image.color.b, image.color.a - Time.deltaTime * totalFadeTime/2);
                if (image.color.a < 0.01f)
                {
                    image.color = new Color(image.color.r, image.color.g, image.color.b,0);
                    fadingOut = false;
                    gameObject.SetActive(false);
                }
            }
        }

        public void startFadeScreen()
        {
            gameObject.SetActive(true);
            fadingIn = true;
        }
    }
}