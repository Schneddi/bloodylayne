﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class SkillChoiceAutoText : MonoBehaviour
    {
        public int skill, skillOption, language;
        public bool upgradeName, skillName, skillDescription;
        Text skillText;
        GameObject globals;
        // Start is called before the first frame update
        void Start()
        {
            updateText();
        }

        // Update is called once per frame
        public void updateText()
        {
            globals = GameObject.FindGameObjectWithTag("Global");
            skillText = GetComponent<Text>();
            if (upgradeName)
            {
                skillText.text = globals.transform.GetChild(0).GetChild(skill).GetChild(language).GetComponent<Text>().text;
            }
            if (skillName)
            {
                skillText.text = globals.transform.GetChild(0).GetChild(skill).GetChild(2 + language + (skillOption - 1) * 4).GetComponent<Text>().text;
            }
            if (skillDescription)
            {
                skillText.text = globals.transform.GetChild(0).GetChild(skill).GetChild(2 + language + 2 + (skillOption - 1) * 4).GetComponent<Text>().text;
            }

        }
    }
}
