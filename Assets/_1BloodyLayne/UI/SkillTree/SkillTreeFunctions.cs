﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace UnityStandardAssets._2D
{
    public class SkillTreeFunctions : MonoBehaviour
    {
        GameObject skillDescribtion, activeSkillOption, skillOptionSource;
        GameObject globals;
        
        // Start is called before the first frame update
        void Start()
        {
            skillDescribtion = transform.Find("Describtion").gameObject;
            globals = GameObject.FindGameObjectWithTag("Global");
        }

        public void showDescribtion(GameObject skillOptionSource)
        {
            this.skillOptionSource = skillOptionSource;
            activeSkillOption = skillDescribtion;
            Button[] skillOptions = GetComponentsInChildren<Button>();
            foreach (Button skillOption in skillOptions)
            {
                skillOption.interactable = false;
            }
            skillDescribtion.SetActive(true);
            skillDescribtion.GetComponentInChildren<Button>().interactable = true;
            skillDescribtion.GetComponentInChildren<Button>().Select();
            skillDescribtion.transform.Find("SkillImage").GetComponent<Image>().sprite = globals.transform.GetChild(0).GetChild(skillOptionSource.GetComponent<SkillOption>().skill - 1).transform.Find("Image" + skillOptionSource.GetComponent<SkillOption>().skillOption).GetComponent<Image>().sprite;
            if (skillOptionSource.GetComponent<SkillOption>().skillOption == 1)
            {
                skillDescribtion.transform.Find("Video").Find("VideoPlayer").GetComponent<VideoPlayer>().clip = globals.transform.GetChild(0).GetChild(skillOptionSource.GetComponent<SkillOption>().skill - 1).GetComponent<SkillClips>().clip1;
            }
            else
            {
                skillDescribtion.transform.Find("Video").Find("VideoPlayer").GetComponent<VideoPlayer>().clip = globals.transform.GetChild(0).GetChild(skillOptionSource.GetComponent<SkillOption>().skill - 1).GetComponent<SkillClips>().clip2;
            }
            foreach(SkillChoiceAutoText skillDescribtionText in skillDescribtion.GetComponentsInChildren<SkillChoiceAutoText>())
            {
                skillDescribtionText.GetComponent<SkillChoiceAutoText>().skill = skillOptionSource.GetComponentInChildren<SkillChoiceAutoText>().skill;
                skillDescribtionText.GetComponent<SkillChoiceAutoText>().skillOption = skillOptionSource.GetComponentInChildren<SkillChoiceAutoText>().skillOption;
                skillDescribtionText.GetComponent<SkillChoiceAutoText>().updateText();
            }
        }

        public void backSkillDescribtion()
        {
            activeSkillOption.SetActive(false);
            Button[] skillOptions = GetComponentsInChildren<Button>();
            foreach (Button skillOption in skillOptions)
            {
                if (skillOption.GetComponent<SkillOption>() != null)
                {
                    if (skillOption.GetComponent<SkillOption>().known)
                    {
                        skillOption.interactable = true;
                    }
                }
                else
                {
                    skillOption.interactable = true;
                }
            }
            skillOptionSource.GetComponentInChildren<Button>().Select();
        }
    }
}
