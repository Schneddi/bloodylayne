﻿using System.Collections;
using System.Collections.Generic;
using _1BloodyLayne.Control;
using Units.Player;
using UnityEngine;
using UnityEngine.UI;
using PlatformerCharacter2D = Units.Player.Scripts.PlayerControl.PlatformCharacter2D.PlatformerCharacter2D;

namespace UnityStandardAssets._2D
{
    public class SkillOption : MonoBehaviour
    {
        public bool known;
        GlobalVariables gc;
        PlatformerCharacter2D larry;
        public int skill, skillOption;
        private Sprite knownImage;
        GameObject autoTextUpdateEng, autoTextUpdateGer, autoTextEng, autoTextGer;
        GameObject globals;

        public void updateSkillTree()
        {
            gc = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalVariables>();
            larry = GameObject.FindGameObjectWithTag("Player").GetComponent<PlatformerCharacter2D>();
            autoTextEng = transform.GetChild(0).GetComponent<SkillChoiceAutoText>().gameObject;
            autoTextGer = transform.GetChild(1).GetComponent<SkillChoiceAutoText>().gameObject;
            autoTextUpdateEng = transform.parent.GetChild(0).GetComponent<SkillChoiceAutoText>().gameObject;
            autoTextUpdateGer = transform.parent.GetChild(1).GetComponent<SkillChoiceAutoText>().gameObject;
            transform.parent.parent.Find("Describtion").gameObject.SetActive(false);
            globals = GameObject.FindGameObjectWithTag("Global");
            if (skill == 0)
            {
                if (gc.skill0Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill0choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill0choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 1)
            {
                if (gc.skill1Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill1choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill1choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 2)
            {
                if (gc.skill2Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill2choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill2choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 3)
            {
                if (gc.skill3Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill3choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill3choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 4)
            {
                if (gc.skill4Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill4choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill4choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 5)
            {
                if (gc.skill5Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill5choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill5choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 6)
            {
                if (gc.skill6Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill6choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill6choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 7)
            {
                if (gc.skill7Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill7choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill7choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 8)
            {
                if (gc.skill8Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill8choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill8choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 9)
            {
                if (gc.skill9Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill9choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill9choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 10)
            {
                if (gc.skill10Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill10choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill10choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 11)
            {
                if (gc.skill11Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill11choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill11choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 12)
            {
                if (gc.skill12Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill12choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill12choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
            if (skill == 13)
            {
                if (gc.skill13Known)
                {
                    knownBehavior();
                }
                if (skillOption == 1 && larry.skill13choice1)
                {
                    GetComponent<ParticleSystem>().Play();
                }
                if (skillOption == 2 && larry.skill13choice2)
                {
                    GetComponent<ParticleSystem>().Play();
                }
            }
        }

        void knownBehavior()
        {
            autoTextEng.GetComponent<SkillChoiceAutoText>().updateText();
            autoTextGer.GetComponent<SkillChoiceAutoText>().updateText();
            autoTextUpdateEng.GetComponent<SkillChoiceAutoText>().updateText();
            autoTextUpdateGer.GetComponent<SkillChoiceAutoText>().updateText();
            known = true;
            GetComponent<Button>().interactable = true;
            GetComponent<Image>().sprite = globals.transform.GetChild(0).GetChild(skill).transform.Find("Image" + skillOption).GetComponent<Image>().sprite;
        }
    }

}
